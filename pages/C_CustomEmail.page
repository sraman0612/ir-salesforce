<apex:page Controller="C_CustomEmail_Controller" id="thePage" docType="html-5.0" lightningstylesheets="true">
    <style type="text/css">
        .overlay {
            background: #e9e9e9;
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1;
            opacity: 0.5;
        }
        
        #loading-img {
            background: url({!$Resource.LoadingGif}) center center no-repeat;
            height: 100%;
            z-index: 20;
        }
    </style>
    <apex:includeScript value="{!URLFOR($Resource.TinyMCE, 'jquery-2.1.4.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.TinyMCE, 'tinymce/tinymce.min.js')}" />
    <script src="/support/console/34.0/integration.js" type="text/javascript"></script>
    <script type="text/javascript">
        var __sfdcSessionId = '{!GETSESSIONID()}';
    </script>
    <script src="/soap/ajax/30.0/connection.js" type="text/javascript"></script>
    <script>
        window.onload = function() {
            tinymce.execCommand('mceFocus', false, 'enhancedEditor');
        }

        function showPopUp() {
            console.log('Showing');
            $(".overlay").show();
        }

        function hidePopUp() {
            console.log('Hiding');
            $(".overlay").hide();
        }

        var editorID = "enhancedEditor";
        var initialSelection = true;

        function setupTINYMCE() {
            console.log('setupTINYMCE');
            tinymce.init({
                height: 300,
                menubar: false,
                selector: "#" + editorID,
                plugins: "image textcolor",
                toolbar: ["forecolor | backcolor | undo redo | styleselect | bold italic | outdent indent | bullist numlist | link image | alignleft aligncenter alignright "],
                force_p_newlines: false,
                force_br_newlines: true,
                convert_newlines_to_brs: false,
                remove_linebreaks: true,
                forced_root_block: false,
                browser_spellcheck: true,
                setup: function(editor) {
                    editor.on("init",
                        function(event) {
                            editor.setContent("{!templatedValue}");
                        }
                    );
                    editor.on("dragover", function(event) {
                        event.stopPropagation();
                        event.preventDefault();
                        event.dataTransfer.dropEffect = 'copy';
                    });
                    editor.on("drop", function(event) {
                        event.stopPropagation()
                        event.preventDefault()
                        var files = event.dataTransfer.files // FileList object.
                        uploadFileE(files);
                    });
                    editor.on("paste", function(event) {

                        var $this, element;
                        element = this;
                        $this = $(this);

                        defaults = {
                            callback: $.noop,
                            matchType: /image.*/
                        };

                        var options = $.extend({}, defaults, options);

                        var clipboardData, found;
                        found = false;
                        clipboardData = event.clipboardData;

                        return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
                            var file, reader;
                            if (found) {
                                return;
                            }

                            if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
                                file = clipboardData.items[i].getAsFile();
                                reader = new FileReader();
                                reader.onload = function(evt) {
                                    return options.callback.call(element, {
                                        dataURL: evt.target.result,
                                        event: evt,
                                        file: file,
                                        name: file.name
                                    });
                                };

                                reader.onloadend = function() {
                                    saveImage(this);
                                }

                                reader.readAsDataURL(file);
                                return found = true;
                            }
                        });
                    });
                }
            });
        }

        $(document).ready(function() {
            setupTINYMCE();
        });
    
        function uploadFileE(filesToUpload) {
            for (var i = 0, f; f = filesToUpload[i]; i++) {
                var reader = new FileReader();

                // Keep a reference to the File in the FileReader so it can be accessed in callbacks
                reader.file = f;

                reader.onload = function(e) {
                    var att = new sforce.SObject("Attachment");
                    att.Name = this.file.name;
                    att.ContentType = this.file.type;
                    att.ParentId = "{!IF(isActionItem, theCase.ParentId , theCase.Id)}";

                    var binary = "";
                    var bytes = new Uint8Array(e.target.result);
                    var length = bytes.byteLength;

                    for (var i = 0; i < length; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }

                    att.Body = (new sforce.Base64Binary(binary)).toString();

                    sforce.connection.create([att], {
                        onSuccess: function(result, source) {
                            if (result[0].getBoolean("success")) {
                                var newId = result[0].id;
                                console.log("new attachment created with id " + result[0].id);
                                //rerenderAttachments();
                                createNewFile(newId);
                            } else {
                                console.log("failed to create attachment " + result[0]);
                            }
                        },
                        onFailure: function(error, source) {
                            console.log("an error has occurred " + error);
                        }
                    });
                };

                reader.readAsArrayBuffer(f);
            }
        }

        function uploadFile() {
            var input = document.getElementById("file-input");
            var filesToUpload = input.files;
            
            for (var i = 0, f; f = filesToUpload[i]; i++) {
                var reader = new FileReader();

                // Keep a reference to the File in the FileReader so it can be accessed in callbacks
                reader.file = f;

                reader.onload = function(e) {
                    var att = new sforce.SObject("Attachment");
                    att.Name = this.file.name;
                    att.ContentType = this.file.type;
                    att.ParentId = "{!IF(isActionItem, theCase.ParentId , theCase.Id)}";

                    var binary = "";
                    var bytes = new Uint8Array(e.target.result);
                    var length = bytes.byteLength;

                    for (var i = 0; i < length; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }

                    att.Body = (new sforce.Base64Binary(binary)).toString();

                    sforce.connection.create([att], {
                        onSuccess: function(result, source) {
                            if (result[0].getBoolean("success")) {
                                var newId = result[0].id;
                                console.log("new attachment created with id " + result[0].id);
                                input.value = null;
                                //rerenderAttachments();
                                createNewFile(newId);
                            } else {
                                console.log("failed to create attachment " + result[0]);
                            }
                        },
                        onFailure: function(error, source) {
                            console.log("an error has occurred " + error);
                        }
                    });
                };

                reader.readAsArrayBuffer(f);
            }
        }

        function saveImage(reader) {
            var imageDataURL = reader.result.split(',')[1];

            // Prompts the user for a name for the image. If you remove this, compute a sensible name for the image in the controller@!
            // Currently, the controller takes whatever is entered by the user and appends a timestamp onto it.
            //var filename = prompt("Enter a name for this image: ", "Pasted_Image");

            console.log(imageDataURL);

            C_CustomEmail_Controller.saveImage('customemail', imageDataURL, handleResult);
        }

        function handleResult(result, event) {
            if (event.type == 'exception') {
                // There was an error, print the message in the console.
                console.debug(event.errorMessage);
            } else {
                // Returning the computed URL to the image via the error message in success cases.
                var imageURL = result.successMessage.replace("amp;", "");

                // Get the editor instance and insert an image with the above URL.
                var ed = tinyMCE.get(editorID);
                var range = ed.selection.getRng();
                var newNode = ed.getDoc().createElement("img");
                newNode.src = imageURL;
                range.insertNode(newNode);

                ed.selection.collapse(false);
                //var  msg = result;
                //refreshSubTab();
            }
        }

        function refreshSubTab() {
            sforce.console.getEnclosingTabId(refreshActiveSubtab);
            return true;
        }

        var refreshActiveSubtab = function refreshActiveSubtab(result) {
            // get tab Id
            var tabId = result.id;
            // refreshSubtabById(id:String, active:Boolean, (optional)callback:function)
            sforce.console.refreshSubtabById(tabId, true);
        };

        function sendContentToController() {
            console.log('sendContentToController');
            var content = tinyMCE.get(editorID).getContent({
                format: 'raw'
            });
            var Evalue = document.getElementById('thePage:theForm:thePageBlock:editorBody');
            Evalue.value = content;

            sendContent();
        }

        function setSubject(subject) {
            console.log('setSubject');
            var theField = document.getElementById('thePage:theForm:thePageBlock:theSection:emailSubjectField');
            theField.value = subject;
        }

        function SubmitOnClick() {
            console.log('SubmitOnClick ');
            $('.btn').each(function(elem) {
                this.className = 'btnDisabled';
                this.disabled = true;
                this.value = 'Processing...';
            })

            doSubmit();
        }
    </script>
    <apex:form id="theForm">
        <div class="overlay">
            <div id="loading-img"></div>
        </div>
        <apex:sectionHeader title="Task" subtitle="{!subjectHeader}" />
        <apex:actionFunction action="{!sendContent}" name="sendContent" rerender="renderMe" />
        <apex:actionFunction action="{!send}" name="doSubmit" />
        <apex:pageBlock mode="edit" id="thePageBlock">
            <apex:outputPanel id="renderMe">
                <apex:pageMessages />
            </apex:outputPanel>
            <apex:pageBlockButtons >
                <apex:commandButton value="Send" onclick="showPopUp();sendContentToController();SubmitOnClick();" /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <apex:commandButton action="{!cancel}" value="Cancel" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Edit Email" Columns="1" Collapsible="False" id="theSection">
                <apex:pageBlockSectionItem >
                    <apex:outputText value="From Address" />
                    <apex:outputText >&quot;{!fromAddress.DisplayName}&quot;&nbsp;&lt;{!fromAddress.Address}&gt;</apex:outputText>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Select Template" />
                    <apex:selectList size="1" value="{!selectedTemplate}" multiselect="false">
                        <apex:actionSupport event="onchange" onsubmit="if(!initialSelection || {!isResponse}){if(!confirm('Warning: Changing templates will overwrite all of the changes you have made to the current email. Do you want to continue?')){return;}showPopUp();}else{initialSelection = false;showPopUp();}" rerender="renderMe" oncomplete="hidePopUp();tinyMCE.activeEditor.setContent('{!templatedValue}');setSubject('{!templateSubject}');" />
                        <apex:selectOptions value="{!availableTemplates}" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!theCase.ContactId}" label="To Address" />
                <apex:pageBlockSectionItem >
                    <apex:outputText value="CC Addresses" />
                    <apex:pageBlockSectionItem >
                        <apex:inputTextArea value="{!freeFormCC}" cols="80" rows="2" />
                        <apex:dataTable value="{!ccWrappers}" var="aW">
                            <apex:column >
                                <apex:facet name="footer">
                                    <apex:commandButton action="{!addCC}" value=" + Row" reRender="theSection" style="float:right;margin-right:10%;" />
                                </apex:facet>
                                <apex:inputField value="{!aW.theWrap.ContactId}" label="" />
                            </apex:column>
                            <!--<apex:column >
                                <apex:outputField value="{!aW.theWrap.Contact.Email}" label=""/>
                                </apex:column>-->
                        </apex:dataTable>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="BCC Addresses" />
                    <apex:pageBlockSectionItem >
                        <apex:inputTextArea value="{!freeFormBCC}" cols="80" rows="2" />
                        <apex:dataTable value="{!bccWrappers}" var="aW">
                            <apex:column >
                                <apex:facet name="footer">
                                    <apex:commandButton action="{!addBCC}" value=" + Row" reRender="theSection" style="float:right;margin-right:10%;" />
                                </apex:facet>
                                <apex:inputField value="{!aW.theWrap.ContactId}" label="" />
                            </apex:column>
                            <!--<apex:column >
                                <apex:outputField value="{!aW.theWrap.Contact.Email}" label=""/>
                                </apex:column>-->
                        </apex:dataTable>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSectionItem>
                <apex:inputText id="emailSubjectField" label="Email Subject" value="{!emailSubject}" size="80" />
            </apex:pageBlockSection>
            <textarea id="enhancedEditor"></textarea>
            <div style="display: none">
                <apex:inputHidden value="{!editorContent}" id="editorBody" />
            </div>
            <apex:pageBlockSection title="Add New Attachments" collapsible="false" columns="1">
                <apex:outputPanel id="newAttachments">
                    <input type="file" id="file-input" />
                    <input id="upload-button" type="button" value="Upload" onclick="uploadFile();"/>
                <!--<apex:actionFunction name="rerenderAttachments" reRender="attachments"/>-->
                <apex:actionFunction action="{!createNewFiles_Stage}" name="createNewFile" reRender="attachments">
                    <apex:param name="firstParam" assignTo="{!attchID}" value="" />
                </apex:actionFunction>
                </apex:outputPanel>
            </apex:pageBlockSection>
            <apex:inputHidden value="{!hasParentAttachments}" id="HPA"/>
            <apex:pageBlockSection id="attachments" title="Select Attachments" collapsible="false" columns="1">
                <apex:pageBlockTable value="{!attachmentsWrapper}" var="attach" rendered="{!hasParentAttachments}">
                    <apex:column headerValue="Include?">
                        <apex:inputCheckbox value="{!attach.include}" />
                    </apex:column>
                    <apex:column value="{!attach.theAttachment.Name}" />
                    <apex:column value="{!attach.theAttachment.ContentType}" />
                    <apex:column value="{!attach.theAttachment.Description}" />
                    <apex:column value="{!attach.theAttachment.CreatedDate}" />
                    <apex:column >
                        <apex:image rendered="{!CONTAINS(attach.theAttachment.ContentType,'image')}" value="/servlet/servlet.FileDownload?file={!attach.theAttachment.Id}" style="max-width:200px;max-height:200px" />
                    </apex:column>
                </apex:pageBlockTable>
                <apex:outputText rendered="{!NOT(hasParentAttachments)}"><b>The Parent Case has no Attachments</b></apex:outputText>
            </apex:pageBlockSection>           
        </apex:pageBlock>
    </apex:form>
</apex:page>