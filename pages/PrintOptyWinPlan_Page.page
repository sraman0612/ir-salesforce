<!--
  -
  - $Author: Sambit Nayak(Cognizant)
  - $Description:This page is used to print OpptyWinPlan_Page after clicking print button.
  
  -->

<apex:page standardcontroller="Opportunity" extensions="OpptyWinPlan_Page_Extension" sidebar="false" showheader="false" standardStyleSheets="false" applyHtmlTag="false">
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
        <style>
        /*@media print {
            body {
                background-color: #E6E7E9 !important;
                -webkit-print-color-adjust: exact; 
            }
        }*/
        
        @page {
            /* Page spaces */
            margin: 20px 30px 60px 30px ; 
            padding: 0px 0px 0px 0px ;
            /* Landscape orientation */ 
            size: A3 landscape;
            /* Footer content */         
            @bottom-left {content: "Text for left"; font-family: Arial Unicode MS; font-size:8pt;}
            @bottom-center {content: "Text for center"; font-family: Arial Unicode MS; font-size:8pt;}
            @bottom-right {content: "Page " counter(page) " of " counter(pages); font-family: Arial Unicode MS; font-size:8pt;}
        }
        body {
            font-family: Arial Unicode MS;
            font-size:9pt;
        }
        </style>
    <div align="center">
    <button type="button" Class="btn btn-primary" onclick="window.close();">Close Tab</button>
    <br/>
    (Please Close this Tab after Print/Cancel)
    </div>
    <title>Opportunity Win Plan</title>
    <!-- Bootstrap -->
    <apex:stylesheet value="{!URLFOR($Resource.Html1,'html/css/bootstrap.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Html1,'html/css/bootstrap-datepicker3.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Html1,'html/css/bootstrap-datetimepicker.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Html1,'html/css/jquery-ui.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.Html1,'html/css/style.css')}"/>
     
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/jquery-1.12.4.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/bootstrap-datepicker.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/bootstrap-datetimepicker.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/jquery-ui.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.Html1,'html/js/script.js')}"/>
    
     <script language="javascript">
        function init()
        {
               window.print();
        }
     </script>
    
</head>
<body onload="init()">
    <div class="container-fluid padding-bottom100 grey-bg">
        <apex:form styleClass="opp-win-form">
        <div class="row">
                <div class="col-xs-12 col-sm-7 columns">
                    <h1 class="main-head">Opportunity Win Plan</h1><apex:outputText rendered="{!oppty.isNew__c}"><h2 style="font-size:15px; font-weight:bold;">Last Modified By: {!oppty.Last_Modified_User__c}<br/>
                    Last Modified Date/Time: {!oppty.Last_Modifed_Date_Time__c}</h2>
                    </apex:outputText>
                </div>
         </div>
            <div class="opp-win-section blue-bg">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 columns">
                        <div class="form-group">
                            <label for="customer-n" class="quest-head white-font">Account Name</label>
                            <apex:outputText id="customer-n" styleClass="form-control" value="{!customer}"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 columns">
                        <div class="form-group">
                        <label for="opportunity-n" class="quest-head white-font">Opportunity Name</label>
                            <apex:outputText id="opportunity-n" styleClass="form-control" value="{!opptyName}"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 columns">
                        <div class="form-group">
                            <label for="opp-poc" class="quest-head white-font">Opportunity Point Of Contact</label>
                            <apex:outputText id="opp-poc" styleClass="form-control" value="{!opptyPOC}"/>
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-6 columns">
                        <div class="form-group">
                            <label for="op-overview" class="quest-head white-font">Overview of the Opportunity<span class="white-sub-head" style="color: #fff;line-height: 15px;">(Describe what the buyer is trying to accomplish, their relevant business drivers, key initiatives, etc..)</span></label>
                            <apex:outputField id="op-overview" value="{!oppty.Overview_of_the_Opportunity__c}" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 columns">
                        <div class="form-group">
                            <label for="opp-custReason" class="quest-head white-font">Account Reason For Taking Action<span class="white-sub-head" style="color: #fff;line-height: 15px;">(Describe the time-bound event or date that is forcing the buyer to make a change or take an action.)</span></label>
                            <apex:outputField id="opp-custReason" value="{!oppty.Customer_Reason_for_Taking_Action__c}"/>
                            <span class="pull-right max-char white-font">1000 Characters</span>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="sub-head">Opportunity Qualification</h2>
            <div class="panel-group accordion bottom-page">
                <div class="panel panel-default">
                    <div class="panel-heading" >
                        <h4 class="panel-title">
                            <a class="text-none">
                        {!$ObjectType.Is_the_opp_real__c.Label}
                    </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in">
                        <div class="panel-body">
                            <apex:repeat value="{!questionList}" var="q">
                                <div class="query">
                                    <div class="col-xs-12 col-md-6 columns acc-query">
                                        <div class="col-xs-12 col-sm-4 columns">
                                            <h3 class="quest-head"><apex:outputText value="{!q.question}"/></h3>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 columns">
                                            <div class="form-group">
                                                <label for="cust-act-evidence" class="inline-label">{!$ObjectType.Is_the_opp_real__c.fields.Evidence__c.Label}</label>
                                                <apex:outputtext value="{!q.evidence}"/>
                                                <span class="pull-right max-char">250 Characters</span>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-xs-12 col-sm-6 columns">
                                                    <label for="cust-act-date" class="inline-label">Date of Evidence</label>
                                                        <apex:outputText value="{!q.dates}" styleClass="form-control" id="cust-act-date" html-placeholder="MM/DD/YYYY"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <!--<div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="text-none">
                        {!$ObjectType.Do_We_Have_a_Sol__c.Label}
                    </a>
                        </h4>
                    </div>-->
                    <div class="panel-collapse collapse in">
                        <div class="panel-body">
                            <apex:repeat value="{!questionListDS}" var="qDS">
                                <div class="query">
                                    <div class="col-xs-12 col-md-6 columns acc-query">
                                            <div class="col-xs-12 col-sm-4 columns">
                                                <h3 class="quest-head"><apex:outputText value="{!qDS.question}"/></h3>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 columns">
                                                <!--commented by CG as part of descoped object-->
                                                <!--<div class="form-group">
                                                    <label for="customer-business-evidence" class="inline-label">{!$ObjectType.Do_We_Have_a_Sol__c.fields.Evidence__c.Label}</label>
                                                    <apex:outputtext value="{!qDS.evidence}"/>
                                                    <span class="pull-right max-char">250 Characters</span>
                                                </div>-->
                                                <div class="row">
                                                    <div class="form-group col-xs-12 col-sm-6 columns">
                                                        <label for="customer-business-date" class="inline-label">Date of Evidence</label>
                                                                <apex:outputText value="{!qDS.dates}" styleClass="form-control" id="customer-business-date" html-placeholder="MM/DD/YYYY"/>                                                           
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </div>
                </div>
                <!--commented by CG as part of descoped object-->
                <!--<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="text-none">
                        {!$ObjectType.Can_We_Win_the_Opp__c.Label}
                    </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in">
                        <div class="panel-body">
                            <apex:repeat value="{!questionListCO}" var="qCO">
                                <div class="query">
                                    <div class="col-xs-12 col-md-6 columns acc-query">
                                            <div class="col-xs-12 col-sm-4 columns">
                                                <h3 class="quest-head"><apex:outputText value="{!qCO.question}"/></h3>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 columns">
                                                <div class="form-group">
                                                    <label for="personal-win-evidence" class="inline-label">{!$ObjectType.Can_We_Win_the_Opp__c.fields.Evidence__c.Label}</label>
                                                    <apex:outputtext value="{!qCO.evidence}"/>
                                                    <span class="pull-right max-char">250 Characters</span>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-xs-12 col-sm-6 columns">
                                                        <label for="personal-win-date" class="inline-label">Date of Evidence</label>
                                                                <apex:outputText value="{!qCO.dates}" styleClass="form-control" id="personal-win-date" html-placeholder="MM/DD/YYYY"/>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </div>
                </div>-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="text-none">
                        {!$ObjectType.Is_the_Value_There__c.Label}
                    </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in">
                        <div class="panel-body">
                            <apex:repeat value="{!questionListIV}" var="qIV">
                                <div class="query">
                                    <div class="col-xs-12 col-md-6 columns acc-query">
                                            <div class="col-xs-12 col-sm-4 columns">
                                                <h3 class="quest-head"><apex:outputText value="{!qIV.question}"/></h3>
                                            </div>
                                            <div class="col-xs-12 col-sm-8 columns">
                                                <div class="form-group">
                                                    <label for="ir-profit-evidence" class="inline-label">{!$ObjectType.Is_the_Value_There__c.fields.Evidence__c.Label}</label>
                                                    <apex:outputtext value="{!qIV.evidence}"/>
                                                    <span class="pull-right max-char">250 Characters</span>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-xs-12 col-sm-6 columns">
                                                        <label for="ir-profit-date" class="inline-label">Date of Evidence</label>
                                                                    <apex:outputText value="{!qIV.dates}" styleClass="form-control" id="ir-profit-date" html-placeholder="MM/DD/YYYY"/>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </apex:repeat>
                        </div>
                    </div>
                </div>
            </div>
            <!--commented by CG as part of descoped object-->
        <!--<div class="politics-relation-section table-section">
            <h2 class="sub-head pull-left">Politics / Relationships</h2>
            <apex:outputPanel id="PolAndRelPanel">
            <apex:variable value="{!0}" var="rowNum"/>
            <table class="table table-striped table-bordered" id="politics-table">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Vendor Preference</th>
                        <th>Type of Buyer</th>
                        <th>Buyer Location</th>
                    </tr>
                </thead>
                <apex:repeat value="{!lstInner}" var="prRecord">
                <tbody>
                    <tr>
                        <td data-th="Role">
                            <div class="form-group">
                            <apex:selectList id="role" value="{!prRecord.polAndRelObj.Role__c}" multiselect="false" size="1" styleClass="form-control">
                              <apex:selectOptions value="{!Roles}"/>
                            </apex:selectList>
                            </div>
                        </td>
                        <td data-th="Name">
                            <div class="form-group">
                                <apex:outputText value="{!prRecord.polAndRelObj.Name__c}" styleClass="form-control"/>
                            </div>
                        </td>
                        <td data-th="Title">
                            <div class="form-group">
                                <apex:outputText value="{!prRecord.polAndRelObj.Title__c}" styleClass="form-control"/>
                            </div>
                        </td>
                        <td data-th="Vendor Preference">
                            <div class="form-group">
                            <apex:selectList id="vendorPreference" value="{!prRecord.polAndRelObj.Vendor_Preference__c}" multiselect="false" size="1" styleClass="form-control">
                              <apex:selectOptions value="{!VendorPreferences}"/>
                            </apex:selectList>
                            </div>
                        </td>
                        <td data-th="Type of Buyer">
                            <div class="form-group">
                            <apex:selectList id="typeOfBuyer" value="{!prRecord.polAndRelObj.Type_of_Buyer__c}" multiselect="false" size="1" styleClass="form-control">
                              <apex:selectOptions value="{!TypeOfBuyer}"/>
                            </apex:selectList>
                            </div>
                        </td>
                        <td data-th="Buyer Location">
                            <div class="form-group">
                            <apex:selectList id="buyerLocation" value="{!prRecord.polAndRelObj.Buyer_Location__c}" multiselect="false" size="1" styleClass="form-control">
                              <apex:selectOptions value="{!BuyerLocation}"/>
                            </apex:selectList>
                            </div>
                        </td>
                    </tr>
                </tbody>
              </apex:repeat>
            </table>
            </apex:outputPanel>
        </div>-->
            <h2 class="sub-head">Competitive Sales Strategy </h2>
            <hr/>
            <h3 class="blue-head">Sales Strategy Examples</h3>
            <ul class="blue-bullet-list">
                <li><span>IR goes head-to-head to compete with a competitor based on superior features and functions of</span></li>
                <li><span>IR differentiates its solution via new or extended requirements and focus, e.g., energy savings, cost of ownership, turnkey, reliability, service, etc.</span></li>
                <li><span>IR decides to pursue only part of an opportunity such as just equipment or services in order to initially penetrate the account and expand the relationship at some later date</span></li>
            </ul>
            <div class="form-group">
                <apex:outputtext value="{!oppty.IR_s_Sales_Strategy_Approach__c}"/>
                <span class="pull-right max-char">1000 Characters</span>
            </div>
        <div class="plain-textarea">
            <h2 class="sub-head">IR Solution Details </h2>
            <div class="form-group">
                <apex:outputtext value="{!oppty.IR_Solution_Details__c}"/>
                <span class="pull-right max-char">1000 Characters</span>
            </div>
        </div>
          <!--commented by CG as part of descoped object-->
        <!--<div class="buying-decision-section table-section">
    <h2 class="sub-head pull-left">Buying / Decision</h2>
        <apex:outputPanel id="BuyAndDecPanel">
            <apex:variable value="{!0}" var="rowNum"/>
        <table class="table table-striped table-bordered" id="buying-table">
            <thead>
                <tr>
                    <th>Buyer Decision Milestone</th>
                    <th>Estimated Date of Decision</th>
                </tr>
            </thead>
            <apex:repeat value="{!lstBuyDecProcessInner}" var="bdP">
            <tbody>
                <tr>
                    <td data-th="Buyer Decision Milestone">
                        <div class="form-group">
                            <apex:outputText value="{!bdP.buyDecProcessObj.Buyer_Decision_Milestone__c}" styleClass="form-control"/>
                        </div>
                    </td>
                    <td data-th="Estimated Date of Decision">
                        <div class="form-group">
                                <apex:outputText value="{!bdP.buyDecProcessObj.Estimated_Date_of_Decision__c}" styleClass="form-control" id="buydecProcessDate" html-placeholder="MM/DD/YYYY"/>
                        </div>
                    </td>
                </tr>
            </tbody>
            </apex:repeat>
        </table>
        </apex:outputPanel>
        </div>-->

    <apex:repeat value="{!compAnalysisList}" var="ca">
        <div class="panel-group accordion" id="accordion3" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <!--<div class="panel-heading" role="tab" id="heading10">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse10" aria-expanded="true" aria-controls="collapse10" class="text-none">
                        {!$ObjectType.Competitive_Anaysis__c.fields.Parity__c.Label}
                    </a>
                    </h4>
                </div>-->
                <div id="collapse10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading10">
                    <div class="panel-body">
                        <div class="form-group text-section">
                            <label for="parity" class="acc-label">{!parityHelpText}</label>
                            <apex:outputtext value="{!ca.parity}"/>
                            <span class="pull-right max-char">1000 Characters</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <!--commented by CG as part of descoped object-->
                <!--<div class="panel-heading" role="tab" id="heading11">
                    <h4 class="panel-title">
                        <a class="collapsed text-none" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                        {!$ObjectType.Competitive_Anaysis__c.fields.Contention__c.Label}
                    </a>
                    </h4>
                </div>-->
                <div id="collapse11" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading11">
                    <div class="panel-body">
                        <div class="form-group text-section">
                            <label for="contention" class="acc-label">{!contentionHelpText}</label>
                            <apex:outputtext value="{!ca.contention}"/>
                            <span class="pull-right max-char">1000 Characters</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <!--commented by CG as part of descoped object-->
                <!--<div class="panel-heading" role="tab" id="heading12">
                    <h4 class="panel-title">
                        <a class="collapsed text-none" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                        {!$ObjectType.Competitive_Anaysis__c.fields.Differentiation__c.Label}
                    </a>
                    </h4>
                </div>-->
                <div id="collapse12" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading12">
                    <div class="panel-body">
                        <div class="form-group text-section">
                            <label for="differentiation" class="acc-label">{!differentiationHelpText}</label>
                            <apex:outputtext value="{!ca.differentiation}"/>
                            <span class="pull-right max-char">1000 Characters</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </apex:repeat>

        <apex:outputPanel id="PriEvalCriteriaPanel">
        <apex:variable value="{!0}" var="rowNum"/>
            <!--commented by CG as part of descoped object-->
            <!--<div class="primary-evaluation-section table-section">
                <h2 class="sub-head pull-left">Primary Evaluation Criteria </h2>
                <table class="table table-striped table-bordered" id="primary-eval-table">
                    <thead>
                        <tr>
                            <th>Comments</th>
                        </tr>
                    </thead>
                    <apex:repeat value="{!lstPriEvalCriteriaInner}" var="pk">
                    <tbody>
                        <tr>
                            <td data-th="Comments">
                                <div class="form-group">
                                    <apex:outputtext value="{!pk.priEvalCriteriaObj.Primary_Evaluation_Criteria_Name__c}" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    </apex:repeat>
                </table>
            </div>-->
    </apex:outputPanel>

        <apex:outputPanel id="keyWinThemesPanel">
        <apex:variable value="{!0}" var="rowNum"/>
        <div class="key-win-section table-section">
            <h2 class="sub-head pull-left">Key Win Themes </h2>
            <table class="table table-striped table-bordered" id="key-win-table">
                <thead>
                    <tr>
                        <th>Comments</th>
                    </tr>
                </thead>
                <apex:repeat value="{!lstKeyWinThemeInner}" var="kw">
                <tbody>
                    <tr>
                        <td data-th="Comments">
                            <div class="form-group">
                                <apex:outputtext value="{!kw.keyWinThemesObj.Key_Win_Themes_Name__c}" />
                            </div>
                        </td>
                    </tr>
                </tbody>
                </apex:repeat>
            </table>
        </div>
</apex:outputPanel>

<apex:outputPanel id="ActAndTacPanel">
        <apex:variable value="{!0}" var="rowNum"/>
    	<!--commented by CG as part of descoped object-->
        <!--<div class="tactics-advance-section table-section">
            <h2 class="sub-head pull-left">Actions / Tactics to Advance Opportunity </h2>
            <table class="table table-striped table-bordered" id="tactics-advance-table">
                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Resources Needed</th>
                        <th>Due Date</th>
                        <th>Follow-up Plan</th>
                    </tr>
                </thead>
                <apex:repeat value="{!lstActAndTacInner}" var="at">
                <tbody>
                    <tr>
                        <td data-th="Owner">
                            <div class="form-group">
                                    <apex:outputText styleClass="form-control" id="owner" value="{!at.actAndTacObj.Owner__c}"/>
                            </div>
                        </td>
                        <td data-th="Resources Needed">
                            <div class="form-group">
                                <apex:outputText styleClass="form-control" value="{!at.actAndTacObj.Resources_Needed__c}"/>
                            </div>
                        </td>
                        <td data-th="Due Date">
                            <div class="form-group">
                                <div class="input-group date datepick">
                                    <apex:inputText value="{!at.actAndTacObj.Due_Date__c}" styleClass="form-control" id="due-date" html-placeholder="MM/DD/YYYY"/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        </td>
                        <td data-th="Follow-up Plan">
                            <div class="form-group">
                                <apex:outputText styleClass="form-control" id="followup-plan" value="{!at.actAndTacObj.Follow_up_Plan__c}"/>
                            </div>
                        </td>
                    </tr>
                </tbody>
                </apex:repeat>
            </table>
        </div>-->
</apex:outputPanel>

        <div class="white-content">
             <div class="row">
                 <div class="attendees-section">
                        <div class="form-group col-xs-12 col-sm-8 columns">
                        <label for="sales-feedback" class="acc-label" style="font-weight:bold;">Sales Manager / Leader Feedback</label>
                        <apex:outputtext value="{!oppty.Sales_Manager_Leader_Feedback__c}" id="sales-feedback"/>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 columns">
                        <label for="feedback-date-time" class="acc-label" style="font-weight:bold;">Feedback Date/Time</label>
                            <apex:outputText styleClass="form-control" id="feedback-date-time" value="{!oppty.Feedback_Date_Time__c}"/>
                 </div>
             </div>
             </div>
        </div>
        </apex:form>
    </div>
</body>

</html>

</apex:page>