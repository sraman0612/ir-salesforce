<apex:page showHeader="true" sidebar="true" standardController="cafsl__Oracle_Quote__c"
            extensions="cafsl.MarkAsActiveCtrl" recordSetVar="oracleQuotes" action="{!autoSync}">
    <apex:stylesheet value="{!URLFOR($Resource.oafsl__oafsl, 'assets/styles/salesforce-lightning-design-system-vf.css')}"/>
    <apex:form id="theForm"
               rendered="{!!isMobile}">
        <apex:pageBlock id="theBlock">
            <apex:pageMessages id="pgMsgs"/>
            <apex:commandLink value="<< Return to Opportunity." action="{!cancel}" id="back"/>
        </apex:pageBlock>
    </apex:form>
    <apex:form id="mobileForm"
               rendered="{!isMobile}">
        <!-- Function used to update the Syncing__c flag of all Oracle Quotes in the current view -->
        <apex:actionFunction name="updateChildSelection" action="{!updateChildSelection}" rerender="none" >
            <apex:param name="selQt" assignTo="{!selectedQuoteId}" value="" />
        </apex:actionFunction>
        <!-- meta tag is a workaround for iOS devices to use the vh/vw CSS attributes -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style type="text/css" rel="stylesheet">
            .ola {
                width: 100%;            /* Let's be honest, you don't need an explanation for this */
                height: 100vh;          /* This, in combination with the "meta" viewport tag, set ola to the viewport height */
            }
            .quotes-list {
                padding-bottom: 12vh;   /*This gives the buttons space to breathe */
                z-index: 1;             /* Allows button group to float over list */
            }
            .buttons {
                background-color: #fff; /* Color to make the buttons more pleasing to look at */
                height: 10vh;           /* Height for buttons */
                position: fixed;        /* Holds these jerks in place on the viewport */
                top: 90vh;              /* Places the buttons at the bottom of the viewport */
                left: 0;                /* Prevents wrapper from pushing buttons over */
                width: inherit;         /* Makes the button strip stretch across the viewport */
                vertical-align: center;
                z-index: 100;           /* Lets the button group float over everything */
            }
            .clear-checkbox::after {
                display: none !important;   /*JUSTIFICATION: This is to override a UI issue, in which previously selected quote appears 'selected' even when unchecked */
            }
        </style>
        <div class="ola">
            <div class="quotes-list">
                <table class="slds-table slds-table_bordered slds-table_cell-buffer slds-max-medium-table--stacked-horizontal" >
                    <thead>
                        <tr class="slds-text-title_caps">
                            <th scope="col">
                                <div class="slds-truncate"
                                     title="{!$ObjectType.Oracle_Quote__c.Fields.Syncing__c.Label}">{!$ObjectType.Oracle_Quote__c.Fields.Syncing__c.Label}</div>
                            </th>
                            <th scope="col">
                                <div class="slds-truncate"
                                     title="{!$ObjectType.Oracle_Quote__c.Fields.Name.Label}">{!$ObjectType.Oracle_Quote__c.Fields.Name.Label}</div>
                            </th>
                            <th scope="col">
                                <div class="slds-truncate"
                                     title="Id">Id</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Loop to display children of the record that was used to navigate to this page -->
                        <apex:repeat id="oracleQuotesTable"
                                     value="{!childQuotes}"
                                     var="quote" >
                            <tr>
                                <!-- LDS Checkbox For Syncing__c -->
                                <td data-label="{!$ObjectType.Oracle_Quote__c.Fields.Syncing__c.Label}">
                                    <div class="slds-form-element">
                                        <div class="slds-form-element__control">
                                            <span class="slds-checkbox">
                                                <input class="sync_checkboxes"
                                                       data-id="{!quote.Id}"
                                                       id="{!'checkbox-' + quote.Id}"
                                                       onclick="updateCheckboxes(this)"
                                                       name="{!$ObjectType.Oracle_Quote__c.Fields.Syncing__c.Label}"
                                                       type="checkbox"
                                                       value="{!quote.Syncing__c}" />
                                                <label class="slds-checkbox__label" for="{!'checkbox-' + quote.Id}">
                                                    <span class="slds-checkbox_faux"></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <!-- LDS Output Text For Quote Name -->
                                <td data-label="{!$ObjectType.Oracle_Quote__c.Fields.Name.Label}">
                                    <div class="slds-truncate" title="{!$ObjectType.Oracle_Quote__c.Fields.Name.Label}">
                                        <apex:outputText value="{!quote.Name}" />
                                    </div>
                                </td>
                                <!-- Navigable For ID -->
                                <td data-label="Id">
                                    <div class="slds-truncate" title="Id">
                                        <apex:outputLink value="{!'/' + quote.Id}" >{!quote.Id}</apex:outputLink>
                                    </div>
                                </td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            <footer class="slds-button-group slds-align_absolute-center buttons"
                 role="group">
                <apex:commandButton action="{!updateSync}"
                                    styleClass="slds-button slds-button_neutral"
                                    value="Sync" />
                <apex:commandButton action="{!cancel}"
                                    styleClass="slds-button slds-button_neutral"
                                    value="Cancel" />
            </footer>
        </div>
        <script type="text/javascript">
        /** BUG FOR WIPING CHECKED AND RE-RUNNING CSS SELECTOR add class to faux's for finding and toggle class **/
            // This function serves to update the UI, and server values (no rerender blink)
            //  This also prevents the user from being able to select multiple quotes
            function updateCheckboxes(checkbox) {
                var selectElems = document.getElementsByClassName("sync_checkboxes");
                var cbElems = document.getElementsByClassName("slds-checkbox_faux");
                var srcElem = event.srcElement;
                var selectedId = '';
                /* LOOP : Add the checked attr and true value to the user-selected syncing quote
                *          and remove the checked and true value from all other quotes
                */
                for(var i=0; i<selectElems.length; i++) {
                    var selectElem = selectElems[i];
                    var cbElem = cbElems[i];
                    if(srcElem.dataset.id === selectElem.dataset.id) {
                        selectElem.setAttribute("checked","checked");
                        selectElem.value = true;
                        selectedId = selectElem.dataset.id;
                        cbElem.classList.toggle("clear-checkbox",false);
                    } else {
                        selectElem.removeAttribute("checked");
                        selectElem.value = false;
                        cbElem.classList.toggle("clear-checkbox",true);
                    }
                }
                updateChildSelection(selectedId);
            }

        </script>
        <script type="text/javascript" defer="defer">
            //  Upon user entry, this code sets the checkbox for the currently sync'd quote
            var initialList = document.getElementsByClassName("sync_checkboxes");   //  Gathers all checkbox elements
            //  Loop over the list of quotes, and set the "checked" attribute of the actively sync'd quote
            for(var i=0; i<initialList.length; i++) {
                //  The value is read by the js as the string representation of true|false
                if(initialList[i].value === "true") {
                    initialList[i].setAttribute("checked","checked");
                }
            }
        </script>
    </apex:form>
</apex:page>