<apex:page lightningStylesheets="true" showHeader="true" sidebar="false" StandardController="Campaign" extensions="GMBLBC.CampaignAttendeesScannerController">
    <apex:slds />

    <apex:includeScript value="{!$Resource.GMBLBC__gsrSpinner}"/>

    <apex:remoteObjects >
        <apex:remoteObjectModel name="CampaignMember" fields="Id,Status,ContactId,LeadId"/>
    </apex:remoteObjects>

    <apex:form >
        <apex:actionFunction name="rerenderAttendees" action="{!refreshAttendees}" rerender="registered,responded" oncomplete="gSpinner.hide();" />
    </apex:form>

    <br/>
    <br/>

    <div style="text-align:center;">
        <button class="slds-button slds-button_neutral" onclick="window.open('/{!Id}', '_top')">Back to Campaign</button>
        &nbsp;&nbsp;
        <button class="slds-button slds-button_neutral" onclick="printLabels();">Print All Badges</button>
        <br/>
        <br/>
        <span style="font-weight:bold;">Campaign:</span>&nbsp;&nbsp;<apex:outputField value="{!Campaign.Name}"/>
    </div>
    <br/>

    <div style="margin: 0 20px">
        <apex:pageBlock title="Scan Attendee">
            <c:barcodeTextAction callback="barcodeScan" barcodeInputElementId="barcodeInput" autofocus="true" />
            <div class="slds-form-element slds-has-error slds-hide" id="barcode-image-error-container">
                <div class="slds-form-element__help" id="barcode-image-error-message"></div>
            </div>
        </apex:pageBlock>
    </div>

    <table style="border-spacing: 20px; border-collapse: separate;">
        <tr>
            <td width="50%" valign="top">
                <apex:outputPanel id="registered">
                    <apex:pageBlock title="Planned/Sent/Received: {!registered.size}">
                        <apex:pageBlockTable value="{!registered}" var="r">
                            <apex:column style="padding-left: 5px;" headerValue="Record">
                                <apex:outputLink value="/{!r.Id}">{!r.FirstName + ' ' + r.LastName}</apex:outputLink>
                            </apex:column>
                            <apex:column style="padding-left: 5px;" headerValue="Contact/Lead">
                                <apex:outputLink value="/{!IF(r.ContactId != null, r.ContactId, r.LeadId)}">{!IF(r.ContactId != null, 'Contact', 'Lead')}</apex:outputLink>
                            </apex:column>
                            <apex:column style="padding-left: 5px;" value="{!r.Contact.AccountId}"/>
                            <apex:column style="padding-left: 5px;" value="{!r.Status}"/>
                        </apex:pageBlockTable>
                    </apex:pageBlock>
                </apex:outputPanel>
            </td>
            <td width="50%" valign="top">
                <apex:outputPanel id="responded">
                    <apex:pageBlock title="Responded: {!responded.size}">
                        <apex:pageBlockTable value="{!responded}" var="r">
                            <apex:column style="padding-left: 5px;" headerValue="Record">
                                <apex:outputLink value="/{!r.Id}">{!r.FirstName + ' ' + r.LastName}</apex:outputLink>
                            </apex:column>
                            <apex:column style="padding-left: 5px;" headerValue="Contact/Lead">
                                <apex:outputLink value="/{!IF(r.ContactId != null, r.ContactId, r.LeadId)}">{!IF(r.ContactId != null, 'Contact', 'Lead')}</apex:outputLink>
                            </apex:column>
                            <apex:column style="padding-left: 5px;" value="{!r.Contact.AccountId}"/>
                        </apex:pageBlockTable>
                    </apex:pageBlock>
                </apex:outputPanel>
            </td>
        </tr>
    </table>

    <script type="text/javascript">

        window.addEventListener("load", function load(event) {
            document.getElementById('barcodeInput').focus();
        });

        function showError(error)
        {
            var errorMessage = document.getElementById('barcode-image-error-message');
            var errorContainer = document.getElementById('barcode-image-error-container');

            errorMessage.innerHTML = error;
            errorContainer.classList.remove("slds-hide");

            gSpinner.hide();

            return false;
        }

        function hideError()
        {
            var errorMessage = document.getElementById('barcode-image-error-message');
            var errorContainer = document.getElementById('barcode-image-error-container');

            errorMessage.innerHTML = '';
            errorContainer.classList.add("slds-hide");
        }


        function barcodeScan(event)
        {
            hideError();

            var barcode = event.barcode;

            var o = new SObjectModel.CampaignMember();

            gSpinner.show();

            o.retrieve({where: {
                    or: {
                        ContactId: {eq: barcode},
                        LeadId: {eq: barcode}
                    }
                }
            },
            function(error,results,event)
            {
                if (error) {
                    return showError('Error identifying Campaign Member: ' + error.message);
                }
                else if (results.length < 1)
                {
                    return showError('A Contact having Id [' + barcode + '] was not found in this Campaign.');
                }
                else if(results[0]._props.Status && results[0]._props.Status === 'Responded')
                {
                    return showError('Campaign Member has previously been scanned in.');
                }
                else
                {
                    updateCampaignMember(results[0]._props.Id);
                }
            });
        }

        function updateCampaignMember(Id) {
            var oupdate = new SObjectModel.CampaignMember({
                Id: Id,
                Status: 'Responded'
            });
            oupdate.update(rerenderAll);
        }

        function rerenderAll()
        {
            rerenderAttendees();
        }

        function printLabels()
        {
            window.open('/apex/pages/barcodePrint_CampaignMembers?Id={!CampaignId}','_blank');
        }

    </script>
</apex:page>