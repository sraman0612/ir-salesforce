<apex:page standardController="User" extensions="cafsl.OracleUsersCtrl">
    <style>
        select.cafsl-groups-multipick {
            width:12rem !important;
        }

		select.cafsl-groups-multipick optgroup option::before {
			   content:"" !important;
	    }
	</style>
    <apex:form >
        <apex:pageBlock rendered="{!usersWithSites.size > 0}" id="thePage">
            <apex:pageMessages />
            <apex:pageBlockButtons >
                <apex:commandButton value="Edit" action="{!edit}" rendered="{!!editMode}"/>
                <apex:commandButton value="Save" action="{!save}" rendered="{!editMode}"/>
                <apex:commandButton value="Cancel" action="{!cancel}" rendered="{!editMode}"/>
            </apex:pageBlockButtons>

            <apex:pageBlockSection collapsible="true" showHeader="true" columns="1" title="{!'Default Profile for '+User.Username}" id="defaultProfileSection">

                <apex:pageBlockSectionItem rendered="{!editMode}">
                    <apex:outputLabel value="{!$ObjectType.User.fields.cafsl__Default_Profile__c.label}"/>
                    <apex:selectList value="{!User.cafsl__Default_Profile__c}" size="1">
                        <apex:selectOption itemValue="" itemLabel="--None--"/>
                        <apex:selectOptions value="{!userProfileOpts}"/>
                        <apex:actionSupport event="onchange" action="{!updateDefaultProfile}" reRender="thePage"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{!!editMode}">
                    <apex:outputLabel value="{!$ObjectType.User.fields.cafsl__Default_Profile__c.label}"/>
                    <apex:outputText value="{!userProfileOptsMap[User.cafsl__Default_Profile__c]}" rendered="{!User.cafsl__Default_Profile__c != null && User.cafsl__Default_Profile__c != ''}"/>
                </apex:pageBlockSectionItem>

            </apex:pageBlockSection>
            <apex:repeat value="{!usersWithSites}" var="uws">
                <apex:pageBlockSection collapsible="true" showHeader="true" columns="2" title="{!uws.sectionTitle}" rendered="{!editMode}" id="CurrentSection">

                    <apex:inputField value="{!uws.ou.cafsl__Oracle_CPQ_Cloud_Login__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:inputField value="{!uws.ou.cafsl__Language_Preference__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__User_Type__c.label}"/>
                        <apex:selectList value="{!uws.ou.cafsl__User_Type__c}" size="1">
                            <apex:selectOption itemValue="" itemLabel="--None--"/>
                            <apex:selectOptions value="{!uws.accessTypes}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:inputField value="{!uws.ou.cafsl__Currency_Preference__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}" helpText="{!HELP_TEXT_MAP['Partner_User__c']}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Partner_User__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Partner_User__c}">
                            <apex:actionSupport event="onchange" reRender="CurrentSection"/>
                        </apex:inputField>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Number_Format__c.label}"/>
                        <apex:selectList value="{!uws.ou.cafsl__Number_Format__c}" size="1">
                            <apex:selectOption itemValue="" itemLabel="--None--"/>
                            <apex:selectOptions value="{!numberFormats}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}" helpText="{!HELP_TEXT_MAP['Partner_Organization__c']}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Partner_Organization__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Partner_Organization__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Units__c.label}"/>
                        <apex:selectList value="{!uws.ou.cafsl__Units__c}" size="1">
                            <apex:selectOption itemValue="" itemLabel="--None--"/>
                            <apex:selectOptions value="{!unitSystems}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Link_to_CPQ_User__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Link_to_CPQ_User__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Link_to_CPQ_User__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Date_Time_Format__c.label}"/>
                        <apex:selectList value="{!uws.ou.cafsl__Date_Time_Format__c}" size="1">
                            <apex:selectOption itemValue="" itemLabel="--None--"/>
                            <apex:selectOptions value="{!dateTimeFormats}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Suspend_CPQ_User_Sync__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Suspend_CPQ_User_Sync__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Suspend_CPQ_User_Sync__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Time_Zone__c.label}"/>
                        <apex:selectList value="{!uws.ou.cafsl__Time_Zone__c}" size="1">
                            <apex:selectOption itemValue="" itemLabel="--None--"/>
                            <apex:selectOptions value="{!uws.timeZones}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Allow_Quote_Creation__c']}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Allow_Quote_Creation__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Allow_Quote_Creation__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:inputField value="{!uws.ou.cafsl__Oracle_User_Profile__c}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:actionSupport event="onchange" action="{!uws.setProfileDefaults}" reRender="CurrentSection"/>
                    </apex:inputField>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Delegated_Approver__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Delegated_Approver__c.label}"/>
                        <apex:inputField value="{!uws.ou.cafsl__Delegated_Approver__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem />

                    <apex:inputField styleClass="cafsl-groups-multipick" value="{!uws.ou.cafsl__Groups__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                </apex:pageBlockSection>

                <apex:pageBlockSection collapsible="true" showHeader="true" title="{!uws.sectionTitle}" rendered="{!!editMode}">

                    <apex:outputField value="{!uws.ou.cafsl__Oracle_CPQ_Cloud_Login__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:outputField value="{!uws.ou.cafsl__Language_Preference__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__User_Type__c.label}"/>
                        <apex:outputText value="{!uws.accessTypesMap[uws.ou.cafsl__User_Type__c]}" rendered="{!uws.ou.cafsl__User_Type__c != null}"/>
                    </apex:pageBlockSectionItem>

                    <apex:outputField value="{!uws.ou.cafsl__Currency_Preference__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Partner_User__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Partner_User__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Partner_User__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Number_Format__c.label}"/>
                        <apex:outputText value="{!numberFormatsMap[uws.ou.cafsl__Number_Format__c]}" rendered="{!uws.ou.cafsl__Number_Format__c != null}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Partner_Organization__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Partner_Organization__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Partner_Organization__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Units__c.label}"/>
                        <apex:outputText value="{!unitSystemsMap[uws.ou.cafsl__Units__c]}" rendered="{!uws.ou.cafsl__Units__c != null}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Link_to_CPQ_User__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Link_to_CPQ_User__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Link_to_CPQ_User__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Date_Time_Format__c.label}"/>
                        <apex:outputText value="{!dateTimeFormatsMap[uws.ou.cafsl__Date_Time_Format__c]}" rendered="{!uws.ou.cafsl__Date_Time_Format__c != null}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Suspend_CPQ_User_Sync__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Suspend_CPQ_User_Sync__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Suspend_CPQ_User_Sync__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Time_Zone__c.label}"/>
                        <apex:outputText value="{!timeZonesMap[uws.ou.cafsl__Time_Zone__c]}" rendered="{!uws.ou.cafsl__Time_Zone__c != null}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Allow_Quote_Creation__c']}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Allow_Quote_Creation__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Allow_Quote_Creation__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:outputField value="{!uws.ou.cafsl__Oracle_User_Profile__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                    <apex:pageBlockSectionItem helpText="{!HELP_TEXT_MAP['Delegated_Approver__c']}" rendered="{!uws.ecs.cafsl__Primary__c}">
                        <apex:outputLabel value="{!$ObjectType.cafsl__Oracle_User__c.fields.cafsl__Delegated_Approver__c.label}"/>
                        <apex:outputField value="{!uws.ou.cafsl__Delegated_Approver__c}"/>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem />

                    <apex:outputField value="{!uws.ou.cafsl__Groups__c}" rendered="{!uws.ecs.cafsl__Primary__c}"/>

                </apex:pageBlockSection>
            </apex:repeat>
        </apex:pageBlock>
    </apex:form>
</apex:page>