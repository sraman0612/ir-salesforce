import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import {FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

import welch_CommId from '@salesforce/label/c.welch_CommId';
import welch_address_tab_link from "@salesforce/label/c.Welch_Address_Tab_link";
import getPaymentInfo from '@salesforce/apex/welch_PaymentAdapter.getPaymentInfo';
import updatePaymentInfo from '@salesforce/apex/welch_PaymentAdapter.setPaymentInfo';

const SET_BILLING_ADDRESS = 'setbillingaddress';


export default class welch_PaymentMethod extends LightningElement 
{

    welchAddressTabLink = welch_address_tab_link;
    communityId = welch_CommId;
    @api
    cartId;
        
    @api
    commId;
    @api
    paymentType;

    @track
    cartRecord;
    _cardAuthErrorMessage;
    _addresses;
    _purchaseOrderNumber;

    _selectedPaymentType = 'CARDPAYMENT';
    paymentTypes= {
        poNumber: 'PONUMBER',
        cardPayment: 'CARDPAYMENT'
    };
    /**
     * Determines if Card Holder Name is a required field.
     * @type {Boolean}
     */
     @api cardHolderNameRequired = false;

     /**
     * Determines if Card Type is a required field.
     * @type {Boolean}
     */
    @api cardTypeRequired = false;
    
    /**
     * Determines if CVV is a required field.
     * @type {Boolean}
     */
     @api cvvRequired = false;
     /**
     * Determines if Expiry Month is a required field.
     * @type {Boolean}
     */
    @api expiryMonthRequired = false;
    /**
     * Determines if Expiry Year is a required field.
     * @type {Boolean}
     */
     @api expiryYearRequired = false;

    /**
     * Determines if PO Number is a required field.
     * @type {Boolean}
     */
         @api poRequired = false;

     /**
     * Determines if Card Holder Name is a hidden field.
     * @type {Boolean}
     */
    @api hideCardHolderName = false;

    /**
     * Determines if Card Type is a hidden field.
     * @type {Boolean}
     */
     @api hideCardType = false;
     /**
     * Determines if CVV is a hidden field.
     * @type {Boolean}
     */
    @api hideCvv = false;

    /**
     * Determines if Expiry Month is a hidden field.
     * @type {Boolean}
     */
    @api hideExpiryMonth = false;

    /**
     * Determines if Expiry Year is a hidden field.
     * @type {Boolean}
     */
    @api hideExpiryYear = false;

    /**
     * Determines if PO Number is a hidden field.
     * @type {Boolean}
     */
    @api hidePoNumber = false;

    @track
    hasData = false;
    @track
    showSpinner = true;

    connectedCallback()
    {
        this.getPaymentInfo();
    }

    getPaymentInfo()
    {
        if(this.cartId)
        {
            getPaymentInfo({ cartId: this.cartId, communityId: this.communityId })
                .then((data) => {
                    
                    this._purchaseOrderNumber = data.purchaseOrderNumber;
                    this._addresses = data.addresses;
                })
                .catch((error) => {
                    //do nothing, continue as normal
                    console.log(error.body.message);
            });
            
        }
        else
        {
            console.log('no cartId!');
        }
    }
    /**
     * The purchase order number. Used to pass the purchase order to the server and to
     * display the existing purchase order when the component loads.
     *
     * The value of this property is updated in response to user interactions with the control.
     *
     * @type {string}
     */
     @api
     get purchaseOrderNumber() {
         return this._purchaseOrderNumber;
     }
 
     set purchaseOrderNumber(newNumber) {
         this._purchaseOrderNumber = newNumber;
     }
    /**
     * The address data. Used to pass the user's addresses to the child billing address components.
     * @type {Address[]}
     */
     @api
     get addresses() {
         return this._addresses;
     }
 
     set addresses(billingAddresses) {
         this._addresses = billingAddresses;
     }
 
     /**
      * Gets or sets the selected billing address.
      * @type {String}
      */
     @api
     get selectedBillingAddress() {
         return this._selectedBillingAddress;
     }
 
     set selectedBillingAddress(address) {
         this._selectedBillingAddress = address;
     }
     /**
     * Determines if Billing Address is a hidden field.
     * @type {Boolean}
     */
    @api hideBillingAddress = false;
    /**
     * Determines if the billing address field should be marked required.
     * @type {Boolean}
     */
     @api billingAddressRequired = false;
     /**
     * Gets or sets the currently selected payment type.
     *
     * The value of this property is updated in response to user interactions with the control.
     *
     * @type {String}
     */
    @api
    get selectedPaymentType() {
        return this._selectedPaymentType;
    }

    set selectedPaymentType(newPaymentType) {
        this._selectedPaymentType = newPaymentType;
    }
    /**
     * Gets the payment types
     * PO Number or Card Payment
     
     get paymentTypes() {
        return {
            poNumber: 'PONUMBER',
            cardPayment: 'CARDPAYMENT'
        };
    }*/
    /**
     * Handler for the 'click' event fired from the payment type radio buttons.
     * @param {event} event - The selected payment type
     */
     handlePaymentTypeSelected(event) {
        this._selectedPaymentType = event.currentTarget.value;
    }
    /**
     * Get state of selected payment type
     * @return {boolean} true if selected payment type is PO number
     */
     
    get isPoNumberSelected2() {
        return this.selectedPaymentType === 'PONUMBER'
    }
    /**
     * Get state of selected payment type
     * @return {boolean} true if selected payment type is Card Payment
     */
     get isCardPaymentSelected2() {
        return (
            this.selectedPaymentType === 'CARDPAYMENT'
        );
    }
     /**
     * Gets or sets error messages received from card authorization.
     * The value of this property is updated in response to user interactions with the control.
     * @type {String}
     */
    @api
    get cardAuthErrorMessage() {
        return this._cardAuthErrorMessage;
    }

    set cardAuthErrorMessage(newMessage) {
        this._cardAuthErrorMessage = newMessage;
    }

    

    /**
     * Sets the cartId of the current webCart.
     * @param {String} cartId
     
    set cartId(cartId) {
        this._cartId = cartId;
        if (cartId) {
            this.initializePaymentData(cartId);
        }
    }*/

    /**
     * Gets the payment types
     * PO Number or Card Payment
     */
    get paymentTypes() {
        return {
            poNumber: 'PONUMBER',
            cardPayment: 'CARDPAYMENT'
        };
    }

    /**
     * Get state of selected payment type
     * @return {boolean} true if selected payment type is Card Payment
     */
    get isCardPaymentSelected() {
        return (
            this.selectedPaymentType === 'CARDPAYMENT'
        );
    }

    /**
     * Get state of selected payment type
     * @return {boolean} true if selected payment type is PO number
     */
    get isPoNumberSelected() {
        return this.selectedPaymentType === 'PONUMBER';
    }

    /**
     * Handler to initialize the payment component
     * @param {String} cartId - the current webCart ID
     */
    initializePaymentData(cartId) {
        // If we don't have those values yet
        getPaymentInfo({ cartId: cartId, communityId: communityId })
            .then((data) => {
                
                this._purchaseOrderNumber = data.purchaseOrderNumber;
                this._addresses = data.addresses;
            })
            .catch((error) => {
                //do nothing, continue as normal
                console.log(error.body.message);
            });
    }

    /**
     * Handler for the 'blur' event fired from the purchase order input.
     */
    handleUpdate() {
        const poComponent = this.getComponent('[data-po-number]');
        const poData = (poComponent.value || '').trim();
        this._purchaseOrderNumber = poData;
    }

    /**
     * Handler for the 'click' event fired from the payment type radio buttons.
     * @param {event} event - The selected payment type
     */
    handlePaymentTypeSelected(event) {
        this._selectedPaymentType = event.currentTarget.value;
    }

    /**
     * Handler for the 'click' event fired from the payment button.
     * If PO Number is selected, it fires the 'FlowNavigationNextEvent'.
     * If Credit Card is selected, check to see that all required fields are filled in first,
     * then makes an apex call which in turns makes a server call to Payment.authorize endpoint
     */
    handlePaymentButton() {
        if (
            this.selectedPaymentType !== 'CARDPAYMENT'
        ) {
            // For Purchase Order - the server calls are done in the flow.
            // Navigate NEXT in the flow
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);

        } else {
            // First let's get the cc data
            const creditPaymentComponent = this.getComponent(
                '[data-credit-payment-method]'
            );

            // Second let's make sure the required fields are valid
            if (
                creditPaymentComponent != null &&
                !creditPaymentComponent.reportValidity()
            ) {
                return;
            }

            const creditCardData = this.getCreditCardFromComponent(
                creditPaymentComponent
            );

            //Get the addresselected
            const selectedAddress = this._addresses.filter(add => add.id === this.selectedBillingAddress)[0];
            console.log('PURCHASE ORDER: ' + creditPaymentComponent['poNumber'] );
            console.log('CREDITPAYMENTCOMPONENT: ' + creditPaymentComponent );
            this._purchaseOrderNumber = creditPaymentComponent['poNumber'];
            updatePaymentInfo({
                cartId: this.cartId,
                selectedBillingAddress: selectedAddress,
                paymentInfo: creditCardData
            }).then(() => {
                // After making the server calls, navigate NEXT in the flow
                const navigateNextEvent = new FlowNavigationNextEvent();
                this.dispatchEvent(navigateNextEvent);
            }).catch((error) => {
                this._cardAuthErrorMessage = error.body.message;
            });
        }
    }

    getCreditCardFromComponent(creditPaymentComponent) {
        const cardPaymentData = {};
        [
            'cardHolderName',
            'cardNumber',
            'cvv',
            'expiryYear',
            'expiryMonth',
            'expiryYear',
            'cardType',
            'poNumber'
        ].forEach((property) => {
            cardPaymentData[property] = creditPaymentComponent[property];
        });
        return cardPaymentData;
    }

    /**
     * Set the address selected
     */
    handleChangeSelectedAddress(event) {
        const address = event.detail.address;
        if (address.id !== null && (address.id).startsWith('8lW')) {
            this._selectedBillingAddress = address.id;
        } else {
            this._selectedBillingAddress = '';
        }
    }

    /**
     * Simple function to query the passed element locator
     * @param {*} locator The HTML element identifier
     * @private
     */
    getComponent(locator) {
        return this.template.querySelector(locator);
    }

    dispatchCustomEvent(eventName, detail) {
        this.dispatchEvent(
            new CustomEvent(eventName, {
                bubbles: false,
                composed: false,
                cancelable: false,
                detail
            })
        );
    }
    
    showToast(title, message, variant) 
    {
        let event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(event);
    }
}