import { LightningElement,api, wire } from 'lwc';
import { FlowAttributeChangeEvent} from 'lightning/flowSupport';
//import getRecords from '@salesforce/apex/MRAssignmentGroupExtract.getRecords';
import getDistRecords from '@salesforce/apex/MRAssignmentGroupExtract.getChannelPartnerInformation';
import MRDistributorLabel from '@salesforce/label/c.MR_Distributor_Territory';
import MRRSMLabel from '@salesforce/label/c.MR_RSM_Territory';
import ARODistributorLabel from '@salesforce/label/c.ARO_Distributor_Territory';
import ARORSMLabel from '@salesforce/label/c.ARO_RSM_Territory';
//import DistributorLabel from '@salesforce/label/c.MR_Distributor_Trio';
//import RSMLabel from '@salesforce/label/c.RSM_Group_Name';
import RSMLabelMPOB from '@salesforce/label/c.MP_OB_IRP_RSM_Group_Name';
import DistributorLabelMPOB from '@salesforce/label/c.MP_OB_IRP_Distributor_Territory';
import formFactorName from "@salesforce/client/formFactor";



export default class MRGlobalSearchComponent extends LightningElement {

@api inputLabelName;
@api GroupName;
 searchKey;
 searchText = '';
@api records = [];
@api tableRenderFlag = false;
@api isFormFactorMobile = false;
searchDisable = true;
isLoading = false;
isGlobalSearch = false;
 
@wire(getDistRecords,{
    GroupName:"$GroupName",
    searchKey: "$searchKey" 
})
getDistributorList({data, errors}){
    this.records = [];
    let recordsMap = new Map();
    if(data){
        console.log('Check MPOB Data');
        data.forEach((item)=>{
            if((this.GroupName == MRDistributorLabel || this.GroupName == ARODistributorLabel)  && item.DISTID__c != undefined){
              
                recordsMap.set(item.DISTID__c,item);
            }
            if((this.GroupName == MRRSMLabel || this.GroupName == ARORSMLabel) && item.RSMID__c != undefined){
                recordsMap.set(item.RSMID__c,item);
            }
            if(this.GroupName == RSMLabelMPOB  && item.RSMID__c != undefined){
                recordsMap.set(item.RSMID__c,item);
                console.log('RSM Data Mapped for MPOB');
            }
            if(this.GroupName == DistributorLabelMPOB  && item.DISTID__c != undefined){
              
                recordsMap.set(item.DISTID__c,item);
                console.log('Distributor Data Mapped for MPOB');
            }
           
        });
        
        this.records = Array.from(recordsMap.values());
      
       
    }
    else{
        console.log('No Data returned');
        console.log(this.GroupName);
    }
   
    if(errors){
        console.log(errors);
    }
    this.isLoading = false;
    const refreshEvent = new FlowAttributeChangeEvent("records",this.records);
    this.dispatchEvent(refreshEvent);
   
  
}

connectedCallback(){

    console.log('form Factor Name : '+ formFactorName);
    if(this.GroupName == MRDistributorLabel || this.GroupName == MRRSMLabel || this.GroupName == RSMLabelMPOB || this.GroupName == DistributorLabelMPOB || this.GroupName == ARODistributorLabel || this.GroupName == ARORSMLabel ){this.isGlobalSearch = true;}
    if(formFactorName == 'Medium' || formFactorName == 'Small'){
        this.isFormFactorMobile = true;
    }
    else{this.isFormFactorMobile = false;}
    const refereshFormFactorFlag = new FlowAttributeChangeEvent("isFormFactorMobile",this.isFormFactorMobile);
    this.dispatchEvent(refereshFormFactorFlag);
    
}

 SearchKeyHandler(event){
   
    if(event.target.value.length >= 3){
        this.searchDisable = false;
        this.searchText = event.target.value;
        
    }
    else{
        this.searchDisable = true;
    }
   
 }
 handleSearch() {
                if(this.searchKey != this.searchText){
                    this.searchKey = this.searchText; 
                    this.isLoading = true;
                }
               if(this.tableRenderFlag == false){
                this.tableRenderFlag = true;
                const refreshTableFlag = new FlowAttributeChangeEvent("tableRenderFlag",this.tableRenderFlag);
                this.dispatchEvent(refreshTableFlag);
            
               }
                   
                
    }
}