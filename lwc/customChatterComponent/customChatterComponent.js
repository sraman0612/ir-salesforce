import { LightningElement, wire, api, track } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import Assigned_From_Address_Picklist__c from '@salesforce/schema/Case.Assigned_From_Address_Picklist__c';
import getOrgWideFromAddress from '@salesforce/apex/FeedController.orgWideFromAddress';
import getChatterPosts from '@salesforce/apex/FeedController.getChatterPosts';
import postComment from '@salesforce/apex/FeedController.postComment';
import getCaseEmails from '@salesforce/apex/FeedController.getCaseEmails';
import sendEmail from '@salesforce/apex/FeedController.sendEmail';
import searchEmails from '@salesforce/apex/FeedController.searchEmails';
import markEmailAsRead from '@salesforce/apex/FeedController.markEmailAsRead';
import getEmailAttachments from '@salesforce/apex/FeedController.getEmailAttachments';
import uploadFiles from '@salesforce/apex/FeedController.uploadFiles';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CustomChatterComponent extends LightningElement {
    @track postData;
    @track caseEmailData;
    @api recordId; // Case ID
    @track feedItems = [];
    @track posts = [];
    @track emails = [];
    @track fromAddresses = [];
    @track searchTerm = '';
    @track toSearchTerm = '';
    @track ccSearchTerm = '';
    @track bccSearchTerm = '';
    @track emailSuggestions = [];
    @track storeEmails = [];
    @track selectedToEmails = [];
    @track selectedCcEmails = [];
    @track selectedBccEmails = [];
    @track commentText = '';
    @track userHyperLink = '';
    @track replyDiv = false;
    @track renderTosuggestion = false;
    @track renderCcsuggestion = false;
    @track renderBccsuggestion = false;
    @track emailDetails = {};
    @track emailData = {};
    @track assignedFromAddressPicklist;
    @track isAllExpanded = false;
    @track iconState = 'utility:expand_all';
    @track showBcc = false;
    @track showCc = false;
    @track refreshClass = ''; // Initialize without animation class
    @track invalidEmail = false;
    @track selectedAttachmentIds = [];
    @track newAttachments = [];
    @track existingAttachments = [];
    @track fileData = [];
    @track showProgress = false;
    @track progressValue = 0;
    @track attachments;
    refreshInterval;
    BASE_DOWNLOAD_PATH = '/sfc/servlet.shepherd/version/download';

    // Fetch the Chatter posts when the component loads

    //auto refresh this component every 5 seconds 
    /* connectedCallback(){
         this.refreshInterval = setInterval (() => {
             this.handleRefresh();
         }, 5000);
     }*/
    columns = [
        { label: 'File Name', fieldName: 'fileName' },
        /*{ label: 'File Type', fieldName: 'FileExtension' },*/
        { label: 'File Size', fieldName: 'fileSize' },
        { label: 'Created Date', fieldName: 'formattedDate' },
    ];
    @wire(getRecord, {
        recordId: '$recordId',
        fields: [Assigned_From_Address_Picklist__c]
    })
    wiredCase({ error, data }) {
        if (data) {
            this.assignedFromAddressPicklist = data.fields.Assigned_From_Address_Picklist__c.value;
        }
        else if (error)
            console.error('Error in fetching Case', error);
    }
    @wire(getOrgWideFromAddress)
    wiredgetOrgWideEmails({ error, data }) {
        if (data)
            this.fromAddresses = data.map(frAdd => {
                return {
                    label: frAdd.DisplayName + ' <' + frAdd.Address + '> ',
                    value: frAdd.Address
                }
            })
        else if (error)
            console.error('Error in fetching org Wide email addresses ', error);
    }
    @wire(getChatterPosts, { caseId: '$recordId' })
    wiredPosts(result) {
        this.postData = result;
        const { data, error } = result;
        if (this.postData.data) {
            this.posts = this.postData.data.map(post => {
                return {
                    ...post,
                    iconState: 'utility:expand_all', newBody: this.updateImageSrc(post.Body, post.instanceURL), isExpanded: false, isPost: true, showCommentBox: false, userHyperLink: '/lightning/r/User/' + post.CreatedById + '/view', formattedDate: this.formatDateTime(post.CreatedDate),
                    Comments: post.Comments.map(comment => {
                        return {
                            ...comment,
                            userHyperLink: '/lightning/r/User/' + comment.CreatedById + '/view',
                            formattedDate: this.formatDateTime(comment.CreatedDate)
                        };
                    })
                };
            })
            this.updateFeed(this.posts);
        } else if (this.postData.error) {
            console.error('Error fetching posts:', this.postData.error);
        }
    }
    @wire(getCaseEmails, { caseId: '$recordId' })
    wiredEmails(result) {
        this.caseEmailData = result;
        const { data, error } = result;
        if (this.caseEmailData.data) {
            this.emails = this.caseEmailData.data.map(email => ({ ...email, bccAdd: email.BccAddress === undefined ? '' : email.BccAddress, iconState: 'utility:expand_all', emailIcon: email.Status == '0' ? 'utility:email' : 'utility:email_open', isEmail: true, isExpanded: false, userHyperLink: '/lightning/r/User/' + email.CreatedById + '/view', formattedDate: this.formatDateTime(email.CreatedDate), attachmentIcon: email.HasAttachment == true ? 'utility:attach' : '', showAttachmentTable: false }));
            this.updateFeed(this.emails);
        } else if (this.caseEmailData.error) {
            console.error('Error fetching emails:', this.caseEmailData.error);
        }
    }
    @wire(searchEmails, { searchTerm: '$searchTerm' })
    wiredSearchEmails({ error, data }) {
        if (data) {
            this.emailSuggestions = data.filter(email => !this.emailSuggestions.some(pill => pill.label === email));
            this.storeEmails = [...this.selectedToEmails, ...this.selectedCcEmails, this.selectedBccEmails];
            // if (this.storeEmails.length > 0) {
            //     this.emailSuggestions = this.emailSuggestions.filter(item => !this.storeEmails.some(pill => pill.label === item.label));
            // }

        } else if (error) {
            console.error('Error fetching email suggestions:', error);
            this.emailSuggestions = [];
        }
    }
    formatDateTime(dateString) {
        const date = new Date(dateString);
        return new Intl.DateTimeFormat('en-US', {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        }).format(date);
    }
    updateFeed(data) {
        // Combine and sort by created date
        this.feedItems = [...this.feedItems, ...data].sort((a, b) => {
            return new Date(b.CreatedDate) - new Date(a.CreatedDate);
        });
    }
    updateImageSrc(body, instanceURL) {
        if (!body) {
            return body;
        }
        const imgTagRegex = /<img\s+[^>]*src="sfdc:\/\/([^"]+)"/g;
        const modifiedBody = body.replace(imgTagRegex, (match, id) => {
            let newSrc = '/sfc/servlet.shepherd/document/download/' + id;
            return match.replace(`sfdc://${id}`, newSrc);
        });
        return modifiedBody; // Return the modified body string
    }
    // Handle refresh button click
    handleRefresh() {
        this.refreshClass = 'spin';
        setTimeout(() => {
            // Stop the animation after refresh is complete
            this.feedItems = [];
            refreshApex(this.postData);
            refreshApex(this.caseEmailData);
            refreshApex(this.posts);
            this.updateFeed(this.posts);
            this.updateFeed(this.emails);
            this.refreshClass = '';
        }, 2000); // Adjust timing as needed based on your refresh logic
        this.feedItems = [];
        refreshApex(this.postData);
        refreshApex(this.caseEmailData);
        refreshApex(this.posts);
        this.updateFeed(this.posts);
        this.updateFeed(this.emails);
    }
    // Handle expand all posts
    expandAll() {
        this.isAllExpanded = !this.isAllExpanded;
        this.feedItems = this.feedItems.map(item => {
            item.isExpanded = this.isAllExpanded;
            item.iconState = this.isAllExpanded ? 'utility:collapse_all' : 'utility:expand_all';
            return item;
        });
        this.iconState = this.isAllExpanded ? 'utility:collapse_all' : 'utility:expand_all';
        if(!this.isAllExpanded){
            this.feedItems = this.feedItems.map(item => {
                const attachmentLabel = this.template.querySelector('.attachment-button');
                if (item.isEmail && item.showAttachmentTable == true) {
                    item.showAttachmentTable = false;
                    attachmentLabel.label = 'Show Attachments';
                    attachmentLabel.title = 'Click to see the available attachemnts';
                }
                return item;
            });
        }
    }
    toggleItem(event) {
        console.log('inside toggleItem ---> itemId :' + event.target.dataset.id);
        const itemId = event.target.dataset.id;
        this.feedItems = this.feedItems.map(item => {
            if (item.Id === itemId) {
                item.isExpanded = !item.isExpanded;
                item.iconState = item.isExpanded ? 'utility:collapse_all' : 'utility:expand_all';
                const attachmentLabel = this.template.querySelector('.attachment-button');
                if (item.isEmail && item.emailIcon != 'utility:email_open') {
                    markEmailAsRead({ emailId: item.Id }).then(
                        () => {
                            console.log('Email ' + item.Id + ' marked as read');
                            //Set emailIcon to open
                            item.emailIcon = 'utility:email_open';
                        });
                }
                if (!isExpanded && item.isEmail && item.showAttachmentTable == true) {
                    item.showAttachmentTable = false;
                    attachmentLabel.label = 'Show Attachments';
                    attachmentLabel.title = 'Click to see the available attachemnts';
                }
            }
            return item;
        });
        this.fileData = [];
    }
    //
    handleComment(event) {
        console.log('inside handleComment ---> postId :' + event.target.dataset.id);
        const postId = event.target.dataset.id;
        this.feedItems = this.feedItems.map(item => {
            if (item.Id === postId) {
                item.showCommentBox = !item.showCommentBox;
            }
            return item;
        });
        console.log(' handleComment ---> this.feedItems ', JSON.stringify(this.feedItems));
    }
    //
    cancelComment(event) {
        const postId = event.target.dataset.id;
        this.feedItems = this.feedItems.map(item => {
            if (item.Id === postId) {
                item.showCommentBox = false;
            }
            return item;
        });
        this.commentText = '';
    }
    // Handle comment input change
    handleCommentChange(event) {
        this.commentText = event.target.value;
    }
    // Add a comment to a post
    addComment(event) {
        const postId = event.target.dataset.id;
        postComment({ postId: postId, commentBody: this.commentText })
            .then(() => {
                this.commentText = ''; // Clear the input field
                this.feedItems = this.feedItems.map(item => {
                    if (item.Id === postId) {
                        item.showCommentBox = false;
                        item.isExpanded = false;
                    }
                    return item;
                });
                this.showToast('Success', 'Comment added successfully', 'success');
                setTimeout(() => {

                    this.handleRefresh();
                }, 1000);
            })
            .catch(error => {
                this.showToast('Error', 'Failed to add comment :\n' + error.body.message, 'error');
                console.error('Error adding comment:', error);
            });
    }
    handleReply(event) {
        console.log('inside handleReply ---> actionType :' + event.target.name);

        const emailId = event.target.dataset.id;
        const actionType = event.target.name;
        const generateEmail = this.feedItems.find(email => email.Id === emailId);
        if (generateEmail.ToAddress == '' || generateEmail.ToAddress == null || generateEmail.ToAddress === undefined)
            generateEmail.ToAddress = '';
        if (generateEmail.CcAddress == '' || generateEmail.CcAddress == null || generateEmail.CcAddress === undefined)
            generateEmail.CcAddress = '';
        if (generateEmail.BccAddress == '' || generateEmail.BccAddress == null || generateEmail.BccAddress === undefined)
            generateEmail.BccAddress = '';

        if (actionType === 'Reply') {
            this.showCc = false;
            this.showBcc = false;
            console.log(' reply  === actionType :' + event.target.name);
            if (generateEmail.ToAddress != '' && generateEmail.ToAddress != null) {
                const multipleToMail = generateEmail.FromAddress.split(';');
                this.selectedToEmails = multipleToMail.map(email => ({ label: email }));
            }
            this.emailDetails = {
                emailId: emailId,
                toAddresses: generateEmail.FromAddress,
                fromAddress: generateEmail.ToAddress,
                ccAddresses: '',
                bccAddresses: '',
                subject: 'Re : ' + generateEmail.Subject,
                emailBody: `<p><br/><br/> --------------- Original Message --------------- <br/>
                <strong>From:</strong> ${generateEmail.FromAddress}<br/>
                <strong>Sent:</strong> ${generateEmail.formattedDate}<br/>
                <strong>To:</strong> ${generateEmail.ToAddress}<br/>
                <strong>Cc:</strong> ${generateEmail.CcAddress}<br/>
                <strong>Bcc:</strong> ${generateEmail.BccAddress}<br/>
                <strong>Subject:</strong> ${generateEmail.Subject}<br/><p><br/>` + generateEmail.HtmlBody
            }
        }
        else if (actionType === 'Forward') {
            this.showCc = false;
            this.showBcc = false;
            console.log(' Forward  === actionType :' + event.target.name);
            this.emailDetails = {
                emailId: emailId,
                toAddresses: '',
                fromAddress: generateEmail.FromAddress,
                ccAddresses: '',
                bccAddresses: '',
                subject: 'Fw : ' + generateEmail.Subject,
                emailBody: `<p><br/><br/> --------------- Original Message --------------- <br/>
                <strong>From:</strong> ${generateEmail.FromAddress}<br/>
                <strong>Sent:</strong> ${generateEmail.formattedDate}<br/>
                <strong>To:</strong> ${generateEmail.ToAddress}<br/>
                <strong>Cc:</strong> ${generateEmail.CcAddress}<br/>
                <strong>Bcc:</strong> ${generateEmail.BccAddress}<br/>
                <strong>Subject:</strong> ${generateEmail.Subject}<br/><p><br/>` + generateEmail.HtmlBody
            }
        }
        else if (actionType === 'ReplyAll') {
            this.showCc = false;
            this.showBcc = false;
            console.log(' ReplyAll  === actionType :' + event.target.name);
            if (generateEmail.ToAddress != '' && generateEmail.ToAddress != null) {
                const multipleToMail = generateEmail.FromAddress.split(';');
                this.selectedToEmails = multipleToMail.map(email => ({ label: email }));
            }
            if (generateEmail.CcAddress != '' && generateEmail.CcAddress != null) {
                const multipleCcMail = generateEmail.CcAddress.split(';');
                this.selectedCcEmails = multipleCcMail.map(email => ({ label: email }));
            }

            if (generateEmail.BccAddress != '' && generateEmail.BccAddress != null) {
                const multipleBccMail = generateEmail.BccAddress.split(';');
                this.selectedBccEmails = multipleBccMail.map(email => ({ label: email }));
            }
            this.emailDetails = {
                emailId: emailId,
                toAddresses: generateEmail.FromAddress,
                fromAddress: generateEmail.ToAddress,
                ccAddresses: generateEmail.CcAddress,
                bccAddresses: generateEmail.BccAddress,
                subject: 'Re : ' + generateEmail.Subject,
                emailBody: `<p><br/><br/> --------------- Original Message --------------- <br/>
                <strong>From:</strong> ${generateEmail.FromAddress}<br/>
                <strong>Sent:</strong> ${generateEmail.formattedDate}<br/>
                <strong>To:</strong> ${generateEmail.ToAddress}<br/>
                <strong>Cc:</strong> ${generateEmail.CcAddress}<br/>
                <strong>Bcc:</strong> ${generateEmail.BccAddress}<br/>
                <strong>Subject:</strong> ${generateEmail.Subject}<br/><p><br/>` + generateEmail.HtmlBody
            }
        }
        this.replyDiv = true;
        //to see the available attachments in current email
        this.getAttachments(emailId)
    }
    validateEmail(email) {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }
    handleToChange(event) {
        this.renderTosuggestion = true;
        this.renderCcsuggestion = false;
        this.renderBccsuggestion = false;
        this.invalidEmail = '';
        const newSearchTerm = event.target.value;
        if (newSearchTerm.length >= 3) {
            this.searchTerm = newSearchTerm;
        }
        else {
            this.searchTerm = '';
            this.emailSuggestions = []; // Clear suggestions if less than 3 characters
            this.renderTosuggestion = false;
        }
        this.toSearchTerm = '';
    }
    handleCcChange(event) {
        this.renderCcsuggestion = true;
        this.renderTosuggestion = false;
        this.renderBccsuggestion = false;
        const newSearchTerm = event.target.value;
        if (newSearchTerm.length >= 3) {
            this.searchTerm = newSearchTerm;
        }
        else {
            this.searchTerm = '';
            this.renderCcsuggestion = false;
            this.emailSuggestions = []; // Clear suggestions if less than 3 characters
        }
        this.ccSearchTerm = '';
    }
    handleBccChange(event) {
        this.renderBccsuggestion = true;
        this.renderTosuggestion = false;
        this.renderCcsuggestion = false;
        const newSearchTerm = event.target.value;
        if (newSearchTerm.length >= 3) {
            this.searchTerm = newSearchTerm;
        }
        else {
            this.searchTerm = '';
            this.renderBccsuggestion = false;
            this.emailSuggestions = []; // Clear suggestions if less than 3 characters
        }
        this.bccSearchTerm = '';
    }
    handleEmailkeyDown(event) {
        const selectedLabel = event.target.label;
        const selectedEmail = event.target.value;
        console.log('inside handleEmailkeyDown');
        console.log('selectedLabel : ', selectedLabel);
        console.log('selectedEmail : ', selectedEmail);
        console.log('event.key : ', event.key);
        if (event.key === 'Enter' && selectedLabel === 'To' && this.validateEmail(selectedEmail)) {
            console.log(' inside toemail validation');
            if (!this.selectedToEmails.some(pill => pill.label === selectedEmail)) {
                this.selectedToEmails.push({ label: selectedEmail });
                this.renderTosuggestion = false;
            }
        } else {
            this.invalidEmail = true
        }
        if (event.key === 'Enter' && selectedLabel === 'Cc' && this.validateEmail(selectedEmail)) {
            console.log(' inside ccemail validation');
            if (!this.selectedCcEmails.some(pill => pill.label === selectedEmail)) {
                this.selectedCcEmails.push({ label: selectedEmail });
                this.renderCcsuggestion = false;
            }

        } else {
            this.invalidEmail = true
        }
        if (event.key === 'Enter' && selectedLabel === 'Bcc' && this.validateEmail(selectedEmail)) {
            console.log(' inside bccemail validation');
            if (!this.selectedBccEmails.some(pill => pill.label === selectedEmail)) {
                this.selectedBccEmails.push({ label: selectedEmail });
                this.renderBccsuggestion = false;
            }
        } else {
            this.invalidEmail = true
        }
    }
    handleToEmailSelect(event) {
        const selectedEmail = event.target.dataset.email;
        console.log('inside handleToEmailSelect');
        console.log('selectedEmail : ', selectedEmail);
        console.log('this.selectedToEmails : ', this.selectedToEmails);
        if (this.validateEmail(selectedEmail)) {
            if (!this.selectedToEmails.some(pill => pill.label === selectedEmail)) {
                this.selectedToEmails.push({ label: selectedEmail });
                this.renderTosuggestion = false;
            }
            this.searchTerm = '';
            this.toSearchTerm = '';
            this.emailSuggestions = [];
        }
        else
            this.invalidEmail = true
    }
    handleCcEmailSelect(event) {
        console.log('inside handleCcEmailSelect');
        const selectedEmail = event.target.dataset.email;
        console.log('selectedEmail : ', selectedEmail);
        if (!this.selectedCcEmails.some(pill => pill.label === selectedEmail)) {
            this.selectedCcEmails.push({ label: selectedEmail });
            this.renderCcsuggestion = false;
        }
        this.searchTerm = '';
        this.ccSearchTerm = '';
        this.emailSuggestions = [];
    }
    handleBccEmailSelect(event) {
        const selectedEmail = event.target.dataset.email;
        console.log('inside handleBccEmailSelect');
        console.log('selectedEmail : ', selectedEmail);
        if (!this.selectedBccEmails.some(pill => pill.label === selectedEmail)) {
            this.selectedBccEmails.push({ label: selectedEmail });
            this.renderBccsuggestion = false;
        }
        this.searchTerm = '';
        this.bccSearchTerm = '';
        this.emailSuggestions = [];
    }
    handlePillRemove(event) {
        console.log('inside handlePillRemove');
        const emailToRemove = event.detail.item.label;
        const selectedLabel = event.target.label;
        console.log('inside handlePillRemove ---> selectedLabel : ', selectedLabel);
        console.log('inside handlePillRemove ---> emailToRemove : ', emailToRemove);
        if (selectedLabel === 'To') {
            this.selectedToEmails = this.selectedToEmails.filter(email => email.label !== emailToRemove);
        }
        if (selectedLabel === 'Cc') {
            this.selectedCcEmails = this.selectedCcEmails.filter(email => email.label !== emailToRemove);
        }
        if (selectedLabel === 'Bc') {
            this.selectedBccEmails = this.selectedBccEmails.filter(email => email.label !== emailToRemove);
        }
    }
    handleInputChange(event) {
        const field = event.target.name;
        this[field] = event.target.value;
        this.emailData[field] = event.target.value;
    }
    handleSendEmail() {
        const comboBox = this.template.querySelector('lightning-combobox');
        if (comboBox.value) {
            console.log('inside handleSendEmail');
            let emailId = this.template.querySelector("[data-id = 'currentEmailId']").value;
            let fromAddress = this.emailData.fromAddress;
            let subject = this.emailData.subject;
            let emailBody = this.emailData.emailBody;
            let input;
            let fromName;

            if (this.emailData.fromAddress == undefined)
                fromAddress = this.template.querySelector("[data-id = 'fromAddress']").value;
            if (this.emailData.subject == undefined)
                subject = this.template.querySelector("[data-id = 'subject']").value;
            if (this.emailData.emailBody == undefined)
                emailBody = this.template.querySelector("[data-id = 'emailBody']").value;

            this.fromAddresses.map(item => {
                if (item.value === fromAddress)
                    input = item.label;
            });

            const regex = /(.*?)\s*<([^>]+)>/;
            const match = input.match(regex);
            if (match) {
                fromName = match[1].trim();     // Extracted fromName
                fromAddress = match[2].trim();    // Extracted fromAddress
                console.log('fromName : ', fromName);
                console.log('fromAddress : ', fromAddress);
            }
            const finalToEmails = this.selectedToEmails.map(pill => pill.label);
            const finalCcEmails = this.selectedCcEmails.map(pill => pill.label);
            const finalBccEmails = this.selectedBccEmails.map(pill => pill.label);
            // Combine existing and new attachment IDs
            console.log('allAttachmentIds : ', this.selectedAttachmentIds);
            console.log('email Id ', emailId);
            console.log('final from address ', fromAddress);
            console.log('finalToEmails ', finalToEmails);
            console.log('finalCcEmails ', finalCcEmails);
            console.log('finalBccEmails ', finalBccEmails);
            console.log('fromName ', fromName);
            console.log('fromAddress ', fromAddress);
            console.log('subject ', subject);
            console.log('emailBody ', emailBody);
            sendEmail({ emailId: emailId, attachmentIds: this.selectedAttachmentIds, fromAddress: fromAddress, toAddresses: finalToEmails, ccAddresses: finalCcEmails, bccAddresses: finalBccEmails, subject: subject, emailBody: emailBody })
                .then(() => {
                    console.log('Email sent successfully');
                    this.showToast('Success', 'Email sent successfully', 'success');
                    this.feedItems = this.feedItems.map(item => {
                        if (item.Id === emailId) {
                            item.isExpanded = false;
                        }
                        return item;
                    });
                    this.selectedAttachmentIds = [];
                    this.replyDiv = false;
                    setTimeout(() => {
                        this.handleRefresh();
                    }, 1000);
                    this.clearFields(); // Clear fields after sending email
                })
                .catch(error => {
                    this.showToast('Error', 'Failed to send email :\n' + error.body.message, 'error');
                    console.error('Error while sending email :', error);
                });
            this.replyDiv = false;
        } else {
            comboBox.setCustomValidity('Please select from address');
            comboBox.reportValidity();
        }

    }
    showToast(title, message, variant) {
        const toastEvent = new ShowToastEvent({
            title,
            message,
            variant
        });
        this.dispatchEvent(toastEvent);
    }
    clearFields() {
        this.selectedToEmails = [];
        this.selectedCcEmails = [];
        this.selectedBccEmails = [];
        this.emailData = {};
    }
    closeModal() {
        this.clearFields();
        this.replyDiv = false;
    }
    // Toggle CC field visibility
    toggleCcField() {
        this.showCc = !this.showCc;
    }
    // Toggle BCC field visibility
    toggleBccField() {
        this.showBcc = !this.showBcc;
    }
    showAttachments(event) {
        this.attachments = [];
        const emailId = event.target.dataset.id;
        this.getAttachments(emailId);
        console.log('emailId ---> ', emailId);
        this.feedItems = this.feedItems.map(item => {
            if (item.isEmail && item.Id === emailId) {
                const attachmentLabel = this.template.querySelector('.attachment-button');
                console.log('entry attachmentLabel ', attachmentLabel.label);
                console.log('entry item.showAttachmentTable ', item.showAttachmentTable);
                if (item.showAttachmentTable) {
                    item.showAttachmentTable = false;
                    attachmentLabel.label = 'Show Attachments';
                    attachmentLabel.title = 'Click to see the available attachemnts';
                } else {
                    item.showAttachmentTable = true;
                    attachmentLabel.label = 'Hide Attachments';
                    attachmentLabel.title = 'Click to hide the attachemnts';
                }
                console.log('exit attachmentLabel ', attachmentLabel.label);
                console.log('exit item.showAttachmentTable ', item.showAttachmentTable);
            }
            return item;
        });
    }
    getAttachments(emailId) {
        getEmailAttachments({ emailMessageId: emailId })
            .then(result => {
                this.attachments = result.map((attachment) => ({
                    ...attachment,
                    id: attachment.Id,
                    fileSize: attachment.LatestPublishedVersion.File_Size__c,
                    formattedDate: this.formatDateTime(attachment.CreatedDate),
                    fileName: attachment.Title
                }));
            })
            .catch(error => {
                console.log('error getEmailAttachment *** ', error);
            });
    }
    //
    downloadFiles() {
        let selectedFiles = this.template.querySelector('lightning-datatable').getSelectedRows();
        let downloadString = '';
        if (selectedFiles.length === 0) {
            const evt = new ShowToastEvent({
                title: 'No Files Selected',
                message: 'Please Select atleast 1 File to Proceed',
                variant: 'error',
            });
            this.dispatchEvent(evt);
            return;
        }
        console.log('selectedFiles : ', selectedFiles);
        selectedFiles.forEach(item => {
            downloadString += '/' + item.LatestPublishedVersionId
            console.log('downloadString : ', downloadString);
        });
        console.log('downloadString : ', downloadString);
        window.open(this.BASE_DOWNLOAD_PATH + downloadString, '_blank');
    }
    //
    handleExistingAttachmentSelect(event) {
        const fileId = event.target.dataset.id;
        console.log('fileId : ', fileId);
        if (!this.selectedAttachmentIds.includes(fileId)) {
            this.selectedAttachmentIds.push(fileId);
        } else {
            this.selectedAttachmentIds = this.selectedAttachmentIds.filter(item => item !== fileId);
        }
    }

    handleFileUpload(event) {
        console.log('inside handleFileUpload');
        const emailId = event.target.dataset.id;
        const files = event.target.files;
        for (const file of files) {
            //file size should be less than 5MB
            if (file.size > 10000000) {
                this.showToast('Files are too large \n', 'File size of file : ' + file.name + ' is more than 10MB. Kindly upload files below 10 MB', 'error');
                this.fileData = [];
                return;
            }
            let fileData;
            const reader = new FileReader();
            reader.onloadend = () => {
                const base64 = reader.result.split(',')[1];
                fileData = {
                    'fileName': file.name,
                    'base64': base64,
                    'emailId': emailId
                }
                this.fileData.push(fileData);
                console.log('from parent this.fileData : ', this.fileData);
            };
            reader.readAsDataURL(file);
        }
    }
    async insertFiles(event) {
        if (this.fileData.length > 0) {
            const emailId = event.target.dataset.id;
            this.showProgress = true;
            this.progressValue = 0;
            let interval = setInterval(() => {
                if (this.progressValue >= 100) {
                    clearInterval(interval);
                } else {
                    this.progressValue += 5;
                }
            }, 200)
            for (let file of this.fileData) {
                await uploadFiles({ emailMessageId: file.emailId, filename: file.fileName, base64: file.base64 })
                    .then(result => {
                        let res = JSON.parse(JSON.stringify(result));
                        this.attachments.push({
                            ...res,
                            fileName: result.Title,
                            newAttachment: true
                        });
                        console.log('Strigify result : ', JSON.stringify(result));
                        console.log('result Id : ', result.Id);
                        this.selectedAttachmentIds.push(result.Id);
                        console.log('this.selectedAttachmentIds : ', this.selectedAttachmentIds);
                        this.fileData = this.fileData.filter(item => item.fileName !== result.Title);
                    })
                    .catch(error => {
                        console.log('error : ', JSON.stringify(error));
                        this.showToast('Error', 'Failed to upload file with name :' + file.name + '\n ' + error.body.message, 'error');
                    });
            }
            clearInterval(interval);
            this.progressValue = 101;
            setTimeout(() => {
                this.showProgress = false;
            }, 500)
            console.log('attachments : ', JSON.stringify(this.attachments));
            this.fileData = [];
        }
        else {
            this.showToast('Error', 'No files to upload', 'error');
        }

    }
}