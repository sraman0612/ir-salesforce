import { LightningElement, api, wire } from 'lwc';
import getLoadInfo from '@salesforce/apex/CC_Order_LoadDetailsController.getLoadInfo';

export default class CC_Order_LoadDetails extends LightningElement {

    @api recordid;
    isLoadingInternal = true;
    loadInfo = [];
    loadsFound = false;
    isPortalUser = false;
    errorMessage;
    showTitle = false;
    kochLoadDataFound = false;
    fileDownloadHRef;
    fileDownloadName;

    connectedCallback(){

        console.log("connectedCallback");

        if ((!this.loadInfo || this.loadInfo.length == 0) && this.recordid){

            getLoadInfo({recordId: this.recordid})
            .then(result => {

                console.log("getLoadInfo");
                console.log(JSON.stringify(result));

                let loadInfo = result;

                if (loadInfo.loads){

                    let fileContent = "Order Number, Load Code, Serial Numbers\n";

                    loadInfo.loads.forEach(function(load){

                        fileContent += `${loadInfo.OrderNumber}, ${load.LoadCode}, ${load.SerialNumbers},\n`;
                        
                        if (loadInfo.kochLoadDataFound){
                            load.mapMarkers = [{location: {Latitude: load.Latitude, Longitude: load.Longitude, description: load.NearestGPSLocation}}];
                        }

                        let stopArrivalStatusClasses = 'slds-dl_horizontal__detail slds-tile__meta load-arrival-status-';
                                          
                        if (load.StpStatus){
                            stopArrivalStatusClasses += load.StpStatus.replaceAll(' ', '-').toLowerCase();
                        }

                        load.stopArrivalStatusClasses = stopArrivalStatusClasses;
                    }.bind(this));
               
                    this.fileDownloadName = "Load Tracking Details - " + loadInfo.OrderNumber + ".csv";
                    this.fileDownloadHRef = "data:text/csv;base64, " + btoa(fileContent);
                }

                if (loadInfo.kochLoadDataFound){
                    loadInfo.columnSize = 2;
                }
                else{
                    loadInfo.columnSize = 3;
                }                

                this.loadInfo = loadInfo;
                this.loadsFound = this.loadInfo.loads && this.loadInfo.loads.length > 0;
                this.isPortalUser = result.isPortalUser;
                this.kochLoadDataFound = result.kochLoadDataFound;
    
                if (!this.loadInfo.success && this.loadInfo.errorMessage){
                    this.errorMessage = this.loadInfo.errorMessage;
                }
            })
            .catch(error => {
                console.log('getLoadInfo error: ' + JSON.stringify(error));
                this.errorMessage = error ? error.message : "Unknown error.";
            })
            .finally(() => {
                this.isLoading = false;
            });                
        }
    }

    get isLoading(){
        return this.isLoadingInternal;
    }

    /**
     * @param {boolean} val
     */
    set isLoading(val){

        console.log("setIsLoading: " + val);
        this.isLoadingInternal = val;
        this.dispatchEvent(new CustomEvent("isloadingchange", {detail: {isLoading: this.isLoading}}));
    }    
}