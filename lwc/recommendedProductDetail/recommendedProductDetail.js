import { LightningElement, api } from "lwc";
//import addToCart from "@salesforce/apex/RecommendedProducts.addToCart";

import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { addItemToCart } from 'commerce/cartApi';

import ADD_TO_CART from "@salesforce/label/c.Add_To_Cart";
import SKU from "@salesforce/label/c.SKU";
import CART_SUCCESS_TITLE from "@salesforce/label/c.Cart_Success_Title";
import CART_FAILURE_TITLE from "@salesforce/label/c.Cart_Failure_Title";

export default class RecommendedProductDetail extends LightningElement {
	@api crossSellItem;
	label = {
		ADD_TO_CART,
		SKU,
		CART_SUCCESS_TITLE,
		CART_FAILURE_TITLE
	};
	//to store the map between productId & quantity
	qtyMap = new Map();
	quantity;
	loaded = true;
	connectedCallback() {
		console.log(this.crossSellItem.defaultImageUrl);
		this.quantity = this.crossSellItem.quantity;
		this.qtyMap.set(this.crossSellItem.recId, this.quantity);
	}

	handleRemoveClick() {
		if (this.quantity > 1) {
			this.qtyMap.set(this.crossSellItem.recId, --this.quantity);
		} else {
			this.qtyMap.set(this.crossSellItem.recId, this.quantity);
		}
	}
	handleAddClick() {
		this.qtyMap.set(this.crossSellItem.recId, ++this.quantity);
	}
	handleQuantityChange(evt) {
		this.qtyMap.set(this.crossSellItem.recId, evt.currentTarget.value);
	}

	handleAddToCart() {
		console.log("handleAddToCart>>>" + this.qtyMap.get(this.crossSellItem.recId));
		console.log("label>>>" + JSON.stringify(this.label));
		this.loaded = false;
		let quant = this.qtyMap.get(this.crossSellItem.recId);
		addItemToCart(this.crossSellItem.recId, quant).then(() => {
			this.loaded = true;
			this.dispatchEvent(
				new CustomEvent("addtocart", {
					detail: {
						message: "item added"
					}
				})
			);
			this.dispatchEvent(
				new ShowToastEvent({
					title: this.label.CART_SUCCESS_TITLE,
					message: " ",
					variant: "success"
				})
			);
		}).catch((error) => {
			this.loaded = true;
			console.log("errors: " + JSON.stringify(error));
			this.dispatchEvent(
				new ShowToastEvent({
					title: this.label.CART_FAILURE_TITLE,
					message: error.message,
					variant: "error"
				})
			);
		});

	}
}