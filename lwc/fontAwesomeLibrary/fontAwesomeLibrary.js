import { LightningElement } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import Font_Awesome_Resource from '@salesforce/resourceUrl/Font_Awesome_Library';

export default class Font_Awesome_Library extends LightningElement {

    initialized = false;

    renderedCallback(){

        if (!this.initialized){

            loadStyle(this, Font_Awesome_Resource + '/library/css/all.min.css')
            .then(() => {
                console.log("font awesome library loaded");
            })    
            .catch(error => {
                console.log("error loading font awesome library: " + error);
            });   

            this.initialized = true;
        } 
    }
}