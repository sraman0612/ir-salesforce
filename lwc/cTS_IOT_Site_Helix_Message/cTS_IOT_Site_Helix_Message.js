import { LightningElement, api, wire } from 'lwc';
import NOT_CONNECTED_MSG from '@salesforce/label/c.CTS_IOT_Site_Not_Helix_Connected_Compatible';
import hasConnectedAssets from '@salesforce/apex/CTS_IOT_Site_Helix_MessageController.hasConnectedAssets';

export default class CTS_IOT_Asset_Site_Message extends LightningElement {
    
    @api recordId;
    hasConnectedAssets = false;

    connectedCallback(){

        hasConnectedAssets({siteId: this.recordId})
        .then(result => {
            this.hasConnectedAssets = result;
        })
        .catch(error => {
            console.log("error checking for connected assets: " + JSON.stringify(error));
        })         
    }    

    get showMessage(){
        return this.showNotConnectedMessage;
    }

    get showNotConnectedMessage(){
        return !this.hasConnectedAssets;
    }

    get notConnectedMessage(){
        return NOT_CONNECTED_MSG;
    } 
}