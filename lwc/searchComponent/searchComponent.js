import { LightningElement,api } from 'lwc';
import { FlowAttributeChangeEvent} from 'lightning/flowSupport';

export default class SearchComponent extends LightningElement {

@api SearchKey = '';
@api ContactList = [];
@api CheckNull;


 flowName = '';
 


 handleSearch() {

            this.flowName = 'Fetch_return_Contact_details';
            console.log(this.SearchKey);
           
    }

    handleStatusChange(event) {
        this.flowName = '';

        if (event.detail.status === "FINISHED_SCREEN") {    
            
            for(let i = 0; i < event.detail.outputVariables.length; i++){
                if(event.detail.outputVariables[i].name == 'ContactList'){
                    console.log(JSON.stringify(event.detail.outputVariables[i].value));
                    this.ContactList = event.detail.outputVariables[i].value;
                    console.log(JSON.stringify(this.ContactList));
                }
                if(event.detail.outputVariables[i].name == 'CheckNull_Variable'){
                    console.log(JSON.stringify(event.detail.outputVariables[i].value));
                    this.CheckNull = event.detail.outputVariables[i].value;
                    console.log(this.CheckNull);
                }
            }

            if(this.CheckNull == true){
                this.ContactList = []; 
            }

            console.log(this.ContactList.length);
            console.log(Array.isArray(this.ContactList));
            const refreshEvent = new FlowAttributeChangeEvent("ContactList",this.ContactList);
            this.dispatchEvent(refreshEvent);

            const refreshEvent1 = new FlowAttributeChangeEvent("CheckNull",this.CheckNull);
            this.dispatchEvent(refreshEvent1);

        
                
  
        } else if (event.detail.status === "ERROR") {            
            console.log('Error...!!!');
        }
    }


    get inputVariables() {
        return [
            {
                name: 'searchKey',
                type: 'String',
                value: this.SearchKey
            }
        ]
    }
}