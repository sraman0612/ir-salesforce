import { LightningElement, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CTS_IOT_C0MMUNITY_RESOURCES from '@salesforce/resourceUrl/CTS_IOT_Community_Resources';
import UserID from '@salesforce/user/Id';
import USER_CONTACTID_FIELD from '@salesforce/schema/User.ContactId';
import USER_TIMEZONE_FIELD from '@salesforce/schema/User.TimeZoneSidKey';
import USER_EMAIL_FIELD from '@salesforce/schema/User.Email';
import USER_MOBILE_PHONE_FIELD from '@salesforce/schema/User.MobilePhone';
import DELIVERY_WINDOW_START_FIELD from '@salesforce/schema/Contact.CTS_Notifications_Delivery_Window_Start__c';
import DELIVERY_WINDOW_END_FIELD from '@salesforce/schema/Contact.CTS_Notifications_Delivery_Window_End__c';
import getInit from '@salesforce/apex/Notifications_Delivery_Options_Ctrl.getInit';
import updatePreferences from '@salesforce/apex/Notifications_Delivery_Options_Ctrl.updatePreferences';

export default class Notifications_Delivery_Options extends LightningElement {

    userId = UserID;
    contactId;
    userDefaultRecordTypeId = null;
    timeZone;
    timeZoneOptions = [];
    mobilePhone;
    email;
    emailChangedFlag = false;
    deliveryWindowStart;
    deliveryWindowEnd;
    isLoading = true;  

    @wire(getInit, {})
    getInit({ error, data }) {
        if (data) {

            console.log("getInit: " + JSON.stringify(data));

            let timeZoneOptions = [];

            for (let tzLabel in data.timeZoneOptions){
                timeZoneOptions.push({label: tzLabel, value: data.timeZoneOptions[tzLabel]});
            }

            this.timeZoneOptions = [...timeZoneOptions];
        } 
        else if (error){
            console.log(JSON.stringify(error));
            this.showToast("Error during getInit.", error, "error");
        }        
    }  

    @wire(getRecord, { recordId: '$userId', fields: [USER_CONTACTID_FIELD, USER_TIMEZONE_FIELD, USER_MOBILE_PHONE_FIELD, USER_EMAIL_FIELD] })
    wiredUser({ error, data }) {
        if (data) {
            console.log("wiredUser: " + JSON.stringify(data));
            this.contactId = data.fields.ContactId.value;
            this.timeZone = data.fields.TimeZoneSidKey.value;
            this.mobilePhone = data.fields.MobilePhone.value;
            this.email = data.fields.Email.value;
        } 
        else if (error){
            this.showToast("Error retrieving user record.", error, "error");
        }        
    }  
    
    @wire(getRecord, { recordId: '$contactId', fields: [DELIVERY_WINDOW_START_FIELD, DELIVERY_WINDOW_END_FIELD] })
    wiredContact({ error, data }) {
        if (data) {
            console.log("wiredContact");
            this.deliveryWindowStart = data.fields.CTS_Notifications_Delivery_Window_Start__c.value;
            this.deliveryWindowEnd = data.fields.CTS_Notifications_Delivery_Window_End__c.value;
            this.isLoading = false;
        } 
        else if (error){
            this.showToast("Error retrieving contact record.", error, "error");
            this.isLoading = false;
        }
    }   
    
    get communityStaticResourceURL(){
        return CTS_IOT_C0MMUNITY_RESOURCES;
    }
    
    get titleIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/IncreasedUptime_Red&Gray.png';
    }

    get lightBulbIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/EfficientEnergyUse_Red&Gray.png';
    }

    onMobilePhoneChange(event){
        this.mobilePhone = event.detail.value;
    }

    onEmailChange(event){
        this.email = event.detail.value;
        this.emailChangedFlag = true;
    }
    
    onDeliveryWindowStartChange(event){
        this.deliveryWindowStart = event.detail.value;
    }

    onDeliveryWindowEndChange(event){
        this.deliveryWindowEnd = event.detail.value;
    }

    onTimeZoneChange(event){
        this.timeZone = event.detail.value;
    }

    onUpdatePreferences(event){

        console.log("onUpdatePreferences");
        this.isLoading = true;

        if (this.emailChangedFlag){
            this.showToast("Email address changes must be verified.  After updating your preferences you should receive a verification email to complete the email address change.", "", "info");
        }

        let request = {
            contactId: 
            this.contactId, 
            userId: UserID, 
            deliveryWindowStart: this.deliveryWindowStart, 
            deliveryWindowEnd: this.deliveryWindowEnd,
            timezone: this.timeZone,
            mobile: this.mobilePhone,
            email: this.email
        };

        console.log("request: " + JSON.stringify(request));
        
        updatePreferences (request)
        .then(result => {
            console.log("update result: " + JSON.stringify(result));

            if (result.success){
                this.showToast("Delivery preferences successfully updated.", "", "success");                
            }
            else{
                this.showToast("Delivery preferences update failed.", result.errorMessage, "error");
            }

            this.isLoading = false;
        })
        .catch(error => {

            let msg;
            
            if (error && error.body.output && error.body.output.errors){
                msg = error.body.output.errors[0].message;
            }
            else if (error && error.body && error.body.message){
                msg = error.body.message
            } 
            else{
                msg = error;
            }

            console.log("updateRecord error: " + msg);
            this.showToast("Delivery preferences update failed.", msg, "error");
            this.isLoading = false;
        });   
    }

    showToast(title, message, variant){
        
        let event = new ShowToastEvent({
            "variant": variant,
            "title": title,
            "message": message,
            duration: 10000
        });

        this.dispatchEvent(event);        
    }    
}