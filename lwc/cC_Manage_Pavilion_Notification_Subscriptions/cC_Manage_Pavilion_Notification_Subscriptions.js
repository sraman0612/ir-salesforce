import { LightningElement, api, wire, track } from 'lwc';
import { createRecord, updateRecord, deleteRecord } from 'lightning/uiRecordApi';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import CC_Load_Tracking_Resources from '@salesforce/resourceUrl/CC_Load_Tracking_Resources';
import getInit from '@salesforce/apex/CC_Manage_Pavilion_Notification_SubCtrl.getInit';
import NOTIFICATION_SUBSCRIPTION_OBJECT from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c';
import SUBSCRIPTION_SCOPE_TYPE_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.Subscription_Scope_Type__c';
import SUBSCRIPTION_SCOPE_VALUE_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.Subscription_Scope_Value__c';
import ORDER_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.CC_Order__c';
import CAR_ITEM_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.CC_Car_Item__c';
import DELIVERY_FREQUENCIES_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.Delivery_Frequencies__c';
import DELIVERY_METHODS_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.Delivery_Methods__c';
import NOTIFICATIONS_TYPE_FIELD from '@salesforce/schema/CC_Pavilion_Notification_Subscription__c.Pavilion_Notification_Type__c';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import COM_ZONE_FIELD from '@salesforce/schema/Account.CC_COM_Zone__c';

const UNEXPECTED_ERROR_TITLE = "An unexpected error occurred.";
const USER_SCOPE_TYPES = ["Account Owner", "Sales Rep", "Used Sales Rep", "National Account Rep", "Strategic Account Rep", "Connectivity Sales Manager"];
const SUBSCRIPTION_SCOPE_VALUE_REQ_EXCLUSIONS = ["All"];

export default class CC_Manage_Pavilion_Notification_Subscriptions extends LightningElement {

    @api recordid;
    @api showClose;
    order;
    carItem;
    isPortalUser = false;
    mobilePhone;
    currentUserId;
    subscriptionDefaultRecordTypeId;
    accountDefaultRecordTypeId;
    isLoadingInternal = true;

    @track subscriptionOptions;    
    subscriptionScopeTypeOptions;
    userOptions;
    comZoneOptions;
    zoneOptions;
    profitCenterOptions;
    targetAccountTypeOptions;
    operationCount = 0;
    operationSuccessCount = 0;
    showMessage = false;
    messageType;
    message;

    @wire(getObjectInfo, {objectApiName: NOTIFICATION_SUBSCRIPTION_OBJECT})
    getNotificationSubscriptionObjectInfo({error, data}){

        if (data){

            console.log("getNotificationSubscriptionObjectInfo");
            this.subscriptionDefaultRecordTypeId = data.defaultRecordTypeId;       
        }
        else if (error){
            console.log('getNotificationSubscriptionObjectInfo error: ' + JSON.stringify(error));
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
        }   
    }       

    @wire(getObjectInfo, {objectApiName: ACCOUNT_OBJECT})
    getAccountObjectInfo({error, data}){

        if (data){

            console.log("getAccountObjectInfo");
            this.accountDefaultRecordTypeId = data.defaultRecordTypeId;       
        }
        else if (error){
            console.log('getAccountObjectInfo error: ' + JSON.stringify(error));
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
        }   
    }             

    @wire(getPicklistValues, { recordTypeId: "$subscriptionDefaultRecordTypeId", fieldApiName: SUBSCRIPTION_SCOPE_TYPE_FIELD })
    getSubscriptionScopeTypeOptions({error, data}){

        if (data){

            console.log("getSubscriptionScopeTypeOptions");
            let subscriptionScopeTypeOptions = [];

            data.values.forEach(function(option){
                subscriptionScopeTypeOptions.push({label: option.label, value: option.value});
            });

            this.subscriptionScopeTypeOptions = [...subscriptionScopeTypeOptions];
        }
        else if (error){
            console.log('getMyOrderFieldValues error: ' + JSON.stringify(error));
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
        }   
    }

    @wire(getPicklistValues, { recordTypeId: "$accountDefaultRecordTypeId", fieldApiName: COM_ZONE_FIELD })
    getComZoneOptions({error, data}){

        if (data){

            console.log("getComZoneOptions");
            let comZoneOptions = [];

            data.values.forEach(function(option){
                comZoneOptions.push({label: option.label, value: option.value});
            });

            this.comZoneOptions = [...comZoneOptions];
            this.getInit();
        }
        else if (error){
            console.log('getComZoneOptions error: ' + JSON.stringify(error));
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
        }   
    }    

    connectedCallback(){
        console.log("connectedCallback");
        this.getInit();
    }

    getInit(){

        console.log("getInit check");        
        console.log("recordid: " + this.recordid);
        console.log("order: " + this.order);
        console.log("carItem: " + this.carItem);  
        console.log("this.comZoneOptions: " + this.comZoneOptions);      

        if (this.comZoneOptions && ((this.recordid && !this.order && !this.carItem) || (!this.recordid))){

            console.log("proceeding to initliaze..");

            getInit({recordId: this.recordid})
            .then(result => {
                this.handleInitResponse(result);
            })
            .catch(error => {
                let msg = (error && error.body && error.body.message) ? error.body.message : error;
                console.log("getInit error: " + msg);
                this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
                this.isLoadingInternal = false;
            });              
        }        
    }

    handleInitResponse(data){

        console.log("getInit");
        console.log(JSON.stringify(data));

        if (this.recordid){

            if (data.order){
                this.order = data.order;
            }

            if (data.carItem){
                this.carItem = data.carItem;
            }
        }

        this.isPortalUser = data.isPortalUser;
        this.mobilePhone = data.mobilePhone;
        this.currentUserId = data.currentUserId;

        if (this.showSubscriptionScope){

            let userOptions = [];

            data.users.forEach(function(user){
                userOptions.push({label: user.Name, value: user.Id});
            });

            this.userOptions = [...userOptions];

            let profitCenterOptions = [];

            data.profitCenters.forEach(function(profitCenter){
                profitCenterOptions.push({label: profitCenter, value: profitCenter});
            });

            this.profitCenterOptions = [...profitCenterOptions];

            let zoneOptions = [];

            data.zones.forEach(function(zone){
                zoneOptions.push({label: zone, value: zone});
            });

            this.zoneOptions = [...zoneOptions];            
        }

        let subscriptionOptions = [];

        data.subscriptionOptions.forEach(function(subscriptionOption){

            let subscriptionOptionClone = {...subscriptionOption};
            let availableDeliveryMethodOptions = [];

            subscriptionOptionClone.notificationType.Available_Delivery_Methods__c.split(";").forEach(function(deliveryMethod){
                availableDeliveryMethodOptions.push({label: deliveryMethod, value: deliveryMethod});
            });

            subscriptionOptionClone.availableDeliveryMethodOptions = [...availableDeliveryMethodOptions];
            subscriptionOptionClone.selectedDeliveryMethods = subscriptionOption.subscription && subscriptionOption.subscription[DELIVERY_METHODS_FIELD.fieldApiName] ? subscriptionOption.subscription[DELIVERY_METHODS_FIELD.fieldApiName].split(";") : "";

            let selectedDeliveryFrequencies = subscriptionOption.subscription ? subscriptionOption.subscription[DELIVERY_FREQUENCIES_FIELD.fieldApiName].split(";") : "";
            subscriptionOptionClone.dailyEmailDigest = selectedDeliveryFrequencies.indexOf("Daily") > -1;
            subscriptionOptionClone.weeklyEmailDigest = selectedDeliveryFrequencies.indexOf("Weekly") > -1;            

            subscriptionOptionClone.selectedSubscriptionScopeType = subscriptionOption.subscription ? subscriptionOption.subscription[SUBSCRIPTION_SCOPE_TYPE_FIELD.fieldApiName] : "";
            subscriptionOptionClone.selectedSubscriptionScopeValue = subscriptionOption.subscription ? subscriptionOption.subscription[SUBSCRIPTION_SCOPE_VALUE_FIELD.fieldApiName] : "";
                        
            subscriptionOptionClone.showUserSelection = false;
            subscriptionOptionClone.showComZone = false;
            subscriptionOptionClone.showZone = false;
            subscriptionOptionClone.showProfitCenter = false;

            if (USER_SCOPE_TYPES.includes(subscriptionOptionClone.selectedSubscriptionScopeType)){
                subscriptionOptionClone.showUserSelection = true;
            }
            else if (subscriptionOptionClone.selectedSubscriptionScopeType === "COM Zone"){
                subscriptionOptionClone.showComZone = true;
            }
            else if (subscriptionOptionClone.selectedSubscriptionScopeType === "Profit Center"){
                subscriptionOptionClone.showProfitCenter = true;
            }    
            else if (subscriptionOptionClone.selectedSubscriptionScopeType === "Zone"){
                subscriptionOptionClone.showZone = true;
            }            

            subscriptionOptions.push(subscriptionOptionClone);
        }.bind(this));

        this.subscriptionOptions = [...subscriptionOptions];

        this.isLoadingInternal = false;        
    }

    get pageTitle(){

        let carItem = (this.recordid && this.carItem && this.carItem.Id);
        let order = (this.recordid && this.order && this.order.Id);

        return "Notification Subscriptions for " + (carItem ? "Order " + this.order.Order_Number__c + "-" + this.carItem.Load_Code__c : (order ? "Order " + this.order.Order_Number__c : "All New Car Orders"));
    }
    
    handleDailyDigestChange(event){

        let index = event.target.dataset.index;
        console.log("handleDailyDigestChange: (" + index + ")");
        this.subscriptionOptions[index].dailyEmailDigest = !this.subscriptionOptions[index].dailyEmailDigest;                      
    }    

    handleWeeklyDigestChange(event){

        let index = event.target.dataset.index;
        console.log("handleWeeklyDigestChange: (" + index + ")");
        this.subscriptionOptions[index].weeklyEmailDigest = !this.subscriptionOptions[index].weeklyEmailDigest;
    }     

    handleDeliveryMethodChange(event){

        let index = event.detail.index;
        let value = event.detail.value;        
        console.log("handleDeliveryMethodChange: (" + index + ") " + value);           
        this.subscriptionOptions[index].selectedDeliveryMethods = value;
    }

    handleSubscriptionScopeTypeChange(event){

        let index = event.target.dataset.index;
        let value = event.detail.value;        
        console.log("handleSubscriptionScopeTypeChange: (" + index + ") " + value);         
        
        this.subscriptionOptions[index].selectedSubscriptionScopeType = value;
        this.subscriptionOptions[index].selectedSubscriptionScopeValue = "";

        this.subscriptionOptions[index].showUserSelection = false;
        this.subscriptionOptions[index].showComZone = false;
        this.subscriptionOptions[index].showZone = false;
        this.subscriptionOptions[index].showProfitCenter = false;

        if (USER_SCOPE_TYPES.includes(value)){
            this.subscriptionOptions[index].showUserSelection = true;
            this.subscriptionOptions[index].selectedSubscriptionScopeValue = this.currentUserId;  
        }
        else if (value === "COM Zone"){
            this.subscriptionOptions[index].showComZone = true;
        }
        else if (value === "Zone"){
            this.subscriptionOptions[index].showZone = true;
        }        
        else if (value === "Profit Center"){
            this.subscriptionOptions[index].showProfitCenter = true;
        }      
    }

    handleUserChange(event){

        let index = event.target.dataset.index;
        let value = event.detail.value;
        console.log("handleUserChange: (" + index + ") " + value);         
        
        this.subscriptionOptions[index].selectedSubscriptionScopeValue = value;
    }    

    handleComZoneChange(event){

        let index = event.detail.index;
        let value = event.detail.value;
        console.log("handleComZoneChange: (" + index + ") " + value);         
        
        this.subscriptionOptions[index].selectedSubscriptionScopeValue = value;
    }

    handleZoneChange(event){

        let index = event.detail.index;
        let value = event.detail.value;
        console.log("handleZoneChange: (" + index + ") " + value);         
        
        this.subscriptionOptions[index].selectedSubscriptionScopeValue = value;
    }    
    
    handleProfitCenterChange(event){

        let index = event.detail.index;
        let value = event.detail.value;
        console.log("handleProfitCenterChange: (" + index + ") " + value);         
        
        this.subscriptionOptions[index].selectedSubscriptionScopeValue = value;
    }    

    onSubscriptionChange(event){
        let index = event.target.dataset.index;
        console.log("onSubscriptionChange: (" + index + ")");
        this.subscriptionOptions[index].subscribed = !this.subscriptionOptions[index].subscribed;        
    }    

    get showSubscriptionScope(){
        return !this.isPortalUser && !this.recordid;
    }

    onUpdateSubscriptions(event){

        console.log("onUpdateSubscriptions");
        this.isLoadingInternal = true;
        this.message = "";
        this.messageType = "";
        this.showMessage = false;

        // Validations
        let errors;

        this.subscriptionOptions.forEach(function(subscriptionOption){

            if (subscriptionOption.subscribed){

                console.log("Evaluating: " + JSON.stringify(subscriptionOption));

                if (subscriptionOption.selectedDeliveryMethods && subscriptionOption.selectedDeliveryMethods.indexOf('SMS') > -1 && this.mobilePhone == ''){
                    let message = "You have subscribed for SMS notifications on Notification Event '" + subscriptionOption.notificationType.Description__c + "' but do not have a mobile phone number on record.  Please update your mobile phone" + (this.isPortalUser ? " <a href='/ClubCarLinks/CC_PavilionMyProfile'>here</a> " : " ") + "and try again.";
                    errors = !errors ? message : errors + "\n" + message;
                }              

                if (this.showSubscriptionScope && (!subscriptionOption.selectedSubscriptionScopeType || subscriptionOption.selectedSubscriptionScopeType === "")){

                    let message = "You must select a Subscription Scope for Notification Event '" + subscriptionOption.notificationType.Description__c + "'.";
                    errors = !errors ? message : errors + "\n" + message;
                }          
                
                if (this.showSubscriptionScope && subscriptionOption.selectedSubscriptionScopeType && !SUBSCRIPTION_SCOPE_VALUE_REQ_EXCLUSIONS.includes(subscriptionOption.selectedSubscriptionScopeType) && (!subscriptionOption.selectedSubscriptionScopeValue || subscriptionOption.selectedSubscriptionScopeValue === "")){

                    let message = "You must select a value for " + subscriptionOption.selectedSubscriptionScopeType + " for Notification Event '" + subscriptionOption.notificationType.Description__c + "'.";                                            
                    errors = !errors ? message : errors + "\n" + message;
                }

                if (!subscriptionOption.dailyEmailDigest && !subscriptionOption.weeklyEmailDigest && subscriptionOption.selectedDeliveryMethods === ""){

                    let message = "You must select a Delivery Option for Notification Event '" + subscriptionOption.notificationType.Description__c + "'.";
                    errors = !errors ? message : errors + "\n" + message;
                }                                 

                console.log("errors: " + errors);
            }

        }.bind(this));

        if (errors){
            this.showToast("Error(s) found while updating subscrptions.", errors, "error");
            this.isLoadingInternal = false;
        }
        else{

            console.log("no errors, proceeding to update subscriptions..");
            console.log(JSON.stringify(this.subscriptionOptions));

            // Keep track of the expected number of operations so a success prompt can be displayed after all have completed successfully
            this.operationCount = 0;
            this.operationSuccessCount = 0;

            this.subscriptionOptions.forEach(function(subscriptionOption){

                if (!subscriptionOption.subscribed && subscriptionOption.subscription && subscriptionOption.subscription.Id){
                    this.operationCount++;
                }
                else if (subscriptionOption.subscribed && subscriptionOption.subscription && subscriptionOption.subscription.Id){
                    this.operationCount++;
                }
                else if (subscriptionOption.subscribed && (!subscriptionOption.subscription || !subscriptionOption.subscription.Id)){
                    this.operationCount++;
                }                
            }.bind(this));            

            this.subscriptionOptions.forEach(function(subscriptionOption){

                if (!subscriptionOption.subscribed && subscriptionOption.subscription && subscriptionOption.subscription.Id){
                    this.deleteSubscription(subscriptionOption.subscription.Id);
                }
                else if (subscriptionOption.subscribed && subscriptionOption.subscription && subscriptionOption.subscription.Id){
                    this.updateSubscription(subscriptionOption);
                }
                else if (subscriptionOption.subscribed && (!subscriptionOption.subscription || !subscriptionOption.subscription.Id)){
                    this.createSubscription(subscriptionOption);
                }                
            }.bind(this));

            console.log("final operationCount: " + this.operationCount);

            if (this.operationCount === 0){
                this.showToast(null, "Notification subscriptions updated successfully.", "success");
                this.isLoadingInternal = false;
                this.dispatchEvent(new CustomEvent("close", {detail: {recordid: this.recordid}}));
            }
        }
    }

    onOperationSuccess(){

        this.operationSuccessCount++;
        console.log("operationSuccessCount: " + this.operationSuccessCount + ", operationCount: " + this.operationCount);

        if (this.operationSuccessCount == this.operationCount){

            this.showToast(null, "Notification subscriptions updated successfully.", "success");

            // Reinitialize the page
            getInit({recordId: this.recordid})
            .then(result => {
                this.handleInitResponse(result);
                this.dispatchEvent(new CustomEvent("close", {detail: {recordid: this.recordid}}));
            })
            .catch(error => {
                let msg = (error && error.body && error.body.message) ? error.body.message : error;
                console.log("getInit error: " + msg);
                this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
                this.isLoadingInternal = false;
            });                   
        }
    }

    onCancel(event){
        this.dispatchEvent(new CustomEvent("close", {detail: {recordid: this.recordid}}));
    }

    deleteSubscription(recordid){

        deleteRecord(recordid)
        .then(() => {            
            console.log("deleteSubscription success");
            this.onOperationSuccess();
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("deleteSubscription error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoadingInternal = false;
        });        
    }    

    updateSubscription(subscriptionOption){

        const fields = {};

        fields["Id"] = subscriptionOption.subscription.Id;

        if (this.recordid && this.carItem && this.carItem.Id){
            fields[CAR_ITEM_FIELD.fieldApiName] = this.recordid;
        }
        else if (this.recordid && this.order && this.order.Id){
            fields[ORDER_FIELD.fieldApiName] = this.recordid;
        }
              
        fields[SUBSCRIPTION_SCOPE_TYPE_FIELD.fieldApiName] = subscriptionOption.selectedSubscriptionScopeType;
        fields[SUBSCRIPTION_SCOPE_VALUE_FIELD.fieldApiName] = subscriptionOption.selectedSubscriptionScopeValue;        
        fields[DELIVERY_METHODS_FIELD.fieldApiName] = subscriptionOption.selectedDeliveryMethods;
        fields[DELIVERY_FREQUENCIES_FIELD.fieldApiName] = this.buildFrequencies(subscriptionOption);

        const recordInput = { fields };

        updateRecord(recordInput)
        .then(() => {
            console.log("updateSubscription success");
            this.onOperationSuccess();
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("updateSubscription error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoadingInternal = false;
        });        
    }    

    createSubscription(subscriptionOption){

        const fields = {};

        if (this.recordid && this.carItem && this.carItem.Id){
            fields[CAR_ITEM_FIELD.fieldApiName] = this.recordid;
        }
        else if (this.recordid && this.order && this.order.Id){
            fields[ORDER_FIELD.fieldApiName] = this.recordid;
        }
        
        fields[SUBSCRIPTION_SCOPE_TYPE_FIELD.fieldApiName] = subscriptionOption.selectedSubscriptionScopeType;
        fields[NOTIFICATIONS_TYPE_FIELD.fieldApiName] = subscriptionOption.notificationType.Id;
        fields[SUBSCRIPTION_SCOPE_TYPE_FIELD.fieldApiName] = subscriptionOption.selectedSubscriptionScopeType;
        fields[SUBSCRIPTION_SCOPE_VALUE_FIELD.fieldApiName] = subscriptionOption.selectedSubscriptionScopeValue;
        fields[DELIVERY_METHODS_FIELD.fieldApiName] = subscriptionOption.selectedDeliveryMethods;
        fields[DELIVERY_FREQUENCIES_FIELD.fieldApiName] = this.buildFrequencies(subscriptionOption);

        const recordInput = { apiName: NOTIFICATION_SUBSCRIPTION_OBJECT.objectApiName, fields };
console.log(JSON.stringify(recordInput));
        createRecord(recordInput)
        .then(() => {
            console.log("createSubscription success");
            this.onOperationSuccess();
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("createSubscription error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoadingInternal = false;
        });        
    }     

    buildFrequencies(subscriptionOption){

        let frequencies;

        if (subscriptionOption.selectedDeliveryMethods.length > 0){
            frequencies = 'Per Occurrence';
        }

        if (subscriptionOption.dailyEmailDigest){
            
            if (frequencies){
                frequencies += ';Daily';
            }
            else{
                frequencies = 'Daily';
            }
        }

        if (subscriptionOption.weeklyEmailDigest){
            
            if (frequencies){
                frequencies += ';Weekly';
            }
            else{
                frequencies = 'Weekly';
            }
        } 
        
        return frequencies;
    }

    showToast(title, message, variant){

        if (this.isPortalUser){            
            this.message = message;
            this.messageType = variant;
            this.showMessage = true;
        }
        else{
            this.dispatchEvent(new CustomEvent("showtoast", {detail: {duration: 7000, "title": title, "variant": variant, "message": message}}));
        }
    }

    get isLoading(){
        return this.isLoadingInternal;
    }

    get deliveryMethodClasses(){
        return this.isPortalUser ? 'slds-p-bottom_xx-small' : 'slds-p-top_medium slds-p-bottom_xx-small';
    }

    get dropdownAlignment(){
        return this.isPortalUser ? 'bottom-left' : 'auto';
    }

    /**
     * @param {boolean} val
     */
    set isLoading(val){

        console.log("setIsLoading: " + val);
        this.isLoadingInternal = val;
        this.dispatchEvent(new CustomEvent("isloadingchange", {detail: {isLoading: this.isLoading}}));
    }

    get notificationsImageURL(){
        return CC_Load_Tracking_Resources + "/images/alerts.png";
    }
}