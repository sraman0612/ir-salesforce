import { LightningElement, api, track} from 'lwc';

export default class MultiSelectComboBox extends LightningElement {

    @api index;
    @api initialValue;
    @api label;
    @api options;
    @track optionsDisplay;
    @api placeholder;
    isExpanded = false;

    connectedCallback(){
        console.log("connectedCallback");
        this.buildOptionsDisplay();
    }

    get comboboxClasses(){

        let classes = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ";
        
        if (this.isExpanded){
            classes += "slds-is-open";
        }

        return classes;
    }

    get inputClasses(){

        let classes = "slds-input slds-combobox__input slds-combobox__input-value ";

        if (this.isExpanded){
            classes += "slds-has-focus";
        }

        return classes;
    }

    @api
    get value(){
        
        let value;

        if (this && this.optionsDisplay){

            this.optionsDisplay.forEach(function(option){
                
                if (option.isSelected){
                   value = value ? value + ";" + option.value : option.value;
                }
            });
        }

        return value ? value : "";        
    }

    @api
    updateSelections(values){

        console.log("updateSelections: " + values);
  
        if (this && this.optionsDisplay && values != null){

            let selections = values.split(";");
            let options = [];
            
            this.optionsDisplay.forEach(function(option){

                let optionClone = {...option};
                optionClone.isSelected = selections.includes(optionClone.value);
                options.push(optionClone);
            }.bind(this));

            this.optionsDisplay = [...options];
        }        
    }

    buildOptionsDisplay(){        

        if (this && this.options && !this.optionsDisplay){

            let options = [];
            
            this.options.forEach(function(option){

                let optionClone = {...option};
          
                if (this.initialValue && this.initialValue.indexOf(optionClone.value) > -1){
                    optionClone.isSelected = true;
                }
                else{
                    optionClone.isSelected = false;
                }

                options.push(optionClone);
            }.bind(this));

            this.optionsDisplay = [...options];
        }        
    }

    onComponentClick(event){

        event.stopPropagation();
        console.log("onComponentClick");
        this.isExpanded = !this.isExpanded;
    }  

    onFocusOut(event){

        event.stopPropagation();
        console.log("onFocusOut");
        this.isExpanded = false;
    }

    onOptionClick(event){

        event.stopPropagation();

        let index = event.currentTarget.dataset.index;
        let option = {...this.optionsDisplay[index]};
        console.log("onOptionClick: " + JSON.stringify(option));  
       
        option.isSelected = !option.isSelected;

        this.optionsDisplay[index] = {...option};

        this.dispatchEvent(new CustomEvent("valuechange", {detail: {value: this.value, index: this.index}}));
    }
}