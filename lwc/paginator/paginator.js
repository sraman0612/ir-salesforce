import { LightningElement, api} from 'lwc';

export default class Paginator extends LightningElement {

    @api pageSize;
    @api showFirstPageButton = false;
    @api showLastPageButton = false;    
    
    page = 1;
    offSet = 0;
    recordCount;   

    get hasPrevious(){
        return this.page > 1;
    }

    get hasNext(){
        return this.page < this.totalPages;
    } 

    get recordsInDisplay(){

        if (this.totalRecordCount > 0){           
            return (this.offSet > 0 ? this.offSet + 1 : 1) + "-" + (this.totalRecordCount < this.pageSize*this.page ? this.totalRecordCount : this.pageSize*this.page) + " of " + this.totalRecordCount;
        }
        else{
            return "0 of 0";
        }     
    }

    @api
    get totalRecordCount(){
        return this.recordCount;
    }

    set totalRecordCount(val){
        
        this.recordCount = val;
        
        this.dispatchEvent(new CustomEvent(
            "initialize",
            {"detail" : {
                totalPages : this.totalPages
            }}
        ));        
    }    
    
    firstPage() {

        this.page = 1;
        this.offSet = 0;
        this.dispatchPageChangeEvent();
    }    

    lastPage() {

        this.page = this.totalPages;
        this.offSet = (this.totalPages - 1) * this.pageSize;
        this.dispatchPageChangeEvent();
    }       

    previous() {

        if (this.page > 1){
            this.page--;
            this.offSet = this.offSet - parseInt(this.pageSize);
            this.dispatchPageChangeEvent();
        }
    }

    next() {

        this.page++;
        this.offSet = this.offSet + parseInt(this.pageSize);
        this.dispatchPageChangeEvent();
    }

    get totalPages(){
        return Math.floor(this.totalRecordCount / parseInt(this.pageSize)) + ((this.totalRecordCount % parseInt(this.pageSize)) > 0 ? 1 : 0);
    }

    get showPaginatorButtons(){
        return this.totalPages > 1;
    }

    get pageSizeOptions(){

        let options = [
            {id: "5", label:"5", value:"5"},
            {id: "10", label:"10", value:"10"},
            {id: "20", label:"20", value:"20"},
            {id: "50", label:"50", value:"50"}
        ];
        
        options.forEach(function(option){

             if (option.value == this.pageSize){
                 option.checked = true;
             }
         }, this);
        
        return options;
    }

    onPageSizeClick(event){

        if (this.pageSize != event.target.value){

            this.pageSize = event.target.value;
            this.dispatchPageSizeChangeEvent();
        }
    }

    dispatchPageSizeChangeEvent(){

        this.dispatchEvent(new CustomEvent(
            "pagesizechange",
            {"detail" : {
                pageSize : this.pageSize,
                totalPages : this.totalPages
            }}
        ));       
    }    

    dispatchPageChangeEvent(){
        this.dispatchEvent(new CustomEvent(
            "pagechange",
            {"detail" : {
                page : this.page,
                offSet : this.offSet
            }}
        ));
    }
}