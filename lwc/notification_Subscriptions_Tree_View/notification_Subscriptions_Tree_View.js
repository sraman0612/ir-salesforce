import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CTS_IOT_C0MMUNITY_RESOURCES from '@salesforce/resourceUrl/CTS_IOT_Community_Resources';
import getInit from '@salesforce/apex/Notification_Subscriptions_Tree_View_Ctr.getInit';

const UNEXPECTED_ERROR_TITLE = "An unexpected error occurred."

export default class Notification_Subscriptions_Tree_View extends NavigationMixin(LightningElement) {

    isLoading = true;
    showHelixDisabled = false;
    @track treeItems = [];

    connectedCallback(){

        console.log("notification_Subscriptions_Tree_View.connectedCallback");   
        this.getInit();      
    }

    get communityStaticResourceURL(){
        return CTS_IOT_C0MMUNITY_RESOURCES;
    }

    get titleIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/Preventative-Predictive_Red&Gray.png';
    }

    get lightBulbIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/EfficientEnergyUse_Red&Gray.png';
    }

    get subscribedIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/CARE_Red&Gray.png';
    } 
    
    get unsubscribedIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/IncreasedUptime_Gray.png';
    }
    
    get subscriptionsDisabledIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/NoPaperworkHassle-Red&Gray.png';
    }         

    getInit(){

        this.isLoading = true;

        getInit({showHelixDisabled: this.showHelixDisabled})
        .then(result => {
            console.log("getInit result: " + JSON.stringify(result));

            let treeItems = [
                {
                    label: 'All My Sites',
                    value: "",
                    title: "Manage Subscriptions For All My Sites",
                    subscribed: result.hasGlobalSubscriptions,
                    variant: result.hasGlobalSubscriptions ? "success" : "",
                    sites: result.sites
                }
            ];

            this.treeItems = [...treeItems];
            this.isLoading = false;
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("getInit error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoading = false;
        });        
    }

    toggleSiteBranch(event){

        let index = event.target.dataset.index;
        console.log("toggleSiteBranch: " + index);

        console.log("before: " + JSON.stringify(this.treeItems));

        let treeItems = this.treeItems;
        treeItems[0].sites[index].expanded = !treeItems[0].sites[index].expanded;

        this.treeItems = [...treeItems];

        console.log("after: " + JSON.stringify(this.treeItems));
    }

    navigateToRecordDetails(event){

        let recordid = event.target.dataset.recordid;
        console.log("navigateToRecordDetails: " + recordid);

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordid,
                actionName: "view"
            }
        });
    }

    manageSubscriptionsFA(event){

        let val = event.detail.val;
        console.log("manageSubscriptionsFA: " + val);

        this.manageSubscriptions({target : {dataset : {value: val}}});
    }

    manageSubscriptions(event){

        let value = event.target.dataset.value;
        console.log("manageSubscriptions: " + value);

        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: "Manage_Notification_Subscriptions__c"
            },
            state: {
                recordId: value
            }            
        });    
        
        event.stopPropagation();
    }

    onShowHelixDisabled(event){
        this.showHelixDisabled = true;
        this.getInit();
    }    

    onHideHelixDisabled(event){
        this.showHelixDisabled = false;
        this.getInit();
    }

    showToast(title, message, variant){
        
        let event = new ShowToastEvent({
            "variant": variant,
            "title": title,
            "message": message,
            duration: 10000
        });

        this.dispatchEvent(event);        
    }    
}