trigger ProductEvents on Product2 (before insert, before update, before delete,
                                  after insert, after update, after delete, after undelete) {
  ProductEventHandler peh = new ProductEventHandler(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
  if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
    } else if (Trigger.isUpdate) {  //BEFORE UPDATE
      peh.updateLocationInventory();
    } else if (Trigger.isDelete) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert) {  // AFTER INSERT
     peh.createStandardPriceBookEntry();
    } else if (Trigger.isUpdate) {  // AFTER UPDATE
    } else if (Trigger.isDelete) {  // AFTER DELETE
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
  }
}