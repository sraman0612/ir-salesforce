//
// (c) 2015, Appirio Inc.
//
// Opportunity_Line_Item_Trigger Trigger
//
// April 17,2015    Surabhi Sharma    T-377640(delete the record)
//
trigger Opportunity_Line_Item_Trigger on Opportunity_Line_Item__c (after insert, after update, after delete) {
    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Opportunity_Line_Item__c');
    if (trigger.isAfter)
    {
        if (trigger.isInsert && !softDisable.insertDisabled())
            Opportunity_Line_Item_TriggerHandler.afterInsert(trigger.new);
        else if (trigger.isUpdate && !softDisable.updateDisabled())
            Opportunity_Line_Item_TriggerHandler.afterUpdate(trigger.new, trigger.oldMap);    //record approved for deletion is deleted
        else if (trigger.isDelete && !softDisable.deleteDisabled())
            Opportunity_Line_Item_TriggerHandler.afterDelete(trigger.old);
    }
}