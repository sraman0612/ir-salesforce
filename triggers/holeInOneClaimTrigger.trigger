trigger holeInOneClaimTrigger on Hole_In_One_Claim__c(before insert, before update, before delete,
                                            after insert, after update, after delete, after undelete) {
   if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
    } else if (Trigger.isUpdate) {  //BEFORE UPDATE 
        holeInOneRequestTriggerHandler.createOrderAndOrderitemForHIOC(trigger.new);
        holeInOneRequestTriggerHandler.submitAndProcessApprovalRequestHIO(trigger.new);
    } else if (Trigger.isDelete) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert) { // AFTER INSERT
        holeInOneRequestTriggerHandler.submitAndProcessApprovalRequestHIO(trigger.new);
    } else if (Trigger.isUpdate) { // AFTER UPDATE
    } else if (Trigger.isDelete) {  // AFTER DELETE
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
  }
}