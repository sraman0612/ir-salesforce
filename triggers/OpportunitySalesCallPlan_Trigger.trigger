// OpportunitySalesCallPlan_Trigger
//
// Created on April 16, 2019 by Ronnie Stegall


trigger OpportunitySalesCallPlan_Trigger on Opportunity_Sales_Call_Plan__c (before insert, before update, after insert) {
    List<Opportunity_Sales_Call_Plan__c> salesCallPlansNew = new List<Opportunity_Sales_Call_Plan__c>();
    List<Opportunity_Sales_Call_Plan__c> salesCallPlansOld = new List<Opportunity_Sales_Call_Plan__c>();
    
    if(null!=trigger.new)
    {
        for (Opportunity_Sales_Call_Plan__c a : trigger.new)
        {
            salesCallPlansNew.add(a);
        }
    }
    
    if(null!=trigger.old)
    {
        for (Opportunity_Sales_Call_Plan__c a : trigger.old)
        {
            salesCallPlansOld.add(a);
        }
    }
    
    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Opportunity_Sales_Call_Plan__c');
    
    if(trigger.isBefore)
    {    
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            if(!salesCallPlansNew.isEmpty())
            {
                OpportunitySalesCallPlan_TriggerHandler.beforeUpdate(salesCallPlansNew);
            }    
        } 
        
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            if(!salesCallPlansNew.isEmpty())
            {
                OpportunitySalesCallPlan_TriggerHandler.beforeInsert(salesCallPlansNew);
            }    
        }
        
    }
    
    if(trigger.isAfter)
    {  
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            OpportunitySalesCallPlan_TriggerHandler.afterInsert(salesCallPlansNew);
        }
    }
    
}