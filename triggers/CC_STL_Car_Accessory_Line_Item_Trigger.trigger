trigger CC_STL_Car_Accessory_Line_Item_Trigger on CC_STL_Car_Accessory_Line_Item__c (before update, before insert, after update, before delete, after insert, after delete, after undelete) {

    if (Trigger.isBefore){
    
        if (Trigger.isUpdate){
            CC_STL_Car_Accessory_Line_Item_Handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
        else if (Trigger.isInsert){
            CC_STL_Car_Accessory_Line_Item_Handler.onBeforeInsert(Trigger.new);
        }
        else if (Trigger.isDelete){
            CC_STL_Car_Accessory_Line_Item_Handler.onBeforeDelete(Trigger.old);
        }     
    }
    else if (Trigger.isAfter){
    
        if (Trigger.isUpdate){
            CC_STL_Car_Accessory_Line_Item_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
        else if (Trigger.isInsert){
            CC_STL_Car_Accessory_Line_Item_Handler.onAfterInsert(Trigger.new);
        }
        else if (Trigger.isDelete){
            CC_STL_Car_Accessory_Line_Item_Handler.onAfterDelete(Trigger.old);            
        }
        else if (Trigger.isUndelete){
            CC_STL_Car_Accessory_Line_Item_Handler.onAfterUndelete(Trigger.new);
        }
    }    
}