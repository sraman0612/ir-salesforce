trigger CTS_FeedCommentTrigger on FeedComment (before insert,after insert,before update,after update,before delete,after delete,after undelete) {
    TriggerDispatcher.run(new FeedCommentTriggerHandler(), 'CTS_FeedCommentTrigger');
}