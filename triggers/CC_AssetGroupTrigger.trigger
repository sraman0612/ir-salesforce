trigger CC_AssetGroupTrigger on CC_Asset_Group__c (before insert, before update, after insert, after update) {
	
  	Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Asset Group');
  
  	if (Trigger.isBefore){  // ALL BEFORE EVENTS
    
	    if (Trigger.isInsert && !softDisable.insertDisabled()) {  //BEFORE INSERT
	    	CC_AssetGroupTriggerHandler.beforeInsert(trigger.new);
	    } 
	    else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  //BEFORE UPDATE 
	    	CC_AssetGroupTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
	    } 
	    else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // BEFORE DELETE
	    
	    }
  	} 
  	else {  // ALL AFTER EVENTS
    
    	if (Trigger.isInsert && !softDisable.insertDisabled()) {  // AFTER INSERT
      		CC_AssetGroupTriggerHandler.afterInsert(trigger.new);
    	} 
    	else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  // AFTER UPDATE
      		CC_AssetGroupTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
    	} 
    	else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // AFTER DELETE
    	
    	} 
    	else if (Trigger.isUndelete && !softDisable.undeleteDisabled()) {  // AFTER UNDELETE
    
    	}
  	}
}