trigger AccountTrigger on Account (before insert, after insert, before update, 
                                   after update,after delete) {  
                                       
	//by mk
    if(Trigger.isUpdate && Trigger.isAfter){
        
    
    List<String> reqFields = new List<String>();         
    Map <String,Schema.SObjectType> gd = Schema.getGlobalDescribe();         
    Schema.SObjectType sobjType = gd.get('Account');         
    Schema.DescribeSObjectResult r = sobjType.getDescribe();         
    Map<String, Schema.SObjectField> MapofField = r.fields.getMap();
    Map<Id,Account> mapNewAccountById = Trigger.NewMap;
    Map<Id,Account> mapAccountById = Trigger.oldMap;
    String strFieldName;
        for(Account objAccount : mapNewAccountById.values()){
            strFieldName='';
            for(String fieldName : MapofField.keySet()) {
                if(objAccount.get(fieldName) != mapAccountById.get(objAccount.Id).get(fieldName)){
                    strFieldName += fieldName+',';
                }
            }
            System.debug('===== strFieldName: '+strFieldName);
        }
    }
    // till this by mk                                       
                                       
                                       

                                       
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Account');  
     //Merged Triggers as per SGOM 144.
     if(skipTrigger!=True){                    
    switch on Trigger.operationType {
                        When BEFORE_INSERT{  
                                EMEIA_AccountTriggerHandler.beforeInsert(trigger.new);                                                                                                                         
                        }
                        When BEFORE_UPDATE{                                             
                                EMEIA_AccountTriggerHandler.beforeUpdate(trigger.new,Trigger.old,Trigger.oldmap);                                                                 
                        }
                        When AFTER_INSERT{                                              
                                EMEIA_AccountTriggerHandler.afterInsert(trigger.new);                                                                                                                         
                        }
                        When AFTER_UPDATE{                                              
                                EMEIA_AccountTriggerHandler.afterUpdate(trigger.new,trigger.oldmap,trigger.newmap);
                        }
                        When AFTER_DELETE{                                              
                                EMEIA_AccountTriggerHandler.afterDelete(trigger.old);                                
                        }
                }
     }
}