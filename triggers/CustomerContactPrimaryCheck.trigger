//Created on 24/02/2020
// CustomerContactPrimaryCheck
// This triggers prevents multiple primary Customer Contacts on one Account 
trigger CustomerContactPrimaryCheck on Contact (before insert , before update) 
{
    if(trigger.isBefore && trigger.isInsert){
        CustomerContactPrimaryCheck.preventCreatePrimaryContactOnInsert(trigger.new);
        CustomerContactPrimaryCheck.updateFirstContactAsPrimaryContact(trigger.new);
    }
    if(trigger.isBefore && trigger.isUpdate){
        CustomerContactPrimaryCheck.preventPrimaryContactOnUpdate(trigger.newMap,trigger.oldMap);
    }
}