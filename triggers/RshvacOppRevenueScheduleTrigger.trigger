trigger RshvacOppRevenueScheduleTrigger on Opportunity_Revenue_Schedule__c (before insert, before update, after insert, after update) {

    RshvacOppRevenueScheduleTriggerHandler handler = new RshvacOppRevenueScheduleTriggerHandler();
    
    if (Trigger.isBefore){
      handler.bulkBefore(Trigger.New);
      if (Trigger.isInsert){
          for(Opportunity_Revenue_Schedule__c ors: Trigger.new) {
              System.debug('Jaya before beforeinsert');
              handler.updatePriorYearSales(ors);
          }  
       }
       if (Trigger.isUpdate){
          for(Opportunity_Revenue_Schedule__c ors: Trigger.new) {
            Opportunity_Revenue_Schedule__c oldOrs = new Opportunity_Revenue_Schedule__c();
            oldOrs = (Opportunity_Revenue_Schedule__c)Trigger.oldMap.get(ors.Id);
          //  handler.updatePriorYearSales(ors); Commented for Case-24336 by Ashwini on 24-NON-17
          }  
       }
      // Call the bulk after to handle any caching of data and enable bulkification
      handler.bulkAfter();
      if (Trigger.isInsert){
        // Iterate through the records inserted passing them to the handler.
        for (Opportunity_Revenue_Schedule__c so : Trigger.new){
            handler.afterInsert(so);
        }
      } else if (Trigger.isUpdate){
        // Iterate through the records updated passing them to the handler.
        for (Opportunity_Revenue_Schedule__c so : Trigger.old){
          handler.afterUpdate(so, (Opportunity_Revenue_Schedule__c) Trigger.newMap.get(so.Id));
        }
      }
    }
    // Perform any post processing
    handler.andFinally();
}