//
// (c) 2015, Appirio Inc.
//
// August 27,2015    Siddharth Varshneya    T-429865
//
trigger AccountTeamTrigger on Account_Team__c (before Insert,  before Update, after Delete) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            AccountTeam_TriggerHandler.beforeInsert(trigger.new);
        }
        if(trigger.isUpdate){
            AccountTeam_TriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
        }
    }
    if(trigger.isAfter){
        if(trigger.isDelete){
            AccountTeam_TriggerHandler.afterDelete(Trigger.old); 
        }
    }

}