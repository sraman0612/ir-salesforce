trigger PT_DeleteOppRelated on PT_Parent_Opportunity__c (before delete) {
    
    List<Opportunity> oppRelated = [select Id from Opportunity where PT_Parent_Opportunity__c in :Trigger.old];
    if(oppRelated.size()>0){
        delete oppRelated;
    }
}