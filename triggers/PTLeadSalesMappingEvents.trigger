trigger PTLeadSalesMappingEvents on PT_Lead_Sales_Mapping__c (before insert, before update) {
  Map<String, Id> salesUserMap = new Map<String, Id>();
  for(PT_Lead_Sales_Mapping__c lsm : trigger.new){
    if(null==trigger.oldMap || lsm.Sales__c!=trigger.oldMap.get(lsm.Id).Sales__c){
      salesUserMap.put(lsm.Sales__c.toLowerCase(),null);
    }
  }
  if(!salesUserMap.isEmpty()){
    for(User u: [SELECT ID, Name FROM User WHERE Profile.Name LIKE 'PT%']){
      if(salesUserMap.keyset().contains(u.Name.toLowerCase())){
        salesUserMap.put(u.Name.toLowerCase(), u.Id);
      }
    }
  }
  for(PT_Lead_Sales_Mapping__c lsm : trigger.new){
    if(salesUserMap.keyset().contains(lsm.Sales__c.toLowerCase()) && null!=salesuserMap.get(lsm.Sales__c.toLowerCase())){
      lsm.User__c = salesUserMap.get(lsm.Sales__c.toLowerCase());
    }
    if(trigger.isInsert){lsm.Name = lsm.Country__c;}
  }
}