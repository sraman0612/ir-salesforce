trigger Accountcurrency on Contact(before insert) {
  Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contact');
  if (!softDisable.insertDisabled()) { 
    Set<Id> accIds=new Set<Id>();
    for (Contact cc : trigger.new) {
      if(cc.RecordTypeId == '012j0000000n3f7AAA'){accIds.add(cc.AccountId);}
    }
    List<Account> accs=[select id, OwnerId,currencyisocode from Account where id in :accIds];
    Map<Id, Account> accById=new Map<Id, Account>();
    accById.putall(accs);
    for (Contact cc : trigger.new){
      if(cc.RecordTypeId == '012j0000000n3f7AAA' && null != cc.AccountId && null != accById.get(cc.AccountId).currencyisocode ){
          cc.currencyisocode=accById.get(cc.AccountId).currencyisocode;
      }
    }
  }
}