trigger ProductConsumedTrigger on ProductConsumed (after insert, after update) {

    if (Trigger.isAfter){

        if (Trigger.isInsert){
            CC_ProductConsumedTriggerHandler.afterInsert(Trigger.new);
        }
        else if (Trigger.isUpdate){
            CC_ProductConsumedTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}