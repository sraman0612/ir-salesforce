/*****************************************************************
Name:            RS_CRS_Contact_Trigger
Handler Class:   RS_CRS_Contact_TriggerHandler
Purpose:         trigger Used for preventing Contact Deletion of It is related To any Case. 
History                                                            
-------                                                            
VERSION    AUTHOR      DATE      DETAIL
1.0      Neela Prasad  02/02/2018    INITIAL DEVELOPMENT
*****************************************************************/
trigger RS_CRS_Contact_Trigger on Contact (before Delete) {
    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contact');
    if (!softDisable.deleteDisabled()) { 
        //Checking Event is After or not.
        if(Trigger.isBefore){ 
            //Checking Event is Insert or not.
            if(Trigger.isdelete){
                // Calling method[CheckingcaseOnContact] from handler Class[RS_CRS_Contact_TriggerHandler].
                RS_CRS_Contact_TriggerHandler.CheckingcaseOnContact(Trigger.old);
                // Calling method[CaseOnContactRelatedList] from handler Class[RS_CRS_Contact_TriggerHandler].
                RS_CRS_Contact_TriggerHandler.CaseOnContactRelatedList(Trigger.old);
            }
        }
    }
}