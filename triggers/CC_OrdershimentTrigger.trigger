trigger CC_OrdershimentTrigger on CC_Order_Shipment__c (After Delete) {

    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Ordershipment');
    if(trigger.isAfter && trigger.isDelete && !softDisable.deleteDisabled()){
        CC_OrderShimpmentTriggerHandler.deleteAssets(trigger.old);
    }

}