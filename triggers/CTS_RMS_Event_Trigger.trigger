trigger CTS_RMS_Event_Trigger on CTS_RMS_Event__c(after insert, after update) {

    Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();

    if (notificationSettings.RMS_Alerts_Trigger_Enabled__c){

        if (Trigger.isAfter){
            CTS_RMS_Event_TriggerTest.processNotificationSubscriptions = true;
            NotificationQueuingService.processNotificationSubscriptions(Trigger.newMap, Trigger.oldMap);
        }
    }
}