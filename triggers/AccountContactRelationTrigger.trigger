trigger AccountContactRelationTrigger on AccountContactRelation (after delete) {

    Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();

    if (notificationSettings.Account_Contact_Relation_Trigger_Enabled__c){

        AccountContactRelationTriggerHandler handler = new AccountContactRelationTriggerHandler(Trigger.newMap, Trigger.oldMap);

        if (Trigger.isAfter){

            if (Trigger.isDelete){
                handler.onAfterDelete();
            }
        }
    }
}