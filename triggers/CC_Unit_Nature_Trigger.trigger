trigger CC_Unit_Nature_Trigger on CC_Unit_Nature__c (after insert, after update, after delete) {
    
    if (Trigger.isAfter){
    	
    	if (Trigger.isInsert){
    		CC_Unit_Nature_TriggerHandler.onAfterInsert(trigger.new);
    	}
    	else if (Trigger.isUpdate){
    		CC_Unit_Nature_TriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
    	}
    	else if (Trigger.isDelete){
    		CC_Unit_Nature_TriggerHandler.onAfterDelete(trigger.old);
    	}
    }
}