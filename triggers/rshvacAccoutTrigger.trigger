trigger rshvacAccoutTrigger on Account (before insert, after insert, before update, after update) {
  /** S.Colman 3/28/2016
   * Changed the handling of the trigger to follow a similar approach to this:
   * http://meltedwires.com/2013/06/05/trigger-pattern-for-tidy-streamlined-bulkified-triggers-revisited/
   * It's not exactly the same, but it'll help bulkify and keep the logic more in order
   */
  
  Id rshvacId = RshvacAccountTriggerHandler.RS_HVAC_RT_ID;
  List<Account> rshvacAccounts = new List<Account>();
  // Filter the records by the RS_HVAC
  for (Account a : Trigger.new){
    if (a.RecordTypeId == rshvacId){
      rshvacAccounts.add(a);
    }
  }
    // Check if there are accounts to process
  if (!rshvacAccounts.isEmpty()){
    // Get the handler for the trigger
    RshvacAccountTriggerHandler handler = new RshvacAccountTriggerHandler();
    if (Trigger.isBefore){
      // Call the bulk before to handle any caching of data and enable bulkification
      handler.bulkBefore();
      if (Trigger.isInsert) {
        // Iterate through the records to be inserted passing them to the handler.
        for (Account so : rshvacAccounts){
            handler.beforeInsert(so);
        }
      } else if (Trigger.isUpdate) {
        // Iterate through the records to be updated passing them to the handler.
        for (Account so : rshvacAccounts){
            handler.beforeUpdate((Account) Trigger.oldMap.get(so.Id), so);
        }
      }
    } else if (Trigger.isAfter){
      // Call the bulk after to handle any caching of data and enable bulkification
      handler.bulkAfter();
      if (Trigger.isInsert){
        // Iterate through the records inserted passing them to the handler.
        for (Account so : rshvacAccounts){
            handler.afterInsert(so);
        }
      } else if (Trigger.isUpdate){
        // Iterate through the records updated passing them to the handler.
        for (Account so : rshvacAccounts){
          handler.afterUpdate((Account) Trigger.oldMap.get(so.Id), so);
        }
      }
    }
    // Perform any post processing
    handler.andFinally();
  }
}