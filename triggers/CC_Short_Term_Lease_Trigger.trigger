trigger CC_Short_Term_Lease_Trigger on CC_Short_Term_Lease__c (before insert, before update, after update, before delete) {
   
    if (Trigger.isBefore){
     
    	if (Trigger.isInsert){
      		CC_Short_Term_Lease_Trigger_Handler.onBeforeInsert(Trigger.new);
     	}
     	else if (Trigger.isUpdate){
      		CC_Short_Term_Lease_Trigger_Handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
     	}
     	else if (Trigger.isDelete){
     		CC_Short_Term_Lease_Trigger_Handler.onBeforeDelete(Trigger.old);
     	}
    }
    else if (Trigger.isAfter){

        if (Trigger.isUpdate){
            CC_Short_Term_Lease_Trigger_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}