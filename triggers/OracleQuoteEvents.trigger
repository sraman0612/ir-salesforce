trigger OracleQuoteEvents on cafsl__Oracle_Quote__c (before insert, before update, before delete,
                                                     after insert, after update, after delete, after undelete) {
  if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
      OracleQuoteEventHandler.markAsPrimary(trigger.new,TRUE);
    } /*else if (Trigger.isUpdate) {  //BEFORE UPDATE
      OracleQuoteEventHandler.markAsPrimary(trigger.new,FALSE);
    }*/ else if (Trigger.isDelete) {  // BEFORE DELETE
      OracleQuoteEventHandler.preventDelete(trigger.old);
    }
  } else {
    if (Trigger.isInsert) { // AFTER INSERT
      OracleQuoteEventHandler.updateOppAndPriorPrimaryQuote(trigger.new,trigger.oldMap);
    } else if (Trigger.isUpdate) {  // AFTER UPDATE
      OracleQuoteEventHandler.updateOppAndPriorPrimaryQuote(trigger.new,trigger.oldMap);
    } else if (Trigger.isDelete) {  // AFTER DELETE
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
  }
}