trigger PT_DSMP_NPD on PT_DSMP_NPD__c (before insert, after insert, after update) {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Trigger on PT_DSMP_NPD__c events
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  
	PT_DSMP_NPDTriggerHandler handler = new PT_DSMP_NPDTriggerHandler();
	
	//if(Trigger.isBefore && Trigger.isInsert) {
 //       handler.handleBeforeInsert(Trigger.new);
 //   }

	if(Trigger.isAfter && Trigger.isInsert) {
        handler.handleAfterInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate) {
        handler.handleAfterUpdate(Trigger.new, Trigger.old);
    }


}