/*****************************************************************
Name:            RS_CRS_Case_Trigger
Handler Class:   RS_CRS_Case_TriggerHandler
Purpose:         trigger Used for getting the previous Owner of Case. 
History                                                            
-------                                                            
VERSION    AUTHOR      DATE      DETAIL
1.0      Neela Prasad  02/27/2018    INITIAL DEVELOPMENT
*****************************************************************/
trigger RS_CRS_Case_Trigger on Case(before Update) {
    //Checking Event is After or not.
    if(Trigger.isBefore){ 
        //Checking Event is Insert or not.
        if(Trigger.isUpdate){
            // Calling method[getPreviousOwner] from handler Class[RS_CRS_Case_TriggerHandler].
            RS_CRS_Case_TriggerHandler.getPreviousOwner(Trigger.New);
        }
    }
}