trigger ProductItemTransactionTrigger on ProductItemTransaction(before insert, before update, before delete, after insert, after update, after delete, after undelete) {   
  Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('ProductItemTransaction');
  if (Trigger.isBefore){
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  //BEFORE INSERT
      CC_ProductItemTransactionHandler.assignWarehouses(trigger.new);
    } /*else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  //BEFORE UPDATE
    } else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  // AFTER INSERT
    } else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  // AFTER UPDATE
    } else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // AFTER DELETE
    } else if (Trigger.isUndelete && !softDisable.undeleteDisabled()) {  // AFTER UNDELETE
    }*/
  }
}