trigger rshvacQuoteTrigger on RSHVAC_Quote__c (after insert, after update, before delete) {
    
   Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('RSHVAC_Quote__c');  
   if(!softDisable.insertDisabled()){
    //OpportunityUtils utils = new OpportunityUtils();
    
    //for (RSHVAC_Quote__c quote : Trigger.new) {
    //    utils.quoteRecordChanged(quote);
    //}
  /** S.Colman 3/28/2016
     * Changed the handling of the trigger to follow a similar approach to this:
     * http://meltedwires.com/2013/06/05/trigger-pattern-for-tidy-streamlined-bulkified-triggers-revisited/
     * It's not exactly the same, but it'll help bulkify and keep the logic more in order
     */
     // Get the handler for the trigger
    RshvacQuoteTriggerHandler handler = new RshvacQuoteTriggerHandler();
    if (Trigger.isBefore){
      // Call the bulkBefore to handle any caching of data and enable bulkification
      handler.bulkBefore();
      if (Trigger.isDelete){
        // Iterate through the records inserted passing them to the handler.
        for (RSHVAC_Quote__c quote : Trigger.Old){
            handler.beforeDelete(quote);
        }
      }
    }
    if (Trigger.isAfter){
      // Call the bulk after to handle any caching of data and enable bulkification
       handler.bulkAfter();
      if (Trigger.isInsert){
        // Iterate through the records inserted passing them to the handler.
        for (RSHVAC_Quote__c quote : Trigger.new){
            handler.afterInsert(quote);
        }
      } else if (Trigger.isUpdate){
        // Iterate through the records updated passing them to the handler.
        for (RSHVAC_Quote__c quote : Trigger.old){
          handler.afterUpdate(quote, (RSHVAC_Quote__c) Trigger.newMap.get(quote.Id));
        }
      }
      //handler.andFinally();
    }
    // Perform any post processing
    handler.andFinally();
    }
}