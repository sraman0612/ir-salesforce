//
// (c) 2015, Appirio Inc.
//
// Account Trigger
//
// April 16,2015    Surabhi Sharma    T-377640
// Dec 8 Dhilip updated

  trigger Account_Trigger on Account(before insert,before update, after update, after delete) {    
  //create record type map and SBU based maps of triggerOld and triggerNew
  public Map<Id, String> accountRTMap = new Map<Id, String>();
  public Map<String, List<Account>> SBUTriggerNewMap = new Map<String, List<Account>>();
  public Map<String, List<Account>> SBUTriggerOldMap = new Map<String, List<Account>>();
  
  for (RecordType rt : [SELECT Id,Name,DeveloperName FROM RecordType where SobjectType='Account']) {
    accountRTMap.put(rt.Id,rt.Name);
    SBUTriggerNewMap.put(accountRTMap.get(rt.Id),new List<Account>());
    SBUTriggerOldMap.put(accountRTMap.get(rt.Id),new List<Account>());
  }
  
  //add records into the SBU specific map
  if (trigger.new != null){for(Account newA : trigger.new) {SBUTriggerNewMap.get(accountRTMap.get(newA.RecordTypeId)).add(newA);}}
  if (trigger.old != null){for(Account oldA : trigger.old) {SBUTriggerOldMap.get(accountRTMap.get(oldA.RecordTypeId)).add(oldA);}}
  
  //afterUpdate method of Account Trigger Handler being called to delete the record
   Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'Site_Account_NA_Air' and SobjectType='Account' limit 1].Id;
   List<Account> airNAAcctsNew = new List<Account>();
   List<Account> airNAAcctsOld = new List<Account>();
   
   //Separate new updates objects by record types
   
   if(null!=trigger.new){
       for (Account a : trigger.new){
           if (a.RecordTypeId==AirNAAcctRT_ID){
               airNAAcctsNew.add(a);
               System.debug('im inside account trigger.new');
           }
       }
   }
       
   //Separate old values by record types
      if(trigger.old!=NULL){
             for (Account a : trigger.old){
               if (a.RecordTypeId==AirNAAcctRT_ID){
                   airNAAcctsOld.add(a);
                   System.debug('im inside account trigger.old');
               } 
           }
      }
  
    
    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Account');
   
    if(trigger.isAfter){    
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            //Air Handler
            if(!airNAAcctsNew.isEmpty())
            {
                Account_TriggerHandler.afterUpdate(airNAAcctsNew);
                System.debug('im inside account trigger afterupdate');
            }    
        }   
        if(trigger.isDelete && !softDisable.deleteDisabled())
        {
            //Air Handler
            if(!airNAAcctsOld.isEmpty())
            {
                Account_TriggerHandler.afterDelete(airNAAcctsOld);
                System.debug('im inside account trigger afterdelete');
            }
        }
    }
    else if (trigger.isBefore)
    {
        if (Trigger.isInsert ) { 
            //CC_Account_TriggerHandler.beforeInsertUpdate(SBUTriggerNewMap);
        }
        if (trigger.isUpdate && !softDisable.updateDisabled())
        { //Air Handler
            if(!airNAAcctsNew.isEmpty())
            {
                Account_TriggerHandler.beforeUpdate(airNAAcctsNew,trigger.oldMap);
                System.debug('im inside account trigger beforeupdate');
            }        
        }
    }
}