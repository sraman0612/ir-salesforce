trigger PreventOracleQuoteLineitemDeletion on cafsl__Oracle_Quote_Line_Item__c (before delete) 
{
    if (Trigger.isBefore)
    {
        if(Trigger.isdelete)
        {
            for(cafsl__Oracle_Quote_Line_Item__c  QLI : Trigger.old)
            {
                QLI.addError('Oracle Quote Line Items cannot be deleted from Salesforce');
            }
        }
    }
}