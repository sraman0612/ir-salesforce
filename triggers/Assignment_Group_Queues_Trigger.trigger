trigger Assignment_Group_Queues_Trigger on Assignment_Group_Queues__c (before insert, before update)
{
    if (trigger.isBefore)
    {
        if (trigger.isInsert)
            Assignment_Group_Queues_TriggerHandler.beforeInsert(trigger.new);
        else if (trigger.isUpdate)
            Assignment_Group_Queues_TriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }
}