trigger PT_DSMP_TaskTrigger on Task (after insert, after update, after delete) {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Trigger on Task events
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 03-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  
	PT_DSMP_TaskTriggerHandler handler = new PT_DSMP_TaskTriggerHandler();

	if(Trigger.isAfter && Trigger.isInsert) {
       // handler.handleAfterInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate) {
        //handler.handleAfterUpdate(Trigger.new, Trigger.old);
    }

    if(Trigger.isAfter && Trigger.isDelete) {
        //handler.handleAfterDelete(Trigger.old);
    }


}