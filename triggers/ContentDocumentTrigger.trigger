trigger ContentDocumentTrigger on ContentDocument (before insert, before update, before delete,
                                            after insert, after update, after delete, after undelete) {
                                            
  PavilionSettings__c contentDocTrigger = PavilionSettings__c.getAll().get('ContentDocumentTrigger');  
 
 if((contentDocTrigger != null && contentDocTrigger.Value__c == 'Active')
     || contentDocTrigger == null){   
      if (Trigger.isBefore){
        if (Trigger.isInsert) {  //BEFORE INSERT
        } else if (Trigger.isUpdate) {  //BEFORE UPDATE 
        } else if (Trigger.isDelete) {  // BEFORE DELETE
        }
      } else {
        if (Trigger.isInsert) {  // AFTER INSERT          
        } else if (Trigger.isUpdate) {  // AFTER UPDATE
            if(ContentDocumentTriggerHandler.runOnce()){ContentDocumentTriggerHandler.sendEmailForNewContDoc(trigger.new);}
        } else if (Trigger.isDelete) {  // AFTER DELETE
        } else if (Trigger.isUndelete) {  // AFTER UNDELETE
        }
      }
  }
}