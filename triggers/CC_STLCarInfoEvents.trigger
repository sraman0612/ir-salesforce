trigger CC_STLCarInfoEvents on CC_STL_Car_Info__c (before insert) {
	
    if (Trigger.isBefore){
  	
        if (Trigger.isInsert){
            CC_STLCarInfoEventsHandler.onBeforeInsert(Trigger.new);
	    }
    }
}