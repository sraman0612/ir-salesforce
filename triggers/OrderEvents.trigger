trigger OrderEvents on CC_Order__c(before insert, before update, before delete,
                                   after insert, after update, after delete, after undelete) {
  
  	OrderEventHandler OEH = new OrderEventHandler(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
  	  
	if (Trigger.isBefore){
    
    	if (Trigger.isInsert) {  //BEFORE INSERT
    		OEH.onBeforeInsert();
    	} 
    	else if (Trigger.isUpdate) {  //BEFORE UPDATE
        	OEH.onBeforeUpdate();
    	} 
    	else if (Trigger.isDelete) {  // BEFORE DELETE
    	
    	}
  	} 
  	else {
    
    	if (Trigger.isInsert) { // AFTER INSERT
      		OEH.onAfterInsert();
    	} 
    	else if (Trigger.isUpdate) {  // AFTER UPDATE
      		OEH.onAfterUpdate();
    	} 
    	else if (Trigger.isDelete) {  // AFTER DELETE
    	
    	} 
    	else if (Trigger.isUndelete) {  // AFTER UNDELETE
    	
    	}
  	}
}