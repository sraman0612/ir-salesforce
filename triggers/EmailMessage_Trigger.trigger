trigger EmailMessage_Trigger on EmailMessage (before insert) {
    
    system.debug('EmailMessage_Trigger');
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('EmailMessage');
    if(skipTrigger!=true){  
    Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    
    // Trigger kill switch
    if (settings.Email_Message_Trigger_Enabled__c){
        
        system.debug('Email trigger enabled');
    
        if (Trigger.isInsert){
                
            if (Trigger.isBefore){
               // CTS_NCP_EmailMessage_Trigerhandler.reopenCase(Trigger.new, 'CTS_NA_North_Central_Parts', 'Open');
              //  CTS_NCP_EmailMessage_Trigerhandler.reopenCase(Trigger.new, 'CTS_NA_South_Parts', 'Open');
               // CTS_NCP_EmailMessage_Trigerhandler.reopenCase(Trigger.new, 'CTS_NA_South_Central_Parts', 'Open');
               // CTS_NCP_EmailMessage_Trigerhandler.reopenCase(Trigger.new, 'South_Area_Area_Specialists', 'Open');
              //  CTS_NCP_EmailMessage_Trigerhandler.reopenCase(Trigger.new, 'North_Central_Area_Area_Specialists', 'Open');
                if (settings.CTS_OM_Email_Trigger_Enabled__c){
                    system.debug('CTS email trigger enabled');
                    CTS_OM_EmailMessage_TriggerHandler.beforeInsert(Trigger.new);
                }           
            }       
        }
    }
    }
}