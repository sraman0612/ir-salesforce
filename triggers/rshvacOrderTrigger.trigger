trigger rshvacOrderTrigger on RSHVAC_Order__c (before insert, after insert, before update, after update) {
    /** S.Colman 3/28/2016
     * Changed the handling of the trigger to follow a similar approach to this:
     * http://meltedwires.com/2013/06/05/trigger-pattern-for-tidy-streamlined-bulkified-triggers-revisited/
     * It's not exactly the same, but it'll help bulkify and keep the logic more in order
     */
     // Get the handler for the trigger
    RshvacOrderTriggerHandler handler = new RshvacOrderTriggerHandler();
    if (Trigger.isBefore){
      // Call the bulk before to handle any caching of data and enable bulkification
      handler.bulkBefore();
      if (Trigger.isInsert) {
        // Iterate through the records to be inserted passing them to the handler.
        for (RSHVAC_Order__c so : Trigger.new){
            handler.beforeInsert(so);
        }
      } else if (Trigger.isUpdate) {
        // Iterate through the records to be updated passing them to the handler.
        for (RSHVAC_Order__c so : Trigger.old){
            handler.beforeUpdate(so, (RSHVAC_Order__c) Trigger.newMap.get(so.Id));
        }
      }
    } else if (Trigger.isAfter){
      // Call the bulk after to handle any caching of data and enable bulkification
      handler.bulkAfter();
      if (Trigger.isInsert){
        // Iterate through the records inserted passing them to the handler.
        for (RSHVAC_Order__c so : Trigger.new){
            handler.afterInsert(so);
        }
      } else if (Trigger.isUpdate){
        // Iterate through the records updated passing them to the handler.
        for (RSHVAC_Order__c so : Trigger.old){
          handler.afterUpdate(so, (RSHVAC_Order__c) Trigger.newMap.get(so.Id));
        }
      }
    }
    // Perform any post processing
    handler.andFinally();
}