trigger ContractTrigger on Contract(before insert, before update, before delete, after insert, after update, after delete, after undelete) {   
  
  Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contract');
   if (Trigger.isBefore){
       if (Trigger.isInsert && !softDisable.insertDisabled()) {  //BEFORE INSERT
           CC_ContractTriggerHandler.onBeforeInsert(Trigger.new);
       } else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  //BEFORE UPDATE
           CC_FieldHistoryLogger logger = new CC_FieldHistoryLogger(SObjectType.Contract.FieldSets.CC_ContractFieldHistory);
           logger.LogHistory(trigger.oldMap, trigger.newMap);
       } else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // BEFORE DELETE
            CC_ContractTriggerHandler.onBeforeDelete(Trigger.old);
       }
   } else {
       if (Trigger.isInsert && !softDisable.insertDisabled()) {  // AFTER INSERT
       } else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  // AFTER UPDATE
       } else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // AFTER DELETE
       } else if (Trigger.isUndelete && !softDisable.undeleteDisabled()) {  // AFTER UNDELETE
       }
   }
}