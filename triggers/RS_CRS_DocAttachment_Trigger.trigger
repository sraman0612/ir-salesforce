/*****************************************************************
Name:            RS_CRS_DocAttachment_Trigger
Handler Class:   RS_CRS_DocAttachment_TriggerHandler
Purpose:         trigger Used for identidifing Attachement is added to case Object to send Email alert to Case Owner. 
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Neela Prasad	12/26/2017		INITIAL DEVELOPMENT
*****************************************************************/
trigger RS_CRS_DocAttachment_Trigger on ContentDocumentLink (after insert) {
    
    //Checking Event is After or not.
    if(Trigger.isAfter){ 
        //Checking Event is Insert or not.
        if(Trigger.isInsert){
            // Calling method[sendEmailOnAttachment] from handler Class[RS_CRS_DocAttachment_TriggerHandler].
            RS_CRS_DocAttachment_TriggerHandler.sendEmailOnAttachment(Trigger.new);
        }
   
    }

}