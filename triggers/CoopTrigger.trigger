trigger CoopTrigger on CC_Coop_Claims__c (before insert, before update, before delete,
                                            after insert, after update, after delete, after undelete) {
    
  if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
    } else if (Trigger.isUpdate) {  //BEFORE UPDATE
        coopClaimTriggerHandler.submitAndProcessApprovalRequest(trigger.new);
        coopClaimTriggerHandler.createOrderAndOrderitem(trigger.new); 
    } else if (Trigger.isDelete) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert) { // AFTER INSERT
        coopClaimTriggerHandler.submitAndProcessApprovalRequest(trigger.new);
    } else if (Trigger.isUpdate) { // AFTER UPDATE
    } else if (Trigger.isDelete) {  // AFTER DELETE
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
  }
}