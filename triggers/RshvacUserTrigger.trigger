trigger RshvacUserTrigger on User (after insert, after update) {
  RshvacUserTriggerHandler handler = new RshvacUserTriggerHandler();
  if (Trigger.isAfter){
    // Call the bulk after to handle any caching of data and enable bulkification
    handler.bulkAfter();
    if (Trigger.isInsert){
      // Iterate through the records inserted passing them to the handler.
      for (User so : Trigger.new){
          handler.afterInsert(so);
      }
    } else if (Trigger.isUpdate){
      // Iterate through the records updated passing them to the handler.
      for (User so : Trigger.old){
        handler.afterUpdate(so, (User) Trigger.newMap.get(so.Id));
      }
    }
  }
  // Perform any post processing
  handler.andFinally();
}