trigger AccountTrig on Account (after insert) {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Trigger on Account
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 10-DEC-2018  MGR    1.1     Create PT DSMP child records
--------------------------------------------------------------------------------------
**************************************************************************************/ 


            
    List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
    List<Selling__c> sellings = new List<Selling__c>();    
    string currentYear = string.valueof(system.today().year());
    string nextYear = string.valueof((system.today().year())+1);
    PT_Record_types__c ptrt = PT_Record_types__c.getOrgDefaults();
    List<Account> lstAccountForBatch = new List<Account>();
    for(account acc : trigger.new){
        if(acc.recordtypeid == ptrt.Account_RT_Id__c){
            for(string mo : months){
                Selling__c s = new Selling__c(name=mo+' '+currentYear,Account__c=acc.id,PT_Account__c=acc.id,PT_Year__c=currentYear,currencyisocode='EUR');
                sellings.add(s);
                if(system.today().month()>=10){
                    sellings.add(new Selling__c(name=mo+' '+nextYear,PT_Account__c=acc.id,PT_Year__c=nextYear));
                }
            }
            if(acc.PT_BO_Customer_Number__c != null){lstAccountForBatch.add(acc);}
        }
    }    
    if(!sellings.isEmpty()){insert sellings;}
    
    if(!lstAccountForBatch.isEmpty()){
        PT_DSMP_BAT01_createRecordsAccountDVP batch = new PT_DSMP_BAT01_createRecordsAccountDVP(lstAccountForBatch);
        Database.executebatch(batch);
    }
}