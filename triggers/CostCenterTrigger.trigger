trigger CostCenterTrigger on Cost_Center__c (before insert, before update, after insert, after update) {
  if(Trigger.isBefore){
    List<Cost_Center__c> costCenterList = new List<Cost_Center__c>();
    for(Cost_Center__c costCenter : Trigger.New){
      if(costCenter.Create_Existing_Business_Opportunity__c && costCenter.Channel__c != 'DSO'){
          costCenterList.add(costCenter);
          CostCenterTriggerHandler.costCenterNumberSet.add(costCenter.Default_Branch__c);
      }
    }
    if(Trigger.isInsert){
      CostCenterTriggerHandler.beforeInsertUpdate(Trigger.New);    
    }
    if(Trigger.isUpdate){
      CostCenterTriggerHandler.beforeInsertUpdate(Trigger.New);
    }
  }
  If(Trigger.isAfter){
    If(Trigger.isInsert){
      CostCenterTriggerHandler.afterInsert(Trigger.New);        
    }
    If(Trigger.isUpdate){
      CostCenterTriggerHandler.afterupdate(Trigger.New);
    }  
  }
}