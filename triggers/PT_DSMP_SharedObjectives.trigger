trigger PT_DSMP_SharedObjectives on PT_DSMP_Shared_Objectives__c (after insert, after update) {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Trigger on PT_DSMP_Shared_Objectives__c events
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  
	PT_DSMP_SharedObjTriggerHandler handler = new PT_DSMP_SharedObjTriggerHandler();

	//if(Trigger.isAfter && Trigger.isUpdate) {
	if(Trigger.isAfter && Trigger.isInsert) {
        handler.handleAfterInsert(Trigger.new);
    }

}