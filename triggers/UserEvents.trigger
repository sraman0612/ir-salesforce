trigger UserEvents on User (before insert, before update, before delete,
                            after insert, after update, after delete, after undelete) {
   Boolean skipTrigger=CGCommonUtility.sKipTrigger('User');  
     //Merged Triggers as per SGOM 144.
     if(skipTrigger!=True){                    
    switch on Trigger.operationType {
                        When BEFORE_INSERT{  
                        }
                        When BEFORE_UPDATE{                                             
                                EMEIA_UserTriggerHandler.beforeUpdate(Trigger.new);                                                                 
                        }
                        When AFTER_INSERT{                                              
                                EMEIA_UserTriggerHandler.afterInsert(Trigger.new,Trigger.old,Trigger.newmap,Trigger.oldmap);                                                                                                                         
                        }
                        When AFTER_UPDATE{                                              
                                EMEIA_UserTriggerHandler.afterUpdate(Trigger.new,Trigger.old,Trigger.newmap,Trigger.oldmap);
                        }
                        When AFTER_DELETE{                                              
                        }
                }
     }
}