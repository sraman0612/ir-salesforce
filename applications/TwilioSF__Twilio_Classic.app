<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <label>Twilio</label>
    <logo>TwilioSF__Twilio/TwilioSF__TwilioIcon_new.png</logo>
    <tabs>TwilioSF__TwilioConfigurationClassic</tabs>
    <tabs>TwilioSF__Twilio_Inbox</tabs>
    <tabs>TwilioSF__Message__c</tabs>
    <tabs>TwilioSF__BulkMessage__c</tabs>
    <tabs>TwilioSF__Opt_In_Keyword__c</tabs>
    <tabs>TwilioSF__Twilio_Personal_Number__c</tabs>
    <tabs>TwilioSF__Log__c</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Assignment_Groups__c</tabs>
</CustomApplication>
