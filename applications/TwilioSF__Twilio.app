<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>TwilioSF__twiliologosm1</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <label>Twilio</label>
    <navType>Standard</navType>
    <tabs>TwilioSF__TwilioConfigurationClassic</tabs>
    <tabs>TwilioSF__Twilio_Inbox_Lightning</tabs>
    <tabs>TwilioSF__Message__c</tabs>
    <tabs>TwilioSF__BulkMessage__c</tabs>
    <tabs>TwilioSF__Opt_In_Keyword__c</tabs>
    <tabs>TwilioSF__Twilio_Personal_Number__c</tabs>
    <tabs>TwilioSF__Log__c</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <uiType>Lightning</uiType>
</CustomApplication>
