<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Data.com Assessment</label>
    <logo>ddcAssess__Clean_Assessment/ddcAssess__Clean_Assessment_Tab.jpg</logo>
    <tabs>ddcAssess__Data_com_Assessment</tabs>
    <tabs>ddcAssess__Assessment_Summary__c</tabs>
</CustomApplication>
