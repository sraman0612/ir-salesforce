<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>echosign_dev1__Adobe_Sign_logo_1024px</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Adobe Sign application for Lightning Experience.</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Adobe Sign for Salesforce</label>
    <navType>Standard</navType>
    <tabs>standard-home</tabs>
    <tabs>echosign_dev1__Adobe_Sign_Admin_Component_Tab</tabs>
    <tabs>echosign_dev1__SIGN_Agreement__c</tabs>
    <tabs>echosign_dev1__ManageAgreements</tabs>
    <tabs>echosign_dev1__Agreement_Template__c</tabs>
    <tabs>echosign_dev1__Agreement_Type__c</tabs>
    <tabs>echosign_dev1__SIGN_Data_Mapping__c</tabs>
    <tabs>echosign_dev1__SIGN_Merge_Mapping__c</tabs>
    <tabs>echosign_dev1__EchoSign_Group_Mappings</tabs>
    <tabs>echosign_dev1__Template_Batch</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>echosign_dev1__Adobe_Sign_for_Salesforce_UtilityBar</utilityBar>
</CustomApplication>
