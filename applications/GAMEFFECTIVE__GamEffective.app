<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>GamEffective</label>
    <logo>GAMEFFECTIVE__GE_Documents/GAMEFFECTIVE__GE_Logo.png</logo>
    <tabs>GAMEFFECTIVE__GamEffective_Admin</tabs>
    <tabs>GAMEFFECTIVE__GamEffective</tabs>
</CustomApplication>
