<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Use this as the default app for all sales functions.</description>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <label>Sales Force Automation</label>
    <logo>SharedDocuments/IR_Logo_For_Letterhead.png</logo>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-CollaborationGroup</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Account_Team__c</tabs>
    <tabs>Snapshot_Test__c</tabs>
    <tabs>Release_Mgmt__c</tabs>
    <tabs>IR_Employee__c</tabs>
    <tabs>GridBuddy</tabs>
    <tabs>standard-Survey</tabs>
    <tabs>standard-Case</tabs>
    <tabs>Assignment_Groups__c</tabs>
</CustomApplication>
