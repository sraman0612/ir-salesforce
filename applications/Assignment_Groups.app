<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Assignment_Group_Name__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <label>Assignment Groups</label>
    <tabs>Assignment_Group_Name__c</tabs>
    <tabs>standard-Case</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-report</tabs>
    <tabs>Quote__c</tabs>
    <tabs>OBM_Notifier__c</tabs>
    <tabs>Account_Team__c</tabs>
    <tabs>Snapshot_Test__c</tabs>
    <tabs>Assignment_Groups__c</tabs>
</CustomApplication>
