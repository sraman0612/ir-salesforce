<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Gimbal Barcode</label>
    <logo>GMBLBC__Gimbal_Barcode/GMBLBC__Gimbal_Barcode_App_Icon.png</logo>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Product2</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Case</tabs>
    <tabs>GMBLBC__Barcode_Text_Redirect_Case</tabs>
    <tabs>GMBLBC__Barcode_Text_Action_Case</tabs>
    <tabs>GMBLBC__Barcode_Text_Flow_Case</tabs>
</CustomApplication>
