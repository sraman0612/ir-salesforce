<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Lightning Adoption Tracker</label>
    <tabs>ltnadptn__LightningAdoptionTrackerSetup</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-report</tabs>
</CustomApplication>
