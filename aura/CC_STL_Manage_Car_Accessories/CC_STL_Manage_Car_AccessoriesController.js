({
    init: function (cmp, event, helper) {
        
        console.log("CC_STL_Manager_Car_Accessories: init");
        helper.initComponent(cmp, event, helper);
    },
    
    handleSave: function(cmp, event, helper){
        
      	console.log("handle save");     
        helper.saveAccessories(cmp, event, helper);
    },
    
    handleCancel: function(cmp, event, helper){
        
        console.log("handle cancel");
        
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();    
    },

    toggleSelectAll: function(cmp, event, helper){
    	
        console.log("toggleSelectAll");
        var selectAll = cmp.get("v.selectAll");
        var data = cmp.get("v.data");
        
        data.forEach(function(lineItem, index){
            lineItem.isSelected = selectAll;
        });
        
        cmp.set("v.data", data);
    },
    
    handleSelectClick: function(cmp, event, helper){
        
        var selectAll = cmp.get("v.selectAll");
        var checkbox = event.getSource();
        
        // Reset the select all flag if one of the rows is unselected
        if (selectAll && !checkbox.get("v.value")){
            cmp.set("v.selectAll", false);
        }             
    }
});