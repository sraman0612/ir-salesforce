({
    initComponent: function (cmp, event, helper) {
        
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");
        action.setParams({"stlCarInfoId" : cmp.get("v.recordId")});
        
        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);
    },
    
    handleGetInitSuccess: function(cmp, event, helper, response){
    
		try{
            
			var responseVal = JSON.parse(response.getReturnValue());
		
			console.log(responseVal);
				
			if (responseVal.success){		
                                
                var data = [];
                
                responseVal.lineItemOptions.forEach(function(item, index){
                    
                    data.push({
                        "id" : item.lineItemId,
                        "accessoryName" : item.accessoryName,
                        "quantity" : item.quantity,
                        "unitLaborCost" : item.unitLaborCost,
                        "isSelected" : item.isSelected,
                        "carInfoId" : item.carInfoId,
                        "carAccessoryId" : item.carAccessoryId
                    });
                });
                
                cmp.set("v.data", data);     
			}
			else{
			
                console.log("failed getInfo response");
                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 10000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();	
			}	
        }
        catch(e){
        	console.log("exception: " + e);
        }    
        
        cmp.set("v.isLoading", false);
	},
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);        
	},
    
    saveAccessories: function(cmp, event, helper){
     	
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.saveAccessories");
        action.setParams({"stlCarInfoId" : cmp.get("v.recordId"), "lineItemOptionsStr" : JSON.stringify(cmp.get("v.data"))});      
        utils.callAction(action, helper.handleSaveAccessoriesSuccess, helper.handleActionCallFailure, cmp, helper);
    },
    
    handleSaveAccessoriesSuccess: function(cmp, event, helper, response){
        
   		try{
            
			var responseVal = JSON.parse(response.getReturnValue());
		
			console.log(responseVal);
				
			if (responseVal.success){		
                
                var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "Save successful",
			        type : "success"			        
			    });
			    
				toastEvent.fire();  
                
                // Refresh the car detail page to reflect the new line items
                $A.get('e.force:refreshView').fire();
                
                // Close the action panel
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();            
            }              
			else{
			
                console.log("failed response");
                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();                 
			}	
        }
        catch(e){
        	console.log("exception: " + e);
        }    
        
        cmp.set("v.isLoading", false);     
    }
});