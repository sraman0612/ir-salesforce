({
    openSingleFile : function (component,event){
        var guideID = '';
        //call apex controller method
        var action = component.get("c.fetchConcessionGuideId");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //get concession guide Id
                guideID = response.getReturnValue();
                
                //open guide as window
                var fireEvent = $A.get("e.lightning:openFiles");
                fireEvent.fire({
                    recordIds: [guideID]
                }); 
            }
        });
        
        $A.enqueueAction(action);     
    }
});