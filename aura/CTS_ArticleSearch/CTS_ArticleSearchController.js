//methods called from the cmp. handler methods are in the helper file.
({    
    fireSearchEvent : function(cmp, event) {
        var searchEvent = $A.get("e.c:CTS_ArticleSearchEventV2");
        var tabName = event.getSource().get("v.id");
        searchEvent.setParams({"tabName" : tabName});
        searchEvent.fire();
    }
})