/**
 * @File Name          : Opporutnity_Product_ConfirmationHelper.js
 * @Description        : 
 * @Author             : Murthy Tumuluri
 * @Group              : 
 * @Last Modified By   : Murthy Tumuluri
 * @Last Modified On   : 7/12/2019, 11:14:27 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/12/2019, 11:03:25 AM   Murthy Tumuluri     Initial Version
**/
({
    callServer: function(component, method, callback, params, cacheable) {
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        if (cacheable) {
            action.setStorable();
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback.call(this, response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.errorMessage", errors[0].message);
                    }
                } else {
                    component.set("v.errorMessage", errors[0].message);
                }
            }
        });
        $A.enqueueAction(action);
    },
    navigateToObject: function(recId,place,redirectVal){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": place,
            "isredirect": redirectVal
        });
        navEvt.fire();
    }
})