({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getAssetInit");
        let recordId = cmp.get("v.recordId");
        console.log("recordId: " + recordId);
        
        action.setParams({
            "assetId" : recordId
        });

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	     
    },

    handleGetInitSuccess: function(cmp, event, helper, response){        

        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);

            if (responseVal.success){   

                cmp.set("v.assetDetail", responseVal.assetDetail);
                cmp.set("v.communitySettings", responseVal.communitySettings);    
                
                // Update breadcrumbs
                // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
                
                // navEvent.setParams({
                //     "label" : responseVal.assetDetail.asset.Name + " Dashboard",
                //     "url" : "/asset-losant-detail?asset-id=" + responseVal.assetDetail.asset.Id
                // });

                // navEvent.fire();                 
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);
        }    
       
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})