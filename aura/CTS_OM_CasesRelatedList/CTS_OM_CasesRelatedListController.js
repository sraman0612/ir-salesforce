({
	doInit : function(cmp, event, helper) {
		var fieldsToShow = cmp.get("v.fieldsToShow");
        var recordId = cmp.get("v.recordId");
        var filterLookups = cmp.get("v.filterLookups");
		var fields = fieldsToShow.replace(/;/g, ",");
        var recordTypeIdOrName = cmp.get("v.recordTypeIdOrName");
        var recordTypeId, recordTypeName;
        var sortedBy = cmp.get("v.sortedBy");
        if(recordTypeIdOrName && recordTypeIdOrName.startsWith("012")) recordTypeId = recordTypeIdOrName;
        else recordTypeName = recordTypeIdOrName;
        console.log('recordId ::1::'+recordId);
        if(recordId == null || recordId == undefined){
		   var sParam = 'recordId';
		   var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;
			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] === sParam) {
					recordId = sParameterName[1];
					break;
				}
            }
        }
        var action = cmp.get("c.getData");
        console.log('fields:::'+fields +'\n::fieldsToShow::'+fieldsToShow);
        action.setParams({
            recordId: recordId,
            recordTypeId: recordTypeId,
            recordTypeName: recordTypeName,
            fields: fields,
            filterLookups: filterLookups,
            sortedBy: sortedBy
        });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === 'SUCCESS') {
            	var res = JSON.parse(response.getReturnValue());
                if(res != null){
                res[0].forEach(function (column) {
                	
                    switch (column.type) {
                        case 'date-local':
                          column['typeAttributes'] = { year: 'numeric', month: 'short', day: 'numeric' };
                          break;
                        
                        case 'date':
                          column['typeAttributes'] = { year: 'numeric', month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit' };
                          break;
                            
                        default:
                          break;
                	}
                });
                 var tempCols = res[0];
                 var columns = [];
                 for (var j = 0; j < tempCols.length; j++) {
                    var col = tempCols[j];
                    col.sortable = true;
                    if (!col.fieldName.endsWith('Id') && col.fieldName != 'CaseNumber') {
                        col.fieldName = col.fieldName.replace('.Name', '');
                        columns.push(col);
                    }
                    if (col.fieldName == 'Show_Record__c') {
                        col.label = 'Case Number';
                        col['typeAttributes'] = {label: {fieldName: 'CaseNumber'}};
                        col.sortable = true;
                        col.type = 'url';
                    }
                }
                var isViewAll = cmp.get("v.isViewAll");
                var numOfRecords = isViewAll == true ?  parseInt(cmp.get("v.pageSize")) : parseInt(cmp.get("v.NumOfRecordsToDisplay"));
                var recordsToShow = [];
                cmp.set("v.columns", columns);
               // cmp.set("v.data", res[1]);
                cmp.set("v.size", res[1].length);
                console.log('res0**', res[0]);
                console.log('res1**', res[1]);
                //cmp.set("v.data", objectList);
                
                if(numOfRecords < res[1].length){
                    var allRecords = res[1];
                    for(var i = 0; i < numOfRecords; i++){
                        var rec = JSON.stringify(allRecords);
                        if(rec.indexOf('RecordTypeId') > 0){
                            allRecords[i].RecordType = allRecords[i].RecordType.Name;
                        }
                        if(rec.indexOf('OwnerId') > 0){
                            allRecords[i].Owner = allRecords[i].Owner.Name;
                        }
                        recordsToShow.push(allRecords[i])
                    }
                    cmp.set("v.data", recordsToShow);
                    
                }
                if(isViewAll == true && document.getElementById('viewAllDiv') != null){
                    document.getElementById('viewAllDiv').style.display = 'none';
                     var workspaceAPI = cmp.find("workspace");
                     var icon = cmp.get("v.icon");
                     workspaceAPI.getFocusedTabInfo().then(function(response) {
                            var focusedTabId = response.tabId;
                            workspaceAPI.setTabLabel({
                                tabId: focusedTabId,
                                label: "Matching Cases"
                            });
                         	workspaceAPI.setTabIcon({tabId: focusedTabId, 
                            icon: "standard:"+ icon});
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                }
				cmp.set("v.allData", res[1]);
                cmp.set("v.totalRecords", res[1].length);
                cmp.set("v.startPage", 0);
            	cmp.set("v.endPage", numOfRecords - 1);
                //Calculate total pages
            	var totalPage = res[1].length % numOfRecords > 0 ? parseInt(res[1].length / numOfRecords) + 1 : res[1].length / numOfRecords;
            	cmp.set('v.totalPage', parseInt(res[1].length / numOfRecords) + 1);
                 helper.sortData(cmp, event, this, 'Show_Record__c', 'DESC');
                    //cmp.set("v.sortedBy", 'Show_Record__c');
                 cmp.set("v.sortedDirection", 'DESC');
                   
                
            } else {
            	console.log('error**' + state);
            }
            }else{
                cmp.set("v.columns", columns);
                cmp.set("v.data", []);
                cmp.set("v.size", 0);
            }
        });
        $A.enqueueAction(action);
	},
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    viewAllRecords : function(cmp, event, helper){
        var allRecords = cmp.get("v.allData");
        cmp.set("v.data",allRecords);
        var recordId = cmp.get('v.recordId');
        var navService = cmp.find("navService");
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                componentName: "c__CTS_OM_CaseRelatedListViewAll"
            }, 
            "state": {
                c__recordId: recordId
            }
        };
        cmp.set("v.isViewAll", false);
        navService.navigate(pageReference);
    },
    showRecords: function(cmp, event, helper){
        var numOfRecords = parseInt(cmp.get("v.NumOfRecordsToDisplay"));
        var recordsToShow = [];
        var allRecords = cmp.get("v.allData");
        if(numOfRecords < allRecords.length){
            for(var i = 0; i < numOfRecords; i++){
                recordsToShow.push(allRecords[i]);
            }
        }
        cmp.set("v.data", recordsToShow);
        document.getElementById('viewAllDiv').style.display = '';
        document.getElementById('viewLessDiv').style.display = 'none';
    }, 
    onNext: function(component, event, helper) {
        helper.onNext(component, event, helper);
    },
    onPrevious: function(component, event, helper) {
        helper.onPrevious(component, event, helper);
    }
})