({
	sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.data", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    onNext: function(component, event) { //on click of next button for pagination
        component.set('v.data', null); //set results to blank
        var sObjectList = component.get("v.allData");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                var rec = JSON.stringify(sObjectList);
                if(rec.indexOf('RecordTypeId') > 0){
                    sObjectList[i].RecordType = sObjectList[i].RecordType.Name;
                }
                if(rec.indexOf('OwnerId') > 0){
                    sObjectList[i].Owner = sObjectList[i].Owner.Name;
                }
                pagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.data', pagList);
        component.set('v.currentPage', currPage + 1);
    },    
    onPrevious: function(component, event, helper) { //on click of previous button for pagination
        component.set('v.data', null); //set results to blank
        var sObjectList = component.get("v.allData");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                var rec = JSON.stringify(sObjectList);
                if(rec.indexOf('RecordTypeId') > 0){
                    sObjectList[i].RecordType = sObjectList[i].RecordType.Name;
                }
                if(rec.indexOf('OwnerId') > 0){
                    sObjectList[i].Owner = sObjectList[i].Owner.Name;
                }
                pagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.data', pagList);
        if (currPage > 1) {
            component.set('v.currentPage', currPage - 1);
        }
    }
})