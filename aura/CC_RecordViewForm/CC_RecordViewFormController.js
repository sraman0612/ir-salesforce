({
	doInit : function(cmp) {
        var recordId = cmp.get("v.recordId");
        
        $A.createComponent(
            "forceChatter:publisher",
            {
                "type": "RECORD",
                "subjectId": recordId
            },
            function(recordPublisher, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(recordPublisher);
                    cmp.set("v.body", body);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.");
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                }
            }
        );
        
        //Update ViewStat
        var action = cmp.get("c.updateViewStat");
        action.setParams({recordId: recordId});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var res = response.getReturnValue();
                    console.log('res**', recordId);
                } else {
                    console.log('error**' + state);
                }
            });
            $A.enqueueAction(action);
 	}
 })