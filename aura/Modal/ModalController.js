({
    onCancel : function(cmp, event, helper) {
        console.log("onCancel");
     
        cmp.set('v.errorMessage', '');
        var modalEvent = cmp.getEvent('onModalCancel');
        modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
        modalEvent.fire();              
	},
	onSave : function(cmp, event, helper) {
        console.log("onSave");
        
        cmp.set('v.errorMessage', '');
        var modalEvent = cmp.getEvent('onModalSave');
        modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
        modalEvent.fire();   
	},
    handleError : function(cmp, event, helper){
        cmp.set('v.errorMessage', event.getParam('message'));
    }
})