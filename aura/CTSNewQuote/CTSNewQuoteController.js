({
    doInit: function(component, event, helper) {
        var actions = [
            { label: 'Edit', name: 'edit' },
            { label: 'Mark as Primary', name: 'primary'}
        ];

        component.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'url', typeAttributes: {label: { fieldName: 'LinkName' }, target: '_self'}},
            {label: 'Primary', fieldName: 'Primary__c', type: 'boolean'},
            {label: 'Transaction Id', fieldName: 'cafsl__Transaction_ID__c', type: 'text', sortable: 'true'},
            {label: 'Created Date', fieldName: 'CreatedDate', type: 'date', sortable: 'true', typeAttributes: {
                year: 'numeric',
                month: 'short',
                day: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            }},

            { type: 'action', typeAttributes: { rowActions: actions }}
        ]);

        var page = component.get("v.page") || 1;
        helper.getData(component, page);
    },

    navigate: function(component, event, helper) {
        var page = component.get("v.page") || 1;
        var direction = event.getSource().get("v.label");
        page = direction === "Previous Page" ? (page - 1) : (page + 1);

        helper.getData(component, page);
    },
    
    navigateFullRelatedList: function(component, event, helper) {
        var navService = component.find("navService");
        event.preventDefault();
        let pr = {    
            "type": "standard__recordRelationshipPage",
            "attributes": {
                "recordId": component.get("v.recordId"),
                "objectApiName": "Opportunity",
                "relationshipApiName": "cafsl__Oracle_Quotes__r",
                "actionName": "view"
            }
        }
        navService.navigate(pr);
    },

    onSelectChange: function(component, event, helper) {
        var page = 1;
        helper.getData(component, page);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'edit':
                /*
                var editRecordEvent = $A.get("e.force:viewRecord");
                editRecordEvent.fire({
                    "recordId":  row.Id
                });
                */
                var navService = component.find("navService");
                event.preventDefault();
                var urlString = window.location.href;
                var baseURL = urlString.substring(0, urlString.indexOf("/s"));
                var quoteURLStr = baseURL + '/s/quoteeditor?id=' + row.Id;
                let pr2 = {"type": "standard__webPage","attributes": {"url": quoteURLStr}}
                navService.navigate(pr2);
                /* 
                  TRIED CALLING THE CANVAS EMBEDDED PAGE BUT IT ALWAYS CREATES A NEW QUOTE
                  @TODO MAYBE THERE IS SOME OTHER WAY TO MAKE THE EMBED PAGE WORK FOR AN EXISTING QUOTE
                
                var navService = component.find("navService");
                event.preventDefault();
                var urlString = window.location.href;
                var baseURL = urlString.substring(0, urlString.indexOf("/s"));
                var quoteURLStr = baseURL + '/s/quotecreator?id=' +  component.get("v.recordId") + '&quoteid=' + row.Id;
                let pr2 = {"type": "standard__webPage","attributes": {"url": quoteURLStr}}
                navService.navigate(pr2);
                */
                break;
            case 'delete':
                console.log('delete called');
                var page = component.get("v.page") || 1;
                helper.delete(component, page, row.Id);
                break;
            case 'primary':
                helper.markPrimary(component, row.Id);
                break;                
        }
    },

    createRecord : function (component) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var objectType = component.get("v.objectType");

        createRecordEvent.fire({
            "entityApiName": objectType
        });
    },

    handleApplicationEvent: function (component, event, helper) {
        var page = component.get("v.page") || 1;
        helper.getData(component, page);
    },

    updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    
    
	launchCPQ : function(component, event, helper) {
	      var action = component.get("c.setTriageSessionInfo");
        action.setParams({ oppId : component.get("v.recordId") });
		    action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var navService = component.find("navService");
                event.preventDefault();
                var urlString = window.location.href;
                var baseURL = urlString.substring(0, urlString.indexOf("/s"));
                var quoteURLStr = baseURL + '/s/quotecreator?id=' +  component.get("v.recordId");
                let pr2 = {"type": "standard__webPage","attributes": {"url": quoteURLStr}}
                navService.navigate(pr2);
            }
            else if (state === "INCOMPLETE") {
                console.log("Incomplete error");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}
})