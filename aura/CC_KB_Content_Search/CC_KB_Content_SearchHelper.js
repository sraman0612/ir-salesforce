({
    //method called on init
    getSearchConfig: function(component, event, helper) {
      //read keyword from param if keyword is null
      var searchText = component.get("v.searchText");
      if(searchText == null || searchText == '') {
          var getUrlParameter = function getUrlParameter(sParam) {
              var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                  sURLVariables = sPageURL.split('&'), sParameterName, i;
              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');
                  if (sParameterName[0] === sParam) {
                    var searchStr = sParameterName[1].replace(/\+/g, " ");
                      return searchStr === undefined ? true : searchStr;
                  }
              }
          };            
          var initVal = getUrlParameter('keyword');
          component.set("v.searchText", initVal);            
      }
        if(component.get("v.searchConfig") == null) {
            var action = component.get("c.getSearchConfigDetails");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var searchConfig = response.getReturnValue();
                    component.set("v.searchConfig", searchConfig);
                    
                    var pageSizeSelectJson = [];
                    var pageSizeSelectVals = searchConfig.pageSizeOptions.split(',');
                    for(var i=0;i<pageSizeSelectVals.length;i++) {
                        var temp = {};
                        temp.label = pageSizeSelectVals[i];
                        temp.value = pageSizeSelectVals[i];
                        pageSizeSelectJson.push(temp);
                    }
                    component.set("v.pageSizeSelect", pageSizeSelectJson);      
                    //call search if a param was passed from the URL
                    var searchText = component.get("v.searchText");
        			if(searchText != null && searchText != '') {
                        component.set("v.searchOrTypeahead", "search");
                        this.searchKBHelper(component, event, helper);
                    }
                } else {
                    console.log('error' + state);
                }
            });
            $A.enqueueAction(action);
        }
    },
    //main search method
    searchKBHelper: function(component, event, helper) { 
        console.log('in search');
        component.set('v.paginationList', null); //set results to blank
        component.set("v.showSpinner", true);
        var searchText = component.get("v.searchText");
        if(searchText.length < 2) {
            component.set("v.searchResults", null);
            return;
        }
        //capture the searched keywrods
        var allSearchTexts;
        if(component.get("v.searchOrTypeahead") == 'search' || searchText.length > 2) {
        	var allSearchTexts = component.get("v.allSearchTexts");
        	allSearchTexts += searchText + ';';
            component.set("v.allSearchTexts", allSearchTexts);
        }
        var action = component.get("c.search");
        var searchTypes = component.get("v.selectedSearchType");
        action.setParams({
            keyword: searchText,
            searchTypes: searchTypes
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === 'SUCCESS') {                
                var results = response.getReturnValue();                
                component.set("v.searchResults", results);
                if(results == null) return;
                //for paging
                var pageSize = component.get("v.pageSize");                
                component.set("v.totalRecords", component.get("v.searchResults").length);
                component.set("v.startPage", 0);
                component.set("v.endPage", pageSize - 1);
                var pgList = [];
                for (var i = 0; i < pageSize; i++) {
                    if (results.length > i)
                        pgList.push(results[i]);
                }
                //calculate total pages
                var totalPage = component.get("v.searchResults").length%pageSize > 0 ? parseInt(component.get("v.searchResults").length/pageSize) + 1 : component.get("v.searchResults").length/pageSize;
                component.set('v.paginationList', pgList);
                component.set('v.totalPage', parseInt(component.get("v.searchResults").length/pageSize) + 1);                                
            }
        });

        $A.enqueueAction(action);
    },
    //on click of next button for pagination
    onNext: function(component, event) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                pagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        component.set('v.currentPage', currPage + 1);
    },
    //on click of previous button for pagination
    onPrevious: function(component, event, helper) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");  
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                pagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        if(currPage > 1){
            component.set('v.currentPage', currPage - 1);
        }
    },
    //on change of page size for pagination
    pageSizeChanged: function(component, event, helper) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var pageSize = parseInt(component.find("pageSize").getElement().value);
        component.set("v.pageSize", pageSize);
        component.set("v.totalRecords", component.get("v.searchResults").length);
        component.set("v.startPage", 0);
        component.set("v.endPage", pageSize - 1);
        var pgList = [];
        for (var i = 0; i < pageSize; i++) {
            if (sObjectList.length > i) {
                pgList.push(sObjectList[i]);
            }
        }
        component.set('v.paginationList', pgList);
        component.set('v.currentPage', 1);
        var totalPage = component.get("v.searchResults").length%pageSize > 0 ? parseInt(component.get("v.searchResults").length/pageSize) + 1 : component.get("v.searchResults").length/pageSize;
        component.set('v.totalPage', parseInt(component.get("v.searchResults").length/pageSize) + 1);
    },    
    //called when case form is loaded
    handleCaseFormLoad: function(component, event, helper) {
        var searchText = component.get("v.searchText");
        var allSearchTexts = component.get("v.allSearchTexts");
        
        if(allSearchTexts && allSearchTexts.endsWith(';')) {
            allSearchTexts = allSearchTexts.substring(0,allSearchTexts.length-1);
            component.set("v.allSearchTexts", allSearchTexts);
        } else if((allSearchTexts == null || allSearchTexts == '') && searchText != null) {
            component.set("v.allSearchTexts", searchText);
        }
    },
    //called when case form is submitted
    handleCaseSubmit: function(component, event, helper) {
        component.set("v.showSpinner", true);
        event.preventDefault();
        var fields = event.getParam("fields");        
        component.find("caseCreateForm").submit(fields);
    },
    //called when case is created to show user feedback
    handleCaseSuccess: function(component, event, helper){
        var record = event.getParam("response");
        var recordId = record["id"];
        var action = component.get("c.getCaseNumber");
        action.setParams({
            caseId: recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.caseNumber", response.getReturnValue());
                component.set("v.isCaseCreated", true);
                component.set("v.caseId", recordId);
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    //called when click on case number. shows in modal box
    openCaseDetail: function(component, event, helper) {
        var objectName, title;        
        var recId = component.get('v.caseId');        
        $A.createComponent("c:CC_RecordViewForm", {
                recordId: recId,
                objectName: 'Case'
            },
            function(modalCmp, status) {
                if (status === "SUCCESS") {
                    component.find('overlayLib').showCustomModal({
                        header: title,
                        body: modalCmp,
                        showCloseButton: true,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            console.log('in modal callback!');
                        }
                    });
                }
            }
        )
    },    
    redirectToCC_Create_Case: function(cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/ClubCarLinks/s/club-car-links-community-Ask"
        });
        urlEvent.fire();
    },
	callServer : function(method,parameter,component) {
		component.set("v.paginationList",null);
        component.set("v.showSpinner", true);
		var action = component.get(method);
        action.setParams({parameter : parameter});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set("v.paginationList",response.getReturnValue());
                component.set("v.showSpinner", false);
            }
            else if (state === "INCOMPLETE") {}
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}    
})