//method called from the cmp. handler methods are in the helper file.
({
	getSearchConfig : function(component, event, helper) {
    	helper.getSearchConfig(component, event, helper);
    },
    onRender : function(component, event, helper) {
        var pageSize = component.get("v.pageSize");
    	var opt = document.getElementById(pageSize);
        if(opt) opt.selected = true;
    },
    clearText: function(component, event, helper) {
        component.set("v.searchText", "");
        component.set("v.searchResults", null);
    },
    searchTypeAhead : function(component, event, helper) {
    	if(event.keyCode === 13) {
			component.set("v.searchOrTypeahead", "search");
            helper.searchKBHelper(component, event, helper);
        } else {
        	component.set("v.searchOrTypeahead", "typeahead");
            if(component.get("v.enableTypeAhead")) {
                helper.searchKBHelper(component, event, helper);
            }            
        }        
    },
    searchKB : function(component, event, helper) {
    	component.set("v.searchOrTypeahead", "search");
        helper.searchKBHelper(component, event, helper);
    },    
    handleChange: function (component, event, helper) {
        //no action needed
    },
    onNext : function(component, event, helper){
        helper.onNext(component, event, helper);
    },
    onPrevious: function(component, event, helper){
        helper.onPrevious(component, event, helper);
    },
    pageSizeChanged: function(component, event, helper){
        helper.pageSizeChanged(component, event, helper);
    },
    openDetail: function(component, event, helper) {
        helper.openDetail(component, event, helper);
    },
    handleCaseSubmit: function(component, event, helper){
        helper.handleCaseSubmit(component, event, helper);
    },
    handleCaseSuccess: function(component, event, helper){
        helper.handleCaseSuccess(component, event, helper);
    },
    waiting: function(component, event, helper) {
      component.set("v.showSpinner", true);
    },
    doneWaiting: function(component, event, helper) {
      component.set("v.showSpinner", false);
    },
    openCaseDetail: function(component, event, helper) {
        helper.openCaseDetail(component, event, helper);
    },
    resetCaseForm: function(component, event, helper) {
        component.set("v.isCaseCreated", false);
    },
    handleCaseFormLoad: function(component, event, helper) {
        helper.handleCaseFormLoad(component, event, helper);
    },
    handleOpenFiles: function(component, event, helper) {
		helper.handleOpenFiles(component, event, helper);
	},
    redirectToCC_Create_Case: function(component, event, helper) {
        helper.redirectToCC_Create_Case(component, event, helper);
    },
    newArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "ORDER BY LastModifiedDate DESC";
        helper.callServer(method,parameter,component);
    },
    mostViwedArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "ORDER BY ArticleTotalViewCount DESC";
        helper.callServer(method,parameter,component);
    },
    followingArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "EntitySubscription";
        helper.callServer(method,parameter,component);
    },
    mostHelpfulArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "mostHelpfulArticles";
        helper.callServer(method,parameter,component);
    }
})