({
    updateConcessionStatus : function(component, event) {
        //prepare action to get case concession status
        var updateCall = component.get("c.updateConcessionStatus");
        
        updateCall.setParams({
            "caseId" : component.get("v.recordId")
        });
        
        //configure response handler for this action
        updateCall.setCallback(this, function(response){
            var state = response.getState();
            
            if(state === "SUCCESS"){
                var resp = response.getReturnValue();
                
                //check whether response received with values
                if(resp == 'SUBMITTED'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "success", 
                        "title": "Success!",
                        "message": "Case submitted for FTP successfully."
                    });
                    toastEvent.fire();
                    
                    //Update the UI: closePanel, show toast, refresh page
                    //$A.get("e.force:closeQuickAction").fire();
                    
                    $A.get("e.force:refreshView").fire();
                } else if(resp == 'ALREADY_SUBMITTED'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "warning", 
                        "title": "Warning!",
                        "message": "Case already submitted for FTP."
                    });
                    toastEvent.fire();
                    
                    $A.get("e.force:refreshView").fire();
                } else if(resp == 'NOT_APPROVED'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error", 
                        "title": "Error!",
                        "message": "Concession is not approved to submit Case to FTP."
                    });
                    toastEvent.fire();
                    
                    $A.get("e.force:refreshView").fire();
                } else if(resp == 'EXCEPTION'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error", 
                        "title": "Error!",
                        "message": "Exception occured while submitting Case to FTP. Please check with your System Administrator."
                    });
                    toastEvent.fire();
                }
                
            }else if(state === "ERROR"){
                console.log('Problem updating call, response state '+state);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error", 
                    "title": "Error!",
                    "message": "Error occured while submitting Case to FTP. Please try again later."
                });
                toastEvent.fire();
            }else{
                console.log('Unknown problem: '+state);
            }
        });
        
        //send the request to updateCall
        $A.enqueueAction(updateCall);
    }
})