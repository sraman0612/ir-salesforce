({
    initialize : function(cmp, event, helper) {
        
        cmp.set("v.isLoading", true);

        cmp.find("newUserModal").set("v.saveButtonDisabled", true);
        cmp.find("newUserSuperUser").set("v.checked", false);
        cmp.find("terms").set("v.checked", false);              
        cmp.set("v.newUser", {IsSuperUser : false, Terms: false});
        cmp.set("v.selectedNewUserAdditionalSites", []);
        cmp.set("v.newUserAdditionalSiteOptions", []);
        cmp.set("v.selectedNewUserPrimarySite", "");
        cmp.set("v.showNewUserForm", false);        

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            searchTerm : cmp.get("v.searchTerm"),
            "siteId" : cmp.get("v.siteID"),
            pageSize : cmp.get("v.pageSize"),
            offSet : cmp.get("v.offSet"),
            totalRecordCount: cmp.get("v.totalRecordCount")
        });        

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
            
			utils.logObjectToConsole(responseVal);
				
			if (responseVal.success){		

                var siteOptions = [];

                for (var i = 0; i < responseVal.sites.length; i++){
                    
                    var site = responseVal.sites[i];
                    
                    siteOptions.push({
                        "label": site.Account_Name_Displayed__c,
                        "value": site.Id
                    });
                }

                cmp.set("v.contacts", responseVal.ccr.communityContacts);
                cmp.set("v.newUserPrimarySiteOptions", siteOptions);
                cmp.set("v.newUserAdditionalSiteOptions", siteOptions);                
                cmp.set("v.isSuperUser", responseVal.isSuperUser);
                cmp.set("v.totalRecordCount", responseVal.totalRecordCount); 
                cmp.set("v.communitySettings", responseVal.communitySettings);

                if (responseVal.userSettings.User_Selected_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.userSettings.User_Selected_List_Page_Size__c);
                }
                else if (responseVal.communitySettings.Default_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.communitySettings.Default_List_Page_Size__c);
                }
                else{
                    cmp.set("v.pageSize", 20);
                }           
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);       
        }    
        
        cmp.set("v.isLoading", false);
    },
    
    createContact : function(cmp, event, helper) {
        console.log("createContact");

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.createUser");
        var newUser = cmp.get("v.newUser");
        var selectedNewUserPrimarySite = cmp.get("v.selectedNewUserPrimarySite");
        var selectedNewUserAdditionalSites = cmp.get("v.selectedNewUserAdditionalSites");
        var newUserAdditionalSiteOptions = cmp.get("v.newUserAdditionalSiteOptions");
        var newUserSuperUserCmp = cmp.find("newUserSuperUser");
        var termsCmp = cmp.find("terms");
        var firstName = cmp.get("v.newUser.FirstName");
        var lastName = cmp.get("v.newUser.LastName");
        var primarySite = cmp.get("v.selectedNewUserPrimarySite");
        var email = cmp.get("v.newUser.Email");        

        // Get values from checkboxes
        newUser.IsSuperUser = newUserSuperUserCmp.get("v.checked");
        newUser.Terms = termsCmp.get("v.checked");

        // Validations
        var isValid = true;
        var input = cmp.find("newUserFirstName");

        if (!firstName || firstName.length == 0){
              
            input.setCustomValidity("First Name is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }        

        input = cmp.find("newUserLastName");

        if (!lastName || lastName.length == 0){
              
            input.setCustomValidity("Last Name is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }  

        input = cmp.find("newUserPrimarySite");

        if (!primarySite || primarySite.length == 0){
              
            input.setCustomValidity("Primary Site is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }     

        input = cmp.find("newUserEmail");

        if (!email || email.length == 0){
              
            input.setCustomValidity("Email is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }               

        console.log("isValid: " + isValid);

        if (isValid){

            // Determine if standard user hybrid profile should be assigned (i.e. if all additional sites were selected)
            if (!newUser.IsSuperUser && selectedNewUserAdditionalSites.length == newUserAdditionalSiteOptions.length){
                console.log("assigning hybrid standard user profile");
                newUser.IsHybridStandardUser = true;
            }

            var request = {
                "newUser" : newUser,
                "siteId" : selectedNewUserPrimarySite,
                "additionalSiteIds" : selectedNewUserAdditionalSites
            };

            utils.logObjectToConsole(request);

            action.setParams({
                "requestStr" : JSON.stringify(request)
            });

            utils.callAction(action, helper.handleCreateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
        }    
        else{

            cmp.set("v.isLoading", false);

            var toastEvent = $A.get("e.force:showToast");
            
            toastEvent.setParams({
                mode : "dismissible",
                duration : 5000,
                message : "Required fields are missing.  Please review each required field and provide a value.",
                type : "error"			        
            });
            
            toastEvent.fire();            
        }
    },

    handleCreateUserSuccess : function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	

				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "New user created successfully.",
			        type : "success"			        
			    });

                toastEvent.fire();	  

                helper.initialize(cmp, event, helper);            
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);
        }    
        
        cmp.set("v.isLoading", false);
    },         

    activateUser : function(cmp, event, helper, selectedContactID) {
        console.log("activateUser");

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.activateUser");
        var contactID = selectedContactID ? selectedContactID : cmp.get("v.contactID");

        action.setParams({
            "contactId" : contactID
        });

        utils.callAction(action, helper.handleActivateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleActivateUserSuccess: function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "User successfully activated.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();

                helper.initialize(cmp, event, helper);
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
                cmp.set("v.isLoading", false);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);        
            cmp.set("v.isLoading", false);
        }                    
    },    

    deactivateUser : function(cmp, event, helper, selectedContactID) {
        console.log("deactivateUser");

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.deactivateUser");
        var contactID = selectedContactID ? selectedContactID : cmp.get("v.contactID");

        action.setParams({
            "contactId" : contactID
        });

        utils.callAction(action, helper.handleDeactivateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleDeactivateUserSuccess: function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "User successfully deactivated.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();	   

                helper.initialize(cmp, event, helper);
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
                cmp.set("v.isLoading", false);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);       
            cmp.set("v.isLoading", false);
        }    
    },  
    
    onEditUser : function(cmp, event, helper, contactIdOverride){
        console.log("onEditUser");

        var className = event.getSource().get("v.class");
        let contactIdToUse;

        if (contactIdOverride){
            contactIdToUse = contactIdOverride;
        }
        else{
            contactIdToUse = className.substring(11);
        }

        cmp.set("v.selectedContactID", contactIdToUse);
    },

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})