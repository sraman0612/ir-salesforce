({
    init: function (cmp, event, helper) {
        console.log("CTS_IOT_Contact_List:init");
        
        var siteId = decodeURIComponent(window.location.search.substring(1).substring(8));
        console.log("site ID found: " + siteId);
        
        if (siteId && siteId.length <= 18 && siteId.startsWith("001")){
            cmp.set("v.siteID", siteId);
        }

        helper.initialize(cmp, event, helper);
    },

    onTermsChange : function(cmp, event, helper){
        console.log("onTermsChange");

        var utils = cmp.find("utils");
        var newUserModal = cmp.find("newUserModal");
        var termsCmp = cmp.find("terms");

        newUserModal.set("v.saveButtonDisabled", !termsCmp.get("v.checked"));        
    },

    onNewUserSuperUserChange : function(cmp, event, helper){

        var showNewUserAdditionalSites = cmp.get("v.showNewUserAdditionalSites");

        if (!showNewUserAdditionalSites){
            cmp.set("v.selectedNewUserAdditionalSites", []);
        }

        cmp.set("v.showNewUserAdditionalSites", !showNewUserAdditionalSites);
    },

    onEditUserFA : function(cmp, event, helper){

        let contactId = event.getParam("val");
        console.log("onEditUserFA: " + contactId);
        helper.onEditUser(cmp, event, helper, contactId);
    },
    
    onEditUser : function(cmp, event, helper){
        console.log("onEditUser");
        helper.onEditUser(cmp, event, helper);
    },

    onAddUser : function(cmp, event, helper){
        console.log("onAddUser");        
        cmp.set("v.showNewUserForm", true);
    },

    onActivateUserFA : function(cmp, event, helper){
        console.log("onActivateUserFA");        
        
        let contactId = event.getParam("val");
        console.log("onEditUserFA: " + contactId);
        helper.activateUser(cmp, event, helper, contactId); 
    }, 

    onActivateUser : function(cmp, event, helper){
        console.log("onActivateUser");        
        
        var className = event.getSource().get("v.class");
        var contactID = className.substring(11);
        helper.activateUser(cmp, event, helper, contactID); 
    },        

    onDeactivateUserFA : function(cmp, event, helper){
        console.log("onDeactivateUserFA"); 
        
        let contactId = event.getParam("val");
        console.log("onEditUserFA: " + contactId);
        helper.deactivateUser(cmp, event, helper, contactId);
    },

    onDeactivateUser : function(cmp, event, helper){
        console.log("onDeactivateUser");        
        
        var className = event.getSource().get("v.class");
        var contactID = className.substring(11);
        helper.deactivateUser(cmp, event, helper, contactID); 
    },    

    handleModalSave : function(cmp, event, helper){
        console.log("handleModalSave");

        var modalID = event.getParams().modalID;
        console.log("modalID: " + modalID);

        if (modalID == "newUserModal"){
            helper.createContact(cmp, event, helper);
        }
        else if (modalID == "editUserModal"){
            cmp.set("v.selectedContactID", "");
            helper.initialize(cmp, event, helper);
        }
    },

    handleModalCancel : function(cmp, event, helper){
        console.log("handleModalCancel");

        var modalID = event.getParams().modalID;
        console.log("modalID: " + modalID);

        if (modalID == "newUserModal"){
            cmp.set("v.newUser", {"IsSuperUser":false});
            cmp.set("v.showNewUserForm", false);
        }
        else if (modalID == "editUserModal"){
            cmp.set("v.selectedContactID", "");
        }
    },

    onNewUserPrimarySiteSelect : function(cmp, event, helper){

        // Remove primary site from additional sites (options and values)
        var selectedNewUserPrimarySite = cmp.get("v.selectedNewUserPrimarySite");

        if (selectedNewUserPrimarySite != ""){
        
            var newUserPrimarySiteOptions = cmp.get("v.newUserPrimarySiteOptions");
            var selectedNewUserAdditionalSites = cmp.get("v.selectedNewUserAdditionalSites");
            var newOptions = [], newValues = [];

            for (var i = 0; i < newUserPrimarySiteOptions.length; i++){

                var option = newUserPrimarySiteOptions[i];
                var siteId = option.value;

                if (siteId != selectedNewUserPrimarySite){
                    newOptions.push(option);
                }
            }

            for (var i = 0; i < selectedNewUserAdditionalSites.length; i++){

                var value = selectedNewUserAdditionalSites[i];

                if (value != selectedNewUserPrimarySite){
                    newValues.push(value);
                }
            }            

            cmp.set("v.newUserAdditionalSiteOptions", newOptions);
            cmp.set("v.selectedNewUserAdditionalSites", newValues);            
        }
    }, 

    handlePageChange: function(cmp, event, helper){
        console.log("handlePageChange");
        cmp.set("v.offSet", event.getParam("offSet"));
        helper.initialize(cmp, event, helper);
    },

    handleGlobalPageSizeChange: function(cmp, event, helper){
        console.log("handleGlobalPageSizeChange");
        let pageSize = event.getParam("pageSize");
        console.log("pageSize: " + pageSize);
        cmp.set("v.pageSize", pageSize);
        helper.initialize(cmp, event, helper);
    },    

    handlePageSizeChange: function(cmp, event, helper){
        console.log("handlePageSizeChange");
        cmp.set("v.pageSize", event.getParam("pageSize"));
        helper.initialize(cmp, event, helper);

        // Notify any other sibling list view components of the list page size change
        var pageSizeChangeEvent = $A.get("e.c:CTS_IOT_List_Page_Size_Change");
        pageSizeChangeEvent.setParams({pageSize: event.getParam("pageSize")});
        pageSizeChangeEvent.fire();           
    },
    
    handleSearchTermChange: function(cmp, event, helper){
        console.log("handleSearchTermChange: " + event.getParam("searchTerm"));

        cmp.set("v.searchTerm", event.getParam("searchTerm"));
        helper.initialize(cmp, event, helper);
    }    
})