({
    getInit : function(cmp, event, helper) {
        
        var action = cmp.get("c.getInit");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var initResponse = JSON.parse(response.getReturnValue());
                var outOfOfficeSettings = initResponse.outOfOfficeSettings;
                var caseRoutings = initResponse.caseRoutings;
                
                cmp.set("v.outOfOfficeSettings", outOfOfficeSettings);
                cmp.set("v.caseRoutings", caseRoutings);
                
                //get primary user/queue
                for(var i = 0; i < caseRoutings.length; i++){
                    if(caseRoutings[i].Name.charAt(caseRoutings[i].Name.length - 1) === "_"){
                        //check if primary is queue or user
                        if(caseRoutings[i].Backup_User_Queue_ID__c.substring(0, 3) === "005"){
                            //005 is user
                            cmp.set("v.userActive", true);
                            cmp.set("v.backupUserId", caseRoutings[i].Backup_User_Queue_ID__c);
                        }
                        else{
                            cmp.set("v.userActive", false);
                            cmp.set("v.backupQueueId", caseRoutings[i].Backup_User_Queue_ID__c);
                        }
                    }
                }
                
                //default to user for new users
                if(caseRoutings.length === 0){
                    cmp.set("v.userActive", true);
                }
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors) {
                    
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown error");
                }
            }
        });     
        
        $A.enqueueAction(action);   	
    },
    
    save : function(cmp, event, helper){
        
        console.log("CTS_TechDirect_OutOfOfficeAsst.helper.save");
        
        var saveRequest = {};
        saveRequest.outOfOfficeSettings = cmp.get("v.outOfOfficeSettings");
        saveRequest.caseRoutings = cmp.get("v.caseRoutings");
        
        var action = cmp.get("c.save");
        
        action.setParams({
            'saveRequest' : JSON.stringify(saveRequest)
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                console.log("raw response: " + response.getReturnValue());
                var saveResponse = JSON.parse(response.getReturnValue());
                if(saveResponse.success){
                	var message = "Your out of office settings have successfully been saved.";
                    helper.showSuccessToast(cmp, event, helper, message);
                } else{
                    helper.showErrorToast(cmp, event, helper, saveResponse.errorMessage);
                }
                
                console.log(saveResponse);
                
                for(var i = 0; i < saveResponse.warningMessage.length; i++){
                	helper.showWarningToast(cmp, event, helper, saveResponse.warningMessage[i]);
                }
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                var message = '';
                
                if (errors) {
                    
                    if (errors[0] && errors[0].message) {
                        
                        console.log("Error message: " + errors[0].message);
                        message = errors[0].message;
                    }
                } 
                else {
                    console.log("Unknown error");
                }
                
                helper.showErrorToast(cmp, event, helper, message);
            }
        });     
        
        $A.enqueueAction(action);   		
    },
    
    //create Case Routings
    //LS 3/2 | Currently only creating primary routing
    //			Will need enhancement for multiple record-type based routings
    createRoutings : function(cmp, event, helper){
        //TODO: find and remove Primary case routing from routings list
        //currently clearing list
        var tempRoutings = [];
        
        //TODO: handle additional routings list/table
        
        //handle adding primary Case Routing
        var routingId;
        if(cmp.get("v.backupUserId") != null && cmp.get("v.backupUserId") != ""){
            routingId = cmp.get("v.backupUserId");
        }
        else{
            routingId = cmp.get("v.backupQueueId");
        }
        
        //create primary routing setting
        //Name will end with "_" char
        var newPrimaryRouting = {
            Backup_User_Queue_ID__c : routingId,
            Name : $A.get('$SObjectType.CurrentUser.Id') + "_",
            User_ID__c : $A.get('$SObjectType.CurrentUser.Id')
        };
        
        tempRoutings.push(newPrimaryRouting);
        cmp.set("v.caseRoutings", tempRoutings);
    },
    
    //function to show success Toast popup and handle success messaging on client side
    showSuccessToast : function(cmp, event, helper, message){
        var successEvent = $A.get("e.force:showToast");
        successEvent.setParams({
            "title": "Success",
            "message": message,
            "type": "success"
        });
        
        successEvent.fire();
    },
    
    //function to show error Toast popup and handle error on client side
    showErrorToast : function(cmp, event, helper, message){
        var errorEvent = $A.get("e.force:showToast");
        errorEvent.setParams({
            "title": "Error",
            "message": message,
            "type": "error"
        });
        
        errorEvent.fire();
    },
    
    //function to show error Toast popup and handle error on client side
    showWarningToast : function(cmp, event, helper, message){
        var errorEvent = $A.get("e.force:showToast");
        errorEvent.setParams({
            "title": "Warning",
            "message": message,
            "type": "warning",
            "duration": 10000
        });
        
        errorEvent.fire();
    }
})