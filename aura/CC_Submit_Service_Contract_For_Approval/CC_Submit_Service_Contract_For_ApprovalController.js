({
    handleContractLoaded : function(cmp, event, helper) {

        var eventParams = event.getParams();

        if (eventParams.changeType === "LOADED") {
           
            console.log("contract recorded loaded successfully.");
            let contractRecord = cmp.get("v.contractRecord");
            
            if (contractRecord.CC_Billing_Type__c == "Bill to Internal (Non - Revenue/Deal Consideration)"){
            
                if (confirm("For approval of Internal Non-Revenue contracts, an ODA or proposal must be attached to the contract record.")){
                    helper.submitContractForApproval(cmp, event, helper);
                }
                else{

                    // Close the quick action
                    $A.get("e.force:closeQuickAction").fire();                     
                }
            }
            else{
                helper.submitContractForApproval(cmp, event, helper);
            }            
        } 
        else {

            console.log("contract recorded failed to load.");

            let toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({
                mode : "dismissible",
                duration : 5000,
                message : cmp.get("v.contractRecordLoadError"),
                type : "error"			        
            });	
            
            toastEvent.fire();	  
            
            // Close the quick action
            $A.get("e.force:closeQuickAction").fire();              
        }
    }
})