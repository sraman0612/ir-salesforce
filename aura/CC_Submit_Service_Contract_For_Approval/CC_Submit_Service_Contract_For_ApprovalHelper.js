({
    submitContractForApproval: function (cmp, event, helper) {
        console.log("submitContractForApproval");
            
        let utils = cmp.find("utils");
        let action = cmp.get("c.submitContractForApproval");
        action.setParams({"contractId" : cmp.get("v.recordId")});
            
        utils.callAction(action, helper.handleSubmitContractForApprovalSuccess, helper.handleActionCallFailure, cmp, helper);
    },
        
    handleSubmitContractForApprovalSuccess: function(cmp, event, helper, response){
    
        try{
            
            let responseVal = response.getReturnValue();
        
            console.log(responseVal);
                
            let toastEvent = $A.get("e.force:showToast");
                
            if (responseVal.success){		
                                
                toastEvent.setParams({
                    mode : "dismissible",
                    duration : 3000,
                    message : "Contract successfully submitted for approval.",
                    type : "success"			        
                });	
                
                toastEvent.fire();
                
                // Refresh the contract
                $A.get('e.force:refreshView').fire();			
            }
            else{
            
                console.log("failed getInfo response");

                toastEvent.setParams({
                    mode : "dismissible",
                    duration : 5000,
                    message : responseVal.errorMessage,
                    type : "error"			        
                });	
                
                toastEvent.fire();			
            }
        }
        catch(e){

            console.log("exception: " + e);

            toastEvent.setParams({
                mode : "dismissible",
                duration : 5000,
                message : e,
                type : "error"			        
            });	
            
            toastEvent.fire();	            
        }    

        // Close the quick action
        $A.get("e.force:closeQuickAction").fire();        
    },
        
    handleActionCallFailure: function(cmp, event, helper, response){
                    
        console.log("failed action call");
        
        let toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();   
        
        // Close the quick action
        $A.get("e.force:closeQuickAction").fire();        
    }    
})