({
	init : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        
        var action = cmp.get("c.getData");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === 'SUCCESS') {
            	var res = JSON.parse(response.getReturnValue());
                cmp.set("v.data", res);
                cmp.set("v.size", res.length);
                console.log('res0**', res);
            } else {
            	console.log('error*gen list*' + state);
            }
        });
        $A.enqueueAction(action);
	},
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    }
})