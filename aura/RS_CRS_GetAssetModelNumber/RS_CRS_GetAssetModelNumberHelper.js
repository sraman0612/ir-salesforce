({
    getModelNum : function(component, event) {
        //call controller method
        var action = component.get("c.getAssetModelNum");
        
        //set method parameters
        action.setParams({
            "assetId": component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            //valid response
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                
                //check whether response received with values
                if(resp == true){
                    //close quick action pop up
                    $A.get("e.force:closeQuickAction").fire();
                    
                    //add success message at top of record
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "success", 
                        "title": "Success!",
                        "message": "Model Number has been updated successfully.",
                        "duration": "500ms"
                        //"mode": "dismissible"
                    });
                    toastEvent.fire();

                    //refresh record view
                    $A.get('e.force:refreshView').fire();
                } else {
                    //set error message
                    $A.get("e.force:closeQuickAction").fire();
                    
                    //add success message at top of record
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error", 
                        "title": "Error!",
                        "message": "No Model Number found in Comfort Site for this Asset...",
                        "duration": "500ms"
                        //"mode": "dismissible"
                    });
                    toastEvent.fire();
                    
                    //refresh record view
                    $A.get('e.force:refreshView').fire();
                }
                
            }
        });
        $A.enqueueAction(action);
    }
})