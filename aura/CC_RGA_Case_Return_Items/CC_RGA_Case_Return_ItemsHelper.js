({
	getOrderLineItems : function(cmp, event, helper) {
		console.log("helper.getOrderLineItems");
	
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getOrderLineItems");
        
        action.setParams({
        	"caseId" : cmp.get("v.recordId"),
        	"orderId" : cmp.get("v.caseRecord").CC_Order__c
    	});
            
        utils.callAction(action, helper.handleGetOrderLineItemsSuccess, helper.handleActionCallFailure, cmp, helper);                 	
	},
	
   handleGetOrderLineItemsSuccess: function(cmp, event, helper, response){
	    console.log("handleGetOrderLineItemsSuccess");
	    
	    var utils = cmp.find("utils");
	    utils.logObjectToConsole(response.getReturnValue());
	   
		try{
            
			var responseVal = response.getReturnValue();
				
			if (responseVal.success){		
               
               cmp.set("v.lineItems", responseVal.lineItems);    
               
               var selections = [];
               
               for (var i = 0; i < responseVal.lineItems.length; i++){
            	   
            	   var row = responseVal.lineItems[i];
            	   
            	   utils.logObjectToConsole(row);
            	   
            	   if (row.selected){
            		   selections.push(row.uniqueId);
            	   }
               }      	             
			}
			else{
			
                console.log("failed response");
                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();	
			}	   			
        }
        catch(e){
        
        	console.log("exception: " + e);
        	
			var toastEvent = $A.get("e.force:showToast");
			
		    toastEvent.setParams({
		        mode : "dismissible",
		        duration : 5000,
		        message : e,
		        type : "error"			        
		    });
		    
			toastEvent.fire();        	
        }    
        
        cmp.set("v.isLoading", false);
	},
	
	updateSelections : function(cmp, event, helper) {
		console.log("helper.updateSelections");
	
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.updateSelections");
        var lineItems = cmp.get("v.lineItems");
        var selectedOrderItems = [];
        var fieldValidation = true;
        
        for (var i = 0; i < lineItems.length; i++){
        	
        	var lineItem = lineItems[i];
        	        	
        	if (lineItem.selected){
        	
        		console.log("selected line item");
        		utils.logObjectToConsole(lineItem);
        		
        		if (lineItem.caseOrderItem.RGA_Type__c == "" || lineItem.caseOrderItem.RGA_Reason__c == ""){
        			fieldValidation = false;
        			break;
        		}
        		else if (lineItem.serialNumberSelections != undefined && lineItem.serialNumberSelections.length > 0){
        			lineItem.caseOrderItem.Serial_Numbers__c = lineItem.serialNumberSelections.join(';');
        		}
        	
        		selectedOrderItems.push(lineItem.caseOrderItem);
    		}        	
        }
        
        console.log("fieldValidation: " + fieldValidation);
        
        if (fieldValidation){
        
        	utils.logObjectToConsole(selectedOrderItems);
        
	        action.setParams({
	        	"caseId" : cmp.get("v.recordId"),
	        	"orderId" : cmp.get("v.caseRecord").CC_Order__c,
	        	"selectedOrderItems" : selectedOrderItems
	    	});
	            
	        utils.callAction(action, helper.handleUpdateSelectionsSuccess, helper.handleActionCallFailure, cmp, helper);     
        } 	
        else{
        	
			var toastEvent = $A.get("e.force:showToast");
			
		    toastEvent.setParams({
		        mode : "dismissible",
		        duration : 5000,
		        message : "RGA Type and RGA Reason are required on all selected items.",
		        type : "error"			        
		    });
		    
			toastEvent.fire();  
			
			cmp.set("v.isLoading", false);        	
        }
	},	
	
  handleUpdateSelectionsSuccess: function(cmp, event, helper, response){
	    console.log("handleUpdateSelectionsSuccess");
	    
	    var utils = cmp.find("utils");
	    utils.logObjectToConsole(response.getReturnValue());
	   
		try{
            
			var responseVal = response.getReturnValue();
			var confirmToast = $A.get("e.force:showToast");
				
			if (responseVal.success){		

			    confirmToast.setParams({
			        mode : "dismissible",
			        duration : 3000,
			        message : "RGA return items successfully saved.",
			        type : "success"			        
			    });
			}
			else{
			
                console.log("failed response");
                				
			    confirmToast.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			}
			
			confirmToast.fire(); 	
			
			cmp.set("v.showRelatedList", true);
			
			// Refresh the list
			helper.getOrderLineItems(cmp, event, helper);			 			
        }
        catch(e){
        
        	console.log("exception: " + e);
        	
			var toastEvent = $A.get("e.force:showToast");
			
		    toastEvent.setParams({
		        mode : "dismissible",
		        duration : 5000,
		        message : e,
		        type : "error"			        
		    });
		    
			toastEvent.fire();        	
        }    
        
        cmp.set("v.isLoading", false);
	},		
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);        
	}
})