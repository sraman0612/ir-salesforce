({
	initialize: function(cmp, event, helper) {	
		console.log("CC_RGA_Case_Return_Items.initialize");	
	},
	
	onRecordLoad: function(cmp, event, helper){
		console.log("onRecordLoad");
		
		var utils = cmp.find("utils");
		var caseRecord = cmp.get("v.caseRecord");		
		utils.logObjectToConsole(caseRecord);
		
		if (caseRecord.CC_Order__c == null || caseRecord.CC_Order__c == ""){
			cmp.set("v.errorMsg", "An order must be linked to this case before RGA return items can be selected.");		
		}
		else{
			helper.getOrderLineItems(cmp, event, helper);
		}				
	},
	
    toggleSelectAll: function(cmp, event, helper){
    	
        console.log("toggleSelectAll");
        var selectAll = cmp.get("v.selectAll");
        var lineItems = cmp.get("v.lineItems");
        
        lineItems.forEach(function(lineItem, index){
            lineItem.selected = selectAll;
        });
        
        cmp.set("v.lineItems", lineItems);
    },
    
    handleSelectClick: function(cmp, event, helper){
        
        var selectAll = cmp.get("v.selectAll");
        var checkbox = event.getSource();
        
        // Reset the select all flag if one of the rows is unselected
        if (selectAll && !checkbox.get("v.value")){
            cmp.set("v.selectAll", false);
        }             
    },	
    
    onManageSelections: function(cmp, event, helper){
    	console.log("onManageSelections");
    	cmp.set("v.showRelatedList", false);
    },
    
    onCancel: function(cmp, event, helper){
    	console.log("onCancel");
    	cmp.set("v.showRelatedList", true);
    },    
	
	onSave: function(cmp, event, helper){
		helper.updateSelections(cmp, event, helper);
	}	
})