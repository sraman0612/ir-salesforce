({
    unlockContract: function (cmp, event, helper) {
        
		var utils = cmp.find("utils");
        var action = cmp.get("c.unlockContract");
        action.setParams({"contractId" : cmp.get("v.recordId")});
        
        utils.callAction(action, helper.handleUnlockContractSuccess, helper.handleActionCallFailure, cmp, helper);
    },
    
    handleUnlockContractSuccess: function(cmp, event, helper, response){
    
		try{
            
			var responseVal = JSON.parse(response.getReturnValue());
		
			console.log(responseVal);
			
			var toastEvent = $A.get("e.force:showToast");
				
			if (responseVal.success){		
                                
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 3000,
			        message : "Contract unlock request submitted successfully.  Please allow for up to 15 minutes for this to process.",
			        type : "success"			        
			    });	
			    
			    toastEvent.fire();
			    
			    // Refresh the contract
			    $A.get('e.force:refreshView').fire();			
			}
			else{
			
                console.log("failed getInfo response");

			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });	
			    
			    toastEvent.fire();			
			}

			// Close the quick action
			$A.get("e.force:closeQuickAction").fire();
        }
        catch(e){
        	console.log("exception: " + e);
        }    
	},
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();       
	}
})