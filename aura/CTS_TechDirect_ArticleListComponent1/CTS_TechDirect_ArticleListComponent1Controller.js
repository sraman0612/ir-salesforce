({
	doInit : function(component, event, helper) {   
        component.set('v.mycolumn', [
            {label: 'Id', fieldName: 'ArticleNumber', type: 'text', sortable: 'true'},
            {label: 'Title', fieldName: 'TitleURL', wraptext: 'TRUE',  type: 'url', sortable: 'true', initialWidth:385, typeAttributes: {
                label: { fieldName: 'Title' }
            }},
            {label: 'Type', fieldName: 'CTS_TechDirect_Article_Type__c', type: 'text', sortable: 'true'},
            // {label: 'Validation Status', fieldName: 'ValidationStatus', type: 'text', sortable: 'true'},
            {label: 'Language', fieldName: 'Language', type: 'text', sortable: 'true'}
        ]);        
        var action = component.get("c.getAllArticles");        
        action.setParams({
            "validationStatus" : "Publish",
            "searchKeyword" : "",
            "language" : "en_US",
            "product" : "",
            "category" : ""            
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.AllArticles', response.getReturnValue());
                var returnList = response.getReturnValue();
                helper.createPagination(component, returnList);                
            }
        });         
        $A.enqueueAction(action);
	},
    searchArticles : function(component, event, helper) {      
        var validationStatus = event.getParam("validationStatus");
        var keywordValue = event.getParam("searchKeyword");
        var languageValue = event.getParam("language");
        var productValue = event.getParam("product");
        var categoryValue = event.getParam("category");
        // set the handler attributes based on event data        
        var action = component.get("c.getAllArticles");
        action.setParams({
            "validationStatus" : validationStatus,
            "searchKeyword" : keywordValue,
            "language" : languageValue,
            "product" : productValue,
            "category" : categoryValue
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.AllArticles', response.getReturnValue());                
                var returnList = response.getReturnValue();
                helper.createPagination(component, returnList);
            }
        }); 
        $A.enqueueAction(action);
    },
    updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    getSelectedName: function (component, event) {        
        var selectedRows = event.getParam('selectedRows');
        for (var i = 0; i < selectedRows.length; i++){
           console.log(selectedRows[i].Id);
        }       
    },
    next: function (component, event, helper) {
    	helper.next(component, event);
	},
    previous: function (component, event, helper) {
    	helper.previous(component, event);
	}
})