({
	openDetail: function(component, event, helper) {
        var objectName, title;
        var recId = event.currentTarget.id;
        console.log('recId**', recId);
        var kaId;
        var contentDocId;
        var paginationList = component.get("v.paginationList");
        for (var cnt in paginationList) {
            var rec = paginationList[cnt];
            if (rec.sobj.Id == recId) {
                objectName = rec.searchType;
                title = rec.sobj.Title;
                if(objectName == 'Knowledge__kav') kaId = rec.sobj.KnowledgeArticleId;
                else if(objectName == 'ContentVersion') contentDocId = rec.sobj.ContentDocumentId;
                break;
            }
        }
        //For content, open the doc preview and return
        if(objectName == 'ContentVersion') {
            $A.get('e.lightning:openFiles').fire({
                recordIds: [contentDocId]
            });
            return;
        }
        console.log('before**');
        var modalBody;
    	var modalFooter;
        $A.createComponents([
            	["c:CC_RecordViewForm", {recordId: recId, objectName: objectName}],
            	["c:CC_RecordViewFormFooter", {recordId: kaId}]
            ],
            function(modalCmps, status, errorMessage) {
                console.log('status**', status);
                if (status === "SUCCESS") {
                    modalBody = modalCmps[0];
                    modalFooter = modalCmps[1];
                    component.find('overlayLib').showCustomModal({
                        header: title,
                        body: modalBody,
                        footer: objectName == 'Knowledge__kav' ? modalFooter : null,
                        showCloseButton: true,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            console.log('in modal callback!');
                        }
                    });
                } else if (status === "ERROR") {
                    console.log('ERROR: ', errorMessage);
                }
            }
        )
    },
    handleOpenFiles: function(cmp, event, helper) {
		console.log('Opening files: ' + event.getParam('recordIds').join(', ') 
			+ ', selected file is ' + event.getParam('selectedRecordId')); 
	}
})