({
    doInit: function (component, event, helper) {        
        // Sets custom tab label to "Knowledge Search"
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: "Knowledge Search"
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "standard:article",
                iconAlt: "Knowledge Search"
            });
        })
        .catch(function(error) {
            console.log(error);
        });
        
        var action1 = component.get("c.getPicklistValues");  
        action1.setParams({
            "object_name" : "Knowledge__kav",
            "field_name" : "Language",
            "default_val" : "en_US"     
        });
        action1.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {                                
                var returnList = response.getReturnValue();
                component.set("v.languageOptions", returnList); 
                for(var i = 0; i < returnList.length; i++) {
                    if(returnList[i].selected == true) {
                        component.find("languageId").set("v.value", returnList[i].value); 
                    }
                }    
            }
        });
        $A.enqueueAction(action1);
        
        var action2 = component.get("c.getPicklistValues");  
        action2.setParams({
            "object_name" : "Knowledge__kav",
            "field_name" : "ValidationStatus",
            "default_val" : "Publish"     
        });
       action2.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {                
                var returnList = response.getReturnValue();
                component.set("v.validationStatusOptions", returnList); 
                for(var i = 0; i < returnList.length; i++) {
                    if(returnList[i].selected == true) {
                        component.find("validationStatusId").set("v.value", returnList[i].value); 
                    }
                }                
            }
        });
        $A.enqueueAction(action2);   
              
        var action3 = component.get("c.getDataCategoryStructure");  
        action3.setParams({
            "object_name" : "KnowledgeArticleVersion",
            "data_category_group_name" : "IR_Global_Products_Group"
        });
        action3.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {                
                component.set("v.products", response.getReturnValue());               
            }
        });
        $A.enqueueAction(action3); 
        
        var action4 = component.get("c.getDataCategoryStructure");  
        action4.setParams({
            "object_name" : "KnowledgeArticleVersion",
            "data_category_group_name" : "IR_Global_Category_Group"
        });
        action4.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {                
                component.set("v.categories", response.getReturnValue());               
            }
        });
        $A.enqueueAction(action4); 
    },
    searchArticle: function (component, event, helper) {
        helper.searchArticle(component, event, helper);
    },
    resetSearchFilter: function (component, event, helper) {
        component.find("validationStatusId").set("v.value", "Publish");  
        component.find("keywordId").set("v.value", "");
        component.find("languageId").set("v.value", "en_US");
        component.set("v.selectedProduct", "");
        component.set("v.selectedCategory", "");
        helper.searchArticle(component, event, helper);
    },
    captureProductValue: function (component, event, helper) {
        var selectedProduct = event.getParam('name');
     	component.set('v.selectedProduct', selectedProduct);
 	},
    captureCategoryValue: function (component, event, helper) {
		var selectedCategory = event.getParam('name');       
       	component.set('v.selectedCategory', selectedCategory);
   	}
})