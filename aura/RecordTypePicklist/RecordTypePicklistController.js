({
    doInit : function(component, event, helper) {
        helper.callAction( component, 'c.getRecordTypes', {
            'objectName' : component.get('v.objectName'),
        }, function( data ) {
			data = Array.from(new Set(data.map(JSON.stringify))).map(JSON.parse);
            data.sort(function(a, b) { //Sort by label, alphabetically ASC
                return a.label == b.label ? 0 : +(a.label > b.label) || -1;
            });
            console.log(':data:::'+data);
            var defaultOption = {};
            defaultOption.label = component.get("v.defaultOptionLabel");
            for(var i = 0; i < data.length; i++){
                if(data[i].label == defaultOption.label){
                    defaultOption.value = data[i].value;
                }
            }
            component.set('v.value', defaultOption.value);
            component.set('v.options', data);                            
        });
    }
})