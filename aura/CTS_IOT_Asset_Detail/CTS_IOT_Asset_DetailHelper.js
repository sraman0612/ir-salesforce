({
    initialize : function(cmp, event, helper) {
        
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            "assetId" : cmp.get("v.assetID")
        });

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	

                cmp.set("v.assetDetail", responseVal.assetDetail);
                cmp.set("v.site", responseVal.site);
                cmp.set("v.communitySettings", responseVal.communitySettings);

                // Update breadcrumbs
                // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
                
                // navEvent.setParams({
                //     "label" : responseVal.assetDetail.asset.Name,
                //     "url" : "/asset-detail?asset-id=" + responseVal.assetDetail.asset.Id
                // });

                // navEvent.fire();                  
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
        	utils.handleFailure(e.message);
        }    
        
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        console.log("handleActionCallFailure");
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }  
})