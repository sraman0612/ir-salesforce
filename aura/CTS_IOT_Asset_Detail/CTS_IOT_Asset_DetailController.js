({
    init : function(cmp, event, helper) {
        console.log("CTS_IOT_Asset_Detail:init");

        var assetId = decodeURIComponent(window.location.search.substring(1).substring(9));
        console.log("asset ID found: " + assetId);
        
        cmp.set("v.assetID", assetId);
        cmp.set("v.assetDetail", {});

        helper.initialize(cmp, event, helper);
    },

    onViewDashboard : function(cmp, event, helper){
        console.log("onViewDashboard");

        var utils = cmp.find("utils");
        var assetId = cmp.get("v.assetID");
        utils.navigateToCommunityPage(cmp.find("navService"), "asset-losant-detail", {"asset-id": assetId}); 
    },

    onUpgrade : function(cmp, event, helper){
        console.log("onUpgrade");

        var utils = cmp.find("utils");
        var assetId = cmp.get("v.assetID");
        utils.navigateToCommunityPage(cmp.find("navService"), "submit-service-request", {"asset-id": assetId}); 
    }
})