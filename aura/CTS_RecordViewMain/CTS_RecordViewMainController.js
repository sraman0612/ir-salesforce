({
    doInit : function(cmp) {
        var recordId = cmp.get("v.recordId");
        console.log('view main**', recordId, cmp.get("v.kbRecord"));
        
        //Get associate Product Data Categories
        var actionDT = cmp.get("c.getKBWProdDataCats");
        actionDT.setParams({recordId: recordId});
        actionDT.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var prodDCGroupName = 'IR_Global_Products_Group';
                var prodDataCats = [];
                var res = response.getReturnValue();
                console.log('res**', res);
                cmp.set("v.kbRecord", res)
                var dcs = res.DataCategorySelections
                for(var i=0;i<dcs.length;i++) {
                    if(dcs[i].DataCategoryGroupName == prodDCGroupName) {
                        dcs[i].DataCategoryName = dcs[i].DataCategoryName.replace(/_/g,' ')
                        prodDataCats.push(dcs[i]);
                    }
                }
                cmp.set("v.prodDataCats", prodDataCats);
            } else {
                console.log('error**' + state);
            }
        });
        $A.enqueueAction(actionDT);
        
        //Update ViewStat
        var action = cmp.get("c.updateViewStat");
        action.setParams({recordId: recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var res = response.getReturnValue();
                console.log('res**' + JSON.stringify(res));
            } else {
                console.log('error**' + state);
            }
        });
        $A.enqueueAction(action);
    }
})