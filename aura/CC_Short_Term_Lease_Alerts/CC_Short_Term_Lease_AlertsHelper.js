({
    getAlerts : function(cmp, event, helper) { 

        var utils = cmp.find("utils");
        var action = cmp.get("c.getAlerts");
        action.setParams({"leaseId" : cmp.get("v.recordId")});
        
        utils.callAction(action, helper.handleGetAlertsSuccess, helper.handleActionCallFailure, cmp, helper);
    },

    handleGetAlertsSuccess: function(cmp, event, helper, response){

        try{
            
            var responseVal = response.getReturnValue();
        
            console.log(responseVal);
            
            if (responseVal.success){		

                for (var i = 0; i < responseVal.alerts.length; i++){

                    var alert = responseVal.alerts[i];

                    var toastEvent = $A.get("e.force:showToast");
                
                    toastEvent.setParams({
                        "type": alert.type,
                        "mode": alert.mode,
                        "duration": alert.duration,
                        "title": alert.title,
                        "message": alert.message
                    });
                    
                    toastEvent.fire();  
                }
            }
            else{            
                console.log("failed response");                		
            }	
        }
        catch(e){
            console.log("exception: " + e);
        } 
    },

    handleActionCallFailure: function(cmp, event, helper, response){
                
        console.log("failed action call");
        
        // Display an error message
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();		        
    }    
})