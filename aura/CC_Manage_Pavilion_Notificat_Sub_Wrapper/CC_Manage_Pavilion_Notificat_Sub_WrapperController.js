({
    doInit : function(cmp, event, helper) {

        console.log('CC_Manage_Pavilion_Notificat_Sub_Wrapper doInit');

        let displayInModal = cmp.get("v.displayInModal");

        if (!displayInModal){
            cmp.set("v.showModal", false);
        }

        if (cmp.get("v.pageReference") && cmp.get("v.pageReference").state){

            let recordId = cmp.get("v.pageReference").state.c__id;
            console.log('recordId: ' + recordId);

            cmp.set("v.recordId", recordId);
        }
    },

    onPageReferenceChange : function(cmp, event, helper){

        console.log("onPageReferenceChange");

        if (cmp.get("v.pageReference") && cmp.get("v.pageReference").state){

            let recordId = cmp.get("v.pageReference").state.c__id;
            console.log('recordId: ' + recordId);

            cmp.set("v.recordId", recordId);

            // Due to quick action caching, we must refresh the view now
            // https://salesforce.stackexchange.com/questions/295116/connectedcallback-disconnectedcallback-not-firing-on-2nd-display
            $A.get('e.force:refreshView').fire();
        }        
    },

    handleIsLoadingChange: function(cmp, event, helper){
        helper.handleIsLoadingChange(cmp, event, helper);
    },

    handleShowToast: function(cmp, event, helper){

        console.log("handleShowToast: " + JSON.stringify(event.getParams()));

        let duration = event.getParam("duration");
        let title = event.getParam("title");
        let type = event.getParam("variant");
        let message = event.getParam("message");        
        
        var toast = $A.get("e.force:showToast");

        if (toast){

            toast.setParams({
                "title": title,
                "message": message,
                "duration": duration,
                "type": type
            });

            toast.fire();    
        }    
    },

    handleClose : function(cmp, event, helper) {

        let recordid = event.getParam("recordid");
        let showModal = cmp.get("v.showModal");

        console.log("handleClose: " + recordid);
        console.log("recordid: " + recordid);
        console.log("showModal: " + showModal);

        if (recordid && !showModal){

            var navEvt = $A.get("e.force:navigateToSObject");

            navEvt.setParams({
                "recordId": recordid
            });

            console.log("firing navigation event..");
            navEvt.fire();
        }

        helper.handleIsLoadingChange(cmp, event, helper);

        cmp.set("v.showModal", false);
    }
})