({
    handleIsLoadingChange : function(cmp, event, helper) {

        let isLoading = event.getParam("isLoading");
        let isLoadingCallback = cmp.get("v.isLoadingCallback");

        if (isLoadingCallback){
            isLoadingCallback({isLoading});
        }  
    }
})