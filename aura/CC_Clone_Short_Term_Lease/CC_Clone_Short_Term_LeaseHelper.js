({
	processLeaseClone : function(cmp, event, helper) {
		console.log("helper.processLeaseClone");
		
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.cloneLeaseRecord");
        
        action.setParams({
        	"leaseId" : cmp.get("v.recordId"),
        	"isExtension" : cmp.get("v.isExtension"),
        	"beginDate" : cmp.get("v.beginDate"),
        	"endDate" : cmp.get("v.endDate")
    	});
    	
    	console.log("params--");
    	console.log("leaseId: " + action.getParam("leaseId"));
    	console.log("isExtension: " + action.getParam("isExtension"));
    	console.log("beginDate: " + action.getParam("beginDate"));
    	console.log("endDate: " + action.getParam("endDate"));
            
        utils.callAction(action, helper.handleCloneLeaseSuccess, helper.handleActionCallFailure, cmp, helper);                       	
	},
	
   handleCloneLeaseSuccess: function(cmp, event, helper, response){
	    console.log("handleCloneLeaseSuccess");
	    
	    var utils = cmp.find("utils");
	    utils.logObjectToConsole(response.getReturnValue());
	   
		try{
            
			var responseVal = JSON.parse(response.getReturnValue());
			console.log("handleCloneLeaseSuccess response");
			console.log(responseVal);
				
			if (responseVal.success){		
                                
                // Show success confirmation
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 3000,
			        message : "New lease successfully created.",
			        type : "success"			        
			    });
			    
				toastEvent.fire();	                                
                                
                // Redirect to the new lease
				var viewNewLease = $A.get("e.force:navigateToSObject");
				
				viewNewLease.setParams({
				      "recordId": responseVal.newLeaseId,
				      "slideDevName": "detail"
				});
				
				viewNewLease.fire();                                
			}
			else{
			
                console.log("failed getInfo response");
                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();	
			}	
			
            // Close the action panel
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();    			
        }
        catch(e){
        
        	console.log("exception: " + e);
        	
			var toastEvent = $A.get("e.force:showToast");
			
		    toastEvent.setParams({
		        mode : "dismissible",
		        duration : 5000,
		        message : e,
		        type : "error"			        
		    });
		    
			toastEvent.fire();        	
        }    
        
        cmp.set("v.isLoading", false);
	},
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);        
	}		
})