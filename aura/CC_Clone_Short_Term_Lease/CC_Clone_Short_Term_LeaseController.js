({
	initialize: function(cmp, event, helper) {	
		console.log('CC_Clone_Short_Term_Leae.initialize');
		
	},
	
	onRecordLoad: function(cmp, event, helper){
		console.log("onRecordLoad");
		
		var utils = cmp.find("utils");
		var leaseRecord = cmp.get("v.leaseRecord");		
		utils.logObjectToConsole(leaseRecord);		
		
		if (cmp.get("v.isExtension")){

			var endDateParts = leaseRecord.Ending_Date__c.split("-");
			var endDate = new Date(endDateParts[0], endDateParts[1] - 1, endDateParts[2]);
			endDate.setDate(endDate.getDate() + 1);
			console.log("endDate: " + endDate);

			var beginDate = endDate.toISOString().substring(0, 10);			
			console.log("beginDate: " + beginDate);

			// If this is an extension, default the begin date of the new lease to 1 day past the end date of the original lease			
			cmp.set("v.beginDate", beginDate);
		}
		else{
			cmp.set("v.beginDate", leaseRecord.Beginning_Date__c);
			cmp.set("v.endDate", leaseRecord.Ending_Date__c);
		}
	},
	
	onSave: function(cmp, event, helper){
		helper.processLeaseClone(cmp, event, helper);
	},
	
	onCancel: function(cmp, event, helper){
		event.preventDefault(); 
		$A.get("e.force:closeQuickAction").fire();
	}
})