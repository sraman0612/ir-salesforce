({    
	doInit: function(component, event, helper) {
		var productOptions = [
            { class: "optionClass", label: "--None--", value: ""},
            { class: "optionClass", label: "Small Reciprocating", value: "Small Reciprocating"},
            { class: "optionClass", label: "Small Rotary", value: "Small Rotary" },
            { class: "optionClass", label: "Large Rotary - Contact Cooled", value: "Large Rotary - Contact Cooled" },           
            { class: "optionClass", label: "Large Rotary - Oil Free", value: "Large Rotary - Oil Free" },           
            { class: "optionClass", label: "Dryers", value: "Dryers" },           
            { class: "optionClass", label: "Large Reciprocating", value: "Large Reciprocating" },           
            { class: "optionClass", label: "Centac", value: "Centac" },
            { class: "optionClass", label: "ECC | Turbo-Air", value: "ECC | Turbo-Air" }
        ];
        component.set("v.productOptions", productOptions); 
        
        var action1 = component.get("c.getPicklistValues");  
        action1.setParams({
            "object_name" : "Account",
            "field_name" : "CC_Primary_Country__c",
            "default_val" : "USA"     
        });
        action1.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {                                
                var returnList = response.getReturnValue();
                component.set("v.countryOptions", returnList); 
                for(var i = 0; i < returnList.length; i++) {
                    if(returnList[i].selected == true) {
                        component.find("selectedCountry").set("v.value", returnList[i].value); 
                    }
                }    
            }
        });
        $A.enqueueAction(action1);        
	},
    submit: function(component, event, helper) {          
        var assetNumber = component.find("assetNumber").get("v.value");  
        var productName = component.find("selectedProduct").get("v.value");
        var selectedRegion = 'North America';
        var selectedCountry = component.find("selectedCountry").get("v.value");        
        var contactObj = component.get("v.contactObj");
        contactObj.MailingCountry = selectedCountry;
        var caseObj = component.get("v.caseObj");   
        /***** Commented out category and sub category code
        var caseCategory = component.find("caseCategory").get("v.value");
        var caseSubCategory = component.find("caseSubCategory").get("v.value");
        caseObj.CTS_TechDirect_Category__c = caseCategory;
        caseObj.CTS_TechDirect_Sub_Category__c = caseSubCategory;
        *****/
        
        // Validation
        if($A.util.isEmpty(assetNumber) || $A.util.isUndefined(assetNumber)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Asset Number field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
		if($A.util.isEmpty(productName) || $A.util.isUndefined(productName)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Product field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        }        
        
        if($A.util.isEmpty(caseObj.Subject) || $A.util.isUndefined(caseObj.Subject)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Subject field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
        if($A.util.isEmpty(caseObj.Description) || $A.util.isUndefined(caseObj.Description)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Description field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
        if($A.util.isEmpty(contactObj.LastName) || $A.util.isUndefined(contactObj.LastName)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Name field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
        if($A.util.isEmpty(contactObj.Phone) || $A.util.isUndefined(contactObj.Phone)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Phone field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
        if($A.util.isEmpty(contactObj.Email) || $A.util.isUndefined(contactObj.Email)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Email field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
        if($A.util.isEmpty(selectedCountry) || $A.util.isUndefined(selectedCountry)) {
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Country field is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        } 
        
		var MAX_FILE_SIZE = 26214400; // Max file size 25 MB        
		// get the selected files using aura:id [return array of files]
        var fileInput = component.find("fileId").get("v.files");
        
        if(fileInput != null && fileInput.length > 0) {
            // get the first file using array index[0]  
            var file = fileInput[0];
            // check the selected file size, if select file size greter then MAX_FILE_SIZE,
            // then show a alert msg to user,hide the loading spinner and return from function  
            if (file.size > MAX_FILE_SIZE) {
                var fileSize = Math.round(file.size / 1024);
                var maxFileSize = Math.round(MAX_FILE_SIZE / 1024);
                component.set("v.fileName", 'Alert : File size cannot exceed ' + maxFileSize + ' KB.\n' + ' Selected file size: ' + fileSize + ' KB');
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'File size cannot exceed ' + maxFileSize + ' KB.\n' + ' Selected file size: ' + fileSize + ' KB',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire();
                return;
            } 
        } 
        
        var action1 = component.get("c.createCase");  
        action1.setParams({
            "assetNumber" : assetNumber,
            "productName" : productName,
            "caseObj" : caseObj,
            "contactObj" : contactObj,
            "region" : selectedRegion     
        });
        action1.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {             
                component.set("v.caseNumber", response.getReturnValue()["CaseNumber"]);
                component.set("v.isSuccess", true);  
                component.set("v.parentId", response.getReturnValue()["Id"]);                                  
                if (fileInput != null && fileInput.length > 0) {
                    helper.uploadHelper(component, event);
                }                
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Success', 
                    'message' : 'Case record is created successfully.',
                    'type' : 'success',
            		'mode' : 'pester'
                }); 
                showToast.fire();
            } else {
                var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Error has occurred. Kindly try again after some time.',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire();
            }
        });
        $A.enqueueAction(action1);
    },
    handleFilesChange: function(component, event, helper) {
        var fileName = '';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
})