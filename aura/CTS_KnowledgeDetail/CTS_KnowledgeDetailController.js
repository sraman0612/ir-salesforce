({    
    doInit : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        console.log('view main**', recordId, component.get("v.kbRecord"));
                
        //if recordId is null, grab it from the URL
        if(recordId == null){
            var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            console.log('sPageURL', sPageURL);
            console.log('sURLVariables', sURLVariables);
            
            var sParameterName;
            var i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                
                if (sParameterName[0] === 'aid') { //find aid
                    recordId = sParameterName[1];
                }
            }
            
        }
        
        //Get associate Product Data Categories
        var actionDT = component.get("c.getKBWProdDataCats");
        actionDT.setParams({recordId: recordId});
        actionDT.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var prodDCGroupName = 'IR_Global_Products_Group';
                var prodDataCats = [];
                var res = response.getReturnValue();
                console.log('res**', res);
                component.set("v.kbRecord", res)
                var dcs;
                
                if(res != null){
                	dcs = res.DataCategorySelections
                }
                if(dcs != null){
                    for(var i=0;i<dcs.length;i++) {
                        if(dcs[i].DataCategoryGroupName == prodDCGroupName) {
                            dcs[i].DataCategoryName = dcs[i].DataCategoryName.replace(/_/g,' ')
                            prodDataCats.push(dcs[i]);
                        }
                    }
                }
                component.set("v.prodDataCats", prodDataCats);
            } else {
                console.log('error**' + state);
            }
        });
        $A.enqueueAction(actionDT);
	}
})