({
    init: function(component,event,helper){
        let recordId = component.get('v.acct');
        console.log('ACCOUNT ID :: ' + recordId);
        let showSpinner = component.get('v.showSpinner');
        showSpinner = true;
        component.set("v.showSpinner", showSpinner);
        let action = component.get("c.getEntityRecordFiles");
        action.setParams({
            recordId: recordId,
            isRecordUploaded: false
        });
        
        action.setCallback(this, function(response) {
            let state = response.getState();
            
            if(state === "SUCCESS") {
                let result = response.getReturnValue();
                result.forEach(file =>{
                    file.fileSize = helper.formatBytes(component,file.Size, 2);
                    file.formattedDate = file.CreatedDate.toString();
                }); 
                component.set("v.files",result);
                component.set("v.displayFile", true);
                showSpinner = false;
                component.set("v.showSpinner", showSpinner);
            } else if(state === "ERROR") {
                showSpinner = false;
                component.set("v.showSpinner", showSpinner);
                /* Handle and print out errors to console*/
                let errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.error("**** ERROR ****", errors[0].message);
                        helper.showToast(component, event, helper);
                    }
                } 
            }
        });
        
        $A.enqueueAction(action);
    },

    UploadFinished : function(component, event, helper) {  
        helper.UploadFinished(component, event, helper);
    },

    previewFile :function(component, event, helper){  
        var rec_id = event.currentTarget.id; 
        $A.get('e.lightning:openFiles').fire({ 
            recordIds: [rec_id]
        });  
    },

    formatBytes : function(component, bytes,decimals) {
        if(bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
})