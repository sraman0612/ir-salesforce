({
    UploadFinished : function(component, event, helper) {  
        let recordId = component.get('v.acct');
        console.log('ACCOUNT ID :: ' + recordId);
        let showSpinner = component.get('v.showSpinner');
        showSpinner = true;
        component.set("v.showSpinner", showSpinner);
        let action = component.get("c.getEntityRecordFiles");
        action.setParams({
            recordId: recordId,
			isRecordUploaded: true
        });
        
        action.setCallback(this, function(response) {
            let state = response.getState();
            
            if(state === "SUCCESS") {
                let result = response.getReturnValue();  
                component.set("v.files",null);
                result.forEach(file =>{
                    file.fileSize = this.formatBytes(component, file.Size, 2);
                    file.formattedDate = file.CreatedDate.toString();
                }); 
                component.set("v.files",result);
                component.set("v.displayFile", true);
                showSpinner = false;
                component.set("v.showSpinner", showSpinner);
            } else if(state === "ERROR") {
                /* Handle and print out errors to console*/
                showSpinner = false;
                component.set("v.showSpinner", showSpinner);
                let errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.error("**** ERROR ****", errors[0].message);
                        this.showToast(component, event, helper);
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
    }, 

    formatBytes : function(component, bytes, decimals) {
        if(bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },

    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "type": "error",
            "title": "Error!",
            "message": "There was an error trying to upload your tax documents. Please try again with a smaller document size"
        });
        toastEvent.fire();
    }
})