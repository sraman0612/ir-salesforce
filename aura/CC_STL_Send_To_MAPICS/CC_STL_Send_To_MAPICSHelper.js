({
   sendLeaseToMAPICS: function (cmp, event, helper) {
        
        cmp.set("v.isLoading", true);
        cmp.set("v.errorMessgae", "");
		var utils = cmp.find("utils");
        var action = cmp.get("c.sendLeaseToMAPICS");
        action.setParams({"stlId" : cmp.get("v.recordId")});
        
        utils.callAction(action, helper.handleSendLeaseToMAPICSSuccess, helper.handleActionCallFailure, cmp, helper);
    },
    
    handleSendLeaseToMAPICSSuccess: function(cmp, event, helper, response){
    
		try{
            
			var responseVal = response.getReturnValue();
		
            console.log(responseVal);
            
			if (responseVal.success){		
                
                // Display a success message           
                var toastEvent = $A.get("e.force:showToast");

			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 3000,
			        message : "Request successful to send the lease to MAPICS.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();		
                
                // Close the action panel
                $A.get("e.force:closeQuickAction").fire();                 
			}
			else{
			
                console.log("failed response");
                
                // Display an error message
			    cmp.set("v.errorMessgae", responseVal.errorMessage);			
			}	
        }
        catch(e){
            
            // Display an error message
            cmp.set("v.errorMessgae", e);
        }    
        
        cmp.set("v.isLoading", false);  
	},
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        // Display an error message
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);
 		
        // Close the action panel
	    $A.get("e.force:closeQuickAction").fire(); 		        
	}
})