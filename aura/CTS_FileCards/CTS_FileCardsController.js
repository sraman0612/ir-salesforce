({
	init : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");  
        console.log('recordId before change--->'+recordId);
        var action = cmp.get("c.getData");
        //Start of Changes done for case no:- 00911953 
        if(recordId == null || recordId == '' ){
            var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            console.log('sPageURL', sPageURL);
            console.log('sURLVariables', sURLVariables);
            
            var sParameterName;
            var i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                if (sParameterName[0] === 'aid') { //find kaid
                    recordId = sParameterName[1];
                    cmp.set('v.recordId', recordId);
                    console.log('recordId------>'+recordId);
                }
            }
        }
        //End of Changes done for case no:- 00911953 
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === 'SUCCESS') {
            	var res = JSON.parse(response.getReturnValue());
                cmp.set("v.data", res);
                cmp.set("v.size", res.length);
            } else {
            	console.log('error**' + state);
            }
        });
        $A.enqueueAction(action);
	}
})