({
	init: function(cmp, event, helper){
		console.log("helper.init");
	
        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
		var recordId = cmp.get("v.recordId");
        var action = cmp.get("c.getInit");

        action.setParams({
            "carInfoId" : cmp.get("v.carInfoId")
        });

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	
	},
	
   handleGetInitSuccess: function(cmp, event, helper, response){
    
		try{
            
            var utils = cmp.find("utils");
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                                
                var itemOptions = [];
                
                responseVal.items.forEach(function(item, index){
                    
                    var itemOption = {
		                "label": item.ProductCode + " - " + item.Name,
		                "value": item.Id
		            };
		            
		            itemOptions.push(itemOption);
                });
                
                cmp.set("v.itemOptions", itemOptions);
                cmp.set("v.stlRecord", responseVal.stl);
			}
			else{
			
                console.log("failed getInfo response");
                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 10000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();	
			}	
        }
        catch(e){
        	console.log("exception: " + e);
        }    
        
        cmp.set("v.isLoading", false);
	},
    
    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);        
	},	
	
	save : function(cmp, event, helper) {
		console.log("save");
		
        var utils = cmp.find("utils");
        var stlRecord = cmp.get("v.stlRecord");
		
		// Build out all of the inputs including the custom one for the product picklist
		var fields = {};
    	
    	fields["Short_Term_Lease__c"] = cmp.find("Short_Term_Lease__c").get("v.value");
    	fields["Item__c"] = cmp.find("Item__c").get("v.value");
    	fields["Per_Car_Cost__c"] = cmp.find("Per_Car_Cost__c").get("v.value");
    	fields["Quantity__c"] = cmp.find("Quantity__c").get("v.value");
    	fields["Base_Cost__c"] = cmp.find("Base_Cost__c").get("v.value");
        fields["Cost_Adjustment__c"] = cmp.find("Cost_Adjustment__c").get("v.value");      
        
        if (stlRecord.Lease_Extension__c == 'Y'){
            fields["Accessory_Labor_Cost__c"] = cmp.find("Accessory_Labor_Cost__c").get("v.value");
        }
    	
    	console.log("fields to submit");		
		utils.logObjectToConsole(fields);    				
		
		cmp.find("editForm").submit(fields);
		
		cmp.set("v.isLoading", false); 
	},
	
	navigateToPreviousPage: function(cmp, event, helper){
		console.log("navigateToPreviousPage");
		
    	 var recordId = cmp.get("v.recordId");
    	 var carInfoRecord = cmp.get("v.carInfoRecord");
    	 
    	 var navEvt = $A.get("e.force:navigateToSObject");
    	 
    	 navEvt.setParams({
    		 "recordId": recordId  		 
    	 });
    	 
    	 navEvt.fire();	
	},
	
    refreshListHeight: function(cmp, event, helper) {
    	console.log("refreshListHeight");
    	
    	try{

    		var headerHeight = cmp.find("itemHeader").getElement().clientHeight;    		
    		cmp.find("Item__c").style.height = "calc(100% - " + headerHeight + "px)";
    	}
    	catch(e){console.log("error during refreshListHeight: " + e);}
    },	
    
    initInfiniteScroll : function(cmp, event, helper) {     

    	console.log("initInfiniteScroll");

        if (cmp.get("v.infiniteScrollInitialized")) {
            return;
        }
        
        console.log("initializing initInfiniteScroll");
        
        helper.refreshListHeight(cmp);

        var utils = cmp.find("utils");       
		var element = cmp.find("Item__c");
        var header = cmp.find("itemHeader").getElement();
        var loadingMoreDiv = cmp.find("loadingMoreMarker").getElement();
        
		utils.initInfiniteScroll(element);
        
        // Add infinite scroll listener
        element.addEventListener("optimizedScroll", function() {

            if ((element.scrollTop + element.clientHeight + header.clientHeight) >= loadingMoreDiv.offsetTop) {
            	helper.init(cmp, event, helper);
            }
        });
        
        console.log("initInfiniteScroll initialized");
        
        cmp.set("v.infiniteScrollInitialized", true);
	},     
})