({
	init: function(cmp, event, helper) {
		console.log('CC_STL_Car_Info_Edit: init');		
		
		// Check to see if the record ID is car info or its parent short term lease
		var recordId = cmp.get("v.recordId");
		
		if (recordId != null && (recordId.toUpperCase().startsWith("A9W") || recordId.toUpperCase().startsWith("ABZ"))){ // Car Info ID
			cmp.set("v.carInfoId", recordId);			
		}
				
		helper.init(cmp, event, helper);
	},
	
	handleLoad: function(cmp, event, helper) {
		console.log('handleLoad');
	
		var utils = cmp.find("utils");
		var record = event.getParam("recordUi").record;
		utils.logObjectToConsole(record);
		
		var recordId = cmp.get("v.recordId");
       	
       	console.log("recordId: " + recordId);
       		
		if (recordId.toUpperCase().startsWith("A9X") || recordId.toUpperCase().startsWith("ABB")){ // Lease ID		
			cmp.find("Short_Term_Lease__c").set("v.value", recordId);
		}
		else{
			cmp.find("Item__c").set("v.value", record.fields.Item__c.value);
		}

		cmp.set("v.carInfoRecord", record.fields);
		
		//helper.initInfiniteScroll(cmp, event, helper);
    },   
    
    initTest: function(cmp, event, helper){
    	
		var utils = cmp.find("utils");
		var itemHeader = cmp.find("itemHeader").getElement();
		var item = cmp.find("Item__c");
		
		utils.logObjectToConsole(cmp.getElements());
		utils.logObjectToConsole(itemHeader);
		utils.logObjectToConsole(item);
		utils.logObjectToConsole(item.get("v.value"));
		console.log("header client height: " + itemHeader.clientHeight);
		console.log("item style height: " + item.style.height);
    },
    
    onSave: function(cmp, event, helper){  
    	console.log("handleSubmit");
    	
    	cmp.set("v.isLoading", true); 
    	
    	var utils = cmp.find("utils");
    	
    	// Validations
    	var errors = [];
    	
    	if (cmp.find("Short_Term_Lease__c").get("v.value") == null){
    		errors.push("Short Term Lease is required.");
    	}
    	
    	if (cmp.find("Item__c").get("v.value") == null){
    		errors.push("Item is required.");
    	}
    	
    	if (cmp.find("Quantity__c").get("v.value") == null){
    		errors.push("Quantity is required.");
    	}     	    	    	
    	
    	console.log("errors: " + errors.length);
    	
    	if (errors.length > 0){
    	
    		var toastEvent = $A.get("e.force:showToast");
        
	        toastEvent.setParams({
	            mode : "dismissible",
	            duration : 5000,
	            message : errors.join(", "),
	            type : "error"			        
	        });
	        
	        toastEvent.fire();	  
	        
	        cmp.set("v.isLoading", false);   	
    	}
    	else{
    		helper.save(cmp, event, helper);
		}
    },
    
    handleSuccess: function(cmp, event, helper){
    	console.log("handleSuccess");
    	
    	helper.navigateToPreviousPage(cmp, event, helper);
    },
    
    handleError: function(cmp, event, helper){
    	console.log("handleError: " + event.getParam("message"));
    	
    	
    },
    
    onCancel: function(cmp, event, helper){
    	 
    	helper.navigateToPreviousPage(cmp, event, helper);
    }
})