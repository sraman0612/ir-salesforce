({
	getQuarter : function(cmp, event, helper, dt){
		console.log("getQuarter");
			
		console.log("date: " + dt);
				
		var quarter = Math.floor(dt.getMonth() / 3);
		
		return quarter + 1; 	
	},
	getDaysInMonth : function(cmp, event, helper, dt){
		console.log("getDaysInMonth");
				
		console.log("date: " + dt);
		
		var originalMonth = dt.getMonth();
		
		var newDt = new Date();
		newDt.setYear(dt.getYear());
		newDt.setMonth(dt.getMonth());
		
		// Start at the beginning of the month and keep adding a day until the month changes
		newDt.setDate(1);
		
		var days = 0;
		
		for (var i = 1; i < 33; i++){
		
			newDt.setDate(i);
			
			if (newDt.getMonth() == originalMonth){
				days = i;
			}		
			else{
				break;
			}	
		}
		
		console.log("days: " + days);
		
		return days;		
	}
})