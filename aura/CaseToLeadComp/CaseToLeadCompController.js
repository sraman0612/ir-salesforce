({
	doInit : function(component, event, helper) {
        var action = component.get("c.caseMapping");
        action.setParams({ lead_id : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var data = response.getReturnValue();
            if (state === "SUCCESS") {
       
                
                if(data.includes("Success"))
                {
                     helper.showSuccess(component, event, helper);
                    
               }
                else
                {
                    //var actionex =component.get("c.showError");
                    //actionex.setParams({errormessage:data});
                     //$A.enqueueAction(actionex);
                    helper.showError(component, event, helper,data);
                }
                
            }
            else
                
            {
                helper.showError(component, event, helper,data);
                
            }
        
                });
		$A.enqueueAction(action);
        
	},
    
   
})