({
	onInit : function(component, event, helper) {
		
        var filterField = component.get("v.filterField");
        var filterValue = component.get("v.filterValue");
        var fields = component.get("v.fieldNames");
        var objectName = component.get("v.objectName");
        var action = component.get("c.getData");
        action.setParams({
            objectName: objectName,
            fields: fields,
            filterField: filterField,
            filterValue: filterValue
        });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === 'SUCCESS') {
            	var res = response.getReturnValue();
                component.set("v.record", res);
                console.log('res**', res);
            } else {
            	console.log('error**' + state);
            }
        });
        $A.enqueueAction(action);
	}
})