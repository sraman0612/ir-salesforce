({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            searchTerm : cmp.get("v.searchTerm"),
            pageSize : cmp.get("v.pageSize"),
            offSet : cmp.get("v.offSet"),
            totalRecordCount: cmp.get("v.totalRecordCount")
        });           

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
		try{
            
            var utils = cmp.find("utils");
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	

                var sites = [];
                var siteParentFound = false;
                var siteParent = responseVal.siteHierarchy.siteParent;

                if (siteParent && siteParent.Name){
                    siteParentFound = true;
                    siteParent.isParent = true;
                    sites.push(siteParent);
                }

                cmp.set("v.siteParentFound", siteParentFound);

                if (responseVal.siteHierarchy.siteChildren.length > 0){

                    for (var i = 0; i < responseVal.siteHierarchy.siteChildren.length; i++){
                        
                        var siteChild = responseVal.siteHierarchy.siteChildren[i];
                        siteChild.isParent = siteParentFound ? false : true;
                        sites.push(siteChild);
                    }                  
                }

                cmp.set("v.sites", sites);
                cmp.set("v.totalRecordCount", responseVal.totalRecordCount); 
                cmp.set("v.communitySettings", responseVal.communitySettings);

                if (responseVal.userSettings.User_Selected_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.userSettings.User_Selected_List_Page_Size__c);
                }
                else if (responseVal.communitySettings.Default_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.communitySettings.Default_List_Page_Size__c);
                }
                else{
                    cmp.set("v.pageSize", 20);
                }               
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);	
        }    
        
        cmp.set("v.isLoading", false);
    },

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }       
})