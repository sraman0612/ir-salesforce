({
    getCustAssets : function(component, event, next, prev, offset) {
        //call controller method
        var action = component.get("c.fetchCustAssetsList");
        
        //set method parameters
        action.setParams({
            "caseId": component.get("v.recordId"),
            "next" : next,
            "prev" : prev,
            "off" : offset
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                //display error message as No Account associated with Case
                if(!storeResponse.isAccountLinked){
                    component.set("v.isMessage", true);
                    component.set("v.Message", "There is no Account associated with Case...");
                } 
                // if storeResponse size is 0 ,display no record found message on screen.
                else if (storeResponse.totalAssets == 0) {
                    component.set("v.isMessage", true);
                    component.set("v.Message", "No Assets associated with Account in case...");
                } else {
                    component.set("v.isMessage", false);
                    
                    // set searchResult list with return value from server.
                    component.set("v.assetList", storeResponse.assetList);
                    component.set("v.numberOfAssets", storeResponse.totalAssets);
                    component.set('v.offset',storeResponse.offset);
                    component.set('v.next',storeResponse.hasNext);
                    component.set('v.prev',storeResponse.hasPrev);
                }
                
            }
        });
        
        $A.enqueueAction(action);
    }
})