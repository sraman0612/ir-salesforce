({
    //on load of case record
    doInit : function(component, event, helper) {
        var next = false;
        var prev = false;
        var offset = 0;
        helper.getCustAssets(component, event, next, prev, offset);
    },
    //on click of Next button
    next : function(component, event, helper){
        var next = true;
        var prev = false;
        var offset = component.get("v.offset");
        helper.getCustAssets(component, event, next, prev, offset); 
    },
    //on click of Previous button
    previous : function(component, event, helper){
        var next = false;
        var prev = true;
        var offset = component.get("v.offset");
        helper.getCustAssets(component, event, next, prev, offset); 
    },
})