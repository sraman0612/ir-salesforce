({
    onManageUsersClick : function(cmp, event, helper) {
        console.log("onManageUsersClick");

        var utils = cmp.find("utils");
        utils.navigateToCommunityPage(cmp.find("navService"), "manage-users", {});    
    },
    
    onViewSiteHierarchyClick : function(cmp, event, helper) {
        console.log("onManageUsersClick");

        var utils = cmp.find("utils");
        utils.navigateToCommunityPage(cmp.find("navService"), "site-hierarchy", {}); 
    }
})