({
    CSV2JSON: function (component,csv,helper) {
        //  console.log('Incoming csv = ' + csv);
        
        //var array = [];
        var arr = []; 
        
        arr =  csv.split('\n');
        console.log('arr = '+arr);
        arr.pop();
        var jsonObj = [];
        var headers = arr[0].split(',');
        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
            var obj = {};
            for(var j = 0; j < data.length; j++) {
                console.log('obj headers = ' + headers[j].trim());
                console.log('data[j]--->'+data[j]);
                console.log('typeof data[j]'+typeof data[j]);
                var dateFieldLabels = ($A.get("$Label.c.Centac_Part_Date_Fields")).split(";");
                
                if(!(data[j] === "" && dateFieldLabels.includes(headers[j].trim()))){
                    console.log('In non blank date field');
                    if(data[j].includes(';')){
                        console.log('Data contains ;'+data[j]);
                        obj[headers[j].trim()] = helper.replaceAll(data[j].trim(),';',',');
                        console.log('after removing comma'+helper.replaceAll(data[j].trim(),';',','));
                    }else{
                        console.log('no comma field');
                        obj[headers[j].trim()] = data[j].trim();
                    } 
                    
                }
                
            }
            jsonObj.push(obj);
        }
        //console.log('jsonObj------->'+jsonObj);
        var json = JSON.stringify(jsonObj);
        console.log('json = '+ json);
        return json;
        
        
    },
    isDate : function(field) {
  		return field.constructor.toString().indexOf("Date") > -1;
	},
    replaceAll : function(text, search, replace){
  		return text.split(search).join(replace);
	},
    CreateParts : function (component,jsonstr,helper){
        // console.log('jsonstr' + jsonstr);
        var action = component.get('c.insertData');
        //  alert('Server Action' + action);  
        var centacQuoteId = component.get("v.centacQuoteId");  
        action.setParams({
            strfromle : jsonstr,
            centacQuoteId:centacQuoteId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {  
                var result=response.getReturnValue();
                if(result="SUCCESS"){
                    alert("Centac RFQ to Fill Inserted Succesfully");
                }else{
                    var errors = response.getError();
                    if(errors){
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        alert(errors[0].message);
                    }
                }
                    
                }     
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        alert(errors[0].message);
                    }
                } else {
                    //console.log("Unknown error");
                    alert('Unknown error ');
                }
            }
            component.set("v.isLoading", false);
            console.log('after-------->'+component.get("v.isLoading"));
        }); 
        
        $A.enqueueAction(action);    
        
    },
    
    
    //export helper start from here
    onLoad: function(component, event,helper) {
        //call apex class method
        var action = component.get('c.fetchRFQToFill');
        var recordId = component.get("v.recordId");
        if(recordId!= null){
        	component.set("v.centacQuoteId",recordId); 
        }
        var centacQuoteId = component.get("v.centacQuoteId");
        action.setParams({
            centacQuoteId:centacQuoteId
        });
        action.setCallback(this, function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in ListOfContact attribute on component.
                //Commented by CG as part of Descoped Object
               // component.set('v.ListOfParts', response.getReturnValue().dataList);
               // component.set('v.columns', response.getReturnValue().labelList);
               // var stockData = component.get('v.ListOfParts');
               // console.log('----In Success of onLoad---------->'+stockData.length);
               /* if(stockData.length>0){
                    helper.convertArrayOfObjectsToCSV(component,stockData,helper);
                }else{
                    alert('No parts to download!!');
                    component.set("v.isLoading", false);
                    return null;
                }*/
                   
                
            }else{
                return null;
            }
        });
        $A.enqueueAction(action);
        
    },
    
    convertArrayOfObjectsToCSV : function(component,objectRecords,helper){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = component.get('v.columns');
        console.log('keys-->'+keys);
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        console.log('csvStringResult------->'+csvStringResult);        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;   
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey]; 
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                } 
                // if condition for blank column display if value is empty
                if(objectRecords[i][skey] != undefined){
                    var fieldValue = objectRecords[i][skey];
                    console.log('typeof fieldValue------->'+typeof fieldValue);
                    console.log('fieldValue-------->'+fieldValue);
                    if(typeof fieldValue == 'string' && fieldValue.includes(',')){
                        csvStringResult += helper.replaceAll(fieldValue,',',';');
                    }else{
                        csvStringResult += objectRecords[i][skey];
                    }
                }else{
                    csvStringResult += '' ;
                    
                }
                counter++;
                
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        if (csvStringResult == null){return;} 
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'ExportData.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
        
        
        
        component.set("v.isLoading", false);
        
        
    },
})