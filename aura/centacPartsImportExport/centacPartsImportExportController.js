({    
    CreateRecord: function (component, event, helper) {
        component.set("v.isLoading", true);
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        //alert(file);
        if (file){
            //console.log("File");
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                
                //console.log("EVT FN");
                var csv = evt.target.result;
                //console.log('csv file contains'+ csv);
                var result = helper.CSV2JSON(component,csv,helper);
                //console.log('result = ' + result);
                //console.log('Result = '+JSON.parse(result));
                helper.CreateParts(component,result,helper);
                
            }
            reader.onerror = function (evt) {
                alert("error reading file");
            }
            //
            
        }else{
            alert("Please select file!!");
        }
        
        
    },
    
    showfiledata :  function (component, event, helper){
        
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        if (file) {
            component.set("v.showcard", true);
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                var csv = evt.target.result;
                var table = document.createElement("table");
                var rows = csv.split("\n");
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    if (cells.length > 1) {
                        var row = table.insertRow(-1);
                        var count= 0
                        for (var j = 0; j < cells.length && count<4; j++) {
                            count++
                            var cell = row.insertCell(-1);
                            cell.innerHTML = cells[j];
                        }
                    }
                }
                var divCSV = document.getElementById("divCSV");
                divCSV.innerHTML = "";
                divCSV.appendChild(table);
            }
            
            reader.onerror = function (evt) {
                alert("error reading file");
            }
            
        }
    },    
    
    // export data start from here    
    // ## function call on component load  
    loadPartsList: function(component, event, helper){
        component.set("v.isLoading", true);
        var recordId = component.get("v.recordId");
        if(recordId!= null){
            component.set("v.centacQuoteId",recordId); 
        }
        component.set("v.isLoading", false);
        
    },
    
    // ## function call on Click on the "Download As CSV" Button. 
    downloadCsv : function(component,event,helper){
        component.set("v.isLoading", true);
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.onLoad(component, event,helper); 
        
    }, 
    clearInputFile : function(component, event, helper){
        console.log('In clearInputFile-------->');
        var file =  document.getElementById("myFile");
        console.log('file---------->'+file);
        if(file){
            console.log('file.value---->'+file.value);
            
                console.log('file.value---->'+file.value);
            
                file.value = ''; //for IE11, latest Chrome/Firefox/Opera...
                console.log('file.value after---->'+file.value);
                
                    }
    }
})