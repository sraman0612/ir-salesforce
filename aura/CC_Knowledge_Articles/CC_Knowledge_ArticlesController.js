({
    init : function(component, event, helper) {
       /* we don't want to show anything on init...to confusing for user as they don't know what they are seeing
         var method = "c.getArticlesOnLoad";
         helper.callServer(method,null,component);
        */
    },
    newArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "ORDER BY LastModifiedDate DESC";
        helper.callServer(method,parameter,component);
    },
    mostViwedArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "ORDER BY ArticleTotalViewCount DESC";
        helper.callServer(method,parameter,component);
    },
    followingArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "EntitySubscription";
        helper.callServer(method,parameter,component);
    },
    mostHelpfulArticles :  function(component, event, helper) {
        var method = "c.getArticlesOnLoad";
        var parameter = "mostHelpfulArticles";
        helper.callServer(method,parameter,component);
    }
})