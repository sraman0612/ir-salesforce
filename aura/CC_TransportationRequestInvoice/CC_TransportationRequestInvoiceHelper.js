({
    createMAPICSInvoice: function (cmp, event, helper) {
         
         cmp.set("v.isLoading", true);
         cmp.set("v.errorMessage", "");
         var utils = cmp.find("utils");
         var action = cmp.get("c.createOrderInvoice");
         action.setParams({"caseId" : cmp.get("v.recordId")});
         
         utils.callAction(action, helper.handleCreateMAPICSInvoiceSuccess, helper.handleActionCallFailure, cmp, helper);
     },
     
    handleCreateMAPICSInvoiceSuccess: function(cmp, event, helper, response){
     
        try{
             
            var responseVal = response.getReturnValue();
         
            console.log(responseVal);
             
            if (responseVal.success){		
                 
                // Display a success message           
                var toastEvent = $A.get("e.force:showToast");
 
                toastEvent.setParams({
                    mode : "dismissible",
                    duration : 3000,
                    message : "Invoice successfully created.",
                    type : "success"			        
                });
                 
                toastEvent.fire();		
                 
                // Refresh the case page
                $A.get("e.force:refreshView").fire();                 
            }
            else{
             
                console.log("failed response");
                 
                // Display an error message
                var toastEvent = $A.get("e.force:showToast");
 
                toastEvent.setParams({
                    mode : "dismissible",
                    duration : 5000,
                    message : responseVal.errorMessage,
                    type : "error"			        
                });
                 
                toastEvent.fire();	                 		
                
                cmp.set("v.isLoading", false);
            }	
        }
        catch(e){
             
            // Display an error message
            var toastEvent = $A.get("e.force:showToast");
 
            toastEvent.setParams({
                mode : "dismissible",
                duration : 5000,
                message : e,
                type : "error"			        
            });
             
            toastEvent.fire();	
        }    
         
        cmp.set("v.isLoading", false);  
   },
     
    handleActionCallFailure: function(cmp, event, helper, response){
                     
        console.log("failed action call");
         
        // Display an error message
        var toastEvent = $A.get("e.force:showToast");
         
        toastEvent.setParams({
             mode : "dismissible",
             duration : 5000,
             message : response.getError(),
             type : "error"			        
        });
         
        toastEvent.fire();	
 
        cmp.set("v.isLoading", false);		        
    }
})