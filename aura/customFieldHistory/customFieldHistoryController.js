({
	init : function(component, event, helper) {
		var action = component.get("c.getHistoryTable");
		var parentId = component.get("v.recordId");
		var objectAPIName = component.get("v.ChildObject");
		component.set('v.tblColumns', [
            {label: 'DATE', fieldName: 'CreatedDate', type: 'date-local'},
            {label: 'USER', fieldName: 'CreatedBy', type: 'text'},
            {label: 'FIELD', fieldName: 'Field', type: 'text'},
            {label: 'OLD VALUE', fieldName: 'OldValue', type: 'text'},
            {label: 'NEW VALUE', fieldName: 'NewValue', type: 'text'}
        ]);
		action.setParams({
			sObjName: objectAPIName,
			parentRecordId: parentId
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
            console.log(response);
            console.log('### result is ' + result);
			if (state === "SUCCESS") {
			  console.log('Results ==',response.getReturnValue());
				var result = response.getReturnValue();
				console.log('Results ==',result);
				console.log('tblData ==',result);
				component.set("v.tblData", result);
			} else {
			  console.log('### state is ');
			  console.log(state);
			}
		});
		$A.enqueueAction(action);
	}
})