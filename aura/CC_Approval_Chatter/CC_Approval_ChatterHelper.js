({
    getRecordInApproval: function (cmp, event, helper) {
         
         cmp.set("v.isLoading", true);
         var utils = cmp.find("utils");
         var action = cmp.get("c.getRecordInApproval");
         
         action.setParams({"processInstanceWorkitemId" : cmp.get("v.recordId")});
         
         utils.callAction(action, helper.handleGetRecordInApprovalSuccess, helper.handleActionCallFailure, cmp, helper);
     },
     
     handleGetRecordInApprovalSuccess: function(cmp, event, helper, response){
     
        try{
             
            var responseVal = response.getReturnValue();
         
            console.log(responseVal);
             
            if (responseVal.success){		
                cmp.set("v.recordInApprovalId", responseVal.recordInApprovalId);
            }
            else{             
                console.log("failed response: " + responseVal.errorMessage);	
            }	
        }
        catch(e){
            console.log("exception: " + e);
        }    
         
        cmp.set("v.isLoading", false);  
    },
     
    handleActionCallFailure: function(cmp, event, helper, response){
                     
        console.log("failed action call: " + response.getError());
        cmp.set("v.isLoading", false);		        
    }
 })