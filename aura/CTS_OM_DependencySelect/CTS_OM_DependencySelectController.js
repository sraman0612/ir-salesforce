({        
    onInit: function(component, event, helper) {                
        
        component.set("v.showSpinner", true);
        let objectName = component.get('v.objectName');
        let fields = component.get('v.fields');
        let params = {
            fields: fields,
            objectName: objectName
        };
        console.log('picklist initialized with fields**', fields);
        helper.apex(component, 'populatePicklists', params)
            .then((result) => {
                console.log('result**', result);
                let firstSetCtrlVal;
                let selectedCat = event.getParam('selectedCat');
                if(selectedCat) firstSetCtrlVal = selectedCat;
                else firstSetCtrlVal = component.get("v.firstSetCtrlVal");
                if(firstSetCtrlVal && firstSetCtrlVal != null) helper.setLevel2Select(component, event, result);
                else {
                	helper.initHelper(component, event, result);//More intialization logic
					helper.setInitVals(component, event); //Populate the value from record
            	}
                component.set("v.showSpinner", false);
            })
            .catch((error) => {
                console.log('error**', error);
                component.set("v.showSpinner", false);
            });            
    },
    nextLevel: function(component, event, helper) {
		helper.nextLevel(component, event);       
    },
	openPicklist: function(component, event, helper) {
        $A.util.addClass(component.find('opener'), 'slds-is-open');
    },
    closePicklist: function(component, event, helper) {
        if(event.relatedTarget == undefined) $A.util.removeClass(component.find('opener'), 'slds-is-open');
    },
    selectPick: function(component, event, helper) {
		helper.selectPick(component, event, null, null);
    },
	previousLevel: function(component, event, helper) {
		let currentPickVals = component.get('v.currentPickVals');
		let currentLevel = component.get('v.currentLevel');
        
        let currentClickedVal = component.get('v.currentClickedVal');
        
        let allPicks = JSON.parse(JSON.stringify(component.get('v.allPickVals')));      
        let keys = JSON.parse(JSON.stringify(component.get('v.allKeys')));
        keys.sort();        
        
        if (keys.length > 0) {
            //let previousVals = Object.keys(allPicks[keys[currentLevel - 1]]);
            //LS 4/2
            let previousVals;
            if(currentLevel >= 2)
                previousVals = allPicks[keys[currentLevel - 2]][component.get('v.selectedVals')[currentLevel-2]];
            else
                previousVals = Object.keys(allPicks[keys[currentLevel - 1]]);
            
            let previousValsObj = [];            
            previousVals.forEach(val => {
                let previousValObj = {};
                previousValObj['field'] = val;
                previousValObj['hasNext'] = allPicks[keys[currentLevel - 1]] !== undefined ? (allPicks[keys[currentLevel - 1]][val]).length > 0 : false;
                previousValsObj.push(previousValObj);
            });
        	component.set('v.currentPickVals', previousValsObj);        
        	component.set('v.currentLevel', currentLevel - 1);
        }
    }    
})