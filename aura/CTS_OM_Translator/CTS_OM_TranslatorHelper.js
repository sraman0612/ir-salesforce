({
    translateVals : function(component, event, fields, relatedList) {
        
        let action = component.get("c.translate");
        
        action.setParams({recordId : component.get("v.recordId"), fields: relatedList ? null : fields, relatedList: fields ? null : relatedList });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                let result = response.getReturnValue();
                 //let translations = result[0] != null && result[0].length > 0  ? JSON.parse(result[0]).data.translations : null;
                
                if(relatedList != null) {
                    component.set("v." + relatedList, result[0]);
                    //if(relatedList == 'emailMessages') component.set("v.emailTranslations", translations);
                    //else if(relatedList == 'caseComments') component.set("v.commentTranslations", translations);
                } /*else {
                    component.set("v.translations", translations);
                }*/
                //console.log('translations result**', relatedList, result);                                                                
                component.set("v.translated", true);
            } else {
                console.log('error state**', state);
            }
        });
        $A.enqueueAction(action);
    }
})