({
    init : function(cmp, event, helper) {

        console.log("CTS_IOT_Asset_List-init");

        cmp.set("v.equipmentTypeFilters", 
            [{'label': 'All Equipment Types', 'value': ''},
            {'label': 'Compressors', 'value': 'Compressors'},
            {'label': 'Dryers', 'value': 'Dryers'}]);        

        cmp.set("v.helixStatusFilters", 
            [{'label': 'All Helix Statuses', 'value': ''},
            {'label': 'Helix Enabled', 'value': 'True'},
            {'label': 'Helix Disabled', 'value': 'False'}]);
        
        var urlBase = decodeURIComponent(window.location);
        var siteId = decodeURIComponent(window.location.search.substring(1).substring(8));
        console.log("site ID found: " + siteId);
        
        if (siteId && siteId.length <= 18 && siteId.startsWith("001")){
            cmp.set("v.siteID", siteId);
        }

        console.log("urlBase: " + urlBase);
/*
        if (urlBase.endsWith("my-equipment")){
        
            // Update breadcrumbs
            var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
            
            navEvent.setParams({
                "label" : "COMPRESSED AIR EQUIPMENT",
                "url" : "/my-equipment"
            });

            navEvent.fire();
        }
*/        

        helper.initialize(cmp, event, helper);
    },
    
    onMouseOverHelixIcon : function(cmp, event, helper){
        console.log("onMouseOverHelixIcon");   

        var utils = cmp.find("utils");
        var imgSource = event.target;
        imgSource.src = cmp.get("v.helixIconURLHover");
    },
    
    onMouseOutHelixIcon : function(cmp, event, helper){
        console.log("onMouseOutHelixIcon");   
           
        var utils = cmp.find("utils");
        var imgSource = event.target;
        imgSource.src = cmp.get("v.helixIconURLDefault");
    },      

    onMouseOverServiceRequestIcon : function(cmp, event, helper){

        var utils = cmp.find("utils");
        var cmpSource = event.getSource();
        cmpSource.set("v.variant", "error");
    },
    
    onMouseOutServiceRequestIcon : function(cmp, event, helper){
          
        var utils = cmp.find("utils");
        var cmpSource = event.getSource();
        cmpSource.set("v.variant", "");
    },
    
    onUpgrade : function(cmp, event, helper){
        console.log("onUpgrade");
        var utils = cmp.find("utils");
        utils.navigateToCommunityPage(
            cmp.find("navService"), 
            "submit-service-request", 
            {
                "asset-id": event.currentTarget.dataset.assetId,
                type : "Upgrade",
                description: $A.get("$Label.c.CTS_IOT_Administration_Asset_Service_Request_Upgrade_Default"),
                priority: "3 - Medium"
            }
        );       
    },

    onViewDashboard : function(cmp, event, helper){
        console.log("onViewDashboard");
        let communitySettings = cmp.get("v.communitySettings");
        window.open("/s/asset/" + event.currentTarget.dataset.assetId + "?tabset-" + communitySettings.Asset_Real_Time_Data_Tabset_ID__c, "_self");
    }, 

    handlePageChange: function(cmp, event, helper){
        console.log("handlePageChange");
        cmp.set("v.offSet", event.getParam("offSet"));
        helper.initialize(cmp, event, helper);
    },

    handleGlobalPageSizeChange: function(cmp, event, helper){
        console.log("handleGlobalPageSizeChange");
        let pageSize = event.getParam("pageSize");
        console.log("pageSize: " + pageSize);
        cmp.set("v.pageSize", pageSize);
        helper.initialize(cmp, event, helper);
    },

    handlePageSizeChange: function(cmp, event, helper){
        console.log("handlePageSizeChange");
        cmp.set("v.pageSize", event.getParam("pageSize"));      
        helper.initialize(cmp, event, helper);

        // Notify any other sibling list view components of the list page size change
        var pageSizeChangeEvent = $A.get("e.c:List_Page_Size_Change");
        pageSizeChangeEvent.setParams({pageSize: event.getParam("pageSize")});
        pageSizeChangeEvent.fire();        
    },

    handleSearchTermChange: function(cmp, event, helper){
        console.log("handleSearchTermChange: " + event.getParam("searchTerm"));

        cmp.set("v.searchTerm", event.getParam("searchTerm"));
        helper.initialize(cmp, event, helper);
    },

    equipmentTypeFilterChange: function(cmp, event, helper){
        helper.initialize(cmp, event, helper);
    },    

    helixStatusFilterChange: function(cmp, event, helper){
        helper.initialize(cmp, event, helper);
    }    
})