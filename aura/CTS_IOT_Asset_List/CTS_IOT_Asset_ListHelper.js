({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");
        var siteID = cmp.get("v.siteID");
        var equipmentTypeFilter = cmp.get("v.equipmentTypeFilter");
        var helixStatusFilter = cmp.get("v.helixStatusFilter");

        action.setParams({
            searchTerm : cmp.get("v.searchTerm"),
            "siteId" : siteID != null ? siteID : "",
            pageSize : cmp.get("v.pageSize"),
            offSet : cmp.get("v.offSet"),
            totalRecordCount: cmp.get("v.totalRecordCount"),
            "equipmentTypeFilter" : equipmentTypeFilter,
            "helixStatusFilter" : helixStatusFilter
        });        

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	
    },

    handleGetInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	    

                cmp.set("v.assets", responseVal.assets);
                cmp.set("v.communitySettings", responseVal.communitySettings);  
                cmp.set("v.totalRecordCount", responseVal.totalRecordCount);    
                
                if (responseVal.userSettings.User_Selected_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.userSettings.User_Selected_List_Page_Size__c);
                }
                else if (responseVal.communitySettings.Default_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.communitySettings.Default_List_Page_Size__c);
                }
                else{
                    cmp.set("v.pageSize", 20);
                }             
                
                let equipmentTypeOptions = [{label: "All Equipment Types", value: ""}];
                
                responseVal.equipmentTypeOptions.forEach(function(option){
                    equipmentTypeOptions.push(option);
                });

                cmp.set("v.equipmentTypeFilters", equipmentTypeOptions);
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);
        }    
        
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})