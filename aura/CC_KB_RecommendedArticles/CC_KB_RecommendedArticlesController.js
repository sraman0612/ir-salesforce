({
	onInit : function(component, event, helper) {
		var action = component.get("c.getRecArticles");
        var topicNames = component.get("v.topicNames");
        var numOfArticles = component.get("v.numOfArticles");
        action.setParams({topicNames: topicNames, numOfArticles: numOfArticles});
		action.setCallback(this, function(response) {
            var state = response.getState();
			if (state === 'SUCCESS') {
                var result = response.getReturnValue();
            	component.set("v.articles", result);
                console.log('result**' + result);
            } else {
				console.log('error**' + response);              
        	}
        });
		$A.enqueueAction(action);
	}    
})