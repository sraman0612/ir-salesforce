/**
 * @File Name          : Opportunity_Product_SelectionController.js
 * @Description        :
 * @Author             : Murthy Tumuluri
 * @Group              :
 * @Last Modified By   : Murthy Tumuluri
 * @Last Modified On   : 7/26/2019, 6:25:31 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/19/2019, 10:09:26 AM   Murthy Tumuluri     Initial Version
 **/
({
  /*Data loading methods on to the Picker*/
  init: function(component, event, helper) {
    var opportunityId = component.get("v.recordId");
    var baseProduct;
    var emeaData = [];
    var naData = [];
    helper.callServer(
      component,
      "c.getPriceCheck",
      function(response) {
        console.log("getPriceCheck", response);
        if (response == false) {
          console.log("false", false);
          component.set("v.priceCheck", false);
        } else if (response == true) {
          console.log("true", true);
          component.set("v.priceCheck", true);
        }
        /*Server call to bring price check*/
      },
      {
        recId: opportunityId
      }
    );
    /*Server call to bring select options with default*/
    helper.callServer(
      component,
      "c.getBaseModels",
      function(response) {
        component.set("v.selectedValue", response[0].id);
        console.log("@@ Models", response);
        component.set("v.basemodels", response);
        /*Server call to bring base model details*/
        helper.callServer(
          component,
          "c.getCardProduct",
          function(response) {
            component.set("v.selectedBaseModel", response);
          },
          {
            prodId: response[0].id,
            recId: opportunityId
          }
        );
      },
      {
        oppId: opportunityId
      }
    );
    /*Server call to bring all avaialble products to select*/
    helper.callServer(
      component,
      "c.getProductByOppModel",
      function(response) {
        console.log(response);
        for (var i = 0; i < response.length; i++) {
          if (response[i].RecordType.Name == "TK Transport EMEA") {
            emeaData.push(response[i]);
            component.set("v.naData", false);
            component.set("v.emeaData", true);
            console.log("emeaData");
          } else if (response[i].RecordType.Name == "TK Transport NA") {
            naData.push(response[i]);
            component.set("v.naData", true);
            component.set("v.emeaData", false);
            console.log("naData");
          }
        }
        if (emeaData.length > 0) {
          component.set("v.productData", emeaData);
        }
        if (naData.length > 0) {
          component.set("v.productData", naData);
        }
      },
      {
        oppId: opportunityId
      }
    );
  },
  /*Server call to bring changed select options values*/
  onChange: function(component, event, helper) {
    var valueInsde = component.find("baseModelId").get("v.value");
    var recCompId = component.get("v.recordId");
    var emeaData = [];
    var naData = [];
    console.log(valueInsde);
    /*Server call to bring modfied base model details*/
    helper.callServer(
      component,
      "c.getCardProduct",
      function(response) {
        component.set("v.selectedBaseModel", response);
      },
      {
        prodId: valueInsde,
        recId: recCompId
      }
    );
    /*Server call to bring child products for modified base model*/
    helper.callServer(
      component,
      "c.getProductByModel",
      function(response) {
        for (var i = 0; i < response.length; i++) {
          console.log(response[i]);

          if (response[i].RecordType.Name == "TK Transport EMEA") {
            emeaData.push(response[i]);
          } else if (response[i].RecordType.Name == "TK Transport NA") {
            naData.push(response[i]);
          }
        }
        if (emeaData.length > 0) {
          component.set("v.naData", false);
          component.set("v.emeaData", true);
          component.set("v.productData", emeaData);
        }
        if (naData.length > 0) {
          component.set("v.naData", true);
          component.set("v.emeaData", false);
          component.set("v.productData", naData);
        }
      },
      {
        prodId: valueInsde
      }
    );
  },
  /*Addition, Deletion and Save methods for Products*/
  handleAddition: function(component, event, helper) {
    var buttonVal = event.getSource().get("v.value");
    var oppId = component.get("v.recordId");
    var selectedProducts = [];
    var existingProducts = component.get("v.productData");
    var selBaseModel = component.get("v.selectedBaseModel");
    var emeaCheck = component.get("v.priceCheck");

    console.log("@@ emeaCheck", emeaCheck);
    console.log("@@ selBaseModel", selBaseModel);
    for (var i = 0; i < existingProducts.length; i++) {
      console.log("existingProducts[i].Id", existingProducts[i].Id);
      console.log("buttonVal.Id", buttonVal.Id);

      if (existingProducts[i].Id == buttonVal.Id) {
        existingProducts.splice(i, 1);
        selectedProducts.push(buttonVal);
      }
    }
    component.set("v.productData", existingProducts);

    var cmpEvent = component.find("cComp");

    cmpEvent.confirmationMethod({
      selectedProdList: buttonVal,
      recId: oppId,
      selectedModel: selBaseModel,
      emea: emeaCheck
    });
  },
  parentEvt: function(component, event, helper) {
    var existingProducts = component.get("v.productData");
    var changedProd = event.getParam("addededProd");

    existingProducts.push(changedProd);
    component.set("v.productData", existingProducts);
  },
  handleCustomProdAddition: function(component, event, helper) {
    var item = component.find("prodSelCode").get("v.value");
    var description = component.find("prodSelDes").get("v.value");
    var price = component.find("prodSelPrice").get("v.value");
  }
});