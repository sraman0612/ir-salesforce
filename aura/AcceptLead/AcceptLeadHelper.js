({
    doInit : function(component, event, helper) {
        
        var apexProxy = component.find('apexProxy');
        var action = component.get('c.changeLeadOwner');
        action.setParams({
             "leadId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Lead Updated",
                    "message" : "The Lead Owner has been updated.",
                    "type" : "success"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }else if(state === "ERROR"){
                var errors = response.getError();
                console.log('errors ===> '+JSON.stringify(errors));
                console.log('Problem updating Lead, response state = '+state);
                let name = errors[0].pageErrors;
                 console.log('##########1'+ name );
                 console.log('$$$$$$$$$'+ JSON.stringify(name[0].message));
               
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Lead Updated",
                    "message" : JSON.stringify(name[0].message),
                   
                    "type" : "Error"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }else{
                console.log('Unknown problem: '+state);
            }
        });
        $A.enqueueAction(action);
    },
        

        
})