({
	 showSuccess : function(component, event, helper) {
         
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'Case Created Succesfuly ',
            duration:' 10000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
          var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
          $A.get('e.force:refreshView').fire();
    },
    showError : function(component, event, helper, errormessage) {
       
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:errormessage,
            duration:' 10000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
         var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
         $A.get('e.force:refreshView').fire();
    },
})