({
    init : function(cmp, event, helper) {
        console.log("CTS_IOT_Account_List-init");
        
        helper.initialize(cmp, event, helper);
    },
    
    onMouseOverHelixIcon : function(cmp, event, helper){
        console.log("onMouseOverHelixIcon");   

        var utils = cmp.find("utils");
        var imgSource = event.target;
        imgSource.src = cmp.get("v.helixIconURLHover");
    },
    
    onMouseOutHelixIcon : function(cmp, event, helper){
        console.log("onMouseOutHelixIcon");   
           
        var utils = cmp.find("utils");
        var imgSource = event.target;
        imgSource.src = cmp.get("v.helixIconURLDefault");
    },     

    onMouseOverServiceRequestIcon : function(cmp, event, helper){

        var utils = cmp.find("utils");
        var cmpSource = event.getSource();
        cmpSource.set("v.variant", "error");
    },
    
    onMouseOutServiceRequestIcon : function(cmp, event, helper){           

        var utils = cmp.find("utils");
        var cmpSource = event.getSource();
        cmpSource.set("v.variant", "");
    },

    onViewDashboard : function(cmp, event, helper){      
        console.log("onViewDashboard");
        var utils = cmp.find("utils");
        utils.navigateToCommunityPage(cmp.find("navService"), "site-losant-details", {"site-id": event.currentTarget.dataset.siteId}); 
    },

    handlePageChange: function(cmp, event, helper){
        console.log("handlePageChange");
        cmp.set("v.offSet", event.getParam("offSet"));
        helper.initialize(cmp, event, helper);
    },

    handleGlobalPageSizeChange: function(cmp, event, helper){
        console.log("handleGlobalPageSizeChange");
        let pageSize = event.getParam("pageSize");
        console.log("pageSize: " + pageSize);
        cmp.set("v.pageSize", pageSize);
        helper.initialize(cmp, event, helper);
    },    

    handlePageSizeChange: function(cmp, event, helper){
        console.log("handlePageSizeChange");
        cmp.set("v.pageSize", event.getParam("pageSize"));      
        helper.initialize(cmp, event, helper);

        // Notify any other sibling list view components of the list page size change
        var pageSizeChangeEvent = $A.get("e.c:List_Page_Size_Change");
        pageSizeChangeEvent.setParams({pageSize: event.getParam("pageSize")});
        pageSizeChangeEvent.fire();        
    },

    handleSearchTermChange: function(cmp, event, helper){
        console.log("handleSearchTermChange: " + event.getParam("searchTerm"));

        cmp.set("v.searchTerm", event.getParam("searchTerm"));
        helper.initialize(cmp, event, helper);
    }    
})