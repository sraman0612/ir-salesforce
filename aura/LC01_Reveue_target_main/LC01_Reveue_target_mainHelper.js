({
	changePathBg : function(component,classRevenueActive,classProductActive, classObjectActive) {
		var revenue;
		var product;
		var objective;
		if(classRevenueActive == true){
			revenue = "slds-is-current slds-is-active slds-path__item";
			product = "slds-path__item  slds-is-incomplete";
			objective = "slds-path__item  slds-is-incomplete"
		}
		if(classProductActive == true){
			product = "slds-is-current slds-is-active slds-path__item";
			revenue = "slds-path__item  slds-is-incomplete";
			objective = "slds-path__item  slds-is-incomplete"
		}
		if(classObjectActive == true){
			objective = "slds-is-current slds-is-active slds-path__item";
			product = "slds-path__item  slds-is-incomplete";
			revenue = "slds-path__item  slds-is-incomplete"
		}

		component.set("v.selectedPathRevenue",revenue);
		component.set("v.selectedProduct",product);
		component.set("v.selectedObjective",objective);
	},

	calculateTotals : function(component, listTargets){
		// console.log('mgr listTargets ', listTargets);

		var sprint1_Target_NPD = 0;
		var sprint1_Target_Solution = 0;
		var sprint1_Target_Footprint = 0;
		var sprint1_Target_Share = 0;
		var sprint1_Target_base = 0;

		var sprint1_Actuals_NPD = 0;
		var sprint1_Actuals_Solution = 0;
		var sprint1_Actuals_Footprint = 0;
		var sprint1_Actuals_Share = 0;
		var sprint1_Actuals_base = 0;

		var sprint2_Target_NPD = 0;
		var sprint2_Target_Solution = 0;
		var sprint2_Target_Footprint = 0;
		var sprint2_Target_Share = 0;
		var sprint2_Target_base = 0;

		var sprint2_Actuals_NPD = 0;
		var sprint2_Actuals_Solution = 0;
		var sprint2_Actuals_Footprint = 0;
		var sprint2_Actuals_Share = 0;
		var sprint2_Actuals_base = 0;
		
		var sprint3_Target_NPD = 0;
		var sprint3_Target_Solution = 0;
		var sprint3_Target_Footprint = 0;
		var sprint3_Target_Share = 0;
		var sprint3_Target_base = 0;

		var sprint3_Actuals_NPD = 0;
		var sprint3_Actuals_Solution = 0;
		var sprint3_Actuals_Footprint = 0;
		var sprint3_Actuals_Share = 0;
		var sprint3_Actuals_base = 0;

		for (var i = 0; i < listTargets.length; i++) {

			if(listTargets[i].month >= 1 && listTargets[i].month <= 4){

				sprint1_Target_NPD += listTargets[i].npd;
				sprint1_Target_Solution += listTargets[i].solution;
				sprint1_Target_Footprint += listTargets[i].footprint;
				sprint1_Target_Share += listTargets[i].share;
				sprint1_Target_base += listTargets[i].base;

				sprint1_Actuals_NPD += listTargets[i].actuals_npd;
				sprint1_Actuals_Solution += listTargets[i].actuals_solution;
				sprint1_Actuals_Footprint += listTargets[i].actuals_footprint;
				sprint1_Actuals_Share += listTargets[i].actuals_share;
				sprint1_Actuals_base += listTargets[i].actuals_base;

			} else if(listTargets[i].month >= 5 && listTargets[i].month <= 8){

				sprint2_Target_NPD += listTargets[i].npd;
				sprint2_Target_Solution += listTargets[i].solution;
				sprint2_Target_Footprint += listTargets[i].footprint;
				sprint2_Target_Share += listTargets[i].share;
				sprint2_Target_base += listTargets[i].base;

				sprint2_Actuals_NPD += listTargets[i].actuals_npd;
				sprint2_Actuals_Solution += listTargets[i].actuals_solution;
				sprint2_Actuals_Footprint += listTargets[i].actuals_footprint;
				sprint2_Actuals_Share += listTargets[i].actuals_share;
				sprint2_Actuals_base += listTargets[i].actuals_base;

			} else if(listTargets[i].month >= 9 && listTargets[i].month <= 12){

				sprint3_Target_NPD += listTargets[i].npd;
				sprint3_Target_Solution += listTargets[i].solution;
				sprint3_Target_Footprint += listTargets[i].footprint;
				sprint3_Target_Share += listTargets[i].share;
				sprint3_Target_base += listTargets[i].base;

				sprint3_Actuals_NPD += listTargets[i].actuals_npd;
				sprint3_Actuals_Solution += listTargets[i].actuals_solution;
				sprint3_Actuals_Footprint += listTargets[i].actuals_footprint;
				sprint3_Actuals_Share += listTargets[i].actuals_share;
				sprint3_Actuals_base += listTargets[i].actuals_base;

			}

		}

		component.set("v.sprint1_Target_NPD", sprint1_Target_NPD);
		component.set("v.sprint1_Target_Solution", sprint1_Target_Solution);
		component.set("v.sprint1_Target_Footprint", sprint1_Target_Footprint);
		component.set("v.sprint1_Target_Share", sprint1_Target_Share);
		component.set("v.sprint1_Target_base", sprint1_Target_base);

		component.set("v.sprint1_Actuals_NPD", sprint1_Actuals_NPD);
		component.set("v.sprint1_Actuals_Solution", sprint1_Actuals_Solution);
		component.set("v.sprint1_Actuals_Footprint", sprint1_Actuals_Footprint);
		component.set("v.sprint1_Actuals_Share", sprint1_Actuals_Share);
		component.set("v.sprint1_Actuals_base", sprint1_Actuals_base);

		component.set("v.sprint2_Target_NPD", sprint2_Target_NPD);
		component.set("v.sprint2_Target_Solution", sprint2_Target_Solution);
		component.set("v.sprint2_Target_Footprint", sprint2_Target_Footprint);
		component.set("v.sprint2_Target_Share", sprint2_Target_Share);
		component.set("v.sprint2_Target_base", sprint2_Target_base);

		component.set("v.sprint2_Actuals_NPD", sprint2_Actuals_NPD);
		component.set("v.sprint2_Actuals_Solution", sprint2_Actuals_Solution);
		component.set("v.sprint2_Actuals_Footprint", sprint2_Actuals_Footprint);
		component.set("v.sprint2_Actuals_Share", sprint2_Actuals_Share);
		component.set("v.sprint2_Actuals_base", sprint2_Actuals_base);
	
		component.set("v.sprint3_Target_NPD", sprint3_Target_NPD);
		component.set("v.sprint3_Target_Solution", sprint3_Target_Solution);
		component.set("v.sprint3_Target_Footprint", sprint3_Target_Footprint);
		component.set("v.sprint3_Target_Share", sprint3_Target_Share);
		component.set("v.sprint3_Target_base", sprint3_Target_base);

		component.set("v.sprint3_Actuals_NPD", sprint3_Actuals_NPD);
		component.set("v.sprint3_Actuals_Solution", sprint3_Actuals_Solution);
		component.set("v.sprint3_Actuals_Footprint", sprint3_Actuals_Footprint);
		component.set("v.sprint3_Actuals_Share", sprint3_Actuals_Share);
		component.set("v.sprint3_Actuals_base", sprint3_Actuals_base);

		component.set("v.sprint_Target_NPD", sprint1_Target_NPD+sprint2_Target_NPD+sprint3_Target_NPD);
		component.set("v.sprint_Target_Solution", sprint1_Target_Solution+sprint2_Target_Solution+sprint3_Target_Solution);
		component.set("v.sprint_Target_Footprint", sprint1_Target_Footprint+sprint2_Target_Footprint+sprint3_Target_Footprint);
		component.set("v.sprint_Target_Share", sprint1_Target_Share+sprint2_Target_Share+sprint3_Target_Share);
		component.set("v.sprint_Target_base", sprint1_Target_base+sprint2_Target_base+sprint3_Target_base);

		component.set("v.actuals_NPD", sprint1_Actuals_NPD+sprint2_Actuals_NPD+sprint3_Actuals_NPD);
		component.set("v.actuals_Solution", sprint1_Actuals_Solution+sprint2_Actuals_Solution+sprint3_Actuals_Solution);
		component.set("v.actuals_Footprint", sprint1_Actuals_Footprint+sprint2_Actuals_Footprint+sprint3_Actuals_Footprint);
		component.set("v.actuals_Share", sprint1_Actuals_Share+sprint2_Actuals_Share+sprint3_Actuals_Share);
		component.set("v.actuals_base", sprint1_Actuals_base+sprint2_Actuals_base+sprint3_Actuals_base);

		// Final target
		var finalTarget = 0;
		var totalCurrent = 0;
		var total_lastYear = 0;
		totalCurrent = component.get("v.sprint_Target_NPD") + component.get("v.sprint_Target_Solution") + component.get("v.sprint_Target_Footprint") + component.get("v.sprint_Target_Share") + component.get("v.sprint_Target_base");
		total_lastYear = component.get("v.total_lastYear");

		// console.log('cal totals totalCurrent ', totalCurrent);
		// console.log('cal totals total_lastYear ', total_lastYear);
		
		if(total_lastYear != 0)
			finalTarget = ((totalCurrent-total_lastYear)/total_lastYear) * 100;
		else 
			finalTarget = 0;

		/*if(totalCurrent != 0)
			finalTarget = ((totalCurrent-total_lastYear)/totalCurrent) * 100;
		else 
			finalTarget = 0;*/
		// console.log('cal totals finalTarget 1 ', finalTarget);

		finalTarget = Math.round(finalTarget);
		// console.log('cal totals finalTarget 2 ', finalTarget);
		component.set("v.final_target", finalTarget);

		var min_target = 0;
		min_target = component.get("v.min_target");
		// if(min_target >= (totalCurrent-total_lastYear) )
		// if(min_target < (totalCurrent-total_lastYear) )
		// if(total_lastYear > min_target)
		if(finalTarget >= min_target)
			component.set("v.growth_mintarget", true);
		else 
			component.set("v.growth_mintarget", false);

		var targetBase_y = 0;
		targetBase_y = component.get("v.sprint_Target_base");
		// if(targetBase_y >= total_lastYear)
		// if(targetBase_y < total_lastYear && targetBase_y != 0)
		if(targetBase_y < total_lastYear)
			component.set("v.growth_base", true);
		else
			component.set("v.growth_base", false);

		
	},


	saveTargets : function (component, listTargets) {
		// console.log('mgr saveTargets ', listTargets);
		
		if(listTargets.length > 0){

			var action = component.get("c.saveTarget");

			action.setParams({"lstTarget": JSON.stringify(listTargets)});  
			action.setCallback(this, function(response) {

			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
				// console.log('mgr result ', result);

				if(result.error){
	                // console.log('mgr saveTargets 1');



				} else if(!result.error){
	                // console.log('mgr saveTargets 2--end');

	                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "success",
                        "title": "Success!",
                        "message": result.message
                    });
                    toastEvent.fire();
	            } 
	            
			}

			});
	 
			$A.enqueueAction(action);

		} else {



		}

	}


})