({
    parseTranslations : function(component, event, helper) {        
        /*let fields = JSON.parse(JSON.stringify(component.get("v.fields")));
        let translations = JSON.parse(JSON.stringify(component.get("v.translations")));
        console.log("fields, isField**", fields, component.get("v.isField"));
        console.log("elements translations**", translations);
        
        for(let i=0;i<fields.length;i++) {            
            $A.createComponents([
                ["lightning:layoutItem", {
                    "size": 12,
                    "padding": "horizontal-small",
                    "class": "slds-box"
                }],
                ["lightning:formattedRichText", {
                    "value" :  translations[i] != null ? translations[i].translatedText : ''
                }],
                ["lightning:formattedRichText", {
                    "value" : "<b>" + fields[i] + "</b><br/> "
                }]
            ], function(components, status, errorMessage) {
                if (status === 'SUCCESS') {
                    var lItem = components[0];
                    var fText = components[1];
                    var lbl = components[2];
                    
                    var lItemBody = lItem.get("v.body");
                    if(typeof fields[i] == 'string') lItemBody.push(lbl);
                    lItemBody.push(fText);                            
                    lItem.set("v.body", lItemBody);
                    
                    var body = component.get("v.body");
                    body.push(lItem);
                    component.set("v.body", body);
                }
            });
        }*/
    },
    translateVal: function(component, event, helper){
      	 var target = event.getSource();
         var txtVal = target.get("v.value") ;
         var recordId = component.get("v.recordId") ;
         var translatedResult;
         let action = component.get("c.translateText");
        action.setParams({textForTranslation : txtVal, recordId : recordId});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                let result = response.getReturnValue();
                if(result != null){
                    console.log('result::1::'+result);
                    result = JSON.parse(result);
                    console.log('result::'+result);
                    translatedResult = result.data.translations[0].translatedText;
                    component.set("v.translatedResult", unescape(translatedResult));
                    var openModelVar = component.get('c.openModel');
                    $A.enqueueAction(openModelVar);
                }
            } else {
                console.log('error state**', state);
            }
        });
        $A.enqueueAction(action);
        

   },
   openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   },
   copyClip: function(component, event, helper) {
        
     var copyText = document.getElementById("textCopy");
      copyText.select();
      document.execCommand("copy");
      
      component.set("v.isOpen", true);
        
   }
  
  
  
})