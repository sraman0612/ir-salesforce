({

	init : function(component, event, helper) {
		// console.log("in init");

		var recordId = component.get("v.recordId");   
		var action = component.get("c.retrieveTarget");

		// console.log('mgr component.get("v.accountId") ', component.get("v.accountId"));
		// console.log('mgr component.get("v.year") ', component.get("v.year"));
		// console.log('mgr component.get("v.year_1") ', component.get("v.year_1"));
		// console.log('mgr component.get("v.year_2") ', component.get("v.year_2"));

		action.setParams({
			"accountId": component.get("v.accountId"),
			"currentYear": component.get("v.year"),
			"currentYear_1": component.get("v.year_1"),
			"currentYear_2": component.get("v.year_2")
		});
		action.setCallback(this, function(response) {

			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
				console.log('mgr result ', result);
				// console.log('mgr result.lstProductTarget ', result.lstProductTarget);
				
				if(result.error){
                    // console.log('mgr 1');

				} else if(!result.error){
                    // console.log('mgr 2--end');

					component.set("v.lstProductTarget", result.lstProductTarget);
					// component.set("v.year", result.currentYear);
					// component.set("v.year_1", result.currentYear_1);
					// component.set("v.year_2", result.currentYear_2);

                } 
                
			}

		});
 
		$A.enqueueAction(action);

	},


	saveProducts : function(component, event, helper) {

		var lstTarget = component.get("v.lstProductTarget");
		 console.log("in save prod------- targets", lstTarget);
		
		var action = component.get("c.saveTarget");

		action.setParams({"lstTarget": JSON.stringify(lstTarget)});  
		action.setCallback(this, function(response) {

			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
					 console.log('result', result);
			
				if(result.error){
                     console.log('mgr 1');

				} else if(!result.error){
                     console.log('mgr 2--end');

                 	var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "success",
                        "title": "Success!",
                        "message": result.message
                    });
                    toastEvent.fire();


                } 
                
			}

		});
 
		$A.enqueueAction(action);

	},


	changeWarning : function(component, event, helper) {
		var countProduct = 0;
		var lstTarget = component.get("v.lstProductTarget");
		 console.log('mgr changeWarning ', lstTarget);

		for (var i = 0; i < lstTarget.length; i++) {
			if(lstTarget[i].targetY != null && lstTarget[i].targetY > 0){
				countProduct = countProduct + 1;
			}
		}

		 console.log('countProduct ', countProduct);

		if(countProduct >= 3){
			component.set("v.isChangedProductQuantity", component.get("v.isChangedProductQuantity") + 1);
		} else {
			component.set("v.isChangedProductQuantity", 0);
		}

	}/*,


	checkNumber : function(component, event, helper) {
		console.log('checkNumber ', component);
		// console.log('checkNumber ', component.get("v.input"));
	}
*/
})