({
    init : function(cmp, event, helper) {

        console.log("CTS_IOT_Submit_Service_Request:init");

        // Update breadcrumbs
        // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
        
        // navEvent.setParams({
        //     "label" : "New Service Request",
        //     "url" : "/submit-service-request"
        // });

        // navEvent.fire();         

        let urlParams = new URLSearchParams(window.location.search);
        var assetId = urlParams.get("asset-id");
        var siteId = urlParams.get("site-id");
        var type = urlParams.get("type");
        var priority = urlParams.get("priority");
        var description = urlParams.get("description");

        var serviceRequest = cmp.get("v.serviceRequest");

        if (assetId && assetId.length > 0){

            cmp.set("v.assetID", assetId);  
            serviceRequest.Asset__c = assetId;       
        }

        if (siteId && siteId.length > 0){
            cmp.set("v.siteID", siteId); 
        }

        if (type){
            serviceRequest.Type__c = type;
        }

        if (priority){
            serviceRequest.Severity__c = priority;
        }        

        if (description){
            serviceRequest.Problem_Description__c = description;
        }

        cmp.set("v.serviceRequest", serviceRequest);

        var today = $A.localizationService.formatDate(new Date(), "MM/DD/YYYY");
        cmp.set("v.today", today);      

        var typeOptions = [
            {"label" : "Issue", "value" : "Issue"},
            {"label" : "Upgrade", "value" : "Upgrade"},
            {"label" : "Other", "value" : "Other"}
        ];

        cmp.set("v.typeOptions", typeOptions);

        var severityOptions = [            
            {"label" : "4 - Low", "value" : "4 - Low"},            
            {"label" : "3 - Medium", "value" : "3 - Medium"},            
            {"label" : "2 - High", "value" : "2 - High"},
            {"label" : "1 - Critical", "value" : "1 - Critical"}
        ];

        cmp.set("v.severityOptions", severityOptions);           

        helper.initialize(cmp, event, helper);      
    },

    onSave : function(cmp, event, helper){
        console.log("onSave");

        // Validations
        var isValid = true;

        var assetID = cmp.get("v.assetID");
        var type = cmp.get("v.serviceRequest.Type__c");
        var severity = cmp.get("v.serviceRequest.Severity__c");
        var asset = cmp.get("v.serviceRequest.Asset__c");
        var description = cmp.get("v.serviceRequest.Problem_Description__c");

        var input = cmp.find("type");

        if (!type || type.length == 0){
              
            input.setCustomValidity("Type is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }      
        
        input = cmp.find("severity");

        if (!severity || severity.length == 0){
              
            input.setCustomValidity("Priority is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }    
        
        if (assetID == ""){

            input = cmp.find("asset");

            if (!asset || asset.length == 0){
                
                input.setCustomValidity("Asset is required.");
                input.reportValidity();
                isValid = false;
            }
            else{
                input.setCustomValidity("");
                input.reportValidity();
            }  
        }
        
        input = cmp.find("description");

        if (!description || description.length == 0){
              
            input.setCustomValidity("Description is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }         

        console.log("isValid: " + isValid);

        if (isValid){
            helper.submitServiceRequest(cmp, event, helper);            
        }
    }
})