(
    {   
    	
        
      init : function(component, event, helper) {
        component.set("v.Spinner", true); 
           var action = component.get("c.isInDeleteRequestApproval");
			action.setParams({ 
                recordId: component.get("v.recordId")
            });
 		
          action.setCallback(this, function(response) {
      	  
              var state = response.getState();
              if(component.isValid() && state == "SUCCESS"){
                  
                  if(response.getReturnValue()==true){
                     var toastEvent = $A.get("e.force:showToast");
					 toastEvent.setParams({
                    "title": "This record is already in a delete approval process!", 
                    "message": "You can recall the request in the Approvals related list or contact your manager."
               		 });
                    $A.get("e.force:closeQuickAction").fire();
                	toastEvent.fire();
                  }else{
                     
                   	//Load approval process / dialog 
                   	var action2 = component.get("c.getRecordName");
                        action2.setParams({ 
                            recordId: component.get("v.recordId")
                        });
                    
                      action2.setCallback(this, function(response) {
                      component.set("v.Spinner", false); 
                         console.log('in callback2 :'+response.getReturnValue().toString()); 
                         var state = response.getState();
                          if(component.isValid() && state == "SUCCESS"){
                             component.set("v.name", response.getReturnValue());
                          } else {
                             console.log('There was a problem and the state is: '+state);
                          }
                   	});
                  	 $A.enqueueAction(action2);
                  }
                  
              } else {
                 console.log('There was a problem and the state is: '+state);
              }
          })
     
          $A.enqueueAction(action);
         
      
    console.log('out of init :');          
   },
        
 
	setDeleteRequestFlag : function(component, event, helper) {
    console.log('setDeleteRequestFlag');
        if( component.find("request_delete_reason").get("v.value")==''){
            
    console.log('setDeleteRequestFlag - no comment entered');   
             alert('A Delete Request Reason must be entered before continuing.')
           	 return;
        }else{//fire del request
      
    console.log('in set request');       
                var action = component.get("c.setDeleteRequest");
                
                 action.setParams({
                        recordId: component.get("v.recordId"),
                        reason: component.find("request_delete_reason").get("v.value") 
                 });
                component.set("v.Spinner", true); 
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('it worked...');
                         $A.get("e.force:closeQuickAction").fire();
                        var toastEvent = $A.get("e.force:showToast");
                             toastEvent.setParams({
                            "title": "Success!",  
                            "message": "The record has submitted for deletion."
                        });
                        toastEvent.fire();
        
                    }else{
                         $A.get("e.force:closeQuickAction").fire();
                         var toastEvent = $A.get("e.force:showToast");
                             toastEvent.setParams({
                             "title": "Error!",
                            "message": "Sorry, there has been an error submitting your request to delete. Contact your admin."
                        });
                    }
                      
                });
             $A.enqueueAction(action);
        }
        
      console.log('completed');
	}
   
})