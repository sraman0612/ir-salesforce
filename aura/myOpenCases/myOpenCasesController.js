({
	init : function(component, event, helper) {
        var action = component.get("c.getTableAndData");
        var fieldNames = component.get("v.fieldAPINames");
        console.log('fieldNames',fieldNames);
        var hyperCaseNumberLink = {label: 'Case Number', fieldName: 'CaseNumberLink', type: 'url', 
             typeAttributes: {label: { fieldName: 'CaseNumber' }, target: '_parent'}};
        var hyperOwnerIdLink = {label: 'Owner Name', fieldName: 'OwnerIdLink', type: 'url', 
             typeAttributes: {label: { fieldName: 'Owner.Name' }, target: '_parent'}};
        var hyperContactIdLink = {label: 'Contact Name', fieldName: 'ContactIdLink', type: 'url', 
             typeAttributes: {label: { fieldName: 'Contact.Name' }, target: '_parent'}};
        var hyperAccountIdLink = {label: 'Account Name', fieldName: 'AccountIdLink', type: 'url', 
             typeAttributes: {label: { fieldName: 'Account.Name' }, target: '_parent'}};
        var fieldList = fieldNames.split(";");
        action.setParams({
            sObjectName : 'Case', 
            fieldsToDisplay : fieldList
        });
        action.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            var columns = response.getReturnValue().tableColumnsLst;
            var records = response.getReturnValue().tableDataLst;
            console.log('Table Column', columns);
            console.log('Table Data', records);
            for(var i = 0; i < records.length; i++){
                var record = records[i];
                for(var field in record) {      
                    if(typeof record[field] == 'object'){
                        var parentObj = record[field];
                        for(var parentField in parentObj) {
                            if(parentField=='Name'){
                                /* add the value as a new element to the record */
                                var newField = field + '.Name';
                                record[newField] = parentObj.Name;
                            }
                        }
                    }
                }
            }
            for(var i=0; i<columns.length; i++ ){
                if(columns[i].fieldName == hyperCaseNumberLink.typeAttributes.label.fieldName){
                    columns.splice(i,1,hyperCaseNumberLink);
                }else if(columns[i].fieldName == hyperOwnerIdLink.typeAttributes.label.fieldName){
                    columns.splice(i,1,hyperOwnerIdLink);
                }else if(columns[i].fieldName == hyperContactIdLink.typeAttributes.label.fieldName){
                    columns.splice(i,1,hyperContactIdLink);
                }else if(columns[i].fieldName == hyperAccountIdLink.typeAttributes.label.fieldName){
                    columns.splice(i,1,hyperAccountIdLink);
                }
            }
            
            records.forEach(function(record){
                record.CaseNumberLink = '/'+record.Id;
                record.OwnerIdLink = (record.OwnerId === undefined)? '' : ('/'+record.OwnerId);
                record.ContactIdLink = (record.ContactId === undefined)? '' : ('/'+record.ContactId);
                record.AccountIdLink = (record.AccountId === undefined)? '' : ('/'+record.AccountId);
            });
            console.log(columns);
            console.log(records);
            component.set("v.tblColumns", columns);
            component.set("v.tblData", records);
        });
        $A.enqueueAction(action);
	},
})