({
    init: function (cmp, event, helper) {
        console.log("CTS_IOT_Site_Details:init");
        
        var siteId = decodeURIComponent(window.location.search.substring(1).substring(8));
        console.log("site ID found: " + siteId);
        
        cmp.set("v.siteID", siteId);

        var contactColumns = [
            {label: "Name", fieldName: "ContactDetailsURL", type: "url", typeAttributes: { label: { fieldName: "Name" } }},
            {label: "Title", fieldName: "Title", type: "text"},
            {label: "Nickname", fieldName: "Nickname", type: "text"},
            {label: "Status", fieldName: "Status", type: "text"},
            {label: "Created Date", fieldName: "CreatedDate", type: "date"},
            {label: "Last Login Date", fieldName: "LastLoginDate", type: "date"}
        ];

        cmp.set("v.contactColumns", contactColumns);

        helper.initialize(cmp, event, helper);
    }
})