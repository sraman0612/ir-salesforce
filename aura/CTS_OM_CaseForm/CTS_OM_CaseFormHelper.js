({
	showHideFields: function(component, event, checkVal, checkId) {
        checkVal = checkVal != undefined ? checkVal : event.getSource().get("v.value");
        checkId = checkId != undefined ? checkId : event.getSource().get("v.id");        
        
        if(checkId == 'CTS_OM_Expedite_Ticket_Submitted__c') {
            component.set("v.expTickSubmitted", checkVal);
        } else if(checkId == 'CTS_OM_MTS_Part_Not_Available__c') {
            component.set("v.mtsPartsNotAvail", checkVal);
        } else if(checkId == 'CTS_OM_MTO_Part_Not_Available__c') {
            component.set("v.mtoPartsNotAvail", checkVal);
        } else if(checkId == 'CTS_OM_DLR_Created__c') {
            component.set("v.dlrCreated", checkVal);
        } else if(checkId == 'CTS_OM_NIC_Ticket_Created__c') {
            component.set("v.nicTickCreated", checkVal);
        } else if(checkId == 'CTS_OM_PR_Created_11i__c') {
            component.set("v.prCreated", checkVal);
        } else if(checkId == 'CTS_OM_Pricing_Ticket_R12_Created__c') {
            component.set("v.priceTickCreated", checkVal);
        } else if(checkId == 'CTS_OM_Receiver_Supplier__c') {
            component.set("v.receiverSupplier", checkVal);
        }
        
        //Check if all the fields are populated
        let disableSave = false;
        let reqFields = component.find('customRequired');
        console.log('reqFields**', JSON.stringify(reqFields));
        reqFields.forEach( f => {
            if(f.get("v.class") == 'customRequired' && (f.get("v.value") == null || f.get("v.value") == undefined)) {
            	disableSave = true;
        	}
        });
        component.set("v.disabled", disableSave);        
	},
    handleSelectEvent: function(component, event, fromInit) {
        component.set("v.showSpinner", true);
        
        let vals = event.getParam('selectVals') ? JSON.parse(event.getParam('selectVals')) : component.get("v.record");
        console.log('selected vals in handler**', vals);
        
        let showValFields = (vals.CTS_OM_Category__c == 'Order Management' && vals.CTS_OM_Category2__c == 'Standard Order Modifications') && 
           					(vals.CTS_OM_Category3__c == 'Date Change (Pull In)' || vals.CTS_OM_Category3__c == 'Date Change (Push Out)');
        component.set("v.showValFields", showValFields);
        
        let catFieldsVals, dispFieldsVals;
        
        let catFirstVal = component.get("v.catFirstVal");
        let record = JSON.parse(JSON.stringify(component.get("v.record")));
        let valFromEventOrDB = event.getParam('selectVals') && vals.hasOwnProperty('CTS_OM_Category__c') ? vals.CTS_OM_Category__c : record.CTS_OM_Category__c;
        if( (valFromEventOrDB && fromInit) || JSON.parse(event.getParam('selectVals')).hasOwnProperty('CTS_OM_Category__c') ) {
            component.set("v.catFieldsVals", vals);
            component.set("v.dispFieldsDisabled", false);
            component.set("v.catFirstVal", valFromEventOrDB);
            
            console.log('catFirstVal**', catFirstVal, valFromEventOrDB, catFirstVal != valFromEventOrDB);
            //If the category was reselected
			component.set("v.catReselected", catFirstVal != valFromEventOrDB);
            if(catFirstVal != vals.CTS_OM_Category__c) {
                var selectEvent = $A.get("e.c:CTS_OM_CatSelectionEvent");
                    if (selectEvent) {
                    selectEvent.setParams({
                        "selectedCat": valFromEventOrDB
                    });
                    selectEvent.fire();
                }
                component.set("v.dispFieldsVals", {"CTS_OM_Disposition_Category__c": null, "CTS_OM_Disposition_Category2__c": null, "CTS_OM_Disposition_Category3__c": null});
            }
			            
        } else if(vals.hasOwnProperty('CTS_OM_Disposition_Category__c')) {
            component.set("v.dispFieldsVals", vals);
        }
        
        component.set("v.showSpinner", false);
        console.log('catFieldsVals**', JSON.stringify(component.get("v.catFieldsVals")));
        console.log('dispFieldsVals**', JSON.stringify(component.get("v.dispFieldsVals")));
    }
})