({
    doInit : function(component, event, helper) {
        helper.soql(component, 'SELECT RecordTypeId, CTS_OM_Category__c, CTS_OM_Category2__c, CTS_OM_Category3__c, CTS_OM_Category4__c, CTS_OM_Disposition_Category__c, CTS_OM_Disposition_Category2__c, CTS_OM_Disposition_Category3__c FROM Case WHERE Id = \'' + component.get("v.recordId") + '\'')
            .then((result) => {
                console.log('result**', result);
                component.set("v.record", result[0]);                        
                helper.handleSelectEvent(component, event, true);                                                
            })
            .catch((error) => {
                console.log('error**', error);
                component.set("v.showSpinner", false);
            });		
    },    
	checkValue : function(component, event, helper) {
        component.set("v.cStatus", event.getSource().get("v.value"));
    },
	handleStatusChange: function(component, event, helper) {
        let newValue = event.getParam("value");
        let oldValue = event.getParam("oldValue");
    	console.log('newValue**', newValue);
        console.log('oldValue**', oldValue);
        if(newValue === 'Closed') {
            component.find('notifLib').showNotice({
                "variant": "error",
                "header": "Cannot Close!",
                "message": 'You cannot select the Closed Status value from here. Please use the "Details" tab to select Closed status.',
                closeCallback: function() {                    
                    component.find("Status").set("v.value", oldValue);
                }
            });
        } /*else if(newValue === 'Escalated') {
            let response = confirm("This will set the Status to Escalated. Other people may be notified. Are you sure?");
            console.log('response**', response);
            if(response === false) {
                component.find("Status").set("v.value", oldValue);
            }           
        }*/
    },
    handleLoad : function(component, event, helper) {
      let ctrlFields = component.find("ctrlField");
        ctrlFields.forEach( f => {
            let checkVal = f.get("v.value");
            let checkId = f.get("v.id");
            helper.showHideFields(component, event, checkVal, checkId);
        });
        component.set("v.cStatus", component.find("Status").get("v.value"));
    },
    showHideFields : function(component, event, helper) {
		let checkVal = event.getSource().get("v.value");
        let checkId = event.getSource().get("v.id");
        helper.showHideFields(component, event, checkVal, checkId);
	},
    handleSelectEvent: function(component, event, helper) {
        helper.handleSelectEvent(component, event);
    },
    handleSubmit: function(component, event) {        
        event.preventDefault();
        component.set("v.showSpinner", true);
        let catFieldsVals = component.get("v.catFieldsVals");
        let dispFieldsVals = component.get("v.dispFieldsVals");
        console.log('catFieldsVals && dispFieldsVals**', JSON.stringify(catFieldsVals), JSON.stringify(dispFieldsVals));
        let fields = event.getParam('fields');
        //let allFields = Object.assign({}, fields, catFieldsVals, dispFieldsVals.CTS_OM_Disposition_Category__c == null ? null : dispFieldsVals);
        let allFields = Object.assign({}, fields);
        console.log('fields in submit**', JSON.stringify(allFields));
        component.find('omCaseForm').submit(allFields);
        component.set("v.showSpinner", false);
    },
    handleSuccess: function(component, event) {        
        component.set("v.showSpinner", false);
        component.set("v.isSaved", true);
        component.find('notifLib').showToast({
            "title": "Success!",
            "message": "The record has been updated successfully."
        });
    },
    handleError: function(component, event) {        
        component.set("v.showSpinner", false);
        component.set("v.isSaved", false);
        var payload = event.getParams().error;
        console.log(JSON.stringify(payload));
    },
    edit: function(component, event) {        
        component.set("v.showSpinner", false);
        component.set("v.isSaved", false);
    },
    handleSectionToggle: function (cmp, event) {
        cmp.set("v.section", ["AdditionalInformation"]);
    }
                
})