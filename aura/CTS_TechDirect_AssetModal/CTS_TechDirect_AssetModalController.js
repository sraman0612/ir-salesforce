({
    doInit : function(component, event, helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
        var userRegion = component.get("v.userInfo.DB_Region__c");
        var apac = $A.get("$Label.c.End_Customer_Account_APAC");
        //var emeia = $A.get("$Label.c.End_Customer_Account_EMIEA");
        var emeia = $A.get("$Label.c.End_Customer_Account_EMEIA");
        var northAmerica = $A.get("$Label.c.End_Customer_Account_North_America");
        var latam = $A.get("$Label.c.End_Customer_Account_LATAM");
        var recType = $A.get("$Label.c.Asset_Community_Record_Type");
                
        if(userRegion == "APAC"){
            
        //Reset Form        
        var newAsset = {'sobjectType': 'asset',
                        'Name': '',                      
                        'Product__c': '', 
                        'Sub_product__c': '',
                        'CTS_TechDirect_Model_Number__c':'',
                        'InstallDate':'',
                        'AccountId': apac,
                        'RecordType' : recType
                       };
        //resetting the Values in the form      
          component.set("v.asset",newAsset);
        }
        
        else if(userRegion=="EMEIA"){
             //Reset Form
        var newAsset = {'sobjectType': 'asset',
                        'Name': '',
                        'Product__c': '', 
                        'Sub_product__c': '',
                        'CTS_TechDirect_Model_Number__c':'',
                        'InstallDate':'',
                        'AccountId': emeia,
                        'RecordType' : recType
                       };
        //resetting the Values in the form      
          component.set("v.asset",newAsset);
        }
            else if(userRegion=="North America"){
                //Reset Form
        var newAsset = {'sobjectType': 'asset',
                        'Name': '',
                        'Product__c': '', 
                        'Sub_product__c': '',
                        'CTS_TechDirect_Model_Number__c':'',
                        'InstallDate':'',
                        'AccountId': northAmerica,
                        'RecordType' : recType
                       };
        //resetting the Values in the form      
          component.set("v.asset",newAsset);
            }
                else if(userRegion=="LATAM"){
                     //Reset Form
        var newAsset = {'sobjectType': 'asset',
                        'Name': '',
                        'Product__c': '', 
                        'Sub_product__c': '',
                        'CTS_TechDirect_Model_Number__c':'',
                        'InstallDate':'',
                        'AccountId': latam,
                        'RecordType' : recType
                       };
        //resetting the Values in the form      
          component.set("v.asset",newAsset);
                }
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "False"  
        component.set("v.isOpen", false);
        
    },
    save:function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
       
        
      
      
        //getting the candidate information
        var asset = component.get("v.asset");
        var productName = component.get("v.selectedRecord.Name");
        
        //Validation
        if($A.util.isEmpty(asset.Name) || $A.util.isUndefined(asset.Name)){
            component.set("v.isOpen", true);
          
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Asset Name is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        }            
        if($A.util.isEmpty(asset.CTS_TechDirect_Model_Number__c) || $A.util.isUndefined(asset.CTS_TechDirect_Model_Number__c)){
           
           component.set("v.isOpen", true);
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Model Number is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        }
        if(productName == null || productName == undefined){
           
           component.set("v.isOpen", true);
            var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Product is Required',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
            return;
        }
        
        /* if($A.util.isEmpty(candidate.Email__c) || $A.util.isUndefined(candidate.Email__c)){
            alert('Email is Required');
            return;
        }
        if($A.util.isEmpty(candidate.SSN__c) || $A.util.isUndefined(candidate.SSN__c)){
            alert('SSN is Required');
            return;
        }*/
        //Calling the Apex Function
        var action = component.get("c.createRecord"); 
        
        var productid = component.get("v.selectedRecord.Id");
                //alert('productid'+productid);
               	component.set("v.asset.Product2Id",productid);
                //alert('Product'+component.get("v.asset.Product2Id"));
        //Setting the Apex Parameter
        action.setParams({
            asset : asset            
        });
        
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                 var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Success', 
                    'message' : 'Asset record created',
                    'type' : 'success',
            		'mode' : 'pester'
                }); 
                showToast.fire(); 
                component.set("v.isOpen", false);
                
                //Reset Form
                var newAsset = {'sobjectType': 'asset',
                                'Name': '',
                                'Product__c': '', 
                                'Sub_product__c': '',
                                'CTS_TechDirect_Model_Number__c':'',
                                'InstallDate':'',
                                'AccountId':'',
                                'RecordType' : recType
                               };
                //resetting the Values in the form
                component.set("v.asset",newAsset);
                
              
                
            } else if(state == "ERROR"){
                component.set("v.isOpen", false);
                 var showToast = $A.get("e.force:showToast"); 
                showToast.setParams({ 
                    'title' : 'Error', 
                    'message' : 'Asset record not created',
                    'type' : 'error',
            		'mode' : 'pester'
                }); 
                showToast.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    keyPressController : function(component, event, helper) {
      // get the search Input keyword   
		var getInputkeyWord = component.get("v.SearchKeyWord");
      // check if getInputKeyWord size id more then 0 then open the lookup result List and 
      // call the helper 
      // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchRes");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
             var forclose = component.find("searchRes");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
         
	},
  
  // function for clear the Record Selaction 
    clear :function(component,event,heplper){
      
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
    },
    
  // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
     
    // get the selected Account record from the COMPONETN event 	 
       var selectedAssetGetFromEvent = event.getParam("assetByEvent");
	   
	   component.set("v.selectedRecord" , selectedAssetGetFromEvent); 
       
        var forclose = component.find("lookup-pill");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
      
        
        var forclose = component.find("searchRes");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	},
  // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();    
    },
 // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();    
    },
    
})