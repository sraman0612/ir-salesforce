({
    initialize : function(cmp, event, helper) {
      
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({"contactID" : cmp.get("v.contactID")});

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);    
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                cmp.set("v.contact", responseVal.contact);
                cmp.set("v.userSites", responseVal.userSites);
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);       
        }    
        
        cmp.set("v.isLoading", false);
    },

    updateUser : function(cmp, event, helper) {
        
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.updateUser");
        var contact = cmp.get("v.contact");

        action.setParams({
            "userStr" : JSON.stringify(contact)
        });

        utils.callAction(action, helper.handleUpdateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleUpdateUserSuccess: function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                helper.updateUserSiteAccess(cmp, event, helper);
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
                cmp.set("v.isLoading", false);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);   
            cmp.set("v.isLoading", false);     
        }                    
    },        
    
    updateUserSiteAccess : function(cmp, event, helper) {
        
		var utils = cmp.find("utils");
        var action = cmp.get("c.updateUserSiteAccess");
        var contactID = cmp.get("v.contactID");
        var userSites = cmp.get("v.userSites");

        action.setParams({
            "contactId" : contactID,
            "userSitesStr" : JSON.stringify(userSites)
        });

        utils.callAction(action, helper.handleUpdateUserSiteAccessSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleUpdateUserSiteAccessSuccess : function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "User updated successfully.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();	
                
                var modalEvent = cmp.getEvent('onModalSave');
                modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
                modalEvent.fire();                   
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);        
        }    
        
        cmp.set("v.isLoading", false);
    },         

    processApprovalAction : function(cmp, event, helper, approvalDecision, reason) {
        
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.approvalAction");
        var contactID = cmp.get("v.contactID");

        action.setParams({
            "contactId" : contactID,
            "approve" : approvalDecision,
            "reason" : reason
        });

        utils.callAction(action, helper.handleApprovalActionSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleApprovalActionSuccess : function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		   
                
                if (responseVal.approve){
                    helper.activateUser(cmp, event, helper);
                }
                else{

                    var toastEvent = $A.get("e.force:showToast");
				
                    toastEvent.setParams({
                        mode : "dismissible",
                        duration : 2000,
                        message : "Access request successfully rejected.",
                        type : "success"			        
                    });
                    
                    toastEvent.fire();	                    

                    var modalEvent = cmp.getEvent('onModalSave');
                    modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
                    modalEvent.fire();  

                    cmp.set("v.approvalRequest", false);
                    cmp.set("v.isLoading", false);
                } 
			}
			else{
                utils.handleFailure(responseVal.errorMessage);	
                cmp.set("v.isLoading", false);
                cmp.set("v.approvalRequest", false);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);       
            cmp.set("v.isLoading", false);
            cmp.set("v.approvalRequest", false);
        }                    
    },   

    activateUser : function(cmp, event, helper) {
        console.log("activateUser");

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.activateUser");
        var contactID = cmp.get("v.contactID");

        action.setParams({
            "contactId" : contactID
        });

        utils.callAction(action, helper.handleActivateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleActivateUserSuccess: function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

                var approvalRequest = cmp.get("v.approvalRequest");
                var toastEvent = $A.get("e.force:showToast");
                
                if (approvalRequest){

                    toastEvent.setParams({
                        mode : "dismissible",
                        duration : 2000,
                        message : "Access request successfully approved.",
                        type : "success"			        
                    });
                }
                else{

                    toastEvent.setParams({
                        mode : "dismissible",
                        duration : 2000,
                        message : "User successfully activated.",
                        type : "success"			        
                    });
                }
			    
                toastEvent.fire();	 
                
                var modalEvent = cmp.getEvent('onModalSave');
                modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
                modalEvent.fire();   
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);        
        }    
        
        cmp.set("v.approvalRequest", false);
        cmp.set("v.isLoading", false);
    },    

    deactivateUser : function(cmp, event, helper) {
        console.log("deactivateUser");

        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.deactivateUser");
        var contactID = cmp.get("v.contactID");

        action.setParams({
            "contactId" : contactID
        });

        utils.callAction(action, helper.handleDeactivateUserSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleDeactivateUserSuccess: function(cmp, event, helper, response){
        
        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                                
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "User successfully deactivated.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();	   
                
                var modalEvent = cmp.getEvent('onModalSave');
                modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
                modalEvent.fire();   
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);       
        }    
        
        cmp.set("v.isLoading", false);
    },                 

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }    
})