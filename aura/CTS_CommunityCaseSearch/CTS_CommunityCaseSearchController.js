({
    init: function(component, event, helper) {
        helper.init(component, event);
    },
    onRender: function(component, event, helper) {
        var opt = document.getElementById('pageSize' + component.get("v.pageSize"));
        if (opt) opt.selected = true;
    },
    filter: function(component, event, helper) {
        if(component.get("v.searchType") == "allOnInit") {
        	component.set('v.isLoading', true);        
            //setTimeout(function() {
                helper.filter(component, event, helper);
                component.set('v.isLoading', false);
            //}, 300);   
        }        
    },
    updateColumnSorting: function(cmp, event, helper) {
        cmp.set('v.isLoading', true);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, event, helper, fieldName, sortDirection);
        cmp.set('v.isLoading', false);
    },
    onNext: function(component, event, helper) {
        helper.onNext(component, event, helper);
    },
    onPrevious: function(component, event, helper) {
        helper.onPrevious(component, event, helper);
    },
    pageSizeChanged: function(component, event, helper) {
        helper.pageSizeChanged(component, event, helper);
    },
    resetSearch: function(component, event, helper) {
        helper.resetSearch(component, event, helper);
    },
    search: function(component, event, helper) {
        helper.search(component, event);
    }
})