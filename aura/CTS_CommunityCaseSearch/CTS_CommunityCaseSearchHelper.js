({	
    init: function(component, event) {
        component.set('v.isLoading', true);
        
        //Set submitters picklist
    	var submitters = [];
        var mineObj = {};
        mineObj.label = $A.get("$Label.c.CTS_My_Cases");
        mineObj.value = 'mine';
        submitters.push(mineObj);
		var orgsObj = {};
        orgsObj.label = $A.get("$Label.c.CTS_My_Org_Cases");
        orgsObj.value = 'orgs';
        submitters.push(orgsObj);
        component.set('v.submitters', submitters);                
        
        //Populate Status picklist based the record types
        this.callAction( component, 'c.getPicklistOptions', {
            'objectName' : component.get('v.pickObjectName'),
            'fieldName'  : component.get('v.pickFieldName'),
            'recordTypes' : component.get('v.recordTypes')
        }, function( data ) {
			data = Array.from(new Set(data.map(JSON.stringify))).map(JSON.parse);
            data.sort(function(a, b) { //Sort by label, alphabetically ASC
                return a.label == b.label ? 0 : +(a.label > b.label) || -1;
            });
            var defaultOption = {};
            defaultOption.label = component.get("v.defaultOptionLabel");
            defaultOption.value = "";
            if(component.get("v.addDefaultOption")) data.unshift(defaultOption);
            console.log('statusPickVals**', data);
            component.set('v.statusPickVals', data);
        });		
        
        //call server to get search params/filters presented
        this.getSearchParams(component, event);
        
    },
    filter: function(component, event) {
        //Set results to blank
        component.set("v.filteredData", []);
        component.set("v.paginationList", []);        
       
        var opt = document.getElementById('pageSize' + component.get("v.pageSize"));
        opt.selected = true;
        
        var data = component.get("v.data"),
            filterText = component.get("v.filterText"),
            accFilter = component.get("v.accountName"),
            statusFilter = component.find("statusFilter") ? component.find("statusFilter").get("v.value") : "",
            rtFilter = component.get("v.rtFilter"),
            listType = component.get("v.listType"),
            closedStatuses = component.get("v.closedStatuses"),
            submitter = component.get("v.submitter"),
            cols = component.get("v.columns").map(function(item) {return item['fieldName'];}),
            results = data;

        try {
            //Filter checks each row, constructs new array where function returns true
            if(listType == 'search') {
                let user = component.get("v.userInfo")[0];
                if(filterText && filterText.length !== 0) {
                    results = results.filter(row =>   RegExp(filterText, "i").test(row.CaseNumber) || 
                                   			RegExp(filterText, "i").test(row.Subject) ||
                                   			RegExp(filterText, "i").test(row.Account) ||
                                   			RegExp(filterText, "i").test(row.Contact) ||
                                   			RegExp(filterText, "i").test(row.Status) ||
                                   			RegExp(filterText, "i").test(row.RecordType) ||
                                   			RegExp(filterText, "i").test(row.Asset)
                                  );
                }
                if(accFilter && accFilter.length !== 0) results = results.filter(row => row.AccountId == accFilter);
                if(submitter && submitter.length !== 0) results = results.filter(row => (submitter == 'mine' ? (row.CreatedById == user.Id || row.ContactId == user.ContactId) : (row.CreatedById != user.Id && row.ContactId != user.ContactId) ) );
                if(rtFilter && rtFilter.length !== 0) results = results.filter(row => rtFilter == row.RecordTypeId);
                if(statusFilter && statusFilter.length !== 0) results = results.filter(row => RegExp(statusFilter, "i").test(row.Status));
            } else if(listType == 'open') {
                var closedStatusVals = closedStatuses.map(stat => stat['MasterLabel']);
                var regex = new RegExp(closedStatusVals.join("|"));
                results = results.filter(row => !regex.test(row.Status));
            } else if(listType == 'closed') {
                var closedStatusVals = closedStatuses.map(stat => stat['MasterLabel']);
                var regex = new RegExp(closedStatusVals.join("|"));
                results = results.filter(row => regex.test(row.Status));
            } else if(listType == 'recent') {
                var d = new Date();
                var days = component.get("v.lastNDays");
                d.setDate(d.getDate() - Number(days)); //last n days
                results = results.filter(row => new Date(row.LastModifiedDate) >= d);
            } else if(listType == 'collaborator') {
                var contactId = component.get("v.userInfo")[0].ContactId;
                results = results.filter(row =>
                    (
                        (row.CTS_Case_Collaborator_1__c && row.CTS_Case_Collaborator_1__c == contactId) || 
                        (row.CTS_Case_Collaborator_2__c && row.CTS_Case_Collaborator_2__c == contactId) || 
                        (row.CTS_Case_Collaborator_3__c && row.CTS_Case_Collaborator_3__c == contactId)
                    )
				);
            }
            
            //For paging
            var pageSize = component.get("v.pageSize");
            component.set("v.totalRecords", results.length);
            component.set("v.startPage", 0);
            component.set("v.endPage", pageSize - 1);
            var pgList = [];
            for (var i = 0; i < pageSize; i++) {
                if (results.length > i) pgList.push(results[i]);
            }
            //Calculate total pages
            var totalPage = results.length % pageSize > 0 ? parseInt(results.length / pageSize) + 1 : results.length / pageSize;
            component.set('v.paginationList', pgList);
            component.set('v.totalPage', parseInt(results.length / pageSize) + 1);
            
        } catch(e) {           
            console.log('e**', e);
        }
        console.log('filteredData**', results);
        component.set("v.filteredData", results);
    },
    sortData: function (cmp, event, helper, fieldName, sortDirection) {
        var data = cmp.get("v.filteredData");
        var reverse = sortDirection !== 'asc';
        if (fieldName == 'Show_Record__c') {
            data.sort(this.sortBy('CaseNumber', reverse));
        } else {
            data.sort(this.sortBy(fieldName, reverse));
        }
        cmp.set("v.filteredData", data);
        helper.pageSizeChanged(cmp, event, helper); //call the paging method to reset
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
        ? function(x) { return primer(x[field]) }
        : function(x) { return x[field] };
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a) ? key(a) : '', b = key(b) ? key(b) : '', reverse * ((a > b) - (b > a));
        };
    },
    onNext: function(component, event) { //on click of next button for pagination
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.filteredData");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                pagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        component.set('v.currentPage', currPage + 1);
    },    
    onPrevious: function(component, event, helper) { //on click of previous button for pagination
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.filteredData");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                pagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        if (currPage > 1) {
            component.set('v.currentPage', currPage - 1);
        }
    },    
    pageSizeChanged: function(component, event, helper) { //on change of page size for pagination
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.filteredData");        
        var pageSize = parseInt(component.find("pageSize").getElement().value);
        component.set("v.pageSize", pageSize);
        component.set("v.totalRecords", component.get("v.filteredData").length);
        component.set("v.startPage", 0);
        component.set("v.endPage", pageSize - 1);
        var pgList = [];
        for (var i = 0; i < pageSize; i++) {
            if (sObjectList.length > i) {
                pgList.push(sObjectList[i]);
            }
        }
        component.set('v.paginationList', pgList);
        component.set('v.currentPage', 1);
        console.log('sObjectList**', sObjectList.length, pageSize, sObjectList.length/pageSize);
        var totalPage = Math.ceil(sObjectList.length/pageSize);
        component.set('v.totalPage', totalPage == 0 ? 1 : totalPage);
    },
    resetSearch: function(component, event, helper) {       	
       	component.set("v.accountName", "");
       	component.find("statusFilter").set("v.value", "");
       	component.set("v.rtFilter", "");
       	component.set("v.submitter", "mine");
        component.set("v.filterText", "");
        component.set('v.currentPage', 1);
        component.set('v.totalRecords', 0);
        component.set('v.totalPage', 1);
        
        let searchType = component.get("v.searchType");
        if(searchType != 'allOnInit') {
        	component.set("v.data", []);
        	component.set("v.filteredData", []);
            component.set("v.paginationList", []);            
        } else {            
        	helper.filter(component, event, helper);   
        }            	
    },
    search: function(component, event) {
        component.set('v.isLoading', true);
        //Query filters
        let listType = component.get("v.listType");
        let searchType = component.get("v.searchType");
        let qfKeyword = component.get("v.filterText");
        let qfAccountId = component.get("v.accountName");
        let qfSubmitter = component.get("v.submitter");
        let qfStatusFilter = component.get("v.statusFilter");
        let qfRTFilter = component.get("v.rtFilter");
        
        //call server for querying cases        
        var action = component.get("c.getAllCases");
        action.setParams({
            strObjectName: 'Case',
            strFieldSetName: 'CTS_Case_Search_Fields',
            listType: listType,
            searchType: searchType,            
            qfKeyword: qfKeyword,
            qfAccountId: qfAccountId,
            qfSubmitter: qfSubmitter,
            qfStatusFilter: qfStatusFilter,
            qfRTFilter: qfRTFilter
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var res = response.getReturnValue();

                var data = res[0].lstDataTableData;
                console.log('data**', JSON.stringify(data));
                var tempCols = res[0].lstDataTableColumns;
                var columns = [];                

                for (var j = 0; j < tempCols.length; j++) {
                    var col = tempCols[j];
                    col.sortable = true;
                    if (!col.fieldName.endsWith('Id') && col.fieldName != 'CaseNumber') {
                        col.fieldName = col.fieldName.replace('.Name', '');
                        columns.push(col);
                    }
                    if (col.fieldName == 'Show_Record__c') {
                        col.label = 'Case Number';
                        col['typeAttributes'] = {label: {fieldName: 'CaseNumber'}};
                        col.sortable = true;
                        col.type = 'url';
                    }
                }

                //Accounts names associated with cases
                var accountNames = [];//component.get('v.accountNames');                

                for (var i = 0; i < data.length; i++) {
                    var row = data[i];
                    if (row.RecordType) row.RecordType = row.RecordType.Name;
                    if (row.Account) {
                        row.Account = row.Account.Name;
                        //if(searchType == 'allOnInit') {
                        	var acc = {};
                            acc.label = row.Account;
                            acc.value = row.AccountId;
                            accountNames.push(acc);
                        //}
                    }
                    if (row.Asset) row.Asset = row.Asset.Name;
                    if (row.Contact) row.Contact = row.Contact.Name;
                }
                
                let uniqueAccounts = accountNames.map(e => e['value']).map((e, i, final) => final.indexOf(e) === i && i).filter(e => accountNames[e]).map(e => accountNames[e]);
                uniqueAccounts.sort(function(a, b) { //Sort by label, alphabetically ASC
                    return a.label.toLowerCase() == b.label.toLowerCase() ? 0 : +(a.label.toLowerCase() > b.label.toLowerCase()) || -1;
                });
                var allAcc = {};
                allAcc.label = $A.get("$Label.c.CTS_All");
                allAcc.value = "";
                uniqueAccounts.unshift(allAcc);
        		component.set('v.accountNames', uniqueAccounts);

                component.set("v.columns", columns);
                component.set("v.data", data);
                component.set("v.filteredData", data);

                this.pageSizeChanged(component, event); //call the paging method to reset
                this.sortData(component, event, this, 'Show_Record__c', 'DESC');
                component.set("v.sortedBy", 'Show_Record__c');
        		component.set("v.sortedDirection", 'DESC');

                if(searchType == 'allOnInit') this.filter(component, event);
                component.set('v.isLoading', false);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set('v.isLoading', false);
            } else {
                console.log('Something went wrong, Please check with your admin');
                component.set('v.isLoading', false);
            }
        });
        $A.enqueueAction(action);
        
        
    },
    getSearchParams: function(component, event) {
        var action = component.get("c.getSearchParamsOnLoad");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var res = response.getReturnValue();

                component.set("v.closedStatuses", res[0]);
                component.set("v.userInfo", res[1]);
                if(res.length > 3 && res[2]) {
                    component.set("v.recordTypes", res[2]);
                    
                    //Set record types picklist
                    var rts = res[2];
                    var rtBox = [];
                    var rtObjAll = {};
                    rtObjAll.label = $A.get("$Label.c.CTS_All");
                    rtObjAll.value = "";
                    rtBox.push(rtObjAll);
                    rts.forEach(rt => {
                        var rtObj = {};
                        rtObj.label = rt.Name;
                        rtObj.value = rt.Id;
                        rtBox.push(rtObj);
                    });
                    component.set('v.caseRTs', rtBox);
                }                
                
            	if(res.length >= 5 && res[4]) { //Set the custom setting to be used by other logic
                    component.set('v.setting', res[4]);
                }
            	if(res.length >= 6 && res[5]) { //check the count to set search type
                    console.log('res[5]', res[5], typeof res[5]);
                    console.log('component.get("v.caseCountLimit")', component.get("v.caseCountLimit"), typeof component.get("v.caseCountLimit"));
                    if(res[5] > component.get("v.caseCountLimit")) component.set("v.searchType", "filterBased");
                }
            	let searchType = component.get("v.searchType");
        		let listType = component.get("v.listType");
                if(searchType != 'allOnInit') {                	
                    /*var accountNames = []; //Accounts names associated with cases               
                    
                    if(res.length >= 4 && res[3]) {
                        res[3].forEach( a => {
                            var acc = {};
                            acc.label = a.Name;
                            acc.value = a.Id;
                            accountNames.push(acc);
                        });
                    }
                    let uniqueAccounts = accountNames.map(e => e['value']).map((e, i, final) => final.indexOf(e) === i && i).filter(e => accountNames[e]).map(e => accountNames[e]);
                    uniqueAccounts.sort(function(a, b) { //Sort by label, alphabetically ASC
                        return a.label.toLowerCase() == b.label.toLowerCase() ? 0 : +(a.label.toLowerCase() > b.label.toLowerCase()) || -1;
                    });*/
            		var allAcc = {};
                    allAcc.label = $A.get("$Label.c.CTS_All");
                    allAcc.value = "";
                    /*uniqueAccounts.unshift(allAcc);
                    component.set('v.accountNames', uniqueAccounts);*/
                    component.set('v.accountNames', [allAcc]);
                }
        		
        		//Call search if all visible cases should be quried on load        		
                if(searchType == "allOnInit" || listType == "open" || listType == "recent" || listType == "closed" || listType == "collaborator") {
            		this.search(component, event);
        		} else component.set('v.isLoading', false);
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set('v.isLoading', false);
            } else {
                console.log('Something went wrong, Please check with your admin');
                component.set('v.isLoading', false);
            }
        });
        $A.enqueueAction(action);
    },
    callAction : function( component, actionName, params, successCallback, failureCallback ) {        
        var action = component.get( actionName );        
        if(params) {
            action.setParams( params );
        }
        action.setCallback( this, function( response ) {
            if( component.isValid() && response.getState() === 'SUCCESS' ) {
                if( successCallback ) {
                    successCallback( response.getReturnValue() );
                }
            } else {
                console.error( 'Error calling action "' + actionName + '" with state: ' + response.getState() );
                if( failureCallback ) {
                    failureCallback( response.getError(), response.getState() );
                } else {
                    this.logActionErrors( component, response.getError() );
                }
            }
        });
        $A.enqueueAction( action );
    },
    logActionErrors : function( component, errors ) {
        if( errors ) {
            for( var index in errors ) {
                console.error( 'Error: ' + errors[index].message );
            }
        } else {
            console.error( 'Unknown error' );
        }
    }
})