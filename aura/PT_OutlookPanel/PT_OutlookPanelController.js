({	
    onCheckLogs: function(cmp,evt,hlp)
    {
        var isChecked = cmp.find("logCheckbox").get("v.checked");
        cmp.set("v.showLogs", isChecked);      
    },
    init: function(cmp, evt, hlp)
    {   
        var log = '[OutlookEventPanel].Init';
        cmp.set('v.log',log);

        var mode = cmp.get('v.mode');
        var source = cmp.get('v.source');
        
        if(source === 'event')
        {
            hlp.pollPeople(cmp,evt,hlp);
            var source = cmp.get('v.source');
            
            var eventcontext = {};
            eventcontext.dates = cmp.get('v.dates');
            eventcontext.subject = cmp.get('v.subject');
            eventcontext.messageBody = cmp.get('v.messageBody');
            eventcontext.people = cmp.get('v.people');
            
            var getEventAction = cmp.get('c.getEvent');
            
            var getEventActionParams = {
                eventContextJson: JSON.stringify(eventcontext)
            }
            
            console.log('[OutlookEventPanel].Init getEventActionParams',getEventActionParams);
            getEventAction.setParams(getEventActionParams);
            
            getEventAction.setCallback(this, function(response){
                
                console.log('[OutlookEventPanel].Init getEventAction callback response state:',response.getState());
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('[OutlookEventPanel].Init getEventAction response returnValue',response.getReturnValue());
                    
                    var result = response.getReturnValue();
                    cmp.set('v.log','No event found' + JSON.stringify(result));
                
                    if(result.isSuccess)
                    {
                        var responseEvents = result.events;
                        var calendarEvent = {};
                        if(responseEvents.length === 0)
                        {
                            calendarEvent.type = '';
                            calendarEvent.accountId = '';
                            cmp.set('v.panelStatus','No Event');
                        }
                        else
                        {
                            var responseEvent = responseEvents[0];
                            calendarEvent.id = responseEvent.Id;
                            calendarEvent.objectType = responseEvent.objectType;
                            
                            var accounts = [];
                            var account = {
                                "icon":"standard:account",
                                "sObjectType":"Account",
                                "subtitle":"Account • null"
                            };
                            if(result.objectType === 'PT_Outlook_Event__c')
                            {
                                calendarEvent.type = responseEvent.Type__c;
                                calendarEvent.whom = responseEvent.Whom__c;
                                calendarEvent.accountId = responseEvent.Account__c;

                                var responseAccount = responseEvent.Account__r;
                                account.id = responseEvent.Account__c;
                                account.title = responseAccount.Name;
                                accounts.push(account);

                                cmp.set('v.log','Outlook_Event__c found with result ' + JSON.stringify(result) + '. calendarEvent:'+JSON.stringify(calendarEvent));
                            	cmp.set('v.panelStatus',result.objectType);
                            }
                            else
                            {
                                calendarEvent.type = responseEvent.Type;
                                calendarEvent.whom = responseEvent.PT_Whom__c;
                                calendarEvent.accountId = responseEvent.WhatId;

                                if(result.RelatedAccount !== undefined)
                                {
                                    var responseAccount = result.RelatedAccount;
                                    account.id = responseAccount.Id;
                                    account.title = responseAccount.Name;
                                    accounts.push(account);
                                }
                                cmp.set('v.log','Salesforce Event found with result ' + JSON.stringify(result)  + '. calendarEvent:'+JSON.stringify(calendarEvent));
								cmp.set('v.panelStatus',result.objectType + '('+responseEvent.Id+')');
                            }
                            cmp.set('v.selectedAccount',accounts);
                        	                         
                        }
                        cmp.set('v.calendarEvent', calendarEvent);  
                    }
                    else
                    {
                        cmp.set('v.log',result.errorMessage);
                        hlp.toastError(result.errorMessage);
                    }
                    
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            cmp.set('v.log','$A.enqueueAction(getEventAction)');
            $A.enqueueAction(getEventAction)
        }
    },
    lookupSearch : function(cmp, evt, hlp) {
        const lookupComponent = evt.getSource();
        const serverSearchAction = cmp.get('c.search');
        serverSearchAction.setParam('anOptionalParam', 'not used');
        lookupComponent.search(serverSearchAction);
    },

    onSelectAccount: function(cmp, evt, hlp) {
        const selection = cmp.get('v.selectedAccount');
        const errors = cmp.get('v.accountErrors');

        if (selection.length && errors.length) {
            cmp.set('v.log', 'Account deleted');
            cmp.set('v.accountErrors', []);
        }
        else
        {
            cmp.set('v.log', 'Account selected');
            var attendees = cmp.get('v.attendees');
            attendees.forEach(function(attendee){
                
            });
        }
        
        cmp.set('v.log', JSON.stringify(selection));
    },
    onUpdateEvent : function(cmp, evt, hlp) {
        cmp.set('v.spinner','TRUE');
		var eventcontext = cmp.get('v.calendarEvent');
        
        var accounts = cmp.get('v.selectedAccount');
        if(accounts.length === 0) 
        {
            cmp.set('v.log', 'Please choose an account');
            cmp.set('v.spinner',FALSE);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": 'error',
                "title": "Please select an account",
                "message": "Please select an account"
            });
    		toastEvent.fire();
            return null;
            
        }

        eventcontext.accountId = accounts[0].id;
        eventcontext.dates = cmp.get('v.dates');;
        eventcontext.subject = cmp.get('v.subject');

        var attendees = cmp.get('v.attendees');
        var contacts = [];
        attendees.forEach(function(attendee){
            if(attendee.statusCode === '003' || attendee.statusCode === '004' || attendee.statusCode === '005')
            {
            	contacts.push(attendee);
        	}
        });
        eventcontext.contacts = contacts;
        
        cmp.set('v.log',eventcontext);
        var updateEventActionParams = {
            eventContextJson: JSON.stringify(eventcontext)
        }
        console.log('updateEventActionParams',updateEventActionParams);
        
        var updateEventAction = cmp.get('c.updateEvent');
        updateEventAction.setParams(updateEventActionParams);
        
        updateEventAction.setCallback(this, function(response){
            console.log('[Salesforce] updateEventAction callback response state:',response.getState());
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
            	cmp.set('v.log',JSON.stringify(result));
                if(result.isSuccess)
                {
                    console.log('Processing success');
                    //cmp.set('v.log','Event Updated');
                    var calendarEvent = cmp.get('v.calendarEvent');
                    calendarEvent.id = result.upsertedEvent.Id;
                    
                    cmp.set('v.calendarEvent',calendarEvent);
                    
                    console.log('Checking upsertedContacts');
                    var attendees = cmp.get('v.attendees');
                    if(typeof(result.upsertedContacts) !== "undefined"){
                        var upsertedContacts = result.upsertedContacts;
                        
                        upsertedContacts.forEach(function(upsertedContact){
                            console.log('upsertedContacts',upsertedContact);
                            attendees.forEach(function(attendee){
                            console.log('looping attendees. attendee.email:'+attendee.email + ' - upsertedContact.email:'+upsertedContact.Email);
                                if(attendee.email === upsertedContact.Email)
                                {
                                    console.log('attendee matched');
                                    attendee.Id = upsertedContact.Id;
                                    attendee.statusCode = '005';
                                    //attendee.statusLabel = hlp.getStatusLabel('005');
                                    attendee.checked = false;
                                    attendee.disabled = true;
                                    attendee.hasCheckbox = false;
                                    
                                    attendee.statusLabel = hlp.setStatusLabel(cmp, attendee);
                                    
                                }
                            });
                            
                        });
                    }
                    //cmp.set('v.log','attendees:' +JSON.stringify(attendees));
                    cmp.set('v.attendees', attendees);
                    cmp.set('v.spinner',false);
                    cmp.set('v.panelStatus',result.objectType);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": 'success',
                        "title": "Success",
                        "message": "Event succesfully updated"
                    });
                    toastEvent.fire();
                }
                else
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode": 'sticky',
                        "type": 'error',
                        "title": "Error!",
                        "message": result.errorMessage
                    });
    				toastEvent.fire();
                    cmp.set('v.spinner',false);
                }
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        cmp.set('v.spinner',false);
                    }
                    cmp.set('v.spinner',false);
                } else {
                    console.log("Unknown error");
                    cmp.set('v.spinner',false);
                }
            }
        });
        //cmp.set('v.log','$A.enqueueAction(updateEventAction)');
        $A.enqueueAction(updateEventAction);
        
        
	},
    onContactToggle: function(cmp,evt,hlp)
    {
        let checkbox = evt.getSource();
        let checkboxname = checkbox.get("v.name");
        
        
        let attendeeIndex = parseInt(checkboxname);
        
        let attendees = cmp.get('v.attendees');
        let attendee = attendees[attendeeIndex];
        
        //cmp.set('v.log','checked:' + attendee.checked + ' - ' + JSON.stringify(attendee));
        attendee.statusLabel = hlp.setStatusLabel(cmp, attendee);
    }
})