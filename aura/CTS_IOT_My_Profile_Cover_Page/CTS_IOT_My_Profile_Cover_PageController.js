({
    init : function(cmp, event, helper){
        console.log("CTS_IOT_My_Profile_Cover_Page:init");

        helper.initialize(cmp, event, helper);
    },
    onEditProfile : function(cmp, event, helper) {
        console.log("onEditProfile");

        // Update breadcrumbs
        // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
        
        // navEvent.setParams({
        //     "label" : "MY PROFILE",
        //     "url" : "/profile/home"
        // });

        // navEvent.fire(); 

        var navService = cmp.find("navService");

        var pageReference = {
            type: 'standard__webPage',
            attributes: {
                url: '/profile/home'
            }
        };

        navService.navigate(pageReference);        
    }
})