({
    doInit : function(cmp, event, helper) {

        console.log("CC_Order_LoadDetailsWrapper:doInit")
        
        let pageRef = cmp.get("v.pageReference");

        if (pageRef && pageRef.state){
            cmp.set("v.recordId", cmp.get("v.pageReference").state.c__id);
        }
    },

    onPageReferenceChange : function(cmp, event, helper){

        console.log("onPageReferenceChange");

        if (cmp.get("v.pageReference") && cmp.get("v.pageReference").state){

            let recordId = cmp.get("v.pageReference").state.c__id;
            console.log('recordId: ' + recordId);

            cmp.set("v.recordId", recordId);

            // Due to quick action caching, we must refresh the view now
            // https://salesforce.stackexchange.com/questions/295116/connectedcallback-disconnectedcallback-not-firing-on-2nd-display
            $A.get('e.force:refreshView').fire();
        }        
    },    

    onModalCancel: function(cmp, event, helper){

        console.log("onModalCancel");
        cmp.set("v.showModal", false);
    },    

    handleIsLoadingChange: function(cmp, event, helper){

        let isLoading = event.getParam("isLoading");
        let isLoadingCallback = cmp.get("v.isLoadingCallback");

        if (isLoadingCallback){
            isLoadingCallback({isLoading});
        }        
    } 
})