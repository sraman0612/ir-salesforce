({
    doInit : function(component, event, helper) {
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCase');

        // params to the method are pass as an object with property names matching
        var params = {
            "caseId" : component.get("v.recordId")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.case", payload);
                console.log('case Id ===> '+component.get("v.case.Id"));
                console.log('case OwnerId ===> '+component.get("v.case.OwnerId"));
                console.log('case Status ===> '+component.get("v.case.Status"));
                console.log('case AccountId ===> '+component.get("v.case.AccountId"));
                console.log('case Org_Wide_Email_Address_ID__c ===> '+component.get("v.case.Org_Wide_Email_Address_ID__c"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.getRecordType(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    getRecordType :function(component, event, helper){
        // locate the apex proxy component within this component
        var apexProxy = component.find('apexProxy');

        // the "action" is the apex @auraenabled method on this component's controller
        var action = component.get('c.getCaseRecType');

        // params to the method are pass as an object with property names matching
        var params = {
            "name" : component.get("v.recordTypeName")
        };

        // call the aura:method exposed on the ApexProxy component
        apexProxy.call(
            action,
            params,
            function(payload) {
                // onSuccess function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                component.set("v.recordTypeId", payload);
                console.log('case record type name ===> '+component.get("v.recordTypeName"));
                console.log('case record type Id ===> '+component.get("v.recordTypeId"));
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
                helper.createCaseAI(component, event, helper);
            },
            function(payload) {
                // onError function
                // anonymous function retains references to component, event and helper
                // ApexProxy component passes "payload", which is whatever the Apex method returns
                console.log(payload);
                // from here, you could make further calls to helper.whateverMethodToDoStuff();
            }
        );
    },
    createCaseAI : function(component, event, helper) {
        var currCase = component.get("v.case");
        var caseId = component.get("v.recordId");
        var acctId = component.get("v.case.AccountId");
        var caseRecTypeId = component.get("v.recordTypeId");
        console.log('case,acct and recType ===> '+caseId+', '+acctId+', '+caseRecTypeId);

        var createRecordCase = $A.get("e.force:createRecord");

        createRecordCase.setParams({
            "entityApiName": "Case",
            "defaultFieldValues": {
                'RecordType' : caseRecTypeId,
                'Subject': currCase.Subject,
                'CaseNumber': currCase.CaseNumber,
                'ParentId': caseId,
                'AccountId': currCase.AccountId,
                'Description': currCase.Description
            }
        });
        createRecordCase.fire();
    },
    cancelCaseAI: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})