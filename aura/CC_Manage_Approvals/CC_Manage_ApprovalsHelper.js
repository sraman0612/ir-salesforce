({
    getApprovals: function (cmp, event, helper) {
        console.log("getApprovals");

        cmp.set("v.isLoading", true); 
        cmp.set("v.approvalCount", 0);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getApprovals");

        var request = {
            "viewModes" : cmp.get("v.viewModes"),
            "showAll" : cmp.get("v.showAll"),
            "showLimit" : cmp.get("v.showLimit")
        };

        action.setParams({
            "requestStr" : JSON.stringify(request)
        });        
        
        console.log("calling get approvals");

        utils.callAction(action, helper.handleGetApprovalsSuccess, helper.handleActionCallFailure, cmp, helper);
    },

    handleGetApprovalsSuccess: function(cmp, event, helper, response){
    
		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                
                for (obj in responseVal.objects){
                    console.log(obj);                    
                }

                cmp.set("v.approvalCount", responseVal.approvalCount);

                var data = responseVal.approvalsByObj;

                for (var i = 0; i < data.length; i++){
                    
                    var objApprovals = data[i];
                    console.log(objApprovals);
                    
                    // Go through each approval and add an attribute for each defined column
                    for (var x = 0; x < objApprovals.approvals.length; x++){

                        var objApproval = objApprovals.approvals[x];

                        for (var y = 0; y < objApprovals.columns.length; y++){
                            
                            var column = objApprovals.columns[y];

                            if (column.fieldName != undefined && objApproval.record[column.fieldName] != undefined){                              
                                objApproval[column.fieldName] =  objApproval.record[column.fieldName];                                                                                         
                            }
                            else if (column.typeAttributes != undefined && column.typeAttributes.label != undefined){
                                objApproval[column.typeAttributes.label.fieldName] = objApproval.record[column.typeAttributes.label.fieldName];      
                            }
                            else if (column.fieldName == undefined){
                                objApproval[column.fieldName] =  objApproval.record[column.label];
                            }
                        }
                    }
                }
                
                console.log(data);

                cmp.set("v.data", data);   
                
                var utils = cmp.find("utils");
                var body = cmp.get("v.body")
                utils.logObjectToConsole(body);
			}
			else{
			
                console.log("failed response");
                
                // Display an error message
				var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
			    });
			    
				toastEvent.fire();				
			}	
        }
        catch(e){
        	console.log("exception: " + e);
        }    
        
        cmp.set("v.isLoading", false);        
    },
    
    submitManagerApprovalOverride: function (cmp, event, helper, approve) {
        console.log("submitManagerApprovalOverride");

        cmp.set("v.isLoading", true);  

		var utils = cmp.find("utils");
        var action = cmp.get("c.submitManagerApprovalOverride");

        var params = {
            "recordId" : cmp.get("v.selectedRecordId"),
            "requestId" : cmp.get("v.selectedRequestId"),
            "comments" : cmp.get("v.approvalComments"),
            "approve" : approve
        };

        action.setParams(params);        
        
        utils.callAction(action, helper.handleSubmitManagerApprovalOverrideSuccess, helper.handleActionCallFailure, cmp, helper);
    },

    handleSubmitManagerApprovalOverrideSuccess: function(cmp, event, helper, response){

        try{
            
            var toastEvent = $A.get("e.force:showToast");
            var responseVal = response.getReturnValue();
            
			console.log(responseVal);
				
			if (responseVal.success){		
                
                cmp.set("v.showMgrApprovalOverrideModal", false);
                cmp.set("v.selectedRecordId", null);
                cmp.set("v.selectedRequestId", null);
                cmp.set("v.approvalComments", "");
                
                toastEvent.setParams({
			        mode : "dismissible",
			        duration : 2000,
			        message : "Approval override request has been submitted.",
			        type : "success"			        
                });
                
                helper.getApprovals(cmp, event, helper);
			}
			else{
			
                console.log("failed response");
                
                // Display an error message
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 5000,
			        message : responseVal.errorMessage,
			        type : "error"			        
                });		
                
                cmp.set("v.isLoading", false); 
            }	
            
            toastEvent.fire();
        }
        catch(e){
            console.log("exception: " + e);
            cmp.set("v.isLoading", false); 
        }           
    },

    handleActionCallFailure: function(cmp, event, helper, response){
					
        console.log("failed action call");
        
        // Display an error message
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            mode : "dismissible",
            duration : 5000,
            message : response.getError(),
            type : "error"			        
        });
        
        toastEvent.fire();	

 		cmp.set("v.isLoading", false);
 		
        // Close the action panel
	    $A.get("e.force:closeQuickAction").fire(); 		        
    },
    sortData : function(cmp, column, obj, sortDirection){

        var data = cmp.get("v.data");
        var reverse = sortDirection !== "asc";

        // Update the sorted field, direction, and actual data for the selected object/table
        for (var i = 0; i < data.length; i++){
            
            var objApprovals = data[i];
            console.log(objApprovals);  
            
            if (objApprovals.objName == obj){    

                objApprovals.sortDirection = sortDirection;
                objApprovals.sortField = column;
                objApprovals.approvals.sort(this.sortBy(column, reverse))
            }
        }
     
        cmp.set("v.data", data);        
    },
    sortBy : function (column, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[column])} :
            function(x) {return x[column]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }    
})