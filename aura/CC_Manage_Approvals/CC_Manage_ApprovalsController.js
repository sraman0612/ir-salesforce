({
    init : function(cmp, event, helper) {
        console.log("CC_Manage_ApprovalsController.init");
        
        var viewModeOptions = [
            {"label" : "My Approvals", "value" : "mine"},
            {"label" : "My Team's Approvals", "value" : "myteam"},
            {"label" : "Delegated Approvals", "value" : "mydelegates"}
        ];

        cmp.set("v.viewModeOptions", viewModeOptions);

        helper.getApprovals(cmp, event, helper);
    },
    handleRowAction : function(cmp, event, helper){
        console.log("handleRowAction");

        var utils = cmp.find("utils");
        var action = event.getParam('action');
        var row = event.getParam('row');

        console.log("action: " + action.name);
        utils.logObjectToConsole(row);

        switch (action.name) {
            case 'manager_override':  
                cmp.set("v.showMgrApprovalOverrideModal", true);  
                cmp.set("v.selectedRecordId", row.record.Id);   
                cmp.set("v.selectedRequestId", row.requestId);        
                break;
        }
    },
    onApprovalOverride : function(cmp, event, helper){
        console.log("onApprovalOverride");
        helper.submitManagerApprovalOverride(cmp, event, helper, true);
    },
    onRejectionOverride : function(cmp, event, helper){
        console.log("onRejectionOverride");
        helper.submitManagerApprovalOverride(cmp, event, helper, false);
    },   
    onMgrOverrideCancel : function(cmp, event, helper){
        console.log("onMgrOverrideCancel");
        cmp.set("v.showMgrApprovalOverrideModal", false);
        cmp.set("v.selectedRecordId", null);
        cmp.set("v.selectedRequestId", null);
        cmp.set("v.approvalComments", "");
    }, 
    onViewModeChange : function(cmp, event, helper){
        console.log("onViewModeChange");

        var viewModes = cmp.get("v.viewModes");

        var showMine = false, showMyTeam = false, showMyDelegates = false;

        for (var i = 0; i < viewModes.length; i++){

            if (viewModes[i] == "mine"){
                showMine = true;
            }
            else if (viewModes[i] == "myteam"){
                showMyTeam = true;
            }    
            else if (viewModes[i] == "mydelegates"){
                showMyDelegates = true;
            }        
        }

        if (showMyTeam || showMyDelegates){
            cmp.set("v.showLimit", 50);
        }
        else{
            cmp.set("v.showLimit", 25);
        }

        helper.getApprovals(cmp, event, helper);
    },
    onViewAllClick : function(cmp, event, helper){
        console.log("onViewAllClick");
        cmp.set("v.showAll", !cmp.get("v.showAll"));
        helper.getApprovals(cmp, event, helper);
    },
    onColumnSort : function(cmp, event, helper){
        console.log("onColumnSort");
        var obj = event.getSource().get("v.class").substring(6);
        var column = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");

        console.log("obj: " + obj)
        console.log("column: " + column);
        console.log("sortDirection: " + sortDirection);

        helper.sortData(cmp, column, obj, sortDirection);
    }    
})