({
	doInit : function(component, event, helper) {
		var action = component.get("c.getCanVote");
        var rId = component.get("v.recordId");
        action.setParams({
            recordId: rId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var canVote = response.getReturnValue();
                component.set("v.canVote", canVote);                    
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    upVote : function(component, event, helper) {
		var action = component.get("c.vote");
        var rId = component.get("v.recordId");
        action.setParams({
            recordId: rId,
            voteType : 'Up'});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var successStr = response.getReturnValue();
                if(successStr == 'SUCCESS'){
                    component.set("v.canVote", true); 
                }
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    downVote : function(component, event, helper) {
		var action = component.get("c.vote");
        var rId = component.get("v.recordId");
        action.setParams({
            recordId: rId,
            voteType : 'down'});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var successStr = response.getReturnValue();
                if(successStr == 'SUCCESS'){
                    component.set("v.canVote", true); 
                }
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	}
})