({
	doInit : function(component, event, helper) {
		var action = component.get("c.getInit");
        var artId = component.get("v.recordId");
        action.setParams({recordId: artId});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = JSON.parse(response.getReturnValue());
                console.log('results init**', results);
                if(results != null) {
                    component.set("v.hasVotedWith", results[0]);
                    component.set("v.following", results[1]);
                }
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    upVote : function(component, event, helper) {
        var action = component.get("c.vote");
        var artId = component.get("v.recordId");
        action.setParams({aId: artId, type: 'Up'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.hasVotedWith", 'Up');
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    downVote : function(component, event, helper) {
        var action = component.get("c.vote");
        var artId = component.get("v.recordId");
        action.setParams({aId: artId, type: 'Down'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.hasVotedWith", 'Down');
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    follow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        console.log('recId**', recId);
        action.setParams({recId: recId, action: 'follow'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", true);
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    unfollow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        action.setParams({recId: recId, action: 'unfollow'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", false);
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	}
})