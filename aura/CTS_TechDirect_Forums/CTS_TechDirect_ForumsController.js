({
	initialize : function(cmp, event, helper) {
	
		console.log("CTS_TechDirect_Forums.controller.initialize");
		
		helper.getInit(cmp, event, helper);
	},
	
	onAcceptTerms : function(cmp, event, helper){
	
		console.log("CTS_TechDirect_Forums.controller.onAcceptTerms");
	
		helper.acceptTerms(cmp, event, helper);
	}
})