({
    commentCase : function(component, event, helper) {
        var caseId = component.get("v.recordId");
        console.log(caseId);
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            var url = '/apex/C_Action_Item_Resolution?Id='+caseId+'&OriginatingId='+caseId+'&tabId='+focusedTabId;
            console.log(url);
            workspaceAPI.openSubtab({
                parentTabId: focusedTabId,
                url:  url,
                focus: true
            }).then(function(response) {
                workspaceAPI.refreshTab({
                    tabId: focusedTabId,
                    includeAllSubtabs: false
                });
            })
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    cancelCase: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})