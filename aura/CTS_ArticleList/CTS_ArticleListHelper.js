({
	//method called on init
	getSearchConfig: function(component, event, helper) {

		var tabName = component.get("v.tabName");
		//Populate Data Category picklists
		var action = component.get("c.getFilters");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				var results = response.getReturnValue();
				//set the data category picklists
                //console.log("data categories:");
                //console.log(JSON.stringify(JSON.parse(results[0])));
				helper.setDataCatPicks(JSON.parse(results[0]),component);
				component.set("v.langList", JSON.parse(results[1]));
				component.set("v.userInfo", JSON.parse(results[2]));
				component.set("v.selectedLang", component.get("v.userInfo").LanguageLocaleKey);
			} else {
				console.log('error' + state);
			}
		});
		$A.enqueueAction(action);

		//Read keyword from param if keyword is null
		var searchText = component.get("v.searchText");
		if (searchText == null || searchText == '') {
			var getUrlParameter = function getUrlParameter(sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
					sURLVariables = sPageURL.split('&'),
					sParameterName, i;
				for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');
					if (sParameterName[0] === sParam) {
						var searchStr = sParameterName[1].replace(/\+/g, " ");
						return searchStr === undefined ? true : searchStr;
					}
				}
			};
			var initVal = getUrlParameter('keyword');
			component.set("v.searchText", initVal);
		}
		if (component.get("v.searchConfig") == null) {
			var action = component.get("c.getSearchConfigDetails");
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === 'SUCCESS') {
					var searchConfig = response.getReturnValue();
					component.set("v.searchConfig", searchConfig);
					helper.setPaging(component);
					//call search if a param was passed from the URL
					var searchText = component.get("v.searchText");
					if (tabName == 'search' && searchText) {
						component.set("v.searchOrTypeahead", "search");
						helper.searchKBHelper(component, event, helper);
					}
				} else {
					console.log('error' + state);
				}
			});
			$A.enqueueAction(action);
		}
	},
	setPaging: function(component) {
		var searchConfig = component.get("v.searchConfig");

		var pageSizeSelectJson = [];
		var pageSizeSelectVals = searchConfig.pageSizeOptions.split(',');
		for (var i = 0; i < pageSizeSelectVals.length; i++) {
			var temp = {};
			temp.label = pageSizeSelectVals[i];
			temp.value = pageSizeSelectVals[i];
			pageSizeSelectJson.push(temp);
		}
		component.set("v.pageSizeSelect", pageSizeSelectJson);
	},
	setSubProd: function(component, event, helper) {
		var whofired = event.target.id != 'enter-search-div' ? event
			.getSource().get("v.name") : '';
		if (whofired == 'productSelect') {
			component.set("v.selectedSubProd", "");
			var selectedProduct = component.get('v.selectedProduct');
			if (selectedProduct != '') {
				var subProdList = JSON.parse(component.get('v.subProdMap'))[selectedProduct];
				subProdList.sort(function(a,b) { //Sort by label, alphabetically ASC
					return a.label == b.label ? 0 : +(a.label >
						b.label) || -1;
				});
				component.set('v.subProdList', subProdList);
				component.set('v.disabledSubProd', false);
			}
			else component.set('v.disabledSubProd', true);
		}
	},
	//main search method
	searchKBHelper: function(component, event, helper) {
		component.set('v.paginationList', null);
		var searchText = component.get("v.searchText");
		if (component.get('v.isInitialSearchLoad') || (searchText && searchText.length == 1)) {
			component.set("v.showSpinner", false);
			component.set("v.searchResults", null);
			return;
		}
		//Data categories, language, type
		var selectedProduct = component.get("v.selectedProduct");
		var selectedSubProduct = component.get("v.selectedSubProd");
		var selectedCategory = component.get("v.selectedCategory");
		var lang = component.get("v.selectedLang");
		var type = component.find("selectedArtType").get("v.value");

		if (selectedSubProduct) selectedProduct = selectedSubProduct;

		//capture the searched keywrods
		var allSearchTexts;
		if (searchText && searchText.length > 2 && component.get("v.searchOrTypeahead") == 'search') {
			var allSearchTexts = component.get("v.allSearchTexts");
			allSearchTexts += searchText + ';';
			component.set("v.allSearchTexts", allSearchTexts);
		}

		component.set("v.showSpinner", true);
		var action = component.get("c.search");
		action.setParams({
			keyword: searchText,
			selectedProduct: selectedProduct,
			selectedCategory: selectedCategory,
			lang: lang,
			type: type
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			component.set("v.showSpinner", false);
			if (state === 'SUCCESS') {
				var results = response.getReturnValue();
				component.set("v.searchResults", results);
                
                if (results != null) {				
                    helper.captureKeyword(component, event);
                    console.log('capture keyword called!!');
                    helper.pageSizeChanged(component);
                    console.log('page size chanegd called!!');
                }
			} else {
				console.log('state**', state);
			}
		});
		$A.enqueueAction(action);
	},
	//on click of next button for pagination
	onNext: function(component, event) {
		component.set('v.paginationList', null); //set results to blank
		var sObjectList = component.get("v.searchResults");
        console.log('sObjectList length**', sObjectList.length);
		var end = component.get("v.endPage");
		var start = component.get("v.startPage");
		var pageSize = component.get("v.pageSize");
		var currPage = component.get("v.currentPage");
		var pagList = [];
		var counter = 0;
        console.log('start*end*currPage*pageSize*', start, end, currPage, pageSize);
		for (var i = end + 1; i < (end + pageSize + 1); i++) {
			if (sObjectList.length > i) {
				pagList.push(sObjectList[i]);
			}
			counter++;
		}        
		start = start + counter;
		end = end + counter;
        console.log('start*counter*end*currPage*pageSize*', start, counter, end, currPage, pageSize);
		//setting pagination variables
		component.set("v.startPage", start);
		component.set("v.endPage", end);
		component.set('v.paginationList', pagList);
		component.set('v.currentPage', currPage + 1);
	},
	//on click of previous button for pagination
	onPrevious: function(component, event, helper) {
		component.set('v.paginationList', null); //set results to blank
		var sObjectList = component.get("v.searchResults");
		var end = component.get("v.endPage");
		var start = component.get("v.startPage");
		var pageSize = component.get("v.pageSize");
		var currPage = component.get("v.currentPage");
		var pagList = [];
		var counter = 0;
		for (var i = start - pageSize; i < start; i++) {
			if (i > -1) {
				pagList.push(sObjectList[i]);
				counter++;
			} else {
				start++;
			}
		}
		start = start - counter;
		end = end - counter;
		//setting pagination variables
		component.set("v.startPage", start);
		component.set("v.endPage", end);
		component.set('v.paginationList', pagList);
		if (currPage > 1) {
			component.set('v.currentPage', currPage - 1);
		}
	},
	//on change of page size for pagination
	pageSizeChanged: function(component) {
        console.log('pagesizechanged**');
		component.set('v.paginationList', null); //set results to blank
		var sObjectList = component.get("v.searchResults");
		var pageSize = component.find("pageSize").getElement() ? parseInt(component.find("pageSize").getElement().value) : component.get('v.pageSizeSelect')[0].value;
		component.set("v.pageSize", parseInt(pageSize));
		component.set("v.totalRecords", component.get("v.searchResults").length);
		component.set("v.startPage", 0);
		component.set("v.endPage", pageSize - 1);
		var pgList = [];
		for (var i = 0; i < pageSize; i++) {
			if (sObjectList.length > i) {
				pgList.push(sObjectList[i]);
			}
		}
		component.set('v.paginationList', pgList);
		component.set('v.currentPage', 1);
        console.log('sObjectList.length**', sObjectList.length);
		var totalPage = sObjectList.length%pageSize != 0 ? parseInt(sObjectList.length/pageSize) + 1 : parseInt(sObjectList.length/pageSize);
		console.log('totalPage**', totalPage);
        component.set('v.totalPage', totalPage);
	},
	callServer: function(method, parameter, component) {
		component.set("v.paginationList", null);
		component.set("v.showSpinner", true);
		var action = component.get(method);
		action.setParams({
			parameter: parameter
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.searchResults", response.getReturnValue());
				component.set("v.showSpinner", false);
				this.setPaging(component);
				this.pageSizeChanged(component);
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					}
				} else {
					console.log("Unknown error");
				}
			}
		});
		$A.enqueueAction(action);
	},
	setDataCatPicks: function(allcats, component) {
		var catlist = [];
		var prodlist = [];
		var subprodmap = {};
        //console.log("all categories:");
		//console.log(allcats);
		for (var i = 0; i < allcats.length; i++) {
			var grp = allcats[i];
			if (grp.name == 'IR_Global_Region_Group') continue;

			for (var j = 0; j < grp.topCategories.length; j++) {

				var tcat = grp.topCategories[j];

                var tempTopLevel = {};
                tempTopLevel.label = tcat.label;
                tempTopLevel.value = tcat.name;

                switch (grp.name) {
                    case 'IR_Global_Products_Group':
                        prodlist.push(tempTopLevel);
                        break;
                    case 'IR_Global_Category_Group':
                        break;                        
                    default:
                        break;
                }

                var tempSubProds = [];

                for (var k = 0; k < tcat.childCategories.length; k++) {

                    var ccat = tcat.childCategories[k];

                    var tempSubLevel = {};
                    tempSubLevel.label = ccat.label;
                    tempSubLevel.value = ccat.name;

                    switch (grp.name) {
                        case 'IR_Global_Products_Group':
                            tempSubProds.push(tempSubLevel);
                            break;
                        case 'IR_Global_Category_Group':
                            catlist.push(tempSubLevel);
                            break;                        
                        default:
                            break;
                    }
                }

                subprodmap[tcat.name] = tempSubProds;
			}
		}
		component.set("v.prodList", prodlist);
		component.set("v.categList", catlist);
		component.set("v.subProdMap", JSON.stringify(subprodmap));
	},
	resetSearch: function(component) {
		component.set("v.searchText", "");
		component.set("v.searchResults", null);
		component.set("v.selectedLang", component.get("v.userInfo").LanguageLocaleKey);
		component.set("v.selectedProduct", "");
		component.set("v.selectedCategory", "");
		component.set("v.selectedSubProd", "");
		component.set("v.selectedArtType", component.find("selectedArtType").set("v.value", ""));
	},
	openDetail: function(component, event, helper) {
		var objectName, title;
		var recId = event.currentTarget.id;
		var kaId, kavId, kavURL, lang;
		var paginationList = component.get("v.paginationList");
		for (var cnt in paginationList) {
			var rec = paginationList[cnt];
			if (rec.sobj.Id == recId) {
				objectName = rec.searchType;
				title = rec.sobj.Title;
				if (objectName == 'Knowledge__kav') {
					kaId = rec.sobj.KnowledgeArticleId;
					kavURL = rec.sobj.UrlName;
					lang = rec.sobj.Language;
				}
				break;
			}
		}

		var modalBody;
		var modalFooter;
		$A.createComponents([
				["c:CTS_RecordViewMain", {
					recordId: recId,
					objectName: objectName
				}],
				["c:CTS_RecordViewFooter", {
					recordId: kaId,
					kavId: recId,
					kavURL: kavURL,
					lang: lang,
                    isOpenNewWindowBtn: true
				}]
			],
			function(modalCmps, status, errorMessage) {
				if (status === "SUCCESS") {
					modalBody = modalCmps[0];
					modalFooter = modalCmps[1];
					component.find('overlayLib').showCustomModal({
						header: title,
						body: modalBody,
						footer: objectName == 'Knowledge__kav' ? modalFooter : null,
						showCloseButton: true,
						cssClass: "slds-modal_large",
						closeCallback: function() {
							console.log('modal closed!');
						}
					});
				} else if (status === "ERROR") {
					console.log('ERROR: ', errorMessage);
				}
			}
		)
	},
    captureKeyword: function(component, event) {
        var searchText = component.get("v.searchText");
        var searchResults = component.get("v.searchResults");
        var selectedProduct = component.get("v.selectedProduct");
		var selectedSubProduct = component.get("v.selectedSubProd");
		var selectedCategory = component.get("v.selectedCategory");
		var lang = component.get("v.selectedLang");
		var type = component.find("selectedArtType").get("v.value");
        
        var action = component.get("c.saveSearchKeyword");
		action.setParams({
			term: searchText,
            count: searchResults ? searchResults.length : 0,
            product: selectedProduct,
            subProduct: selectedSubProduct,
            category: selectedCategory,
            lang: lang,
            type: type
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				console.log('Stats Id**', response.getReturnValue());
			} else {
				console.log('state**', state);
			}
		});
		$A.enqueueAction(action);
    }
})