({
    doInit: function(component) {
        
        // Update the count down every 1 second
        var timer = setInterval(function() {
            
            // Get todays date and time
            var now = new Date().getTime();
            var assignedDateTime = component.get("v.simpleRecord.Assigned_Date_Time__c");
            console.log('assignedDateTime 1------>'+assignedDateTime);
            var assignedDate = new Date(component.get("v.simpleRecord.Assigned_Date_Time__c"));
            
            // Find the distance between now and the count down date
            var distance = now - assignedDate;
            var days,hours,minutes,seconds;
            var isOkay = true;
            var isLate = false;
            if(assignedDateTime== null){
                days = 0;
                hours = 0;
                minutes = 0;
                seconds = 0;
            }else{
                // Time calculations for days, hours, minutes and seconds                
                days = Math.floor(distance / (1000 * 60 *60*24));
                hours = Math.floor((distance / (1000*60*60))% 24);
                minutes = Math.floor((distance /(1000*60))%60) ;
                seconds = ((distance/ 1000)%60).toFixed(2); 
                
            }
            
            
            
            if (minutes===5 && seconds ===0) {
                //component.set("v.simpleRecord.Lead_Time_Violation__c", isOkay);
                /*component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
                    
                    // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful 
                    // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                        // handle component related logic in event handler
                        
                    } else if (saveResult.state === "INCOMPLETE") {
                        console.log("User is offline, device doesn't support drafts.");
                        
                    } else if (saveResult.state === "ERROR") {
                        console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
                        
                    } else {
                        console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                    }
                }));*/
            };
            
            if (minutes>4 || (minutes<4 && hours>0)) {
                console.log('minutes-->'+minutes);
                component.set("v.timeOkay", isLate);
            } else {
                console.log('minutes in less than 5-->'+minutes);
                component.set("v.timeOkay", isOkay);
            };
            
            // Display the result in the element with id="demo"
            var timeLeft =  days+"days "+hours + "h " + minutes + "m " + seconds + "s ";
            component.set("v.timeLeft", timeLeft);
            component.set("v.totalTime", minutes);
        }, 1000);
    }
})