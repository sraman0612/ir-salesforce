<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ARC_End_date_Notified_to_Owner</fullName>
        <description>Notify Account and Annual Rate Contract Owner that Contract reached 90 days prior End date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/X90_Days_before_Annual_Rate_Contract_end_date</template>
    </alerts>
</Workflow>
