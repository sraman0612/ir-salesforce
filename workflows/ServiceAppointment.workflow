<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_FSL_Update_Scheduled_End_Date_and_Tim</fullName>
        <field>SchedEndTime</field>
        <formula>DueDate</formula>
        <name>CC FSL Update Scheduled End Date and Tim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_FSL_Update_Scheduled_Start_Date_and_T</fullName>
        <field>SchedStartTime</field>
        <formula>EarliestStartTime</formula>
        <name>CC FSL Update Scheduled Start Date and T</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC FSL Update Scheduled Start and End Date</fullName>
        <actions>
            <name>CC_FSL_Update_Scheduled_End_Date_and_Tim</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CC_FSL_Update_Scheduled_Start_Date_and_T</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() || ISCHANGED(DueDate) || ISCHANGED(EarliestStartTime)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
