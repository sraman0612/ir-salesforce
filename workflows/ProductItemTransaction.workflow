<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>CC_ERP_INVENTORY_TRX</fullName>
        <apiVersion>45.0</apiVersion>
        <description>Used to send inventory transactions (currently transfer and consume on work order) to Club Car ERP (currently MAPICS).</description>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/MapicsInventoryFSLReqABCSImpl/ProxyServices/MapicsInventoryFSLReqABCSImpl_PS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>CC_ERP_Item_Number__c</fields>
        <fields>CC_Inventory_Trx_Type__c</fields>
        <fields>CreatedDate</fields>
        <fields>Destination_Location__c</fields>
        <fields>Destination_Warehouse__c</fields>
        <fields>Id</fields>
        <fields>Quantity</fields>
        <fields>Source_Location__c</fields>
        <fields>Source_Warehouse__c</fields>
        <fields>Work_Order_Number__c</fields>
        <fields>Work_Order__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>benjamin.lorenz@irco.com</integrationUser>
        <name>CC ERP INVENTORY TRX</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CC Inventory Reproces</fullName>
        <actions>
            <name>CC_ERP_INVENTORY_TRX</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ProductItemTransaction.Reprocess__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule is available to enable reprocessing of any inventory transaction from the Salesforce side in the event of failures that exceed the 24hr built in re-processing or external system failures.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC Inventory Transfer or Consume</fullName>
        <actions>
            <name>CC_ERP_INVENTORY_TRX</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3) OR (3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>ProductItemTransaction.TransactionType</field>
            <operation>equals</operation>
            <value>Consumed</value>
        </criteriaItems>
        <criteriaItems>
            <field>ProductItemTransaction.TransactionType</field>
            <operation>equals</operation>
            <value>Transferred</value>
        </criteriaItems>
        <criteriaItems>
            <field>ProductItemTransaction.Quantity</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>ProductItemTransaction.TransactionType</field>
            <operation>equals</operation>
            <value>Adjusted</value>
        </criteriaItems>
        <criteriaItems>
            <field>ProductItemTransaction.CreatedById</field>
            <operation>notEqual</operation>
            <value>Clubcar Integration</value>
        </criteriaItems>
        <description>This rule fires anytime inventory is tranferred or consumed.  This rule only fires on the positive half of a transfers so the resulting OBM being sent does not send twice.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
