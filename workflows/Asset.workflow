<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Asset_Record_Type_Changed</fullName>
        <description>Record type changed from Create to Edit once the record is inserted.</description>
        <field>RecordTypeId</field>
        <lookupValue>Hibon_Asset</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Asset Record Type Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <field>SBU_ID_Workflow__c</field>
        <formula>IF( 
Account.RecordType.DeveloperName = &apos;Thermo_King_Marine&apos;, &apos;TK&apos;, 
IF(Account.RecordType.DeveloperName = &apos;Site_Account_NA_Air&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_AP&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_AP_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_EU&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_LATAM&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_LATAM_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_MEIA&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;GES_Account&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;Hibon_Account&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;OEM_Account&apos;, &apos;CTS&apos;, 
IF(Account.RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;, 
IF(Account.RecordType.DeveloperName = &apos;Thermo_King_Strategic&apos;, &apos;TK&apos;, 
IF(Account.RecordType.DeveloperName = &apos;PT_powertools&apos;, &apos;Power Tools&apos;,
IF( BEGINS(Account.RecordType.DeveloperName, &apos;Club Car&apos;), &apos;Club Car&apos;,
&apos;&apos;))))))))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Asset_Division</fullName>
        <field>Division__c</field>
        <formula>Account.Division__c</formula>
        <name>Update_Asset_Division</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_OBM_DateTime_Asset</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>NOW()</formula>
        <name>Update_Integration_OBM_DateTime_Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status_Asset</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update_Integration_Status_Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>GD_Compressor_Asset_Sync_to_Siebel</fullName>
        <apiVersion>59.0</apiVersion>
        <endpointUrl>https://oci-icosbomwdev.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrD3vSalesforce&amp;password=IRPrtnrD3vSalesforce&amp;</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>GD Compressor Asset Sync to Siebel</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>IR_Asset_OBM_PROD</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://oci-icosbomwdev.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrD3vSalesforce&amp;password=IRPrtnrD3vSalesforce&amp;</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_Asset_OBM_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Add_Division_OnCreate</fullName>
        <actions>
            <name>Update_Asset_Division</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND ( OR( RecordType.DeveloperName == &quot;NA_Air_Create&quot;, RecordType.DeveloperName == &quot;NA_Air_Edit&quot;,RecordType.DeveloperName == &quot;CTS_Global_Asset&quot; ), true )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IR_Asset_OBM_CTS_GLOBAL</fullName>
        <actions>
            <name>Update_Integration_OBM_DateTime_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Asset_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to send Asset Outbound notifications to Siebel via Fusion</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF(AND(ISNEW(), NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, $User.LastName&lt;&gt;&quot;Integration&quot;, $User.LastName&lt;&gt;&quot;Migration&quot;, OR(RecordType.DeveloperName == &quot;CTS_EU_Asset&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect_Asset&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM_Asset&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect_Asset&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA_Asset&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect_Asset&quot;, RecordType.DeveloperName == &quot;CTS_AIRD_Asset&quot;,RecordType.DeveloperName == &quot;CTS_AIRD_Asset_R12&quot;) ),true, IF(AND(NOT(ISNEW()),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, $User.LastName&lt;&gt;&quot;Integration&quot;, $User.LastName&lt;&gt;&quot;Migration&quot;, OR(RecordType.DeveloperName == &quot;CTS_EU_Asset&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect_Asset&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM_Asset&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect_Asset&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA_Asset&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect_Asset&quot;, RecordType.DeveloperName == &quot;CTS_AIRD_Asset&quot;), OR(ISCHANGED(IRITParent_Frame_Type_Name__c),ISCHANGED(IRIT_Change_Coolant_Per_Sample__c),ISCHANGED(IRIT_Frame_Type__c), ISCHANGED(IRIT_Last_Hours_on_Asset__c),ISCHANGED(IRIT_Last_PM_Date__c),ISCHANGED(IRIT_PC_Flag__c), ISCHANGED(IRIT_PC_Quoted__c),ISCHANGED(IRIT_PLC_Flag__c),ISCHANGED(IRIT_PM_Flag__c),ISCHANGED(IRIT_Projected_Overhaul_Date__c), ISCHANGED(InstallDate),ISCHANGED(Manufacturer__c),ISCHANGED(Model_Name__c),ISCHANGED(Operating_Status__c), ISCHANGED(Ownership__c),ISCHANGED(Product_Name__c),ISCHANGED(Product_Line__c),ISCHANGED(SerialNumber), ISCHANGED(Ship_Date__c),ISCHANGED(Start_Date__c),ISCHANGED(Division__c),ISCHANGED(Status), ISCHANGED(AccountId),ISCHANGED(HP__c),ISCHANGED(CTS_Model__c),ISCHANGED(CTS_KW__c),ISCHANGED(CTS_Voltage__c),ISCHANGED(Local_Controller_Type__c),ISCHANGED(CTS_Motor_Manufacturer__c),ISCHANGED(CTS_Motor_kW__c),ISCHANGED(CTS_Design_Pressure_BARG__c),ISCHANGED(CTS_System_Pressure_BARG__c),ISCHANGED(Condensate_Drain_Type__c),ISCHANGED(CTS_Replacement_Airend_Serial_Number__c),ISCHANGED(CTS_Airend_Replaced_at_running_hours__c),ISCHANGED(Load_Status__c),ISCHANGED(Lubricant_Type__c),ISCHANGED(Inlet_Valve_Type__c),ISCHANGED(CTS_Running_Hours_at_Last_Overhaul__c),ISCHANGED(CTS_Last_Motor_Overhaul_Date__c),ISCHANGED(CTS_Competetive_Account__c),ISCHANGED(CTS_Last_Overhaul_Date__c),ISCHANGED(Siebel_ID__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Asset_OBM_PROD</fullName>
        <actions>
            <name>Update_Integration_OBM_DateTime_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Asset_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(ISNEW(), NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, $User.LastName&lt;&gt;&quot;Integration&quot;, $User.LastName&lt;&gt;&quot;Migration&quot;, OR($RecordType.DeveloperName = &quot;NA_Air_Edit&quot;, $RecordType.DeveloperName = &quot;NA_Air_Create&quot;,$RecordType.DeveloperName == &quot;CTS_Direct_Sales_and_Service&quot;,$RecordType.DeveloperName == &quot;CTS_AIRD_Asset&quot;,$RecordType.DeveloperName == &quot;CTS_AIRD_New_Asset&quot;)),true, IF(AND(NOT(ISNEW()),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, $User.LastName&lt;&gt;&quot;Integration&quot;, $User.LastName&lt;&gt;&quot;Migration&quot;, OR($RecordType.DeveloperName = &quot;NA_Air_Edit&quot;, $RecordType.DeveloperName = &quot;NA_Air_Create&quot;,$RecordType.DeveloperName == &quot;CTS_Direct_Sales_and_Service&quot;,$RecordType.DeveloperName == &quot;CTS_AIRD_Asset&quot;,$RecordType.DeveloperName == &quot;CTS_AIRD_New_Asset&quot;), OR(ISCHANGED(IRITParent_Frame_Type_Name__c),ISCHANGED(IRIT_Change_Coolant_Per_Sample__c),ISCHANGED(IRIT_Frame_Type__c), ISCHANGED(IRIT_Last_Hours_on_Asset__c),ISCHANGED(IRIT_Last_PM_Date__c),ISCHANGED(IRIT_PC_Flag__c), ISCHANGED(IRIT_PC_Quoted__c),ISCHANGED(IRIT_PLC_Flag__c),ISCHANGED(IRIT_PM_Flag__c),ISCHANGED(IRIT_Projected_Overhaul_Date__c), ISCHANGED(InstallDate),ISCHANGED(Manufacturer__c),ISCHANGED(Model_Name__c),ISCHANGED(Operating_Status__c), ISCHANGED(Ownership__c),ISCHANGED(Product_Name__c),ISCHANGED(Product_Line__c),ISCHANGED(SerialNumber), ISCHANGED(Ship_Date__c),ISCHANGED(Start_Date__c),ISCHANGED(Division__c),ISCHANGED(Status), ISCHANGED(AccountId),ISCHANGED(HP__c),ISCHANGED(CTS_Model__c),ISCHANGED(CTS_KW__c),ISCHANGED(CTS_Voltage__c),ISCHANGED(Local_Controller_Type__c),ISCHANGED(Condensate_Drain_Type__c),ISCHANGED(Load_Status__c),ISCHANGED(Lubricant_Type__c),ISCHANGED(Inlet_Valve_Type__c),ISCHANGED(Hours_Year__c),ISCHANGED(Set_Pressure_psig__c),ISCHANGED(Design_Pressure_psig__c),ISCHANGED(Service_Strategy__c),ISCHANGED(CTS_Competetive_Account__c),ISCHANGED(CTS_Last_Overhaul_Date__c),ISCHANGED(Siebel_ID__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Asset</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <formula>AND( $Permission.DataMigrationUser == FALSE,   ISNULL(SBU_ID_Workflow__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>