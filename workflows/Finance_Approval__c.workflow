<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_Finance_Approval_Notification</fullName>
        <description>CTS Finance Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Approver_Email_update__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Finance_Approval_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>CTS_Finance_Approver_email_Update</fullName>
        <field>Approver_Email_update__c</field>
        <formula>Approver_email_copy__c</formula>
        <name>CTS Finance Approver email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Finance Approval Notification</fullName>
        <actions>
            <name>CTS_Finance_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
Approved__c  = FALSE,
OR( 
ISNEW(),
ISCHANGED(  Approval_Authority__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Finance approval email copy</fullName>
        <actions>
            <name>CTS_Finance_Approver_email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Finance_Approval__c.Approver_email_copy__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
