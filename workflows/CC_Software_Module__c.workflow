<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_Golf_Genius_Module_Added</fullName>
        <ccEmails>steve.johnson@gpsindustries.com</ccEmails>
        <description>Notification: Golf Genius Module Added</description>
        <protected>false</protected>
        <recipients>
            <recipient>amy.hoellrich@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rob.abrams@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>scott.bradford@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shari.harvey@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car_GPSi/Notification_GG_Module_Added</template>
    </alerts>
    <alerts>
        <fullName>Notification_Mobile_App_Module_Added</fullName>
        <description>Notification: Mobile App Added</description>
        <protected>false</protected>
        <recipients>
            <recipient>amy.hoellrich@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cindy.eley@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car_GPSi/Notification_Mobile_App_Added</template>
    </alerts>
    <rules>
        <fullName>Notification%3A Golf Genius Module Added</fullName>
        <actions>
            <name>Notification_Golf_Genius_Module_Added</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Software_Module__c.Module__c</field>
            <operation>equals</operation>
            <value>Tournaments: GG</value>
        </criteriaItems>
        <description>Triggers when Tournament Manager: GG module added</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification%3A Mobile App Added</fullName>
        <actions>
            <name>Notification_Mobile_App_Module_Added</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>CC_Software_Module__c.Module__c</field>
            <operation>equals</operation>
            <value>Mobile App</value>
        </criteriaItems>
        <description>Triggers when Mobile App module is added</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
