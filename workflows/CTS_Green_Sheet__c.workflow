<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QFR_Creation_Notification</fullName>
        <description>QFR Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>alexandre_jordao@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>leandro.alves@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mario.rodriguez@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/QFR_Creation_Notification</template>
    </alerts>
    <rules>
        <fullName>Notify on QFR creation</fullName>
        <actions>
            <name>QFR_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>BEGINS(Opportunity__r.RecordTypeName__c, &apos;CTS_LATAM&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>