<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IRSPX_Create_Sales_Call_Plan_Name</fullName>
        <field>Name</field>
        <formula>&apos;Sales Call Plan - &apos; &amp;  Account__r.Name &amp; &apos; - &apos; &amp;  TEXT( DATEVALUE(CreatedDate))</formula>
        <name>IRSPX Create Sales Call Plan Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>IRSPX Update Sales Call Plan Name</fullName>
        <actions>
            <name>IRSPX_Create_Sales_Call_Plan_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity_Sales_Call_Plan__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Creates a more user-friendly, searchable plan name for use in global search</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>