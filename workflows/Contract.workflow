<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ARO_Contract_Approved_Notification</fullName>
        <description>ARO Contract Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARO_Contract_Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>ARO_Contract_Expiry_Email_Alert</fullName>
        <description>ARO Contract Expiry Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/ARO_Contract_Expiry_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>ARO_Contract_Rejected_Notification</fullName>
        <description>ARO Contract Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARO_Contract_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Approved_Notification_to_Sales_Rep</fullName>
        <description>Send Service Contract Approved Notification to Sales Rep</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Approved</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Finance</fullName>
        <description>Send Service Contract Rejected Notification from Finance</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_General_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Sales_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_VP_of_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Finance_Sales_Director_Lev</fullName>
        <description>Send Service Contract Rejected Notification from Finance-Sales Director Level</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_General_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Sales_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Finance_VP_Sales_Level</fullName>
        <description>Send Service Contract Rejected Notification from Finance-VP Sales Level</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_General_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Sales_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_VP_of_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Sales_Director</fullName>
        <description>Send Service Contract Rejected Notification from Sales Director</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_General_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Sales_GM</fullName>
        <description>Send Service Contract Rejected Notification from Sales GM</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_Service_Regional_Leader</fullName>
        <description>Send Service Contract Rejected Notification from Service Regional Leader</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Rejected_Notification_from_VP_Sales</fullName>
        <description>Send Service Contract Rejected Notification from VP Sales</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Sales_Rep_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_General_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Regional_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>CC_Service_Sales_Director__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Service_Contract_Signature_Notification</fullName>
        <ccEmails>test@test.com</ccEmails>
        <description>Send Service Contract Signature Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Service_Contract_Signed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Contract_Is_Active_True_Flag</fullName>
        <field>Is_Active__c</field>
        <literalValue>1</literalValue>
        <name>Contract Is Active True Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_Approved_Update</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Contract Status Approved Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_Rejected_Update</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Contract Status Rejected Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Status_Review_Update</fullName>
        <field>Status</field>
        <literalValue>Review</literalValue>
        <name>Contract Status Review Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_by_Executive</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved by Executive</literalValue>
        <name>Set Status to Approved by Executive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_by_Sales_Director</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved by Sales Director</literalValue>
        <name>Set Status to Approved by Sales Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_by_Sales_GM</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved by Sales GM</literalValue>
        <name>Set Status to Approved by Sales GM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_by_Service</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved by Service</literalValue>
        <name>Set Status to Approved by Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_by_VP_Sales</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Approved by VP Sales</literalValue>
        <name>Set Status to Approved by VP Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Draft</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Pending</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_Executive</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by Executive</literalValue>
        <name>Set Status to Rejected by Executive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_Finance</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by Finance</literalValue>
        <name>Set Status to Rejected by Finance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_Sales_Director</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by Sales Director</literalValue>
        <name>Set Status to Rejected by Sales Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_Sales_GM</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by Sales GM</literalValue>
        <name>Set Status to Rejected by Sales GM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_Service</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by Service</literalValue>
        <name>Set Status to Rejected by Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected_by_VP_Sales</fullName>
        <field>CC_Contract_Status__c</field>
        <literalValue>Rejected by VP Sales</literalValue>
        <name>Set Status to Rejected by VP Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CC Service Contract Signed</fullName>
        <actions>
            <name>Send_Service_Contract_Signature_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>Club Car Service Contract</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.CC_Contract_Status__c</field>
            <operation>equals</operation>
            <value>Signed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>