<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ClubCar_Bulletin_Digest_Email</fullName>
        <description>ClubCar Bulletin Digest Email</description>
        <protected>false</protected>
        <recipients>
            <field>CC_Send_Email_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>clubcarbulletins@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/ClubCar_Bulletin_Digest_Email</template>
    </alerts>
    <alerts>
        <fullName>ClubCar_Bulletin_Email_publish_External</fullName>
        <description>ClubCar Bulletin Email publish External</description>
        <protected>false</protected>
        <recipients>
            <field>CC_Send_Email_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>clubcarbulletins@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/ClubCar_Bulletin_Email_publish_External</template>
    </alerts>
    <fieldUpdates>
        <fullName>Content_Publish_Name_update</fullName>
        <field>Name</field>
        <formula>&apos;CPEN&apos;&amp; &apos;-&apos;&amp; Email_autonumber__c</formula>
        <name>Content Publish Name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Bulletin Workflow</fullName>
        <actions>
            <name>ClubCar_Bulletin_Email_publish_External</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Content_Publish_Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Content_Publish_Email__c.CC_Content_Document_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>bulletin</value>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Content_Publish_Email__c.CC_Bulletin_Type__c</field>
            <operation>notEqual</operation>
            <value>Digest</value>
        </criteriaItems>
        <description>Workflow to send emails on Bulletins</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Bulletin Digest Workflow</fullName>
        <actions>
            <name>Content_Publish_Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>CC_Content_Publish_Email__c.CC_Content_Document_Record_Type_Name__c</field>
            <operation>equals</operation>
            <value>bulletin</value>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Content_Publish_Email__c.CC_Bulletin_Type__c</field>
            <operation>equals</operation>
            <value>Digest</value>
        </criteriaItems>
        <description>Workflow to send emails for Bulletin Digests</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ClubCar_Bulletin_Digest_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>CC_Content_Publish_Email__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>