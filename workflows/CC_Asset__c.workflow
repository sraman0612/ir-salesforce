<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Asset_Name_with_Serial_Number</fullName>
        <field>Name</field>
        <formula>IF(
	ISBLANK(Serial__c),
	IF(
		ISBLANK(Product__c),
		Id,
		IF(
			ISBLANK(Model_Year__c),
			Product__r.Name,
			Product__r.Name + &quot;~&quot; + Model_Year__c
		)
	),
	IF(
		ISBLANK(Decal_Number__c),
		Serial__c,
		Serial__c + &quot;~&quot; + Decal_Number__c
	)
)</formula>
        <name>Update Asset Name with Serial Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Asset Created or Updated</fullName>
        <actions>
            <name>Update_Asset_Name_with_Serial_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Asset__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
