<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CTS_Global_Update_Serv_Req_RecType_Dir</fullName>
        <description>CTS Global Update Serv Req RecType Dir</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Service_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Serv Req RecType Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Serv_Req_RecType_Ind</fullName>
        <description>CTS Global Update Serv Req RecType Ind</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Ind_Service_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Serv Req RecType Ind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Global Update Serv Req RecType Dir</fullName>
        <actions>
            <name>CTS_Global_Update_Serv_Req_RecType_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Serv Req RecType Dir</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DIRECT&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update Serv Req RecType Ind</fullName>
        <actions>
            <name>CTS_Global_Update_Serv_Req_RecType_Ind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Serv Req RecType Ind</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DISTRIBUTOR&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
