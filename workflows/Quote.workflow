<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_Account_Name</fullName>
        <description>Updates the Quote&apos;s Billing Account&apos;s Name value to the Account&apos;s Billing Name value for TKM Quote records</description>
        <field>BillingName</field>
        <formula>Opportunity.Account.Name</formula>
        <name>TKM Update Quote Billing Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_City</fullName>
        <description>Updates the Quote&apos;s Billing City value to the Account&apos;s Billing City value for TKM Quote records</description>
        <field>BillingCity</field>
        <formula>Opportunity.Account.BillingCity</formula>
        <name>TKM Update Quote Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_Country</fullName>
        <description>Updates the Quote&apos;s Billing Country value to the Account&apos;s Billing Country value for TKM Quote records</description>
        <field>BillingCountry</field>
        <formula>Opportunity.Account.BillingCountry</formula>
        <name>TKM Update Quote Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_Postal_Code</fullName>
        <description>Updates the Quote&apos;s Billing Postal Code value to the Account&apos;s Billing Postal Code value for TKM Quote records</description>
        <field>BillingPostalCode</field>
        <formula>Opportunity.Account.BillingPostalCode</formula>
        <name>TKM Update Quote Billing Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_State</fullName>
        <description>Updates the Quote&apos;s Billing State value to the Account&apos;s Billing State value for TKM Quote records</description>
        <field>BillingState</field>
        <formula>Opportunity.Account.BillingState</formula>
        <name>TKM Update Quote Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Billing_Street</fullName>
        <description>Updates the Quote&apos;s Billing Street value to the Account&apos;s Billing Street value for TKM Quote records</description>
        <field>BillingStreet</field>
        <formula>Opportunity.Account.BillingStreet</formula>
        <name>TKM Update Quote Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Name</fullName>
        <description>Updates the TKM Quote Name to the associated Opportunity&apos;s Name</description>
        <field>Name</field>
        <formula>Opportunity.Name</formula>
        <name>TKM Update Quote Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_Account_Name</fullName>
        <description>Updates the Quote&apos;s Shipping Account Name value to the Account&apos;s Shipping Name value for TKM Quote records</description>
        <field>ShippingName</field>
        <formula>Opportunity.Account.Name</formula>
        <name>TKM Update Quote Shipping Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_City</fullName>
        <description>Updates the Quote&apos;s Shipping City value to the Account&apos;s Shipping City value for TKM Quote records</description>
        <field>ShippingCity</field>
        <formula>Opportunity.Account.ShippingCity</formula>
        <name>TKM Update Quote Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_Country</fullName>
        <description>Updates the Quote&apos;s Shipping Country value to the Account&apos;s Shipping Country value for TKM Quote records</description>
        <field>ShippingCountry</field>
        <formula>Opportunity.Account.ShippingCountry</formula>
        <name>TKM Update Quote Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_Postal_Code</fullName>
        <description>Updates the Quote&apos;s Shipping Postal Code value to the Account&apos;s Shipping Postal Code value for TKM Quote records</description>
        <field>ShippingPostalCode</field>
        <formula>Opportunity.Account.ShippingPostalCode</formula>
        <name>TKM Update Quote Shipping Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_State</fullName>
        <description>Updates the Quote&apos;s Shipping State value to the Account&apos;s Shipping Stae value for TKM Quote records</description>
        <field>ShippingState</field>
        <formula>Opportunity.Account.ShippingState</formula>
        <name>TKM Update Quote Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TKM_Update_Quote_Shipping_Street</fullName>
        <description>Updates the Quote&apos;s Shipping Street value to the Account&apos;s Shipping Street value for TKM Quote records</description>
        <field>ShippingStreet</field>
        <formula>Opportunity.Account.ShippingStreet</formula>
        <name>TKM Update Quote Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TK_Copy_Contact_Email</fullName>
        <description>TK-Copy Contact Email from Contact record to Quote record</description>
        <field>Email</field>
        <formula>Contact.Email</formula>
        <name>TK-Copy Contact Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TK_Copy_Contact_Phone</fullName>
        <description>TK-Copy Contact Phone from Contact Record to Quote Record</description>
        <field>Phone</field>
        <formula>Contact.Phone</formula>
        <name>TK-Copy Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TK-Copy Oppty Name to Quote Name</fullName>
        <actions>
            <name>TK_Copy_Contact_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TK_Copy_Contact_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Thermo King Marine</value>
        </criteriaItems>
        <description>TK-Copy Contact Phone/Email from Contact Record to Quote Record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TKM Update Quote with Opportunity Billing Data</fullName>
        <actions>
            <name>TKM_Update_Quote_Billing_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Billing_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Thermo King Marine</value>
        </criteriaItems>
        <description>Updates newly created Quote records with billing address information from the associated Opportunity record for Thermo King Marine records.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TKM Update Quote with Opportunity Shipping Data</fullName>
        <actions>
            <name>TKM_Update_Quote_Shipping_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Shipping_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Shipping_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TKM_Update_Quote_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Thermo King Marine</value>
        </criteriaItems>
        <description>Updates newly created Quote records with shipping address information from the associated Opportunity record for Thermo King Marine records.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
