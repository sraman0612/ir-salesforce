<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_CustomerVIEW_Training_Requestor</fullName>
        <description>Email to CustomerVIEW Training Requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Attendee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/CustomerVIEW_Requestor_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Customer_VIEW_Group</fullName>
        <description>Email to Customer VIEW Group</description>
        <protected>false</protected>
        <recipients>
            <recipient>CC_CustomerVIEW_group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/New_CC_Links_Systems_Training_Request_Mailer</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Tech_Warranty_Group</fullName>
        <ccEmails>techtraining@clubcar.com</ccEmails>
        <description>Email to Tech Warranty Group</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/New_Training_Request_Mailer</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Training_Support_Training_Requestor</fullName>
        <description>Email to Training Support Training Requestor</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>CC_Attendee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Training_Support_Training_Request_Submitter_Email</template>
    </alerts>
    <rules>
        <fullName>CustomerVIEW Training Request</fullName>
        <actions>
            <name>Email_to_Customer_VIEW_Group</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name  = &apos;CustomerView&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Training and Support - Training Request</fullName>
        <actions>
            <name>Email_to_Tech_Warranty_Group</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_Training_Support_Training_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name  = &apos;TechWarranty&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
