<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Check_Connectivity_Product</fullName>
        <field>CC_Connectivity_Product__c</field>
        <literalValue>1</literalValue>
        <name>Check Connectivity Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Check_Golf_Product</fullName>
        <field>CC_Golf_Product__c</field>
        <literalValue>1</literalValue>
        <name>Check Golf Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Check_Service_Product</fullName>
        <field>Service_Product__c</field>
        <literalValue>1</literalValue>
        <name>Check Service Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Check_Utility_Product</fullName>
        <field>CC_Utility_Product__c</field>
        <literalValue>1</literalValue>
        <name>Check Utility Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Connectivity_Product_Update</fullName>
        <description>Used to populate the Connectivity Product name selected in Opportunity Product</description>
        <field>CC_Connectivity_Product__c</field>
        <formula>Product2.Name</formula>
        <name>CC Connectivity Product Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Populate_Club_Car_Price_from_price_bo</fullName>
        <field>CC_Club_Car_Price__c</field>
        <formula>PricebookEntry.CC_Club_Car_Price__c</formula>
        <name>CC Populate Club Car Price from price bo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Populate_Product_Line_from_Product</fullName>
        <field>CC_Club_Car_Product_Line__c</field>
        <formula>Product2.Product_Line__c</formula>
        <name>Populate Product Line from Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC Check Connectivity Product</fullName>
        <actions>
            <name>CC_Check_Connectivity_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CC_Connectivity_Product_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR(  Opportunity.RecordType.DeveloperName = &apos;Club_Car&apos;, Opportunity.RecordType.DeveloperName = &apos;CC_Golf_Sales&apos;), OR(   CONTAINS( Product2.M1__c, &quot;Visage&quot;),   CONTAINS( Product2.M2__c, &quot;Visage&quot;),   CONTAINS( Product2.M3__c, &quot;Visage&quot;),   CONTAINS( Product2.P1__c, &quot;Visage&quot;),   CONTAINS( Product2.P2__c, &quot;Visage&quot;),   CONTAINS( Product2.P3__c, &quot;Visage&quot;),   CONTAINS( Product2.R1__c, &quot;Visage&quot;),   CONTAINS( Product2.R2__c, &quot;Visage&quot;),   CONTAINS( Product2.R3__c, &quot;Visage&quot;))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CC Check Golf Product</fullName>
        <actions>
            <name>CC_Check_Golf_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
OR(  
Opportunity.RecordType.DeveloperName = &apos;Club_Car&apos;,  Opportunity.RecordType.DeveloperName = &apos;CC_Golf_Sales&apos;),  
OR(  
CONTAINS( Product2.M1__c, &quot;Golf&quot;),  
CONTAINS( Product2.M2__c, &quot;Golf&quot;),  
CONTAINS( Product2.M3__c, &quot;Golf&quot;),  
CONTAINS( Product2.P1__c, &quot;Golf&quot;),  
CONTAINS( Product2.P2__c, &quot;Golf&quot;),  
CONTAINS( Product2.P3__c, &quot;Golf&quot;),  
CONTAINS( Product2.R1__c, &quot;Golf&quot;),  
CONTAINS( Product2.R2__c, &quot;Golf&quot;),  
CONTAINS( Product2.R3__c, &quot;Golf&quot;))  
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Check Service Product</fullName>
        <actions>
            <name>CC_Check_Service_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
OR( 
Opportunity.RecordType.DeveloperName = &apos;Club_Car&apos;, Opportunity.RecordType.DeveloperName = &apos;CC_Golf_Sales&apos;), 
OR( 
CONTAINS( Product2.M1__c, &quot;Service&quot;),  
CONTAINS( Product2.M2__c, &quot;Service&quot;),  
CONTAINS( Product2.M3__c, &quot;Service&quot;),  
CONTAINS( Product2.P1__c, &quot;Service&quot;),  
CONTAINS( Product2.P2__c, &quot;Service&quot;),  
CONTAINS( Product2.P3__c, &quot;Service&quot;),  
CONTAINS( Product2.R1__c, &quot;Service&quot;),  
CONTAINS( Product2.R2__c, &quot;Service&quot;),  
CONTAINS( Product2.R3__c, &quot;Service&quot;)) 
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Check Utility Product</fullName>
        <actions>
            <name>CC_Check_Utility_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  
OR(  
Opportunity.RecordType.DeveloperName = &apos;Club_Car&apos;,  Opportunity.RecordType.DeveloperName = &apos;CC_Golf_Sales&apos;),  
OR(  
CONTAINS( Product2.M1__c, &quot;Utility&quot;),  
CONTAINS( Product2.M2__c, &quot;Utility&quot;),  
CONTAINS( Product2.M3__c, &quot;Utility&quot;),  
CONTAINS( Product2.P1__c, &quot;Utility&quot;),  
CONTAINS( Product2.P2__c, &quot;Utility&quot;),  
CONTAINS( Product2.P3__c, &quot;Utility&quot;),  
CONTAINS( Product2.R1__c, &quot;Utility&quot;),  
CONTAINS( Product2.R2__c, &quot;Utility&quot;),  
CONTAINS( Product2.R3__c, &quot;Utility&quot;)) 
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Populate Club Car Price from price book entry</fullName>
        <actions>
            <name>CC_Populate_Club_Car_Price_from_price_bo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PricebookEntry.CC_Club_Car_Price__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.CC_Club_Car_Price__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CC Populate Product Line from Product</fullName>
        <actions>
            <name>CC_Populate_Product_Line_from_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
OR( 
Opportunity.RecordType.DeveloperName = &apos;Club_Car&apos;, Opportunity.RecordType.DeveloperName = &apos;CC_Golf_Sales&apos;), NOT(ISBLANK(CC_Product_Line__c)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
