<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_key_for_non_MAPICS_assets</fullName>
        <description>key for non MAPICS assets</description>
        <field>Asset_Group_Key__c</field>
        <formula>TEXT(Acquired_Date__c) &amp;&quot;_&quot;&amp; Account__r.Name &amp;&quot;_&quot;&amp; Product__r.Name</formula>
        <name>Set key for non MAPICS assets</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set key for non MAPICS assets</fullName>
        <actions>
            <name>Set_key_for_non_MAPICS_assets</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CC_Asset_Group__c.Vendor_Formula__c</field>
            <operation>notEqual</operation>
            <value>CLUB CAR</value>
        </criteriaItems>
        <description>sets key for non MAPICS assets [date]_[account]_[product]</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
