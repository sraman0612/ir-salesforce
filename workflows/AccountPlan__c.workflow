<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IRSPX_Account_Plan_Name_Generation</fullName>
        <description>Creates the Account Plan Name in a searchable form that is easier for users to identify with, rather than the autonumber format.</description>
        <field>Name</field>
        <formula>&apos;AP - &apos; &amp;  AccountName__r.Name &amp; &apos; - &apos; &amp;  TEXT( TODAY())</formula>
        <name>IRSPX Account Plan Name Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>IRSPX Account Plan Name Generation</fullName>
        <actions>
            <name>IRSPX_Account_Plan_Name_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Name == &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>