<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_OppInput_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Created</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change OppInput RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Update_Campaign</fullName>
        <field>pt_campaign_desc_sales__c</field>
        <literalValue>CA_000013</literalValue>
        <name>PT Update Campaign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Source_Other</fullName>
        <field>Lead_SourcePL__c</field>
        <literalValue>Other</literalValue>
        <name>Set Lead Source Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>New 5%2EBase</fullName>
        <actions>
            <name>Set_Lead_Source_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Type__c , &quot;5.Base&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PT WF05_Opp Input RT</fullName>
        <actions>
            <name>Change_OppInput_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Migrated from PowerTools Org</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; isnew()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
