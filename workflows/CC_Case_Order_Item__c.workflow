<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Recieved_At_Dock_Date</fullName>
        <field>Received_At_Dock_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Recieved At Dock Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Recieved At Dock</fullName>
        <actions>
            <name>Set_Recieved_At_Dock_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Case_Order_Item__c.Status__c</field>
            <operation>equals</operation>
            <value>Received at Dock</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
