<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IRSPX_Sales_Call_Plan_Send_Manager_Feedback</fullName>
        <description>IRSPX Sales Call Plan Send Manager Feedback</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/IRSPX_Sales_Call_Plan_Manager_Feedback</template>
    </alerts>
    <rules>
        <fullName>IRSPX Sales Call Plan Send Manager Feedback</fullName>
        <actions>
            <name>IRSPX_Sales_Call_Plan_Send_Manager_Feedback</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Call_Plan_Manager_Feedback__c.Sales_Leader_Manager_Feedback__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email notification to the recipient of the feedback from the sales manger.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
