<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_FSL_Product_Transfer_Recieved</fullName>
        <field>IsReceived</field>
        <literalValue>1</literalValue>
        <name>CC FSL Product Transfer Recieved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_FSL_Product_Transfer_Update_Received</fullName>
        <field>QuantityReceived</field>
        <formula>QuantitySent</formula>
        <name>CC FSL Product Transfer Update Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
