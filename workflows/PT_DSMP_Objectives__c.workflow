<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PT_DSMP_FU_DSMPObj_ExtId</fullName>
        <field>Ext_ID__c</field>
        <formula>PT_DSMP__r.PT_DSMP_Year__r.Name &amp;&quot;-&quot;&amp; PT_DSMP__r.Account__r.PT_BO_Customer_Number__c &amp;&quot;-&quot;&amp; TEXT(Objectives_Number__c)</formula>
        <name>PT_DSMP_FU_DSMPObj_ExtId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_DSMP_FU_DSMPObj_Name</fullName>
        <field>Name</field>
        <formula>PT_DSMP__r.PT_DSMP_Year__r.Name &amp;&quot;-&quot;&amp; PT_DSMP__r.Account__r.PT_BO_Customer_Number__c &amp;&quot;-&quot;&amp; TEXT(Objectives_Number__c)</formula>
        <name>PT_DSMP_FU_DSMPObj_Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_DSMP_FU_DSMPObjective_UpdateName</fullName>
        <field>Name</field>
        <formula>Objective__c</formula>
        <name>PT_DSMP_FU_DSMPObjective_UpdateName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Objective_Year</fullName>
        <field>Year__c</field>
        <formula>left(Ext_ID__c,4)</formula>
        <name>PT_Objective_Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PT_DSMP_WF_DSMPObj_ExtId</fullName>
        <actions>
            <name>PT_DSMP_FU_DSMPObj_ExtId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PT_DSMP_WF_DSMPObj_UpdateName</fullName>
        <actions>
            <name>PT_DSMP_FU_DSMPObjective_UpdateName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND(   OR(  ISNEW(),  ISCHANGED(  Objective__c  )  ),   NOT( ISBLANK(Objective__c) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PT_Objectives_Year</fullName>
        <actions>
            <name>PT_Objective_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; isnew()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
