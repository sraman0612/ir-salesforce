<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>COOP_Claim_Approval_Notification_to_Partner</fullName>
        <description>COOP Claim Approval Notification to Partner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/COOP_Claim_Approval_Notification_to_Partner</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Approval_Notification_to_Partner_EMEA</fullName>
        <description>COOP Claim Approval Notification to Partner EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/COOP_Claim_Approval_Notification_to_Partner_EMEA</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Rejection_Notification_to_Partner</fullName>
        <description>COOP Claim Rejection Notification to Partner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/COOP_Claim_Rejection_Notification_to_Partner</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Rejection_Notification_to_Partner_EMEA</fullName>
        <description>COOP Claim Rejection Notification to Partner EMEA</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/COOP_Claim_Rejection_Notification_to_Partner_EMEA</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Submission_To_CC</fullName>
        <ccEmails>EMEAcoop@clubcar.com</ccEmails>
        <description>COOP_Claim_Submission_To_CC</description>
        <protected>false</protected>
        <recipients>
            <recipient>suzanne_mccarthy@eu.irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/COOP_Claim_Submission_to_CC</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Submission_To_CC_NA</fullName>
        <ccEmails>coopclaims@clubcar.com</ccEmails>
        <description>COOP_Claim_Submission_To_CC_NA</description>
        <protected>false</protected>
        <recipients>
            <recipient>trent.bailey@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/COOP_Claim_Submission_to_CC</template>
    </alerts>
    <alerts>
        <fullName>COOP_Claim_Submission_To_Partner</fullName>
        <description>COOP_Claim_Submission_To_Partner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/COOP_Claim_Submission_Partner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Coop_Rejection_Code</fullName>
        <field>Rejection_Code__c</field>
        <literalValue>See Comments</literalValue>
        <name>Coop Rejection Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_CoopClaim</fullName>
        <field>CC_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Reject CoopClaim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>CC_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>CC_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
