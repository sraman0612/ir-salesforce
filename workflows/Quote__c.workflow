<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Delete_Request_Rejected</fullName>
        <description>Delete Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Quote_RecType_Dir</fullName>
        <description>CTS Global Update Quote RecType Dir</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Quote RecType Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Quote_RecType_Ind</fullName>
        <description>CTS Global Update Quote RecType Ind</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Indirect_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Quote RecType Ind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DeleteRequest_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PromotedToOrderSetTrue</fullName>
        <field>Promoted_To_Order__c</field>
        <literalValue>1</literalValue>
        <name>PromotedToOrderSetTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Stage_to_Propose</fullName>
        <field>StageName</field>
        <literalValue>Stage 3. Propose/Quote</literalValue>
        <name>Update Opp Stage to Propose</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_After_Rejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>Updates After Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Global Update Quote RecType Dir</fullName>
        <actions>
            <name>CTS_Global_Update_Quote_RecType_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CTS Global Update Quote RecType Dir</description>
        <failedMigrationToolVersion>252.14.3</failedMigrationToolVersion>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(   Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DIRECT&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update Quote RecType Ind</fullName>
        <actions>
            <name>CTS_Global_Update_Quote_RecType_Ind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CTS Global Update Quote RecType Ind</description>
        <failedMigrationToolVersion>252.14.3</failedMigrationToolVersion>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(   Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DISTRIBUTOR&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Promoted To Order</fullName>
        <actions>
            <name>PromotedToOrderSetTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If(Status = Quote Promoted){
Promoted to Order = true}</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; TEXT( Status__c ) == &apos;Quote Promoted&apos; &amp;&amp; RecordType.Name ==&apos;NA Air&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>