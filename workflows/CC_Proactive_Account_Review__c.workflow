<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_GPSi_Update_Last_PAR_Date</fullName>
        <field>CC_GPSI_Last_PAR__c</field>
        <formula>Date_Performed__c</formula>
        <name>CC GPSi Update Last PAR Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CC_GPSi_Update Account Last PAR</fullName>
        <actions>
            <name>CC_GPSi_Update_Last_PAR_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Proactive_Account_Review__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>12/31/1999 10:00 PM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
