<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PDR_Approved_Date</fullName>
        <field>PDR_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>PDR Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDR_Approved_Status</fullName>
        <field>PDR_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>PDR Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDR_Pending_Approval_Status</fullName>
        <field>PDR_Approval_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>PDR Pending Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDR_Recalled_Status</fullName>
        <field>PDR_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>PDR Recalled Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDR_Rejected_Status</fullName>
        <field>PDR_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PDR Rejected Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PDR_Submitted_Date</fullName>
        <field>PDR_Submitted_Date__c</field>
        <formula>TODAY()</formula>
        <name>PDR Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
