<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Jarrett_to_Violation</fullName>
        <description>Alert Jarrett to Violation</description>
        <protected>false</protected>
        <recipients>
            <recipient>kelly.hill@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mario.coco@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>troy.slack@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/CTS_Product_Support_Case_Violation</template>
    </alerts>
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LGD/LGD_CASE_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>CARE_case_submitted</fullName>
        <description>CARE case submitted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CARE_Case_Submitted</template>
    </alerts>
    <alerts>
        <fullName>CTS_LATAM_Brazil_Case_Closed_Alert</fullName>
        <description>CTS LATAM Brazil Case Closed Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_LATAM_Brazil_Case_Closed_Notification</template>
    </alerts>
    <alerts>
        <fullName>CTS_LATAM_Brazil_Case_Created_Alert</fullName>
        <description>CTS LATAM Brazil Case Created Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_LATAM_Brazil_Case_Created_Notification</template>
    </alerts>
    <alerts>
        <fullName>CTS_LATAM_Expected_Answer_Date_Alert</fullName>
        <description>CTS LATAM Expected Answer Date Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_LATAM_Expected_Answer_Date_Nofiication</template>
    </alerts>
    <alerts>
        <fullName>CTS_NA_North_Central_Parts_Response_Email_Alert</fullName>
        <description>CTS NA North Central Parts Response Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>ncd_parts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Auto_Response</template>
    </alerts>
    <alerts>
        <fullName>CTS_NA_North_Central_Parts_Violation_Email_Alert</fullName>
        <description>CTS NA North Central Parts Violation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Violation_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_NA_South_Central_Parts_Violation_Email_Alert</fullName>
        <description>CTS NA South Central Parts Violation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Violation_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_NA_South_Parts_Violation_Email_Alert</fullName>
        <description>CTS NA South Parts Violation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Violation_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_NWC_Pending_Validation_1st_Alert</fullName>
        <description>CTS NWC Pending Validation 1st Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_NWC_Validation_Pending_Task_Assigned_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_NWC_Pending_Validation_2nd_Alert</fullName>
        <description>CTS NWC Pending Validation 2nd Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_NWC_1st_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CTS_NWC_2nd_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_NWC_Validation_Pending_Task_Assigned_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_NWC_Pending_Validation_3rd_Alert</fullName>
        <description>CTS NWC Pending Validation 3rd Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_NWC_Validation_Pending_Task_Assigned_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_North_Central_Area_Specialists_Violation_Email_Alert</fullName>
        <description>CTS North Central Area Specialists Violation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Violation_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Account_Management_Case_Milestone_Violation</fullName>
        <description>CTS OM Account Management Case Milestone Violation</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.powell@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Account_Mgmt_Case_Milestone_Violation</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Admin_Notification</fullName>
        <description>CTS OM Admin Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.powell@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Admin_Notification</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Escalation_Notification_1st</fullName>
        <description>CTS OM Case Escalation Notification - 1st</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Case_Escalation_Notification_1st</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Escalation_Notification_2nd</fullName>
        <description>CTS OM Case Escalation Notification - 2nd</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Case_Escalation_Notification_2nd</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Escalation_Notification_3rd</fullName>
        <description>CTS OM Case Escalation Notification - 3rd</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Case_Escalation_Notification_3rd</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Escalation_Notification_4th</fullName>
        <description>CTS OM Case Escalation Notification - 4th</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Case_Escalation_Notification_4th</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Resolved_Alert</fullName>
        <description>CTS OM Case Resolved Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_Case_ResolvedCTS_OM_Case_Resolved</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Resolved_Alert_w_Survey</fullName>
        <description>CTS OM Case Resolved Alert w/Survey</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_Case_Resolved_w_Survey</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Survey_Results_Received</fullName>
        <description>CTS OM Case Survey Results Received</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Case_Survey_Results_Received</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_Case_Survey_Results_Received_ZEKS</fullName>
        <description>CTS OM Case Survey Results Received ZEKS</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_Case_Survey_Results_Received_ZEKS</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_Case_Milestone_Expired</fullName>
        <description>CTS OM NWC Case Milestone Expired</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Case_Milestone_Expired</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_Case_Milestone_Expiring_Soon</fullName>
        <description>CTS OM NWC Case Milestone Expiring Soon</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Case_Milestone_Expiring_Soon</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_Case_Pending_Return_Expired</fullName>
        <description>CTS OM NWC Case Pending Return Expired</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Case_Pending_Return_Expired</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_Case_Submitted_for_Approval_Approved</fullName>
        <description>CTS OM NWC Case Submitted for Approval - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Case_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_Case_Submitted_for_Approval_Rejected</fullName>
        <description>CTS OM NWC Case Submitted for Approval - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Case_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_NWC_New_Web_Form_Case_Auto_Response</fullName>
        <description>CTS OM NWC New Web Form Case Auto-Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_OM_NWC_Web_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_New_W2C_Response</fullName>
        <description>CTS OM New Web-to-Case Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_Order_Management/CTS_OM_Web_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_New_W2C_Response_ZEKS</fullName>
        <description>CTS OM New Web-to-Case Response ZEKS</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_Web_Case_Created_ZEKS</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Escalation_Notification_1st</fullName>
        <description>CTS OM ZEKS Case Escalation Notification - 1st</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Escalation_Notification_1st</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Escalation_Notification_2nd</fullName>
        <description>CTS OM ZEKS Case Escalation Notification - 2nd</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Escalation_Notification_2nd</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Escalation_Notification_3rd</fullName>
        <description>CTS OM ZEKS Case Escalation Notification - 3rd</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Escalation_Notification_3rd</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Escalation_Notification_4th</fullName>
        <description>CTS OM ZEKS Case Escalation Notification - 4th</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>CTS_OM_Case_Escalated_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Escalation_Notification_4th</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Resolved_Alert</fullName>
        <description>CTS OM ZEKS Case Resolved Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Resolved</template>
    </alerts>
    <alerts>
        <fullName>CTS_OM_ZEKS_Case_Resolved_Alert_w_Survey</fullName>
        <description>CTS OM ZEKS Case Resolved Alert w/Survey</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_OM_ZEKS_Case_Resolved_w_Survey</template>
    </alerts>
    <alerts>
        <fullName>CTS_RMS_Losant_Event_New_Case_Notice</fullName>
        <ccEmails>benjamin.lorenz@irco.com,suresh.raman@irco.com</ccEmails>
        <description>CTS_RMS_Losant_Event_New_Case_Notice</description>
        <protected>false</protected>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_RMS/CTS_RMS_Losant_Event_New_Case_Notice</template>
    </alerts>
    <alerts>
        <fullName>CTS_South_Area_Specialists_Violation_Email_Alert</fullName>
        <description>CTS South Area Specialists Violation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_North_Central_Parts_Violation_Email</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirectCommunity_Case_Confirmation</fullName>
        <description>CTS TechDirectCommunity Case Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/Community_acknowledgment</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Case_by_Agent_Alert</fullName>
        <description>CTS TechDirect Case by Agent Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_by_Agent</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Assignment_from_OOO</fullName>
        <description>CTS TechDirect Community Case Assignment from OOO</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_Assignment_from_OOO</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Close_Alert</fullName>
        <description>CTS TechDirect Community Case Close Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_Case_Collaborator_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Collaborator_Notification</fullName>
        <description>CTS TechDirect Community Case Collaborator Notification</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_Case_Collaborator_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_Case_Collaborator_Added_Notification</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Comment_Alert</fullName>
        <description>CTS TechDirect Community Case Comment Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_Comment_Added</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Comment_Alert1</fullName>
        <ccEmails>sraman@irco.com</ccEmails>
        <description>CTS TechDirect Community Case Comment Alert</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_Case_Collaborator_1__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_2__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>CTS_Case_Collaborator_3__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_Comment_Added</template>
    </alerts>
    <alerts>
        <fullName>CTS_TechDirect_Community_Case_Create_Alert</fullName>
        <description>CTS TechDirect Community Case Create Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Case_Assignment_from_Community2</template>
    </alerts>
    <alerts>
        <fullName>Case_Closer_Email_Notification</fullName>
        <description>Case Closer Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AB_1598528843212</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_12_Hour_GD_Internal</fullName>
        <description>Case On Hold - 12 Hour GD Internal</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contact_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X12_Hour_Reminder_Internal</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_12_Hour_Supplier</fullName>
        <description>Case On Hold - 12 Hour Supplier</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X12_Hour_Reminder_Supplier</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_2_Days_Customer</fullName>
        <description>Case On Hold - 2 Days Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X2_Day_Reminder_Customer</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_7_Day_Customer</fullName>
        <description>Case On Hold - 7 Day Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X7_Day_Reminder_Customer</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_8_Hour_GD_Internal</fullName>
        <description>Case On Hold - 8 Hour GD Internal</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Contact_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X8_Hour_Reminder_Internal</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Hold_8_Hour_Supplier</fullName>
        <description>Case On Hold - 8 Hour Supplier</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Queue_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Reminder_Templates/X8_Hour_Reminder_Supplier</template>
    </alerts>
    <alerts>
        <fullName>Emergency_Case_Notification</fullName>
        <description>Emergency Case Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>surraman@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Emergency_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>Inactive_Case_Owner_Queue_Assignment_Notification</fullName>
        <description>Inactive Case Owner Queue Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jelena.poddubnaja@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>juraj.stecak@irco.com.lifesciences</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kitty_kam@eu.irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_emailalerts_globalsfa@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Inactive_Case_Owner_Queue_Assignemnt</template>
    </alerts>
    <alerts>
        <fullName>India_CS_Case_Email_Urgent_Priority</fullName>
        <ccEmails>K_Venkatesan@trane.com</ccEmails>
        <ccEmails>Saravana.D@trane.com</ccEmails>
        <ccEmails>N_Sriram@trane.com</ccEmails>
        <ccEmails>API_SVCTechSupport@trane.com</ccEmails>
        <ccEmails>ydli@trane.com</ccEmails>
        <description>India CS Case Email Urgent Priority</description>
        <protected>false</protected>
        <senderAddress>customersupportindia@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/India_CS_Case_Urgent_Priority_Notification_for_TRANE</template>
    </alerts>
    <alerts>
        <fullName>India_CS_Case_Escalation_to_Parts_Team</fullName>
        <ccEmails>deepak_kaushik@trane.com</ccEmails>
        <description>India CS Case Escalation to Parts Team</description>
        <protected>false</protected>
        <senderAddress>customersupportindia@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/India_Case_escalation_notification_Singapore</template>
    </alerts>
    <alerts>
        <fullName>India_CS_Case_Escalation_to_Pre_Sales_Team</fullName>
        <ccEmails>Rajani_Suresh@trane.com</ccEmails>
        <description>India CS Case Escalation to Pre-Sales Team</description>
        <protected>false</protected>
        <senderAddress>customersupportindia@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/India_Case_escalation_notification_Singapore</template>
    </alerts>
    <alerts>
        <fullName>India_CS_Case_Escalation_to_Sourcing_Team</fullName>
        <ccEmails>Avtar.Suri@irco.com</ccEmails>
        <description>India CS Case Escalation to Sourcing Team</description>
        <protected>false</protected>
        <senderAddress>customersupportindia@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/India_Case_escalation_notification_Singapore</template>
    </alerts>
    <alerts>
        <fullName>MR_Auto_Response_Email_Alert</fullName>
        <description>MR Auto Response Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MR_Templates/MR_Auto_Response_Template</template>
    </alerts>
    <alerts>
        <fullName>MR_Claims_Web_To_Case_Response_Email_Alert</fullName>
        <description>MR Claims Web To Case Response Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MR_Templates/MR_Claims_Web_To_Case_Response_Template</template>
    </alerts>
    <alerts>
        <fullName>MR_Email_Alert_for_Expedite_Case_AE_PE_Supervisor</fullName>
        <description>MR Email Alert for Expedite Case AE PE Supervisor</description>
        <protected>false</protected>
        <recipients>
            <recipient>MR_AE_PE_Supervisor</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MR_Templates/MR_Email_Template_for_Expedite_Case_AE_PE_Supervisor</template>
    </alerts>
    <alerts>
        <fullName>MR_Email_Alert_for_Urgent_Case_Internal</fullName>
        <description>MR Email Alert for Urgent Case Internal</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MR_Templates/MR_Email_Template_for_Urgent_Case_Internal</template>
    </alerts>
    <alerts>
        <fullName>New_Enhancement_or_Issue_Case_Created</fullName>
        <description>New Enhancement or Issue Case Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>artur.poiata.ext@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>craig_van_tine@irco.comx</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>darshan.shah.ext@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>erika.hoppanova1@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>harishkumar.kolalajayanna@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jayashree.cr@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laura.jauregui@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mahesh.karadkar.ext@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>marieke_van_der_post@eu.irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>masthan.shaik.ext@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>prashanth_rao1@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sa_deepa1@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>surraman@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Enhancement_or_Issue_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>New_Transportation_Request_Case_Created_Internal</fullName>
        <description>New Transportation Request Case Created Internal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/AB_1598528843212</template>
    </alerts>
    <alerts>
        <fullName>Notify_TR_Requestor_of_Quoted</fullName>
        <description>Notify TR Requestor of Quoted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Notify_TR_Requestor_of_Quoted</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Email</fullName>
        <description>Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>LGD/LGD_CASE_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Case_owner</fullName>
        <description>Send Email to Case owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply1@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_case_enhancement_request_is_done_by</fullName>
        <description>Send Email when case enhancement request is done by sfaemeia</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfaemeia@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Enhancment_Request_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Initial_Action_Item_Email_Include_Email_Body</fullName>
        <ccEmails>aman.ab.kumar@capgemini.com</ccEmails>
        <description>Send Initial Action Item Email - Include Email Body</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Action_Item_Escalations/Action_Item_Task_With_Email_Body</template>
    </alerts>
    <alerts>
        <fullName>Send_Initial_Action_Item_Email_No_Email_Body</fullName>
        <ccEmails>aman.ab.kumar@capgemini.com</ccEmails>
        <description>Send Initial Action Item Email - No Email Body</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Action_Item_Escalations/Action_Item_Task_No_Email_Body</template>
    </alerts>
    <alerts>
        <fullName>Survey</fullName>
        <description>Survey</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Reminder_Templates/Survey_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>AI_Ref_ID_Field_Update</fullName>
        <field>Action_Item_Reference_ID__c</field>
        <formula>IF( NOT(ISBLANK(Email_Ref_ID__c)) ,  Email_Ref_ID__c , &apos;&apos;)</formula>
        <name>AI Ref ID Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Accepted_Date</fullName>
        <field>Accepted_Date__c</field>
        <formula>NOW()</formula>
        <name>Accepted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_Check</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Approved Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Care_Support_Case_Subject_Update</fullName>
        <field>Subject</field>
        <formula>LEFT((Account.Name +&apos;-&apos;+ TEXT(CTS_Care_Request_Type__c)),255)</formula>
        <name>CTS Care Support Case Subject Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_NWC_Set_Validation_Violation_Flag</fullName>
        <description>To fire flow to send alerts</description>
        <field>CTS_NWC_Validation_Violation__c</field>
        <literalValue>1</literalValue>
        <name>CTS NWC Set Validation Violation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_NWC_Validation_1st_Violation</fullName>
        <field>CTS_NWC_Validation_Violation_Level__c</field>
        <literalValue>1st Violation</literalValue>
        <name>CTS NWC Validation 1st Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_NWC_Validation_2nd_Violation</fullName>
        <field>CTS_NWC_Validation_Violation_Level__c</field>
        <literalValue>2nd Violation</literalValue>
        <name>CTS NWC Validation 2nd Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_NWC_Validation_3rd_Violation</fullName>
        <field>CTS_NWC_Validation_Violation_Level__c</field>
        <literalValue>3rd Violation</literalValue>
        <name>CTS NWC Validation 3rd Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_NWC_Validation_Final_Violation</fullName>
        <field>CTS_NWC_Validation_Violation_Level__c</field>
        <literalValue>Final Violation</literalValue>
        <name>CTS NWC Validation Final Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_2nd_Escalation_Flag</fullName>
        <field>CTS_OM_2nd_Escalation__c</field>
        <literalValue>1</literalValue>
        <name>CTS OM 2nd Escalation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_3rd_Escalation_Flag</fullName>
        <field>CTS_OM_3rd_Escalation__c</field>
        <literalValue>1</literalValue>
        <name>CTS OM 3rd Escalation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_4th_Escalation_Flag</fullName>
        <field>CTS_OM_4th_Escalation__c</field>
        <literalValue>1</literalValue>
        <name>CTS OM 4th Escalation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_AM_Set_SStatus_to_Closed</fullName>
        <description>CTS OM AM - Set Status to Closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CTS OM AM - Set Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_AM_Set_Status_to_Closed</fullName>
        <description>Automatically sets Case Status = Closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>CTS OM AM - Set Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Americas_Record_Type_to_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CTS_Non_Warranty_Claims</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS OM Americas - Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Milestone_Violation</fullName>
        <field>CTS_OM_Milestone_Violation__c</field>
        <literalValue>Violation</literalValue>
        <name>CTS OM Milestone Violation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Milestone_Violation_Pending</fullName>
        <field>CTS_OM_Milestone_Violation__c</field>
        <literalValue>Pending Violation</literalValue>
        <name>CTS OM Milestone Violation Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Milestone_Violation_Success</fullName>
        <field>CTS_OM_Milestone_Violation__c</field>
        <literalValue>Success</literalValue>
        <name>CTS OM Milestone Violation Success</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_NWC_Set_Record_Type_to_Closed</fullName>
        <description>Automatically changes the Record Type to CTS NWC Closed</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_Non_Warranty_Claims</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS OM NWC - Set Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_NWC_Set_Unread_Email_False</fullName>
        <field>Unread_Email_box__c</field>
        <literalValue>0</literalValue>
        <name>CTS OM NWC Set Unread Email False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_NWC_Status_Approval_Complete</fullName>
        <field>Status</field>
        <literalValue>Approval Complete</literalValue>
        <name>CTS OM NWC Status Approval Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_NWC_Status_In_Process</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>CTS OM NWC Status In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_NWC_Status_Sent_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Sent for Approval</literalValue>
        <name>CTS OM NWC Status Sent for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_ZEKS_Set_Record_Type_to_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CTS_Non_Warranty_Claims</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS OM ZEKS - Set Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancel_Case</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Case Status = Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Action_Item_Subject_Name_Update</fullName>
        <field>Subject</field>
        <formula>AI_Email_Subject__c</formula>
        <name>Case Action Item Subject - Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Clear_Unread_Action_Item_Flag</fullName>
        <field>Unread_Action_Items__c</field>
        <literalValue>0</literalValue>
        <name>Case - Clear Unread Action Item Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Closed_Field_Update</fullName>
        <field>Case_Close_Date__c</field>
        <formula>NOW()</formula>
        <name>Case Closed Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Do_Not_Send_Acknowledgement_box</fullName>
        <field>Do_Not_Send_New_Case_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Case - Do Not Send Acknowledgement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_On_Hold</fullName>
        <field>Status</field>
        <literalValue>On Hold</literalValue>
        <name>Case Status On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Priority_to_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Change Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_On_Hold_Date_Time</fullName>
        <field>On_Hold_Date_Time__c</field>
        <name>Clear On Hold Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_Case</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalate Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation_Count_Clear_Field</fullName>
        <field>Escalation_Count__c</field>
        <formula>0</formula>
        <name>Escalation Count Clear Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Escalation_Count</fullName>
        <field>Escalation_Count__c</field>
        <formula>(Escalation_Count__c) + 1</formula>
        <name>Increment Escalation Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsEscalatedCheckbox</fullName>
        <field>IsEscalated</field>
        <literalValue>0</literalValue>
        <name>IsEscalatedCheckbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Priority_Update_to_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Priority Update to &apos;High&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_Parent_Case</fullName>
        <field>Re_opened__c</field>
        <literalValue>1</literalValue>
        <name>Reopen Parent Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_Back_to_AI_Field_Update</fullName>
        <field>Send_Back_to_AI__c</field>
        <literalValue>0</literalValue>
        <name>Send Back to AI Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CTS_OM_SLA_Expiration_DateTime</fullName>
        <field>Milestone_Expiration__c</field>
        <formula>NOW()</formula>
        <name>Set CTS OM SLA Expiration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Status_Change_to_Now</fullName>
        <description>Sets the Last Status Change field to NOW() to capture the datetime stamp for the change of status.</description>
        <field>Last_Status_Change__c</field>
        <formula>NOW()</formula>
        <name>Set Last Status Change to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_to_High</fullName>
        <description>Set the priority for any case related to a VIP account to high.</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Set Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_to_Low</fullName>
        <field>Priority</field>
        <literalValue>Low</literalValue>
        <name>Set Priority to Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Changed_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Status Changed to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp_On_Hold_Date_Time</fullName>
        <field>On_Hold_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Timestamp On Hold Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Case_Updated_Received</fullName>
        <field>Case_Update_Received__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Case Updated Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_Initial_Email</fullName>
        <field>AI_Send_Initial_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send Initial Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_Initial_Email_2</fullName>
        <field>AI_Send_Initial_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send Initial Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Unread_Email_Checkbox</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Unread Email Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Do_Not_Send_Case_Notification</fullName>
        <description>This will update the Do Not Send Case Notification Email box to be checked.</description>
        <field>Do_Not_Send_New_Case_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Update Do Not Send Case Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initial_Email_Sent</fullName>
        <description>Updates the &apos;Initial Email Sent&apos; checkbox when an Action Item record is saved for the first time and the criteria to send is met.</description>
        <field>Initial_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update &apos;Initial Email Sent&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initial_Email_Sent_Email_Body</fullName>
        <field>Initial_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update &apos;Initial Email Sent&apos; - Email Body</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unread_Email_to_True</fullName>
        <description>When Community user added the comments on case not owned by him</description>
        <field>Unread_Email_box__c</field>
        <literalValue>1</literalValue>
        <name>Update Unread Email to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_prior_last_modified_date</fullName>
        <field>Prior_User_Date_Time__c</field>
        <formula>LastModifiedDate</formula>
        <name>Update prior last modified date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Approval_Complete</fullName>
        <field>Status</field>
        <literalValue>Approval Complete</literalValue>
        <name>Update status to Approval Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Update status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_Sent_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Update status to Sent for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X7_day_auto_closed_Testing_only</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>7 day auto closed_Testing only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>is_Approval_Inprogress_Final_step</fullName>
        <field>Is_Approval_InProgress__c</field>
        <literalValue>0</literalValue>
        <name>is Approval Inprogress Final step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Accepted Date</fullName>
        <actions>
            <name>Accepted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( Case_Accepted__c , true,  ISBLANK( Accepted_Date__c )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Account Management - 15-day auto-close of resolved cases</fullName>
        <active>true</active>
        <description>Automatically sets the Case status to Closed after 7-days (changed from 15 days) and flips the Record Type to Closed</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (ISPICKVAL(Status, &apos;Resolved&apos;), OR (RecordType.Name ==&apos;IR Comp OM Account Management&apos;,RecordType.Name ==&apos;IR Comp OM Account Management (Problem)&apos;,RecordType.Name ==&apos;IR Comp OM Account Management (Incident)&apos;,RecordType.DeveloperName = &apos;Customer_Service&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CTS_OM_AM_Set_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTS OM Case Survey Results Received</fullName>
        <actions>
            <name>CTS_OM_Case_Survey_Results_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS OM: For any Case Survey feedback received, notify the Case Owner and Manager for anything less than an 8 rating (1-10)</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF(         AND(               OR(                           RecordType.DeveloperName = &apos;CTS_OM_Account_Management&apos;,                   RecordType.DeveloperName = &apos;CTS_OM_Account_Management_Problem&apos;,                   RecordType.DeveloperName = &apos;CTS_OM_Account_Management_Incident&apos;              ),              ISCHANGED(CTS_Case_Survey_Satisfaction_Level__c),              OR(                    ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;1&apos;),                    ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;2&apos;),                    ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;3&apos;),                    ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;4&apos;), ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;5&apos;)             )        ),         TRUE,        FALSE  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Case Survey Results Received ZEKS</fullName>
        <actions>
            <name>CTS_OM_Case_Survey_Results_Received_ZEKS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS OM: For any Case Survey feedback received, notify the Case Owner and Manager for anything less than an 8 rating (1-10) for ZEKS</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF(      AND(              RecordType.DeveloperName = &apos;CTS_OM_ZEKS&apos;,            ISCHANGED(CTS_Case_Survey_Satisfaction_Level__c),            OR(                   ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;1&apos;),                   ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;2&apos;),                   ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;3&apos;),                   ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;4&apos;), ISPICKVAL(CTS_Case_Survey_Satisfaction_Level__c, &apos;5&apos;)            )     ),      TRUE,      FALSE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM NWC - 6 months auto-close of resolved cases</fullName>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IR Comp Non-Warranty Claims</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ETO Support Case</value>
        </criteriaItems>
        <description>Automatically sets the Case status to Closed after 6 Months and flips the Record Type to Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CTS_OM_AM_Set_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTS OM NWC - Clear Unread Email Flag on Case Update</fullName>
        <actions>
            <name>CTS_OM_NWC_Set_Unread_Email_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears the Unread Email field when a CTS Non-Warranty Case is picked up (from New status) or when the Owner updates the case in any way.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF(   AND(     Email_Waiting_Icon__c = TRUE,     OR(       AND(         ISCHANGED(Status),         PRIORVALUE(Status) = &apos;New&apos;       ),       AND(         ISCHANGED(LastModifiedDate),         LastModifiedBy.Id = OwnerId       )     )   ),   TRUE,   FALSE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM NWC Priority Update</fullName>
        <actions>
            <name>Change_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update priority to high if NWC has fedex.com in the web email</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( RecordType.DeveloperName =&apos;CTS_Non_Warranty_Claims&apos;, CONTAINS(  UPPER(SuppliedEmail) , &apos;FEDEX.COM&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Update prior last modified date</fullName>
        <actions>
            <name>Update_prior_last_modified_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update prior last modified date when there are changes on CTS OM Cases</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(OR(            RecordType.DeveloperName = &apos;CTS_OM_Account_Management&apos;,            RecordType.DeveloperName = &apos;Customer_Service&apos;,                  RecordType.DeveloperName = &apos;CTS_OM_Transfers&apos;,                       RecordType.DeveloperName = &apos;CTS_OM_ZEKS&apos;),OR(           CONTAINS(LastModifiedBy.Profile.Name,&apos;CTS OM Administrator&apos;),           CONTAINS(LastModifiedBy.Profile.Name,&apos;CTS OM Specialist&apos;),          CONTAINS(LastModifiedBy.Profile.Name,&apos;Order Management&apos;),
CONTAINS(LastModifiedBy.Profile.Name,&apos;Standard Sales&apos;))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM ZEKS Account Management -7-day auto-close of resolved cases</fullName>
        <active>false</active>
        <description>Automatically sets the Case status to Closed after 7-days (changed from 15 days) and flips the Record Type to Closed for ZEKS</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND ( ISPICKVAL(Status, &apos;Resolved&apos;), RecordType.Name ==&apos;CTS OM ZEKS&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CTS_OM_AM_Set_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTS TechDirect Case Collaborator Added</fullName>
        <actions>
            <name>CTS_TechDirect_Community_Case_Collaborator_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>TechDirect - Case Collaborator Added to a Case</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (   				OR(  RecordType.DeveloperName = &apos;CTS_TechDirect_Ask_a_Question&apos;,   							  RecordType.DeveloperName = &apos;CTS_TechDirect_Issue_Escalation&apos;,   							  RecordType.DeveloperName = &apos;CTS_TechDirect_Start_Up&apos; 				),       OR(  AND(  ISNEW(), 													  OR(     							        NOT ISBLANK(CTS_Case_Collaborator_1__c),                     NOT ISBLANK(CTS_Case_Collaborator_2__c),                    NOT ISBLANK(CTS_Case_Collaborator_3__c) 															) 									),	 									OR(  ISCHANGED(CTS_Case_Collaborator_1__c), 							       ISCHANGED(CTS_Case_Collaborator_2__c), 							       ISCHANGED(CTS_Case_Collaborator_3__c) 									) 				)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Case Comment Added</fullName>
        <actions>
            <name>CTS_TechDirect_Community_Case_Comment_Alert1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Comment added from Community</description>
        <formula>AND($Permission.DataMigrationUser == FALSE ,OR(   RecordType.DeveloperName = &apos;CTS_TechDirect_Ask_a_Question&apos;,  RecordType.DeveloperName = &apos;CTS_TechDirect_Issue_Escalation&apos;, RecordType.DeveloperName = &apos;CTS_TechDirect_Start_Up&apos;,  RecordType.DeveloperName = &apos;CTS_Tech_Direct_Article_Feedback&apos;,  RecordType.DeveloperName = &apos;CTS_Tech_Direct_Site_Feedback&apos;), ISCHANGED( RS_CRS_Most_Recent_Case_Comment__c ), OR(ISPICKVAL( Origin , &apos;Chat&apos;), ISPICKVAL( Origin , &apos;Community&apos;) ), LastModifiedById &lt;&gt; Owner:User.Id)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Case by Agent</fullName>
        <actions>
            <name>CTS_TechDirect_Case_by_Agent_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (OR(RecordType.Name ==&apos;IR Comp TechDirect Ask a Question&apos;,RecordType.Name ==&apos;IR Comp TechDirect Issue Escalation&apos;,RecordType.Name ==&apos;IR Comp Tech Direct Article Feedback&apos;,RecordType.Name ==&apos;IR Comp TechDirect Start Up&apos;,RecordType.Name ==&apos;IR Comp Tech Direct Site Feedback&apos;), CTS_TechDirect_Service_Region__c  &lt;&gt; &apos;EMEIA&apos;, CreatedById  &lt;&gt;  OwnerId , OR(TEXT(Origin) ==&apos;Email&apos;,TEXT(Origin) ==&apos;Phone&apos;,TEXT(Origin) ==&apos;Walk Up&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Community Case</fullName>
        <actions>
            <name>CTS_TechDirect_Community_Case_Create_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  				OR( 								RecordType.DeveloperName = &apos;CTS_TechDirect_Ask_a_Question&apos;,  								RecordType.DeveloperName = &apos;CTS_TechDirect_Issue_Escalation&apos;,  								RecordType.DeveloperName = &apos;CTS_TechDirect_Start_Up&apos;, 								RecordType.DeveloperName = &apos;CTS_Tech_Direct_Article_Feedback&apos;, 								RecordType.DeveloperName = &apos;CTS_Tech_Direct_Site_Feedback&apos; 				),   				(Owner:Queue.DeveloperName &lt;&gt; &apos;CTS_TechDirect_EMEIA_PartsID_Case_Q&apos;),   				(CTS_TechDirect_Service_Region__c &lt;&gt; &apos;North America&apos;),  								ISPICKVAL (Status, &apos;New&apos;),   				OR( 								ISPICKVAL( Origin , &apos;Chat&apos;),  								ISPICKVAL( Origin , &apos;Community&apos;) 				), 				(LastModifiedById &lt;&gt; Owner:User.Id) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Community Case Close</fullName>
        <actions>
            <name>CTS_TechDirect_Community_Case_Close_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (OR(RecordType.Name ==&apos;IR Comp TechDirect Ask a Question&apos;,RecordType.Name ==&apos;IR Comp TechDirect Issue Escalation&apos;,RecordType.Name ==&apos;IR Comp TechDirect Start Up&apos;,RecordType.Name ==&apos;IR Comp Tech Direct Article Feedback&apos;,RecordType.Name ==&apos;IR Comp Tech Direct Site Feedback&apos;),OR(TEXT(Status) ==&apos;Resolved/Monitor&apos;,TEXT(Status) ==&apos;Resolved&apos;,TEXT(Status) ==&apos;Closed&apos;,TEXT(Status) ==&apos;Resolved/QRMS&apos;),OR(TEXT(Origin) ==&apos;Chat&apos;,TEXT(Origin) ==&apos;Community&apos;,TEXT(Origin) ==&apos;Web&apos;,TEXT(Origin) ==&apos;Email&apos;,TEXT(Origin) ==&apos;Phone&apos;,TEXT(Origin) ==&apos;Walk Up&apos;), $Profile.Name   &lt;&gt; &apos;CTS_TechDirect_Global_Administrator&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Update Care Support Case Subject</fullName>
        <actions>
            <name>CTS_Care_Support_Case_Subject_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (RecordType.Name ==&apos;CTS CARE Support&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS_OM_NWC_New_Owner_Status_Change</fullName>
        <actions>
            <name>CTS_OM_NWC_Status_In_Process</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS OM NWC - When owner is changed from NWC Queue, then update status to In Process (Unless also changing)</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND(    RecordType.DeveloperName = &apos;CTS_Non_Warranty_Claims&apos;,    ISCHANGED(OwnerId),    PRIORVALUE(OwnerId) = &apos;00G0c000004ZqiT&apos;,    OwnerId = $User.Id,    NOT(ISCHANGED(Status)),    ISPICKVAL(Status, &apos;New&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS_OM_New_Web_to_Case_Received</fullName>
        <actions>
            <name>CTS_OM_New_W2C_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CTS OM - New Web-to-Case Received</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND(         OR(        RecordType.DeveloperName = &apos;CTS_OM_Americas&apos;     ),        ISPICKVAL(Origin, &apos;Web2Case&apos;),         NOT(        ISNULL(CTS_OM_Account_Organization_Name__c)       ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS_OM_New_Web_to_Case_Received_ZEKS</fullName>
        <actions>
            <name>CTS_OM_New_W2C_Response_ZEKS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS OM ZEKS- New Web-to-Case Received</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(         RecordType.DeveloperName = &apos;CTS_OM_ZEKS&apos;,         ISPICKVAL(Origin, &apos;Web2Case&apos;),         NOT(        ISNULL(CTS_OM_Account_Organization_Name__c)     ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Name Default</fullName>
        <actions>
            <name>Case_Action_Item_Subject_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Action Item &apos;Subject&apos; name to the Case Parent Name + the Contact Name.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; OR(RecordType.Name = &apos;External Action Item&apos;, RecordType.Name = &apos;Internal Action Item&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Uncheck Email Waiting Icon With Open AI</fullName>
        <actions>
            <name>Uncheck_Unread_Email_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the status of an Action Item is &apos;Open&apos;, uncheck the Unread Email checkbox which drives the email icon.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(ISPICKVAL(Status,&apos;Open&apos;), OR(RecordType.Name = &apos;External Action Item&apos;, RecordType.Name = &apos;Internal Action Item&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Assign Priority %27High%27 for %27High Priority Contacts%27</fullName>
        <actions>
            <name>Priority_Update_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Assign the Priority picklist to a value of &apos;High&apos; for Cases that are created for High Priority Contacts.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Contact.High_Priority_Contact__c</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Clear Unread Action Item Flag</fullName>
        <actions>
            <name>Case_Clear_Unread_Action_Item_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; RecordType.DeveloperName == &apos;Internal_Case&apos; &amp;&amp; ISPICKVAL(Status, &apos;Closed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Escalation Count Clear</fullName>
        <actions>
            <name>Escalation_Count_Clear_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever a Case &apos;Status&apos; is changed, this means that any Escalations should be reset; meaning the Escalation Count returns to zero.</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - High Priority</fullName>
        <actions>
            <name>Set_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sub-types that result in a case getting set to high priority</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( TEXT(Sub_Type__c)  == &apos;Warranty Issue&apos;   ||  TEXT(Sub_Type__c)  == &apos;Warranty Order&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Increment Escalation Count</fullName>
        <actions>
            <name>Increment_Escalation_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IsEscalatedCheckbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Each time Case criteria triggers an Escalation, increment the &apos;Escalation Count&apos; field on the Case by one, which will determine the Status color on the Case.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IsEscalated = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Initial Action Email Send - Includes Email Body</fullName>
        <actions>
            <name>Send_Initial_Action_Item_Email_Include_Email_Body</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Initial_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Initial_Email_Sent_Email_Body</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sends an Initial Action email when a record is saved for the first time with the &apos;Send Initial Email&apos; box is checked.  Includes the original email body</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; OR(RecordType.Name == &apos;External Action Item&apos;, RecordType.Name == &apos;Internal Action Item&apos;) &amp;&amp;  AI_Send_Initial_Email__c == TRUE &amp;&amp;  Initial_Email_Sent__c == FALSE &amp;&amp;  AI_Add_Body_of_Original_Email__c  == TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Low Priority</fullName>
        <actions>
            <name>Set_Priority_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sub-types that result in a case getting set to low priority</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND ( TEXT(Sub_Type__c) == &apos;Disputes&apos; || TEXT(Sub_Type__c) == &apos;General Inquiry&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Not On Hold</fullName>
        <actions>
            <name>Clear_On_Hold_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears out the on hold timestamp when a case is moved out of On Hold.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (TEXT(Status) ==&apos;On Hold&apos;, On_Hold_Date_Time__c != null)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Last Status Change DateTime</fullName>
        <actions>
            <name>Set_Last_Status_Change_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Last Status Change DateTime field when a status value is updated.  Initially built for CTS.  Can be extended to other SBU&apos;s and Record Types as necessary.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF( 			    AND( 			           OR(          RecordType.DeveloperName = &quot;CTS_OM_Account_Management&quot;, 											           RecordType.DeveloperName = &quot;CTS_OM_Account_Management_Incident&quot;, 											           RecordType.DeveloperName = &quot;CTS_OM_Account_Management_Problem&quot;, 											           RecordType.DeveloperName = &quot;CTS_OM_Account_Management_Closed&quot;, 											                            RecordType.DeveloperName = &quot;CTS_OM_Transfers&quot; 							       ),  		           ISCHANGED(Status) 			    ), 			    TRUE, 			    FALSE  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Set Ref ID Internal</fullName>
        <actions>
            <name>AI_Ref_ID_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Action Item Reference ID field as a way to send back emails into the Action Item record that created it.</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; OR ( RecordType.DeveloperName == &apos;Internal_Case&apos;,  RecordType.DeveloperName ==  &apos;Action_Item&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - VIP Account</fullName>
        <actions>
            <name>Set_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the priority for any case related to a VIP account to high.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND ( Account.VIP__c == true)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Worked After Reopening</fullName>
        <actions>
            <name>Uncheck_Case_Updated_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Unchecks the Case Updated Received checkbox when a user edits the case.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Case_Update_Received__c = TRUE,  ISCHANGED(LastModifiedDate ),  ISPICKVAL(LastModifiedBy.UserType, &apos;Standard&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Emergency Case Notification</fullName>
        <actions>
            <name>Emergency_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies Enterprise CRM Leadership when an Emergency case has been created for their review</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (OR(RecordType.Name ==&apos;SFA Enhancement/Defect Tracker&apos;,RecordType.Name ==&apos;PA Advisiory&apos;),TEXT(Type)==&apos;Emergency Break/Fix&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Enhancement or Issue Case has been created</fullName>
        <actions>
            <name>New_Enhancement_or_Issue_Case_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to the admin/development group for SFDC when an enhancement or issue case has been created</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; 
AND(RecordType.Name ==&apos;SFA Enhancement/Defect Tracker&apos;,
  NOT(BEGINS(&quot;00G&quot;, OwnerId)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Details_sent_via_SMS_China</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Case-3808</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Case Details sent via SMS - China</subject>
    </tasks>
    <tasks>
        <fullName>Send_Details_Via_SMS_Service_Engineer</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Case-fa2f</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Send Details Via SMS - Service Engineer</subject>
    </tasks>
    <tasks>
        <fullName>Send_Details_via_SMS_Customer_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Case-eb3a</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Send Details via SMS - Customer Contact</subject>
    </tasks>
    <tasks>
        <fullName>Send_Details_via_SMS_Singapore</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Case-71ce</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Send Details via SMS - Singapore</subject>
    </tasks>
    <tasks>
        <fullName>Send_Details_via_SMS_Thailand</fullName>
        <assignedToType>owner</assignedToType>
        <description>SMS-Notification-Case-bf9b</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Send Details via SMS - Thailand</subject>
    </tasks>
    <tasks>
        <fullName>X12_Hour_Reminder_Sent_GD_Internal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>12 Hour Reminder Sent - GD Internal</subject>
    </tasks>
    <tasks>
        <fullName>X12_Hour_Reminder_Sent_Supplier</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>12 Hour Reminder Sent - Supplier</subject>
    </tasks>
    <tasks>
        <fullName>X2_Day_Reminder_Sent_Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>2 Day Reminder Sent - Customer</subject>
    </tasks>
    <tasks>
        <fullName>X7_Day_Reminder_Sent_Customer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>7 Day Reminder Sent - Customer</subject>
    </tasks>
    <tasks>
        <fullName>X8_Hour_Reminder_Sent_GD_Internal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>8 Hour Reminder Sent - GD Internal</subject>
    </tasks>
    <tasks>
        <fullName>X8_Hour_Reminder_Sent_Supplier</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>8 Hour Reminder Sent - Supplier</subject>
    </tasks>
</Workflow>