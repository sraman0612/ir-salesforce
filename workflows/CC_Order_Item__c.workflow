<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Dealer_Invoice_Product_Validation</fullName>
        <field>CC_Product_Code__c</field>
        <formula>CC_Product_Code_Populate_hidden_Field__c</formula>
        <name>Dealer Invoice Product Validation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Order_Item_Key</fullName>
        <field>Order_Item_Key__c</field>
        <formula>Order__r.Name + &quot;_&quot; + Source_line_item_sequence__c +  Order_Item_Key_Test_Helper__c</formula>
        <name>Create Order Item Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Price</fullName>
        <field>CC_TotalPrice__c</field>
        <formula>CC_Quantity__c  *  CC_UnitPrice__c</formula>
        <name>Set Total Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Calculate Credit%2FRe-bill Total Amount</fullName>
        <actions>
            <name>Set_Total_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	OR( 		RecordType.DeveloperName =&quot;Credit_Re_Bill&quot;, 		RecordType.DeveloperName =&quot;CC_Dealer_Invoicing&quot; 	), 	OR( 		ISNEW(), 		OR( 			ISCHANGED(CC_Quantity__c), 			ISCHANGED(CC_UnitPrice__c) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Order Item Key</fullName>
        <actions>
            <name>Create_Order_Item_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Order_Item__c.Order_Item_Key__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This rule fires when Order Items are created without an Order Item Key</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Dealer Invoice Product Validation</fullName>
        <actions>
            <name>CC_Dealer_Invoice_Product_Validation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Order_Item__c.CC_Product_Code_Populate_hidden_Field__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Order_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealer Invoicing</value>
        </criteriaItems>
        <description>Used for clubcar dealer invoice product validation (if a product is attached.)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Product Code for Dealer Invoice and Credit%2FRe-bill</fullName>
        <actions>
            <name>CC_Dealer_Invoice_Product_Validation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Order_Item__c.CC_Product_Code_Populate_hidden_Field__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Order_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealer Invoicing,Credit/Re-Bill</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
