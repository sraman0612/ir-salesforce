<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CC_Warranty_Labor_Rate_Final_Action_Notice</fullName>
        <description>CC Warranty Labor Rate Final Action Notice</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car/Warranty_Labor_Rates_Approved_Rejected</template>
    </alerts>
</Workflow>
