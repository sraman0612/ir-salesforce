<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IRSPX_Opportunity_Win_Plan_Send_Manager_Feedback</fullName>
        <description>IRSPX Opportunity Win Plan Send Manager Feedback</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/IRSPX_Opportunity_Win_Plan_Manager_Feedback</template>
    </alerts>
    <fieldUpdates>
        <fullName>Migrate_Win_Plan_Creator_s_Email</fullName>
        <description>Migrate Win Plan Creator&apos;s Email to the Sales Manager Feedback record</description>
        <field>IRSPX_RS_Win_Plan_Creator_s_Email__c</field>
        <formula>Opportunity_Win_Plan__r.IRSPX_RS_Creator_s_Email__c</formula>
        <name>Migrate Win Plan Creator&apos;s Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>IRSPX Opportunity Win Plan Send Manager Feedback</fullName>
        <actions>
            <name>IRSPX_Opportunity_Win_Plan_Send_Manager_Feedback</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow sends an email to the feedback recipient displaying their manager&apos;s feedback.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Sales_Leader_Manager_Feedback__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
