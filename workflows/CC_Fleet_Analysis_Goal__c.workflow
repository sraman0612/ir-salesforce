<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Fleet_Analysis_Goal_Name</fullName>
        <field>Name</field>
        <formula>&quot;Fleet Analysis Goal::&quot; +  TEXT(Year__c) + &quot;::&quot; +  Sales_Rep__r.Name</formula>
        <name>Set Fleet Analysis Goal Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fleet Analysis Goal Created</fullName>
        <actions>
            <name>Set_Fleet_Analysis_Goal_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Fleet_Analysis_Goal__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
