<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Mark_completion_timestamp_on_status_A</fullName>
        <field>Fleet_Analysis_Completion_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>CC Mark completion timestamp on status A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Fleet_Analysis_Name</fullName>
        <field>Name</field>
        <formula>IF(
	ISPICKVAL( Status__c , &quot;Analysis Completed&quot;),
	IF(
		ISCHANGED(Status__c),
		&quot;FA::&quot; &amp;  Account__r.Name + &quot;::&quot; &amp; TEXT(TODAY()),
		&quot;FA::&quot; &amp;  Account__r.Name + &quot;::&quot; &amp; TEXT(DATEVALUE(Fleet_Analysis_Completion_Timestamp__c))
	),
	&quot;FA::&quot; &amp;  Account__r.Name
)</formula>
        <name>Set Fleet Analysis Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC Mark completion timestamp on status Analysis Completed</fullName>
        <actions>
            <name>CC_Mark_completion_timestamp_on_status_A</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CC_Fleet_Analysis__c.Status__c</field>
            <operation>equals</operation>
            <value>Analysis Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fleet Analysis Created</fullName>
        <actions>
            <name>Set_Fleet_Analysis_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Fleet_Analysis__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule fires when a Fleet Analysis is created.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
