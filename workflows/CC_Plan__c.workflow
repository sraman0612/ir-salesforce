<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Set_Product_Line_Changed_Flag</fullName>
        <field>Product_Line_Changed__c</field>
        <literalValue>1</literalValue>
        <name>CC Set Product Line Changed Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Product Line Changed</fullName>
        <actions>
            <name>CC_Set_Product_Line_Changed_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Leave this workflow on when bulk Upserting records.</description>
        <formula>AND( NOT(IsNew()), ISCHANGED(Product_Line_Lookup__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
