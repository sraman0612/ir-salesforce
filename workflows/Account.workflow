<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <alerts>
        <fullName>Account_Delete_Approved</fullName>
        <description>Account:  Delete Approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Account_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Account_Delete_Denied</fullName>
        <description>Account:  Delete Denied</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Approval_Denied</template>
    </alerts>
    <alerts>
        <fullName>Account_Delete_Request</fullName>
        <description>Account:  Delete Request</description>
        <protected>false</protected>
        <recipients>
            <field>Delete_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Account_Record_Request</template>
    </alerts>
    <alerts>
        <fullName>Delete_Account_Request_Reject</fullName>
        <description>Delete Account Request Reject</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Approved_Account</fullName>
        <description>Email Alert For Approved Dealer Account</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Request_for_Dealer_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Rejected_Account</fullName>
        <description>Email Alert For Rejected Account</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Request_for_Dealer_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Submission</fullName>
        <description>Request for Dealer approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>surraman@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Account_Record_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Shipping_County_UPPER</fullName>
        <description>Need to ensure the Account&apos;s County field is an uppercase value for integration to Siebel. In Siebel the County field value must be uppercase for account assignment rules to function properly.</description>
        <field>County__c</field>
        <formula>UPPER( County__c )</formula>
        <name>Account Shipping County UPPER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ApprovalRejection</fullName>
        <field>Dealer__c</field>
        <literalValue>0</literalValue>
        <name>ApprovalRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ApprovedDate</fullName>
        <field>IRIT_Dealer_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>ApprovedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Candian_Province_updates_for_Siebel</fullName>
        <field>ShippingState</field>
        <formula>CASE( ShippingState , 
&apos;AB&apos;, &apos;Alberta&apos;,
&apos;BC&apos;, &apos;British Columbia&apos;,
&apos;MB&apos;, &apos;Manitoba&apos;,
&apos;NB&apos;, &apos;New Brunswick&apos;,
&apos;NL&apos;, &apos;Newfoundland and Labrador&apos;,
&apos;NS&apos;, &apos;Nova Scotia&apos;,
&apos;ON&apos;, &apos;Ontario&apos;,
&apos;PE&apos;, &apos;Prince Edward Island&apos;,
&apos;QC&apos;, &apos;Quebec&apos;,
&apos;SK&apos;, &apos;Saskatchewan&apos;,
&apos;NT&apos;, &apos;Northwest Territories&apos;,
&apos;NU&apos;, &apos;Nunavut&apos;,
&apos;YT&apos;, &apos;Yukon&apos;,
&apos;&apos;)</formula>
        <name>CTS Candian Province updates for Siebel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Populate_Seibel_Created_Date</fullName>
        <field>Seibel_Created_Date__c</field>
        <formula>TODAY()</formula>
        <name>CTS Populate Seibel Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Set_Channel</fullName>
        <field>CTS_Channel__c</field>
        <literalValue>DISTRIBUTOR</literalValue>
        <name>CTS Set Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Set_PriorValue_Type_Field</fullName>
        <field>CTS_PriorValue_Type__c</field>
        <formula>TEXT(Type)</formula>
        <name>CTS Set PriorValue Type Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Techdirect_Data_com_Clean_Status_upd</fullName>
        <description>Updates CleanStatus to Skipped for CTS Techdirect accounts when the &quot;Data.com Does Not Auto-Update&quot; checkbox value equals true so that the Data.com clean process will skip the record.</description>
        <field xsi:nil="true" />
        <literalValue>Skipped</literalValue>
        <name>CTS Techdirect Data_com_Clean_Status_upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>CTS_Global_Shipping_City__c</formula>
        <name>CTS Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Shipping_Country</fullName>
        <description>Updates standard Shipping Country from custom CTS_Country when record is created</description>
        <field>ShippingCountry</field>
        <formula>TEXT( Country__c )</formula>
        <name>CTS Update Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Shipping_Postal_Code</fullName>
        <field>ShippingPostalCode</field>
        <formula>CTS_Global_Shipping_Postal_Code__c</formula>
        <name>CTS Update Shipping Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Shipping_State</fullName>
        <field>ShippingState</field>
        <formula>CTS_Global_Shipping_State_Province__c</formula>
        <name>CTS Update Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>CTS_Global_Shipping_Address_1__c &amp; BR() &amp; CTS_Global_Shipping_Address_2__c</formula>
        <name>CTS Update Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CleanstatusUpdate</fullName>
        <description>Update the field to Skipped if the Record type is RSHVAC</description>
        <field xsi:nil="true" />
        <literalValue>Skipped</literalValue>
        <name>CleanstatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DcomDNAutoUpdate</fullName>
        <description>Data_com_Does_Not_Auto_Update__c field update</description>
        <field>Data_com_Does_Not_Auto_Update__c</field>
        <literalValue>1</literalValue>
        <name>DcomDNAutoUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DealerFields_Update</fullName>
        <field>IRIT_Dealer_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>DealerFieldsUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dealer_Field_Update</fullName>
        <field>Dealer__c</field>
        <literalValue>1</literalValue>
        <name>Dealer_Field_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dealer_Update</fullName>
        <field>Dealer__c</field>
        <literalValue>0</literalValue>
        <name>DealerUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_requestApproved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Delete requestApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IR_Account_IntStatus</fullName>
        <description>InTransit</description>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>IR_Account_IntStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IR_Account_OBMDate</fullName>
        <description>update integration obm datetime</description>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>IR_Account_OBMDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MarkDealerFalse</fullName>
        <field>Dealer__c</field>
        <literalValue>0</literalValue>
        <name>MarkDealerFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <field>SBU_ID_Workflow_Account__c</field>
        <formula>IF( 
RecordType.DeveloperName = &apos;Thermo_King_Marine&apos;, &apos;TK&apos;, 
IF(RecordType.DeveloperName = &apos;CHVAC_China&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;CHVAC_Singapore&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;CHVAC_Thailand&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;India_CS_Account&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;CTS_Global_Distributor&apos;, &apos;CTS&apos;,   
IF(RecordType.DeveloperName = &apos;Site_Account_NA_Air&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_AP&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_AP_INDIRECT&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_New_AP_Account&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_EU&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_LATAM&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_LATAM_INDIRECT&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_New_LATAM_Account&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_MEIA&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_TechDirect_Account&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;GES_Account&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;Hibon_Account&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;OEM_Account&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;,
IF(RecordType.DeveloperName = &apos;RS_CRS_Home_Owner&apos;, &apos;RHVAC&apos;,
IF(RecordType.DeveloperName = &apos;RS_CRS_IWD_Dealer&apos;, &apos;RHVAC&apos;,
IF(RecordType.DeveloperName = &apos;Thermo_King_Strategic&apos;, &apos;TK&apos;, 
IF(RecordType.DeveloperName = &apos;PT_powertools&apos;, &apos;Power Tools&apos;,
IF( LEFT(RecordType.DeveloperName,4) = &apos;Club&apos;, &apos;Club Car&apos;,
&apos;&apos;))))))))))))))))))))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Null_ErrorMessage</fullName>
        <field>Integration_Error_Message__c</field>
        <name>Null_ErrorMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Integration_Error_Message</fullName>
        <field>Integration_Error_Message__c</field>
        <name>Reset Integration Error Message</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Skipped_Clean_Status</fullName>
        <description>Update Data.com Clean Status to skipped.</description>
        <field xsi:nil="true" />
        <literalValue>Skipped</literalValue>
        <name>Set Skipped Clean Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Account_Plan_Created_Date_field</fullName>
        <description>Updates the Account Plan Created Date field to the current date when the Account Plan Exists field is set to &apos;True&apos;.</description>
        <field>AccountPlanCreatedDate__c</field>
        <formula>TODAY()</formula>
        <name>Set the Account Plan Created Date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Skip_Data_com_Clean</fullName>
        <description>Set the Data.com clean skip checkbox to true.</description>
        <field>Data_com_Does_Not_Auto_Update__c</field>
        <literalValue>1</literalValue>
        <name>Skip Data.com Clean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateBillingCountry</fullName>
        <field>BillingCountry</field>
        <formula>&quot;USA&quot;</formula>
        <name>UpdateBillingCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCountryShipping</fullName>
        <field>ShippingCountry</field>
        <formula>&quot;USA&quot;</formula>
        <name>UpdateCountryShipping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDealerApprovedBy</fullName>
        <field>IRIT_Dealer_Approved_By_text__c</field>
        <formula>$User.FirstName + &quot;&quot; + $User.LastName</formula>
        <name>UpdateDealerApprovedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Billing_Address</fullName>
        <field>BillingCity</field>
        <formula>ShippingCity</formula>
        <name>Update Account Billing Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>Update Account Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>Update Account Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Shipping_Postal_Code</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Update Account Shipping Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Shipping_State</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Update Account Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Update Account Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Alias_Name</fullName>
        <field>Alias_Name__c</field>
        <formula>Name</formula>
        <name>Update Alias Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Date_Dealer</fullName>
        <field>IRIT_Dealer_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Update Approved Date (Dealer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country</fullName>
        <field>BillingCountry</field>
        <formula>ShippingCountry</formula>
        <name>Update Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_State</fullName>
        <field>BillingState</field>
        <formula>ShippingState</formula>
        <name>Update Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Street</fullName>
        <field>BillingStreet</field>
        <formula>ShippingStreet</formula>
        <name>Update Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Zip</fullName>
        <field>BillingPostalCode</field>
        <formula>ShippingPostalCode</formula>
        <name>Update Billing Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Canada_County_to_Null</fullName>
        <field>County__c</field>
        <name>Update Canada County to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Approved_By</fullName>
        <field>IRIT_Dealer_Approved_By_text__c</field>
        <formula>$User.Id</formula>
        <name>Update Dealer Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Approved_Date</fullName>
        <field>IRIT_Dealer_Approved_Date__c</field>
        <formula>Today()</formula>
        <name>Update Dealer Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Approver</fullName>
        <field>IRIT_Dealer_Approved_By_text__c</field>
        <formula>$User.Id</formula>
        <name>Update Dealer Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Checkbox</fullName>
        <field>Dealer__c</field>
        <literalValue>0</literalValue>
        <name>Update_Dealer_Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Integration_DateTime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update Dealer Integration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dealer_Integration_Status</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update Dealer Integration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update Integration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime_Approval</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update_Integration_DateTime_Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime_Rejection</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update_Integration_DateTime_Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update Integration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status_Approval</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update_Integration_Status_Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status_Rejection</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update_Integration_Status_Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>Update Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_State</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Update Shipping State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Update Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Zip</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Update Shipping Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Siebel_Ship_To_ID</fullName>
        <field>Siebel_Ship_To_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Update Siebel Ship To ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>GD_Compressor_Account_Sync_to_Siebel</fullName>
        <apiVersion>59.0</apiVersion>
        <endpointUrl>https://oci-icosbomwdev.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrD3vSalesforce&amp;password=IRPrtnrD3vSalesforce&amp;</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>GD Compressor Account Sync to Siebel</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>IR_Account_OBM_PROD</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://oci-icosbomwdev.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrD3vSalesforce&amp;password=IRPrtnrD3vSalesforce&amp;</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_Account_OBM_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account_Null_ErrorMessage</fullName>
        <actions>
            <name>Null_ErrorMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Permission.DataMigrationUser == FALSE,ISPICKVAL(Integration_Status__c, &apos;Success&apos;), RecordType.DeveloperName = &apos;Parent_Account&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Distributor End User Channel</fullName>
        <actions>
            <name>CTS_Set_Channel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Permission.DataMigrationUser == FALSE, RecordType.DeveloperName = &apos;CTS_MEIA_Distributor_End_User&apos;, RecordType.DeveloperName = &apos;CTS_EU_Distributor_End_User&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Populate Seibel Created Date</fullName>
        <actions>
            <name>CTS_Populate_Seibel_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For any Accounts that do not have a value in the field &quot;Seibel Created Date,&quot; populates TODAY() on creation</description>
        <formula>AND($Permission.DataMigrationUser == FALSE,  ISNULL( Seibel_Created_Date__c ), RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Populate Shipping Address on New Accounts</fullName>
        <actions>
            <name>CTS_Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Update_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Update_Shipping_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Update_Shipping_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Update_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the standard Shipping Address fields from custom Shipping Address fields on CTS Global new Account create</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( OR(BEGINS(RecordType.DeveloperName,&quot;CTS_New&quot;)), ($RecordType.DeveloperName &lt;&gt; &quot;CTS_TechDirect_Account&quot; ), WasConverted__c = FALSE )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Track PriorValue Type</fullName>
        <actions>
            <name>CTS_Set_PriorValue_Type_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the CTS PriorValue Type field used for the &apos;Ship To&apos; Accounts field validation.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( OR(  ISCHANGED(Type),  ISNEW()),  OR(  RecordType.DeveloperName == &quot;CTS_EU&quot;,  RecordType.DeveloperName == &quot;CTS_EU_Indirect&quot;,  RecordType.DeveloperName == &quot;CTS_MEIA&quot;,  RecordType.DeveloperName == &quot;CTS_MEIA_Indirect&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Account_Create_Workflow</fullName>
        <actions>
            <name>Update_Alias_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Siebel_Ship_To_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update field values on account creation</description>
        <formula>AND($Permission.DataMigrationUser == FALSE,  !CONTAINS($User.LastName, &apos;Integration&apos;), OR(RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos;, RecordType.DeveloperName = &apos;Parent_Account&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IR_Account_OBM_CTS_GLOBAL</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Account_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Deactivated after merging LATAM &amp; AIRD recordtypes into the NA WF</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(OR(OR(RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, RecordType.DeveloperName = &apos;CTS_EU&apos;,RecordType.DeveloperName = &apos;CTS_MEIA&apos;,RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;), AND(ShippingCountry = &apos;Chile&apos;)), NOT(ISBLANK(ShippingStreet)), AND($User.Alias != &apos;autocln&apos;, $User.Alias != &apos;sjobs&apos; , $User.Alias != &apos;sfdcjobs&apos;), NOT(Contains(OwnerId, &apos;Integration&apos;)), NOT(Contains(OwnerId, &apos;Migration&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Account_OBM_PROD</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Account_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(ISNEW(),NOT($Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT(Is_Approved__c ), ShippingStreet &lt;&gt; &quot;&quot;, Dealer__c = false, $User.Alias &lt;&gt; &quot;autocln&quot;, $User.Alias &lt;&gt; &quot;sjobs&quot;,Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, OR( RecordType.DeveloperName == &quot;AIRD_Accounts&quot;, RecordType.DeveloperName == &quot;Parent_Account&quot;, RecordType.DeveloperName == &quot;Site_Account_NA_Air&quot;,RecordType.DeveloperName == &quot;CTS_Direct_Sales_and_Service&quot;,RecordType.DeveloperName == &quot;CTS_New_NA_Account&quot;,RecordType.DeveloperName == &quot;CTS_AIRD_Distributor_End_User&quot;,RecordType.DeveloperName == &quot;CTS_New_AIRD_Account&quot;)),true, IF(AND(NOT(ISNEW()),NOT($Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT(Is_Approved__c ), ShippingStreet &lt;&gt; &quot;&quot;, Dealer__c = false, $User.Alias &lt;&gt; &quot;autocln&quot;, $User.Alias &lt;&gt; &quot;sjobs&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, OR( RecordType.DeveloperName == &quot;CTS_AIRD_Distributor_End_User&quot;, RecordType.DeveloperName == &quot;Parent_Account&quot;, RecordType.DeveloperName == &quot;Site_Account_NA_Air&quot;,RecordType.DeveloperName == &quot;CTS_Direct_Sales_and_Service&quot;), OR(ISCHANGED(Name),ISCHANGED(Alias_Name__c),ISCHANGED(ShippingStreet),ISCHANGED(Address_2__c),ISCHANGED(Address3__c), ISCHANGED(ShippingCity),ISCHANGED(ShippingPostalCode),ISCHANGED(Strategic_Account_Name__c),ISCHANGED(Strategic_Account_Contact__c), ISCHANGED(SicDesc),ISCHANGED(IRIT_Customer_Number__c),ISCHANGED(Siebel_Ship_To_Id__c),ISCHANGED(ShippingState),ISCHANGED(ShippingCountry), ISCHANGED(IRIT_Contract_Customer__c),ISCHANGED(County__c),ISCHANGED(Oracle_ID__c),ISCHANGED(OwnerId),ISCHANGED(Phone), ISCHANGED(AnnualRevenue),ISCHANGED(NumberOfEmployees),ISCHANGED(Currency__c),ISCHANGED(Fax),ISCHANGED(Website),ISCHANGED(Type), ISCHANGED(Status__c),ISCHANGED(District_Territory__c),ISCHANGED(Region__c),ISCHANGED(Organization__c), ISCHANGED(Industry),ISCHANGED(IRIT_Dealer_Approved_Date__c),ISCHANGED(Dealer__c),ISCHANGED(IRIT_Parent_HQ_DUNS_Name__c), ISCHANGED(Domestic_Ultimate_DUNS__c),ISCHANGED(IRIT_Domestic_Ultimate_DUNS_Name__c),ISCHANGED(Global_Ultimate_DUNS__c), ISCHANGED(Siebel_ID__c),ISCHANGED(New_Customer__c),ISCHANGED(Integration_Status__c), ISCHANGED(Integration_Error_Message__c),ISCHANGED(Integration_OBM_DateTime__c),ISCHANGED(Siebel_Address_Id__c),ISCHANGED(Managed_Account__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Dealer_OBM_PROD</fullName>
        <actions>
            <name>Update_Dealer_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Dealer_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Account_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(ISNEW(),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.Alias &lt;&gt; &quot;autocln&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, ShippingStreet &lt;&gt; &quot;&quot;, Dealer__c = true, NOT( ISBLANK( IRIT_Dealer_Approved_Date__c ) ), $RecordType.DeveloperName = &quot;Site_Account_NA_Air&quot;),true, IF(AND(NOT(ISNEW()),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.Alias &lt;&gt; &quot;autocln&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, ShippingStreet &lt;&gt; &quot;&quot;, Dealer__c = true, NOT( ISBLANK( IRIT_Dealer_Approved_Date__c ) ), $RecordType.DeveloperName = &quot;Site_Account_NA_Air&quot;, ISCHANGED(IRIT_Dealer_Approved_Date__c) ),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Initializing Account Plan Created Date Field</fullName>
        <actions>
            <name>Set_the_Account_Plan_Created_Date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow stamps the Account Plan Created Date field with the current date once the Account Plan Exists field is equal to &apos;True&apos;.</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, Is_Account_Plan_Exists__c = TRUE, AND(RecordType.DeveloperName &lt;&gt; &apos;GD_Parent&apos;,RecordType.DeveloperName &lt;&gt; &apos;GD_Parent_New&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Account</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <formula>AND($Permission.DataMigrationUser == FALSE ,  SBU_ID_Workflow_Account__c = &apos;&apos;, AND(RecordType.DeveloperName &lt;&gt; &apos;GD_Parent&apos;,RecordType.DeveloperName &lt;&gt; &apos;GD_Parent_New&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Success Integration Status</fullName>
        <actions>
            <name>Reset_Integration_Error_Message</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Permission.DataMigrationUser == FALSE,   ISPICKVAL(Integration_Status__c , &apos;Success&apos;), AND(RecordType.DeveloperName &lt;&gt; &apos;GD_Parent&apos;,RecordType.DeveloperName &lt;&gt; &apos;GD_Parent_New&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Billing Address</fullName>
        <actions>
            <name>Update_Account_Billing_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow Rule to update Account Billing Address for  NA Air - Site Account record type.   Billing Address is a hidden field that is needed for integration and Data.com clean functionality.</description>
        <formula>AND($Permission.DataMigrationUser == FALSE,  ShippingStreet &lt;&gt; &apos;&apos;,  BillingStreet = &apos;&apos;, OR(RecordType.DeveloperName = &apos;Parent_Account&apos;, RecordType.DeveloperName = &apos;CTS_MEIA_Distributor_End_User&apos;, RecordType.DeveloperName = &apos;CTS_EU_Distributor_End_User&apos;, RecordType.DeveloperName = &apos;CTS_EU&apos;, RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, RecordType.DeveloperName = &apos;CTS_MEIA&apos;, RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;, RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Shipping Address</fullName>
        <actions>
            <name>Update_Account_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Shipping_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Shipping_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update shipping address when leads are converted</description>
        <formula>AND( $Permission.DataMigrationUser == FALSE,  BillingStreet &lt;&gt; &apos;&apos;,  ShippingStreet = &apos;&apos;, !ISPICKVAL( Type , &apos;Ship To&apos;), OR(RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos;, RecordType.DeveloperName = &apos;Parent_Account&apos;,RecordType.DeveloperName = &apos;AIRD_Accounts&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>