<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Part_Used_Naming_Convention</fullName>
        <field>Name</field>
        <formula>Part_Description__r.ProductCode</formula>
        <name>Part Used - Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Parts Used - Constant Actions</fullName>
        <actions>
            <name>Part_Used_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Part_Used__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>12/31/1999 7:00 PM</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
