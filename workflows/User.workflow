<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>RHVAC_Update_User_Start_Date</fullName>
        <description>RHVAC_Update_User_Start_Date</description>
        <field>Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>RHVAC_Update_User_Start_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RHVAC_Update_User_Start_Date</fullName>
        <actions>
            <name>RHVAC_Update_User_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>RHVAC_Update_User_Start_Date</description>
        <formula>IF( CONTAINS(UserRole.Name, &apos;RSHVAC&apos;), IF( CONTAINS(UserRole.Name, &apos;Sales Team&apos;), true, false), false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
