<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Bay_Area</fullName>
        <description>Amount Update Approved Alert(Bay Area)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Federal_City</fullName>
        <description>Amount Update Approved Alert(Federal City)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Georgia</fullName>
        <description>Amount Update Approved Alert(Georgia)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_GreatLakes</fullName>
        <description>Amount Update Approved Alert(GreatLakes)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_GreatPlains</fullName>
        <description>Amount Update Approved Alert(GreatPlains)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Great_Southern</fullName>
        <description>Amount Update Approved Alert(Great Southern)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Michigan</fullName>
        <description>Amount Update Approved Alert(Michigan)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Mid_Atlantic</fullName>
        <description>Amount Update Approved Alert(Mid Atlantic)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Mid_West</fullName>
        <description>Amount Update Approved Alert(Mid West)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_NorthWest</fullName>
        <description>Amount Update Approved Alert(NorthWest)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_North_Central</fullName>
        <description>Amount Update Approved Alert(North Central)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_North_East</fullName>
        <description>Amount Update Approved Alert(North East)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_North_Texas</fullName>
        <description>Amount Update Approved Alert(North Texas)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Ontario</fullName>
        <description>Amount Update Approved Alert(Ontario)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_RockyMountains</fullName>
        <description>Amount Update Approved Alert(RockyMountains)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_South</fullName>
        <description>Amount Update Approved Alert(South)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_South_Alberta</fullName>
        <description>Amount Update Approved Alert(South Alberta)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_South_Central</fullName>
        <description>Amount Update Approved Alert(South Central)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_South_Texas</fullName>
        <description>Amount Update Approved Alert(South Texas)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_South_West</fullName>
        <description>Amount Update Approved Alert(South West)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Southern_Gulf</fullName>
        <description>Amount Update Approved Alert(Southern Gulf)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Southwest</fullName>
        <description>Amount Update Approved Alert(Southwest)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_Upper_Midwest</fullName>
        <description>Amount Update Approved Alert(Upper Midwest)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Approved_Alert_UpstateNY</fullName>
        <description>Amount Update Approved Alert(UpstateNY)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Bay_Area</fullName>
        <description>Amount Update Reject Alert(Bay Area)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Federal_City</fullName>
        <description>Amount Update Reject Alert(Federal City)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Georgia</fullName>
        <description>Amount Update Reject Alert(Georgia)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_GreatLakes</fullName>
        <description>Amount Update Reject Alert(GreatLakes)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Great_Plains</fullName>
        <description>Amount Update Reject Alert(Great Plains)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Great_Southern</fullName>
        <description>Amount Update Reject Alert(Great Southern)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Michigan</fullName>
        <description>Amount Update Reject Alert(Michigan)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Mid_Atlantic</fullName>
        <description>Amount Update Reject Alert(Mid Atlantic)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Mid_West</fullName>
        <description>Amount Update Reject Alert(Mid West)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_North_Central</fullName>
        <description>Amount Update Reject Alert(North Central)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_North_Texas</fullName>
        <description>Amount Update Reject Alert(North Texas)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_North_West</fullName>
        <description>Amount Update Reject Alert(North West)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_North_east</fullName>
        <description>Amount Update Reject Alert(North east)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Ontario</fullName>
        <description>Amount Update Reject Alert(Ontario)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_RockyMountains</fullName>
        <description>Amount Update Reject Alert(RockyMountains)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_South</fullName>
        <description>Amount Update Reject Alert(South)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_South_Alberta</fullName>
        <description>Amount Update Reject Alert(South Alberta)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_South_Central</fullName>
        <description>Amount Update Reject Alert(South Central)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_South_Texas</fullName>
        <description>Amount Update Reject Alert(South Texas)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_South_West</fullName>
        <description>Amount Update Reject Alert(South West)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Southern_Gulf</fullName>
        <description>Amount Update Reject Alert(Southern Gulf)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Southwest</fullName>
        <description>Amount Update Reject Alert(Southwest)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_Upper_Midwest</fullName>
        <description>Amount Update Reject Alert(Upper Midwest)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>Amount_Update_Reject_Alert_UpstateNY</fullName>
        <description>Amount Update Reject Alert(UpstateNY)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_Process_Denied</template>
    </alerts>
    <alerts>
        <fullName>CTS_MEIA_New_Opportunity</fullName>
        <description>CTS MEIA New Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Technician_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_New_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>CTS_NA_Email_Alert_for_System_Assessment_Type_Opportunities</fullName>
        <description>CTS NA Email Alert for System Assessment Type Opportunities</description>
        <protected>false</protected>
        <recipients>
            <recipient>District Assessment Engineer</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_NA_System_Assessment</template>
    </alerts>
    <alerts>
        <fullName>CTS_Opportunity_100k_Alert</fullName>
        <description>CTS Opportunity 100k Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Manager__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Delete_Opportunity_Record_Request</fullName>
        <description>Delete Opportunity Record Request</description>
        <protected>false</protected>
        <recipients>
            <field>Delete_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Opp_Records_Request</template>
    </alerts>
    <alerts>
        <fullName>Delete_Opportunity_Request_Reject</fullName>
        <description>Delete Opportunity Request Reject</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>Email_notify_Opportunity_close_date_in_1_week</fullName>
        <description>Email notify - Opportunity close date in 1 week</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/X1_week_prior_to_Opportunity_close_date</template>
    </alerts>
    <alerts>
        <fullName>Engineering_Needed</fullName>
        <description>Engineering Needed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Engineering_Needed</template>
    </alerts>
    <alerts>
        <fullName>Final_Approval</fullName>
        <description>Final Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Final_Approval</template>
    </alerts>
    <alerts>
        <fullName>New_Opp_Created_through_SA</fullName>
        <ccEmails>bethany.barnes@irco.com</ccEmails>
        <description>New Opp Created through SA and notification send to user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Expiring_Service_Agreement_Notification_HTML</template>
    </alerts>
    <alerts>
        <fullName>Next_Step_Due_Date_Overdue</fullName>
        <description>Next Step Due Date Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Next_Step_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Notify_the_opportunity_owner</fullName>
        <description>Notify the opportunity owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>RSHVAC/Opportunity_Owner_Notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Approved_Notification</fullName>
        <description>Opportunity Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EW_Opportunity_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Delete_Approved</fullName>
        <description>Opportunity: Delete Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Opp_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Delete_Rejected</fullName>
        <description>Opportunity: Delete Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Opp_Approval_Denied</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Great_Lake_over_100k</fullName>
        <description>Opportunity: Great Lakes over 100k</description>
        <protected>false</protected>
        <recipients>
            <recipient>surraman@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Federal_City</fullName>
        <description>Opportunity: Over 100K (Federal City)</description>
        <protected>false</protected>
        <recipients>
            <recipient>shashank.singh@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Georgia</fullName>
        <description>Opportunity: Over 100K (Georgia)</description>
        <protected>false</protected>
        <recipients>
            <recipient>greg_snyder@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeremy_w_bivins@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Great_Lakes</fullName>
        <description>Opportunity: Over 100K (Great Lakes)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Great_Plains</fullName>
        <description>Opportunity: Over 100K (Great Plains)</description>
        <protected>false</protected>
        <recipients>
            <recipient>jeffrey_esparrago@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jon_sprunk@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phbecker@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Great_Southern</fullName>
        <description>Opportunity: Over 100K (Great Southern)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Michigan</fullName>
        <description>Opportunity: Over 100K (Michigan)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Mid_Atlantic</fullName>
        <description>Opportunity: Over 100K (Mid Atlantic)</description>
        <protected>false</protected>
        <recipients>
            <recipient>andy_washo@trane.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>robert_dorrill@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Midwest</fullName>
        <description>Opportunity: Over 100K (Midwest)</description>
        <protected>false</protected>
        <recipients>
            <recipient>chad_gooding@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>scott_graham@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Norhwest</fullName>
        <description>Opportunity: Over 100K (Norhwest)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_North_Central</fullName>
        <description>Opportunity: Over 100K (North Central)</description>
        <protected>false</protected>
        <recipients>
            <recipient>dave_yaggy@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>john_gardner@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_North_Texas</fullName>
        <description>Opportunity: Over 100K (North Texas)</description>
        <protected>false</protected>
        <recipients>
            <recipient>mont_wilkes@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paul_cerda@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Northeast</fullName>
        <description>Opportunity: Over 100K (Northeast)</description>
        <protected>false</protected>
        <recipients>
            <recipient>shashank.singh@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Ontario</fullName>
        <description>Opportunity: Over 100K (Ontario)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Rocky_Mountain</fullName>
        <description>Opportunity: Over 100K (Rocky Mountain)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_South_Alberta</fullName>
        <description>Opportunity: Over 100K (South Alberta)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_South_Texas</fullName>
        <description>Opportunity: Over 100K (South Texas)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Southern_Gulf</fullName>
        <description>Opportunity: Over 100K (Southern Gulf)</description>
        <protected>false</protected>
        <recipients>
            <recipient>shashank.singh@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Southwest</fullName>
        <description>Opportunity: Over 100K (Southwest)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_100K_Upstate_NY</fullName>
        <description>Opportunity: Over 100K (Upstate NY)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_100K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Over_250k_Chuck_Funk</fullName>
        <description>Opportunity: Over 250k (Chuck Funk)</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Created_AmountUpdatedBy_250K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Rejected_Notification</fullName>
        <description>Opportunity Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EW_Opportunity_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Submitted_for_Approval_Notification</fullName>
        <description>Opportunity Submitted for Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EW_Opportunity_Approver_Email_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>PTL_Opportunity_Approved_Notification</fullName>
        <description>PTL Opportunity Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EW_Opportunity_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>PTL_Opportunity_Rejected_Notification</fullName>
        <description>PTL Opportunity Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EW_Opportunity_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>SPR_Requester_Approval_Email</fullName>
        <description>SPR Requester Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SPR_Requester_Approval</template>
    </alerts>
    <alerts>
        <fullName>SPR_Requester_Rejection_Email</fullName>
        <description>SPR Requester Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SPR_Requester_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_Approved</fullName>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Pending</fullName>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Approval Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_Rejected</fullName>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Check_Reached_Propose_Stage</fullName>
        <field>Reached_Propose_Stage__c</field>
        <literalValue>1</literalValue>
        <name>CTS Check Reached Propose Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_GES_0_Confidence</fullName>
        <field>Confidence__c</field>
        <literalValue>0%</literalValue>
        <name>CTS GES 0 Confidence</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Install_Opp_from_Lead_Close_Date</fullName>
        <description>Updates the Close Date for an Installation-type opportunity created from a converted lead.</description>
        <field>CloseDate</field>
        <formula>Today()+90</formula>
        <name>CTS Install Opp from Lead - Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Install_Opp_from_Lead_Stage_Update</fullName>
        <description>Sets opportunity stage to &apos;Qualify&apos; for an Installation-type opportunity created from a converted lead.</description>
        <field>StageName</field>
        <literalValue>Stage 1. Qualify</literalValue>
        <name>CTS Install Opp from Lead - Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Install_Opp_from_Lead_Type_Update</fullName>
        <field>Type</field>
        <literalValue>Service(Installs&amp;Lrg.Comissionable Svce)</literalValue>
        <name>CTS Install Opp from Lead - Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Parts_Opp_from_Lead_Amount_Update</fullName>
        <description>Updates the opportunity Amount field with the value from the lead&apos;s AIRD Closed Won Total field if the opportunity results from a converted lead.</description>
        <field>Amount</field>
        <formula>CTSClosedWonTotal__c</formula>
        <name>CTS Parts Opp from Lead - Amount Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Parts_Opp_from_Lead_Close_Date</fullName>
        <description>Updates the opportunity Close Date field to Today if the opportunity is a result of a converted lead.</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>CTS Parts Opp from Lead - Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Parts_Opp_from_Lead_Reason_Update</fullName>
        <description>Updates the opportunity Won Reason field to &apos;Due to Lead Conversion, Opportunity Stage updated to Closed Won&apos; for the opportunity resulting from a converted lead.</description>
        <field>Closed_Lost_Reason__c</field>
        <literalValue>Due to Lead Conversion, Opportunity Stage updated to Closed Won</literalValue>
        <name>CTS Parts Opp from Lead - Reason Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Parts_Opp_from_Lead_Stage_Update</fullName>
        <description>Updates the opportunity Stage to &apos;Closed Won&apos; for a parts opportunity resulting from a converted lead.</description>
        <field>StageName</field>
        <literalValue>Stage 5. Closed Won</literalValue>
        <name>CTS Parts Opp from Lead - Stage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Populate_Pipeline_Date</fullName>
        <field>Pipeline_Dte__c</field>
        <formula>IF((TEXT(StageName)=&quot;Qualify&quot; &amp;&amp; ISNEW()), DATEVALUE(CreatedDate),
IF(TEXT(PRIORVALUE(StageName))=&quot;Stage 1. Qualify&quot; || TEXT(PRIORVALUE(StageName))=&quot;Target&quot;, TODAY(),
Pipeline_Dte__c ))</formula>
        <name>CTS Populate Pipeline Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Populate_Propose_Date</fullName>
        <field>Propose_Date__c</field>
        <formula>TODAY()</formula>
        <name>CTS Populate Propose Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Confidence_100</fullName>
        <field>Confidence__c</field>
        <literalValue>100%</literalValue>
        <name>CTS Update Confidence 100%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Opportunity_Amount</fullName>
        <field>Amount</field>
        <formula>IF(ISBLANK(Opportunity_Line_Item_Amount__c),0,Opportunity_Line_Item_Amount__c) + 
IF(ISBLANK(Oracle_Quote_Amount__c),0,Oracle_Quote_Amount__c)</formula>
        <name>CTS Update Opportunity Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_WaitQuoteApproval</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Clear WaitQuoteApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_won</fullName>
        <field>StageName</field>
        <literalValue>Stage 5. Closed Won</literalValue>
        <name>Close won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Approved_Amount_1000K</fullName>
        <description>Amount that is currently approved for this approval step</description>
        <field>Current_Approved_Amount__c</field>
        <formula>1000000</formula>
        <name>Current Approved Amount_1000K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Approved_Amount_1500K</fullName>
        <description>Amount that is currently approved for this approval step</description>
        <field>Current_Approved_Amount__c</field>
        <formula>1500000</formula>
        <name>Current Approved Amount _1500K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Approved_Amount_2500K</fullName>
        <description>Amount that is currently approved for this approval step</description>
        <field>Current_Approved_Amount__c</field>
        <formula>2500000</formula>
        <name>Current Approved Amount_2500K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Approved_Amount_250K</fullName>
        <description>Amount that is currently approved for this approval step</description>
        <field>Current_Approved_Amount__c</field>
        <formula>250000</formula>
        <name>Current Approved Amount_250K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Approved_Amount_500K</fullName>
        <description>Amount that is currently approved for this approval step</description>
        <field>Current_Approved_Amount__c</field>
        <formula>500000</formula>
        <name>Current Approved Amount_500K</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiatedMichigan_Sales</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiatedMichigan Sales)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Bay_Area</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Bay Area)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_FederalCity</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(FederalCity)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Georgia</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Georgia)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Great_Plains</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Great Plains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Great_Southern</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Great Southern)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Great_lakes</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Great lakes)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_MMidWest</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(MMidWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Mid_Atlantic</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Mid Atlantic)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_NorthWest</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(NorthWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_North_Central</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(North Central)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_North_East</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(North East)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_North_Texas</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(North Texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Ontario</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Ontario)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_RockyMountains</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(RockyMountains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_South</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(South)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_SouthCentral</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(SouthCentral)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_SouthWest</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(SouthWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_South_Alberta</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(South Alberta)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_South_Texas</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(South Texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Southern_Gulf</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Southern Gulf)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Southwest_Area</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Southwest Area)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Upper_MidWest</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Upper MidWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DateApprovalInitiated_Upstate_NY</fullName>
        <field>DateApprovalInitiated__c</field>
        <formula>Today()</formula>
        <name>DateApprovalInitiated(Upstate NY)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_Request_Approv</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Quote_Approved</fullName>
        <field>EW_Quote_Approval_Firm__c</field>
        <literalValue>Approved</literalValue>
        <name>Firm Quote - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Quote_Recalled</fullName>
        <field>EW_Quote_Approval_Firm__c</field>
        <literalValue>Recalled</literalValue>
        <name>Firm Quote - Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Quote_Rejected</fullName>
        <field>EW_Quote_Approval_Firm__c</field>
        <literalValue>Rejected</literalValue>
        <name>Firm Quote - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Firm_Quote_Submitted</fullName>
        <field>EW_Quote_Approval_Firm__c</field>
        <literalValue>Submitted</literalValue>
        <name>Firm Quote - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Get_Parent_name</fullName>
        <field>PT_Parent_Name__c</field>
        <formula>PT_Parent_Opportunity__r.Name</formula>
        <name>Get Parent name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Push_Counter_Field</fullName>
        <description>Increment the Push Counter by 1</description>
        <field>Push_Counter__c</field>
        <formula>IF( 
ISNULL( Push_Counter__c ), 
1, 
Push_Counter__c + 1 
)</formula>
        <name>Increment Push Counter Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_Close</fullName>
        <field>NextStep</field>
        <formula>&quot;Close&quot;</formula>
        <name>Next Step Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_Discover</fullName>
        <field>NextStep</field>
        <formula>&quot;Discover&quot;</formula>
        <name>Next Step Discover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_Negotiate</fullName>
        <field>NextStep</field>
        <formula>&quot;Negotiate&quot;</formula>
        <name>Next Step Negotiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_Propose</fullName>
        <field>NextStep</field>
        <formula>&quot;Propose&quot;</formula>
        <name>Next Step Propose</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Step_Verify</fullName>
        <field>NextStep</field>
        <formula>&quot;Verify&quot;</formula>
        <name>Next Step Verify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Null_ErrorMessage</fullName>
        <field>Integration_Error_Message__c</field>
        <name>Opportunity_Null_ErrorMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PTL_Approval_Status_Set_to_Approved</fullName>
        <description>updates the LFS approval status field to Approved.</description>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>PTL Approval Status Set to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PTL_Approval_Status_set_to_Submitted</fullName>
        <description>updated the LFS Approval Status field to Submitted</description>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>PTL  Approval Status set to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PTL_Approval_Status_sets_to_Recalled</fullName>
        <description>updates the LFS Approval Status field to Recalled</description>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>PTL Approval Status sets to  Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PTL_Approval_Status_sets_to_Rejected</fullName>
        <description>updates the LFS Approval Status field to Rejected</description>
        <field>LFS_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>PTL Approval Status sets to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Reset_Solution_Status</fullName>
        <field>PT_Solution_Center__c</field>
        <name>PT Reset Solution Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_No_Next_Step</fullName>
        <field>NextStep</field>
        <formula>&quot;No next step&quot;</formula>
        <name>PT Set No Next Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_Solution_Status_Last_Changed_Date</fullName>
        <field>PT_Solution_Status_Last_Changed__c</field>
        <formula>NOW()</formula>
        <name>PT Set Solution Status Last Changed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Group</fullName>
        <field>PTZ_Product_Group__c</field>
        <formula>text(PT_Product_Group__c )</formula>
        <name>Product Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Propose_Date_Update</fullName>
        <description>Updates the propose date field to today</description>
        <field>Propose_Date__c</field>
        <formula>Today()</formula>
        <name>Propose Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Counter_Update</fullName>
        <description>Update the field to 1</description>
        <field>Quote_Counter__c</field>
        <formula>1</formula>
        <name>Quote Counter Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Analyze_data_gathered_to_True</fullName>
        <field>Analyze_the_data_gathered__c</field>
        <literalValue>1</literalValue>
        <name>Set Analyze data gathered to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Oppty_Win_Plan_Created_Date</fullName>
        <description>This field update will set the Opportunity Win Plan Created Date field to the current date when the Opportunity Win Plan Exists field is equal to &apos;True&apos;.</description>
        <field>OpportunityWinPlanCreatedDate__c</field>
        <formula>TODAY()</formula>
        <name>Set the Oppty Win Plan Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_BayArea</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(BayArea)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_Georgia</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(Georgia)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_GreatLakes</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(GreatLakes)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_GreatPlains</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(GreatPlains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_GreatSouthern</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(GreatSouthern)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_Michigan</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(Michigan)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_MidAtlantic</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(MidAtlantic)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_Mid_West</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(Mid West)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_North_Central</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(North Central)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_North_East</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(North East)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_North_West</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(North West)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_North_texas</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(North texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_Ontario</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(Ontario)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_RockyMountain</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(RockyMountain)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_South</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(South)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_SouthAlberta</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(SouthAlberta)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_SouthCentral</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(SouthCentral)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_SouthTexas</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(SouthTexas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_SouthWest</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(SouthWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_SouthernGulf</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(SouthernGulf)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_Upper_Midwest</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(Upper Midwest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_UpstateNY</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(UpstateNY)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StageAfterAmtUpdateReject_federal</fullName>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>StageAfterAmtUpdateReject(federal)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Close_Date</fullName>
        <description>This sets the actual Close Date</description>
        <field>Actual_Close_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Actual Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update Integration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update Integration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Type</fullName>
        <description>Update Opportunity Type</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_EU</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opportunity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vertical_Marketing</fullName>
        <description>If the lead source on the Opportunity is Vertical Market set the &quot;Was this a result of prospecting&quot; field to Vertical marketing</description>
        <field>Prospecting_Activity_Flag__c</field>
        <literalValue>Vertical Prospecting</literalValue>
        <name>Vertical Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApprovalNorthWest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApprovalNorthWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Bay_Area</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Bay Area)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_FederalCity</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(FederalCity)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Georgia</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Georgia)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Great_Lakes</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Great Lakes)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Great_Plains</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Great Plains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Great_Southern</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Great Southern)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Michigan</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Michigan)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_MidWest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(MidWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Mid_Atlantic</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Mid Atlantic)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_North_Central</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(North Central)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_North_East</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(North East)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_North_texas</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(North  texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Ontario</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Ontario)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_RockyMountains</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(RockyMountains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_South</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(South)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_SouthCentral</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(SouthCentral)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_SouthWest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(SouthWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_South_Alberta</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(South Alberta)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_South_Texas</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(South Texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Southern_Gulf</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Southern Gulf)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Southwest_Area</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Southwest Area)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Upper_MidWest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Upper MidWest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteApproval_Upstate_NY</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>1</literalValue>
        <name>WaitQuoteApproval(Upstate NY)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Bay_Area</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Bay Area)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Federal_City</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Federal City)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Georgia</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Georgia)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_GreatSouthern</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(GreatSouthern)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Great_Plains</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Great Plains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Great_lakes</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Great lakes)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Michigan_Sales</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Michigan Sales)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Mid_Atlantic</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Mid Atlantic)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Mid_West</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Mid West)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_North_Central</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(North Central)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_North_East</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(North East)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_North_West</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(North West)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_North_texas</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(North texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Ontario</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Ontario)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Rocky_Mountains</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Rocky Mountains)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_South</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(South)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_South_Alberta</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(South Alberta)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_South_Central</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(South Central)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_South_Texas</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(South Texas)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_South_West</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(South West)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Southern_Gulf</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Southern Gulf)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Southwest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Southwest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Upper_Midwest</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Upper Midwest)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WaitQuoteFalse_Upstate_NY</fullName>
        <field>Waiting_for_Quote_Approval__c</field>
        <literalValue>0</literalValue>
        <name>WaitQuoteFalse(Upstate NY)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1st_value_to_target</fullName>
        <field>StageName</field>
        <literalValue>Stage 1. Qualify</literalValue>
        <name>1st value to target</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_discover</fullName>
        <field>PT_date_discover__c</field>
        <formula>TODAY()</formula>
        <name>date_discover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_negotiate</fullName>
        <field>PT_date_negotiate__c</field>
        <formula>TODAY()</formula>
        <name>date_negotiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_proposal</fullName>
        <field>PT_date_proposal__c</field>
        <formula>TODAY()</formula>
        <name>date_proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_qualify</fullName>
        <field>PT_date_qualify__c</field>
        <formula>TODAY()</formula>
        <name>date_qualify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>date_target</fullName>
        <field>PT_date_target__c</field>
        <formula>TODAY()</formula>
        <name>date_target</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Approval_Inprogress_to_False</fullName>
        <field>Is_Approval_InProgress__c</field>
        <literalValue>0</literalValue>
        <name>set Approval Inprogress to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Process_As_Blank</fullName>
        <field>Process__c</field>
        <name>set Process As Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_stage_0_Plan</fullName>
        <description>When you create an opportunity the stage equals to 0.Plan</description>
        <field>StageName</field>
        <literalValue>0.Plan</literalValue>
        <name>set stage 0.Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>IR_Opportunity_OBM_PROD</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://oci-icosbomwdev.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrD3vSalesforce&amp;password=IRPrtnrD3vSalesforce&amp;</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_Opportunity_OBM_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CTS Check Reached Propose Stage</fullName>
        <actions>
            <name>CTS_Check_Reached_Propose_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(StageName,&apos;Stage 3. Propose/Quote&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS GES Opps 0 Confidence</fullName>
        <actions>
            <name>CTS_GES_0_Confidence</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  RecordType.DeveloperName = &apos;Global_Engineered_Services&apos;, IsClosed,  OR(  TEXT(StageName) = &apos;Stage 5. Closed Lost&apos;,  TEXT(StageName) = &apos;Stage 5. Closed Lost&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS GES%2FGlobal Opps 0 Confidence</fullName>
        <actions>
            <name>CTS_GES_0_Confidence</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(   OR( BEGINS($RecordType.DeveloperName, &apos;CTS&apos;), $RecordType.DeveloperName = &apos;Global_Engineered_Services&apos;,$RecordType.DeveloperName = &apos;Milton_Roy_Opportunity&apos;),  IsClosed, TEXT(StageName) = &apos;Stage 5. Closed Lost&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Won Opps 100%25 Confidence</fullName>
        <actions>
            <name>CTS_Update_Confidence_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(OR((BEGINS($RecordType.DeveloperName, &quot;CTS&quot;)),$RecordType.DeveloperName == &quot;NA_Air&quot;, $RecordType.DeveloperName == &quot;OEM_Opportunity&quot;, $RecordType.DeveloperName == &quot;Hibon_Opportunity&quot;, $RecordType.DeveloperName == &quot;Global_Engineered_Services&quot;, $RecordType.DeveloperName == &quot;Standard_Opportunity&quot;, $RecordType.DeveloperName == &quot;PTL_Opportunity&quot;, $RecordType.DeveloperName == &quot;Milton_Roy_Opportunity&quot;), TEXT(StageName) = &quot;Stage 5. Closed won&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Parts and Service Opp From Lead</fullName>
        <actions>
            <name>CTS_Parts_Opp_from_Lead_Amount_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Parts_Opp_from_Lead_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Parts_Opp_from_Lead_Reason_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Parts_Opp_from_Lead_Stage_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NA Air</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsFromLead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.LeadType__c</field>
            <operation>startsWith</operation>
            <value>Parts</value>
        </criteriaItems>
        <description>Updates the Parts and Service type opportunity when the opportunity is created as a result of a converted lead.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Populate Pipeline Date</fullName>
        <actions>
            <name>CTS_Populate_Pipeline_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(OR(ISPICKVAL(StageName, &apos;Stage 2. Discover/Verify&apos;), ISPICKVAL(StageName, &apos;Stage 3. Propose/Quote&apos;), ISPICKVAL(StageName, &apos;Stage 1. Qualify&apos;), ISPICKVAL(StageName, &apos;Target&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Service Install Opp From Lead</fullName>
        <actions>
            <name>CTS_Install_Opp_from_Lead_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Install_Opp_from_Lead_Stage_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Install_Opp_from_Lead_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Moves the opportunity record updates from Apex code to a workflow for the service installation opportunity created from a converted lead.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(RecordType.DeveloperName = &apos;NA_Air&apos;, Contains(LeadType__c, &apos;Service(Installs)&apos;), IsFromLead__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Update Opportunity Amount</fullName>
        <actions>
            <name>CTS_Update_Opportunity_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is used to update Amount on the Opportunity</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND(OR(ISCHANGED( Opportunity_Line_Item_Amount__c ) ,ISBLANK(Amount),ISCHANGED( Oracle_Quote_Amount__c )),OR(CONTAINS($RecordType.DeveloperName, &quot;CTS&quot;), CONTAINS($RecordType.DeveloperName, &quot;NA_Air&quot;),CONTAINS($RecordType.DeveloperName, &quot;Standard_Opportunity&quot;),CONTAINS($RecordType.DeveloperName, &quot;Global_Engineered_Services&quot;),CONTAINS($RecordType.DeveloperName, &quot;Milton_Roy_Opportunity&quot;),CONTAINS($RecordType.DeveloperName, &quot;PTL_Opportunity&quot;),CONTAINS($RecordType.DeveloperName,&quot;Hibon_Opportunity&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Opportunity_OBM_CTS_GLOBAL</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Opportunity_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to send Opportunity Outbound notifications to Siebel via Fusion</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; IF(AND(ISNEW(), NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;,  Owner.LastName&lt;&gt;&quot;Migration&quot;, Amount &gt; 0, OR( RecordType.DeveloperName == &quot;CTS_EU&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM&quot;, UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect&quot;) ),true, IF(AND(NOT(ISNEW()),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, Amount &gt; 0,  OR( RecordType.DeveloperName == &quot;CTS_EU&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect&quot;),  OR(ISCHANGED(Name),ISCHANGED(Team_Sales__c),ISCHANGED(StageName), ISCHANGED(CloseDate), ISCHANGED(Amount), ISCHANGED(Probability),ISCHANGED(Closed_Lost_Reason__c), ISCHANGED(Closed_Lost_Reason_Notes__c),ISCHANGED(Description),  ISCHANGED(Type),ISCHANGED(Stage_Category__c),ISCHANGED(Confidence__c),ISCHANGED(AccountId)  )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Opportunity_OBM_PROD</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Opportunity_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(ISNEW(), NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;,  Owner.LastName&lt;&gt;&quot;Migration&quot;, Amount &gt; 0, $RecordType.DeveloperName = &quot;NA_Air&quot; ,NOT((ISPICKVAL(StageName, &apos;Target&apos;)))  ),true, IF(AND(NOT(ISNEW()),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT((ISPICKVAL(StageName, &apos;Target&apos;))),  NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, Amount &gt; 0,  $RecordType.DeveloperName = &quot;NA_Air&quot;,  OR(ISCHANGED(Name),ISCHANGED(Team_Sales__c),ISCHANGED(StageName), ISCHANGED(CloseDate), ISCHANGED(Amount), ISCHANGED(Probability),ISCHANGED(Closed_Lost_Reason__c), ISCHANGED(Closed_Lost_Reason_Notes__c),ISCHANGED(Description),  ISCHANGED(Type),ISCHANGED(Stage_Category__c),ISCHANGED(Confidence__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Initializing Opportunity Win Plan Created Date field</fullName>
        <actions>
            <name>Set_the_Oppty_Win_Plan_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The workflow rule time stamps the Opportunity Win Plan Created Date field when the Opportunity Win Plan Exists field is equal to &apos;True&apos;.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(Is_Opportunity_WIN_Plan_Exists__c=True)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity_Null_ErrorMessage</fullName>
        <actions>
            <name>Opportunity_Null_ErrorMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND(TEXT( Integration_Status__c) == &apos;Success&apos;,OR($RecordType.DeveloperName == &quot;CTS_EU&quot; ,$RecordType.DeveloperName == &quot;CTS_EU_Indirect&quot;, $RecordType.DeveloperName == &quot;CTS_MEIA&quot;, $RecordType.DeveloperName == &quot;CTS_MEIA_Indirect&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PT WF03 - Auto close Opportunity</fullName>
        <active>true</active>
        <description>NOTE THIS RULE WILL NOT FIRE FOR ANY RECORDS CREATE OR MOD AFTER OCT 25, 2019 BUT  NEEDS TO STAY ACTIVE UNTL ALL TIME DEPENDENT EXECUTIONS HAVE COMPLETED.  CURRENTLY THIS IS 8/31/2022 HOWEVER THAT WILL CHANGE AS RECORDS ARE MODIFIED, CHECK BACK AS NEEDED</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND($RecordType.DeveloperName == &quot;PT_powertools&quot;, ISPICKVAL(PT_Sales_Activity__c, &apos;5.Base&apos;), IsClosed =False,(Amount &gt; 0), PT_Owner_full_Name__c  &lt;&gt; &apos;Ivan Cuka&apos;, (Last_Modifed_Date_Time__c  &lt; &quot;2019-10-25 18:47:00&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_won</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Propose Date Update</fullName>
        <actions>
            <name>Propose_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND(ISPICKVAL(StageName ,&apos;Stage 3. Propose/Quote&apos;),OR(RecordType.DeveloperName = &apos;CTS_EU&apos;,RecordType.DeveloperName = &apos;CTS_MEIA&apos;,RecordType.DeveloperName = &apos;Standard_Opportunity&apos;,RecordType.DeveloperName = &apos;Milton_Roy_Opportunity&apos;,RecordType.DeveloperName = &apos;PTL_Opportunity&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Push Counter</fullName>
        <actions>
            <name>Increment_Push_Counter_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Increment the Push Counter field by 1</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; IF( CloseDate &gt; PRIORVALUE( CloseDate ), IF (MONTH(CloseDate) &lt;&gt; MONTH(PRIORVALUE( CloseDate )) , TRUE, FALSE), FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quote Counter Update</fullName>
        <actions>
            <name>Quote_Counter_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(ISPICKVAL(StageName, &apos;Stage 3. Propose/Quote&apos;), OR(RecordType.DeveloperName = &apos;NA_Air&apos;, RecordType.DeveloperName = &apos;Hibon_Opportunity&apos;, RecordType.DeveloperName = &apos;OEM_Opportunity&apos;, RecordType.DeveloperName = &apos;Global_Engineered_Services&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Actual Close Date</fullName>
        <actions>
            <name>Update_Actual_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This updates the actual Close Date</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND (OR(RecordType.DeveloperName == &quot;NA_Air&quot;, RecordType.DeveloperName == &quot;CTS_LATAM_Indirect&quot;,  RecordType.DeveloperName == &quot;CTS_AP&quot;,   RecordType.DeveloperName == &quot;CTS_EU&quot;,   RecordType.DeveloperName == &quot;CTS_LATAM&quot;,  RecordType.DeveloperName == &quot;CTS_MEIA&quot;,   RecordType.DeveloperName == &quot;CTS_AIRD_Opportunity&quot;,  RecordType.DeveloperName == &quot;CTS_EU_Indirect&quot;,  RecordType.DeveloperName == &quot;CTS_MEIA_Indirect&quot;,  RecordType.DeveloperName == &quot;Standard_Opportunity&quot;,  RecordType.DeveloperName == &quot;LFS_Opportunity&quot;,RecordType.DeveloperName == &quot;Milton_Roy_Opportunity&quot;,RecordType.DeveloperName == &quot;PTL_Opportunity&quot;),  IsClosed,isnull(Actual_Close_Date__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Vertical Marketing</fullName>
        <actions>
            <name>Vertical_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL( LeadSource , &apos;Vertical Market&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Gather_Asset_Site_Data_Face_to_Face</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Gather Asset / Site Data (Face to Face)</subject>
    </tasks>
    <tasks>
        <fullName>Gather_Historical_Spend_Data</fullName>
        <assignedToType>owner</assignedToType>
        <description>Evaluate expiring service agreement margin.</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Evaluate Expiring Agreement Margin</subject>
    </tasks>
    <tasks>
        <fullName>Gather_Work_Scope_Data_Face_to_Face</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Gather Work Scope Data (Face to Face)</subject>
    </tasks>
    <tasks>
        <fullName>Identify_Potential_Solutions</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Identify Potential Solutions (Discover stage)</subject>
    </tasks>
    <tasks>
        <fullName>Intelli_Survey</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Intelli-Survey (Discover stage)</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step_Close</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>365</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Close</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step_Discover</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Discover</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step_Negotiate</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>240</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Negotiate</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step_Propose</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>270</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Propose</subject>
    </tasks>
    <tasks>
        <fullName>Next_Step_Qualify</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Qualify</subject>
    </tasks>
    <tasks>
        <fullName>Notification_of_expiring_service_agreement</fullName>
        <assignedToType>owner</assignedToType>
        <description>Confirming the notification for the expiring service agreement was delivered to the opportunity owner.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Notification of expiring service agreement</subject>
    </tasks>
    <tasks>
        <fullName>Obtained_PO_and_all_documentations</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Obtained PO and all documentations needed to enter the orders (Negotiate stage)</subject>
    </tasks>
    <tasks>
        <fullName>Please_send_the_customer_quote_for_this_opportunity</fullName>
        <assignedToType>owner</assignedToType>
        <description>The opportunity associated to this task does not currently have any quote records attached to it. Please generate the quote and send it to the customer indicated on the opportunity record.

Thank you!</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Please send the customer quote for this opportunity!</subject>
    </tasks>
    <tasks>
        <fullName>Present_Assessment_Proposal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Present Assessment Proposal</subject>
    </tasks>
    <tasks>
        <fullName>Review_Potenital_Solutions</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review Potenital Solutions (Verify stage)</subject>
    </tasks>
    <tasks>
        <fullName>Review_VIQ_and_Quote_Assessment</fullName>
        <assignedTo>District Assessment Engineer</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Review VIQ and Quote Assessment</subject>
    </tasks>
    <tasks>
        <fullName>Reviewed_TCs_and_timing_of_purchase_decision</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reviewed T&amp;C&apos;s and timing of purchase decision (Propose/Quote stage)</subject>
    </tasks>
    <tasks>
        <fullName>Reviewed_and_Addressed_all_Customer_inquiries</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reviewed and Addressed all Customer inquiries (Negotiate stage)</subject>
    </tasks>
    <tasks>
        <fullName>Site_Condition_Review</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Site Condition Review</subject>
    </tasks>
    <tasks>
        <fullName>TKM_Please_set_up_a_Deal_Review_Board_Assessment</fullName>
        <assignedToType>owner</assignedToType>
        <description>The listed opportunity has met the requirements for assessment by the Deal Review Board. Please begin to gather the materials needed for the review.

Thank you!</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Please set up a Deal Review Board assessment for the listed opportunity</subject>
    </tasks>
    <tasks>
        <fullName>Technical_Qualification_Worksheet</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Technical Qualification Worksheet (Discover stage)</subject>
    </tasks>
    <tasks>
        <fullName>Verify</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Step - Verify</subject>
    </tasks>
</Workflow>