<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RMA_Shipment_Confirmation</fullName>
        <description>RMA Shipment Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>Case_contact_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@clubcar.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Club_Car_GPSi/RMA_Shipped_Letterhead</template>
    </alerts>
    <fieldUpdates>
        <fullName>shipment_email_update</fullName>
        <field>Case_contact_email__c</field>
        <formula>Case__r.Contact.Email</formula>
        <name>shipment-email update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>RMA Shipping Notice</fullName>
        <actions>
            <name>RMA_Shipment_Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Shipment__c.Tracking_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CC GPSi RMA Case</value>
        </criteriaItems>
        <description>This is triggered when a shipping object is saved AND it has a tracking #.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Shipment_Constant</fullName>
        <actions>
            <name>shipment_email_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Shipment__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>12/31/1999 10:00 PM</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
