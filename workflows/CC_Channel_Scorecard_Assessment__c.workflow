<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Estimate_Score</fullName>
        <field>Estimated_Score__c</field>
        <formula>Weighted_Score_Total__c/Possible_Total_Score__c</formula>
        <name>Populate Estimate Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Total_Possible_Score</fullName>
        <field>Possible_Total_Score__c</field>
        <formula>Sales_Performance_Possible_Score__c+
Business_Integration_Possible_Score__c+
Marketing_Performance_Possible_Score__c+
Financial_Performance_Possible_Score__c+
Service_Performance_Possible_Score__c+
Retail_Golf_XRT_Cov_Prac_PossibleScore__c+
Commercial_Industrial_Turf_PossibleScore__c+
Fleet_Golf_Cov_Pract_Possible_Score__c</formula>
        <name>Populate Total Possible Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Total_Weighted_Score</fullName>
        <field>Weighted_Score_Total__c</field>
        <formula>Sales_Performance_Weighted_Score__c+
Business_Integration_Weighted_Score__c+
Marketing_Performance_Weighted_Score__c+
Financial_Performance_Weighted_Score__c+
Service_Performance_Weighted_Score__c+
Retail_Golf_XRT_Cov_Prac_WeightedScores__c+
Commercial_Industrial_Turf_WeightedScore__c+
Fleet_Golf_Cov_Pract_Weighted_Score__c</formula>
        <name>Populate Total Weighted Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Estimate Score</fullName>
        <actions>
            <name>Populate_Estimate_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Channel_Scorecard_Assessment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Total Possible Score</fullName>
        <actions>
            <name>Populate_Total_Possible_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Channel_Scorecard_Assessment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Total Weighted Score</fullName>
        <actions>
            <name>Populate_Total_Weighted_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Channel_Scorecard_Assessment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
