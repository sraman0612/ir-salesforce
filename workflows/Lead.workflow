<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_Distribution_Email_Alert_Internal</fullName>
        <description>CTS Distribution Email Alert Internal</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bethany.barnes@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tj.stevens@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Indirect_Contact_Lead_Alert</fullName>
        <description>CTS Distribution Indirect Contact Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Indirect_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Indirect_Contact_Lead_Alert_Champion</fullName>
        <description>CTS Distribution Indirect Contact Lead Alert Champion</description>
        <protected>false</protected>
        <recipients>
            <field>Indirect_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Champion</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Indirect_Contact_Lead_Alert_CompAir</fullName>
        <description>CTS Distribution Indirect Contact Lead Alert CompAir</description>
        <protected>false</protected>
        <recipients>
            <field>Indirect_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_CompAir</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Indirect_Contact_Lead_Alert_Gardner_Denver</fullName>
        <description>CTS Distribution Indirect Contact Lead Alert Gardner Denver</description>
        <protected>false</protected>
        <recipients>
            <field>Indirect_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Gardner_Denver</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Additional_Email</fullName>
        <description>CTS Distribution Lead Additional Email</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Contact__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Additional_Email_Champion</fullName>
        <description>CTS Distribution Lead Additional Email Champion</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Contact__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Champion</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Additional_Email_CompAir</fullName>
        <description>CTS Distribution Lead Additional Email CompAir</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Contact__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_CompAir</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Additional_Email_Gardner_Denver</fullName>
        <description>CTS Distribution Lead Additional Email Gardner Denver</description>
        <protected>false</protected>
        <recipients>
            <field>Additional_Contact__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Gardner_Denver</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Alert</fullName>
        <description>CTS Distribution Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Distributor_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Alert_Champion</fullName>
        <description>CTS Distribution Lead Alert Champion</description>
        <protected>false</protected>
        <recipients>
            <field>Distributor_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Champion</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Alert_CompAir</fullName>
        <description>CTS Distribution Lead Alert CompAir</description>
        <protected>false</protected>
        <recipients>
            <field>Distributor_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_CompAir</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Alert_Gardner_Denver</fullName>
        <description>CTS Distribution Lead Alert Gardner Denver</description>
        <protected>false</protected>
        <recipients>
            <field>Distributor_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>marketing.simmern@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Distribution_Lead_Gardner_Denver</template>
    </alerts>
    <alerts>
        <fullName>CTS_Distribution_Lead_Contact_Email</fullName>
        <description>CTS Distribution Lead Contact Email</description>
        <protected>false</protected>
        <recipients>
            <field>Indirect_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Distribution_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Notification_IR_Pending_Lead_Request</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Notification - IR (Pending Lead Request)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Notification_IR_Pending_Lead_Request</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Received_Lead</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Received (Lead)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Received_Lead</template>
    </alerts>
    <alerts>
        <fullName>CTS_Lead_Time_Violation</fullName>
        <description>CTS Lead Time Violation</description>
        <protected>false</protected>
        <recipients>
            <recipient>bethany.barnes@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tj.stevens@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Lead_Time_Violation</template>
    </alerts>
    <alerts>
        <fullName>Delete_Lead_Request_Reject</fullName>
        <description>Delete Lead Request Reject</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <alerts>
        <fullName>Hibon_Lead_Creation_Alert</fullName>
        <description>Hibon Lead Creation Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>lucie_gagnon@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Lead_48_Overdue</fullName>
        <description>Lead 48 Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_48_Hours_Overdue</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days</fullName>
        <ccEmails>Brenda.Landry@gardnerdenver.com</ccEmails>
        <ccEmails>Oscar.Lang@gardnerdenver.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days_APAC</fullName>
        <ccEmails>steven.tian@irco.com</ccEmails>
        <ccEmails>mark.lou@irco.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days - Thomas APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days_ILS_APAC</fullName>
        <ccEmails>steven.tian@irco.com</ccEmails>
        <ccEmails>matthias.huebner@irco.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days - ILS APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days_Tricontinent_APAC</fullName>
        <ccEmails>steven.tian@irco.com</ccEmails>
        <ccEmails>oscar.lang@irco.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days - Tricontinent APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days_Welch_APAC</fullName>
        <ccEmails>steven.tian@irco.com</ccEmails>
        <ccEmails>david.cai@irco.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days - Welch APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_14_Days_Zinsser_APAC</fullName>
        <ccEmails>steven.tian@irco.com</ccEmails>
        <ccEmails>matthias.huebner@irco.com</ccEmails>
        <description>Lead in Working Status Reminder 14 Days - Zinsser APAC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Lead_in_Working_Status_Reminder_7_Days</fullName>
        <ccEmails>Brenda.Landry@gardnerdenver.com</ccEmails>
        <description>Lead in Working Status Reminder 7 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_in_Working_Status_Reminder_7_Days</template>
    </alerts>
    <alerts>
        <fullName>Notification_Eloqua_Leads</fullName>
        <ccEmails>rgoyal@providity.com</ccEmails>
        <description>Notification Eloqua Leads</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>PTL_Distribution_Lead_Alert</fullName>
        <description>PTL Distribution Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <field>PTL_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Channel_Partner_Sales_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/PTL_Tech_Support_Distribution_Assignment_Template</template>
    </alerts>
    <alerts>
        <fullName>PTL_Tech_Support_Lead_Assignment</fullName>
        <ccEmails>Fabricio.Velasco@irco.com</ccEmails>
        <description>PTL Tech Support Lead Assignment</description>
        <protected>false</protected>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/PTL_Tech_Support_Distribution_Assignment_Template</template>
    </alerts>
    <alerts>
        <fullName>PT_Closed_Info_Only_Email_Alert</fullName>
        <ccEmails>cx@irco.com</ccEmails>
        <description>PT Closed Info Only Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Powertools/PT_Closed_Info_Only_Lead_Notice</template>
    </alerts>
    <alerts>
        <fullName>PT_Internal_Lead_Assignment</fullName>
        <description>PT Internal Lead Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Powertools/New_Lead_Alert</template>
    </alerts>
    <alerts>
        <fullName>PT_Send_CX_New_Lead</fullName>
        <ccEmails>irtoolhelp@irco.com,ptlead@6-1s8wo3itjny5720j72h0fy56y6lhbfckw83wmyboxronz0k6if.j-1nuezeai.na60.apex.salesforce.com</ccEmails>
        <description>PT Send CX New Lead</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Powertools/CX_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>PT_Send_Distribotor_New_Lead</fullName>
        <ccEmails>ptlead@6-1s8wo3itjny5720j72h0fy56y6lhbfckw83wmyboxronz0k6if.j-1nuezeai.na60.apex.salesforce.com</ccEmails>
        <description>PT Send Distribotor New Lead</description>
        <protected>false</protected>
        <recipients>
            <field>Distribution_Account_s_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Powertools/Distributor_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Send_Asia_Queue_Email_Notification</fullName>
        <ccEmails>Paulina.Gryczka@gardnerdenver.com</ccEmails>
        <description>Send Asia Queue Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>fabricio.velasco@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tj.stevens@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Lead_Assigned_to_Asia_Queue</template>
    </alerts>
    <alerts>
        <fullName>Send_Lead_Assignment</fullName>
        <description>Send Lead Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Lead_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Send_Lead_notification_for_leads_in_the_Not_My_Lead_Queue</fullName>
        <description>Send Lead notification for leads in the Not My Lead Queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>surraman@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Lead_Assigned_to_the_Not_My_Lead_Queue</template>
    </alerts>
    <alerts>
        <fullName>Send_Portable_Queue_Email_Notification</fullName>
        <description>Send Portable Queue Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>interlynx@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tj.stevens@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>notifications.nacts@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Lead_Assigned_to_Portable_Queue</template>
    </alerts>
    <alerts>
        <fullName>Send_email_notification_for_leads_being_routed_to_the_Not_My_Lead_Queue</fullName>
        <ccEmails>irleads@irleads.com</ccEmails>
        <ccEmails>support@irleads.com</ccEmails>
        <description>Send email notification for leads being routed to the Not My Lead Queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>bethany.barnes@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tj.stevens@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CTS_Lead_Assigned_to_the_Not_My_Lead_Queue</template>
    </alerts>
    <alerts>
        <fullName>Target_Delete_Approved</fullName>
        <description>Target: Delete Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Target_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Target_Delete_Rejected</fullName>
        <description>Target: Delete Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Target_Approval_Denied</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing</fullName>
        <description>Telemarketing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Telemarketing</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assignment_Key_Update_if_NULL</fullName>
        <description>Will update the Assignment Key field on the Lead record with the Postal Code field value if the Assignment Key field has a NULL value when integrating from Silverpop.</description>
        <field>Assignment_Key__c</field>
        <formula>UPPER( 
IF(Country = &apos;Canada&apos; ,  LEFT(PostalCode,3) ,
IF(Country = &apos;CANADA&apos; ,  LEFT(PostalCode,3) ,
IF(Country = &apos;USA&apos; ,  LEFT(PostalCode,5) ,
IF(Country = &apos;Mexico&apos; ,  LEFT(PostalCode,5) ,
IF(Country = &apos;MEXICO&apos; ,  LEFT(PostalCode,5) ,
( City+State+Country ))))))
)</formula>
        <name>Assignment Key Update if NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_AIRD_RecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>AIRD_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS AIRD RecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_City</fullName>
        <field>CTS_Global_Shipping_City__c</field>
        <formula>City</formula>
        <name>CTS Global City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_RT_to_NA_Air</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NA_Air</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global RT to NA Air</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_State</fullName>
        <field>CTS_Global_Shipping_State_Province__c</field>
        <formula>State</formula>
        <name>CTS Global State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Street</fullName>
        <field>CTS_Global_Shipping_Address_1__c</field>
        <formula>Street</formula>
        <name>CTS Global Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Zip</fullName>
        <field>CTS_Global_Shipping_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>CTS Global Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Spam_Lead_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Asia_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CTS Spam Lead Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Update_Lead_Country_for_Siebel</fullName>
        <field>Country</field>
        <formula>&apos;USA&apos;</formula>
        <name>CTS Update Lead Country for Siebel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_lead_to_In_Process_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CTS_New_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change lead to In Process RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_rquest_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disposition_Date_Update</fullName>
        <description>Updates Disposition Date field when Disposition Reason has been entered.</description>
        <field>Date_Dispositioned__c</field>
        <formula>Today()</formula>
        <name>Disposition Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LeadStatusUpdate</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>StatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Record_Type_CTS</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NA_Air</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lead Record Type CTS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <field>SBU_ID_Workflow__c</field>
        <formula>IF(
RecordType.DeveloperName = &apos;CHVAC_NA_Mktg_Automation&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;India_CS_Lead&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;CC_Status_Not_New&apos;, &apos;Club Car&apos;,
IF(RecordType.DeveloperName = &apos;Club_Car&apos;, &apos;Club Car&apos;,
IF(RecordType.DeveloperName = &apos;CTS_LATAM_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_MEIA_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_New_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;GES_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;Hibon_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;NA_Air&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;OEM_Lead&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;,
&apos;None&apos;)))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_CTS_Lead_from_Postal_to_Not_My_Lead</fullName>
        <field>OwnerId</field>
        <lookupValue>Not_My_Lead</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Move CTS Lead from Postal to Not My Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_Lead_Source_1</fullName>
        <field>PT_Lead_Source_1__c</field>
        <formula>case(Product__c , &quot;Contact Us&quot;,&quot;IngersollRandProducts.com contact form&quot;, &quot;action&quot;, &quot;reaction&quot;, &quot;&quot;)</formula>
        <name>PT Set Lead Source 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_Lead_Source_2</fullName>
        <field>PT_Lead_Source_2__c</field>
        <formula>PT_Company_Type__c</formula>
        <name>PT Set Lead Source 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_Lead_Source_3</fullName>
        <field>PT_Lead_Source_3__c</field>
        <formula>PT_Product__c</formula>
        <name>PT Set Lead Source 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_Lead_Status_Change_Date</fullName>
        <field>PT_Lead_Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>PT Set Lead Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Reassign_to_Queue_Flag</fullName>
        <description>Sets the Reassign to Queue flag to False for CTS leads being assigned to a user instead of a queue</description>
        <field>Reassign_to_Queue__c</field>
        <literalValue>0</literalValue>
        <name>Reset Reassign to Queue Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reupdate_Email_ID</fullName>
        <description>Reupdate email id as valid</description>
        <field>Email</field>
        <formula>LEFT(Email, LEN(Email)-(LEN(Email)- FIND(&quot;.invalid&quot;, Email)+1))</formula>
        <name>Reupdate Email ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SF_2_SF_Mapping</fullName>
        <field>SF2SF_IR_ID__c</field>
        <formula>Id</formula>
        <name>SF 2 SF Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assigned_Date_Time</fullName>
        <field>Assigned_Date_Time__c</field>
        <formula>IF(
AND( 
TIMEVALUE( NOW() - (6/24) ) &gt;= TIMEVALUE(&quot;08:00:00.000&quot;), 
TIMEVALUE( NOW() - (6/24)) &lt;= TIMEVALUE(&quot;17:00:00.000&quot;), 

CASE( 
MOD( TODAY() - DATE(1900, 1, 7), 7), 
1, 1, 
2, 1, 
3, 1, 
4, 1, 
5, 1, 
0) = 1 
),
NOW(), 

CASE( 
WEEKDAY( TODAY()),
1, DATETIMEVALUE(TEXT(TODAY()+1)+&quot; &quot;+&quot;12:00:00&quot;), 
2, DATETIMEVALUE(TEXT(TODAY()+1)+&quot; &quot;+&quot;12:00:00&quot;), 
3, DATETIMEVALUE(TEXT(TODAY()+1)+&quot; &quot;+&quot;12:00:00&quot;), 
4, DATETIMEVALUE(TEXT(TODAY()+1)+&quot; &quot;+&quot;12:00:00&quot;),
5, DATETIMEVALUE(TEXT(TODAY()+1)+&quot; &quot;+&quot;12:00:00&quot;),
6, DATETIMEVALUE(TEXT(TODAY()+3)+&quot; &quot;+&quot;12:00:00&quot;),
7, DATETIMEVALUE(TEXT(TODAY()+2)+&quot; &quot;+&quot;12:00:00&quot;),
null 
)
)</formula>
        <name>Set Assigned Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PT_Product</fullName>
        <field>PT_Product__c</field>
        <formula>Product__c</formula>
        <name>PT Set PT Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reassign_to_Queue_Flag</fullName>
        <field>Reassign_to_Queue__c</field>
        <literalValue>1</literalValue>
        <name>Set Reassign to Queue Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusUpdate</fullName>
        <description>If no activity is done on Lead record for 45 days from Last Modified date, Status will be updated to &apos;Closed&apos;</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>AutomaticStatusUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update</fullName>
        <field>Status</field>
        <literalValue>Converted</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status</fullName>
        <field>TwilioSF__Last_Message_Status__c</field>
        <name>Clear Last Message Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status_Date</fullName>
        <field>TwilioSF__Last_Message_Status_Date__c</field>
        <name>Clear Last Message Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateLeadSource</fullName>
        <field>LeadSource</field>
        <literalValue>Advertisement</literalValue>
        <name>UpdateLeadSource</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateLeadStatus</fullName>
        <field>Status</field>
        <literalValue>Timed Out - 45Days</literalValue>
        <name>UpdateLeadStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Converted_Checkbox</fullName>
        <field>WasConverted__c</field>
        <literalValue>1</literalValue>
        <name>Update Converted Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_d2subd2</fullName>
        <field>d2subd2__c</field>
        <formula>NOW()- Assigned_Date_Time__c</formula>
        <name>Update Lead d2subd2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Violation_Flag</fullName>
        <field>Lead_Time_Violation__c</field>
        <literalValue>0</literalValue>
        <name>Update Violation Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS AIRD to NA Air RT conversion</fullName>
        <actions>
            <name>CTS_Global_RT_to_NA_Air</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Switch RT back to NA Air when the owner is changed to a non-community user</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( $RecordType.DeveloperName = &apos;AIRD_Lead&apos;, ISCHANGED(OwnerId), TEXT(Owner:User.UserType) = &apos;Standard&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Lead Address Fields</fullName>
        <actions>
            <name>CTS_Global_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Global_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Global_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_Global_Zip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global: Sets the value of the custom address fields to map to Accounts on conversion.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( OR( $RecordType.DeveloperName = &apos;NA_Air&apos;, $RecordType.DeveloperName = &apos;CTS_New_Lead&apos;, $RecordType.DeveloperName = &apos;CTS_NIST_Marketing_Lead&apos;, $RecordType.DeveloperName = &apos;AIRD_Lead&apos;), OR( ISNEW(), ISCHANGED( Street ), ISCHANGED( State ), ISCHANGED( PostalCode ), ISCHANGED( City ), ISCHANGED( Country )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update RT to NA Air</fullName>
        <actions>
            <name>CTS_Global_RT_to_NA_Air</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; RecordType.DeveloperName = &apos;CTS_New_Lead&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Notification - IR %28Pending Lead Request%29</fullName>
        <actions>
            <name>CTS_IOT_Notification_IR_Pending_Lead_Request</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Notification - IR (Pending Lead Request)</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(ISPICKVAL(Status, &apos;Open&apos;),  Lead_Source_1__c = &apos;CTS Customer Portal&apos;, Lead_Source_2__c = &apos;New CTS Customer Portal User Request&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Received %28Lead%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Received_Lead</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Received (Lead)</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp;  ISPICKVAL(Status,&apos;New&apos;) &amp;&amp; Lead_Source_1__c == &apos;CTS Customer Portal&apos; &amp;&amp;  Lead_Source_2__c == &apos;CTS Customer Portal User Request&apos; &amp;&amp; RecordType.DeveloperName == &apos;CTS Community User Global Registration Lead&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Lead Assignment Asia Queue</fullName>
        <actions>
            <name>Send_Asia_Queue_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is triggered when Lead is assigned to Asia Queue.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( (OR( ISNEW(),  ISCHANGED(OwnerId)) ),  LEFT(OwnerId, 3) = &quot;00G&quot;,  Owner:User.Profile.Name &lt;&gt; &quot;Lead Testing&quot;,  Owner:User.LastName &lt;&gt; &quot;Migration&quot;,  Owner:User.LastName &lt;&gt; &quot;Integration&quot;,  Owner:Queue.DeveloperName == &quot;Asia_Queue&quot;,  $RecordType.DeveloperName == &quot;NA_Air&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Lead Assignment Portable Queue</fullName>
        <actions>
            <name>Send_Portable_Queue_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is triggered when Lead is assigned to Queue: Portable Compressor Leads (Doosan)</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( (OR( ISNEW(),  ISCHANGED(OwnerId)) ),  LEFT(OwnerId, 3) = &quot;00G&quot;,  Owner:User.Profile.Name &lt;&gt; &quot;Lead Testing&quot;,  Owner:User.LastName &lt;&gt; &quot;Migration&quot;,  Owner:User.LastName &lt;&gt; &quot;Integration&quot;,  Owner:Queue.DeveloperName == &quot;Portable_Compressor_Leads_Doosan&quot;,  $RecordType.DeveloperName == &quot;NA_Air&quot;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Lead Reupdate Email to Valid</fullName>
        <active>true</active>
        <description>Update email ID where it was set to invalid for &quot;No Form Confirmation&quot;  to valid</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(RecordType.DeveloperName  = &apos;New Global Lead&apos;,CTS_No_Form_Confirmation__c   = true,  Email  &lt;&gt; NUll)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reupdate_Email_ID</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CTS Lead Time Violation Email</fullName>
        <actions>
            <name>CTS_Lead_Time_Violation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( RecordType.DeveloperName = &apos;New Global Lead&apos;,  Lead_Time_Violation__c = true)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Leads assigned to Not My Lead Queue</fullName>
        <actions>
            <name>Send_Lead_notification_for_leads_in_the_Not_My_Lead_Queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email users in the Not My Lead queue when a lead owner is changed to the Not My Lead queue</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Reassign_to_Queue__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Set Converted Checkbox</fullName>
        <actions>
            <name>Update_Converted_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; OR(RecordType.Name = &apos;Hibon Lead&apos;, RecordType.Name = &apos;OEM Lead&apos;,RecordType.Name = &apos;GES Lead&apos;,RecordType.Name = &apos;New Global Lead&apos;,RecordType.Name = &apos;CTS New Lead&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS Update Country Field for Siebel</fullName>
        <actions>
            <name>CTS_Update_Lead_Country_for_Siebel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>NA Air,New Global Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>United States</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>U.S.</value>
        </criteriaItems>
        <description>Updates the Country field on the Lead from the value of &apos;United States&apos;, which comes from the OOB Google address validation, to the Siebel-friendly value of &apos;USA&apos;.  This is needed in the case the Lead is converted, resulting in a new Account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Hibon New Lead</fullName>
        <actions>
            <name>Hibon_Lead_Creation_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule fires when a new Hibon Lead is created.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; $RecordType.DeveloperName == &quot;Hibon_Lead&quot; &amp;&amp;
CC_Region_Name__c  == &quot;Americas&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LeadAssignmentEmail</fullName>
        <actions>
            <name>Send_Lead_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reset_Reassign_to_Queue_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_email_notification_sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is specific to NA CTS only.</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND( (OR( ISNEW(),  ISCHANGED(OwnerId)) ),  LEFT(OwnerId, 3) = &quot;005&quot;,  Owner:User.Profile.Name &lt;&gt; &quot;Lead Testing&quot;,  Owner:User.LastName &lt;&gt; &quot;Migration&quot;,  Owner:User.LastName &lt;&gt; &quot;Integration&quot;,   OR($RecordType.DeveloperName == &quot;NA_Air&quot;,$RecordType.DeveloperName == &quot;Standard_Lead&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Lead</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; OR( ISBLANK( SBU_ID_Workflow__c ), ISCHANGED( OwnerId ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PT CX Lead Notice</fullName>
        <actions>
            <name>PT_Send_CX_New_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND( 	RecordType.DeveloperName =&quot;PT_Lead&quot;, 	NOT(ISBLANK(PT_CX_User__c)), 	ISPICKVAL(Status,&quot;4c. Forward to CX&quot;), 	ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PT New Lead</fullName>
        <actions>
            <name>PT_Set_Lead_Source_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PT_Set_Lead_Source_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PT_Set_Lead_Source_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PT_Set_Lead_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_PT_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule fires when a new PT Lead is created.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; $RecordType.DeveloperName == &quot;PT_Lead&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SF to SF Lead ID</fullName>
        <actions>
            <name>SF_2_SF_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; SF2SF_IR_ID__c == &apos;&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Unassigned CTS Leads from PostalQ to Not My LeadQ</fullName>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>IR Comp New Lead,New Global Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>PostalQueue,CTS AssignmentQueue-Africa,CTS AssignmentQueue-APAC,CTS AssignmentQueue-Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>CTS AssignmentQueue-Latin America,CTS AssignmentQueue-Middle East</value>
        </criteriaItems>
        <description>The workflow will check to see if there are unassigned CTS leads, created prior to the current date, in the PostalQueue, and re-assign these leads to the Not My Lead Queue.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_email_notification_for_leads_being_routed_to_the_Not_My_Lead_Queue</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Move_CTS_Lead_from_Postal_to_Not_My_Lead</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Flag for Not My Lead Queue</fullName>
        <actions>
            <name>Set_Reassign_to_Queue_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Reassign to Queue? flag to true if the lead owner is the Not My Lead Queue</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; Owner:Queue.DeveloperName = &apos;Not_My_Lead&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status Update</fullName>
        <actions>
            <name>Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The workflow rule is specific to NA CTS only.</description>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND ( RecordType.DeveloperName == &quot;NA_Air&quot;, IsConverted )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Target Disposition Date</fullName>
        <actions>
            <name>Disposition_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is specific to NA CTS only; will set Disposition Date when an Unqualified Lead is dispositioned on Target.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; NOT(ISPICKVAL(Disposition_Reason__c, &apos;&apos;)) &amp;&amp; RecordType.Name = &apos;NA Air&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TwilioSF__Clear Last Message Status%2FDate</fullName>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>1!=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type</fullName>
        <actions>
            <name>Lead_Record_Type_CTS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Lead Record Type when Brand = CTS, set Lead Record Type to &apos;NA Air&apos;</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; (Brand__c =  &apos;CTS&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Lead_email_notification_sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>An email notification has been sent to the lead owner regarding lead reassignment.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Lead email notification sent.</subject>
    </tasks>
</Workflow>