<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Name_to_Code_and_Description</fullName>
        <field>Name</field>
        <formula>Nature_Code__c + &quot;/&quot; + Nature_Description__c</formula>
        <name>Set Name to Code and Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Name</fullName>
        <actions>
            <name>Set_Name_to_Code_and_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
