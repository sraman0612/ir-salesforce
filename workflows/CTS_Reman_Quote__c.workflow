<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CTS_Reman_set_Quote_Name</fullName>
        <field>Name</field>
        <formula>Quote_Rename__c</formula>
        <name>CTS Reman set Quote Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CTS Reman set Quote Name</fullName>
        <actions>
            <name>CTS_Reman_set_Quote_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CTS_Reman_Quote__c.Name</field>
            <operation>equals</operation>
            <value>Temp</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
