<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CC_Approved_Tax_Cert_Notification</fullName>
        <description>Approved Tax Cert Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/CC_Approved_Tax_Certificate_Notice</template>
    </alerts>
    <alerts>
        <fullName>New_Record_Notification</fullName>
        <ccEmails>NA_MDMCustomerRequests@irco.com</ccEmails>
        <description>New Record Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>veronica_brown@clubcar.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/CC_New_Record_added</template>
    </alerts>
    <rules>
        <fullName>CC_Approved Tax Certificate Notification</fullName>
        <actions>
            <name>CC_Approved_Tax_Cert_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Tax_Certificate__c.CC_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Tax_Certificate__c.CC_State__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Tax_Certificate__c.Tax_Suffix__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Club Car Tax Certificates approval notice.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CC_New Record email Notification</fullName>
        <actions>
            <name>New_Record_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Clubcar Tax Certificates- new upload.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>CC_Tax_Certificate__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
