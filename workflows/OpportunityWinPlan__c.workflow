<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IRSPX_OWP_Name_Generation</fullName>
        <description>Creates a more user friendly Win Plan Name for searching, rather than using the autonumber format.</description>
        <field>Name</field>
        <formula>&apos;OWP - &apos; &amp;  Opportunity__r.Account.Name &amp; &apos; - &apos; &amp;  TEXT( TODAY() )</formula>
        <name>IRSPX OWP Name Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Win_Plan_Creator_s_Email_Update</fullName>
        <description>Updates the field with the email address for the win plan creator. The address is used for sending notifications to the win plan creator when sales mangers provide feedback on the win plan</description>
        <field>IRSPX_RS_Creator_s_Email__c</field>
        <formula>CreatedBy.Email</formula>
        <name>Win Plan Creator&apos;s Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>IRSPX Opportunity Win Plan Name Generation</fullName>
        <actions>
            <name>IRSPX_OWP_Name_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Name &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>