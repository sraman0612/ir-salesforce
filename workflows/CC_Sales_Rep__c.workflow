<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Sales_Rep_Name</fullName>
        <description>This field update sets the value of the .name field of the record.</description>
        <field>Name</field>
        <formula>CC_Sales_Person_Number__c + &quot;/&quot; + CC_Sales_Rep__r.FirstName + &quot; &quot; + CC_Sales_Rep__r.LastName</formula>
        <name>Update Sales Rep Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sales Rep Create%2FEdit</fullName>
        <actions>
            <name>Update_Sales_Rep_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Sales_Rep__c.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule fires every time a Sales Rep record is created or edited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
