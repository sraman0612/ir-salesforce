<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_summary_key_update</fullName>
        <field>CC_Account_Summary_Key__c</field>
        <formula>Contract__r.Account.CC_Siebel_ID__c + &apos;_&apos; +  TEXT(Contract__r.CC_Sub_Type__c) + &apos;_&apos; + TEXT(CC_Co_Op_Year__c)</formula>
        <name>Account summary key update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CoOp unique key for funds and year</fullName>
        <actions>
            <name>Account_summary_key_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>CC_Co_Op_Account_Summary__c.IsActive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Co_Op_Account_Summary__c.IsActive__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
