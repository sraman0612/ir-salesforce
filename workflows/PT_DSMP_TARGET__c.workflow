<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PT_DSMP_FU_DSMPTarget_ExtId</fullName>
        <field>Ext_ID__c</field>
        <formula>PT_DSMP__r.PT_DSMP_Year__r.Name &amp;&quot;-&quot;&amp; PT_DSMP__r.Account__r.PT_BO_Customer_Number__c &amp;&quot;-&quot;&amp; TEXT(Month__c)</formula>
        <name>PT_DSMP_FU_DSMPTarget_ExtId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_DSMP_FU_DSMPTarget_Name</fullName>
        <field>Name</field>
        <formula>PT_DSMP__r.PT_DSMP_Year__r.Name &amp;&quot;-&quot;&amp; PT_DSMP__r.Account__r.PT_BO_Customer_Number__c &amp;&quot;-&quot;&amp; TEXT(Month__c)</formula>
        <name>PT_DSMP_FU_DSMPTarget_Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PT_DSMP_WF_DSMPTarget_ExtId</fullName>
        <actions>
            <name>PT_DSMP_FU_DSMPTarget_ExtId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PT_DSMP_FU_DSMPTarget_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
