<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Lease_Team_on_Final_Approval_Rejection</fullName>
        <description>Notify Lease Team on Final Approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Lease_Approve_Reject_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Sales_on_Final_Lease_Approval_Rejection</fullName>
        <description>Notify Sales on Final Lease Approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Rep_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Lease_Approve_Reject_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Sales_on_Final_New_Car_Lease_Approval_Rejection</fullName>
        <description>Notify Sales on Final New Car Lease Approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep_VP__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Club_Car/Lease_Approve_Reject_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Approval_Process_Name_to_Charity</fullName>
        <field>Approval_Process_Name__c</field>
        <formula>&quot;Charity_Lease_Approval&quot;</formula>
        <name>Set Approval Process Name to Charity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_Name_to_New_Car</fullName>
        <field>Approval_Process_Name__c</field>
        <formula>&quot;New_Car_Lease_Approval&quot;</formula>
        <name>Set Approval Process Name to New Car</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_Name_to_Non_Revenue</fullName>
        <field>Approval_Process_Name__c</field>
        <formula>&quot;Non_Revenue_Lease_Approval&quot;</formula>
        <name>Set Approval Process Name to Non-Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_Name_to_Revenue</fullName>
        <field>Approval_Process_Name__c</field>
        <formula>&quot;Revenue_Lease_Approval&quot;</formula>
        <name>Set Approval Process Name to Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Not_Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Not Yet Submitted</literalValue>
        <name>Set Approval Status to Not Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Recalled</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Set Approval Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Routing</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Routing</literalValue>
        <name>Set Approval Status to Routing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Submission_Date</fullName>
        <field>Approval_Submission_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Approval Submission Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lease_Key</fullName>
        <field>LEASEKEY__c</field>
        <formula>Lease_Year_Code__c + Lease_Category_Code__c + Lease_Type_Code__c + STL_String_Num__c + Unique_Key_Test_Helper__c</formula>
        <name>Set Lease Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lease_Num</fullName>
        <field>Name</field>
        <formula>Lease_Year_Code__c + Lease_Category_Code__c + Lease_Type_Code__c + STL_String_Num__c</formula>
        <name>Set Lease Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payments_for_Monthly_Frequency</fullName>
        <field>Num_of_Payments__c</field>
        <formula>Number_of_Days__c / 30</formula>
        <name>Set # Payments for Monthly Frequency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Payments_for_Terms_Frequency</fullName>
        <field>Num_of_Payments__c</field>
        <formula>1</formula>
        <name>Set # Payments for Terms Frequency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_Contact_Name</fullName>
        <field>Shipping_Contact_Name__c</field>
        <formula>Shipping_Contact__r.FirstName &amp; &quot; &quot; &amp; Shipping_Contact__r.LastName</formula>
        <name>Set Shipping Contact Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Shipping_Contact_Phone</fullName>
        <field>Shipping_Contact_Phone__c</field>
        <formula>Shipping_Contact__r.Phone</formula>
        <name>Set Shipping Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Approval Status on Margin Change</fullName>
        <actions>
            <name>Set_Approval_Status_to_Not_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TEXT(Approval_Status__c) = &quot;Approved&quot; &amp;&amp;   (    	ISCHANGED(Total_Car_Cost__c) || 	ISCHANGED(Total_Car_Internal_Cost__c) ||       	ISCHANGED(Additional_Discount__c ) || 	ISCHANGED(One_time_Charge__c) ||    	ISCHANGED(Adjustments_and_Prepping_Fees__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set %23 Payments for Monthly Frequency</fullName>
        <actions>
            <name>Set_Payments_for_Monthly_Frequency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISNEW() || ISCHANGED( Num_of_Payments__c ) || ISCHANGED( Invoice_Frequency__c ) || ISCHANGED(Number_of_Days__c)) &amp;&amp; TEXT(Invoice_Frequency__c) = &quot;Monthly&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set %23 Payments for Terms Frequency</fullName>
        <actions>
            <name>Set_Payments_for_Terms_Frequency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Short_Term_Lease__c.Invoice_Frequency__c</field>
            <operation>equals</operation>
            <value>Terms</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Lease Number</fullName>
        <actions>
            <name>Set_Lease_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Lease_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 	ISNEW(), 	AND( 		NOT(Synced_to_MAPICS__c), 		OR( 			ISCHANGED(Lease_Type__c), 			ISCHANGED(Lease_Category__c) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Shipping Contact Info</fullName>
        <actions>
            <name>Set_Shipping_Contact_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Shipping_Contact_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Required for transportation request quick actions since the case relationship limit is maxed out.</description>
        <formula>(ISNEW() &amp;&amp; !ISBLANK(Shipping_Contact__c)) || (ISCHANGED(Shipping_Contact__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
