<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_FSL_Update_Labor_Cost_on_Labor_Record</fullName>
        <field>Labor_Cost__c</field>
        <formula>IF(
	ISBLANK(Employee2__r.CC_FS_Labor_Cost__c),
	95.55*Labor_Hours__c,
	Employee2__r.CC_FS_Labor_Cost__c*Labor_Hours__c
)</formula>
        <name>CC FSL Update Labor Cost on Labor Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC FSL Update Labor Cost on Labor Record</fullName>
        <actions>
            <name>CC_FSL_Update_Labor_Cost_on_Labor_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	RecordType.DeveloperName=&quot;Regular_Hours&quot;, 	OR( 		ISNEW(), 		ISCHANGED(Employee2__c), 		ISCHANGED(Labor_Hours__c) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
