<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sends_manager_feedback_to_the_Account_Owner_of_the_Account_Plan</fullName>
        <description>Sends manager feedback to the Account Owner of the Account Plan</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Plan_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/IRSPX_Account_Plan_Manager_Feedback</template>
    </alerts>
    <rules>
        <fullName>IRSPX Account Plan Send Manager Feedback</fullName>
        <actions>
            <name>Sends_manager_feedback_to_the_Account_Owner_of_the_Account_Plan</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Plan_Manager_Feedback__c.SalesLeaderManagerFeedback__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends manager feedback for the Account Plan to the Account Plan owner.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
