<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>RSHVAC_Upd_Opty_Amount_Quote_Amount_Upd</fullName>
        <description>RSHVAC_Upd_Opty_Amount_Quote_Amount_Upd</description>
        <field>Amount</field>
        <formula>Expected_Revenue__c</formula>
        <name>RSHVAC_Upd_Opty_Amount_Quote_Amount_Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>RSHVAC_Update_Opty_Amount_If_Quote_Amount_IsUpdated</fullName>
        <actions>
            <name>RSHVAC_Upd_Opty_Amount_Quote_Amount_Upd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>RSHVAC_Quote__c.Expected_Revenue__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.QuoteCount_PendingPricingRev__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>equals</operation>
            <value>Integration</value>
        </criteriaItems>
        <description>RSHVAC_Update_Opty_Amount_If_Quote_Amount_IsUpdated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
