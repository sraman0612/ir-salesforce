<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Opportunity_Sales_Call_Plan_Feedback_Notification</fullName>
        <description>Send Opportunity Sales Call Plan Feedback Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/IRSPX_Opportunity_Sales_Call_Plan_Manager_Feedback</template>
    </alerts>
    <rules>
        <fullName>IRSPX Sales Call Plan Send Manager Feedback</fullName>
        <actions>
            <name>Send_Opportunity_Sales_Call_Plan_Feedback_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Sends an email notification to the opportunity owner when a sales leader provides feedback on a sales call plan.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IRSPX_OSCP_Sales_Leader_Manager_Feedback__c &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>