<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TK_Options</fullName>
        <description>Copy Options from Opportunity Product to Quote Line items</description>
        <field>TK_Options__c</field>
        <formula>IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;220V Cable/Plug&quot;), &quot;220V Cable/Plug;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Australia Plug&quot;), &quot;Australia Plug;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Dual Voltage Transformer&quot;), &quot;Dual Voltage Transformer;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Electronic Chart Recorder&quot;), &quot;Electronic Chart Recorder;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Metal Fan Hub&quot;), &quot;Metal Fan Hub;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;RMM Release&quot;), &quot;RMM Release;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;RMM+&quot;), &quot;RMM+;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Seal Cap&quot;), &quot;Seal Cap;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Smart PTI&quot;), &quot;Smart PTI;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Sync&quot;), &quot;Sync;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Top Air Flow&quot;), &quot;Top Air Flow;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;USDA Sensor&quot;), &quot;USDA Sensor;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Vacuum Valve&quot;), &quot;Vacuum Valve;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;V-bar&quot;), &quot;V-bar;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Water Cool&quot;), &quot;Water Cool;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;80 GAL Tank&quot;), &quot;80 GAL Tank;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;230V&quot;), &quot;230V;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;ECO&quot;), &quot;ECO;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Fuel Sensor&quot;), &quot;Fuel Sensor;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Fuel Strainer&quot;), &quot;Fuel Strainer;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Toyo Frame&quot;), &quot;Toyo Frame;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Crowley Spec&quot;), &quot;Crowley Spec;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;CM Arm&quot;), &quot;CM Arm;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;SM Arm&quot;), &quot;SM Arm;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Clamp Mount&quot;), &quot;Clamp Mount;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Header Pin&quot;), &quot;Header Pin;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Dual Mount&quot;), &quot;Dual Mount;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;Emerson Telematics&quot;), &quot;Emerson Telematics;&quot; + BR() , NULL) +

IF(INCLUDES( OpportunityLineItem.TKM_Options__c , &quot;TracKing&quot;), &quot;TracKing&quot;, NULL)</formula>
        <name>Options</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TK_Other_Options</fullName>
        <description>Copy Other Options from Opportunity Products to Quote line Items</description>
        <field>TKM_Options_c__c</field>
        <formula>OpportunityLineItem.TKM_Other_Options__c</formula>
        <name>Other Options</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TK-Copy Options and Other Options</fullName>
        <actions>
            <name>TK_Options</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TK_Other_Options</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Thermo King Marine</value>
        </criteriaItems>
        <description>TK-Copy Options and Other Options from Opportunity Products to Quote line items</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
