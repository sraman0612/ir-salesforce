<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Add_Setup_Cost</fullName>
        <field>Setup_Cost__c</field>
        <formula>IF(TEXT(Short_Term_Lease__r.Car_Type__c) = &quot;New&quot;, 0, Item__r.CC_STL_Setup_Cost__c)</formula>
        <name>Add Setup Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Default_Base_Cost</fullName>
        <field>Base_Cost__c</field>
        <formula>Item__r.CC_STL_Cost__c</formula>
        <name>Set Default Base Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_STL_Car_Internal_Cost</fullName>
        <description>Total Line item for the Car</description>
        <field>Internal_Cost__c</field>
        <formula>(IF(!ISBLANK(Base_Cost__c), Base_Cost__c, 0) * IF(!ISBLANK(Short_Term_Lease__r.Number_of_Days__c), Short_Term_Lease__r.Number_of_Days__c, 0) * Quantity__c) +  
IF(ISBLANK(Setup_Cost__c ), 0, (Setup_Cost__c * Quantity__c)) +
IF(ISBLANK(Accessory_Labor_Cost__c), 0, Accessory_Labor_Cost__c) + 
IF(ISBLANK(Cost_Adjustment__c), 0, Cost_Adjustment__c)</formula>
        <name>Set STL Car Internal Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Total_Car_Cost</fullName>
        <field>Total_Cost__c</field>
        <formula>IF(!ISBLANK(Per_Car_Cost__c), Per_Car_Cost__c , 0) *  
Quantity__c *  
IF(!ISBLANK(Short_Term_Lease__r.Num_of_Payments__c), Short_Term_Lease__r.Num_of_Payments__c, 0)</formula>
        <name>Set Total Car Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Item Added or Changed</fullName>
        <actions>
            <name>Add_Setup_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	NOT(ISBLANK(Item__c)), 	OR( 		ISNEW(), 		ISCHANGED(Item__c) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Default Base Cost</fullName>
        <actions>
            <name>Set_Default_Base_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_STL_Car_Info__c.Base_Cost__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set STL Car Internal Cost</fullName>
        <actions>
            <name>Set_STL_Car_Internal_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.IR_API_Bypass__c.CC_STL_Integration_Bypass__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Total Car Cost</fullName>
        <actions>
            <name>Set_Total_Car_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This sets the Total Cost field for STL rollup</description>
        <formula>NOT($Setup.IR_API_Bypass__c.CC_STL_Integration_Bypass__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
