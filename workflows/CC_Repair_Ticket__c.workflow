<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OOS_Level_Set_Notify</fullName>
        <ccEmails>cams@gpsindustries.com</ccEmails>
        <description>OOS Level Set Notify</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Club_Car_GPSi/OOS_Level_Notify</template>
    </alerts>
    <fieldUpdates>
        <fullName>Model_Number_Created</fullName>
        <field>Model_Number_at_Check_In__c</field>
        <formula>Model_Number__c</formula>
        <name>Model Number Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Repair_Date_Add</fullName>
        <description>Updates the actual Repair Date when technician is selected from list if the date is blank.</description>
        <field>Repair_Date__c</field>
        <formula>Today()</formula>
        <name>Repair Date - Add</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Repair_Date_Clear</fullName>
        <description>Clears the Repair Date if the technician is changed to none.</description>
        <field>Repair_Date__c</field>
        <name>Repair Date - Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Repair_Ticket_Update_Case_RMA_Date_Rec_d</fullName>
        <field>CC_GPSI_RMA_Received_Date__c</field>
        <formula>Today()</formula>
        <name>Repair Ticket Update Case RMA Date Rec&apos;d</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Model Number when Created</fullName>
        <actions>
            <name>Model_Number_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Model_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OOS Level Notification</fullName>
        <actions>
            <name>OOS_Level_Set_Notify</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Out_Of_Scope_Level__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Out_of_Scope_Status__c</field>
            <operation>notEqual</operation>
            <value>Pre-Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Repair Date - Add Rule</fullName>
        <actions>
            <name>Repair_Date_Add</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Technician__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Repair_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>The actual date repair was performed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Repair Date - Clear Rule</fullName>
        <actions>
            <name>Repair_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Technician__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.Repair_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Remove the date if technician is set to None</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Repair Ticket - Creation Actions</fullName>
        <actions>
            <name>Repair_Ticket_Update_Case_RMA_Date_Rec_d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CC_Repair_Ticket__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>12/31/1999 10:00 PM</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
