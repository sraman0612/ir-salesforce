<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Rotary_Pricing_HP</fullName>
        <field>Rotary_Pricing_HP__c</field>
        <formula>Rated_HP__c</formula>
        <name>Update Rotary Pricing HP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Rotary Pricing HP</fullName>
        <actions>
            <name>Update_Rotary_Pricing_HP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( ISCHANGED( Type__c ), ISCHANGED( Rated_HP__c )), TEXT(Type__c) &lt;&gt; &apos;Centrifugal&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
