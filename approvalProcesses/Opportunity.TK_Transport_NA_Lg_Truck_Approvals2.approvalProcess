<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>true</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Account</field>
        <field>Name</field>
        <field>Owner</field>
        <field>TK_ProductFamily__c</field>
        <field>TKM_Model__c</field>
        <field>TK_TotalSellingPrice__c</field>
        <field>TK_DeviationAmount__c</field>
        <field>TK_DeviationPercent__c</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Large Truck / Heater Product Family, the opportunity is routed to the user&apos;s manager if the Deviation % is between 7-10%</description>
        <entryCriteria>
            <formula>TK_DeviationPercent__c &gt; $User.TK_Large_Truck_Approval_Level__c</formula>
        </entryCriteria>
        <ifCriteriaNotMet>ApproveRecord</ifCriteriaNotMet>
        <label>TK Transport - Large Truck Level 1</label>
        <name>TK_Transport_Large_Truck_Deviation_7_10</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Large Truck / Heater Product Family, the opportunity is routed to the user&apos;s manager if the Deviation % is between 10-25%</description>
        <entryCriteria>
            <formula>AND( 
TK_DeviationPercent__c &gt; 0.1, 
TK_DeviationPercent__c &gt; $User.TK_Large_Truck_Approval_Level__c 
)</formula>
        </entryCriteria>
        <ifCriteriaNotMet>ApproveRecord</ifCriteriaNotMet>
        <label>TK Transport - Large Truck Level 2</label>
        <name>TK_Transport_Large_Truck_Deviation_10_25</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Large Truck / Heater Product Family, the opportunity is routed to the user&apos;s manager if the Deviation % is between 25-35%</description>
        <entryCriteria>
            <formula>AND( 
TK_DeviationPercent__c &gt; 0.25, 
TK_DeviationPercent__c &gt; $User.TK_Large_Truck_Approval_Level__c 
)</formula>
        </entryCriteria>
        <ifCriteriaNotMet>ApproveRecord</ifCriteriaNotMet>
        <label>TK Transport - Large Truck Level 3</label>
        <name>TK_Transport_Large_Truck_Deviation_25_35</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Large Truck / Heater Product Family, the opportunity is routed to the user&apos;s manager if the Deviation % is greater than 35%</description>
        <entryCriteria>
            <formula>AND( 
TK_DeviationPercent__c &gt; 0.35, 
TK_DeviationPercent__c &gt; $User.TK_Large_Truck_Approval_Level__c 
)</formula>
        </entryCriteria>
        <label>TK Transport - Large Truck Level 4</label>
        <name>TK_Transport_Large_Truck_Deviation_35</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Base approval process for the TK Transport business group, Large Truck and Heater Product Families, using opportunity record type and Deviation % as criteria</description>
    <emailTemplate>unfiled$public/TK_Transport_Opportunity_Approval</emailTemplate>
    <enableMobileDeviceAccess>false</enableMobileDeviceAccess>
    <entryCriteria>
        <criteriaItems>
            <field>Opportunity.RecordType</field>
            <operation>equals</operation>
            <value>TK Transport NA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Propose,Propose/Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.TK_TotalSellingPrice__c</field>
            <operation>notEqual</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.TK_ProductFamily__c</field>
            <operation>equals</operation>
            <value>,Large Truck,HEATER,TRUCK LARGE</value>
        </criteriaItems>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>Set_SPR_Expiration_Date</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>TK_SPR_Approval_Date</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>TK_SPR_Approved_Status</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>true</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>TK_SPR_Rejected_Status</name>
            <type>FieldUpdate</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>TK_SPR_Approval_Status</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>TK_SPR_Submitted_Date</name>
            <type>FieldUpdate</type>
        </action>
    </initialSubmissionActions>
    <label>TK Transport NA Lg Truck Approvals</label>
    <nextAutomatedApprover>
        <useApproverFieldOfRecordOwner>false</useApproverFieldOfRecordOwner>
        <userHierarchyField>Manager</userHierarchyField>
    </nextAutomatedApprover>
    <recallActions>
        <action>
            <name>TK_SPR_Recalled_Status</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOnly</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
