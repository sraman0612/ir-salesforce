public with sharing class addServiceEngineerController {
    
    public String size { get; set;}
    //This is Our collection of the class/wrapper objects WrapperServiceWrapper
    Public List < WrapperServiceWrapper > wrapperlist;
    Public Integer noOfRecords {get;set;}
    Map < id, Service_Engineer__c > selectedSEMap = new Map < id, Service_Engineer__c > ();
    public boolean display {get;set;}
    public list < Service_Engineer__c > selectedSEList {get;set;}
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController SetSE {
        get {
            if (SetSE == Null) {
                SetSE = new ApexPages.StandardSetController(Database.getQueryLocator([Select id, name, email__c from Service_Engineer__c order by name]));
                // sets the number of records in each page set
                setSE.setpagesize(10);
                noOfRecords = setSE.getResultSize();
            }
            return SetSE;
        }
        set;
    }
    
   //Returns a list of wrapper objects for the sObjects in the current page set
    Public List < WrapperServiceWrapper > getSE() {
       getSelectedSE();
        // Initilaize the list to add the selected contact
        wrapperlist = new List < WrapperServiceWrapper > ();
        for (Service_Engineer__c cc: (List < Service_Engineer__c > ) SetSE.getRecords()) {
            if (SelectedSEMap.ContainsKey(cc.id)) {
                wrapperlist.add(new WrapperServiceWrapper(cc, true));
            } else {
                wrapperlist.add(new WrapperServiceWrapper(cc, false));
            }
        }
        return wrapperlist;
    }

    public void getSelectedSE() {
        String accountId = ApexPages.currentPage().getParameters().get('id');
        if (wrapperlist != null) {
            for (WrapperServiceWrapper wr: wrapperlist) {
                if (wr.bool == true) {
                    SelectedSEMap.put(wr.se.id, wr.se); 
                } else {
                    SelectedSEMap.remove(wr.se.id); 
                }
            }
        }
    }

     public pageReference addSelected() {
        display = true;
        getSelectedSE();
        selectedSEList = SelectedSEMap.values();
        List < Service_Engineer__c > lstaddSelected = new List < Service_Engineer__c > ();
        List < Service_Engineer__c > lstToUpdate = new List < Service_Engineer__c > ();
        String accountId = ApexPages.currentPage().getParameters().get('id');
        if (selectedSEList!= null && !selectedSEList.isEmpty()) {
            for (Service_Engineer__c s: selectedSEList) {s.Account__c = accountId;lstToUpdate.add(s);}
        }
        try {
            update lstToUpdate;
        } catch (Exception e) {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));}
        PageReference page = new PageReference('/' + accountId);
        return page;
    }
    
     public pageReference removeSelected() {
        display = true;
        getSelectedSE();
        selectedSEList = SelectedSEMap.values();
        List < Service_Engineer__c > lstaddSelected = new List < Service_Engineer__c > ();
        List < Service_Engineer__c > lstToUpdate = new List < Service_Engineer__c > ();
        String accountId = ApexPages.currentPage().getParameters().get('id');
        if (selectedSEList!= null && !selectedSEList.isEmpty()) {
            for (Service_Engineer__c s: selectedSEList) {s.Account__c = NULL;lstToUpdate.add(s);}
        }
        try {
            update lstToUpdate;
        } catch (Exception e) {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));}
        PageReference page = new PageReference('/' + accountId);
        return page;
    }
    

    public integer pageNumber {
        get {return SetSE.getPageNumber();}
        set;
    }

    Public class WrapperServiceWrapper
    {
        Public Service_Engineer__c se {get;set;}
        Public boolean bool {get;set;}
        public WrapperServiceWrapper(Service_Engineer__c s, boolean bool) {
            this.se = s;
            this.bool = bool;
        }
    }

    public PageReference cancel() {
        String accountId = ApexPages.currentPage().getParameters().get('id');
        PageReference page = new PageReference('/' + accountId);
        return page.setRedirect(true);
    }
}