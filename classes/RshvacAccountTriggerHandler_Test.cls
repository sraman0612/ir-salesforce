@isTest
private class RshvacAccountTriggerHandler_Test {

    // Name of different districts to set as Default Branch
    private static final String FIRST_NAME = 'Thomas';
    private static final String LAST_NAME = 'Pynchon';
    private static final String USERNAME = 'tpynchon@nowhere.com';
    private static final String NICKNAME = 'TPynch';
    private static final String LOCALE = 'en_US';
    private static final String TIMEZONE = 'America/New_York';
    private static final String ENCODING = 'ISO-8859-1';
    private static final String PROFILE_NAME = 'RHVAC Sales';
    private static final String SALES_PERSON_ID = '4242424242';
    private static final String SALES_PERSON_2_ID = '1212121212';
    private static final String COLUMBIA = 'USR Columbia DSO';
    private static final String PHOENIX = 'USR Phoenix DSO';
    // This map will contain the monthly percentage for each district
    private static Map<String, List<Decimal>> DISTRICT_VALUES = new Map<String, List<Decimal>>{
            COLUMBIA => new List<Decimal>{0.08, 0.07, 0.09, 0.06, 0.08, 0.07, 0.09, 0.06, 0.1, 0.1, 0.1, 0.1}, 
            PHOENIX => new List<Decimal>{0.1, 0.1, 0.1, 0.1, 0.08, 0.07, 0.09, 0.06, 0.08, 0.07, 0.09, 0.06}};
    // RS_HVAC Record Type Id
    private static Id RS_HVAC_RT_ID = RshvacAccountTriggerHandler.RS_HVAC_RT_ID;
    // Account Types
    private static final String DEALER = RshvacAccountTriggerHandler.DEALER;
    // Account Status
    private static final String POTENTIAL = RshvacAccountTriggerHandler.POTENTIAL;
    private static final String CUSTOMER = RshvacAccountTriggerHandler.CUSTOMER;
    private static final String PROSPECT = 'Prospect';
    // Cost Center Channels
    private static final String DSO = 'DSO';
    private static final String IWD = 'IWD';
    // IWD Cost Center Name
    private static final String IWD_COST_CENTER = 'IWD Cost Center';
    private static final String IWD_COST_CENTER_NEW = 'IWD Cost Center New';
    
    /**
     *  Sets the data need for the tests
     */
    @testSetup static void mySetup() {
       
    // Get the Profile Id to assign to the test User
        Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME LIMIT 1].Id;
    // Insert a new User, it'll initially have the FIRST_SP_ID as Sales Person Id
    User testUser = new User(Sales_Person_Id__c = SALES_PERSON_ID, FirstName = FIRST_NAME, LastName = LAST_NAME, ProfileId = profileId,
                              UserName = USERNAME, Email = USERNAME, CommunityNickname = NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                              TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = NICKNAME);
        insert testUser;
        
        List<RS_District_Seasonal_Schedule__c> schedules = new List<RS_District_Seasonal_Schedule__c>();
        List<Cost_Center__c> costCenters = new List<Cost_Center__c>();
        
        for (String district : DISTRICT_VALUES.keySet()){
            Decimal[] values = DISTRICT_VALUES.get(district);
            schedules.add(new RS_District_Seasonal_Schedule__c(Name = district, District__c = district,
                            January__c = values[0], February__c = values[1], March__c = values[2], April__c = values[3], 
                            May__c = values[4], June__c = values[5], July__c = values[6], August__c = values[7], 
                            September__c = values[8], October__c = values[9], November__c = values[10], December__c = values[11]));
            costCenters.add(new Cost_Center__c(Name=district, Default_Branch__c=district, Channel__c=DSO, ownerId = testuser.id));
        }
        costCenters.add(new Cost_Center__c(Name=IWD_COST_CENTER, Default_Branch__c=IWD_COST_CENTER, Channel__c=IWD, ownerId = testuser.id));
        costCenters.add(new Cost_Center__c(Name=IWD_COST_CENTER_NEW, Default_Branch__c=IWD_COST_CENTER_NEW, Channel__c=IWD, ownerId = testuser.id));
        insert schedules;
        insert costCenters;
    }
    

    /**
     *  Tests that the logic on the insert trigger is correct
     */
    static testMethod void testDsoInsertAccount() {
        List<Account> accounts = new List<Account>();
        for (String district : DISTRICT_VALUES.keySet()){
            accounts.add(new Account(Name = district, Default_Branch__c = COLUMBIA, Sales_Person_Id__c = SALES_PERSON_ID,
                                    RecordTypeId = RS_HVAC_RT_ID, Type = DEALER, Status__c = CUSTOMER));
        }
        Test.startTest();
            insert accounts;
        Test.stopTest();
        List<Opportunity> opps = [SELECT Id,OwnerId, Account.OwnerId FROM Opportunity WHERE AccountId IN :accounts];
        System.assertEquals(2, opps.size());
        System.assertEquals(opps[0].OwnerId, opps[0].Account.OwnerId );
    }

    /**
     *  Tests that the logic on the update trigger is correct
     */
    static testMethod void testDsoUpdateAccount() {
        List<Account> accounts = new List<Account>();
        for (String district : DISTRICT_VALUES.keySet()){
            accounts.add(new Account(Name = district,Default_Branch__c = COLUMBIA, Sales_Person_Id__c = SALES_PERSON_ID,
                                    RecordTypeId = RS_HVAC_RT_ID, Type = DEALER));
        }
        insert accounts;
        // For both of the accounts no opportunity should be created
        List<Opportunity> opps = [SELECT Id FROM Opportunity WHERE AccountId IN :accounts];
        System.assertEquals(0, opps.size());
        Test.startTest();
            for (Account a : accounts){
                a.Status__c = POTENTIAL;
            }
            update accounts;
            for (Account a : accounts){
                a.Status__c = CUSTOMER;
            }
            update accounts;
        Test.stopTest();
        // After the update both Accounts should have an Opportunity related to them
        opps = [SELECT Id FROM Opportunity WHERE AccountId IN :accounts];
        System.assertEquals(accounts.size(), opps.size());
        
        User testUser = [Select id from User where Sales_Person_Id__c = :SALES_PERSON_ID];
        testuser.sales_person_Id__c = SALES_PERSON_2_ID;
        update testuser;
        
        accounts[0].sales_person_Id__c = SALES_PERSON_2_ID;
        update accounts[0];
    }
    
    static testMethod void testIwdAccount() {
        Account iwdAccount =new Account(Name = IWD_COST_CENTER,Default_Branch__c = IWD_COST_CENTER, Status__c = PROSPECT, Madis_Number__c = 'xyz',
                                    RecordTypeId = RS_HVAC_RT_ID, Type = DEALER);
        Test.startTest();
          insert iwdAccount;
          iwdAccount = [SELECT Cost_Center_Name__c from Account where Id = :iwdAccount.Id];
          Opportunity[] opps = [SELECT Id FROM Opportunity WHERE AccountId = :iwdAccount.Id];
          Id costCenterId = [Select id from Cost_Center__c where Default_Branch__c = :IWD_COST_CENTER].Id;
          Id newCostCenterId = [Select id from Cost_Center__c where Default_Branch__c = :IWD_COST_CENTER_NEW].Id;
          System.assertEquals(0, opps.size());
          System.assertEquals(costCenterId, iwdAccount.Cost_Center_Name__c);
          iwdAccount.Default_Branch__c = '';
          update iwdAccount;
          // Case 11219 - 9/23/2017 - Ronnie Stegall - Accounts can now be assigned to a cost center without being setup in Oracle
          // (which means they will not have a default branch).  RshvacAccountTriggerHandler was updated to allow an account with a 
          // null default branch to be manually assigned to a cost center.
          //iwdAccount = [SELECT Cost_Center_Name__c from Account where Id = :iwdAccount.Id];
          //System.assertEquals(iwdAccount.Cost_Center_Name__c, null);
          iwdAccount.Default_Branch__c = IWD_COST_CENTER_NEW;
          update iwdAccount;
          iwdAccount = [SELECT Cost_Center_Name__c from Account where Id = :iwdAccount.Id];
          System.assertEquals(iwdAccount.Cost_Center_Name__c, newCostCenterId);
          
          Account iwdAccount1 =new Account(Name = IWD_COST_CENTER,Default_Branch__c = IWD_COST_CENTER, Status__c = PROSPECT, 
                                    RecordTypeId = RS_HVAC_RT_ID, Type = DEALER);
          insert iwdAccount1; 
          iwdAccount1.MADIS_Number__c = 'xyz';
          update iwdAccount1;
          iwdAccount1 = [Select id, status__c from Account where id = :iwdAccount1.id];  
          System.assertEquals(iwdAccount1.Status__c, POTENTIAL);
                                 
        Test.stopTest();
      }
}