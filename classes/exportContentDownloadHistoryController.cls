public class exportContentDownloadHistoryController{
    public List<ContentVersionHistory> cvhList{get;set;}
    public exportContentDownloadHistoryController()
    {
      cvhList = new List<ContentVersionHistory>();
      for (ContentVersionHistory cvh : [select ContentVersionId,ContentVersion.VersionNumber,CreatedById, CreatedDate, Field, Id 
                                        from ContentVersionHistory 
                                        Where Field= 'contentVersionDownloaded']){
           cvhList.add(cvh);
      }
    }
}