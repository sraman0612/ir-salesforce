global class CC_ContractController {
    /* member variables */
    Public list<CC_Coop_Claims__c> lstCoopClaimdetails{get;set;} 
    public List<Contract> listContract{get;set;}
    public List<Integer> setYear {set; get; }
    public List<String> setAgreementType {set; get; }
    public Integer year{get;set;}
    public String invoiceNum{get;set;}
    public String agreementType{get;set;}
    public Integer selectedYear{get;set;}
    public String selectedMAT{get;set;}
    public Boolean isNoRecord{get;set;}
    public Boolean isReload{get;set;}
    public Double fundStartingAmount{get;set;}
    public Double availableAmount{get;set;}
    public Map<Integer,List<String>> aggYearMap{get;set;}
    public Id CoopId; 
    /* constructor */

    public CC_ContractController(CC_PavilionTemplateController controller){
        try{
            
            isNoRecord = true;
            isReload = false;  
            setYear = new List<Integer>();
            List<Integer> setYear1= new List<Integer>();
            List<Integer> reversed = new List<Integer>();
            setAgreementType = new List<String>();
            String accid1 = [select Account.Id from user where id=:UserInfo.getUserId()][0].Account.Id;
            listContract = new List<Contract>();
            aggYearMap = new Map<Integer,List<String>>();
            lstCoopClaimdetails=new list<CC_Coop_Claims__c>();
            List<CC_Co_Op_Account_Summary__c> accSummaryall = [select Id,   Contract__r.CC_Sub_Type__c,    CC_Co_Op_Year__c from CC_Co_Op_Account_Summary__c where Contract__r.AccountId=:accid1 AND Contract__r.CC_Type__c = :'Dealer/Distributor Agreement' and Contract__r.CC_Contract_End_Date__c >= :system.today()];
            Integer currentYear = System.today().year();
            Integer startYear = Integer.valueOf(Label.Pavilion_Coop_Start_Year);
            System.debug('^^^^^^^^^^^^^ in CC_ContractController'+accid1+ listContract);
            Set<Id> contractIds = new Set<Id>();
            Set<Integer> yearsSet = new Set<Integer>();
            Set<String> aggType = new Set<String>();
            if(!accSummaryall.isEmpty()){
                for(CC_Co_Op_Account_Summary__c c: accSummaryall){
                    yearsSet.add(Integer.valueOf(c.CC_Co_Op_Year__c));
                    aggType.add(c.Contract__r.CC_Sub_Type__c);
                    /*if(aggYearMap.get( Integer.valueOf(c.CC_Co_Op_Year__c )) != null){
                        aggYearMap.get(Integer.valueOf( c.CC_Co_Op_Year__c )).add(c.Contract__r.CC_Sub_Type__c);
                    }else{
                        List<String> t = new List<String>();
                        t.add(c.Contract__r.CC_Sub_Type__c);
                        aggYearMap.put( Integer.valueOf(c.CC_Co_Op_Year__c) , t);
                    }*/
                }
                System.debug('@@aggYearMap = '+aggYearMap);
                 
            }
            System.debug('^^^^^^^^^^^contracts Ids list = '+contractIds);
            if(!yearsSet.isEmpty())
               // Changes made by Ashutosh - sort the year in reverse order Ticket #8719031
                setYear1.addAll(yearsSet);
                setYear1.sort();
                for(Integer i = setYear1.size() - 1; i >= 0; i--){
                     reversed.add(setYear1[i]);
                 }
                setYear.addAll(reversed);
            if(!aggType.isEmpty())
                setAgreementType.addAll(aggType);
        }Catch(Exception ex){}
    }
    
    /* List of SelectOptions for AgreementType */
    /*
    public List<SelectOption> getAgreementTypes(){
    List<SelectOption> options = new List<SelectOption>();
      try
      {
        
        if(setAgreementType!=null && setAgreementType.size()>0){
            options.add(new SelectOption('','--None--'));
            options.add(new SelectOption('All Agreements','All Agreements'));
            for(String objType: setAgreementType) {
                options.add(new SelectOption(objType,objType));
            }    
        }
          
      }
    Catch(Exception ex)
    {
        
    }         
      
      return options;  
    }*/
    
    /* List of SelectOptions for years */
    
    public List<SelectOption> getYears(){
     List<SelectOption> options = new List<SelectOption>(); 
      try
      {
              
        if(!setYear.isEmpty()){
            options.add(new SelectOption('0','--None--'));
            for(Integer objYear: setYear){
                options.add(new SelectOption(String.valueOf(objYear),String.valueOf(objYear)));
            }    
        } 
          
       }
       Catch(Exception ex)
      {
        
      }  
       return options; 
    }
    /* 
    @RemoteAction
    global static String getAggrementTypes(string selectedYear)
    {
        String accid1 = [select Account.Id from user where id=:UserInfo.getUserId()][0].Account.Id;
        String aggList ='';
        Integer currentYear = System.today().year();
        for(CC_Co_Op_Account_Summary__c accSumm : [select   Contract__r.CC_Sub_Type__c from CC_Co_Op_Account_Summary__c where Contract__r.AccountId=:accid1 AND Contract__r.CC_Type__c = :'Dealer/Distributor Agreement' and Contract__r.CC_Contract_End_Date__c >= :system.today()  AND CC_Co_Op_Year__c =: currentYear]){
            aggList = aggList + accSumm.Contract__r.CC_Sub_Type__c +';';
        }
          return aggList;
    } */
    
    
    /* remote action to display listContract  */
    
    @RemoteAction
    global static String refreshTableBasedOnYear(string selectedYear , String selectedMAT)
    {
        String accid1 = [select Account.Id from user where id=:UserInfo.getUserId()][0].Account.Id;
        //System.debug('^^^^^^^^refreshTableBasedOnYear cass');
        Double totalRegisterdUnits = 0.0; 
        Double fundStartingAmount = 0.0;
        Double totalCreditAmount = 0.0;
        Double availableAmount = 0.0;
        Integer selectedYearInt = 0;
        selectedYearInt = Integer.valueOf(selectedYear);
        List<CC_Co_Op_Account_Summary__c> accSumm = new List<CC_Co_Op_Account_Summary__c>();
         if(selectedYear != '' && selectedMAT == 'All Agreements'){
            accSumm = [SELECT Id, Contract__r.CC_Sub_Type__c, Contract__r.StartDate,CC_Co_Op_Year__c,CC_Registered_Units__c, CC_Initial_Funds__c, CC_Available_Funds__c,CC_Incremental_Funds__c, CC_Pending_Claims__c FROM CC_Co_Op_Account_Summary__c Where Contract__r.CC_Type__c = 'Dealer/Distributor Agreement'   AND Contract__r.AccountId = :accid1 and Contract__r.CC_Contract_End_Date__c >= :system.today() and CC_Co_Op_Year__c =:selectedYearInt];
        }else if(selectedYear !='' && selectedMAT != ''){
            accSumm = [SELECT Id, Contract__r.CC_Sub_Type__c,Contract__r.StartDate, CC_Co_Op_Year__c, CC_Registered_Units__c,CC_Initial_Funds__c, CC_Available_Funds__c,CC_Incremental_Funds__c, CC_Pending_Claims__c FROM CC_Co_Op_Account_Summary__c Where Contract__r.CC_Type__c = 'Dealer/Distributor Agreement' AND CC_Co_Op_Year__c= :Integer.valueOf(selectedYear) AND Contract__r.AccountId = :accid1 AND  Contract__r.CC_Sub_Type__c=:selectedMAT and Contract__r.CC_Contract_End_Date__c >= :system.today() and CC_Co_Op_Year__c =:selectedYearInt];
        } 
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeNumberField('totalRegisterdUnits', Integer.valueOf(totalRegisterdUnits));
        gen.writeNumberField('fundStartingAmount', Integer.valueOf(fundStartingAmount));
        gen.writeNumberField('availableAmount', Integer.valueOf(availableAmount));
        gen.writeEndObject();
        String extraJsonData =  gen.getAsString();
        if(!accSumm.isEmpty()) 
        {
             //String jsonData = extraJsonData + '||' + (String)JSON.serialize(listContract);
             String jsonData = extraJsonData + '||' + (String)JSON.serialize(accSumm);
             
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
    
    

    
    @RemoteAction
    global static String refreshCOOPTable(String accSummaryId,String selectedYear , String selectedMAT){
        String accid1 = [select Account.Id from user where id=:UserInfo.getUserId()][0].Account.Id;
        Set<String> ponumbers= new Set<String>();
         List<CC_Order_Shipment__c> invLinkList = new  List<CC_Order_Shipment__c>();
        Integer year1 = Integer.ValueOf(selectedYear);
        List<CC_Co_Op_Account_Summary__c> accSumm = new List<CC_Co_Op_Account_Summary__c>();
        List<CC_Coop_Claims__c> coopList = new List<CC_Coop_Claims__c>();
        List<String> accSummIdList = new List<String>();
        List<String> statusList = new List<String>();
        statusList.add('Approved');
        statusList.add('Paid');
        statusList.add('Unpaid');
        if(selectedYear != '' && selectedMAT == 'All Agreements'){
             accSumm =  [SELECT Id, Contract__r.CC_Sub_Type__c, CC_Co_Op_Year__c,(SELECT  Id,  Name,  CC_Submit_Date__c, CC_Status__c, CC_Credit_Amount__c, Comment__c,Club_Car_Order__r.CC_Customer_Invoice_Number__c,New_Claim_Amount__c,Rejection_Code__c  from CC_Coop_Claims__r) FROM CC_Co_Op_Account_Summary__c  Where Contract__r.CC_Type__c =: 'Dealer/Distributor Agreement' AND CC_Co_Op_Year__c=:year1     AND Contract__r.AccountId =:accid1 ];//PriyaP7NOv 2018 RT 8043665.Added rejection code
             
             
        }else if(selectedYear !='' && selectedMAT != ''){
            accSumm  = [SELECT Id, Contract__r.CC_Sub_Type__c, CC_Co_Op_Year__c,  (SELECT Id, Name, CC_Submit_Date__c, CC_Status__c,CC_Credit_Amount__c,Comment__c,New_Claim_Amount__c,Rejection_Code__c from CC_Coop_Claims__r) FROM CC_Co_Op_Account_Summary__c Where Contract__r.CC_Type__c =: 'Dealer/Distributor Agreement' AND CC_Co_Op_Year__c=:year1   AND Contract__r.CC_Sub_Type__c =: selectedMAT AND Contract__r.AccountId =:accid1];//PriyaP7NOv 2018 RT 8043665.Added rejection code
        }
        Set<String> tmpCoopList = new Set<String> ();
        for(CC_Co_Op_Account_Summary__c acS :accSumm) {
            tmpCoopList.add(acS.Id);
            for(CC_Coop_Claims__c cc : acS.CC_Coop_Claims__r){
            
                if(cc.Name.countMatches('INT-') > 0){
                    //tmpCoopList.add(cc.Name.remove('INT-'));
                    ponumbers.add(cc.Name);
                }else{
                    ponumbers.add('INT-'+cc.Name);
                    //tmpCoopList.add(cc.Name);
                }
            }
        }
        accSummIdList.addAll(tmpCoopList);
        System.debug('@@accSummIdList = '+accSummIdList+tmpCoopList );
        coopList = [SELECT  Id,  Name,  CC_Submit_Date__c,  CC_Status__c, CC_Credit_Amount__c, Comment__c,Club_Car_Order__r.CC_Customer_Invoice_Number__c, (Select Comments From ProcessSteps Order by SystemModstamp desc Limit 1)  from CC_Coop_Claims__c  where  Co_Op_Account_Summary__c IN :accSummIdList];
        System.debug('@@ponumbers = '+ponumbers);
        System.debug('@@ponumbers = '+coopList);
        List<String> tempList = new List<String> ();
        tempList.addAll(ponumbers);
        invLinkList = [SELECT Id,  Invoice_Number__c , CC_Invoice_Key__c , Invoice_Date__c , Order__r.PO__c , Order__r.CC_Customer_Number__c   FROM  CC_Order_Shipment__c  where  Order__r.RecordType.Name =:'Co-Op ordering' AND  Order__r.PO__c  IN : tempList]; 
        System.debug('@@invLinkList = '+invLinkList);
        String maping ='';
        String temp ='';
        for(CC_Order_Shipment__c c: invLinkList){
            temp =(String)c.Order__r.CC_Customer_Number__c  +'_'+(String)c.Invoice_Number__c+'_'+ c.Invoice_Date__c;
            maping  = maping +  (String)c.Order__r.PO__c+':'+(String)c.Invoice_Number__c+':'+(String)temp+';';
        }
        System.debug('@@maping = '+maping);
        String jsonData ='';
        if(accSummaryId != null && accSummaryId!= ''){
            JSONGenerator gen = JSON.createGenerator(true);
            String extraJsonData =  gen.getAsString();
            if(!accSumm.isEmpty()) {
                  jsonData = extraJsonData + '||' + (String)JSON.serialize(accSumm) + '||'+(String)JSON.serialize(invLinkList)+ '||'+(String)JSON.serialize(coopList);
            }
            else {
                jsonData = 'No data found';
            }
        }else {
            jsonData = 'No data found';
        }
        return jsonData;
    }
}