@isTest
private class fileImportExportTest {
    @testSetup static void setup() {
        List<Account> accList = CTS_TestDataFactory.createLatamAccounts(1);
        List<Opportunity> opptyList = CTS_TestDataFactory.createLatamOppoty(1, accList[0]);
        //commented by CG as part of descoped object
        //List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = CTS_TestDataFactory.createCentacPartQuote(1, opptyList[0]);
        /*List<Centac_RFQ_to_Fill__c>centacPartsList = new List<Centac_RFQ_to_Fill__c>();
        for(integer i=0;i<6;i++){
            Centac_RFQ_to_Fill__c centacPartObj = new Centac_RFQ_to_Fill__c();
            //centacPartObj.Centac_Parts_Quote__c = centacPartQuoteList[0].Id;
            centacPartObj.Quantity__c = 1;
            centacPartObj.Description__c = 'Test';
            centacPartObj.CCN__c = '1223';
            centacPartObj.Location_in_Compressor__c = 'Test';
            centacPartObj.Drawing_or_PN__c = 'test12323';
            centacPartObj.Serial__c = 'test12323';
            centacPartObj.Notes__c = 'Test';
            centacPartsList.add(centacPartObj);
        }
        insert centacPartsList;*/
    }
    @isTest static void testPartInsert() {
        //List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = [SELECT Id FROM CTS_Centac_Parts_Quote_ReRate__c];
        List<Account> accList = CTS_TestDataFactory.createLatamAccounts(1);
        Test.startTest();
        //commented by CG as part of descoped object
        
        /*List<Centac_RFQ_to_Fill__c>centacPartsList = new List<Centac_RFQ_to_Fill__c>();
        Centac_RFQ_to_Fill__c centacPartObj = new Centac_RFQ_to_Fill__c();
        centacPartObj.Quantity__c = 1;
        centacPartObj.Centac_Parts_Quote__c = centacPartQuoteList[0].Id;
        centacPartObj.Description__c = 'Test';
        centacPartObj.CCN__c = '1223';
        centacPartObj.Location_in_Compressor__c = 'Test';
        centacPartObj.Drawing_or_PN__c = 'test12323';
        centacPartObj.Serial__c = 'test12323';
        centacPartObj.Notes__c = 'Test';
        centacPartsList.add(centacPartObj);*/
        //String jsonString = JSON.serialize(centacPartsList);
        String jsonString = 'jsonString';
        fileImportExport.insertData(jsonString,  accList[0].Id);
        //List<Centac_RFQ_to_Fill__c > newCentacPartList = [SELECT Id FROM Centac_RFQ_to_Fill__c where Centac_Parts_Quote__c =: centacPartQuoteList[0].Id];
        //system.assertEquals(newCentacPartList.size(),7 );
        Test.stopTest();
        
    }
    @isTest static void testPartUpdate() {
        //List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = [SELECT Id FROM CTS_Centac_Parts_Quote_ReRate__c];
        Test.startTest();
        //commented by CG as part of descoped object
        
        //List<Centac_RFQ_to_Fill__c>centacPartList = new List<Centac_RFQ_to_Fill__c>();
        //System.assertEquals(centacPartList.size(), 0);
        //fileImportExport.DataTableWrapper wrapperObj =fileImportExport.fetchRFQToFill(centacPartQuoteList[0].Id);
        //centacPartList = wrapperObj.dataList;
        //System.assertEquals(centacPartList.size(), 6);
        Test.stopTest();
        
    }
    
    
    
}