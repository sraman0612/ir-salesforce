public class CC_PavilionInventoryCnt {
  /* member variables */
  public List < CC_Vehicle__c > lstuniqueVehicle {get;set;}
  public List < CC_Vehicle__c > SelectedVehicle {get;set;}
  public Map < string, integer > mpforTotal {get;set;}
  public String commentmessage{get;set;}
  public String repEmail {get;set;}
  
  /* constructor */
  public CC_PavilionInventoryCnt(CC_PavilionTemplateController controller) {
    lstuniqueVehicle = new List < CC_Vehicle__c > ();
    SelectedVehicle = new List < CC_Vehicle__c > ();
    mpforTotal = new Map < string, integer > ();
    MAP<String,String>  productlineMarkMap = new Map<String,String>();
    Map<String,ProductLineToMarketType__c> ProductLineToMArketType = ProductLineToMarketType__c.getAll();
    list<String> plM = new list<String>();
    plM.addAll(ProductLineToMArketType.keyset());
    for(String m:plM){
       productlineMarkMap.put(m.toLowerCase(),ProductLineToMArketType.get(m).Agreement_Type__c.toLowerCase());
    }
    set<String> agreeentTypes = new set<String>();
    for(Contract c:[SELECT CC_Sub_Type__c FROM Contract WHERE Accountid =:controller.acctId AND CC_Contract_End_Date__c > :Datetime.NOW()]){
        agreeentTypes.add(c.CC_Sub_Type__c.toLowerCase());
    }
    for (CC_Vehicle__c v : [SELECT Id, CC_Accessories__c, CC_Manufacturing_Date__c, CC_Serial_Number__c, CC_S_Number__c, 
                            CC_Short_Description__c, CC_Year__c,Product_Line__c
                            FROM CC_Vehicle__c]){
      String carkey = v.CC_Short_Description__c + v.CC_Year__c;
      if(mpforTotal.containsKey(carKey)){
        Integer Total = mpforTotal.get(carKey);
        mpforTotal.put(carKey, Total + 1);
      } else {
        mpforTotal.put(carKey, 1);
        Boolean isItTrue =false;
        if(v.Product_Line__c != null){
        String prodline = productlineMarkMap.get(v.Product_Line__c.toLowerCase())!=null?productlineMarkMap.get(v.Product_Line__c.toLowerCase()):'';
        list<String> prodlinelist = prodline.split(';');
        for(String s:prodlinelist){
            if(agreeentTypes.contains(s.toLowerCase().trim()))
                isItTrue = true;
             }
        }
        if(isItTrue)
        lstuniqueVehicle.add(v);
      }
    }
    CC_My_Team__c [] myTeamLst = [Select Displayed_Email__c from CC_My_Team__c where Account__c = :controller.acctId and My_Team_Role__c = 'Utility Sales Representative' LIMIT 1];
    repEmail = myTeamLst.isEmpty() ? '' : myTeamLst[0].Displayed_Email__c;
  }
  
  @remoteaction
  public Static List<CC_Vehicle__c> getLibraryData(String modelYearStr){
    String year = modelYearStr.right(2);
    string model = modelYearStr.remove(year);
    List<CC_Vehicle__c> selectedVehicle=[SELECT Id, CC_Accessories__c, CC_Manufacturing_Date__c, CC_Serial_Number__c, CC_S_Number__c, CC_Short_Description__c, CC_Year__c,Product_Line__c
                                           FROM CC_Vehicle__c
                                          WHERE CC_Short_Description__c = :model and CC_Year__c = :year];
    return selectedVehicle;
  }
}