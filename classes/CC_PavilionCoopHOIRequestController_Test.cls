@isTest 
public class CC_PavilionCoopHOIRequestController_Test
{
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    static testmethod void PavilionHOIRequest()
    {
        // Creation of Test Data
        Account Acc = TestUtilityClass.createAccount();
        insert Acc;
        System.assertEquals('Sample Account',Acc.Name);
        System.assertNotEquals(null, Acc.Id);
        Contact con = TestUtilityClass.createContact(Acc.Id);
        insert con;
        system.debug('the contact created is'+con.Id);
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(Acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
        // PavilionSettings__c records are created to handle User Events trigger
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User portaUser = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert portaUser;
        System.assertEquals(portaUser.ContactId,con.Id);
        System.assertNotEquals(false, portaUser.IsActive);
        system.runAs(portaUser)
        {
            system.debug('-----------user-----------');
            contract cont= TestUtilityClass.createContract(Acc.Id);
            insert cont;
            CC_Co_Op_Account_Summary__c ccas = TestUtilityClass.createCoopAccountSummary(cont.Id);
            insert ccas;
            CC_PavilionCoopHOIRequestController objReq = new CC_PavilionCoopHOIRequestController();//Creating instance for test data.
            List<CC_Coop_Claims__c> lstCoop_Claims = new List<CC_Coop_Claims__c>();
            CC_Coop_Claims__c objCoopClaim = new CC_Coop_Claims__c();
            //objCoopClaim.Contract__c = cont.id;
            objCoopClaim.Co_Op_Account_Summary__c=ccas.Id;
            objCoopClaim.Complete_Name_of_Tournament__c = 'TestTournament';
            objCoopClaim.Course_Name__c = 'TestCourse';
            objCoopClaim.Type_of_pre_approval_Request__c='Hole-In-One'; 
            objCoopClaim.Course_Address__c = 'clucar01';
            objCoopClaim.City__c = 'NewJersey';
            objCoopClaim.State_Province__c = 'alabama';
            objCoopClaim.Country__c = 'USA';
            objCoopClaim.Date_of_the_first_day_of_Tournament__c =date.today() ;
            objCoopClaim.Vehicle__c = '7080';
            objCoopClaim.Number_of_days_vehicle_will_be_offered__c = '2';
            objCoopClaim.Day__c = '20';
            objCoopClaim.Pro_Am__c =  'test';
            objCoopClaim.Players__c ='10';
            objCoopClaim.par__c = '3';
            objCoopClaim.hole__c = '2';
            objCoopClaim.round__c = '2';
            objCoopClaim.Yards__c = '110';
            lstCoop_Claims.add(objCoopClaim);               
            insert lstCoop_Claims ; 
            CC_PavilionCoopHOIRequestController.getCoopHOIRequestJson(Acc.id);
            CC_PavilionCoopHOIRequestController.getCoopHOIRequestJson(null);
        }
    }
}