/*****************************************************************************
Name:       RS_CRS_CustAssetsListControllerTest
Purpose:    Test class for RS_CRS_CustAssetsListController. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Ashish Takke    02/05/2017  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_CustAssetsListControllerTest {
    
    /******************************************************************************
	Method Name : fetchCustAssetsList_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing fetchCustAssetsList Method
	******************************************************************************/
    Static testmethod void fetchCustAssetsList_Test(){
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        //Create test user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type);
        
        //Getting Asset Record Type Id through Schema.Describe Class
        Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        // This code runs as the system user
        System.runAs(userRec){
            //insert test Account
            Account acc = new Account();
            acc.recordTypeId = accRecId;
            acc.LastName = 'Test Account 1';
            acc.Phone = '123456789';
            acc.Email_ID__c = 'first.last@email.com';
            insert acc;        
            
            //insert test case
            Case caseObj = new Case();
            caseObj.RecordTypeId = caseRecId;
            caseObj.Status = 'Customer Action';
            insert caseObj;
            
            //Test case when there is no Account associated with Case
            RS_CRS_CustAssetsListController.fetchCustAssetsList(caseObj.Id, false, false, 0);
            
            caseObj.AccountId = acc.Id;
            update caseObj;
            
            //Test case when there is no Asset associated with Account 
            RS_CRS_CustAssetsListController.fetchCustAssetsList(caseObj.Id, false, false, 0);
            
            List<Asset> assetList = new List<Asset>();
            for(Integer i=0; i<=10; i++){
                Asset assetRec = new Asset();
                assetRec.recordTypeId = assetRecId;
                assetRec.Name = 'Test Asset' + i;
                assetRec.SerialNumber = '12345' + i;
                assetRec.accountId = acc.Id;
                assetRec.Manufacturer__c = 'Trane';
                
                assetList.add(assetRec);
            }
            insert assetList;
            
            //Test case when there is Asset associated with Account 
            RS_CRS_CustAssetsListController.fetchCustAssetsList(caseObj.Id, false, false, 0);
            
            //Test case when user clicks Next button 
            RS_CRS_CustAssetsListController.fetchCustAssetsList(caseObj.Id, true, false, 6);
            
            //Test case when user clicks Previous button
            RS_CRS_CustAssetsListController.fetchCustAssetsList(caseObj.Id, false, true, 6);
        }
    }
}