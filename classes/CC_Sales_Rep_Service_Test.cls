@isTest
public with sharing class CC_Sales_Rep_Service_Test {
    
    @TestSetup
    private static void createData(){	
    	
    	// Create users to use in the sales hiearchy
    	User salesRepUser1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser1.Alias = 'alias121'; 
    	
    	User salesRepUser2 = TestUtilityClass.createUser('CC_Sales Rep', null); // No manager will be assigned to this rep
    	salesRepUser2.Alias = 'alias122';    	
    	
    	User salesRepMgr = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr.Alias = 'alias123';
    	
    	User salesRepDir = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir.Alias = 'alias124';
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    
    	
    	insert new User[]{salesRepUser1, salesRepUser2, salesRepMgr, salesRepDir, salesRepVP};	
    	
    	salesRepUser1.ManagerId = salesRepMgr.Id;
    	salesRepMgr.ManagerId = salesRepDir.Id;
    	salesRepDir.ManagerId = salesRepVP.Id;
    	
    	update new User[]{salesRepUser1, salesRepMgr, salesRepDir};
    	
    	// Create a sales rep that has a manager, director, and VP in the hierarchy
    	CC_Sales_Rep__c salesRep1 = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser1.Id);
    	
    	// Sales rep that doesn't have a manager
    	CC_Sales_Rep__c salesRep2 = TestUtilityClass.createSalesRep('12345_abc124', salesRepUser2.Id);
    	
    	insert new CC_Sales_Rep__c[]{salesRep1, salesRep2};
    }   
    
    @isTest
    private static void testLookupSalesHiearchy(){
    	
    	CC_Sales_Rep__c[] salesReps = [Select Id, CC_Sales_Rep__r.Alias From CC_Sales_Rep__c];
    	system.assertEquals(2, salesReps.size());
    	
    	Set<Id> salesRepIds = new Set<Id>();
    	
    	for (CC_Sales_Rep__c salesRep : salesReps){
    		salesRepIds.add(salesRep.Id);    		
    	}
    	
    	Map<Id, CC_Sales_Rep_Service.SalesHierarchy> salesHierarchyMap;
    	
    	Test.startTest();
    	
    	salesHierarchyMap = CC_Sales_Rep_Service.lookupSalesHiearchy(salesRepIds);
    	
    	Test.stopTest();
    	
    	// Verify the hierarchy results
    	Map<Id, User> userMap = new Map<Id, User>([Select Id, ManagerId, Manager.ManagerId, Manager.Manager.ManagerId From User Where Id = :salesHierarchyMap.values()[0].salesRep.Id or Id = :salesHierarchyMap.values()[1].salesRep.Id]);
    	
    	for (Id salesRepId : salesHierarchyMap.keySet()){
    		
    		CC_Sales_Rep_Service.SalesHierarchy salesHierarchy = salesHierarchyMap.get(salesRepId);
    		
    		if (salesHierarchy.salesRep.Alias == 'alias121'){
    			system.assertEquals(userMap.get(salesHierarchy.salesRep.Id).ManagerId, salesHierarchy.salesManager.Id);
    			system.assertEquals(userMap.get(salesHierarchy.salesRep.Id).Manager.ManagerId, salesHierarchy.salesDirector.Id);
    			system.assertEquals(userMap.get(salesHierarchy.salesRep.Id).Manager.Manager.ManagerId, salesHierarchy.salesVP.Id);
    		}
    		else if (salesHierarchy.salesRep.Alias == 'alias122'){
    			system.assertEquals(null, salesHierarchy.salesManager);
    			system.assertEquals(null, salesHierarchy.salesDirector);
    			system.assertEquals(null, salesHierarchy.salesVP);    			
    		}
    		else{
    			system.assertEquals(true, false, 'Sales rep with expected alias not found.');
    		}
    	}
    } 
}