@isTest
public with sharing class CC_Approval_Override_BatchTest {
    
    @TestSetup
    private static void createData(){

    	TestDataUtility dataUtility = new TestDataUtility();
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;    	

        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
        batchControls.Mgr_Approval_Override_Supported_Objects__c = 'CC_Order__c,CC_Short_Term_Lease__c';
        upsert batchControls;
        
        CC_Short_Term_Lease_Settings__c leaseSettings = CC_Short_Term_Lease_Settings__c.getOrgDefaults();
        leaseSettings.Restricted_Finance_Approval_Roles__c = 'CC_Finance_Leadership,CC_Finance_Team';
        insert leaseSettings;
    	
    	// Create users to use in the sales hiearchy
    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'xyz123';

    	User salesRepUser = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser.Alias = 'alias122';
    	
    	User salesRepMgr = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr.Alias = 'alias123';
    	salesRepMgr.PS_Purchasing_LOA__c = 50000;
    	
    	User salesRepDir = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir.Alias = 'alias124';
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    
    	    	
    	User casUser = TestUtilityClass.createUser('CC_General Admin', null); 	
    	casUser.LastName = 'CASTestUser';
    	
    	insert new User[]{salesRepUser, salesRepMgr, salesRepDir, salesRepVP, casUser, adminUser};
    	
    	salesRepUser.ManagerId = salesRepMgr.Id;
    	salesRepMgr.ManagerId = salesRepDir.Id;
    	salesRepDir.ManagerId = salesRepVP.Id;
    	
    	update new User[]{salesRepUser, salesRepMgr, salesRepDir};    	
    	
    	User financeUser;
    	
    	// Avoid mixed DML since role will be assigned to the finance user
    	system.runAs(adminUser){	
    	
    		UserRole leaseRole = [Select Id From UserRole Where DeveloperName = 'CC_Aftermarket_Team' LIMIT 1];
    	
	    	User leaseUser = TestUtilityClass.createUser('CC_Pavilion Admin', null);
	    	leaseUser.FirstName = 'Lease1';
	    	leaseUser.LastName = 'User';
	    	leaseUser.Alias = 'xyz124';  
	    	leaseUser.UserRoleId = leaseRole.Id;
    	
	        financeUser = TestUtilityClass.createUser('CC_General Admin', null);
	        financeUser.LastName = 'FinanceTestUser';    	
	        financeUser.UserRoleId = [Select Id From UserRole Where DeveloperName = 'CC_Finance_Team' LIMIT 1].Id;

	    	insert new User[]{financeUser, leaseUser};
	    	
    		// Assign the lease permission set to the lease user
			PermissionSet leasePermSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Leases'];	    	
	    	
			PermissionSetAssignment permSetAssign = new PermissionSetAssignment(
				AssigneeId = leaseUser.Id,
				PermissionSetId = leasePermSet.Id
			);    	
			
			insert permSetAssign;	    	
    	}
    	
    	// Create a sales rep that has a manager, director, and VP in the hierarchy
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser.Id);
    	insert salesRep;

        Set<String> objectsConfigured = new Set<String>();

        for (CC_Approval_Column__mdt approvalColumn : [Select Object_API_Name__c From CC_Approval_Column__mdt]){
            objectsConfigured.add(approvalColumn.Object_API_Name__c);
        }

        if (objectsConfigured.contains('CC_Short_Term_Lease__c')){

  	        // Create Customer Accounts 
            Account acct1 = TestUtilityClass.createAccount2();
            acct1.BillingCountry = 'USA';
            acct1.ShippingCountry = 'USA';
            insert acct1;
            
            CC_Order__c order1 = TestUtilityClass.createNatAcctOrder(acct1.Id, acct1.Id);
            insert order1;
            
            // Create Master Lease 
            CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
            
            // Assign new Account to Master Lease
            ml1.Customer__c = acct1.Id;
            insert ml1;
            
            CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
            
            for (Integer i = 0; i < 5; i++){
                
                CC_Short_Term_Lease__c lease = TestDataUtility.createShortTermLease();
                lease.Master_Lease__c = ml1.Id;
        	    lease.Sales_Rep__c = salesRep.Id;
                lease.Sales_Rep_User__c = salesRepUser.Id;                
                leases.add(lease);
            } 	
            
            insert leases;   

            // Submit the leases for approval
            Approval.ProcessSubmitRequest[] approvalRequests = new Approval.ProcessSubmitRequest[]{};

            for (CC_Short_Term_Lease__c lease : leases){

                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setSkipEntryCriteria(true);
                approvalRequest.setObjectId(lease.Id);
                approvalRequest.setProcessDefinitionNameOrId('Revenue_Lease_Approval');
                approvalRequests.add(approvalRequest);                           
            }

            Approval.ProcessResult[] approvalResults = sObjectService.processApprovalRequestsAndLogErrors(approvalRequests, 'CC_Manage_Approvals_ControllerTest', 'createData');
            system.debug('approvalResults: ' + approvalResults);
        }

        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }

    @isTest 
    private static void testBatchFailure(){

        Test.startTest();
        Boolean failure = false;

        try{
            CC_Approval_Override_Batch batch = new CC_Approval_Override_Batch(null); 
            Database.executeBatch(batch);
        }
        catch(Exception e){
            failure = true;
        }

        system.assertEquals(true, failure);

        Test.stopTest();
    }

    @isTest 
    private static void testBatchSuccess(){

        ProcessInstanceWorkitem piw1 = [Select Id, ProcessInstanceId, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId, ProcessInstance.ProcessDefinition.Name From ProcessInstanceWorkitem LIMIT 1];

        CC_Manage_Approvals_Controller.submitManagerApprovalOverride(piw1.ProcessInstance.TargetObjectId, piw1.Id, 'Test comments1', true);

        ProcessInstanceWorkitem piw2 = [Select Id, ProcessInstanceId, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId, ProcessInstance.ProcessDefinition.Name From ProcessInstanceWorkitem Where Id != :piw1.Id LIMIT 1];

        CC_Manage_Approvals_Controller.submitManagerApprovalOverride(piw2.ProcessInstance.TargetObjectId, piw2.Id, 'Test comments2', false);   

        system.assertEquals(2, [Select Count() From CC_Short_Term_Lease__c Where Approval_Override__c = true]);     

        Test.startTest();

        CC_Approval_Override_Batch batch = new CC_Approval_Override_Batch('CC_Short_Term_Lease__c'); 
        Database.executeBatch(batch);

        Test.stopTest();

        system.assertEquals(0, [Select Count() From CC_Short_Term_Lease__c Where Approval_Override__c = true]); 

        // Verify the approval requests are no longer there
        system.assertEquals(0, [Select Count() From ProcessInstanceWorkitem Where Id = :piw1.Id]);
        system.assertEquals(0, [Select Count() From ProcessInstanceWorkitem Where Id = :piw2.Id]);

        // Verify the approval outcomes
        system.debug([Select StepStatus, Comments From ProcessInstanceStep Where ProcessInstanceId = :piw1.ProcessInstanceId]);
        ProcessInstanceStep pis1 = [Select StepStatus, Comments From ProcessInstanceStep Where ProcessInstanceId = :piw1.ProcessInstanceId and StepStatus not in ('Started', 'Pending')];
        ProcessInstanceStep pis2 = [Select StepStatus, Comments From ProcessInstanceStep Where ProcessInstanceId = :piw2.ProcessInstanceId and StepStatus not in ('Started', 'Pending')];
        
        system.assertEquals('Approved', pis1.StepStatus);
        system.assertEquals('Rejected', pis2.StepStatus);

        system.assert(pis1.Comments.contains('Test comments1'));
        system.assert(pis1.Comments.contains(UserInfo.getName()));
        system.assert(pis1.Comments.contains(CC_Approval_Override_Batch.approvalOverrideComment));
        system.assert(pis2.Comments.contains('Test comments2'));  
        system.assert(pis2.Comments.contains(UserInfo.getName()));      
        system.assert(pis2.Comments.contains(CC_Approval_Override_Batch.approvalOverrideComment));

        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }
}