//
// (c) 2015, Appirio Inc
//
// Test class for Contact Trigger
//
// Apr 28, 2015     George Acker     original
// Sep 27, 2019    Ashwni - Increased coverage

@isTest
private class Contact_TriggerHandler_Test {
    
    // Method to test contact trigger
    static testMethod void testContactTrigger(){
        Account acc = new Account(Name = 'Test',ShippingCountry='USA',shippingState='NC',ShippingCity='Davidson',
                                  shippingStreet='800D Betty Street',ShippingPostalCode='6380',county__c='Davidson') ;
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2'; 
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();      
        insert acc;
        Id airContactRTId = [SELECT Id FROM RecordType WHERE sObjectType='Contact' and DeveloperName='Customer_contact' LIMIT 1].Id;
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c.firstname = 'George';
        c.lastname = 'Acker';
        c.phone = '1234567890';
        c.RecordTypeId=airContactRTId;
        insert c;
        
        Test.startTest();
        
        c.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
        update c;
        
        sObject[] sObjQuery = [SELECT Id FROM Contact WHERE Id = :c.Id];
        //System.assertEquals(0, sObjQuery.size());   
        
        Contact[] contacts = new Contact[]{};
       // for (integer i=0;i<250;i++)
        for (integer i=0;i<20;i++)
        {
            Contact c2 = new Contact();
            c2.AccountId = acc.Id;
            c2.firstname = 'George';
            c2.lastname = 'Acker';
            c2.phone = '1234567890';
            c2.RecordTypeId=airContactRTId;
            contacts.add(c2);
        }  
        insert contacts;
        update contacts;
        delete contacts;
        Test.stopTest();
    }

    static testMethod void testBusinessValidation() {
        // Insert a test user
        User u = new User();
        u.Username = 'test@test.com.hasjsa78' + String.valueOf(Math.random()*1000000);
        u.Alias = 'has' + String.valueOf(Math.random()*1000000).substring(0,4);
        u.Email = 'test@test.com';
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard Sales'].Id;
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Test';
		u.TimeZoneSidKey = 'America/Los_Angeles';
        u.Business__c = 'Low Pressure';
        insert u;

        //Set the user as the running user for the test
        System.runAs(u) {
            
            Account acc = new Account(Name = 'Test',ShippingCountry='USA',shippingState='NC',ShippingCity='Davidson',
                                      shippingStreet='800D Betty Street',ShippingPostalCode='6380',county__c='Davidson') ;
            
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();      
            insert acc;
            
            
            Business_Relationship__c testBr = new Business_Relationship__c(Account__c=acc.Id, 
                                                                           Business__c='Low Pressure', status__c = 'Inactive');
    		insert testBr;
            
            
            // Create test contact
            Contact testContact = new Contact();
            testContact.FirstName = 'Test Contact';
            testContact.LastName = 'Test Contact';
            testContact.Business__c = 'Low Pressure';
            testContact.Email = 'Test@test.com';
            testContact.Lead_Convert_Bypass__c = 'false';
            testContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId(); 
			testContact.AccountId = acc.id;
            
            
            Test.startTest();
            try{
              insert testContact;                
            }catch(Exception exc){
                system.debug('Catch Block :'+exc.getMessage());
                System.assertEquals(true, exc.getMessage().contains('Business Relationship is Inactive, Please select another Active Account.'));
            }
            Test.stopTest();
        }
    }
    
  	 static testMethod void testContactwithoutRecordtype()
	 {
		 User u = new User();
        u.Username = 'test@test.com.hasjsa78' + String.valueOf(Math.random()*1000000);
        u.Alias = 'has' + String.valueOf(Math.random()*1000000).substring(0,4);
        u.Email = 'test@test.com';
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard Sales'].Id;
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Test';
		u.TimeZoneSidKey = 'America/Los_Angeles';
        u.Business__c = 'Low Pressure';
        insert u;
         
      
       	 //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'AGS_Standard_User'];
        //insert new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = ps.Id);
		
		System.runAs(u) 
        {
            Account acc = new Account(Name = 'Test',ShippingCountry='USA',shippingState='NC',ShippingCity='Davidson',
                                      shippingStreet='800D Betty Street',ShippingPostalCode='6380',county__c='Davidson') ;
            acc.CTS_Global_Shipping_Address_1__c = '13';
            acc.CTS_Global_Shipping_Address_2__c = 'street2'; 
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();      
            insert acc;
            Id airContactRTId = [SELECT Id FROM RecordType WHERE sObjectType='Contact' and DeveloperName='GES_Contact' LIMIT 1].Id;
            Contact c = new Contact();
            c.AccountId = acc.Id;
            c.firstname = 'George';
            c.lastname = 'Acker';
            c.phone = '1234567890';
            //c.RecordTypeId= airContactRTId;
                 
            Test.startTest();
            insert c;
            CGCommonUtility.getRecordTypeNamefromMetadata('Contact');
            Test.stopTest();
            
		}
	}
    
}