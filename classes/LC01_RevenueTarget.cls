public with sharing class LC01_RevenueTarget {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Lightning controller for LC01_RevenueTarget
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	@AuraEnabled
	public static Map<String, Object> retrieveTarget(String pt_dsmp_id){
		Map<String, Object> mapResult = new Map<String, Object>();

		List<PT_DSMP_TARGET__c> lstTargetSprint11 = new List<PT_DSMP_TARGET__c>();
		//List<PT_DSMP_TARGET__c> lstTargetSprint2 = new List<PT_DSMP_TARGET__c>();
		//List<PT_DSMP_TARGET__c> lstTargetSprint3 = new List<PT_DSMP_TARGET__c>();

		List<wrapperDSMPTarget> lstTargetSprint1 = new List<wrapperDSMPTarget>();
		List<wrapperDSMPTarget> lstTargetSprint2 = new List<wrapperDSMPTarget>();
		List<wrapperDSMPTarget> lstTargetSprint3 = new List<wrapperDSMPTarget>();

		Double sprint1_Target_NPD = 0;

		List<PT_DSMP_TARGET__c> lstTargetY = new List<PT_DSMP_TARGET__c>();
		List<PT_DSMP_TARGET__c> lstTargetY_0 = new List<PT_DSMP_TARGET__c>();
		List<PT_DSMP_TARGET__c> lstTargetY_1 = new List<PT_DSMP_TARGET__c>();
		List<PT_DSMP_TARGET__c> lstTargetY_2 = new List<PT_DSMP_TARGET__c>();

		//Labels
		Map<String, String> mapMonthLabel = new Map<String, String>();
		PT_DSMP_CS_Table_Plan__c ptrt = PT_DSMP_CS_Table_Plan__c.getOrgDefaults();
		System.debug('mgr ptrt ' + ptrt);
		mapResult.put('labels', ptrt);

		mapMonthLabel.put('1', ptrt.Month_1__c);
		mapMonthLabel.put('2', ptrt.Month_2__c);
		mapMonthLabel.put('3', ptrt.Month_3__c);
		mapMonthLabel.put('4', ptrt.Month_4__c);
		mapMonthLabel.put('5', ptrt.Month_5__c);
		mapMonthLabel.put('6', ptrt.Month_6__c);
		mapMonthLabel.put('7', ptrt.Month_7__c);
		mapMonthLabel.put('8', ptrt.Month_8__c);
		mapMonthLabel.put('9', ptrt.Month_9__c);
		mapMonthLabel.put('10', ptrt.Month_10__c);
		mapMonthLabel.put('11', ptrt.Month_11__c);
		mapMonthLabel.put('12', ptrt.Month_12__c);

		System.debug('mgr mapMonthLabel ' + mapMonthLabel);

		if(String.isBlank(ptrt.TablePlan_Warning_1__c) && 
		   String.isBlank(ptrt.TablePlan_Warning_2__c) && 
		   String.isBlank(ptrt.TablePlan_Warning_3__c) && 
		   String.isBlank(ptrt.TablePlan_Warning_4__c) ){
		 	
		 	mapResult.put('displayWarning', false);

		} else {

		 	mapResult.put('displayWarning', true);
		}

		for(Integer i=0; i<12; i++){
			lstTargetY_1.add(new PT_DSMP_TARGET__c(Month__c = String.valueOf(i+1)));
			lstTargetY_2.add(new PT_DSMP_TARGET__c(Month__c = String.valueOf(i+1)));
		}
		
		System.debug('mgr pt_dsmp_id ' + pt_dsmp_id);

		try {

			Double totalCurrent = 0;
			Double totalLast = 0;
			Double final_target = 0;

			Boolean growth_mintarget;
			Boolean growth_base;
			Boolean strategic_prod;
			Boolean incategory;

			List<PT_DSMP__c> currentDSMP = new List<PT_DSMP__c>([SELECT Id,
																		Account__c, 
																		PT_DSMP_Year__c,
																		PT_DSMP_Year__r.Name,
																		Display_Type__c,
																		Minimum_Target__c,
																		DVP_Segment__c,
																		Total_Base__c,
																		Account__r.OwnerId,
																		Account__r.Owner.UserRoleId,
																		Account__r.PT_MIN_Target__c
																FROM PT_DSMP__c
																WHERE Id =: pt_dsmp_id]);

			if(currentDSMP.size() < 0){
				mapResult.put('error', true);
				mapResult.put('message', 'No Account found!');
				return mapResult;
			}


			Integer countStrategicProds = [SELECT COUNT()
									       FROM PT_DSMP_Strategic_Product__c
										   WHERE PT_DSMP__c =: pt_dsmp_id AND
										   		 Quantity__c != null AND
										   		 Quantity__c > 0];

			Boolean readOnly = isReadOnly(currentDSMP[0].Display_Type__c, currentDSMP[0].Account__r.Owner.UserRoleId);
			System.debug('mgr retrieve readOnly ' + readOnly );
			System.debug('mgr retrieve countStrategicProds ' + countStrategicProds );

			strategic_prod = countStrategicProds >= 3;
			incategory = currentDSMP[0].DVP_Segment__c == 'STAR' || currentDSMP[0].DVP_Segment__c == 'TURN' || currentDSMP[0].DVP_Segment__c == 'GROW';

			String accountId = currentDSMP[0].Account__c;
			String currentYear_0 = currentDSMP[0].PT_DSMP_Year__c != null ? currentDSMP[0].PT_DSMP_Year__r.Name : String.valueof(System.Today().year());
			String currentYear_1 = String.valueof( (Integer.valueOf(currentYear_0)) -1);
			String currentYear_2 = String.valueof( (Integer.valueOf(currentYear_0)) -2);

			System.debug('mgr strategic_prod ' + strategic_prod);
			System.debug('mgr currentYear_0 ' + currentYear_0);
			System.debug('mgr currentYear_1 ' + currentYear_1);
			System.debug('mgr currentYear_2 ' + currentYear_2);

			List<PT_DSMP_TARGET__c> lstTarget = new List<PT_DSMP_TARGET__c>([SELECT Id
																				  ,NPD__c
																				  ,Solution__c
																				  ,Footprint__c
																				  ,Share__c
																				  ,Base__c
																				  ,Total__c
																				  ,Month__c
																				  ,Actuals_NPD__c
																				  ,Actuals_Solution__c
																				  ,Actuals_Footprint__c
																				  ,Actuals_Share__c
																				  ,Actuals_Base__c
																				  ,Actuals__c
																			FROM PT_DSMP_TARGET__c
																			WHERE PT_DSMP__R.PT_DSMP_Year__r.Name = :currentYear_0
																			      AND PT_DSMP__r.Account__c =: accountId
																			ORDER BY Month__c]);

			lstTargetY = //new List<PT_DSMP_TARGET__c>(
				[SELECT Id
					   ,NPD__c
					   ,Solution__c
					   ,Footprint__c
					   ,Share__c
					   ,Base__c
					   ,Total__c
					   ,Month__c
					   ,PT_DSMP__R.PT_DSMP_Year__r.Name
					   ,Actuals__c
				FROM PT_DSMP_TARGET__c
				WHERE (PT_DSMP__R.PT_DSMP_Year__r.Name = :currentYear_1 OR
					   PT_DSMP__R.PT_DSMP_Year__r.Name = :currentYear_2)
				      AND PT_DSMP__r.Account__c =: accountId
				ORDER BY Month__c];

				// );

			/*if(current.PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_1)
				lstTargetY_1.add(current);
			
			if(current.PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_2)
				lstTargetY_2.add(current);*/

			System.debug('mgr lstTargetY_1 ' + lstTargetY_1);
			System.debug('mgr lstTargetY_2 ' + lstTargetY_2);
			System.debug('mgr lstTargetY.size() ' + lstTargetY.size());

			for(Integer i=0;i<lstTargetY.size();i++){
				//if(lstTargetY[i].PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_1 && lstTargetY[i].Total__c != null){
				if(lstTargetY[i].PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_1 && lstTargetY[i].Actuals__c != null){
					lstTargetY_1[Integer.valueOf(lstTargetY[i].Month__c)-1] = lstTargetY[i];
				}


				//if(lstTargetY[i].PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_2 && lstTargetY[i].Total__c != null){
				if(lstTargetY[i].PT_DSMP__R.PT_DSMP_Year__r.Name == currentYear_2 && lstTargetY[i].Actuals__c != null){
					lstTargetY_2[Integer.valueOf(lstTargetY[i].Month__c)-1] =  lstTargetY[i];
				}
			}

			for(Integer i=0; i<12; i++){

				if(lstTarget.size() >= (i+1) && Integer.valueOf(lstTarget[i].Month__c) == (i+1) ){

					if(Integer.valueOf(lstTarget[i].Month__c) >= 1 && Integer.valueOf(lstTarget[i].Month__c) <= 4){
						wrapperDSMPTarget sprint1 = new wrapperDSMPTarget();

						sprint1_Target_NPD += lstTarget[i].NPD__c != null ? lstTarget[i].NPD__c : 0 ;

						//lstTargetSprint1.add(lstTarget[i]);
						sprint1.id = lstTarget[i].Id; 
						sprint1.month =lstTarget[i].Month__c;

						if(mapMonthLabel.containsKey( lstTarget[i].Month__c )) 
							sprint1.month_name = mapMonthLabel.get( lstTarget[i].Month__c );

						sprint1.year = lstTarget[i].Total__c; 

						if(lstTarget[i].Total__c != null)
							totalCurrent += lstTarget[i].Total__c;

						//sprint1.year_1 =lstTarget[i].Id;
						//sprint1.year_2 =lstTarget[i].Id;
						
						sprint1.npd =lstTarget[i].NPD__c!=null ? lstTarget[i].NPD__c : 0; 
						sprint1.solution =lstTarget[i].Solution__c!=null ? lstTarget[i].Solution__c : 0; 
						sprint1.footprint =lstTarget[i].Footprint__c!=null ? lstTarget[i].Footprint__c : 0; 
						sprint1.share =lstTarget[i].Share__c!=null ? lstTarget[i].Share__c : 0; 
						sprint1.base =lstTarget[i].Base__c!=null ? lstTarget[i].Base__c : 0;

						sprint1.actuals_npd = lstTarget[i].Actuals_NPD__c!=null ? lstTarget[i].Actuals_NPD__c : 0;
						sprint1.actuals_solution = lstTarget[i].Actuals_Solution__c!=null ? lstTarget[i].Actuals_Solution__c : 0;
						sprint1.actuals_footprint = lstTarget[i].Actuals_Footprint__c!=null ? lstTarget[i].Actuals_Footprint__c : 0;
						sprint1.actuals_share = lstTarget[i].Actuals_Share__c!=null ? lstTarget[i].Actuals_Share__c : 0;
						sprint1.actuals_base = lstTarget[i].Actuals_Base__c!=null ? lstTarget[i].Actuals_Base__c : 0;
						
						lstTargetSprint1.add(sprint1);

					} else if(Integer.valueOf(lstTarget[i].Month__c) >= 5 && Integer.valueOf(lstTarget[i].Month__c) <= 8){
						wrapperDSMPTarget sprint2 = new wrapperDSMPTarget();

						//sprint1_Target_NPD += lstTarget[i].NPD__c != null ? lstTarget[i].NPD__c : 0 ;

						//lstTargetSprint1.add(lstTarget[i]);
						sprint2.id = lstTarget[i].Id; 
						sprint2.month = lstTarget[i].Month__c;

						System.debug('mgr SPRINT 2 ' + lstTarget[i].Month__c);
						System.debug('mgr SPRINT 2 mapMonthLabel.containsKey  ' + mapMonthLabel.containsKey( lstTarget[i].Month__c ) );
						
						if(mapMonthLabel.containsKey( lstTarget[i].Month__c )) 
							sprint2.month_name = mapMonthLabel.get( lstTarget[i].Month__c );

						sprint2.year =lstTarget[i].Total__c; 

						if(lstTarget[i].Total__c != null)
							totalCurrent += lstTarget[i].Total__c;

						sprint2.npd =lstTarget[i].NPD__c!=null ? lstTarget[i].NPD__c : 0; 
						sprint2.solution =lstTarget[i].Solution__c!=null ? lstTarget[i].Solution__c : 0; 
						sprint2.footprint =lstTarget[i].Footprint__c!=null ? lstTarget[i].Footprint__c : 0; 
						sprint2.share =lstTarget[i].Share__c!=null ? lstTarget[i].Share__c : 0; 
						sprint2.base =lstTarget[i].Base__c!=null ? lstTarget[i].Base__c : 0;

						sprint2.actuals_npd = lstTarget[i].Actuals_NPD__c != null ? lstTarget[i].Actuals_NPD__c : 0;
						sprint2.actuals_solution = lstTarget[i].Actuals_Solution__c!= null ? lstTarget[i].Actuals_Solution__c : 0;
						sprint2.actuals_footprint = lstTarget[i].Actuals_Footprint__c!=null ? lstTarget[i].Actuals_Footprint__c : 0;
						sprint2.actuals_share = lstTarget[i].Actuals_Share__c!=null ? lstTarget[i].Actuals_Share__c : 0;
						sprint2.actuals_base = lstTarget[i].Actuals_Base__c!=null ? lstTarget[i].Actuals_Base__c : 0;
						
						lstTargetSprint2.add(sprint2);

						//lstTargetSprint2.add(lstTarget[i]);

					} else if(Integer.valueOf(lstTarget[i].Month__c) >= 9 && Integer.valueOf(lstTarget[i].Month__c) <= 12){

						//lstTargetSprint3.add(lstTarget[i]);
						
						wrapperDSMPTarget sprint3 = new wrapperDSMPTarget();

						//sprint1_Target_NPD += lstTarget[i].NPD__c != null ? lstTarget[i].NPD__c : 0 ;

						//lstTargetSprint1.add(lstTarget[i]);
						sprint3.id = lstTarget[i].Id;
						sprint3.month =lstTarget[i].Month__c; 

						if(mapMonthLabel.containsKey( lstTarget[i].Month__c )) 
							sprint3.month_name = mapMonthLabel.get( lstTarget[i].Month__c );

						sprint3.year =lstTarget[i].Total__c; 

						if(lstTarget[i].Total__c != null)
							totalCurrent += lstTarget[i].Total__c;

						//sprint3.year_1 =lstTarget[i].Id;
						//sprint3.year_2 =lstTarget[i].Id;
						
						sprint3.npd =lstTarget[i].NPD__c!=null?lstTarget[i].NPD__c : 0;
						sprint3.solution =lstTarget[i].Solution__c!=null?lstTarget[i].Solution__c : 0;
						sprint3.footprint =lstTarget[i].Footprint__c!=null?lstTarget[i].Footprint__c : 0; 
						sprint3.share =lstTarget[i].Share__c!=null?lstTarget[i].Share__c : 0; 
						sprint3.base =lstTarget[i].Base__c!=null?lstTarget[i].Base__c : 0;

						sprint3.actuals_npd = lstTarget[i].Actuals_NPD__c!=null ? lstTarget[i].Actuals_NPD__c : 0;
						sprint3.actuals_solution = lstTarget[i].Actuals_Solution__c!=null ? lstTarget[i].Actuals_Solution__c : 0;
						sprint3.actuals_footprint = lstTarget[i].Actuals_Footprint__c!=null ? lstTarget[i].Actuals_Footprint__c : 0;
						sprint3.actuals_share = lstTarget[i].Actuals_Share__c!=null ? lstTarget[i].Actuals_Share__c : 0;
						sprint3.actuals_base = lstTarget[i].Actuals_Base__c!=null ? lstTarget[i].Actuals_Base__c : 0;
						
						lstTargetSprint3.add(sprint3);

						//lstTargetSprint2.add(lstTarget[i]);

					}

				} else {

					if((i+1) >= 1 && (i+1) <= 4){
						wrapperDSMPTarget sprint1 = new wrapperDSMPTarget();
						sprint1.month = String.valueOf((i+1));

						if(mapMonthLabel.containsKey( String.valueOf((i+1)) )) 
							sprint1.month_name = mapMonthLabel.get( String.valueOf((i+1)) );

						lstTargetSprint1.add(sprint1);

					} else if((i+1) >= 5 && (i+1) <= 8){
						wrapperDSMPTarget sprint2 = new wrapperDSMPTarget();
						sprint2.month = String.valueOf(i);

						if(mapMonthLabel.containsKey( String.valueOf(i) )) 
							sprint2.month_name = mapMonthLabel.get( String.valueOf(i) );

						lstTargetSprint2.add(sprint2);
					
					} else if((i+1) >= 9 && (i+1) <= 12){
						wrapperDSMPTarget sprint3 = new wrapperDSMPTarget();
						sprint3.month = String.valueOf((i+1));

						if(mapMonthLabel.containsKey( String.valueOf((i+1)) )) 
							sprint3.month_name = mapMonthLabel.get( String.valueOf((i+1)) );

						lstTargetSprint3.add(sprint3);

					}

				}

			}

			System.debug('mgr lstTargetY_1 ' + lstTargetY_1);
			System.debug('mgr lstTargetY_2 ' + lstTargetY_2);

			// currentYear_1
			for(Integer i=0; i<lstTargetY_1.size(); i++){

				if( Integer.valueOf(lstTargetY_1[i].Month__c) >= 1 && Integer.valueOf(lstTargetY_1[i].Month__c) <= 4){
					//if(lstTargetSprint1[i].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Total__c != null){
					//	lstTargetSprint1[i].year_1 = lstTargetY_1[i].Total__c;
					//	totalLast += lstTargetY_1[i].Total__c;
					//}
					if(lstTargetSprint1[i].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Actuals__c != null){
						lstTargetSprint1[i].year_1 = lstTargetY_1[i].Actuals__c;
						totalLast += lstTargetY_1[i].Actuals__c;
					}

				} else if(Integer.valueOf(lstTargetY_1[i].Month__c) >= 5 && Integer.valueOf(lstTargetY_1[i].Month__c) <= 8){
					//if(lstTargetSprint2[i-4].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Total__c != null){
					//	lstTargetSprint2[i-4].year_1 = lstTargetY_1[i].Total__c;
					//	totalLast += lstTargetY_1[i].Total__c;
					//}
					if(lstTargetSprint2[i-4].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Actuals__c != null){
						lstTargetSprint2[i-4].year_1 = lstTargetY_1[i].Actuals__c;
						totalLast += lstTargetY_1[i].Actuals__c;
					}

				} else if(Integer.valueOf(lstTargetY_1[i].Month__c) >= 9 && Integer.valueOf(lstTargetY_1[i].Month__c) <= 12){
					//if(lstTargetSprint3[i-8].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Total__c != null){
					//	lstTargetSprint3[i-8].year_1 = lstTargetY_1[i].Total__c;
					//	totalLast += lstTargetY_1[i].Total__c;
					//}
					if(lstTargetSprint3[i-8].month == lstTargetY_1[i].Month__c && lstTargetY_1[i].Actuals__c != null){
						lstTargetSprint3[i-8].year_1 = lstTargetY_1[i].Actuals__c;
						totalLast += lstTargetY_1[i].Actuals__c;
					}

				}

			}

			// currentYear_2
			for(Integer i=0; i<lstTargetY_2.size(); i++){

				if( Integer.valueOf(lstTargetY_2[i].Month__c) >= 1 && Integer.valueOf(lstTargetY_2[i].Month__c) <= 4){
					//if(lstTargetSprint1[i].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Total__c != null)
					//	lstTargetSprint1[i].year_2 = lstTargetY_2[i].Total__c;
					if(lstTargetSprint1[i].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Actuals__c != null)
						lstTargetSprint1[i].year_2 = lstTargetY_2[i].Actuals__c;

				} else if(Integer.valueOf(lstTargetY_2[i].Month__c) >= 5 && Integer.valueOf(lstTargetY_2[i].Month__c) <= 8){
					//if(lstTargetSprint2[i-4].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Total__c != null)
					//	lstTargetSprint2[i-4].year_2 = lstTargetY_2[i].Total__c;
					if(lstTargetSprint2[i-4].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Actuals__c != null)
						lstTargetSprint2[i-4].year_2 = lstTargetY_2[i].Actuals__c;

				} else if(Integer.valueOf(lstTargetY_2[i].Month__c) >= 9 && Integer.valueOf(lstTargetY_2[i].Month__c) <= 12){
					//if(lstTargetSprint3[i-8].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Total__c != null)
					//	lstTargetSprint3[i-8].year_2 = lstTargetY_2[i].Total__c;
					if(lstTargetSprint3[i-8].month == lstTargetY_2[i].Month__c && lstTargetY_2[i].Actuals__c != null)
						lstTargetSprint3[i-8].year_2 = lstTargetY_2[i].Actuals__c;

				}

			}

			System.debug('mgr totalCurrent ' + totalCurrent);
			System.debug('mgr totalLast ' + totalLast);
			System.debug('mgr currentDSMP[0].Total_Base__c ' + currentDSMP[0].Total_Base__c);

			System.debug('mgr PT_MIN_Target__c ' + currentDSMP[0].Account__r.PT_MIN_Target__c);

			//growth_mintarget = currentDSMP[0].Minimum_Target__c != null ? (currentDSMP[0].Minimum_Target__c >= (totalCurrent-totalLast)) :  false;
			//growth_mintarget = currentDSMP[0].Minimum_Target__c != null ? (currentDSMP[0].Minimum_Target__c < (totalCurrent-totalLast)) :  false;
			//growth_mintarget = currentDSMP[0].Account__r.PT_MIN_Target__c != null ? (currentDSMP[0].Account__r.PT_MIN_Target__c < (totalCurrent-totalLast)) :  false;
			
			//growth_mintarget = totalLast > currentDSMP[0].Account__r.PT_MIN_Target__c;
			//System.debug('mgr growth_mintarget ' + growth_mintarget);

			//growth_base = currentDSMP[0].Total_Base__c < totalLast && currentDSMP[0].Total_Base__c != null && currentDSMP[0].Total_Base__c != 0;
			if(currentDSMP[0].Total_Base__c != null)
				growth_base = currentDSMP[0].Total_Base__c < totalLast;
			else
				growth_base = 0 < totalLast;

			System.debug('mgr growth_base ' + growth_base);

			//final_target = Integer.valueOf( (totalCurrent-totalLast)/totalLast );
			final_target = totalLast != null && totalLast != 0 ? (totalCurrent-totalLast)/totalLast : 0;
			//final_target = totalCurrent != null && totalCurrent != 0 ? (totalCurrent-totalLast)/totalCurrent : 0;
			final_target = final_target*100;
			System.debug('mgr final_target ' + final_target);

			final_target = final_target.round();

			System.debug('mgr final_target ' + final_target);

			growth_mintarget = currentDSMP[0].Account__r.PT_MIN_Target__c != null && final_target >= currentDSMP[0].Account__r.PT_MIN_Target__c;
			System.debug('mgr growth_mintarget ' + growth_mintarget);

			mapResult.put('accountId', accountId);
			mapResult.put('lstTarget', lstTarget);

			mapResult.put('sprint1', lstTargetSprint1);
			mapResult.put('sprint1_Target_NPD', sprint1_Target_NPD);

			mapResult.put('sprint2', lstTargetSprint2);
			
			mapResult.put('sprint3', lstTargetSprint3);

			mapResult.put('year', currentYear_0);
			mapResult.put('year_1', currentYear_1);
			mapResult.put('year_2', currentYear_2);

			mapResult.put('dvp_segment', currentDSMP[0].DVP_Segment__c);
			mapResult.put('min_target', currentDSMP[0].Account__r.PT_MIN_Target__c);
			mapResult.put('final_target', final_target);
			mapResult.put('total_lastYear', totalLast);
			mapResult.put('total_Current', totalCurrent);
			mapResult.put('total_base', currentDSMP[0].Total_Base__c);
			
			mapResult.put('growth_mintarget', growth_mintarget);
			mapResult.put('growth_base', growth_base);
			mapResult.put('strategic_prod', strategic_prod);
			mapResult.put('incategory', incategory);

			mapResult.put('readOnly', readOnly);
			mapResult.put('error', false);

		} catch(Exception e){

			System.debug('mgr e ' + e);

			mapResult.put('error', true);
			mapResult.put('message', e.getMessage());

		}

		return mapResult;
	}


	@AuraEnabled
	public static Map<String, Object> saveTarget(String lstTarget){
		Map<String, Object> mapResult = new Map<String, Object>();

		PT_DSMP_CS_Table_Plan__c ptrt = PT_DSMP_CS_Table_Plan__c.getOrgDefaults();

		try {

			//List<PT_DSMP_TARGET__c> lstTargetDSMP = new List<PT_DSMP_TARGET__c>();
			List<wrapperDSMPTarget> lstTargetDSMP = new List<wrapperDSMPTarget>();
			List<PT_DSMP_TARGET__c> lstTargetToUpdate = new List<PT_DSMP_TARGET__c>();

			//lstTargetDSMP = (List<PT_DSMP_TARGET__c>) System.JSON.deserialize(lstTarget, List<PT_DSMP_TARGET__c>.class);

			lstTargetDSMP = (List<wrapperDSMPTarget>) System.JSON.deserialize(lstTarget, List<wrapperDSMPTarget>.class);

			//System.debug('mgr lstTargetDSMP1 '+ lstTargetDSMP1);
			//System.debug('mgr lstTargetDSMP '+ lstTargetDSMP);

			for(wrapperDSMPTarget current : lstTargetDSMP){

				lstTargetToUpdate.add(new PT_DSMP_TARGET__c(Id = current.id,
															NPD__c = current.npd,
															Share__c = current.share,
															Base__c = current.base,
															Footprint__c = current.footprint,
															Solution__c = current.solution
															));
			}


			//System.debug('mgr lstTargetToUpdate '+ lstTargetToUpdate);

			if(lstTargetToUpdate.size() > 0)
				update lstTargetToUpdate;
			
			mapResult.put('error', false);
			//mapResult.put('message', 'Targets saved!');
			//mapResult.put('message', Label.PT_DSMP_TablePlan_TargetSaved);
			mapResult.put('message', ptrt.TablePlan_TargetSaved__c);

		} catch(Exception e){

			//mapResult.put('message', e.getMessage());
			mapResult.put('error', true);
			//mapResult.put('message', Label.PT_DSMP_TablePlan_SalesforceError);
			mapResult.put('message', ptrt.TablePlan_SalesforceError__c);

		}

		return mapResult;
	}


	public static Boolean isReadOnly(String displayType, String ownerRoleId){
		System.debug('mgr starting isReadOnly ## ');

		if(displayType == '1'){

			return false;
		
		} else if(displayType == '2' && ownerRoleId != null && UserInfo.getUserRoleId() != null){
			Set<Id> setParentRoles = new Set<Id>();
			setParentRoles = PT_DSMP_Constant.getParentRoles(ownerRoleId);
			//System.debug('mgr setParentRoles ' + setParentRoles);

			if(setParentRoles.size() == 0 || setParentRoles.contains(UserInfo.getUserRoleId())){
				return false;
			}

		} else if(displayType == '3'){
			List<PT_DSMP_CS_App_Admin__c> lstAppAdmin = new List<PT_DSMP_CS_App_Admin__c>([SELECT Id,  Active__c, SetupOwnerId 
																						  FROM PT_DSMP_CS_App_Admin__c
																						  WHERE Active__c = true AND
																							    SetupOwnerId =: UserInfo.getUserId()
																						  ORDER BY CreatedDate Desc]);
			if(!lstAppAdmin.isEmpty()){
				return false;
			}

			return true;

			//System.debug('mgr lstAppAdmin ' + lstAppAdmin);
		}

		return true;
	}


	public class wrapperDSMPTarget {
		@AuraEnabled public String id;
		@AuraEnabled public String month;
		@AuraEnabled public String month_name;
		@AuraEnabled public Double year;
		@AuraEnabled public Double year_1;
		@AuraEnabled public Double year_2;

		@AuraEnabled public Double npd;
		@AuraEnabled public Double solution;
		@AuraEnabled public Double footprint;
		@AuraEnabled public Double share;
		@AuraEnabled public Double base;

		@AuraEnabled public Double actuals_npd;
		@AuraEnabled public Double actuals_solution;
		@AuraEnabled public Double actuals_footprint;
		@AuraEnabled public Double actuals_share;
		@AuraEnabled public Double actuals_base;
		@AuraEnabled public Double total;

		public wrapperDSMPTarget(){
			this.year = 0;
			this.year_1 = 0;
			this.year_2 = 0;

			this.npd = 0;
			this.solution = 0;
			this.footprint = 0;
			this.share = 0;
			this.base = 0;

			this.actuals_npd = 0;
			this.actuals_solution = 0;
			this.actuals_footprint = 0;
			this.actuals_share = 0;
			this.actuals_base = 0;

			this.total = 0;
		}
	}


}