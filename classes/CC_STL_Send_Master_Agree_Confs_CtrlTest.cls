@isTest
public with sharing class CC_STL_Send_Master_Agree_Confs_CtrlTest {
    
    @TestSetup
    private static void createData(){
        
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
        templates.CC_Master_Lease_Template_ID__c = 'ABC123';
        upsert templates;
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;     
        
        TestDataUtility dataUtil = new TestDataUtility(); 
        
        User testUser = dataUtil.createIntegrationUser();
        testUser.LastName = 'Test_User_123';
        insert testUser;             
        
        Account acct = [Select Id From Account LIMIT 1];
        CC_Order__c ccOrder = TestUtilityClass.createNatAcctOrder(acct.id, acct.Id);
        insert ccOrder;
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';     
        insert new Product2[]{prod1, prod2};
            
        CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
        insert salesRep;                    
            
        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        masterLease.Customer__c = acct.Id;
        masterLease.Physical_Damage_Policy_Number__c = 'Test1';

        CC_Master_Lease__c masterLease2 = TestDataUtility.createMasterLease();
        masterLease2.Customer__c = acct.Id;
        masterLease2.Physical_Damage_Policy_Number__c = 'Test2';

        insert new CC_Master_Lease__c[]{masterLease, masterLease2}; 	
  
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;    
            
        CC_Short_Term_Lease__c stl1 = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
        stl1.Lease_Category__c = 'Revenue';
        stl1.Delivery_Quoted_Rate__c = 500;
        stl1.Pickup_Quoted_Rate__c = 500;
        stl1.Delivery_Internal_Cost__c = 500;
        stl1.Pickup_Internal_Cost__c = 500;
        stl1.Adjustments_and_Prepping_Fees__c = 500;
        stl1.Approval_Process_Name__c = 'ABC';
        stl1.Approval_Submission_Date__c = Date.today(); 
        stl1.Cars__c = 'A';
        stl1.Vehicle_Status__c = 'Delivered';
        stl1.Carrier__c = 'UC LS OUT'; 
        stl1.Next_Billing_Date__c = Date.today().addDays(30); 
        stl1.Remaining_Payments__c = 12;  
        
        CC_Short_Term_Lease__c stl2 = stl1.clone(false, true);
        CC_Short_Term_Lease__c stl3 = stl1.clone(false, true);
        stl3.Master_Lease__c = masterLease2.Id;
                
        insert new CC_Short_Term_Lease__c[]{stl1, stl2, stl3};
                    
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl1.Id;
        car1.Quantity__c = 10;
        car1.Per_Car_Cost__c = 25;
        
        CC_STL_Car_Info__c car2 = car1.clone(false, true);
        CC_STL_Car_Info__c car3 = car1.clone(false, true);
        car3.Short_Term_Lease__c = stl2.Id;

        insert new CC_STL_Car_Info__c[]{car1, car2, car3};         
    }

    @isTest
    private static void testConstructor(){
        
        CC_Master_Lease__c masterLease1 = [Select Id, Name From CC_Master_Lease__c Where Physical_Damage_Policy_Number__c = 'Test1'];
        
        ApexPages.currentPage().getParameters().put('id', masterLease1.Id);
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(masterLease1);
        CC_STL_Send_Master_Agree_Confirms_Ctrl ctrl = new CC_STL_Send_Master_Agree_Confirms_Ctrl(stdCtrl);

        system.assertEquals(2, ctrl.leases.size());

        for (CC_STL_Send_Master_Agree_Confirms_Ctrl.LeaseWrapper wrapper : ctrl.leases){
            system.assertEquals(false, wrapper.selected);
        }

        CC_Master_Lease__c masterLease2 = [Select Id, Name From CC_Master_Lease__c Where Physical_Damage_Policy_Number__c = 'Test2'];

        ApexPages.currentPage().getParameters().put('id', masterLease2.Id);
        ApexPages.StandardController stdCtrl2 = new ApexPages.StandardController(masterLease2);
        CC_STL_Send_Master_Agree_Confirms_Ctrl ctrl2 = new CC_STL_Send_Master_Agree_Confirms_Ctrl(stdCtrl2);

        system.assertEquals(1, ctrl2.leases.size());    

        for (CC_STL_Send_Master_Agree_Confirms_Ctrl.LeaseWrapper wrapper : ctrl2.leases){
            system.assertEquals(false, wrapper.selected);
        }            
    }
    
    @isTest
    private static void testBuildAndSend(){
        
        CC_Master_Lease__c masterLease = [Select Id, Name From CC_Master_Lease__c Where Physical_Damage_Policy_Number__c = 'Test1'];
        
        ApexPages.currentPage().getParameters().put('id', masterLease.Id);
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(masterLease);
        CC_STL_Send_Master_Agree_Confirms_Ctrl ctrl = new CC_STL_Send_Master_Agree_Confirms_Ctrl(stdCtrl);	
        
        PageReference sendResult;
        
        Test.startTest();
        
        sendResult = ctrl.buildAndSend();
        
        Test.stopTest();
        
        // Verify the results
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
        system.assertEquals('/apex/echosign_dev1__AgreementTemplateProcess?masterid=' + masterLease.Id + '&templateId=' + templates.CC_Master_Lease_Template_ID__c, sendResult.getUrl());
        
        Attachment att = [Select Name, IsPrivate From Attachment Where ParentId = :masterLease.Id];
        system.assert(att.Name.contains('Master Lease Agreement and Lease Confirmations'));	
        system.assertEquals(false, att.IsPrivate);	
    }    
}