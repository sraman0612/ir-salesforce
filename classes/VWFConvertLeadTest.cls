// Test class for the lead convert Apex plug-in.
@isTest
private class VWFConvertLeadTest {

    @TestSetup
    private static void createData(){
        
        Profile p = [Select Id From Profile Where Name = 'System Administrator'];
        
        User adminUser = TestUtilityClass.createNonPortalUser(p.Id);
        adminUser.LastName = 'Migration';          
        insert adminUser;         
    }

     /*
      * This tests lead conversion with 
      * the Account ID specified.
      */
    static testMethod void basicTestwithAccount() {

        // Create test lead
        Lead testLead = new Lead(
            Company='Test Lead',FirstName='John',LastName='Doe', Phone = '1112223434', Status = 'New', Opportunity_Type__c = 'New Contract', Email='abc@123.com', Title='Test');
            testLead.RecordtypeId = Schema.SObjectType.lead.getRecordTypeInfosByName().get('CTS New Lead').getRecordTypeId();
        insert testLead;
        
        Account testAccount = new Account(name='Test Account',shippingCountry='Mexico',billingCountry='Mexico', County__c='Goodhue', ShippingCity='Wells', ShippingState='MN', ShippingStreet='111 Main St',shippingPostalCode='56097',currencyIsoCode='MXN');
        testAccount.RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert testAccount;
    
           // System.debug('ACCOUNT BEFORE' + testAccount.ID);

        LeadStatus convertStatus = [Select Id, MasterLabel 
                    from LeadStatus where IsConverted=true  limit 1]; 

        User adminUser = [Select Id From User Where LastName = 'Migration' Order By CreatedDate DESC LIMIT 1];

        System.runAs(adminUser){
        
            // Create test conversion
            VWFConvertLead aLeadPlugin = new VWFConvertLead();
            Map<String,Object> inputParams = new Map<String,Object>();
            Map<String,Object> outputParams = new Map<String,Object>();

            inputParams.put('LeadID',testLead.ID);
            inputParams.put('AccountID',testAccount.ID);
           // inputParams.put('ConvertedStatus',
              //  convertStatus.MasterLabel);
             inputParams.put('ConvertedStatus',
             'Converted');

            Process.PluginRequest request = new 
                Process.PluginRequest(inputParams);
            Process.PluginResult result;
            result = aLeadPlugin.invoke(request);
            
            Lead aLead = 
                [select name, id, isConverted, convertedAccountID 
                from Lead where id = :testLead.ID];
            System.Assert(aLead.isConverted);
            //System.debug('ACCOUNT AFTER' + aLead.convertedAccountID);
            System.AssertEquals(testAccount.ID, aLead.convertedAccountID);
        }
    }

    /*
     * This tests lead conversion with the Account ID specified.
    */
    static testMethod void basicTestwithAccounts() {

        User adminUser = [Select Id From User Where LastName = 'Migration' Order By CreatedDate DESC LIMIT 1];

        System.runAs(adminUser){        

            // Create test lead
            Lead testLead = new Lead(
                Company='Test Lead',FirstName='John',LastName='Doe', Phone = '1112223434', Status = 'New', Opportunity_Type__c = 'New Contract');
                testLead.RecordtypeId = Schema.SObjectType.lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
            insert testLead;
            
            Account testAccount1 = new Account(name='Test Account1',shippingCountry='Mexico', billingCountry='Mexico', County__c='Goodhue', ShippingCity='Wells', ShippingState='MN', ShippingStreet='111 Main St',shippingPostalCode='56097',currencyIsoCode='MXN');
                testAccount1.RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            insert testAccount1;
            Account testAccount2 = new Account(name='Test Account2',shippingCountry='Mexico', billingCountry='Mexico', County__c='Goodhue', ShippingCity='Wells', ShippingState='MN', ShippingStreet='113 Main St',shippingPostalCode='56097',currencyIsoCode='MXN');
                testAccount2.RecordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            insert testAccount2;

            // System.debug('ACCOUNT BEFORE' + testAccount.ID);

            LeadStatus convertStatus = [Select Id, MasterLabel 
                from LeadStatus where IsConverted=true AND apiName='Converted' limit 1]; 
            
            // Create test conversion
            VWFConvertLead aLeadPlugin = new VWFConvertLead();
            Map<String,Object> inputParams = new Map<String,Object>();
            Map<String,Object> outputParams = new Map<String,Object>();

            inputParams.put('LeadID',testLead.ID);
            inputParams.put('AccountID',testAccount2.ID);
            inputParams.put('ConvertedStatus',
                convertStatus.MasterLabel);

            Process.PluginRequest request = new 
                Process.PluginRequest(inputParams);
            Process.PluginResult result;
            result = aLeadPlugin.invoke(request);
            
            Lead aLead = 
                [select name, id, isConverted, convertedAccountID 
                from Lead where id = :testLead.ID];
            System.Assert(aLead.isConverted);
        }
    }


     /*
      * -ve Test
      */    
    static testMethod void errorTest() {

        // Create test lead
        // Lead testLead = new Lead(Company='Test Lead',
        //   FirstName='John',LastName='Doe');
        LeadStatus convertStatus = [Select Id, MasterLabel 
            from LeadStatus where IsConverted=true limit 1]; 

        User adminUser = [Select Id From User Where LastName = 'Migration' Order By CreatedDate DESC LIMIT 1];

        System.runAs(adminUser){            
        
            // Create test conversion
            VWFConvertLead aLeadPlugin = new VWFConvertLead();
            Map<String,Object> inputParams = new Map<String,Object>();
            Map<String,Object> outputParams = new Map<String,Object>();
            inputParams.put('LeadID','00Q7XXXXxxxxxxx');
            inputParams.put('ConvertedStatus',convertStatus.MasterLabel);

            Process.PluginRequest request = new 
                Process.PluginRequest(inputParams);
            Process.PluginResult result;
            try {
                result = aLeadPlugin.invoke(request);    
            }
                catch (Exception e) {
                System.debug('EXCEPTION' + e);
                System.AssertEquals(1,1);
            }
        }
    }
    
    
     /*
      * This tests the describe() method
      */ 
    static testMethod void describeTest() {

        User adminUser = [Select Id From User Where LastName = 'Migration' Order By CreatedDate DESC LIMIT 1];

        System.runAs(adminUser){        

            VWFConvertLead aLeadPlugin = 
                new VWFConvertLead();
            Process.PluginDescribeResult result = 
                aLeadPlugin.describe();
            
            System.AssertEquals(
                result.inputParameters.size(), 8);
            System.AssertEquals(
                result.OutputParameters.size(), 3);
        }
     }
}