/*************************************
* CTS_CaseHelper
* Helper class for Case(Feedback)
**************************************/
public class CTS_CaseHelper { 
	/**
	 * create Case for Feedback
	 */
    public static String createCase(String description, String subject, String contactId,String caseRecordType,String articleUrl){
        Map<String, RecordTypeInfo> caseRecordTypes =  Schema.SObjectType.Case.getRecordTypeInfosByName();
        
        Case newCase = new Case(Subject = subject, description = description,
                       Status = CTS_Constants.STATUS_NEW, Priority = CTS_Constants.PRIORITY_LOW, 
                       CTS_TechDirect_Category__c = CTS_Constants.CATEGORY_FEEDBACK, 
                       CTS_TechDirect_Sub_Category__c = CTS_Constants.CATEGORY_FEEDBACK,
                       contactId = contactId,origin = CTS_Constants.CASE_ORIGIN_COMMUNITY);
        newCase.RecordTypeId = caseRecordTypes.get(caseRecordType).getRecordTypeId();
        //newCase.AssetId = '02i22000000NXdX'; // MA : added only temprory/testing purpose
        if(articleUrl != null && articleUrl != ''){
            newCase.Article_Feedback_URL__c = articleUrl;
        }
        try{
            Database.DMLOptions dmo = new Database.DMLOptions();
			dmo.assignmentRuleHeader.useDefaultRule= true;
			newCase.setOptions(dmo);
            insert newCase;
            return newCase.id;
        }catch(Exception ex){
            return ex.getMessage();
        }
     
    }
}