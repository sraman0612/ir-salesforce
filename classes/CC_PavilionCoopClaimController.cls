global class CC_PavilionCoopClaimController {
    
    /* member variables */
    
    public static final String STATUS_PAID = 'Paid';
    
    public string ctID {get;set;}
    public string venderName {get;set;}
    public string invoicedate {get;set;}
    public string numCompBrsnds {get;set;}
    public string invoiceAmmount {get;set;}
    public string copyOfInvoice {get;set;}
    public string tearsheetOfAdd {get;set;}
    public string ccLogoInAd {get;set;}
    public string newClaimAmount {get;set;}
    public string comments {get;set;}
    public string advertisingType {get;set;}
    public string returnStatus{get;set;}
    public static User Usr {get;set;}
    
    public static Contact contct{get;set;}
    public static Account  account{get;set;}
    
    
    public List < String > lstAdvertising {get;set;} /* user for Advertising_Type__c picklist */
    public String UserAccountId { get; set; }
    
    public static Attachment invoiceattachment {
        get {
            if (invoiceattachment == null)
                invoiceattachment = new Attachment();
            return invoiceattachment;
        }
        set;
    }
    //Added 10 attachments by @Priyanka Baviskar for ticket #8287533: USERS CAN'T SELECT MORE THAN ONE FILE TO UPLOAD TO CO-OP CLAIM at 4/25/2019
    public static Attachment tearsheetattachment1 {
        get {
            if (tearsheetattachment1 == null)
                tearsheetattachment1 = new Attachment();
            return tearsheetattachment1;
        }
        set;
    }
    public static Attachment tearsheetattachment2 {
        get {
            if (tearsheetattachment2 == null)
                tearsheetattachment2 = new Attachment();
            return tearsheetattachment2;
        }
        set;
    }
    public static Attachment tearsheetattachment3 {
        get {
            if (tearsheetattachment3 == null)
                tearsheetattachment3 = new Attachment();
            return tearsheetattachment3;
        }
        set;
    }
    public static Attachment tearsheetattachment4 {
        get {
            if (tearsheetattachment4 == null)
                tearsheetattachment4 = new Attachment();
            return tearsheetattachment4;
        }
        set;
    }
    
    public static Attachment tearsheetattachment5 {
        get {
            if (tearsheetattachment5 == null)
                tearsheetattachment5 = new Attachment();
            return tearsheetattachment5;
        }
        set;
    }
    public static Attachment tearsheetattachment6 {
        get {
            if (tearsheetattachment6 == null)
                tearsheetattachment6 = new Attachment();
            return tearsheetattachment6;
        }
        set;
    }
    
    public static Attachment tearsheetattachment7 {
        get {
            if (tearsheetattachment7 == null)
                tearsheetattachment7 = new Attachment();
            return tearsheetattachment7;
        }
        set;
    }
    public static Attachment tearsheetattachment8 {
        get {
            if (tearsheetattachment8 == null)
                tearsheetattachment8 = new Attachment();
            return tearsheetattachment8;
        }
        set;
    }
    public static Attachment tearsheetattachment9 {
        get {
            if (tearsheetattachment9 == null)
                tearsheetattachment9 = new Attachment();
            return tearsheetattachment9;
        }
        set;
    }
    public static Attachment tearsheetattachment10 {
        get {
            if (tearsheetattachment10 == null)
                tearsheetattachment10 = new Attachment();
            return tearsheetattachment10;
        }
        set;
    }
    /*public static Attachment tearsheetattachment {
get {
if (tearsheetattachment == null)
tearsheetattachment = new Attachment();
return tearsheetattachment;
}
set;
}*/
    
    
    @TestVisible static Date currentDate = Date.today();
    /* constructor */
    
    public CC_PavilionCoopClaimController(CC_PavilionTemplateController controller) {
        UserAccountId =controller.acctId;
        lstAdvertising = new List < String > ();
        Set < String > setAdvertising = new Set < String > ();
        Schema.DescribeFieldResult fieldResult = CC_Coop_Claims__c.Advertising_Type__c.getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            setAdvertising.add(f.getValue());
        }
        lstAdvertising.addAll(setAdvertising);
        lstAdvertising.sort();
        
    }
    public CC_PavilionCoopClaimController(){
        
    }
    
    /*** Remote method to be called from CC_PavilionCoopClaim page via JS Remoting **/
    
    
    @RemoteAction
    global static String refreshTableBasedOnYear(String selectedAggrementType, String UserAccountId) {
        List < Contract > listContract = new List < Contract > ();
        List < Contract > listContractToParse = new List < Contract > ();
        currentDate = Test.isRunningTest() ? currentDate : Date.today();
        Integer startYear = currentDate.year(); // Current Year
        
        if (startYear != Integer.valueOf(Label.Pavilion_Start_Year)) {
            if (currentDate.month() <= 2) { // Jan or Feb
                startYear = startYear - 1;
            }
        }
        
        Date startDate = Date.newInstance(startYear, 1, 1);
        Date endDate = Date.newInstance(startYear + 1, 3, 1); // Year will be startYear + 1
        
        if (String.isNotBlank(selectedAggrementType)) {
            
        }
        
        for (Contract objContract: listContract) {
            objContract.CC_Contract_End_Date__c = objContract.CC_Contract_End_Date__c.addHours(-5).addMinutes(-30);
        }
        if (!listContract.isEmpty()) {
            String jsonData = (String) JSON.serialize(listContract);
            return jsonData;
        } else {
            return 'No data found';
        }
    }
    
    /**Remote method to submit the Coop Claim data**/
    
    @RemoteAction
    global Static String  submitCoopClaimData1(CC_Coop_Claims__c objCCCoopClaim){
        return '';
    }
    
    public PageReference submitPavCoopClaimData() {
        System.debug('In pav Coop claim data @@@');
        
        System.debug('In pav Coop claim data @@@ venderName ---> '+venderName);
        system.debug('invoice date is ' + invoicedate);
        system.debug('numCompBrsnds is ' + numCompBrsnds);
        system.debug('invoiceAmmount is ' + invoiceAmmount);
        system.debug('copyOfInvoice is ' + copyOfInvoice);
        system.debug('tearsheetOfAdd is ' + tearsheetOfAdd);
        system.debug('ccLogoInAd is ' + ccLogoInAd);
        system.debug('newClaimAmount is ' + newClaimAmount);
        system.debug('comments is ' + comments);
        system.debug('contract is ' + ctID);
        system.debug('contract from controller var is ' + ctID);
        system.debug('advertisingType is ' + advertisingType);
        
        String claimStatus = '';
        String dealerLevel = '';
        Boolean isClaimCreated = false;
        Boolean isAttachCreated = false;
        String sts='';
        try {
            CC_Co_Op_Account_Summary__c con = [SELECT Id, CC_Available_Funds__c FROM CC_Co_Op_Account_Summary__c WHERE Id = : ctID LIMIT 1];
            Decimal claimAmount = (con.CC_Available_Funds__c >= Double.valueOf(newClaimAmount)) ? Double.valueOf(newClaimAmount) : con.CC_Available_Funds__c;
            CC_Coop_Claims__c objCCCoopClaim = new CC_Coop_Claims__c();
            objCCCoopClaim.Vendor_Adv_Name__c = venderName;
            objCCCoopClaim.Invoice_date__c = String.isBlank(invoicedate) ? null : Date.parse(invoicedate);
            objCCCoopClaim.Number_of_competitive_brands__c = numCompBrsnds;
            objCCCoopClaim.Invoice_amount__c = Decimal.valueOf(invoiceAmmount);
            objCCCoopClaim.Copy_Of_Invoice__c = copyOfInvoice;
            objCCCoopClaim.New_Claim_Amount__c = Double.valueOf(claimAmount);
            objCCCoopClaim.Advertising_Type__c = advertisingType;
            objCCCoopClaim.TearSheet_of_Advertisment__c = tearsheetOfAdd;
            objCCCoopClaim.Club_Car_Logo_in_Add__c = ccLogoInAd;
            objCCCoopClaim.Comment__c = comments;
            objCCCoopClaim.Co_Op_Account_Summary__c = ctID;
            objCCCoopClaim.CC_Submit_Date__c= Date.today(); //MMattawar: 15-Oct-2018 For RT 7360762
            claimStatus = objCCCoopClaim.CC_Status__c;
            //dealerLevel = objCCCoopClaim.CC_Dealer_Level__c;
            
            if(invoiceattachment.name != null && invoiceattachment.name != '') {
                objCCCoopClaim.CC_Status__c = 'Submitted';
                sts = 'Submitted';
            } else {
                objCCCoopClaim.CC_Status__c = 'Incomplete';
                sts = 'Incomplete';
            } 
            System.debug('^^^^^^^^^^^^^^^Sts = '+sts);
            insert objCCCoopClaim;
            isClaimCreated = true;
            try{
                //inserting attachments
                List < Attachment > lstAttachment = new List <Attachment>();
                System.debug('invoiceattachment.name @@ '+invoiceattachment.name);
                System.debug('invoiceattachment.body @@ '+invoiceattachment.body);
                if(String.isNotBlank(invoiceattachment.name)) {
                    invoiceattachment.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(invoiceattachment);
                }
                //Added 10 attachments by @Priyanka Baviskar for ticket #8287533: USERS CAN'T SELECT MORE THAN ONE FILE TO UPLOAD TO CO-OP CLAIM at 4/25/2019
                if(String.isNotBlank(tearsheetattachment1.name)) {
                    tearsheetattachment1.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment1);
                }
                if(String.isNotBlank(tearsheetattachment2.name)) {
                    tearsheetattachment2.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment2);
                }
                if(String.isNotBlank(tearsheetattachment3.name)) {
                    tearsheetattachment3.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment3);
                }
                if(String.isNotBlank(tearsheetattachment4.name)) {
                    tearsheetattachment4.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment4);
                }
                if(String.isNotBlank(tearsheetattachment5.name)) {
                    tearsheetattachment5.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment5);
                }
                if(String.isNotBlank(tearsheetattachment6.name)) {
                    tearsheetattachment6.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment6);
                }
                if(String.isNotBlank(tearsheetattachment7.name)) {
                    tearsheetattachment7.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment7);
                }
                if(String.isNotBlank(tearsheetattachment8.name)) {
                    tearsheetattachment8.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment8);
                }
                if(String.isNotBlank(tearsheetattachment9.name)) {
                    tearsheetattachment9.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment9);
                }
                if(String.isNotBlank(tearsheetattachment10.name)) {
                    tearsheetattachment10.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment10);
                }
                /* if(String.isNotBlank(tearsheetattachment.name)) {
tearsheetattachment.parentId = objCCCoopClaim.Id;
lstAttachment.add(tearsheetattachment);
}*/
                if (!lstAttachment.isEmpty()){
                    insert lstAttachment;
                    isAttachCreated = true;
                }
                
                
            }catch(Exception e){
                System.debug('Exception  = '+e.getStackTraceString());
            }
            
            claimStatus = [select name from CC_Coop_Claims__c  where Id=:objCCCoopClaim.Id limit 1][0].Name;
            if(sts   == 'Pending'){
                returnStatus   =   'Your claim : '+claimStatus+' has not been submitted for approval. To submit your claim for approval, please edit your claim from below table and add your supporting documents.';
            }
            if(sts   == 'Submitted'){
                returnStatus   =   'Your Co-Op claim : '+claimStatus+' has been submitted for approval.';
            }                 
        }catch(Exception e){
            returnStatus = 'Failure';
            system.debug('@@Exception Occured'+e);
        }
        
        PageReference pr = Page.CC_PavilionClaimStatus;
        pr.getParameters().put('claimMsg',returnStatus);
        pr.setRedirect(true);
        return pr;
        
    }
    
    
    @RemoteAction
    global static String submitCoopClaimData(String venderName, String invoicedate, String numCompBrsnds,  String invoiceAmmount, String copyOfInvoice,  String tearsheetOfAdd, String ccLogoInAd, String newClaimAmount,  String comments, String ContractId, String advertisingType) {
        String claimStatus = '';
        Boolean isClaimCreated = false;
        Boolean isAttachCreated = false;
        String sts='';
        String returnStatus  = 'test';
        try {
            CC_Co_Op_Account_Summary__c con = [SELECT Id, CC_Available_Funds__c FROM CC_Co_Op_Account_Summary__c WHERE Id = : ContractId LIMIT 1];
            Decimal claimAmount = (con.CC_Available_Funds__c >= Double.valueOf(newClaimAmount)) ? Double.valueOf(newClaimAmount) : con.CC_Available_Funds__c;
            CC_Coop_Claims__c objCCCoopClaim = new CC_Coop_Claims__c();
            objCCCoopClaim.Vendor_Adv_Name__c = venderName;
            objCCCoopClaim.Invoice_date__c = String.isBlank(invoicedate) ? null : Date.parse(invoicedate);
            objCCCoopClaim.Number_of_competitive_brands__c = numCompBrsnds;
            objCCCoopClaim.Invoice_amount__c = Decimal.valueOf(invoiceAmmount);
            objCCCoopClaim.Copy_Of_Invoice__c = copyOfInvoice;
            objCCCoopClaim.New_Claim_Amount__c = Double.valueOf(claimAmount);
            objCCCoopClaim.Advertising_Type__c = advertisingType;
            objCCCoopClaim.TearSheet_of_Advertisment__c = tearsheetOfAdd;
            objCCCoopClaim.Club_Car_Logo_in_Add__c = ccLogoInAd;
            objCCCoopClaim.Comment__c = comments;
            objCCCoopClaim.Co_Op_Account_Summary__c = ContractId;
            objCCCoopClaim.CC_Submit_Date__c= Date.today(); //MMattawar: 15-Oct-2018 For RT 7360762
            claimStatus = objCCCoopClaim.CC_Status__c;
            
            if(invoiceattachment.name != null && invoiceattachment.name != '') {
                objCCCoopClaim.CC_Status__c = 'Submitted';
                sts = 'Submitted';
            } else {
                objCCCoopClaim.CC_Status__c = 'Incomplete';
                sts = 'Incomplete';
            } 
            System.debug('^^^^^^^^^^^^^^^Sts = '+sts);
            insert objCCCoopClaim;
            isClaimCreated = true;
            System.debug('^^^^^^^^^^^^^^^returnStatus = '+returnStatus );
            try{
                //inserting attachments
                List < Attachment > lstAttachment = new List <Attachment>();
                System.debug('invoiceattachment.name @@ '+invoiceattachment.name);
                System.debug('invoiceattachment.body @@ '+invoiceattachment.body);
                if(String.isNotBlank(invoiceattachment.name)) {
                    invoiceattachment.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(invoiceattachment);
                }
                //Added 10 attachments by @Priyanka Baviskar for ticket #8287533: USERS CAN'T SELECT MORE THAN ONE FILE TO UPLOAD TO CO-OP CLAIM at 4/25/2019
                if(String.isNotBlank(tearsheetattachment1.name)) {
                    tearsheetattachment1.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment1);
                }
                if(String.isNotBlank(tearsheetattachment2.name)) {
                    tearsheetattachment2.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment2);
                }
                if(String.isNotBlank(tearsheetattachment3.name)) {
                    tearsheetattachment3.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment3);
                }
                if(String.isNotBlank(tearsheetattachment4.name)) {
                    tearsheetattachment4.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment4);
                }
                if(String.isNotBlank(tearsheetattachment5.name)) {
                    tearsheetattachment5.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment5);
                }
                if(String.isNotBlank(tearsheetattachment6.name)) {
                    tearsheetattachment6.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment6);
                }
                if(String.isNotBlank(tearsheetattachment7.name)) {
                    tearsheetattachment7.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment7);
                }
                if(String.isNotBlank(tearsheetattachment8.name)) {
                    tearsheetattachment8.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment8);
                }
                if(String.isNotBlank(tearsheetattachment9.name)) {
                    tearsheetattachment9.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment9);
                }
                if(String.isNotBlank(tearsheetattachment10.name)) {
                    tearsheetattachment10.parentId = objCCCoopClaim.Id;
                    lstAttachment.add(tearsheetattachment10);
                }
                
                
                /*if(String.isNotBlank(tearsheetattachment.name)) {
tearsheetattachment.parentId = objCCCoopClaim.Id;
lstAttachment.add(tearsheetattachment);
}*/
                if (!lstAttachment.isEmpty()){
                    insert lstAttachment;
                    isAttachCreated = true;
                }
            }catch(Exception e){
                System.debug('Exception  = '+e.getStackTraceString());
            }
            system.debug('objCCCoopClaim.Id Exception Occured'+objCCCoopClaim.Id);
            claimStatus = [select name from CC_Coop_Claims__c  where Id=:objCCCoopClaim.Id limit 1][0].Name;
            if(sts   == 'Pending'){
                returnStatus   =   'Your claim : '+claimStatus+' has not been submitted for approval. To submit your claim for approval, please edit your claim from below table and add your supporting documents.';
            }
            if(sts   == 'Submitted'){
                returnStatus   =   'Your Co-Op claim : '+claimStatus+' has been submitted for approval.';
            }
            if(isClaimCreated == true ){
                return returnStatus ;
            }
        }catch(Exception e){
            return 'Failure';
            system.debug('Exception Occured'+e);
        }
        return '';
    }
    
    //code contributed by @Priyanka Baviskar for Request 7527295.
    
    @RemoteAction
    public Static List<CC_Co_Op_Account_Summary__c> getAggrementTypes(String ACCID) {
        Set<CC_Co_Op_Account_Summary__c> setContract = new Set<CC_Co_Op_Account_Summary__c>();
        List<CC_Co_Op_Account_Summary__c> agreementList= new List<CC_Co_Op_Account_Summary__c>();
        Integer currentYear=System.Today().year();
        Integer previousYear=System.Today().year()-1;
        For(CC_Co_Op_Account_Summary__c CCop:[SELECT Id, Contract__r.CC_Sub_Type__c, Contract__r.CC_Market__c, IsActive__c, CC_Co_Op_Year__c ,CC_Available_Funds__c
                                              FROM CC_Co_Op_Account_Summary__c Where Contract__r.CC_Type__c = 'Dealer/Distributor Agreement' 
                                              AND IsActive__c = True AND Contract__r.AccountId =:ACCID]){
                                                  setContract.add(CCop);
                                              }
        agreementList.addAll(setContract);
        agreementList.sort();
        return agreementList;
    }
    
    //code contributed by @Priyanka Baviskar for Request 7527295.
    
    @RemoteAction
    public static String getDealerLevel(String claim)
    {
        usr = new User();
        contct =   new Contact();
        account = new Account();
        
        Id userid = UserInfo.getUserId();
        usr = (User)[select Id,Contact.Id, Name, Phone,Extension,MobilePhone from User where Id =:UserInfo.getUserId() limit 1];
        Id cid = usr.Contact.Id;
        if(cid!= null)
            contct = (Contact)[select Id, Account.Id, Email,  HomePhone, MobilePhone, OtherPhone, Phone from Contact where ID =:cid limit 1];
        Id accid = contct.Account.Id;
        if(contct.Account.Id != null) 
            account = (Account)[select Id ,AccountNumber,CC_Classification__c from Account where Id =:accid limit 1];
        String level= account.CC_Classification__c ;
        System.debug('level'+level);
        return level;
        
    }
    
}