public with sharing class CC_My_Team_Service {

  	public static final String financeRepRole = 'Customer Financing Representative';
  	public static final String usedCarSalesRepRole = 'Used Car Sales Representative';
  	public static final String customerAcctSpecRole = 'Customer Account Specialist';

    // Returns a map of account Id with a corresponding map of role name and user Id
    public static Map<Id, Map<String, Id>> lookupUsersByAccountAndRole(Set<String> roleNames, Set<Id> accountIds){
        
        // Build a map of account Id and a mapping of role name and user found
        Map<Id, Map<String, Id>> accountUserRoleMap = new Map<Id, Map<String, Id>>();
        
        for (CC_My_Team__c myTeam : [Select Id, User__c, Account__c, My_Team_Role__c From CC_My_Team__c Where User__r.isActive = true and Account__c in :accountIds and My_Team_Role__c in :roleNames]){
            
            if (accountUserRoleMap.containsKey(myTeam.Account__c)){
                accountUserRoleMap.get(myTeam.Account__c).put(myTeam.My_Team_Role__c, myTeam.User__c);
            }
            else{
                accountUserRoleMap.put(myTeam.Account__c, new Map<String, Id>{myTeam.My_Team_Role__c => myTeam.User__c});
            }         
        } 
        
        // Add any account to the map that did not have roles found
        for (Id accountId : accountIds){
            
            if (!accountUserRoleMap.containsKey(accountId)){
                accountUserRoleMap.put(accountId, new Map<String, Id>());
            }
        }
        
        return accountUserRoleMap;
    }
}