/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */
global class CC_PavilionInvoiceDetailController {
 public String SalesRep{get;set;}
 Public Id AcctId;
 Public String custNum{get;set;}
 public boolean showPricing{get;set;}
 public string comment {get;set;}
 public List < CC_Invoice2__c > lstinvoice{get;set;}
 Public list < CC_Invoice2__c > lstInvoiceitems{get;set;}
 Public list < CC_Invoice2__c > allInvoicesList{get;set;}
 public String InvId;
 public double AmountDue{get;set;}
 Public String InvQuery = ''; 
 public String remitAddress {get;set;}
 Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
 Map < String, Schema.SObjectField > fieldMap = schemaMap.get('CC_Invoice2__c').getDescribe().fields.getMap();
 public String pagetype;
 Public list < CC_SpeedCodeHistory__c > lstPurchaseordernumber{get;set;}   
 public string OrderNumber ='';
 Public string PurchaseOrder{get;set;}
 Public Id OrderID;
 public CC_PavilionInvoiceDetailController(CC_PavilionTemplateController controller) {
           
           double Amount;
           double Tradediscount = 0.0;
           double MiscChargers = 0.0;
           double Taxes = 0.0;
           double TermsDiscount = 0.0;
           double Deductdiscounts = 0.0;
           double TotalTaxes = 0.0;
           double TotalDiscounts = 0.0;
           AmountDue = 0.00;  
           lstInvoiceitems = new list < CC_Invoice2__c > ();
           
           //Account id & Customer Number from Template Controller
           String AcctId = controller.acctid;
           String custNum = controller.customerNumber;
           
            //logic for Remit to Address
           if (AcctId != null) {
                      if (custNum.endswith('21')) {
                        remitAddress = null!=PavilionSettings__c.getInstance('RemitAddress_CA')?PavilionSettings__c.getInstance('RemitAddress_CA').Value__c:'';
                      } else {
                        remitAddress = null!=PavilionSettings__c.getInstance('RemitAddress_US')?PavilionSettings__c.getInstance('RemitAddress_US').Value__c:'';
                      }
            }
            
           String commaSepratedFields = '';
           InvQuery = '';
           for (String fieldName: fieldMap.keyset()) {
                if (commaSepratedFields == null || commaSepratedFields == '') {
                    commaSepratedFields = fieldName;
                } else {
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
           }
           //Passing InvId to invoicedetail page
           InvId = ApexPages.currentPage().getParameters().get('invid');
           pagetype = ApexPages.currentPage().getParameters().get('pagetype');
           System.debug('InvId====' + InvId);
           InvQuery = 'select ' + commaSepratedFields + ' from ' + 'CC_Invoice2__c';
            if (InvId != null) {
                InvQuery += ' where Name =:InvId AND CC_sequence__c = 1';
                /*Logic for Show pricing on National Account Orders */
                showPricing=custNum==InvId.split('_')[1]||AcctId==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
                try {
                      allInvoicesList = [select Id, CC_Item_Number__c, CC_Invoice_Date__c, CC_Net_Due_Date__c, CC_Item_Description__c,CC_Line_Net_Sale_Amt__c,  
                                        CC_Customer_Number__c, CC_Customer_Number__r.CC_Customer_Number__c, CC_Spec_Charges__c, CC_Taxes__c, CC_Terms_Discount__c, 
                                        CC_Trade_Discount__c, CC_Unit_Measure__c, CC_Qty_Shipped__c, CC_Qty_Back_Ord__c, CC_Unit_Price__c, CC_Amount__c, CC_PO_Number__c, 
                                        CC_Invoice_Number__c from CC_Invoice2__c where Name = : InvId order By CC_sequence__c ASC];
                      lstInvoiceitems = database.query(InvQuery);
                      Set<String> invoicesProductListTempIDs=new Set<String>(); 
                      OrderNumber = lstInvoiceitems[0].CC_Order_Number__c; 
                      OrderID = [Select ID from CC_Order__c where Order_Number__c =: OrderNumber].ID;
                      if(pagetype == '2')
                      {
                           PurchaseOrder =[SELECT  CC_Purchase_Order_Number__c FROM CC_SpeedCodeHistory__c where CC_Order_Number__c =: OrderID Limit 1].CC_Purchase_Order_Number__c;
                      }
                    else
                    {
                        PurchaseOrder=lstInvoiceitems[0].CC_PO_Number__c;
                    }
                }catch (Exception e) {
                        System.debug('Errors' + e);
                }
             }
            else{
                  lstInvoiceitems = new list < CC_Invoice2__c > ();
                  comment = 'No Invoice Items';
             }
 }  
}