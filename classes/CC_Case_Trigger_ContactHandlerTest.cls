@isTest
public with sharing class CC_Case_Trigger_ContactHandlerTest {
    
    @TestSetup
    private static void createData(){
 		Case_Trigger_ContactHandlerTest.createData();
    }    
    
    private static testMethod void testContactSingleMatch1(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	Case_Trigger_ContactHandlerTest.testContactSingleMatch1(settings.CC_Email_Case_Record_Type_IDs__c, settings.CC_Contact_Record_Type_IDs_to_Match__c, 'CC_Contact_Match_Status__c');
    }
    
    private static testMethod void testContactSingleMatch2(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	Case_Trigger_ContactHandlerTest.testContactSingleMatch2(settings.CC_Email_Case_Record_Type_IDs__c, settings.CC_Contact_Record_Type_IDs_to_Match__c, 'CC_Contact_Match_Status__c');    
    }    
    
    private static testMethod void testContactSingleDuplicateMatch(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	Case_Trigger_ContactHandlerTest.testContactSingleDuplicateMatch(settings.CC_Email_Case_Record_Type_IDs__c, settings.CC_Contact_Record_Type_IDs_to_Match__c, 'CC_Contact_Match_Status__c');        	
    }
    
    private static testMethod void testContactNoMatch1(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	Case_Trigger_ContactHandlerTest.testContactNoMatch1(settings.CC_Email_Case_Record_Type_IDs__c, settings.CC_Contact_Record_Type_IDs_to_Match__c, 'CC_Contact_Match_Status__c');        	
    }         
    
    private static testMethod void testContactNoMatch2(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	Case_Trigger_ContactHandlerTest.testContactNoMatch2(settings.CC_Email_Case_Record_Type_IDs__c, settings.CC_Contact_Record_Type_IDs_to_Match__c, 'CC_Contact_Match_Status__c');        	
    }    
}