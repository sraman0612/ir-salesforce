global class CC_PavilionClubCarContactsCtr {

  public CC_PavilionClubCarContactsCtr(CC_PavilionTemplateController controller) {}
  
  public Static List<IR_Employee__c> getrequiredContactsList(){
    List<IR_Employee__c> EmpList = [SELECT id, Business_Unit_Description__c, First_Name__c, Last_Name__c, Employee_Type__c, Location_Description__c, 
                                           Work_Email_Address__c, Office_Phone_Number__c, Department_Description__c, Phone_Extension__c
                                      FROM IR_Employee__c 
                                     WHERE Empl_Status__c = 'A' AND ((Business_Unit_Description__c = 'CLUB CAR' and Department_Code__c != 'USI1CLB005') OR 
                                           (Business_Unit_Description__c = 'CORPORATE' and  
                                            Location_Code__c='USGAEV01' AND 
                                            Department_Description__c != 'IT Infrastructure' AND 
                                            Department_Description__c != 'IT BusinessTransformation' AND 
                                            Employee_Type__c = 'S'))];   
    return EmpList;
  }
}