//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class SSO_AD_MatchOnCase implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
    /*
     * This only updates the Federation ID of the user. It does NOT create or update a user for any other reason.
     * The purpose of this was to eliminate a bug where users could not log in due to Case sensitivity between AD
     * Domain ID and SFDC Fed IDs
    */
 
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
     System.debug('##########Create User Called');   
        // SELECT does a case insensitive lookup by defauly
        User u = [SELECT FederationIdentifier, FirstName, LastName FROM User WHERE federationIdentifier=:federationIdentifier];

    if (null!=u)
        {
            u.FederationIdentifier = federationIdentifier;
            update(u);
        } 

        return u;
     }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
       	System.debug('##########Update User Called'); 
         //do nothing 
    }
}