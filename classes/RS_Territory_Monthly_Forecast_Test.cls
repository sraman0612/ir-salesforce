@isTest
private class RS_Territory_Monthly_Forecast_Test {

  // Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
  // Developer Name of the New Business Record Type
  public static final string RECORD_TYPE_NEW_BUSINESS = 'RS_Business';

  public static final Map<Integer, String> monthNamesMap = new Map<Integer, String>{
                                                                1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
                                                                5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
                                                                9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'};

  /*
  *		What page does
  *			getOpportunityStageNames();
  *			getPipelineAmounts();
  *			getRevenueSchedules();
  */

  /**
   *	Sets the data need for the tests
   */
  @testSetup static void mySetup() {

  }

  static testMethod void testForecast(){
    String checkQuery = '';
    // Set up the Account record.
    Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    String AccountName = 'Test Account';
    Account a = new Account(RecordTypeId = rshvacRTID,
                           Name = AccountName,
                           ShippingStreet = '500 Friarscourt Avenue',
                           ShippingCountry = 'USA',
                           ShippingState= 'IL',
                           ShippingPostalCode = '60563',
                           ShippingCity = 'Naperville',
                           County__c = 'Du Page');
    insert a;

    // sanity check on new account
    checkQuery = 'SELECT Name FROM Account WHERE Id = \'' + a.Id + '\'';
    List<Account> createdAccount = Database.query(checkQuery);
    System.assertEquals('Test Account', createdAccount[0].Name);

    String newAccountId = a.id;

    // Setup Opportunity
    Id newBusinessRTID = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_NEW_BUSINESS AND SObjectType = 'Opportunity' LIMIT 1].Id;
    String oppName = 'My Opportunity RS_Intentional_Sales_Call_Test_Opp';
    Opportunity opp = new Opportunity(Name = oppName, AccountId = a.id, RecordTypeId = newBusinessRTID, StageName = 'Prospecting', CloseDate = Date.today());
    insert opp;

    // check new opp
    checkQuery = 'SELECT Name FROM Opportunity WHERE Id = \'' + opp.Id + '\'';
    List<Opportunity> createdOpportunity = Database.query(checkQuery);
    System.assertEquals('My Opportunity RS_Intentional_Sales_Call_Test_Opp', createdOpportunity[0].Name);

    // Setup Page class
    Test.startTest();
      PageReference pageRef = Page.RS_Territory_Monthly_Forecast;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('id', a.id);
      RS_Territory_Monthly_Forecast sc = new RS_Territory_Monthly_Forecast();

      List<String> stages = sc.getOpportunityStageNames();
      System.assertNotEquals(stages[0], null);
      Account acc = sc.currentAccount;
      String currentMonth = sc.getCurrentMonthName();

      Date now = Date.today();
      Integer nowMonth = now.month();
      String expectedMonth = monthNamesMap.get(nowMonth);

      System.Assert(expectedMonth == currentMonth);

      List<sObject> pipelines = sc.getPipelineAmounts();
      //System.assertNotEquals(pipelines[0], null);

      List<Opportunity_Revenue_Schedule__c> opps = sc.getRevenueSchedules();
      //System.assertNotEquals(opps[0], null);

    Test.stopTest();
    System.assertEquals(DateTime.now().format('MMMMM'), currentMonth);
  }

}