//
// (c) 2015, Appirio Inc
//
// Test class for AccountTeam Trigger
//
// Aug 28,2015      Siddharth  Varshneya
//
@isTest
private class AccountTeam_TriggerHandler_Test {

    static testMethod void myUnitTest() {
    
       Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_OEM_EU' 
                                    and SobjectType='Account' limit 1].Id;
        // TO DO: implement unit test
        
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';
        acc.County__c = 'USA';
        acc.recordtypeid=AirNAAcctRT_ID;
        insert acc;
        
        
        Account_Team__c obj = new Account_Team__c();
        obj.Acct__c = acc.Id;
        obj.Team_Member__c = UserInfo.getUserId();
        obj.Role__c  = 'Sales Rep';
        obj.Acct_Access_Level__c = 'Edit';
        obj.Opportunity_Access_Level__c = 'Edit';
        obj.Case_Access_Level__c = 'Edit';
        obj.Contact_Access_Level__c = 'Edit';
        System.assert(obj.Role__c!=null);
        Test.startTest();
        insert obj;
        
        try{
        obj.Opportunity_Access_Level__c = 'Read';
        update obj;}
        catch(exception e){
            
        }
        
        delete obj;
        Test.stopTest();
        System.assertEquals('Read',obj.Opportunity_Access_Level__c);
    }
}