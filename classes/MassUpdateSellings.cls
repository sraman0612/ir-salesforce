global class MassUpdateSellings implements Database.Batchable<sObject>{
    
    string currentYear = string.valueof(system.today().year());
    string nextYear = string.valueof((system.today().year())+1);
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select Id,Account__c,PT_Account__c,PT_Year__c from Selling__c where PT_Year__c =\''+currentYear+'\' or PT_Year__c =\''+nextYear+'\' ';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, list<Selling__c> scope) {
        system.debug('scope '+scope.size()+' '+scope);
        Map<id,Selling__c> sellings = new Map<id,Selling__c>();    
        //Map<id,Year__c> years = new Map<id,Year__c>();   
        //Map<id,Year__c> newYears = new Map<id,Year__c>();
        List<id> ids = new List<id>();
        
        for(Selling__c sell : scope){
            /*if(sell.PT_Year__c==currentYear){
                ids.add(sell.Account__c);
            }else {
                sell.Account__c=sell.PT_Account__c;
                sellings.put(sell.id,sell);
            }*/
            
        }
        if(!ids.isempty()){
            /*List<Year__c> oldYears = [select id,Account__c,name from Year__c where Account__c in :ids and name=:currentYear];
            for(Year__c year : oldYears){
                years.put(year.Account__c,year);
            }
            for(id acc : ids){
                if(!years.containsKey(acc)){
                    //Year__c y = new Year__c(Account__c=acc,name=currentYear);
                    newYears.put(acc,y);
                }
            }
            if(!newYears.isempty()){
                insert newYears.values();
                for(Year__c y : newYears.values()){
                    years.put(y.Account__c,y);
                }
            }*/
            for(Selling__c sell : scope){
                /*if(sell.PT_Year__c==currentYear){
                    if(years.containsKey(sell.Account__c)){
                        sell.Year__c = years.get(sell.Account__c).id;
                    }
                    sell.Account__c=null;
                    sellings.put(sell.id,sell);
                }*/    
            }
        }
        if(!sellings.isEmpty()){
            update sellings.values();
            
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
        system.debug('**** Finish');
    }
    
    
}