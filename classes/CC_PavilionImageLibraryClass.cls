global class CC_PavilionImageLibraryClass{ 
  Public String zipLogoURL{get;set;}
  
  public CC_PavilionImageLibraryClass(CC_PavilionTemplateController controller){
      try{
            zipLogoURL='servlet/servlet.FileDownload?file='+[SELECT Id FROM Document WHERE Name = 'ZipFileLogo'].id;
            
        }
        catch(Exception e){
            system.debug(e);
        }
  }
  
  // Adding Tires
  @Remoteaction
  public static set<String> getTires(string aid){
    return getTireSets(aid);
  }
  
  // Adding Concopy
  @RemoteAction
  public static set<String> getConcopyCol(string aid){
    Set<String> CanOpy=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Canopy_Color__c,Canopy_Length__c,Tires__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            CanOpy.add(s.Canopy__C);
        }   
     }            
    return CanOpy;
  }
  
  @RemoteAction
  public static set<String> getConopyColr(string aid){
    Set<String> CanOpyclr=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Canopy_Color__c,Canopy_Length__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            CanOpyclr.add(s.Canopy_Color__c);
        }   
     }            
    return CanOpyclr;
  }
  
  @RemoteAction
  public static set<String> getConopyLength(string aid){
    Set<String> CanOpylen=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Canopy_Color__c,Canopy_Length__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            CanOpylen.add(s.Canopy_Length__c);
        }   
     }            
    return CanOpylen;
  }

  
  @RemoteAction
  public static set<String> getBoxes(string aid){
    Set<String> boxes=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Body_Color__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            boxes.add(s.Box__c);
        }   
     }            
    return boxes;
  }
  
  
  @RemoteAction
  public static set<String> getTypeBoxes(string aid){
    Set<String> Typeboxes=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Body_Color__c,Bed_Type__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            Typeboxes.add(s.Type_of_Box__c);
        }   
     }            
    return Typeboxes;
  }
  @RemoteAction
  public static set<String> getBoxColor(string aid){
    Set<String> Boxclr=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Body_Color__c,Bed_Type__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            Boxclr.add(s.Box_Color__c);
        }   
     }            
    return Boxclr;
  }
  
  @RemoteAction
  public static set<String> getBodyColor(string aid){
    Set<String> Bodyclr=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Body_Color__c,Bed_Type__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            Bodyclr.add(s.Body_Color__c);
        }   
     }            
    return Bodyclr;
  }
  
  @RemoteAction
  public static set<String> getBedType(string aid){
    Set<String> Bedtype=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Body_Color__c,Bed_Type__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            Bedtype.add(s.Bed_Type__c);
        }   
     }            
    return Bedtype;
  }
  
  @RemoteAction
  public static set<String> getWindows(string aid){
    Set<String> windowset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            windowset.add(s.Windows__c);
        }   
     }            
    return windowset;
  }
  @RemoteAction
  public static set<String> getAmbulance(string aid){
    Set<String> ambset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            ambset.add(s.Ambulance__c);
        }   
     }            
    return ambset;
  }
  //code contributed by @Priyanka Baviskar for issue no 7632211.
  @RemoteAction
  public static set<String> getWindshield(string aid){
    Set<String> winset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Windshield__c,  
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            winset.add(s.Windshield__c);
        }   
     }            
    return winset;
  }

  
  
  @RemoteAction
  public static set<String> getLadderRack(string aid){
    Set<String> ldrset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            ldrset.add(s.Ladder_Rack__c);
        }   
     }            
    return ldrset;
  }
  
  @RemoteAction
  public static set<String> getAccessories(string aid){
    Set<String> Accersset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            Accersset.add(s.Accessories__c);
        }   
     }            
    return Accersset;
  }
  
  @RemoteAction
  public static set<String> getsplfeatures(string aid){
    Set<String> splFetset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            splFetset.add(s.Special_Features__c);
        }   
     }            
    return splFetset;
  }
  
  @RemoteAction
  public static set<String> getPeople(string aid){
    Set<String> peopleset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            peopleset.add(s.People__C);
        }   
     }            
    return peopleset;
  }
  
  @RemoteAction
  public static set<String> getviewSet(string aid){
  Set<Id> grpmemIds=new Set<Id>();
   
    for(GroupMember gm:[SELECT Group.DeveloperName,GroupId,UserOrGroupId FROM GroupMember WHERE Group.DeveloperName='Custom_Solutions_Internal'])
    {
       grpmemIds.add(gm.UserOrGroupId); 
    }
    Set<String> viewsset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
           if(grpmemIds.contains(System.userInfo.getUserId()))
           { 
                viewsset.add(s.view__C);
           }
          if(!grpmemIds.contains(System.userInfo.getUserId()) && s.view__C== System.Label.Custom_Solution_View){
              viewsset.add(s.view__C);
          }
        }   
     }            
    return viewsset;
  }
  
   @RemoteAction
  public static set<String> getModels(string aid){
    Set<String> modelset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            modelset.add(s.Model__c);
        }   
     }            
    return modelset;
  }
  
  @RemoteAction
  public static set<String> getvehicles(string aid){
    Set<String> vehicleset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            vehicleset.add(s.Vehicle_Type__c);
        }   
     }            
    return vehicleset;
  }
  
  @RemoteAction
  public static set<String> getCowlColor(string aid){
    Set<String> cowlColset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            cowlColset.add(s.Cowl_Color__c);
        }   
     }            
    return cowlColset;
  }
  
  @RemoteAction
  public static set<String> getSeatColor(string aid){
    Set<String> SeatColset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            SeatColset.add(s.Seat_Color__c);
        }   
     }            
    return SeatColset;
  }
  
  @RemoteAction
  public static set<String> getAttribuet1(string aid){
    Set<String> attributeset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Attribute_1__c,Attribute_2__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            attributeset.add(s.Attribute_1__c);
        }   
     }            
    return attributeset;
  }
  
  @RemoteAction
  public static set<String> getAttribuet2(string aid){
  
    Set<String> attribute2set=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Attribute_1__c,Attribute_2__c,Canopy__C,Tires__c,Box__c,Canopy_Color__c,Box_Color__c,Ambulance__c,Ladder_Rack__c,Accessories__c,Special_Features__c,
                                            View__c,People__C,Windows__c,Type_of_Box__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            attribute2set.add(s.Attribute_2__c);
        }   
     }            
    return attribute2set;
  }
  
  @Remoteaction
  Public static set<ContentVersion> getImages(string region, string currncy,string aid){
    Id  usrId  = userinfo.getUserId();
    set<string> tires  = getTireSets(aid);
    set<string> contractSubTypes  = getcontractSubTypes(aid);
    set<String> ConOpies=getConcopyCol(aid);
    set<String> Concopyclr=getConopyColr(aid);
    set<String> Concopyleng=getConopyLength(aid);
    set<String> Boxset=getBoxes(aid);
    set<String> typeBoxset=getTypeBoxes(aid);
    set<String> Boxclrset=getBoxColor(aid);
    set<String> Bodyclrset=getBodyColor(aid);
    
    set<String> Bedtypeset=getBedType(aid);
    set<String> windowsset=getWindows(aid);
    set<string> Ambulset=getAmbulance(aid);
    set<string> windset=getWindshield(aid);
    set<string> laddrrackSet=getLadderRack(aid);
    set<string> accessoriesSet=getAccessories(aid);
    set<string> specialFeatSet=getsplfeatures(aid);
    set<string> peoplesSet=getPeople(aid);
    set<string> viewset=getviewSet(aid);
    set<String> modelSet =getModels(aid);
    set<String> vehicleSet=getvehicles(aid);
    set<String> cowlClrSet=getCowlColor(aid);
    set<String> seatClrSet=getSeatColor(aid);
    set<String> attr1Set=getAttribuet1(aid);
    set<String> attr2Set=getAttribuet2(aid);
    
    List<ContentVersion> acctFilteredImageinsLst = new List<ContentVersion>();
    set<ContentVersion> finalImageinSet = new set<ContentVersion>();
    String qryStr = 'SELECT id,Model__c,Canopy__C,Tires__c,Attribute_1__c,Attribute_2__c,Vehicle_Type__c,Windows__c,Agreement_Type__c,Ambulance__c,Canopy_Color__c,Box__c,Type_of_Box__c,Box_Color__c,Cowl_Color__c,Seat_Color__c,Ladder_Rack__c,Accessories__c,Special_Features__c,People__c,View__c,Canopy_Length__c,Windshield__c,Body_Color__c,Bed_Type__c,RecordType.Name ' +
                      'FROM ContentVersion '+
                       'WHERE RecordType.Name=\'Custom Solutions\'';
                     
    qryStr += ' and Currency__c INCLUDES(:currncy) and CC_Region__c INCLUDES(:region)';
    acctFilteredImageinsLst = Database.query(qryStr);
    System.debug('@@@@@acctFilteredImageinsLst '+acctFilteredImageinsLst );
   
    finalImageinSet.addAll(filterImagesByUserandAgreement(acctFilteredImageinsLst,contractSubTypes,tires,ConOpies,Concopyclr,Concopyleng,Boxset,typeBoxset,Boxclrset,Bodyclrset,Bedtypeset,windowsset,Ambulset,windset,
                                    laddrrackSet,accessoriesSet,specialFeatSet,peoplesSet,viewset,modelSet,vehicleSet,cowlClrSet,seatClrSet,attr1Set,attr2Set));
    System.debug('@@@@@Final Set '+finalImageinSet);
    return finalImageinSet;
  }
  
  /* get Imageins from full text search */
  
  
  Public static set<String> getTireSets(String acctId){
    
    Set<String> tirset=new Set<String>();
    List<ContentVersion> ContentList=[SELECT id,Model__c,Canopy__C,Tires__c,Canopy_Color__c,Vehicle_Type__c,Cowl_Color__c,Seat_Color__c,RecordType.Name
                              FROM ContentVersion WHERE RecordType.Name='Custom Solutions'];
    if(ContentList.size()>0 && !ContentList.isEmpty()){
        for(ContentVersion s : ContentList)
        {
            tirset.add(s.Tires__c);
        }   
     }                     
    return tirset;
  }
  
  public static set<String> getcontractSubTypes(String acctId){
    set<string> subtypeSet = new set<string>();
    Contract []  cLst = [SELECT id,name,CC_Sub_Type__c,CC_Type__c 
                           FROM Contract 
                           WHERE AccountId=:acctId and CC_Type__c='Dealer/Distributor Agreement' and 
                                 CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and 
                                 CC_Contract_End_Date__c >=:system.today()];
    for(Contract c: cLst){if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != ''){subtypeSet.add(c.CC_Sub_Type__c); }}                             
    return subtypeSet;
  }
  
  private static ContentVersion [] filterImagesByUserandAgreement(List<ContentVersion> cvLst,set<String> subTypes,set<String> tiresset,Set<string> setconOpy,Set<String> cpColor,Set<String> cpLength,
                                                                   set<String> setBoxes,Set<String> typeBoxset,Set<String> setBoxclr,Set<String> setBodyclr,Set<String> setBedtype,Set<String> setWindow,
                                                                   Set<String> ambset,Set<String> winset, Set<String> ldrset,Set<String> setAccrs,set<String> setSplFet,set<String> pepSet,
                                                                   set<string> viwSet,Set<String> Modelset,Set<String> vehicleSet,Set<String> cowlClrSet,Set<String> seatClrSet,Set<String> attr1Set,Set<String> attr2Set){
    List<ContentVersion> contentListToReturn = new List<ContentVersion>();
    List<ContentVersion> contentListUserFiltered = new List<ContentVersion>();
    System.debug('cvlistcvlist$$$$$$'+cvLst);
    System.debug('cvlistcvlistCount$$$$$$'+cvLst.size());
    System.debug('subTypes'+subTypes);
    
    Set<Id> grpmemIds=new Set<Id>();
   
    for(GroupMember gm:[SELECT Group.DeveloperName,GroupId,UserOrGroupId FROM GroupMember WHERE Group.DeveloperName='Custom_Solutions_Internal'])
    {
       grpmemIds.add(gm.UserOrGroupId); 
    }
    
    for(ContentVersion cv : cvLst){
        for(String s : cv.Agreement_Type__c.split(';')){
            if(subTypes.contains(s))
            {
                if( grpmemIds.contains(System.userInfo.getUserId()))
                {
                     contentListToReturn.add(cv);
                }
               if(!grpmemIds.contains(System.userInfo.getUserId()) && cv.View__C == System.Label.Custom_Solution_View)
                {
                    contentListToReturn.add(cv);
                }
               
                
            }
        }
    }
   
    
    
    return contentListToReturn;
    
  }
}