/* 
 *  Snehal Chabukswar || Date 01/07/2020 
 *  Called from Case_Trigger on before update event 
 *  Update case's caseAge  on case closer
 *  
*/
public class CTS_OM_UpdateCaseAge {
    public static void updateCaseAge(List<Case>newCaseList,Map<Id,Case>oldCaseMap){
        //Get default Business Hours Id
        //string defaultBusinessHourId = [SELECT Id, IsDefault, IsActive FROM BusinessHours WHERE IsDefault=true][0].id;
        //Map of  Id and record type Object of OM record types
        Map<Id,RecordType> OMRecordtypeMap = new Map<Id,RecordType>();
        for(RecordType rt:RecordTypeUtilityClass.getRecordTypeList('Case')){
            if(rt.DeveloperName.startsWith('CTS_OM') || rt.DeveloperName == 'Customer_Service') {
                system.debug('In If loop------>'+rt.Id);
                OMRecordtypeMap.put(rt.Id,rt);
            }
        }
        
        for(Case caseObj:newCaseList){
            String businessHoursId;
            Case oldCase = oldCaseMap.get(caseObj.Id);
            //Calculate Case age only if case status change to close record type is OM case record type
            system.debug('--------->'+ oldCase.IsClosed);
            system.debug('--------->'+caseObj.IsClosed );
            if((caseObj.Status=='Closed'|| caseObj.Status=='Cancelled'||caseObj.Status=='Resolved') && !oldCase.IsClosed  && OMRecordtypeMap.containsKey(caseObj.RecordTypeId)){
                system.debug('caseObj.BusinessHoursId---->'+caseObj.BusinessHoursId);
                if(caseObj.BusinessHoursId!=null){
                    businessHoursId = caseObj.BusinessHoursId;    
                }else{
                    businessHoursId = RecordTypeUtilityClass.defaultBusinessHour.Id; //defaultBusinessHourId;
                }
                //Case age calculation in ms with standard diff function of Business Hours 
                //Long diffMs= BusinessHours.diff(businessHoursId,caseObj.CreatedDate,caseObj.ClosedDate);
                Long diffMs= BusinessHours.diff(businessHoursId,caseObj.CreatedDate,System.Now());
                Double hours = diffMs/3600000.00;
                caseObj.Case_Age_Business_Hours__c = hours;
                //21/12/2021 - Updated as part of Delta Component
                String recordTypeName = OMRecordtypeMap.get(caseObj.RecordTypeId).DeveloperName;
                if(caseObj.Status=='Resolved' && hours>8 && caseObj.CTS_OM_SLA_Miss_Reason__c== null &&
                   (recordTypeName == 'CTS_OM_Americas_Transfers' || recordTypeName == 'CTS_OM_Americas' || recordTypeName == 'CTS_OM_ZEKS')
                  ){
                    caseObj.addError('CTS_OM_SLA_Miss_Reason__c',System.Label.CTS_OM_SLA_Miss_Reason_Error_Msg);   
                }
            }
        }
    }
    
}