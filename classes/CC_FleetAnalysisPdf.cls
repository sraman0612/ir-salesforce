public with sharing class CC_FleetAnalysisPdf {
    public CC_Fleet_Analysis__c fleetData{get;set;}
    public Id fleet_Id{get;set;}
    public List<CC_Asset_Analysis__c> listofAnalysis{get;set;}
    public flow.interview.ScheduleNextFleetAnalysis myflow {get;set;}
    
    public CC_FleetAnalysisPdf(){
        listofAnalysis = new List<CC_Asset_Analysis__c>();
        fleet_Id =  ApexPages.currentPage().getParameters().get('fleetId');
        fleetData = [select id,Name,Account__c,Account__r.Name,Asset__c,Asset__r.Name,Number_of_Assets_Analyzed__c,
                            Required_Number_of_Asset_to_Analyze__c,Owner.Name,Asset_Group_Quantity__c,Analysis_Date__c, Comments__c,
                            Contact__r.Name 
                       from CC_Fleet_Analysis__c 
                      where Id =:fleet_Id 
                      Limit 1];
        for(CC_Asset_Analysis__c asset_Ana : [select id,Fleet_Analysis__r.Name,Name,Decal_Number__c,Serial_Number__c,Model__r.Name, 
                                                     Status_formula__c, Status__c,Body_Cosmetics__c,Seats_Cosmetics__c,X360_Bumper_Cosmetics__c,
                                                     Canopy_Cosmetics__c,SAM_Cosmetics__c,Tires_Tread_Condition__c,Steering_Basic_Operation__c,
                                                     Accelerator_Basic_Operation__c,Brakes_Basic_Operation__c,Batteries_Water_Levels__c,
                                                     Oil_Level_Gas__c,Batteries_Engine_Cleanliness__c,Energy_Units__c,Internal_Comments__c,
                                                     Customer_Comments__c, Front_End_Alignment__c, Tread_Condition__c 
                                                from CC_Asset_Analysis__c 
                                               Where Fleet_Analysis__r.id = :fleet_Id AND Status__c ='Analysis Completed' 
                                            Order by Decal_Number__c Asc 
                                               Limit 1000]){
            listofAnalysis.add(asset_Ana);
        }
    }
    public String getendID() {      
    System.debug('@@@@@ the My flow :'+myflow); 
        if (myflow !=null){
            if(myflow.varNewFleetAnalysisID != null){
             return myflow.varNewFleetAnalysisID;
            }else{
                return myflow.recordid;
            }
        }
        else return fleet_Id;
    }       

    public PageReference getFinishLocation() {        
        PageReference endlocation = new PageReference('/' + getendID());
        return endlocation;
    }
}