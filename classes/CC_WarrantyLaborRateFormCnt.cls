/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */
public class CC_WarrantyLaborRateFormCnt {

 Public List < CC_Comparative_Dealer__c > dealerlist5 {
  get;
  set;
 }
 Public CC_Comparative_Dealer__c dealer {
  get;
  set;
 }
 public CC_Warranty_Labor_Rate__c warranty1 {
  get;
  set;
 }
 public User Usr {
  get;
  set;
 }
 public Contact contct {
  get;
  set;
 }
 public Account account {
  get;
  set;
 }

 public Date datename {
  get;
  set;
 }
 Public CC_Warranty_Labor_Rate__c warranty {
  get;
  set;
 }
 Public boolean dealerDetail {
  get;
  set;
 }
 Public String Warrenty {
  get;
  set;
 }



 public CC_WarrantyLaborRateFormCnt(CC_PavilionTemplateController controller) {
  try {
   usr = new User();
   contct = new Contact();
   account = new Account();

   Id userid = UserInfo.getUserId();
   usr = (User)[select Id, Contact.Id, Name, Phone, Extension, MobilePhone from User where Id = : userid limit 1];
   Id cid = usr.Contact.Id;
   if (cid != null)
    contct = (Contact)[select Id, Account.Id, HomePhone, MobilePhone, OtherPhone, Phone,CC_Account_Number__c from Contact where ID = : cid limit 1];
   Id accid;
   if (contct.Account.Id != null) {
    accid = contct.Account.Id;
    account = (Account)[select Id, AccountNumber, Name, Address_1__c, Address_2__c, Address_3__c from Account where Id = : accid limit 1];
   }
   warranty = new CC_Warranty_Labor_Rate__c();
   if (account.AccountNumber != null) warranty.CC_Dealer_No__c = account.AccountNumber;
   else warranty.CC_Dealer_No__c = '';
   if (account.Name != null) warranty.CC_Dealer_Name__c = account.Name;
   else warranty.CC_Dealer_Name__c = '';
   if (account.Address_1__c == null) account.Address_1__c = '';
   if (account.Address_2__c == null) account.Address_2__c = '';
   if (account.Address_3__c == null) account.Address_3__c = '';
   warranty.CC_Dealer_Address__c = account.Address_1__c + '\n' + account.Address_2__c + '\n' + account.Address_3__c;
   if (usr.Name != null) warranty.CC_Submitted_By_Print_Name__c = usr.Name;
   else warranty.CC_Submitted_By_Print_Name__c = '';
   if (contct.MobilePhone != null) warranty.CC_Phone_No__c = contct.MobilePhone;
   else warranty.CC_Phone_No__c = '';
  } catch (Exception e) {
   System.debug('Exception Occured' + e);
  } finally {}
  dealerDetail = false;
  system.debug('lllllllllllllllll' + datename);
  dealerlist5 = new List < CC_Comparative_Dealer__c > ();
  for (Integer i = 0; i < 5; i++) {
   CC_Comparative_Dealer__c temp = new CC_Comparative_Dealer__c();
   temp.CC_Address__c = '';
   temp.CC_Establishment_Name__c = '';
   temp.CC_Phone_Number__c = '';
   temp.CC_Service_Type__c = '';
   temp.CC_Shop_Rate__c = '';
   //temp.CC_Warranty_Labor_Rate__c = '';
   System.debug('I am Here');
   dealerlist5.add(temp);
  }
  System.debug(dealerlist5);
 }

 public PageReference SubmitInformation() {
  try {
   dealerDetail = false;
   warranty.CC_Date__c = date.valueof(apexpages.currentpage().getparameters().get('dt'));
   System.debug('^^^^^^^^^^^^^^^^^^warranty.CC_Date__c' + warranty.CC_Date__c);
   INSERT warranty;
   System.debug('^^^^^^^^^^^^^^^^^^warranty' + warranty.id);

  } catch (Exception e) {
   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'warranty insert' + e);
   ApexPages.addMessage(myMsg);
  }
  string dealerid = warranty.id;
  try {
   List < CC_Comparative_Dealer__c > selectedDealers = new List < CC_Comparative_Dealer__c > ();
   for (CC_Comparative_Dealer__c cd: dealerlist5) {
    if (cd.CC_Address__c != '' && cd.CC_Establishment_Name__c != '' && cd.CC_Phone_Number__c != '' && cd.CC_Service_Type__c != '' && cd.CC_Shop_Rate__c != '') {
     cd.CC_Warranty_Labor_Rate__c = warranty.Id;
     selectedDealers.add(cd);
    }
   }
   insert selectedDealers;
   System.debug('^^^^^^^^^^^^^^^^^^selectedDealers' + selectedDealers);
  } catch (Exception e) {
   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Dealer' + e);
   ApexPages.addMessage(myMsg);
  }
  // System.debug(selectedDealers);
  return null;
 }
 Public void previewInformation() {
  warranty.CC_Date__c = date.valueof(apexpages.currentpage().getparameters().get('prevwdt'));
  dealerDetail = true;

 }
 public void closepreviw() {

  dealerDetail = false;

 }


}