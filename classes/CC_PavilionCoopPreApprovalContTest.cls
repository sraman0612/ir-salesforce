@isTest
public class CC_PavilionCoopPreApprovalContTest {
    static testmethod void UnitTest_CoopPreApprovalCont()
    {
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contract testContract =new Contract();
        testContract.Name='test Contract';
        testContract.AccountId=acc.Id;
        testContract.CC_Type__c='Dealer/Distributor Agreement';
        testContract.CC_Sub_Type__c='Industrial Utility';
        testContract.CC_Contract_End_Date__c=System.today()+1;
        insert testContract;
        CC_Co_Op_Account_Summary__c ccas = TestUtilityClass.createCoopAccountSummary(testContract.Id);
        insert ccas;
        Test.startTest();
        CC_PavilionTemplateController controller;
        CC_PavilionCoopPreApprovalController PreAppCont =new CC_PavilionCoopPreApprovalController(controller);
        CC_PavilionCoopPreApprovalController.refreshTableBasedOnAggrement('Dealer/Distributor Agreement', acc.Id);
        CC_PavilionCoopPreApprovalController.refreshTableBasedOnAggrement('', '');
        CC_PavilionCoopPreApprovalController.submitCoopClaimData('Sample Event','London',String.valueOf(System.today().format()),String.valueOf(System.today().format()) , '100', '50', 'Laptops', 'Others', '50', '200', '2000', '2000', 'Yes', 'Yes',String.valueOf(System.today().addMonths(1).format()) , 'Yes', String.valueOf(System.today().addMonths(1).format()), '5000', 'Sample EventPlan',testContract.Id, 'Promotional Assistance Funds' ,'Sample EventPlan','', 'Promotional Assistance Funds', 'Yes', 'Test Rep', 'No', '100', 'Yes', '');
        //CC_PavilionCoopPreApprovalController.submitCoopClaimData('Sample Event','London',String.valueOf(System.today().format()),String.valueOf(System.today().format()) , '100', '50', 'Laptops', 'Others', '50', '200', '2000', '2000', 'Yes', 'Yes',String.valueOf(System.today().addMonths(1).format()) , 'Yes', String.valueOf(System.today().addMonths(1).format()), '5000', 'Sample EventPlan','', 'Promotional Assistance Funds','Sample EventPlan',testContract.Id, 'Promotional Assistance Funds');
        CC_PavilionCoopPreApprovalController.submitCoopClaimData('Sample Event','200','London@london.com','LondonEvent','London',String.valueOf(System.today().format()),String.valueOf(System.today().format()) , '100', '50', 'Laptops', 'Others', '50', '200', '2000', '2000', 'Yes', 'Yes',String.valueOf(System.today().addMonths(1).format()) , 'Yes', String.valueOf(System.today().addMonths(1).format()),'5000', 'Sample EventPlan',testContract.Id, 'Promotional Assistance Funds', 'Yes', 'Test Rep', 'Yes', '100', 'Yes', 'PRE-100');
        CC_PavilionCoopPreApprovalController.getAggrementTypes();
        Test.stopTest();
        
        
        
        
        
    }
}