/*
*
* $Revision: 1.0 $
* $Date: $
* $Author: $
*/
@isTest
public class CC_PavilionClaimStatusControllerTest {
    static testMethod void UnitTest_ClaimStatusController()
    { 
        // Creation Of Test Data
        Account sampleAccount = TestUtilityClass.createAccount();
        insert sampleAccount;
        
        System.assertEquals('Sample Account',sampleAccount.Name);
        System.assertNotEquals(null, sampleAccount.Id);
        Contract newContract = TestUtilityClass.createContract(sampleAccount.id);
        insert newContract;
        System.assertEquals(sampleAccount.Id,newContract.AccountId);
        System.assertNotEquals(null,newContract.Id);
        CC_Co_Op_Account_Summary__c newCoopAccountSummary = TestUtilityClass.createCoopAccountSummary(newContract.Id);
        insert newCoopAccountSummary;
        System.assertEquals(newContract.id,newCoopAccountSummary.Contract__c);
        System.assertNotEquals(null,newCoopAccountSummary.Id);
        CC_Order__c newOrder = TestUtilityClass.createOrder(sampleAccount.id);
        insert newOrder;
        System.assertEquals(newOrder.CC_Account__c,sampleAccount.Id);
        System.assertNotEquals(null,newOrder.Id);
        CC_Coop_Claims__c sampleCoopClaims = TestUtilityClass.createCoopClaimWithAccountSummary(newCoopAccountSummary.Id);
        sampleCoopClaims.CC_Status__c = 'Pending';
        insert sampleCoopClaims;
        System.assertEquals(sampleCoopClaims.Co_Op_Account_Summary__c,newCoopAccountSummary.Id);
        System.assertNotEquals(null,sampleCoopClaims.Id);
        Attachment at=new Attachment();
        at.name='Test.pdf';
        at.body=blob.valueof('Test.pdf');
        at.parentId=sampleCoopClaims.id;
        insert at;
        
        CC_PavilionTemplateController controller;
        CC_PavilionClaimStatusController newClaimStatus =new CC_PavilionClaimStatusController(controller);
        CC_PavilionClaimStatusController ClaimStatus =new CC_PavilionClaimStatusController();
        ClaimStatus.invoiceattachment.Name='Test.pdf';
        CC_PavilionClaimStatusController.tearsheetattachment.Name='sample.pdf';
        // Performing unit tests on CC_PavilionClaimStatusController methods.
        ClaimStatus.claimID=sampleCoopClaims.Id;
        CC_PavilionClaimStatusController.getClaimJson();  
        CC_PavilionClaimStatusController.getClaimByIdJson(sampleCoopClaims.Id);
        
        System.assertNotEquals('No data found',CC_PavilionClaimStatusController.getClaimByIdJson(sampleCoopClaims.Id));
        CC_PavilionClaimStatusController.getClaimByIdJson('');
        System.assertEquals('No data found',CC_PavilionClaimStatusController.getClaimByIdJson(''));
        //CC_PavilionClaimStatusController.submitCoopClaimData(sampleCoopClaims.Id,'new vender',String.valueOf(system.today().format()), '0', '123', 'Yes', 'Yes', '123', 'weqwzxv xc', 'Sponsorship', 'AttachmentBody', 'LogoAttachmentName', 'LogoAttachmentBody');       
        //CC_PavilionClaimStatusController.submitCoopClaimData('','new vender',String.valueOf(system.today().format()), '0', '123', 'Yes', 'Yes', 'Yes', '123', 'weqwzxv xc', 'Sponsorship', 'AttachmentBody', 'LogoAttachmentName', 'LogoAttachmentBody');
        ClaimStatus.invoicedate='22/04/1987';
        ClaimStatus.venderName='Test Venodr';
        ClaimStatus.numCompBrsnds='2';
        ClaimStatus.invoiceAmmount='100';
        ClaimStatus.copyOfInvoice='Yes';
        ClaimStatus.tearsheetOfAdd='Yes';
        ClaimStatus.ccLogoInAd='Yes';
        ClaimStatus.newClaimAmount='1000';
        ClaimStatus.comments='Test Comments';        
        ClaimStatus.submitPavCoopClaimData();    
       
    }
    
}