@isTest
public class CC_PavilionTechPubsCntTest {
    static testmethod void UnitTest_CC_PavilionTechPubsCnt()
    {
        // Creation Of Test Data
        ContentVersion ct = TestUtilityClass.createContentVersion('Tech Pubs');
        insert ct;
        Id Rectypid = [select Id from RecordType where Name='Tech Pubs' LIMIT 1].Id;
        System.assertEquals(Rectypid,ct.RecordTypeId);
        System.assertNotEquals(null, ct.Id);
        CC_PavilionTemplateController controller;
        /* Creating an instance for cc_PavilionTechPubsCnt */ 
        CC_PavilionTechPubsCnt obj = new CC_PavilionTechPubsCnt(controller);
        test.StartTest();
        obj.searchStr='';
        obj.testdate();
        CC_PavilionTechPubsCnt.getTechPubsData('test');
        System.assertNotEquals(null,CC_PavilionTechPubsCnt.getTechPubsData('test'));
        CC_PavilionTechPubsCnt.getYear();
        CC_PavilionTechPubsCnt.getSNPrefix();
        CC_PavilionTechPubsCnt.getType();
        /* Verify that method returns list of ContentVersion records */
        System.assertNotEquals(null,cc_PavilionTechPubsCnt.getType());
        CC_PavilionTechPubsCnt.getLanguage();
        CC_PavilionTechPubsCnt.getCSCategory();
        CC_PavilionTechPubsCnt.getTechPubsSearchData('test');
        CC_PavilionTechPubsCnt.getTechPubsDateSearchData('test',string.valueOf(system.today().format()),string.valueOf(system.today().addDays(1).format()));
        CC_PavilionTechPubsCnt.getTechPubsSearchData(' ');
        CC_PavilionTechPubsCnt.getTechPubsDateSearchData('test',' ',' ');
        test.StopTest();         
    }
}