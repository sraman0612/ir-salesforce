public class CC_PavilionSpeedCodeOrderCnt {

    public CC_PavilionSpeedCodeOrderCnt(CC_PavilionTemplateController ctlr){}

    @RemoteAction
    public static String serialNumCallout(String vsn, String csn) {
      String xmlString = '';
      String xmlResponse='';
      String upsertResponse='';
      Boolean vsnValid = false;
      Boolean csnValid = false;
      String returnMsg = 'OK';
      apexLogHandler log = new apexLogHandler('CC_PavilionSpeedCodeOrderCnt','speedModeCallout','');
      try {
        xmlString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:map="http://www.irco.com/MapicsSpeedCodes/">';      
        xmlString += '<soapenv:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">';
        xmlString += '<wsse:UsernameToken><wsse:Username>' + PavilionSettings__c.getInstance('wsseUsername').Value__c + '</wsse:Username>';
        xmlString += '<wsse:Password>' + PavilionSettings__c.getInstance('wssePassword').Value__c + '</wsse:Password></wsse:UsernameToken>';
        xmlString += '</wsse:Security></soapenv:Header><soapenv:Body><map:privateSpeedMode>'; 
        xmlString += generateTag('Integer', 'vsn', vsn.replaceAll('[^a-zA-Z0-9]','').toUpperCase());
        xmlString += generateTag('String', 'vsnValid', '');
        xmlString += generateTag('Integer', 'csn', '');  
        xmlString += generateTag('String', 'csnValid', '');
        string newCSN=csn==null||csn==''?'':csn;
        xmlString += generateTag('String', 'newCSN', newCSN); 
        xmlString += generateTag('String', 'cpc', '');
        xmlString += generateTag('String', 'upsertResponse', ''); 
        xmlString += generateTag('Date', 'fusionTrxId', System.Today());
        xmlString += '</map:privateSpeedMode></soapenv:Body></soapenv:Envelope>';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(PavilionSettings__c.getInstance('MAPICSSPEEDCODEENDPOINT').Value__c);
        req.setMethod('POST');
        req.setHeader('Content-Type','text/xml');
        req.setbody(xmlString);
        HttpResponse res;
        log.logRequestInfo(req.toString() + '\n' + req.getBody());
        res = h.send(req);
        xmlResponse = res.getBody();
        log.logResponseInfo(res.toString() + '\n' + xmlResponse);
        if (!(res.getStatus() == 'OK')) {
            returnMsg='The Private Speed Code service is currently experiencing difficulties.  Please try again later.';
        } else {
            XmlStreamReader reader = new XMLStreamReader(res.getBodyDocument().toxmlstring());
            while(reader.hasNext()) {
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                if (reader.getLocalName() == 'vsnValid') { vsnValid = parseXML(reader).trim()=='True' ? TRUE : FALSE;} 
                else if (reader.getLocalName() == 'csnValid') {csnValid = parseXML(reader).trim()=='True' ? TRUE : FALSE;}
                else if (reader.getLocalName() == 'csn'){returnMsg += '::' + parseXML(reader);} 
                else if (reader.getLocalName() == 'cpc') {
                    String cpc = parseXML(reader);
                    system.debug('### cpc is ' + cpc);
                    if(cpc.Length()==10){ // is vcm
                      returnMsg += '::' + cpc;
                    } else { //is iq
                      returnMsg += '::' + cpc.left(3) + '::' + cpc.mid(3,3) + '::' + cpc.right(3);
                    }
                } else if (reader.getLocalName() ==  'upsertResponse') {upsertResponse = parseXML(reader).trim();}
                }
                reader.next();
            }
            if (!(''==upsertResponse)){
                returnMsg=upsertResponse;
            } 
            else if (!vsnValid){
                returnMsg= 'Vehicle Serial Number is invalid.  Please check and re-enter.';
            } 
            else if(!csnValid) {
                returnMsg='Controller Serial Number is invalid.  Please check and re-enter.';
            }            
        }
      } catch(Exception e){ 
          log.logException(e);
          returnMsg='The Private Speed Code service is currently unavailable.  Please try again later.';
      }
      log.saveLog();
      return returnMsg;
    }
    
    @RemoteAction 
    public static String purchaseSpeedCode(String vsn, String csn,String custName, String poNumber, String codeA, String codeB, String codeC,String accId,Boolean suspended) {
      if (suspended){return System.Label.CC_Credit_Hold;}
      String returnMsg = 'OK';
      Savepoint sp = Database.setSavepoint();
      try {
        // insert order and order line
        CC_Order__c o = new CC_Order__c();
        o.isSystemCreated__c=TRUE;
        o.CC_Account__c=accId;
        o.CC_Status__c='Submitted';
        //o.PO__c=poNumber; 
        o.PO__c='PASSCODE ORDER-*';
        o.RecordTypeId = [SELECT Id FROM recordType WHERE sobjecttype='CC_Order__c' AND DeveloperName='CC_Parts_Ordering'].Id;
        Account a = [SELECT Id,Name,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry FROM Account WHERE Id =:accId LIMIT 1];
        o.CC_Address_line_1__c=a.ShippingStreet;
        o.CC_Addressee_name__c=a.Name;
        o.CC_City__c=a.ShippingCity;
        o.CC_States__c=a.ShippingState;
        o.CC_Postal_code__c=a.ShippingPostalCode;
        o.CC_Country__c=a.ShippingCountry;
        o.CC_Request_Date__c = system.today();
        o.CC_Warehouse__c='1';
        insert o;
        CC_Order_Item__c oli = new CC_Order_Item__c();
        oli.Order__c=o.Id;
        oli.CC_Quantity__c=1;
        oli.CC_Product_Code__c='PASSCODE';
        Product2 p = [SELECT Id, Description FROM Product2 WHERE ProductCode='PASSCODE'];
        oli.Product__c=p.Id; 
        oli.CC_Description__c=p.Description;
        oli.CC_UnitPrice__c=Decimal.valueOf(PavilionSettings__c.getInstance('PASSCODEPRICE').Value__c);
        oli.CC_TotalPrice__c=oli.CC_UnitPrice__c;
        oli.RecordTypeId = [SELECT Id FROM recordType WHERE sobjecttype='CC_Order_Item__c' AND DeveloperName='CC_Parts_Ordering'].Id;
        insert oli;
        CC_SpeedCodeHistory__c sch = new CC_SpeedCodeHistory__c();
        sch.CC_Account__c = accId;
        sch.Name = vsn;
        sch.CC_Controller_Serial_Numbr__c  = csn;
        sch.CC_Customer_Name__c = custName;
        sch.CC_Order_Date__c = system.today();
        sch.CC_Purchase_Order_Number__c = poNumber;
        sch.CC_Speed_Code_A__c = String.isNotBlank(codeA) ? decimal.valueOf(codeA) : null; 
        sch.CC_Speed_Code_B__c = String.isNotBlank(codeB) ? decimal.valueOf(codeB) : null;
        sch.CC_Speed_Code_C__c = String.isNotBlank(codeC) ? decimal.valueOf(codeC) : null;
        sch.CC_Order_Number__c=o.Id;
        insert sch;
        returnMsg += '::' + sch.Id;
      } catch (Exception e) {
        returnMsg = e.getMessage() + ' ' + e.getStackTraceString();
        Database.rollback(sp);
      }
      return returnMsg;
    }

    public static String generateTag(String dataType, String tagName, String value) {return '<' + tagName + '>' + (value != null ? value : '') + '</' + tagName +  '>';}
  
    public static String generateTag(String dataType, String tagName, DateTime value) {return '<' + tagName + '>' + (value != null ? value.format() : '') + '</' + tagName +  '>';}
  
    public static String generateTag(String dataType, String tagName, Decimal value) {return '<' + tagName + '>' + (value != null ? value : 0) + '</' + tagName +  '>';}  

    public static string parseXML(XmlStreamReader reader) {
      while(reader.hasNext()) {
        if (reader.getEventType() == XmlTag.CHARACTERS)
          return reader.getText();
        reader.next();
      }
      return 'NOXMLDATAFOUND';
    }
}