public class EMEIA_contactTriggerHandler {    
    public static void beforeInsert(List<Contact> newContact){
      Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contact');
      //CustomerContactPrimaryCheck
      CustomerContactPrimaryCheck.preventCreatePrimaryContactOnInsert(newContact);
      CustomerContactPrimaryCheck.updateFirstContactAsPrimaryContact(newContact);
    }
    
    //CustomerContactPrimaryCheck
    public static void beforeUpdate(Map<Id, Contact> contactNewMap, Map<id , Contact> contactOldMap){
        //trigger.newMap = contactNewMap,trigger.oldMap = contactOldMap      
        CustomerContactPrimaryCheck.preventPrimaryContactOnUpdate(contactNewMap,contactOldMap);
        
    }
      
      public static void afterUpdate(List<Contact> newContact){ // trigger.new = newContact
          //Contact_Trigger
          Map<Id, String> contactRTMap = new Map<Id, String>();
  		  Map<String, List<Contact>> SBUTriggerNewMap = new Map<String, List<Contact>>();
          for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType where SobjectType='Contact']) {
            contactRTMap.put(rt.Id,rt.DeveloperName);
            SBUTriggerNewMap.put(contactRTMap.get(rt.Id),new List<Contact>());
          }
          if (null!=newContact){for (Contact newC : newContact) {SBUTriggerNewMap.get(contactRTMap.get(newC.RecordTypeId)).add(newC);}}
          Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contact');
          if(!softDisable.updateDisabled()) {  
      		Contact_TriggerHandler.afterUpdate(SBUTriggerNewMap);
          }
      }
    
      public static void afterDelete(List<Contact> oldContact){ //trigger.old = oldContact
          //Contact_Trigger
          Map<Id, String> contactRTMap = new Map<Id, String>();
          Map<String, List<Contact>> SBUTriggerOldMap = new Map<String, List<Contact>>();
          for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType where SobjectType='Contact']) {
            contactRTMap.put(rt.Id,rt.DeveloperName);
            SBUTriggerOldMap.put(contactRTMap.get(rt.Id),new List<Contact>());
          }
          //add records into the SBU specific map
          if (null!=oldContact){for (Contact oldC : oldContact) {SBUTriggerOldMap.get(contactRTMap.get(oldC.RecordTypeId)).add(oldC);}}
          Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Contact');
          if(!softDisable.deleteDisabled()) {  
      		Contact_TriggerHandler.afterDelete(SBUTriggerOldMap);
      }
    }
}