@isTest
public with sharing class CC_Resubmit_Short_Term_Lease_BatchTest {
    
    @TestSetup
    private static void createData(){	
    	/*
        TestDataUtility dataUtility = new TestDataUtility();
            	
    	User user1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user1.Alias = 'alias123';
    	
    	User user2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user2.Alias = 'alias124';
    	
    	User user3 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user3.Alias = 'alias125';    
    	
    	insert new User[]{user1, user2, user3};	        

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;           
        
        Account acct = [Select Id From Account LIMIT 1];
        
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', user1.Id);
    	insert salesRep;                    
            
        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        masterLease.Customer__c = acct.Id;
        insert masterLease;  
        
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;            

    	CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
    	
        for (Integer i = 0; i < 15; i++){       
        	
        	Id userId;
        	
        	if (i < 5){
        		userId = user1.Id;
        	}
        	else if (i < 10){
        		userId = user2.Id;
        	}
        	else{
        		userId = user3.Id;
        	}
        	 	
        	CC_Short_Term_Lease__c l = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
        	l.Approval_Status__c = 'Approved'; 	
        	l.Unlock_Requested__c = true;
        	l.Unlock_Requestor__c = userId;
        	 	
			leases.add(l);  	
        }
        
        for (Integer i = 0; i < 5; i++){       
        	
        	CC_Short_Term_Lease__c l = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
        	l.Approval_Status__c = 'Approved'; 	
        	l.Unlock_Requested__c = false;
        	l.Unlock_Requestor__c = null;
        	 	
			leases.add(l);  	
        }         	
        
        insert leases;
        */
    }
    
    @isTest
    private static void testBatch(){
    	/*
    	CC_Short_Term_Lease__c[] leases = [Select Id, Approval_Status__c, Unlock_Requested__c, Unlock_Requestor__c From CC_Short_Term_Lease__c];
    	//system.assertEquals(20, leases.size());
    	
    	Integer unlockRequestCount = 0;
    	
    	for (CC_Short_Term_Lease__c l : leases){
    		
    		//system.assertEquals('Approved', l.Approval_Status__c);
    		
    		if (l.Unlock_Requested__c && l.Unlock_Requestor__c != null){
    			unlockRequestCount++;
    		}
    	}
    	
    	// Lock all of the leases before running the batch job
		Approval.lock(leases);
		    				  	*/
    	Test.startTest();
    	
    	Database.executeBatch(new CC_Resubmit_Short_Term_Lease_Batch(), 25);
    	
    	Test.stopTest();
    	/*
		// Verify the results
		leases = [Select Id, Approval_Status__c, Unlock_Requested__c, Unlock_Requestor__c From CC_Short_Term_Lease__c];
    	//system.assertEquals(20, leases.size());
    	
    	Integer unlockCount = 0;
    	
    	for (CC_Short_Term_Lease__c l : leases){
    		    		
    		if (l.Approval_Status__c == CC_Resubmit_Short_Term_Lease_Batch.defaultLeaseApprovalStatus && !l.Unlock_Requested__c && l.Unlock_Requestor__c == null){
    			unlockCount++;
    		}
    	}	
    	
    	//system.assertEquals(15, unlockCount);	    	
    	//system.assertEquals(15, EmailService.emailsSent);
    	*/
    }
}