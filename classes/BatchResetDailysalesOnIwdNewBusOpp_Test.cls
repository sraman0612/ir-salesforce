@isTest(seeAllData = false)
private class BatchResetDailysalesOnIwdNewBusOpp_Test{

    // Account Name
    public static final String ACCOUNT_NAME = 'Test Account';
    // Developer Name of the RS_HVAC Record type
   public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
   public static final String RS_HVAC_RT_OPPDEV_NAME = 'IWD_New_Business';
   public static RSHVAC_Properties__c IntegrationUser;
  /**
     *  Tests that the Batch executes properly
     */
    static testMethod void testBatchExecution() {
        // Insert a basic Account, all Oppty will be related to it
        Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
        Id rshvacoppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_OPPDEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;
        Account acc = new Account(Name = ACCOUNT_NAME, RecordTypeId = rshvacRTID);
        insert acc;
        Integer newBillTotal = 2000;
        Opportunity firstOpp = new Opportunity (Name ='opp1', Todays_Claim_Billed__c =0, AccountID = acc.Id, RecordTypeId =rshvacoppRTID, CloseDate=Date.today(), StageName='Request', Type = 'multi-Family - NC', Amount=10000,Competition__c = 'IWD', Effective_Date__c =Date.today(),Expiration_Date__c =Date.today(),Exp_Ship_Date__c=Date.today(), Gross_Sales__c= newBillTotal,IWD_Contact_Email__c='aaa@example.com' );
        Opportunity  secondOpp = new Opportunity (Name ='Opp2', Yesterdays_Claim_Billed__c= 0,Todays_Claim_Billed__c =0,  AccountID = acc.Id, RecordTypeId =rshvacoppRTID, CloseDate=Date.today(), StageName='Request', Type = 'multi-Family - NC', Amount=10000,Competition__c = 'IWD', Effective_Date__c =Date.today(),Expiration_Date__c =Date.today(),Exp_Ship_Date__c=Date.today(), Gross_Sales__c= 0,IWD_Contact_Email__c='bbb@example.com' );
        Opportunity   thirdOpp = new Opportunity (Name = 'opp3', AccountID = acc.Id, RecordTypeId =rshvacoppRTID, Todays_Claim_Billed__c =0,  CloseDate=Date.today(), StageName='Request', Type = 'multi-Family - NC', Amount=10000,Competition__c = 'IWD', Effective_Date__c =Date.today(),Expiration_Date__c =Date.today(),Exp_Ship_Date__c=Date.today(),Gross_Sales__c= 0 ,IWD_Contact_Email__c='ccc@example.com');
        IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
         insert IntegrationUser;
        if(IntegrationUser.Value__c.contains(Userinfo.getName())){
       // Do Nothing
        
        } else {
            insert new List<Opportunity  >{firstOpp, secondOpp, thirdOpp};
        }
        Set<Id> oppIds = new Set<Id>{firstOpp.Id, secondOpp.Id, thirdOpp.Id};
        // Verify that Billed today is not 0 for at least one of the oppty
        // Case 27331 Ronnie Stegall - We are no longer tracking todays claim billed.
        //System.assertEquals(true, [SELECT Id FROM Opportunity WHERE Id IN :oppIds AND Todays_Claim_Billed__c > 0].size() > 0);
        Test.startTest();
      // Instantiate and execute the batch
        BatchResetDailysalesOnIwdNewBusOpp batch = new BatchResetDailysalesOnIwdNewBusOpp();
         Database.executeBatch(batch);
            // Dummy call to add coverage
     //  BatchResetDailysalesOnIwdNewBusOpp batch2 = new BatchResetDailysalesOnIwdNewBusOpp('SELECT Id FROM Opportunity');
       Test.stopTest();
        // Get the updated Oppty
        for (Opportunity oppty : [SELECT Id, Yesterdays_Claim_Billed__c , Todays_Claim_Billed__c FROM Opportunity WHERE Id IN :oppIds]){
            // Billed Today should be 0 for all oppty
            System.assertEquals(0, oppty .Todays_Claim_Billed__c);
            if (oppty.Id == firstOpp.Id){
                // First Oppty should have newOrderTotal set as Billed yesterday
                // Case 27331 Ronnie Stegall - We are no longer tracking todays claim billed.
                //System.assertEquals(newBillTotal , oppty .Yesterdays_Claim_Billed__c );
            } else {
                // For the other oppty billed yesterday should be 0
                //System.assertEquals(0, oppty .Yesterdays_Claim_Billed__c );
            }
        }
  }
}