/*****************************************************************
Name:      RS_CRS_DocAttachment_TriggerHandler
Purpose:   Class used for Sending Email Alert[by enabling RS_CRS_Send_Attachment_Email__c to true] 
           If attachment is attaching Case object[Parent Object].
History                                                            
-------                                                            
VERSION     AUTHOR          DATE            DETAIL
1.0         Neela Prasad    12/26/2017      INITIAL DEVELOPMENT
*****************************************************************/
public class RS_CRS_DocAttachment_TriggerHandler {
    
    /******************************************************************************
    Purpose            :  Method used to send email on attachment                                                
    Parameters         :  Content document list
    Returns            :  void
    Throws [Exceptions]:                                                          
    ******************************************************************************/    
    public static void sendEmailOnAttachment(List<ContentDocumentLink> contDocNewList){
        //To make sure this Class is triggering for RS_CRS Record Types only.
        Id caseRecordTypeId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        List<case> listCase = New List<case>();
        
        //getting the Set of Case id's which attachments are added.
        Set<Id> caseIds = New Set<Id>();
        for(ContentDocumentLink contDoc : contDocNewList ){
            caseIds.add(contDoc.LinkedEntityId);
        }
        
        //Looping over the Correct case Id's and Correct reocrd Types. 
        for(Case caseObj : [select Id from case where id =: caseIds AND recordTypeId =: caseRecordTypeId]){
            //set RS_CRS_Send_Attachment_Email__c to send Email after attachment
            caseObj.RS_CRS_Send_Attachment_Email__c = true;
            listCase.add(caseObj);
        }
        
        //Updating ListCase to if we have found any records
        if(listCase.size() > 0){
            try{
                update listCase;
            } catch(Exception ex){
                System.debug('Error while sending email on attachment in class \'RS_CRS_DocAttachment_TriggerHandler\': \n'+ ex.getMessage());
            }
            
        }
    }
    
        

}