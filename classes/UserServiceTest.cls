@isTest
public with sharing class UserServiceTest {
    
    @TestSetup
    private static void createData(){
        
        User u1 = TestUtilityClass.createTestUserinternal();
        u1.LastName = 'Top Manager';
        update u1;

        User u2 = u1.clone(false, true);
        u2.LastName = 'Direct Report1';
        u2.UserName += '1';
        u2.ManagerId = u1.Id;

        User u3 = u1.clone(false, true);
        u3.LastName = 'Direct Report2';
        u3.UserName += '2';
        u3.ManagerId = u1.Id;        
        u3.DelegatedApproverId = u1.Id;

        insert new User[]{u2, u3};

        User u4 = u1.clone(false, true);
        u4.LastName = 'Indirect Report1';
        u4.UserName += '3';
        u4.ManagerId = u2.Id;    

        User u5 = u1.clone(false, true);
        u5.LastName = 'No Manager1';
        u5.UserName += '4';
        u5.ManagerId = null;      
        u5.DelegatedApproverId = u1.Id;

        insert new User[]{u4, u5};        
    }

    @isTest
    private static void testGetAllReportingToUsers(){

        User topMgr = [Select Id From User Where LastName = 'Top Manager'];
        
        Test.startTest();

        Set<User> reportingUsers = UserService.getAllReportingToUsers(topMgr.Id);
        system.assertEquals(3, reportingUsers.size());

        User[] orderedUsers = [Select Id, LastName From User Where Id in :reportingUsers Order By LastName ASC];

        system.assertEquals('Direct Report1', orderedUsers[0].LastName);
        system.assertEquals('Direct Report2', orderedUsers[1].LastName);
        system.assertEquals('Indirect Report1', orderedUsers[2].LastName);        

        Test.stopTest();
    }

    @isTest
    private static void testGetDelegateUsers(){

        User topMgr = [Select Id From User Where LastName = 'Top Manager'];
        
        Test.startTest();

        Set<User> delegateUsers = UserService.getDelegateUsers(topMgr.Id);
        system.assertEquals(2, delegateUsers.size());

        User[] orderedUsers = [Select Id, LastName From User Where Id in :delegateUsers Order By LastName ASC];

        system.assertEquals('Direct Report2', orderedUsers[0].LastName);
        system.assertEquals('No Manager1', orderedUsers[1].LastName);

        Test.stopTest();
    }    
}