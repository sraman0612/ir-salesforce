global class CC_PavilionInvoiceHistoryController {
  public CC_PavilionInvoiceHistoryController(CC_PavilionTemplateController controller) {}

  @ReadOnly
  @RemoteAction
  global static String getInvHistJson(String userAccountId,String yearFilter){   
    list<invoiceWrapper> Uniquelist = new list<invoiceWrapper>(); 
    Date endDate = system.today();
    Date startDate = endDate.addDays(-120);
    if(yearFilter!=''){
      Integer year = integer.valueOf(yearFilter);
      startDate = date.newInstance(year, 1, 1);
      endDate = date.newInstance(year, 12, 31);
    }
    
    system.debug('### Start Date and End Date ' + startDate + endDate);
    Long startMS = system.now().getTime();
    CC_Invoice2__c [] invLst = getInvoices(startDate,endDate,userAccountId);
    //CC_Invoice2__c [] invLst = getInvoices(startDate,endDate,'001j000000p5g2f'); // CCE
    //CC_Invoice2__c [] invLst = getInvoices(startDate,endDate,'001j000000p5gOH'); // Jeffrey Allen 
    DateTime queryendTime = system.now();
    /* system.debug('### invoice list contains this many records ' + invLst.size());
    system.debug('### query executed in ' + (queryendTime.getTime() - startMS) + 'MS');
    system.debug('### processing query results starting at ' + queryendTime); */
    string descSummary='';
    for(CC_Invoice2__c ccin : invLst){
      descSummary += ccin.CC_Item_Description__c;
      if(ccin.CC_SEQUENCE__c==1){
        Uniquelist.add(new invoiceWrapper(ccin.Name,ccin.CC_Invoice_Number__c,ccin.CC_Invoice_Date__c,ccin.CC_Sold_To__c,ccin.CC_Order_Number__c,
                                          ccin.CC_PO_Number__c,ccin.CC_Amount_Due__c,ccin.CC_Inv_Year__c,descSummary));
        descSummary='';
      }
    }
    DateTime processingEndTime = system.now();
  /*  system.debug('### processing end time is ' + processingEndTime);
    system.debug('### processing time was ' + (processingEndTime.getTime() - queryendTime.getTime()) + 'MS');*/
    String retStr = Uniquelist.isEmpty()?'No data found':(String)JSON.serialize(Uniquelist); 
    return retStr;
  }
  
  public static List<CC_Invoice2__c> getInvoices(Date sd, Date ed, String aid){
    //system.debug('### starting query at ' + system.now());
    CC_Invoice2__c [] retLst = [SELECT id, name, CC_Amount_Due__c, CC_Sold_To__c,CC_Order_Number__c,CC_Inv_Year__c,CC_Customer_Number__r.CC_Customer_Number__c,
                                       CC_Item_Description__c, CC_Invoice_Date__c,CC_Net_Due_Date__c, CC_invoice_Number__c, CC_PO_Number__c, CC_SEQUENCE__c 
                                  FROM CC_Invoice2__c
                                 WHERE CC_Customer_Number__c = :aid  AND 
                                       CC_Invoice_Date__c >=: sd AND
                                       CC_Invoice_Date__c <=: ed
                              ORDER BY Name, CC_SEQUENCE__c DESC];
    return retLst;
  }
  
  public class invoiceWrapper{
    public String invName{get;set;}
    public String invnum{get;set;}
    public Date invDate{get;set;}
    public String shipTo{get;set;}
    public String OrderNum{get;set;}
    public String poNum{get;set;}
    public Decimal Amount{get;set;}
    public String invYear{get;set;}
    public String invDesc{get;set;}
    public invoiceWrapper(String invName1,String invnum1, Date invDate1, String shipTo1, String OrderNum1, 
                          String poNum1, Decimal Amount1,String invYear1, String invDesc1){
      invName = invName1;
      invnum = invnum1;
      invDate = invDate1;
      shipTo = shipTo1;
      OrderNum = OrderNum1;
      poNum = poNum1;
      Amount = Amount1;
      invYear = invYear1;
      invDesc = invDesc1;
    } 
  }
}