/*
* Revisions:
18-Nov-2024 : Update code against Case #03273773 - AMS Team
*/
public class LGD_ApprovalProcess {
    private static Boolean approvalSubmitted= false;
    
    
    @InvocableMethod(label='Update Approver' description='Update Approvers in Opportunity' category='Opportunity')
    public static void updateApprover(List<ID> ids) {
        
        
        List<Opportunity> oppList = [SELECT Id, Discount__c,Discount_Requested__c, LGD_Channel__c, Business__c, 
                                     Approver_Level_2_1__c, Approver_Level_2_2__c, Region__c,
                                     Process__c, Product__c, Approver_Level_3__c, Approver_Level_3_1__c,
                                     Discounted_Price__c, Is_Approval_InProgress__c, CTS_Product__c, Amount // Removed Emco fields Case #03273773 Nov2024
                                     From Opportunity WHERE id IN :ids];
        
        
        Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt> approvalMdt = L_GD_COMP_Opportunity_Approval_Process__mdt.getAll();
        
        
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        List<String> userEmails  = new List<String>(); 
        
        System.debug('oppList >>'+oppList);
        Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt> mapOppMetadata = new Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt>();
        for(Opportunity opp :oppList){
            System.debug('List of oppList >>'+opp);
            for(L_GD_COMP_Opportunity_Approval_Process__mdt mdt : approvalMdt.values()){
                System.debug('Check metadata >>'+mdt);
                if( mdt.Business__c == opp.Business__c && mdt.Channel__c == opp.LGD_Channel__c 
                   && mdt.Region__c == opp.Region__c && mdt.Process__c == opp.Process__c 
                   && mdt.Product__c == opp.Product__c){
                       System.debug('Check metadata records>>'+mdt.Business__c);
                       System.debug('Check metadata records>>'+mdt.Region__c);
                       System.debug('Check metadata records>>'+mdt.Channel__c);
                       System.debug('Check metadata records>>'+mdt.Process__c);
                       System.debug('Check metadata records>>'+mdt.Product__c);
                       userEmails.add(mdt.Level_1__c);
                       userEmails.add(mdt.Level_2__c);
                       userEmails.add(mdt.Sales_Enablement__c);
                       userEmails.add(mdt.Level_3__c);
                       userEmails.add(mdt.Level_3_1__c);
                       mapOppMetadata.put(opp.Id, mdt); 
                   }
            }
            
        }
        
        System.debug('mapOppMetadata >>'+mapOppMetadata);
        System.debug('userEmails >>'+userEmails);
        List<User> users = [Select id, email From User Where email IN : userEmails];
        System.debug('users >>'+users);
        Map<String, Id> mapOppIdNextApprover = new Map<String, Id>();
        
        for(Opportunity opp :oppList){
            for(String oppId: mapOppMetadata.keySet()){
                if(oppId == opp.id){
                    for(User approvers : users){
                        if(approvers.email == mapOppMetadata.get(oppId).Level_1__c ) {
                            mapOppIdNextApprover.put(opp.id,approvers.id);
                        }
                        if(approvers.email == mapOppMetadata.get(oppId).Level_2__c ) {
                            opp.Approver_Level_2_1__c = approvers.id;
                        }
                        if(approvers.email == mapOppMetadata.get(oppId).Sales_Enablement__c ) {
                            opp.Approver_Level_2_2__c = approvers.id;
                        }
                        if(approvers.email == mapOppMetadata.get(oppId).Level_3__c ) {
                            opp.Approver_Level_3__c = approvers.id;
                        }
                        if(approvers.email == mapOppMetadata.get(oppId).Level_3_1__c ) {
                            opp.Approver_Level_3_1__c = approvers.id;
                        }
                    }  
                   System.debug('oppListToUpdate >>'+oppListToUpdate);
                    oppListToUpdate.add(opp);
                }
            }
            
        }
        
        
        try{
            System.debug('approvalSubmitted >>'+approvalSubmitted);
            if(!approvalSubmitted){
                Update oppListToUpdate;
                System.debug('After Update >>'+oppListToUpdate);
                List<Approval.ProcessSubmitRequest>   approvalRequests = new   List<Approval.ProcessSubmitRequest>();
                Approval.ProcessSubmitRequest approvalRequest;
                
                for(Opportunity oppty : oppListToUpdate){
                    Boolean checkDirectLevel2 = false; 
                    
                    if(((oppty.Region__c == 'Austria/Germany/Switzerland' || oppty.Region__c == 'Austria/Germany/Switzerland') && 
                        oppty.LGD_Channel__c == 'Distribution') || 
                       (oppty.Region__c == 'Norway' && oppty.LGD_Channel__c == 'Direct')){
                        checkDirectLevel2 = true;
                    }
                    System.debug('oppty.Discount_Requested__c >>'+oppty.Discount_Requested__c);
                    /* Commented the Emco code for Case #03273773 18 Nov 20204
                    //--start---US SGEM-60--
                    if((Oppty.EW_Product_Group__c == 'MLS' || Oppty.EW_Product_Group__c =='LLS') && Oppty.EW_Manufacturing_Location__c=='Germany') {
                        system.debug('approval entered'+ Oppty.EW_Product_Group__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setObjectId(oppty.Id);
                        approvalRequest.setProcessDefinitionNameOrId('EW_Germany_Quote_Approval_V1');
                        approvalRequests.add(approvalRequest);
                    }//--end---US SGEM-60-- */
                    if(oppty.Discount_Requested__c >= 3.01 && oppty.Discount_Requested__c <= 8.00 && !checkDirectLevel2){
                        //System.debug('oppty.Discount_Requested__c >>'+oppty.Discount_Requested__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from 3 - 8 %');
                        approvalRequest.setObjectId(oppty.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Opportunity_Discount_3_8_Percent_2');
                        approvalRequest.setSkipEntryCriteria(true);
                        approvalRequest.setNextApproverIds(new Id[] {mapOppIdNextApprover.get(oppty.id)});
                        approvalRequests.add(approvalRequest);
                    }
                    else if((oppty.Discount_Requested__c >= 8.01 && oppty.Discount_Requested__c <= 30.00) 
                            || (checkDirectLevel2 && oppty.Discount_Requested__c >= 3.01 
                                && oppty.Discount_Requested__c <= 8.00)){
                                
                        System.debug('oppty.Discount_Requested__c >>'+oppty.Discount_Requested__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from 8 - 30 %');
                        approvalRequest.setObjectId(oppty.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Opportunity_Discount_8_30_Percent_3');
                        approvalRequest.setSkipEntryCriteria(true);
                        System.debug('mapOppIdNextApprover.getoppty.id >>>'+mapOppIdNextApprover.get(oppty.id));
                        if(checkDirectLevel2){
                        //Assigning dummy User.
                        approvalRequest.setNextApproverIds(new Id[] {Userinfo.getUserId()});
 						}else{
                        approvalRequest.setNextApproverIds(new Id[] {mapOppIdNextApprover.get(oppty.id)});
  						}
                        approvalRequests.add(approvalRequest);
                    }
                    else if(oppty.Discount_Requested__c >= 30.01){
                        System.debug('oppty.Discount_Requested__c >>'+oppty.Discount_Requested__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from > 30 %');
                        approvalRequest.setObjectId(oppty.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Opportunity_Discount_more_than_30_1');
                        approvalRequest.setSkipEntryCriteria(true);
                        System.debug('mapOppIdNextApprover.getoppty.id >>>'+mapOppIdNextApprover.get(oppty.id));
                        if(checkDirectLevel2){
                        //Assigning dummy User.
                        approvalRequest.setNextApproverIds(new Id[] {Userinfo.getUserId()});
 						}else{
                        approvalRequest.setNextApproverIds(new Id[] {mapOppIdNextApprover.get(oppty.id)});
  						}
                        approvalRequests.add(approvalRequest);
                    }
                    
                    else{
                        return;
                    }
                    
                }
                System.debug('approvalRequests >>'+approvalRequests);
                // Submit the approval request
                Approval.ProcessResult[] result   =  Approval.process(approvalRequests);
                approvalSubmitted = True;
                
            }
            
        }catch(Exception ex){
           	
        }
    }
    
}