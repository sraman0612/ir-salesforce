@isTest
public class CC_STL_Car_Accessory_Line_Item_Test {

    @TestSetup
    private static void createTestData(){
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';
        
        insert new Product2[]{prod1, prod2};
            
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;
            
    	CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
    	insert masterLease;
    	
    	TestDataUtility dataUtil = new TestDataUtility();
    	
        User testUser = dataUtil.createIntegrationUser();
        testUser.LastName = 'Test_User_123';
        insert testUser;       	
    	
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
    	insert salesRep;        	
    	
    	CC_Short_Term_Lease__c stl = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
    	insert stl;        
            
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl.Id;
        car1.Quantity__c = 10;
        car1.Per_Car_Cost__c = 25; 
                
        CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod2.Id);
        car2.Short_Term_Lease__c = stl.Id;
        car2.Quantity__c = 10;
        car2.Per_Car_Cost__c = 25; 
                
        insert new CC_STL_Car_Info__c[]{car1, car2};
            
        CC_STL_Car_Accessory__c accessory1 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Deluxe Towbar', 500);
        CC_STL_Car_Accessory__c accessory2 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Deluxe Towbar', 500);
        CC_STL_Car_Accessory__c accessory3 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Sandbottle', 500);
        CC_STL_Car_Accessory__c accessory4 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Sandbottle', 500);
        CC_STL_Car_Accessory__c accessory5 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Portable Refreshment Center', 500);
        CC_STL_Car_Accessory__c accessory6 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Portable Refreshment Center', 500);
        insert new CC_STL_Car_Accessory__c[]{accessory1, accessory2, accessory3, accessory4, accessory5, accessory6};
            
        // Assign 1 accessory to each of the cars to start with
        CC_STL_Car_Accessory_Line_Item__c lineItem1 = TestDataUtility.createSTLCarAccessoryLineItem(car1.Id, accessory1.Id, 500, 1);
    	CC_STL_Car_Accessory_Line_Item__c lineItem2 = TestDataUtility.createSTLCarAccessoryLineItem(car2.Id, accessory2.Id, 500, 1);
        insert new CC_STL_Car_Accessory_Line_Item__c[]{lineItem1, lineItem2};
    }
    
    @isTest
    private static void testPreventDeletionsOnActiveLeases(){
        
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCar = [Select Id From CC_STL_Car_Info__c Where Item__c = :prod.Id]; 
        CC_STL_Car_Accessory_Line_Item__c[] lineItems = [Select Id From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id];
		
        system.assertEquals(1, lineItems.size());
        
        // Update the lease to something that does not allow changes
        CC_Short_Term_Lease__c stl = [Select Id,Synced_to_MAPICS__c From CC_Short_Term_Lease__c];
        stl.Synced_to_MAPICS__c = TRUE;
        update stl;
        
        // Confirm the line item cannot be deleted
        Boolean deleteFailed = false;
        
        try{
            delete lineItems[0];
        }
        catch(Exception e){
            if (e.getMessage().contains(CC_STL_Car_Accessory_Line_Item_Handler.deleteAccessoryActiveLeaseErrorMsg)){
            	deleteFailed = true;
            }
        }
        
        system.assertEquals(true, deleteFailed);
        
        // Change the lease to something that allows changes
       	stl.Synced_to_MAPICS__c = FALSE;
        update stl;    
        
        // Delete the line item
        delete lineItems[0];
    }
    
    @isTest
    private static void testRollupAccessoryLaborCostsNoLineItems(){
        
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Item__c = :prod.Id]; 
        AggregateResult aggResult = [Select SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id];
        CC_STL_Car_Accessory_Line_Item__c[] lineItems = [Select Id From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id];
		
        system.assertEquals((Double)aggResult.get('Total_Labor_Cost'), stlCar.Accessory_Labor_Cost__c); 
        
        delete lineItems;
        
        stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Item__c = :prod.Id]; 

        system.assertEquals(0, stlCar.Accessory_Labor_Cost__c);     
    }    
    
    @isTest
    private static void testRollupAccessoryLaborCosts(){
                
        // Verify the current accessory labor costs on each STL car
        AggregateResult[] aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Group By STL_Car__c];
		
        Map<Id, CC_STL_Car_Info__c> stlCars = new Map<Id, CC_STL_Car_Info__c>([Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c]);
        
        system.assertEquals(2, stlCars.size());
        
        for (AggregateResult aggResult : aggResults){
            system.assertEquals((Double)aggResult.get('Total_Labor_Cost'), stlCars.get((Id)aggResult.get('STL_Car__c')).Accessory_Labor_Cost__c);
        }
        
        // Insert a new line item and change the accessory on an existing one
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCar = [Select Id From CC_STL_Car_Info__c Where Item__c = :prod.Id];
        CC_STL_Car_Accessory__c[] availableAccessories = [Select Id From CC_STL_Car_Accessory__c Where Product__c = :prod.Id and Id not in (Select STL_Car_Accessory__c From CC_STL_Car_Accessory_Line_Item__c)];

        CC_STL_Car_Accessory_Line_Item__c lineItem1 = [Select Id, Total_Labor_Cost__c From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__r.Item__c = :prod.Id LIMIT 1];
        lineItem1.STL_Car_Accessory__c = availableAccessories[0].Id;
        
        CC_STL_Car_Accessory_Line_Item__c lineItem2 = TestDataUtility.createSTLCarAccessoryLineItem(stlCar.Id, availableAccessories[1].Id, 500, 1);  
        
        upsert new CC_STL_Car_Accessory_Line_Item__c[]{lineItem1, lineItem2};
            
        // Verify the updated accessory labor cost
        aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id Group By STL_Car__c];
        stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Id = :stlCar.Id];
        
        system.assertEquals((Double)aggResults[0].get('Total_Labor_Cost'), stlCar.Accessory_Labor_Cost__c);
        
        // Delete one of the line items
        delete lineItem2;
        
        // Verify the updated accessory labor cost
        aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id Group By STL_Car__c];
        stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Id = :stlCar.Id];        
        
        system.assertEquals((Double)aggResults[0].get('Total_Labor_Cost'), stlCar.Accessory_Labor_Cost__c);

		// Restore the deleted record from the recycle bin
		undelete lineItem2;        
        
        // Verify the updated accessory labor cost
        aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id Group By STL_Car__c];
        stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Id = :stlCar.Id];        
        
        system.assertEquals((Double)aggResults[0].get('Total_Labor_Cost'), stlCar.Accessory_Labor_Cost__c); 
        
        // Change the quantity
        lineItem1.Quantity__c = 2;
        update lineItem1;
        
        // Verify the updated accessory labor cost
        aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Total_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCar.Id Group By STL_Car__c];
        stlCar = [Select Id, Accessory_Labor_Cost__c From CC_STL_Car_Info__c Where Id = :stlCar.Id];        
        
        system.assertEquals((Double)aggResults[0].get('Total_Labor_Cost'), stlCar.Accessory_Labor_Cost__c);         
        
        // Verify no errors were logged
        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }    
    
    @isTest
    private static void testPreventDuplicateCarAccessories(){
        
        // Verify there are 2 assignments right now
        CC_STL_Car_Accessory_Line_Item__c[] lineItems = [Select STL_Car__c, STL_Car_Accessory__c, STL_Car__r.Item__c From CC_STL_Car_Accessory_Line_Item__c];
        system.assertEquals(2, lineItems.size());
        
        // Insert a duplicate
        CC_STL_Car_Accessory_Line_Item__c lineItem1 = TestDataUtility.createSTLCarAccessoryLineItem(lineItems[0].STL_Car__c, lineItems[0].STL_Car_Accessory__c, 500, 1);
        
        Boolean insertFailed = false;
        
        try{
            insert lineItem1;
        }
        catch(Exception e){
            system.assert(e.getMessage().contains(CC_STL_Car_Accessory_Line_Item_Handler.duplicateAccessoryErrorMsg));
            insertFailed = true;
        }
        
        system.assertEquals(true, insertFailed);
        
        // Insert a valid assignment
        CC_STL_Car_Accessory__c[] availableAccessories = [Select Id From CC_STL_Car_Accessory__c Where Product__c = :lineItems[0].STL_Car__r.Item__c and Id not in (Select STL_Car_Accessory__c From CC_STL_Car_Accessory_Line_Item__c)];
        
        CC_STL_Car_Accessory_Line_Item__c lineItem2 = TestDataUtility.createSTLCarAccessoryLineItem(lineItems[0].STL_Car__c, availableAccessories[0].Id, 500, 1);
		insert lineItem2;   
        
        // Update the new line item making it a duplicate
        system.debug('--update failure test--');
        Boolean updateFailed = false;
        
        CC_STL_Car_Accessory_Line_Item__c lineItem3;
        
        try{
            
            lineItem3 = lineItem2.clone(false, true);
			insert lineItem3;
        }
        catch(Exception e){
            system.assert(e.getMessage().contains(CC_STL_Car_Accessory_Line_Item_Handler.duplicateAccessoryErrorMsg));
            updateFailed = true;
        }     
        
        system.assertEquals(true, updateFailed);
        
        // Change the accessory to a new available one
        lineItem3.STL_Car_Accessory__c = availableAccessories[1].Id;
        insert lineItem3;
    }
}