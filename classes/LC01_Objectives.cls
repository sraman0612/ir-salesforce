public with sharing class LC01_Objectives {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Controller for Lightning component: LC01_Objectives
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	@AuraEnabled
	public static Map<String, Object> retrieveObjectives(String accountId, String pt_dsmp_id){
		Map<String, Object> mapResult = new Map<String, Object>();
		List<PT_DSMP_Objectives__c> lstSuggestedObj = new List<PT_DSMP_Objectives__c>();
		List<PT_DSMP_Objectives__c> lstFreeObj = new List<PT_DSMP_Objectives__c>();

		try {

			for(PT_DSMP_Objectives__c current : [SELECT Id,
														Objective__c,
														Objectives_Number__c,
														PT_DSMP__c
												FROM PT_DSMP_Objectives__c
												WHERE PT_DSMP__c =: pt_dsmp_id
													  AND Account__c =: accountId
												ORDER BY Objectives_Number__c]){

				if(lstSuggestedObj.size() < 3)
					lstSuggestedObj.add(current);
				else
					lstFreeObj.add(current);

			}	

			mapResult.put('error', false);
			mapResult.put('lstSuggestedObj', lstSuggestedObj);
			mapResult.put('lstFreeObj', lstFreeObj);
			return mapResult;

		} catch(Exception e){
			mapResult.put('error', true);
			mapResult.put('message', e.getMessage());
			return mapResult;
		}

	}


	@AuraEnabled
	public static Map<String, Object> updateObjectives(List<PT_DSMP_Objectives__c> lstObjective, List<PT_DSMP_Objectives__c> lstSuggested){
		Map<String, Object> mapResult = new Map<String, Object>();
		List<PT_DSMP_Objectives__c> lstObjectiveToSave = new List<PT_DSMP_Objectives__c>();

		PT_DSMP_CS_Table_Plan__c ptrt = PT_DSMP_CS_Table_Plan__c.getOrgDefaults();

		try {

			for(PT_DSMP_Objectives__c current : lstSuggested){

				lstObjectiveToSave.add(new PT_DSMP_Objectives__c(Id = current.Id,
																Objective__c = current.Objective__c));

			}

			for(PT_DSMP_Objectives__c current : lstObjective){

				lstObjectiveToSave.add(new PT_DSMP_Objectives__c(Id = current.Id,
																Objective__c = current.Objective__c));

			}

			//System.debug('mgr lstObjectiveToSave ' + lstObjectiveToSave);

			if(lstObjectiveToSave.size() > 0)
				update lstObjectiveToSave;

			mapResult.put('error', false);
			//mapResult.put('message', 'Objectives saved!');
			//mapResult.put('message', Label.PT_DSMP_TablePlan_ObjectiveSaved);
			
			mapResult.put('message', ptrt.TablePlan_ObjectiveSaved__c);

			return mapResult;

		} catch(Exception e){
			mapResult.put('error', true);
			//mapResult.put('message', 'Salesforce error. Please contact your System Administrator.');
			//mapResult.put('message', Label.PT_DSMP_TablePlan_SalesforceError);
			mapResult.put('message', ptrt.TablePlan_SalesforceError__c);

			String msg = e.getMessage();
			if(msg.contains('row ') && msg.contains('Objective__c')){
				List<String> lstmsg = new List<String>();
				lstmsg = msg.split('row');

				if(lstmsg.size() > 1){
					msg = lstmsg[1].substring(1,2);

					try {
						Integer rowNumber = Integer.valueOf(msg);
						mapResult.put('message', e.getMessage());

						mapResult.put('message', 'Objective Number '+rowNumber+1+ ': must be of minimum length of 20 characters.');
					} catch(Exception re){

					}
				}

			}
			return mapResult;
		}

	}

}