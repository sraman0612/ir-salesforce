public with sharing class CC_PavilionDealerHomeCtrl {
  
  /* member variables */
  public static List < CC_Pavilion_Content__c > content {get;set;}
  public CC_Pavilion_Content__c marketingcontent {get;set;}
  public CC_Pavilion_Content__c customerexperience {get;set;}
  public CC_Pavilion_Content__c technicalpublications {get;set;}
  
 public CC_Pavilion_Content__c customsolutions {
  get;
  set;
 }
 public CC_Pavilion_Content__c channelcomm {
  get;
  set;
 }
 public CC_Pavilion_Content__c partsbull {
  get;
  set;
 }
 public CC_Pavilion_Content__c pricing {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty1 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty2 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty3 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty4 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty5 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty6 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty7 {
  get;
  set;
 }
 public CC_Pavilion_Content__c ctrlwarrenty8 {
  get;
  set;
 }
 public CC_Pavilion_Content__c prodsupp {
  get;
  set;
 }
 public CC_Pavilion_Content__c sales1 {
  get;
  set;
 }
 public CC_Pavilion_Content__c sppolicy1 {
  get;
  set;
 }
 public CC_Pavilion_Content__c sppolicy2 {
  get;
  set;
 }
 public CC_Pavilion_Content__c sppolicy3 {
  get;
  set;
 }
 public CC_Pavilion_Content__c sppolicy4 {
  get;
  set;
 }
 public CC_Pavilion_Content__c sppolicy5 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance1 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance2 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance3 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance4 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance5 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance6 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance7 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance8 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance9 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance10 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance11 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance12 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance13 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance14 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance15 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance16 {
  get;
  set;
 }
 public CC_Pavilion_Content__c finance17 {
  get;
  set;
 }
 public CC_Pavilion_Content__c homedealer {
  get;
  set;
 }
 public CC_Pavilion_Content__c psts {
  get;
  set;
 }

 public Boolean marketingcontentRender {
  get;
  set;
 }
 public Boolean customerexperienceRender {
  get;
  set;
 }
 public Boolean technicalpublicationsRender {
  get;
  set;
 }
 public Boolean customsolutionsRender {
  get;
  set;
 }
 public Boolean channelcommRender {
  get;
  set;
 }
 public Boolean partsbullRender {
  get;
  set;
 }
 public Boolean pricingRender {
  get;
  set;
 }
 public Boolean ctrlwarrenty1Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty2Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty3Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty4Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty5Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty6Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty7Render {
  get;
  set;
 }
 public Boolean ctrlwarrenty8Render {
  get;
  set;
 }
 public Boolean prodsuppRender {
  get;
  set;
 }
  public Boolean salesRender {
  get;
  set;
 }
 public Boolean sales1Render {
  get;
  set;
 }
 public Boolean sppolicy1Render {
  get;
  set;
 }
 public Boolean sppolicy2Render {
  get;
  set;
 }
 public Boolean sppolicy3Render {
  get;
  set;
 }
 public Boolean sppolicy4Render {
  get;
  set;
 }
 public Boolean sppolicy5Render {
  get;
  set;
 }
 public Boolean finance1Render {
  get;
  set;
 }
 public Boolean finance2Render {
  get;
  set;
 }
 public Boolean finance3Render {
  get;
  set;
 }
 public Boolean finance4Render {
  get;
  set;
 }
 public Boolean finance5Render {
  get;
  set;
 }
 public Boolean finance6Render {
  get;
  set;
 }
 public Boolean finance7Render {
  get;
  set;
 }
 public Boolean finance8Render {
  get;
  set;
 }
 public Boolean finance9Render {
  get;
  set;
 }
 public Boolean finance10Render {
  get;
  set;
 }
 public Boolean finance11Render {
  get;
  set;
 }
 public Boolean finance12Render {
  get;
  set;
 }
 public Boolean finance13Render {
  get;
  set;
 }
 public Boolean finance14Render {
  get;
  set;
 }
 public Boolean finance15Render {
  get;
  set;
 }
 public Boolean finance16Render {
  get;
  set;
 }
 public Boolean finance17Render {
  get;
  set;
 }
 public Boolean homedealerRender {
  get;
  set;
 }
 public Boolean pstsRender {
  get;
  set;
 }


 public map < string, string > accountmap {
  get;
  set;
 }
 public List < string > agrType {
  get;
  set;
 }
 public List < Contract > cont {
  get;
  set;
 }
 public String accountId;
 public Boolean financeRender{get;set;}

  /* constructor */
  public CC_PavilionDealerHomeCtrl(CC_PavilionTemplateController controller) {
    String Region=controller.globalRegion;
    accountmap = new map < string, string > ();
    cont = new List < Contract > ();
    agrType = new List < string > ();
    Boolean isInternal=controller.acctId==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
    cont = [SELECT id, name, CC_Sub_Type__c, CC_Type__c from Contract where AccountId = : controller.acctId AND CC_Type__c = 'Dealer/Distributor Agreement'];
    for (Contract c: cont) {
     if (c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '') {
      accountmap.put(c.CC_Sub_Type__c, '');
     }
    }

    content = [select Id, Name, Title__c, Type__c, CC_Body_1__c, CC_Body_2__c, CC_Body_3__c, CC_Body_4__c, Agreement_Type__c,CC_Region__c from CC_Pavilion_Content__c where Type__c = 'Dealer Manual' ];
    for (CC_Pavilion_Content__c c: content) {
      if (c.Title__c == 'Marketing Co-op Advertising') {marketingcontent = c;}
      if (c.Title__c == 'Customer Experience') { customerexperience = c;}
      if (c.Title__c == 'Technical Publications') {technicalpublications = c;}
      if (c.Title__c == 'Custom Solutions') {customsolutions = c;}
      if (c.Title__c == 'Channel Communications') {channelcomm = c;}
      if (c.Title__c == 'Dealer Manual Home') {homedealer = c;}
      if (c.Title__c == 'Parts Bulletins') {partsbull = c;}
      if (c.Title__c == 'Product Support Technical Services') {psts = c;}
      if (c.Title__c == 'Warranty Home') {ctrlwarrenty1 = c;}
      if (c.Title__c == 'Warranty_Battery Testing (6v,8v,12v)') {ctrlwarrenty2 = c;}
      if (c.Title__c == 'Warranty_Club Car Warranty & Parts Labels') {ctrlwarrenty3 = c;}
      if (c.Title__c == 'Warranty_Club Car Warranty Management System Claim Processing Overview') {ctrlwarrenty4 = c;}
      if (c.Title__c == 'Warranty_Warranty Claim submission') {ctrlwarrenty5 = c;}
      if (c.Title__c == 'Warranty_Warranty Compensation') {ctrlwarrenty6 = c;}
      if (c.Title__c == 'Warranty_Warranty Introduction') {ctrlwarrenty7 = c;}
      if (c.Title__c == 'Warranty_Warranty Policies') {ctrlwarrenty8 = c;}
      if (c.Title__c == 'Sales') {
              sales1 = c;
              salesRender = true;
            }
      if (c.Agreement_Type__c != null) {
        agrType = c.Agreement_Type__c.split(';');
        for (string s: agrType) {
          if ((accountmap.containskey(s) && c.CC_Region__c==Region) || isInternal) { 
            String ser = 'Service Parts Policies Procedures Intl';
            if (c.Title__c == ser) {
              sppolicy1 = c;
              sppolicy1Render=true;
              sales1Render=true;
            }
            if (c.Title__c == 'Service Parts Policies Procedures') {
              sppolicy2 = c;
              sppolicy2Render=true;
              sales1Render=true;
            }
            if (c.Title__c == 'Service Parts Sales Programs – dealers' && controller.acctType=='Dealer') {
              sppolicy3 = c;
              sppolicy3Render=true;
              sales1Render=true;
            }
          
            if (c.Title__c == 'Service Parts Sales Programs Distributor'&& controller.acctType=='Distributor') {
              sppolicy4 = c;
              sppolicy4Render=true;
              sales1Render=true;
            }
            if (c.Title__c == 'Service Parts Sales Programs International') {
              sppolicy5 = c;
              sppolicy5Render=true;
              sales1Render=true;
            }
            if (c.Title__c == 'Pricing Bulletins') {
             pricing = c;
             pricingRender = true;
            }
           /* if (c.Title__c == 'Sales') {
              sales1 = c;
              salesRender = true;
            }*/
            if (c.Title__c == 'US AFTERMARKET USED & CUSTOM VEHICLE FINANCE PROGRAMS') {
             finance1 = c;
             finance1Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'USA & CANADA AFTERMARKET USED & CUSTOM VEHICLES FINANCE PROGRAMS') {
             finance2 = c;
             finance2Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'US & CANADA GOLF CAR DISTRIBUTOR VEHICLE FINANCE PROGRAMS') {
             finance3 = c;
             finance3Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'CANADIAN GOLF TURF UTILITY NEW VEHICLE FINANCE PROGRAMS') {
             finance4 = c;
             finance4Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Canada Commercial & Industrial Utility finance programs') {
             finance5 = c;
             finance5Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'INSTRUCTIONS FOR ENTERING AN INVOICE') {
             finance6 = c;
             finance6Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'TO CHECK ON THE STATUS OF A SUBMITTED INVOICE') {
             finance7 = c;
             finance7Render = true;
            }
            if (c.Title__c == 'Printing an Account Statement') {
             finance8 = c;
             finance8Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Printing an Invoice') {
             finance9 = c;
             finance9Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Letter of Credit Instructions') {
             finance10 = c;
             finance10Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Negotiating/accepting/paying bank Instructions') {
             finance11 = c;
             finance11Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'WIRE INSTRUCTIONS') {
             finance12 = c;
             finance12Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Lock Box AddressES') {
             finance13 = c;
             finance13Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'US commercial & industrial utility vehicle finance programs') {
             finance14 = c;
             finance14Render = true;
             financeRender=true;
            }
            if (c.Title__c == 'Canadian commercial & industrial utility vehicle finance programs') {
             finance15 = c;
             finance15Render = true;
            }
            if (c.Title__c == 'CANADIAN XRT NEW VEHICLE FINANCE PROGRAMS') {
             finance16 = c;
             finance16Render = true;
             financeRender=true;
            }
          }
        }
      }
    }
  }
}