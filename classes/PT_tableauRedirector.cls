public class PT_tableauRedirector {
  
  @RemoteAction    
  public static String findRecordId(string obj, String field, String value){
    String returl='/one/one.app#';
    if(obj != '' && field != '') {
      String queryStr = 'SELECT Id FROM ' + obj + ' WHERE ' + String.escapeSingleQuotes(field) + ' = \'' + String.escapeSingleQuotes(value) + '\'';
      for(sobject so : database.query(queryStr)){returl = '/lightning/r/'+ obj + '/' + so.Id + '/view';}
    } else {
      String searchStr = '{"componentDef":"forceSearch:search","attributes":{"term":"'+value+'","scopeMap":{"type":"TOP_RESULTS"},"context":' +
                         '{"disableSpellCorrection":false,"disableIntentQuery":false,"permsAndPrefs":{"OrgPermissions.UnionAppNavSmartScope":' +
                         'false,"SearchUi.searchUIPilotFeatureEnabled":false,"Search.crossObjectsAutoSuggestEnabled":true,"Search.maskSearchInfoInLogs":' +
                         'false,"SearchUi.orgHasAccessToSearchTermHistory":false,"SearchResultsLVM.lvmEnabledForSearchResultsOn":true,"SearchUi.searchUIInteractionLoggingEnabled":' +
                         'false,"MySearch.userCanHaveMySearchBestResult":false,"MySearch.userCanHaveMySearch":false}}},"state":{}}';
      returl = '/one/one.app#'+EncodingUtil.base64Encode(blob.valueOf(searchStr));
    }
    return returl;
  }
}