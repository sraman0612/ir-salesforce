@isTest
public class CC_WarrantyLaborRateFormCnt_Test {
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    
static testMethod void UnitTest_CC_WarrantyLaborRateFormCnt()
{
        // Creation of Test Data
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con; 
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert u;
        System.assertEquals(con.Id,u.ContactId);
        System.runAs(u)
        {
        CC_PavilionTemplateController Controller;
        CC_WarrantyLaborRateFormCnt laborRate = new CC_WarrantyLaborRateFormCnt(Controller);
        test.starttest();
        apexpages.currentpage().getparameters().put('dt',String.valueOf(System.today()));
        apexpages.currentpage().getparameters().put('prevwdt',String.valueOf(System.today()));
        laborRate.SubmitInformation();
        laborRate.previewInformation();
        laborRate.closepreviw(); 
        apexpages.currentpage().getparameters().put('dt','test');
        apexpages.currentpage().getparameters().put('prevwdt','test');
        laborRate.SubmitInformation();
        test.stopTest();
        }
    }
}