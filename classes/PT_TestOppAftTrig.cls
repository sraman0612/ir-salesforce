@isTest
public class PT_TestOppAftTrig {
    
    static testmethod void testOppAmount(){
        
        List<PT_OppStages__c> stages = new List<PT_OppStages__c>();
        PT_OppStages__c st1 = new PT_OppStages__c(name='0.Plan');
        stages.add(st1);
        PT_OppStages__c st2 = new PT_OppStages__c(name='1.Target');
        stages.add(st2);
        PT_OppStages__c st = new PT_OppStages__c(name='6.Closed won');
        stages.add(st);
        insert stages;
        
        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
        Account a = new Account(
            RecordTypeId = ptAcctRTID,
            Name = 'TestAcc',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );        
        insert a;

        PT_CreateScheduleOpp.testMonths = 1;
        
        PT_Parent_Opportunity__c p = new PT_Parent_Opportunity__c(
            Account__c = a.Id, 
            Sales_Rep__c = system.UserInfo.getUserId(), 
            Type__c = '2.Solution', 
            Year__c = string.valueof(system.today().year()),
            Lead_SourcePL__c='Other'
        );
        insert p;
        
        test.startTest();
        
        List<Opportunity> opps = [select id, amount, stageName from opportunity where PT_parent_opportunity__c=:p.id];
        for(integer i=0;i<opps.size();i++){
            opps[i].amount = 100;
            opps[i].PT_Invoice_Date__c = system.today().addyears(10);
            if(i==0){
                opps[i].stageName = 'Closed Won';
            }
        }
        
        update opps;
                
        test.stopTest();
    }

}