public class CC_PavilionTavantHomeController {
  
  public static CC_Pavilion_Content__c pavContentTavantHeading{get;set;}
  public static CC_Pavilion_Content__c pavContentTavantBody{get;set;}
  
  public static string getTavantHeading() {
    String HeadingText= pavContentTavantHeading.CC_Body_1__c;
    return HeadingText;
  }
    
  public static string getTavantBody() {
    String BodyText=pavContentTavantBody.CC_Body_2__c;
    return BodyText;
  }  
  
  public string getTavantURL(){
    String retStr=null==PavilionSettings__c.getInstance('TavantEndpoint')?'#':PavilionSettings__c.getInstance('TavantEndpoint').Value__c;
    return retStr;
  }
    
  public CC_PavilionTavantHomeController(CC_PavilionTemplateController controller) {
    pavContentTavantHeading = [SELECT CC_Body_1__c  FROM CC_Pavilion_Content__c WHERE  Title__c='Important Message' and RecordType.Name = 'Warranty'];
    pavContentTavantBody = [SELECT CC_Body_2__c  FROM CC_Pavilion_Content__c WHERE  Title__c='Important Message' and RecordType.Name = 'Warranty'];
  }
}