// Test Class for OptyWinPlan_Trigger_TriggerHandler class
//
// Created on 8th Jan 2018    
//
@isTest
private class OptyWinPlan_Trigger_TriggerHandler_Test {
    
    // Method to test account trigger
    static testMethod void testAssetTrigger() {
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_EU' and SobjectType='Account' limit 1].Id;
        
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'testCounty';
        acc.Division__c = 'San Francisco Customer Center';
        acc.recordtypeid=AirNAAcctRT_ID;
        insert acc;
        
        Id NA_Air_Opty_RT_Id =  [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_EU' and SobjectType='Opportunity' limit 1].Id;
        Opportunity Opty = new Opportunity();
        Opty.Name = 'testOppty';
        Opty.amount = 2;
        Opty.StageName = 'Stage 1. Qualify';
        Opty.CloseDate = system.today();
        Opty.AccountId = acc.Id;
        insert Opty;
                
        Id CTSOptyWinPlanRT_ID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'CTS_Opportunity_Win_Plan' and SobjectType='OpportunityWinPlan__c' limit 1].Id;
        OpportunityWinPlan__c obj = new OpportunityWinPlan__c();
        obj.Name = 'test_OptyWinPlan__c';
        obj.RecordTypeId=CTSOptyWinPlanRT_ID;
        obj.Opportunity__c = Opty.Id;
        
        Test.startTest();
        insert obj;    
     // System.debug('obj details == ' + obj + '\n');     
        
        Test.stopTest();
     // System.assertEquals(Opty.Opportunity_Win_Plan_Count__c,1);
        
       }
        }