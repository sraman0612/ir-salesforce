@isTest
public class CC_UpdatePartnerAgreementCountsTest {
    
    static testmethod void test1(){
        StatesCountries__c[] sc = new StatesCountries__c[] {
           new StatesCountries__c (name = 'Georgia',type__c  = 'S',code__c  = 'GA',country__c = 'USA'),
           new StatesCountries__c (name = 'USA',type__c  = 'C',code__c  = 'US',country__c = '')
        };
        insert sc;                                                         
        CC_Sales_Rep__c sr = new CC_Sales_Rep__c(Name = 'Test',   CC_Sales_Person_Number__c = '11111');
        insert sr;       
        Id ccAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='Club_Car'].Id;
        Account a = new Account (Name = 'PartnerAgreementTest',RecordTypeId = ccAcctRTID,CC_Sales_Rep__c = sr.Id,CC_Shipping_Billing_Address_Same__c = true,
                                 ShippingStreet = '2044 Forward Augusta Dr',ShippingCity   = 'Augusta',ShippingState  = 'GA',ShippingPostalCode = '00000',
                                 ShippingCountry = 'USA',ShippingLatitude = 50,ShippingLongitude = 50,BillingStreet = '2044 Forward Augusta Dr',BillingCity   = 'Augusta',
                                 BillingState  = 'GA',BillingPostalCode = '00000',BillingCountry = 'USA',BillingLatitude = 50,BillingLongitude = 50);
        insert a;
        Contract x = New Contract(AccountId = a.Id,CC_Contract_Status__c = 'Current',CC_Type__c = 'Dealer/Distributor Agreement',CC_Sub_Type__c = 'Retail Dealer'); 
        insert x;
        Contract c = New Contract (AccountId = a.Id,CC_Contract_Status__c = 'Current',CC_Type__c = 'Dealer/Distributor Agreement',CC_Sub_Type__c = 'Golf Car Distributor'); 
        insert c;
        Test.startTest();
        Account initAcct = [SELECT Id, Current_Commercial_Agreements__c, Current_Consumer_Agreements__c, Current_Golf_Agreements__c FROM Account WHERE Id = :a.Id];
        system.assertEquals(0,initAcct.Current_Commercial_Agreements__c);
        system.assertEquals(0,initAcct.Current_Consumer_Agreements__c);
        system.assertEquals(0,initAcct.Current_Golf_Agreements__c);
        String qryStr = 'SELECT Id, Current_Commercial_Agreements__c, Current_Consumer_Agreements__c, Current_Golf_Agreements__c ' +
                        'FROM Account ' +
                        'WHERE id = \'' + a.Id + '\'';

        CC_UpdatePartnerAgreementCounts upac = new CC_UpdatePartnerAgreementCounts(qryStr);
        Database.executebatch(upac,1);   
        Test.stopTest(); 
        Account updatedAcct = [SELECT Id, Current_Commercial_Agreements__c, Current_Consumer_Agreements__c, Current_Golf_Agreements__c FROM Account WHERE Id = :a.Id];
        system.assertEquals(1,updatedAcct.Current_Golf_Agreements__c);
    }
    
    static testmethod void test2(){
        test.startTest();
        CC_UpdatePartnerAgreementCounts sc1 = new CC_UpdatePartnerAgreementCounts('');
        String sch = '0 0 23 * * ?'; 
        system.schedule('test partner agreement update', sch, sc1); 
        test.stopTest();
    }
}