@isTest
private class CC_allInvoiceLineControllerTest {
    public static Account acc;
    public Static Contact con;
    public Static User PortalUser;
    public Static CC_Invoice2__c SampleInv1,SampleInv2;
    public  Static List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();
    
    
    static{
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
        acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        con= TestUtilityClass.createContact(acc.id);
        insert con;
        PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        insert PortalUser;
        SampleInv1= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test',2000,'14593');
        SampleInv2= TestUtilityClass.getTestInvoiceData(acc.Id,2,'12345','Sample test',2000,'74593');
        INVLists.add(SampleInv1);
        INVLists.add(SampleInv2);
        insert INVLists;
    }
    private static testMethod void test() {
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.CC_allInvoiceLineController'));
        System.currentPageReference().getParameters().put('id', INVLists[0].Id);
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(INVLists[0]);
        CC_allInvoiceLineController controller = new CC_allInvoiceLineController(stdCtrl);
        controller.redirector();
        Test.stopTest();
    }

}