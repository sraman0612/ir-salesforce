public with sharing class CC_Short_Term_Lease_AlertsController {

    public class AlertsResponse extends LightningResponseBase{
        @AuraEnabled public Alert[] alerts = new Alert[]{};
    }

    public class Alert{
        @AuraEnabled public String type;
        @AuraEnabled public String mode;
        @AuraEnabled public Integer duration;
        @AuraEnabled public String title;
        @AuraEnabled public String message;
    }

    @AuraEnabled
    public static AlertsResponse getAlerts(Id leaseId){

        AlertsResponse response = new AlertsResponse();

        try{

            CC_Short_Term_Lease__c lease = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c Where Id = :leaseId];
            
            if (lease.Sales_Rep__c != null && lease.Sales_Rep_User__c != null){

                system.debug('evaluating alerts for lease: ' + lease);
                
                Map<Id, CC_Sales_Rep_Service.SalesHierarchy> salesHierarchyMap = CC_Sales_Rep_Service.lookupSalesHiearchy(new Set<Id>{lease.Sales_Rep__c});
                CC_Sales_Rep_Service.SalesHierarchy salesHierarchy = salesHierarchyMap.get(lease.Sales_Rep__c);		

                // Fix the lookup fields if the hiearchy is now there
                String message = '';
                Boolean leaseUpdateNeeded = false;

                if (lease.Sales_Rep_Manager__c == null){

                    if (salesHierarchy.salesManager != null){
                        leaseUpdateNeeded = true;
                        lease.Sales_Rep_Manager__c = salesHierarchy.salesManager.Id;
                    }
                    else{
                        message += '\nThe sales rep selected does not have a manager assigned to their user record.';
                    }
                }

                if (lease.Sales_Rep_Director__c == null){

                    if (salesHierarchy.salesDirector != null){
                        leaseUpdateNeeded = true;
                        lease.Sales_Rep_Director__c = salesHierarchy.salesDirector.Id;
                    }
                    else{
                        message += '\nThe sales rep selected does not have a director (Manager of Manager) assigned to their user record.';
                    }
                }
                
                if (lease.Sales_Rep_VP__c == null){

                    if (salesHierarchy.salesVP != null){
                        leaseUpdateNeeded = true;
                        lease.Sales_Rep_VP__c = salesHierarchy.salesVP.Id;
                    }
                    else{                    
                        message += '\nThe sales rep selected does not have a VP (Manager of Director) assigned to their user record.';
                    }
                }  

                system.debug('leaseUpdateNeeded: ' + leaseUpdateNeeded);

                if (leaseUpdateNeeded){
                    update lease;
                }

                system.debug('alert message: ' + message);

                if (String.isNotBlank(message)){

                    message = 'There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.\n' + message;

                    Alert salesHierarchyAlert = new Alert();
                    salesHierarchyAlert.type = 'warning';
                    salesHierarchyAlert.mode = 'dismissible';
                    salesHierarchyAlert.duration = 30000;
                    salesHierarchyAlert.title = 'Missing approvers in the sales hiearchy';
                    salesHierarchyAlert.message = message;
                    
                    response.alerts.add(salesHierarchyAlert);
                }          
            }
        }
        catch (Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}