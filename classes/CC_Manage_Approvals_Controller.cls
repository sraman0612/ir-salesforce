public with sharing class CC_Manage_Approvals_Controller {
    
    private class CC_Manage_Approvals_ControllerException extends Exception{}

    public static final String approvalOverrideFieldName = 'Approval_Override__c';
    public static final String approvalOverrideRequestFieldName = 'Approval_Override_Request__c';    

    private static Set<String> objectsToIgnore = new Set<String>{'KnowledgeArticle'};
    private static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
    private static Set<String> nonRecordFieldColumns = new Set<String>{'assignedTo','requestLink','approvalProcessName'};
    private static Set<String> supportedObjects = new Set<String>{};

    static{

        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();

        if (batchControls != null && String.isNotBlank(batchControls.Mgr_Approval_Override_Supported_Objects__c)){
            supportedObjects.addAll(batchControls.Mgr_Approval_Override_Supported_Objects__c.split(','));
        }
    }    

    private static LightningDataTableService.Column[] defaultColumns = new LightningDataTableService.Column[]{
        new LightningDataTableService.Column('Record Name', 'requestLink', 'url', true, new LightningDataTableService.ColumnTypeAttributesLink(new LightningDataTableService.ColumnLabel('Name'), '_blank')),
        new LightningDataTableService.Column('Assigned To', 'assignedTo', 'text', true, null),
        new LightningDataTableService.Column('Request Type', 'approvalProcessName', 'text', true, null)        
    };

    public class GetApprovalsRequest{
        @AuraEnabled public String[] viewModes;
        @AuraEnabled public Boolean showMyDelegates;
        @AuraEnabled public Boolean showAll;
        @AuraEnabled public Integer showLimit;
    }

    public class GetApprovalsResponse extends LightningResponseBase{
        @AuraEnabled public Integer approvalCount = 0;
        @AuraEnabled public ObjectApprovalRequests[] approvalsByObj = new ObjectApprovalRequests[]{};            
    }

    public class ObjectApprovalRequests{
        @AuraEnabled public String objLabel;
        @AuraEnabled public String objName;        
        @AuraEnabled public String sortDirection;
        @AuraEnabled public String sortField;
        @AuraEnabled public LightningDataTableService.Column[] columns;
        @AuraEnabled public ApprovalRequest[] approvals = new ApprovalRequest[]{};
    }

    public class ApprovalRequest{
        @AuraEnabled public String approvalProcessName;
        @AuraEnabled public String requestLink;
        @AuraEnabled public Id requestId;
        @AuraEnabled public String rowKey;
        @AuraEnabled public sObject record;
        @AuraEnabled public String assignedTo;        
    }

    @AuraEnabled
    public static GetApprovalsResponse getApprovals(String requestStr){

        system.debug('requestStr: ' + requestStr);

        GetApprovalsResponse response = new GetApprovalsResponse();

        try{

            GetApprovalsRequest request = (GetApprovalsRequest)JSON.deserialize(requestStr, GetApprovalsRequest.class);

            system.debug('request: ' + request);

            User currentUser = [Select Id, Name, DelegatedApproverId From User Where Id = :UserInfo.getUserId()];

            // Check to see if the user has manager approval override enabled
            Boolean managerApprovalOverrideEnabled = false;

            if ([Select Count() From PermissionSetAssignment Where AssigneeId = :currentUser.Id and PermissionSet.Name = 'CC_Short_Term_Lease_Manager_Approval_Override'] > 0){
                managerApprovalOverrideEnabled = true;
            }  

            // Get columns to display from custom metadata
            Map<String, LightningDataTableService.Column[]> objColumnMap = new Map<String, LightningDataTableService.Column[]>{};

            for (CC_Approval_Column__mdt approvalColumn : [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                           From CC_Approval_Column__mdt 
                                                           Order By Column_Order__c ASC]){

                LightningDataTableService.ColumnTypeAttributesBase columnTypeAttributes;
                
                if (approvalColumn.Type_Attributes__c != null){
                    
                    if (approvalColumn.Type__c == 'url'){
                        columnTypeAttributes = (LightningDataTableService.ColumnTypeAttributesLink)JSON.deserialize(approvalColumn.Type_Attributes__c, LightningDataTableService.ColumnTypeAttributesLink.class);
                    }
                    else if (approvalColumn.Type__c == 'button'){
                        columnTypeAttributes = (LightningDataTableService.ColumnTypeAttributesButton)JSON.deserialize(approvalColumn.Type_Attributes__c, LightningDataTableService.ColumnTypeAttributesButton.class);
                    }
                }

                if (objColumnMap.containsKey(approvalColumn.Object_API_Name__c)){
                    objColumnMap.get(approvalColumn.Object_API_Name__c).add(new LightningDataTableService.Column(approvalColumn.MasterLabel, approvalColumn.Column_Name__c, approvalColumn.Type__c, approvalColumn.Sortable__c, columnTypeAttributes));
                }
                else{
                    objColumnMap.put(approvalColumn.Object_API_Name__c, new LightningDataTableService.Column[]{new LightningDataTableService.Column(approvalColumn.MasterLabel, approvalColumn.Column_Name__c, approvalColumn.Type__c, approvalColumn.Sortable__c, columnTypeAttributes)});
                }                
            }

            User[] usersToLookup = new User[]{};
            
            Boolean showMine = false, showMyTeam = false, showMyDelegates = false;

            for (String viewMode : request.viewModes){
                
                if (viewMode == 'mine'){
                    showMine = true;
                }
                else if (viewMode == 'myteam'){
                    showMyTeam = true;
                }
                else if (viewMode == 'mydelegates'){
                    showMyDelegates = true;
                }                                
            }

            if (showMine){
                usersToLookup.add(new User(Id = currentUser.Id));
            }

            if (showMyTeam){                
                usersToLookup.addAll(UserService.getAllReportingToUsers(currentUser.Id));
            }

            if (showMyDelegates){
                usersToLookup.addAll(UserService.getDelegateUsers(currentUser.Id));
            }

            Map<Id, User> userMap = new Map<Id, User>();

            for (User u : usersToLookup){
                userMap.put(u.Id, u);
            }

            Map<String, ApprovalRequest[]> objApprovals = new Map<String, ApprovalRequest[]>();
            Map<String, Set<Id>> objRecordIds = new Map<String, Set<Id>>();

            // Find all pending approval requests up to the limit
            String selectClause = 'Select Id, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId, ProcessInstance.ProcessDefinition.Name, OriginalActor.Name, Actor.Name ';
            String fromClause =  'From ProcessInstanceWorkitem ';
            String whereClause = 'Where ActorId = :usersToLookup ';
            String orderByClause = 'Order By ProcessInstance.ProcessDefinition.TableEnumOrId DESC ';
            
            response.approvalCount = Database.countQuery('Select Count() ' + fromClause + whereClause);

            ProcessInstanceWorkitem[] piws = Database.query(selectClause + fromClause + whereClause + orderByClause + (!request.showAll ? ' LIMIT ' + request.showLimit : ''));

            // Get the unique objects in the approval requests
            Set<String> objects = new Set<String>();

            for (ProcessInstanceWorkitem piw : piws){
                	
               if (objectsToIgnore.contains(piw.ProcessInstance.ProcessDefinition.TableEnumOrId)){
                   continue;	
               }   

                String objName = piw.ProcessInstance.ProcessDefinition.TableEnumOrId;

                objects.add(objName);

                if (objRecordIds.containsKey(objName)){
                    objRecordIds.get(objName).add(piw.ProcessInstance.TargetObjectId);
                }
                else{
                   objRecordIds.put(objName, new Set<Id>{piw.ProcessInstance.TargetObjectId});
                }                              
            }

            // Fetch field values from records for any custom field/columns defined
            Map<Id, sObject> recordMap = new Map<Id, sObject>();

            for (String obj : objects){

                Set<Id> recordIds = objRecordIds.get(obj);
                LightningDataTableService.Column[] columns;

                Schema.SObjectType objToken = gd.get(obj);
                Map<String, DescribeFieldResult> fieldDescMap = sObjectService.getObjectFieldDescribeMap(objToken);

                if (objColumnMap.containsKey(obj)){
                    columns = objColumnMap.get(obj);
                }
                else{
                    
                    // Get name field from object, some do not use "Name"
                    String nameField = 'Name';
                    
                    for (String fldName : fieldDescMap.keySet()){
                        
                        DescribeFieldResult fieldDesc = fieldDescMap.get(fldName);
                        
                        if (fieldDesc.isNameField()){
                            nameField = fieldDesc.getName();
                            break;
                        }
                    }
                    
                    ((LightningDataTableService.ColumnTypeAttributesLink)defaultColumns[0].typeAttributes).label.fieldName = nameField;
                    columns = defaultColumns;
                }  

                List<String> fields = new List<String>();

                for (LightningDataTableService.Column col : columns){

                    if (!nonRecordFieldColumns.contains(col.fieldName)){                        
                        fields.add(col.fieldName);
                    }
                    else if (col.typeAttributes != null && col.typeAttributes instanceOf LightningDataTableService.ColumnTypeAttributesLink){
                        fields.add(((LightningDataTableService.ColumnTypeAttributesLink)col.typeAttributes).label.fieldName);
                    }
                }

                if (supportedObjects.contains(obj)){                    
                    fields.add(approvalOverrideFieldName);
                    fields.add(approvalOverrideRequestFieldName);
                }

                for (sObject record : Database.query((fields.size() > 0 ? 'Select Id,' : 'Select Id') + String.join(fields, ',') + ' From ' + String.escapeSingleQuotes(obj) + ' Where Id in :recordIds')){
                    recordMap.put(record.Id, record);
                }
            }

            // Build the approval requests to display
            for (ProcessInstanceWorkitem piw : piws){

                sObject record = recordMap.get(piw.ProcessInstance.TargetObjectId);

                system.debug('processing record: ' + record);

                User approver = userMap.get(piw.ActorId);

                ApprovalRequest approval = new ApprovalRequest();
                approval.approvalProcessName = piw.ProcessInstance.ProcessDefinition.Name;
                approval.assignedTo = piw.Actor.Name;
                approval.requestLink = '/' + piw.Id;
                approval.requestId = piw.Id;
                approval.rowKey = piw.Id + '-me-' + (piw.ActorId == currentUser.Id) + + '-delegate-' + (currentUser.Id == approver.DelegatedApproverId);
                approval.record = record;                
                            
                String objName = piw.ProcessInstance.ProcessDefinition.TableEnumOrId;
                Schema.SObjectType objToken = gd.get(objName);
                Map<String, Schema.SObjectField> fieldTokenMap = sObjectService.getObjectFieldTokenMap(objToken);

                // Skip showing the approval request if there is a pending approval override
                if (supportedObjects.contains(objName) && (Boolean)record.get(approvalOverrideFieldName)){
                    response.approvalCount--;
                    system.debug('skipping record due to pending override');
                    continue;
                }

                if (objApprovals.containsKey(objName)){
                    objApprovals.get(objName).add(approval);
                }
                else{
                   objApprovals.put(objName, new ApprovalRequest[]{approval});
                }                
            }

            for (String objName : objApprovals.keySet()){
                
                if (objectsToIgnore.contains(objName)){
                    continue;
                }   

                Schema.SObjectType objToken = gd.get(objName);
                DescribeSObjectResult objDesc = objToken.getDescribe();

                LightningDataTableService.Column[] columns = new LightningDataTableService.Column[]{};                
                
                columns.addAll(objColumnMap.containsKey(objName) ? objColumnMap.get(objName) : defaultColumns);

                Map<String, Schema.SObjectField> fieldTokenMap = objDesc.fields.getMap();                

                if (managerApprovalOverrideEnabled && fieldTokenMap.containsKey(approvalOverrideFieldName) && showMyTeam){
                    
                    columns.add(
                        new LightningDataTableService.Column(
                            'Actions', 
                            null, 
                            'action', 
                            false, 
                            new LightningDataTableService.ColumnTypeAttributesAction(
                                new LightningDataTableService.ColumnTypeAttributesActionRow[]{
                                    new LightningDataTableService.ColumnTypeAttributesActionRow('Manager Override', 'manager_override')
                                }
                            )
                        )
                    );
                }                

                ObjectApprovalRequests objApproval = new ObjectApprovalRequests();
                objApproval.objLabel = objDesc.getLabel();
                objApproval.objName = objName;
                objApproval.sortDirection = 'asc';
                objApproval.approvals = objApprovals.get(objName);
                objApproval.columns = columns;
                response.approvalsByObj.add(objApproval);
            }
        }
        catch (Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }

    public class ApprovalOverrideRequest{
        public Approval.ProcessWorkitemRequest pwr;
        public Id overrideUserId;

        public ApprovalOverrideRequest(Approval.ProcessWorkitemRequest pwr, Id overrideUserId){
            this.pwr = pwr;
            this.overrideUserId = overrideUserId;
        }
    }

    @AuraEnabled
    public static LightningResponseBase submitManagerApprovalOverride(Id recordId, Id requestId, String comments, Boolean approve){
        
        LightningResponseBase response = new LightningResponseBase();

        try{

            Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
            pwr.setWorkitemId(requestId);
            pwr.setAction(approve ? 'Approve' : 'Reject');
            pwr.setComments(comments);

            Schema.SObjectType objType = recordId.getSobjectType();
            sObject record = objType.newSObject(recordId);

            ApprovalOverrideRequest overrideRequest = new ApprovalOverrideRequest(pwr, UserInfo.getUserId());

            record.put(approvalOverrideFieldName, true);
            record.put(approvalOverrideRequestFieldName, JSON.serialize(overrideRequest));  
            sObjectService.updateRecordsAndLogErrors(new sObject[]{record}, 'CC_Manage_Approvals_Controller', 'submitManagerApprovalOverride');          
        }
        catch(Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}