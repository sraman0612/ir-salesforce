public with sharing class CC_RGA_Case_Return_Items_Controller {
    
    public class OrderLineItemResponse extends LightningResponseBase{
    	@AuraEnabled public OrderLineItemSelection[] lineItems = new OrderLineItemSelection[]{}; 
    }
    
    @TestVisible
    private class SerialNumberOption{
    	
    	@AuraEnabled public String label;
    	@AuraEnabled public String value;
    	
    	public SerialNumberOption(String label, String value){
    		this.label = label;
    		this.value = value;
    	}
    }
    
    @TestVisible
    private class OrderLineItemSelection{
    	
    	@AuraEnabled public Boolean selected;
    	@AuraEnabled public String uniqueId;
    	@AuraEnabled public CC_Case_Order_Item__c caseOrderItem; 
    	@AuraEnabled public SerialNumberOption[] serialNumberOptions = new SerialNumberOption[]{}; 	    	
    	
    	public OrderLineItemSelection(Boolean selected, String uniqueId, CC_Case_Order_Item__c caseOrderItem){
    		this.selected = selected;
    		this.uniqueId = uniqueId;
    		this.caseOrderItem = caseOrderItem;
    	}
    }
    
    @AuraEnabled
    public static OrderLineItemResponse getOrderLineItems(Id caseId, Id orderId){
    	
    	OrderLineItemResponse response = new OrderLineItemResponse();
    	
    	try{
    		
    		// Add already selected order items
    		CC_Case_Order_Item__c[] caseOrderItems = [Select Id, Case__c, Order_Item__r.Order__c, Status__c, Serial_Numbers__c, Order_Item__r.RecordType.Name, Order_Item__r.Product__r.Name, Order_Item__r.Product__r.CC_Market_Type__c From CC_Case_Order_Item__c Where Order_Item__r.Order__c = :orderId];
    		
    		Set<Id> selectedOrderItemIds = new Set<Id>();
    		
    		for (CC_Case_Order_Item__c caseOrderItem : caseOrderItems){
    			
    			selectedOrderItemIds.add(caseOrderItem.Order_Item__c); 			
    			response.lineItems.add(new OrderLineItemSelection(true, caseOrderItem.Id, caseOrderItem));
    		}
    		
    		// Add any remaining order items not selected yet   		
    		for (CC_Order_Item__c orderItem : [Select Id, Order__c, RecordType.Name, Product__r.Name, Product__r.CC_Market_Type__c, (Select Id, Serial_No_s__c From Car_Items__r) From CC_Order_Item__c Where Id not in :selectedOrderItemIds and Order__c = :orderId]){
    			
    			OrderLineItemSelection oli = new OrderLineItemSelection(
    				false, 
    				orderItem.Id, 
    				new CC_Case_Order_Item__c(
						Case__c = caseId,
						Order_Item__c = orderItem.Id,
						Order_Item__r = orderItem
					)
				);
				
				// Add all serial numbers for each line item
    			String[] serialNumbers = new String[]{};  			
    			
    			if (orderItem.Car_Items__r != null && orderItem.Car_Items__r.size() > 0){    			
    				
					for (CC_Car_Item__c carItem : orderItem.Car_Items__r){
						
						if (String.isNotBlank(carItem.Serial_No_s__c)){
							
							carItem.Serial_No_s__c = carItem.Serial_No_s__c.removeEnd(';');
							
							for (String serialNumber : carItem.Serial_No_s__c.split(';')){
								oli.serialNumberOptions.add(new SerialNumberOption(serialNumber.trim(), serialNumber.trim()));
							}
						}					
					}
    			}				
    			
    			response.lineItems.add(oli);
    		}
    	}
    	catch(Exception e){
    		response.success = false;
    		response.errorMessage = e.getMessage();
    	}
    	
    	return response;
    }
    
    @AuraEnabled
    public static LightningResponseBase updateSelections(Id caseId, Id orderId, CC_Case_Order_Item__c[] selectedOrderItems){
    	
    	LightningResponseBase response = new LightningResponseBase();
    	
    	try{
    		
    		system.debug('selectedOrderItems: ' + selectedOrderItems);
    		
    		// Upsert the selected order items
    		upsert selectedOrderItems;
    		
    		// Delete any existing case order items that were unselected
    		delete [Select Id From CC_Case_Order_Item__c Where Case__c = :caseId and Order_Item__r.Order__c = :orderId and Id not in :selectedOrderItems];
    	}
    	catch(Exception e){
    		response.success = false;
    		response.errorMessage = e.getMessage();
    	}
    	
    	return response;
    }    
}