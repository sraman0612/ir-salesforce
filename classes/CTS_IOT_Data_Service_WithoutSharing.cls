public without sharing class CTS_IOT_Data_Service_WithoutSharing {

    // Private variables
    private static CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();

    // Public variables
    public static Set<String> communityProfileNames = new Set<String>{
        communitySettings.Default_Standard_User_Profile_Name__c,
        communitySettings.Default_Standard_User_All_Profile_Name__c,
        communitySettings.Default_Super_User_Profile_Name__c
    };

    public static Profile defaultStandardUserCommunityProfile, defaultStandardUserAllSitesCommunityProfile, defaultSuperUserCommunityProfile;
 
    static{
        
        for (Profile p : [Select Id, Name From Profile Where Name in :communityProfileNames]){

            if (p.Name == communitySettings.Default_Standard_User_Profile_Name__c){
                defaultStandardUserCommunityProfile = p;
            }
            else if (p.Name == communitySettings.Default_Standard_User_All_Profile_Name__c){
                defaultStandardUserAllSitesCommunityProfile = p;
            }
            else if (p.Name == communitySettings.Default_Super_User_Profile_Name__c){
                defaultSuperUserCommunityProfile = p;
            }                        
        }
    }

    /* This class is intended to fetch data that the user would not normally see with sharing rules applied */

    public static Boolean getIsSandbox(){

        Boolean isSandbox = true;

        try{
            isSandbox = ConnectApi.Organization.getSettings().orgId != communitySettings.Production_Org_ID__c;
        }
        catch(Exception e){
            system.debug('exception: ' + e.getMessage());
        }

        return isSandbox;
    }       

    public static User lookupUser(Id userId){
        return [Select Id, isActive, ProfileId, Profile.Name, Name, FirstName, LastName, Phone, Email, CTS_IOT_Email_Address__c, Title, AccountId From User Where Id = :userId];
    }

    public static User lookupUserbyContact(Id contactId){

        User[] users = [Select Id, isActive, ProfileId, Profile.Name, Name, FirstName, LastName, Phone, Email, CTS_IOT_Email_Address__c, Title, AccountId From User Where ContactId = :contactId];
        return users.size() > 0 ? users[0] : null;
    }   
    
    public class CommunityContact{
        @AuraEnabled public Id ContactId;
        @AuraEnabled public Id UserId;
        @AuraEnabled public Account Site;
        @AuraEnabled public String Name;
        @AuraEnabled public String FirstName;
        @AuraEnabled public String LastName;
        @AuraEnabled public String Email;
        @AuraEnabled public String Title;
        @AuraEnabled public String Phone;
        @AuraEnabled public String NickName;
        @AuraEnabled public Date CreatedDate;
        @AuraEnabled public Date LastLoginDate;
        @AuraEnabled public String Status = 'Inactive';
        @AuraEnabled public String ContactDetailsURL;
        @AuraEnabled public Boolean IsSuperUser = false;
        @AuraEnabled public Boolean ApprovalRequested = false;
        @AuraEnabled public String ApprovalRequestMatchDetails = '';
        @AuraEnabled public DateTime ApprovalRequestDate;
        @AuraEnabled public Asset RegistrationAssetMatch;
    }     

    public class CommunityContactResponse{
        @AuraEnabled public CommunityContact[] communityContacts;
    }    

    // Community users do not have access to contacts that are not community-enabled with sharing rules applied, they need to see these contacts
    public static Integer getContactsCount(String searchTerm, Id siteId){

        Set<Id> sites = new Set<Id>();

        if (siteId != null){
            sites.add(siteId);
        }
        else{

            for (Account site : CTS_IOT_Data_Service.getCommunityUserSites(null)){
                sites.add(site.Id);
            }            
        }

        if (String.isBlank(searchTerm)){
            return Database.countQuery('Select Count() From Contact Where AccountId in :sites');    
        }
        else{
            return Search.query('FIND :searchTerm RETURNING Contact(Id Where AccountId in :sites)')[0].size();   
        }
    }
    
    public static CommunityContactResponse getContacts(Id filterId){
        return getContacts(null, filterId, null, null);
    }

    public static CommunityContactResponse getContacts(String searchTerm, Id filterId, Integer pageSize, Integer offSet){

        if (filterId != null){
            
            if (filterId.getSObjectType() == Account.getSObjectType()){
                return getContactList(searchTerm, filterId, null, pageSize, offSet);
            }
            else if (filterId.getSObjectType() == Contact.getSObjectType()){
                return getContactList(searchTerm, null, filterId, pageSize, offSet);
            }     
            else{
                return null;
            }       
        }
        else{
            return getContactList(searchTerm, null, null, pageSize, offSet);
        }
    }

    private static CommunityContactResponse getContactList(String searchTerm, Id siteId, Id contactId, Integer pageSize, Integer offSet){

        CommunityContactResponse response = new CommunityContactResponse();        

        Set<Id> sites = new Set<Id>();

        if (siteId != null){
            sites.add(siteId);
        }
        else if (contactId == null){

            for (Account site : CTS_IOT_Data_Service.getCommunityUserSites(null)){
                sites.add(site.Id);
            }
        }

        String contactFields = 'Id, Name, FirstName, LastName, Email, Phone,' +
                               'AccountId, Account.Name, Account.Account_Name_Displayed__c, Account.BillingCity, Account.BillingState, Account.Bill_To_Account__c,' +
                               'CTS_IOT_Community_Status__c, CTS_IOT_Contact_Match_Detail__c, CTS_IOT_Registration_Date__c,' + 
                               'CTS_IOT_Registration_Asset__c, CTS_IOT_Registration_Asset__r.Name, CTS_IOT_Registration_Asset__r.AccountId, CTS_IOT_Registration_Asset__r.Account.Name ';

        String contactQuery = String.isBlank(searchTerm) ?
            'Select ' + contactFields + ' From Contact' : 
            'FIND :searchTerm RETURNING Contact(' + contactFields;        
            
        contactQuery += ' Where (AccountId in :sites or Id = :contactId) Order By FirstName ASC, LastName ASC';            

        if (pageSize != null){
            contactQuery += ' LIMIT :pageSize';
        }
        else{
            contactQuery += ' LIMIT 2000';
        }

        if (offSet != null){
            contactQuery += ' OFFSET :offSet';
        }     

        if (String.isNotBlank(searchTerm)){
            contactQuery += ')';
        }
        
        system.debug('contactQuery: ' + contactQuery);
        
        Contact[] contacts;
        
        if (String.isNotBlank(searchTerm)){
            contacts = Search.query(contactQuery)[0];
        }
        else contacts = Database.query(contactQuery);            

        User[] users = [Select Id, isActive, ProfileId, ContactId, Email, FirstName, LastName, Name, Title, Phone, CommunityNickname, LastLoginDate, CreatedDate 
                        From User 
                        Where ContactId in :contacts 
                        Order By FirstName ASC, LastName ASC 
                        LIMIT 2000]; 
        
        Set<Id> profileIds = new Set<Id>();

        for (User u : users){
            profileIds.add(u.ProfileId);
        }        

        Set<Id> superUserProfiles = getSuperUserProfiles(profileIds);

        Map<Id, User> contactUserMap = new Map<Id, User>();

        for (User u : users){
            contactUserMap.put(u.ContactId, u);
        }

        CommunityContact[] communityContacts = new CommunityContact[]{};

        for (Contact c : contacts){
            
            CommunityContact communityContact = new CommunityContact();            
            
            communityContact.ContactId = c.Id;
            communityContact.Name = c.Name;
            communityContact.FirstName = c.FirstName;
            communityContact.LastName = c.LastName;
            communityContact.Email = c.Email;
            communityContact.Phone = c.Phone;
            communityContact.Site = c.Account;
            communityContact.ContactDetailsURL = '/manage-users?contact-id=' + c.Id;   
            communityContact.ApprovalRequested = c.CTS_IOT_Community_Status__c == CTS_IOT_Self_RegisterController.pendingReviewStatus;
            communityContact.ApprovalRequestMatchDetails = c.CTS_IOT_Contact_Match_Detail__c;
            communityContact.ApprovalRequestDate = c.CTS_IOT_Registration_Date__c;
            communityContact.RegistrationAssetMatch = c.CTS_IOT_Registration_Asset__r;

            User u = contactUserMap.containsKey(c.Id) ? contactUserMap.get(c.Id) : new User();

            if (u != null && u.Id != null){
                
                communityContact.UserId = u.Id;
                communityContact.Name = u.FirstName + ' ' + u.LastName;
                communityContact.FirstName = u.FirstName;
                communityContact.LastName = u.LastName;                
                communityContact.Email = u.Email;
                communityContact.Title = u.Title;
                communityContact.Phone = u.Phone;
                communityContact.NickName = u.CommunityNickname;
                communityContact.CreatedDate = Date.valueOf(u.CreatedDate);
                communityContact.LastLoginDate = u.LastLoginDate != null ? Date.valueOf(u.LastLoginDate) : null;
                communityContact.Status = u.IsActive ? 'Active' : 'Inactive';
                communityContact.IsSuperUser = superUserProfiles.contains(u.ProfileId);
            }

            communityContacts.add(communityContact);
        }

        response.communityContacts = communityContacts;

        return response;
    }

    public static User[] getSuperUsersInHierarchy(Account site){

        Set<Id> superUserProfiles = getSuperUserProfiles(null);

        system.debug('superUserProfiles: ' + superUserProfiles);
        system.debug('site: ' + site);

        Account[] siteHierarchy = getSiteHierarchy(site.Id);

        return [Select Id, ContactId, AccountId, Email, FirstName, LastName 
                                 From User 
                                 Where isActive = true and ProfileId in :superUserProfiles and AccountId in :siteHierarchy
                                 Order By LastLoginDate DESC Nulls Last];        
    }

    public static Account[] getSiteHierarchy(Id siteId){

        Account[] sites = new Account[]{};

        Account site = [Select Id, Name, Bill_To_Account__c From Account Where Id = :siteId];
        sites.add(site);

        if (site.Bill_To_Account__c != null){
            sites.addAll([Select Id, Name From Account Where (Id = :site.Bill_To_Account__c or Bill_To_Account__c = :site.Bill_To_Account__c)]);
        }

        return sites;
    }    

    public static Boolean getIsSuperUser(Id userId){

        Id profileId = [Select ProfileId From User Where Id = :userId].ProfileId;
        PermissionSet permSet = [Select PermissionsPortalSuperUser From PermissionSet Where ProfileId = :profileId];
        return permSet.PermissionsPortalSuperUser;
    }    

    public static Set<Id> getSuperUserProfiles(Set<Id> profileIdsToFilter){

        Set<Id> superUserProfiles = new Set<Id>();

        String query = 'Select Id, ProfileId, PermissionsPortalSuperUser From PermissionSet Where ProfileId != null and PermissionsPortalSuperUser = true';

        if (profileIdsToFilter != null && profileIdsToFilter.size() > 0){
            query += ' and ProfileId in :profileIdsToFilter';
        }

        for (PermissionSet ps : Database.query(query)){
            superUserProfiles.add(ps.ProfileId);
        }        

        return superUserProfiles;
    }

    public static void updateUser(User u){
        update u;
    }

    public static void updateContact(Contact c){
        update c;
    }

    public static void deactivateUser(Id contactId){

        User u = [Select Id From User Where ContactId = :contactId];
        u.isActive = false;
        update u;
    }

    public static void activateUser(Id userId){

        User u = new User(Id = userId);
        u.isActive = true;
        update u;
    }    

    public static void removeSitesFromContact(Id contactId, Set<Id> sitesToRemove){
         delete [Select Id From AccountContactRelation Where ContactId = :contactId and AccountId in :sitesToRemove];
    }

    public static void addSitesToUser(Id userId, List<Id> sitesToAdd){

        User u = [Select Id, ContactId From User Where Id = :userId];
        addSitesToContact(u.ContactId, sitesToAdd);
    }

    public static Integer checkContactAccountRelation(Id contactId, Id siteId){
        return [Select Count() From AccountContactRelation Where ContactId = :contactId and AccountId = :siteId];
    }

    public static void addSitesToContact(Id contactId, List<Id> sitesToAdd){
        
         AccountContactRelation[] acrs = new AccountContactRelation[]{};

         for (Id siteId : sitesToAdd){
             acrs.add(
                 new AccountContactRelation(AccountId = siteId, ContactId = contactId)
             );
         }
         
         insert acrs;
    }    

    public class RegisterUserRequest{
        public Id contactId;
        public Id profileId;
        public Id siteId;
        public String firstName;
        public String lastName;
        public String username;
        public String email;
        public String phone;
        public String title;
        public String timezone;        
        public String communityApprovalStatus = 'Approved';
        public Boolean superUser = false;
        public Boolean hybridStandardUser = false;
        public Boolean sendEmailConfirmation = true;           
    }

    public class RegisterUserResponse extends LightningResponseBase{
        public Id newUserId;
    }    

    public static Boolean isContactInAssetHierarhcy(Id contactSiteId, Account assetSite){

        Integer siteCount = 0;

        // Asset site is a ship to/child
        if (assetSite.Bill_To_Account__c != null){
            siteCount = [Select Count() From Account Where Id = :contactSiteId and (Id = :assetSite.Bill_To_Account__c or Bill_To_Account__c = :assetSite.Bill_To_Account__c)];
        }
        // Asset site is a bill to
        else{
            siteCount = [Select Count() From Account Where Id = :contactSiteId and Bill_To_Account__c = :assetSite.Bill_To_Account__c];
        }     

        return siteCount > 0;   
    }

    public static RegisterUserResponse registerUser(RegisterUserRequest request){

        RegisterUserResponse response = new RegisterUserResponse();

        System.Savepoint sp = Database.setSavePoint();

        try{

            User newUser = new User(
                FirstName = request.firstName,
                LastName = request.lastName,
                Email = request.email,
                Username = request.username,
                Phone = request.phone,
                MobilePhone = request.phone,
                Title = request.title,
                UserPreferencesHideS1BrowserUI = true,
                Alias = request.username.subString(0, request.username.length() > 8 ? 8 : request.username.length()),
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = String.isNotBlank(request.timezone) ? request.timezone : 'America/New_York'
            );

            // Nickname is required for community users
            String nickname = ((newUser.FirstName != null && newUser.FirstName.length() > 0) ? newUser.FirstName.substring(0,1) : '' ) + newUser.LastName.substring(0,1);
            nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
            newUser.CommunityNickname = nickname;     

            if (request.profileId != null){
                newUser.ProfileId = request.profileId;
            }
            else if (request.superUser != null && request.superUser){
                newUser.ProfileId = defaultSuperUserCommunityProfile.Id;
            }
            else if (request.hybridStandardUser != null && request.hybridStandardUser){
                newUser.ProfileId = defaultStandardUserAllSitesCommunityProfile.Id;
            }
            else{
                newUser.ProfileId = defaultStandardUserCommunityProfile.Id;
            }

            // Check to see if the contact already exists for the provided account
            Contact[] contactCheck = [Select Id, Name From Contact Where AccountId = :request.siteId and Email = :request.email];

            if (request.contactId != null){
                contactCheck = [Select Id, Name From Contact Where Id = :request.contactId];
            }
            else{
                contactCheck = [Select Id, Name From Contact Where AccountId = :request.siteId and Email = :request.email];
            }

            if (contactCheck.size() == 1){
                
                // Only need to activate a user for the existing contact                    
                newUser.ContactId = contactCheck[0].Id;

                contactCheck[0].CTS_IOT_Community_Status__c = request.communityApprovalStatus;
                update contactCheck[0];
            }
            else{

                Contact newContact = new Contact(
                    RecordTypeId = communitySettings != null ? communitySettings.New_Contact_Record_Type_ID_NA__c : null,
                    AccountId = request.siteId,
                    FirstName = request.firstName,
                    LastName = request.lastName,
                    Email = request.email,
                    Phone = request.phone,
                    Title = request.title,
                    CTS_IOT_Community_Status__c = 'Approved'
                );

                insert newContact;

                newUser.ContactId = newContact.Id;
            }

            // Check to see if the username already exists
            Integer userCheck = [Select Count() From User Where AccountId != null and ContactId != null and Username = :newUser.username];

            // Make the username unique
            if (userCheck > 0){
                newUser.Username += '.' + String.valueOf(Crypto.getRandomInteger()).substring(1,4);
            }

            insert newUser;
            response.newUserId = newUser.Id;                        
        }
        catch (Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }
        
        return response;
    }    
}