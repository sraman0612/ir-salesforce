public with sharing class PT_DSMP_TestFactory {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Method to group methods for test data
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 10-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  

	//Randomize a string
    public static String randomizeString(String name) {
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
            randStr += charsForRandom.substring(idx, idx + 1);
        }
        return name + randStr;
    }

	//create admin user
    public static User createAdminUser(String name, Id profId) {
        return new User(
             Username = randomizeString(Name) + '@test.com'
            ,LastName = 'MG'
            ,FirstName = 'SC'
            ,Email = 'SC@test.com'
            ,Alias = 'MG'
            ,LanguageLocaleKey = 'en_US'
            ,TimeZoneSidKey = 'Europe/Paris'
            ,LocaleSidKey = 'en_US'
            ,EmailEncodingKey = 'UTF-8'
            ,ProfileId = profId
        );
    }


    //Create Account
    public static Account createAccount(String name, Id recTypeId, String boCustomerNumber) {
        return new Account(
            Name = randomizeString(Name)
           //,RecordTypeId = recTypeId
           ,PT_Is_DVP__c = 'DVP'
           ,PT_BO_Customer_Number__c = boCustomerNumber
           ,BillingCountry = 'Canada'
           ,ShippingCountry =  'Canada'
           ,AccountSource = 'Data.com'
        );
    }


    //Create PT_DSMP__c
    public static PT_DSMP__c createDSMP(String name, String accountId, String yearId) {
        return new PT_DSMP__c(
            Name = name
           ,Account__c = accountId
           ,PT_DSMP_Year__c = yearId
           ,DVP_Segment__c = 'STAR'
        );
    }


    //Create PT_DSMP_Year__c
    public static PT_DSMP_Year__c createDSMPYear(String name) {
        return new PT_DSMP_Year__c(
            Name = name
           ,Ext_ID__c = name
        );
    }


    //Create PT_DSMP_NPD__c
    public static PT_DSMP_NPD__c createDSMPNPD(String name, Id yearId) {
        return new PT_DSMP_NPD__c(
            Name = name
           ,PT_DSMP_Year__c = yearId
        );
    }


    //Create PT_DSMP_Shared_Objectives__c
    public static PT_DSMP_Shared_Objectives__c createDSMPSharedObjectives(String name, String segment, Integer objectiveNum,
    																	  Id yearId) {
        return new PT_DSMP_Shared_Objectives__c(
            Name = name
           ,PT_DSMP_Year__c = yearId
           ,Text__c = name
           ,Segment__c = segment
           ,Objective_Number__c = objectiveNum
           ,Ext_ID__c = yearId+'-'+segment+'-'+String.valueOf(objectiveNum)
        );
    }


    //Create PT_DSMP_Objectives__c
    public static PT_DSMP_Objectives__c createDSMPObjectives(String name, Id accountId, String objectiveNum,
														   	 Id ptDSMPid) {
        return new PT_DSMP_Objectives__c(
            Name = name
           ,Account__c = accountId
           ,PT_DSMP__c = ptDSMPid
           ,Objectives_Number__c = objectiveNum
        );
    }

    //Create PT_DSMP_Strategic_Product__c
    public static PT_DSMP_Strategic_Product__c createStrategicProduct(String name, 
    																  Id ptDSMPid,
    																  Integer quantity,
    																  Integer actual){
        return new PT_DSMP_Strategic_Product__c(
        	Name = name,
        	PT_DSMP__c = ptDSMPid,
        	Quantity__c = quantity,
        	Actual__c = actual
    	);
    }

    //Create Task
    public static Task createTask(String subject, 
    							  Id whatId, 
    							  Date activityDate,
    							  String status) {
        return new Task(
           Subject = subject
           ,PT_DSMP_Account_Objectives__c = whatId
           ,ActivityDate = activityDate
           ,Status = status
           ,Priority = 'Normal'
        );
    }

    //Create PT_DSMP_TARGET__c
    public static PT_DSMP_TARGET__c createDSMPTarget(String name, 
													 Id ptDSMPid,
													 String month){
        return new PT_DSMP_TARGET__c(
        	Name = name,
        	PT_DSMP__c = ptDSMPid,
        	Month__c = month
    	);
    }
}