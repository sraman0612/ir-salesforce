/*************************************************************
 * Description   : test Class for CC_Links_ContractListController
 * Created Date  : 23/09/2017
 * **********************************************************/



@isTest
public class CC_Links_ContractListControllerTest {
    
  
    
    static testmethod void run2condition(){
        
         //creating test Data here
        TestDataUtility testData = new TestDataUtility();
        
        
        
       
        user u = testData.createPartnerUser();
        system.runAs(u){
            Test.startTest();
            system.debug('@@@@u.AccountId'+u.AccountId);
             
            user u1 = [select id,AccountId from user where id=:u.id];
            system.debug('@@@@@u1.AccountId'+u1.AccountId);
            
           contract contr1 = testData.createContract(u1.AccountId,StringConstantsFinalClass.productAuthorizationGolfUtility);
           insert contr1;
        
             CC_PavilionTemplateController controller = new CC_PavilionTemplateController() ;
             controller.acctId = u1.AccountId;
            CC_Links_ContractListController contractCnt = new CC_Links_ContractListController(controller);
            CC_Links_ContractListController.getcontractsList();
            CC_Links_ContractListController.getUsersList();
            CC_Links_ContractListController.saveleadOwnerAssigned(contr1.id, u1.id);
        
        Test.stopTest();
        }
        
        
    }

}