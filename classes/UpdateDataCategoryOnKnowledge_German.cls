global without sharing class UpdateDataCategoryOnKnowledge_German implements Database.Batchable<sObject>, Database.Stateful, Schedulable
{
  global string query;
  global Map<string, string> RTCatMap= new Map<string, string>{};
  global String RecTypesStr = '';
  public Integer totalrecs = 0;
  public List<String> success_List = new List<String>();
  public List<String> exception_List = new List<String>();
  public string sLanguage = 'de';
  
  public UpdateDataCategoryOnKnowledge_German()
  { 
        
        for(Knowledge_Article_Category_Map__mdt rc : [select Record_Type__c, Data_Category__c from Knowledge_Article_Category_Map__mdt]) 
        {        
            RTCatMap.put(rc.Record_Type__c, rc.Data_Category__c); 
            String RT_in_quotes = '\'' + rc.Record_Type__c + '\'';
            if (RecTypesStr != '') { RecTypesStr +=','; } 
            RecTypesStr += RT_in_quotes ; 
        } 
        
        //query = 'Select Id,Title,RecordTypeId,Recordtype.developername from knowledge__kav where WHERE Id != \'\' AND RecordType.DeveloperName IN :RTCatMap.KeySet() AND Id not in (Select ParentId from knowledge__DataCategorySelection)';
        query = 'SELECT Id, Title, RecordTypeId FROM Knowledge__kav WHERE Language = \''+ sLanguage + '\' AND RecordType.DeveloperName IN (' + RecTypesStr + ')' ; 
  }  
  
  public Database.QueryLocator start(Database.BatchableContext BC) 
  {
        System.debug('<<< Query >>> ' + query);
        return Database.getQueryLocator(query);
  }
  
  public void execute(Database.BatchableContext BC, List<Knowledge__kav> kList)
  {   
        totalrecs += kList.size();
        Knowledge__DataCategorySelection[] newRecords = new Knowledge__DataCategorySelection[] {};
        Set<Id> existingRecordIds = new Set<Id>();   
        Set<Id> articleIds = new Set<Id>();
        for (Knowledge__kav k :Klist) 
        {
            If (null != k.id) 
            articleIds.add(k.Id);    
        }
        
        for (Knowledge__DataCategorySelection dcs :[SELECT Id, ParentId, DataCategoryGroupName, DataCategoryName
                                                    FROM Knowledge__DataCategorySelection
                                                    WHERE ParentId IN :articleIds]) {
            if ((dcs.DataCategoryGroupName == 'IR_Global_Category_Group')) {
                existingRecordIds.add(dcs.ParentId);        
            }                                                       
        }
        
        for (Knowledge__kav k :kList) 
        { 
            if ((null != k.id) && (!existingRecordIds.contains(k.Id))) 
            {
                String RT_DevName = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosById().get(k.RecordTypeId).getDeveloperName();
                newRecords.add(new Knowledge__DataCategorySelection(ParentId = k.Id, 
                                DataCategoryGroupName = 'IR_Global_Category_Group', DataCategoryName = RTCatMap.get(RT_DevName)));    
            }
        }
        
        if (!newRecords.isEmpty())
        {
            for (Knowledge__DataCategorySelection  n :newRecords) 
            {
                String successmsg = 'Attempting to Add Data Category for Knowledge Article Id : ' + n.ParentId + '\n';
                success_List.add(successmsg);
            }
            
            Database.SaveResult[] SaveResultList = Database.insert(newRecords, false);          
        
            for(integer i =0; i<newRecords.size();i++)
            {
            String msg='Error Adding Data Category to the Knowledge Article : ';
            if(!SaveResultList[i].isSuccess())
            {
                msg += newRecords.get(i).ParentId + '\n'+'Error: "';        
                for(Database.Error err: SaveResultList[i].getErrors())
                {  
                     msg += err.getmessage()+'"\n\n';
                } 
            }
            if(msg!='')
                exception_List.add(msg);
            } 
        
        } 
  }
  
  global void execute(SchedulableContext sc) 
  {
      Database.executeBatch(new UpdateDataCategoryOnKnowledge_German(),100);
  }  
  
  public void finish(Database.BatchableContext BC)
  {
        system.debug('Total Number of Knowledge Articles processed:' + totalrecs);
        for (String s :success_List) 
        {
            System.Debug('\n' + s + '\n');
        }
        for (String x :exception_List) 
        {
            System.Debug('\n' + x + '\n');
        }
  }
}