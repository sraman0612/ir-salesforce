/* 
 *  Snehal Chabukswar || Date 01/07/2020 
 *  closed case caseAge update
 *  exceute CTS_OM_CalculateOpenCaseAge batch class 
*/
global class CTS_OM_CalculateOpenCaseAgeScheduler implements Schedulable{
    //Execute method 
    //@parameter SchedulableContext variable
    global void execute(SchedulableContext ctx) {
        
        Id batchJobId = Database.executeBatch(new CTS_OM_CalculateOpenCaseAge(null,false), 25);
    }
}