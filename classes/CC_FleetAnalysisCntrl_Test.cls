@isTest
public with sharing class CC_FleetAnalysisCntrl_Test {
    static testMethod void myUnitTest() {
        CC_Counties__c cntry=new CC_Counties__c();
        cntry.Name='India';
        insert cntry;
        TestDataUtility testData = new TestDataUtility();
        TestDataUtility.createStateCountries(); 
             
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        List<account> acclist = new List<account>();
        account acc = testData.createAccount('Club Car: Channel Partner');
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        insert acc;
        
        Contact con = TestUtilityClass.createContact(acc.id);
        insert con;
        
        CC_Asset__c assetobj=new CC_Asset__c();
        assetobj.Account__c=acc.id;
        insert assetobj;
    
        CC_Fleet_Analysis__c obj=new CC_Fleet_Analysis__c();
        obj.Contact__c=con.id;
        obj.Account__c=acc.id;
        obj.Status__c='Analysis Completed';
        
        insert obj;
        
        CC_Asset_Analysis__c assetanlysObj=new CC_Asset_Analysis__c();
        assetanlysObj.Status__c='Analysis Completed';
        assetanlysObj.Fleet_Analysis__c=obj.id;
        insert assetanlysObj;
        
        ApexPages.StandardController sc = new ApexPages.standardController(obj);
        CC_FleetAnalysisCntrl login=new CC_FleetAnalysisCntrl(sc);
        ApexPages.currentPage().getParameters().put('fleetId', obj.Id);
        login.selectedAssetId=assetanlysObj.id;
        
        Attachment attach=new Attachment();       
        attach.Name='Test';
        Blob bodyBlob=Blob.valueOf('Test');
        attach.body=bodyBlob;
        attach.parentId=obj.id;
        attach.ContentType='Test';
        insert attach;
       
        login.AssetDetails();
        login.hideAssetAnalysis();
        login.SaveAnalysis();
        login.CompleteAnalysis();
      
    }
    static testMethod void myUnitTest1() {
        CC_Counties__c cntry=new CC_Counties__c();
        cntry.Name='India';
        insert cntry;
        TestDataUtility testData = new TestDataUtility();
        TestDataUtility.createStateCountries(); 
             
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        List<account> acclist = new List<account>();
        account acc = testData.createAccount('Club Car: Channel Partner');
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        insert acc;
        
        Contact con = TestUtilityClass.createContact(acc.id);
        insert con;
        
        CC_Asset__c assetobj=new CC_Asset__c();
        assetobj.Account__c=acc.id;
        insert assetobj;
    
        CC_Fleet_Analysis__c obj=new CC_Fleet_Analysis__c();
        obj.Contact__c=con.id;
        obj.Account__c=acc.id;
        obj.Status__c='Analysis Completed';
       
        insert obj;
        
        CC_Asset_Analysis__c assetanlysObj=new CC_Asset_Analysis__c();
        assetanlysObj.Status__c='Analysis Completed';
        assetanlysObj.Fleet_Analysis__c=obj.id;
        insert assetanlysObj;
        
        ApexPages.StandardController sc = new ApexPages.standardController(obj);
        CC_FleetAnalysisCntrl login=new CC_FleetAnalysisCntrl(sc);
        ApexPages.currentPage().getParameters().put('fleetId', obj.Id);
        login.selectedAssetId=assetanlysObj.id;
        
        Attachment attach=new Attachment();       
        attach.Name='Test';
        Blob bodyBlob=Blob.valueOf('Test');
        attach.body=bodyBlob;
        attach.parentId=obj.id;
        attach.ContentType='Test';
        insert attach;
       
        login.AssetDetails();
        login.hideAssetAnalysis();
        login.SaveAnalysis();
        CC_FleetAnalysisCntrl.SaveAnalysis(attach.Name,attach.ContentType,'Test',attach.Id,obj.Id);
        login.CompleteAnalysis();
      
    }
}