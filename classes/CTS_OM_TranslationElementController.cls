public class CTS_OM_TranslationElementController {
	@AuraEnabled
    public static Object translateText(String textForTranslation, String recordId) {
        if(textForTranslation != null  && textForTranslation.startsWith('isField~') ){
            String[] textSplit = textForTranslation.split('~');
            if(textSplit.size() > 1){
                String soql = 'select id, ' +textSplit[1] + ' FROM Case where id = : recordId';
                for(Case cs : Database.query(soql)){
                    textForTranslation = cs.get(textSplit[1]) != null ? (String)cs.get(textSplit[1]) : '';
                }
            }
        }
    	Object translations = String.isEmpty(textForTranslation) ? null : CTS_OM_TranslatorController.callGoogleAPI(textForTranslation);
        System.debug(':::translations'+translations);
        return translations;
    }
        
}