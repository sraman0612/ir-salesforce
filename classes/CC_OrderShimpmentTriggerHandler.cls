/**********************************************************************
 * Description       : Trigger Handler for CC_Order_Shipment__C
 * Created Date      : 10/07/2017
 * *******************************************************************/


public class CC_OrderShimpmentTriggerHandler {
    
    public static void deleteAssets(List<CC_Order_Shipment__c> ordershipmentList){
        
        system.debug('@@@@ in delete trigger'+ordershipmentList);
        //getting all assets belongs to this shipment
        List<CC_Asset__c> assetsList = new List<CC_Asset__c>();
        Set<Id> OrderIds             = new Set<Id>();
       /* List<CC_Order_Shipment__c> templist = [select Order__c from CC_Order_Shipment__c 
                                                    where id IN:ordershipmentList AND Car_Assets_Created__c = true];
        system.debug('@@@@templist'+templist);*/
        
        for(CC_Order_Shipment__c ordershipment : ordershipmentList){
            if(ordershipment.Car_Assets_Created__c){
                OrderIds.add(ordershipment.Order__c);
            }
        }
        
        if(!OrderIds.isEmpty()){
            assetsList = [select id from CC_Asset__c
                                where Order__c IN:OrderIds];
        }
        
        system.debug('@@@assetsList'+assetsList);
        if(!assetsList.isEmpty()){
            try{
                Delete assetsList;
            }
            catch(Exception ex){
                system.debug('@@@@@@ exception'+ ex);
            }
        }
        
        
        
    }

}