@isTest
public class cc_PavilionHomeControllerTest {
    
    public static Account acc;
    public static user testUser;
    public static contact con;
    public static user portalAccountOwner1;
    public cc_PavilionHomeControllerTest(){
          List<PavilionSettings__c> psettingList = TestUtilityClass.createPavilionSettings();
    insert psettingList;
   
        
    }
  
  @testSetup static void setupData() {

    
  
    testUser = createPartnerUser(10);  
      
      system.runAs(portalAccountOwner1){  
    //Create National Account
    Account acc1 = TestUtilityClass.createAccount2();
      acc1.BillingCountry = 'USA';
       acc1.BillingCity = 'Augusta';
            acc1.BillingState = 'GA';
            acc1.BillingStreet = '2044 Forward Augusta Dr';
            acc1.BillingPostalCode = '566';
            acc1.CC_Shipping_Billing_Address_Same__c = true;
    insert acc1;
   
    CC_Order__c carOrder = TestUtilityClass.createNatAcctOrder(acc.id,acc1.id);
    Case [] caseLst2Insert = new List<Case>();
    Case cas = TestUtilityClass.createCustomCase(con.id,'On Hold','pavcasereason');
    caseLst2Insert.add(cas);
    Case ncas = TestUtilityClass.createCustomCase(con.id,'Closed','pavcasereason');
    ncas.RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();      
    caseLst2Insert.add(ncas);
    Case ncas1 = TestUtilityClass.createCustomCase(con.id,'Closed','pavcasereason');
    ncas1.RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();      
    caseLst2Insert.add(ncas1);      
    Pavilion_Navigation_Profile__c pnp = TestUtilityClass.createPavilionNavigationProfile('ccircotst');
    insert pnp;
    
    Case trnCase = TestUtilityClass.createCaseWithRecordType(con.id,'','CC Training Request');
    trnCase.CC_Courses__c='Preventive Maintenance';
    trnCase.CC_Date_Of_Course__c=System.today();
    trnCase.CC_Time_Of_Course__c='2 days';
    caseLst2Insert.add(trnCase);
    insert caseLst2Insert;
    
    CC_Pavilion_Content__c pavilionContent = new CC_Pavilion_Content__c(
            RecordTypeId = Schema.SObjectType.CC_Pavilion_Content__c.getRecordTypeInfosByName().get('Home Page Bulletins').getRecordTypeId(),
            showOnPavilionHome__c = true, 
            Title__c = 'Title', 
            Type__c = 'Home Page Bulletin',
            Agreement_Type__c = 'Golf Utility; Commercial Utility',
            CC_Region__c = 'United States; Canada');
    insert pavilionContent;
    AccountTeamMember accountTeamMember = new AccountTeamMember(UserID = testUser.Id,TeamMemberRole = 'Sales Manager',AccountId = acc.Id);
    insert accountTeamMember;
    // for CAD Pricing 
    Account account = new Account(
      RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
      Name = 'Test',
      CC_Price_List__c = 'PCOPI',
      CC_Customer_Number__c = '5621',
      CC_Global_Region__c = 'United States',
      CC_Bulletins_Currency__c = 'USD',BillingCountry = 'IN',
                              BillingCity = 'BG',BillingState = 'TG',BillingStreet = '17th avenue',BillingPostalCode = '168',
                              CC_Shipping_Billing_Address_Same__c=true
    );
    insert account;
    ContentVersion cv = new ContentVersion(
            Content_Title__c = 'Test Title',
            Bulletin_Title__c = 'Test Bulletin Title', 
            CC_Bulletin_Type__c='Test Bulletine Type',
            Title = 'Title',
            Agreement_Type__c = 'Golf Utility; Commercial Utility', 
            Currency__c = 'USD',
            RecordTypeId = Schema.SObjectType.ContentVersion.getRecordTypeInfosByName().get('bulletin').getRecordTypeId(),
            CC_Region__c = 'United States',
            PathOnClient = 'TestPath',
            VersionData = Blob.valueof('myString')

    );
    insert cv;
    Contract contractInstance = new Contract(Name = 'test name',AccountId = account.Id,CC_Type__c = 'GPSI - Club Car',CC_Sub_Type__c = 'CPO - 48 lock',
                                    CC_Contract_Status__c = 'Suspended',CC_Contract_End_Date__c = Date.today());
    insert contractInstance;
    Contract contractInstance2 = new Contract(Name = 'test name',AccountId = account.Id,CC_Type__c = 'Dealer/Distributor Agreement',CC_Contract_Status__c = 'Active',
                                     CC_Contract_End_Date__c = Date.today(),CC_Sub_Type__c = 'Golf Utility; Commercial Utility');
    insert contractInstance2;
    CC_Pavilion_Content__c data = new CC_Pavilion_Content__c(CC_Body_1__c = 'TestBody',Title__c = 'errorMessage');
    insert data;
      }      
  }

  public static testmethod void test1() {
    User u = [SELECT Id FROM User WHERE Alias = 'test1236'];
    Account a = [SELECT Id, CC_Global_Region__c FROM ACCOUNT WHERE Name='acctforremotetest'];
    test.starttest();
    System.runAs (u) {
      CC_PavilionTemplateController tCTLR = new CC_PavilionTemplateController();
      cc_PavilionHomeController CTLR = new cc_PavilionHomeController(tCTLR);
      CTLR.getunselectedvalues();
      CTLR.getSelectedValues();
      CTLR.redirectGuest();
      CTLR.save();
      CTLR.getAllListofprofiles();
      cc_PavilionHomeController.getTeamMemberRole(a.Id);
      cc_PavilionHomeController.getPavilionContent(a.Id,a.CC_Global_Region__c);
      cc_PavilionHomeController.getOrderItems(a.Id);
    }
    test.stoptest();
  }
    
    
      public static  user createPartnerUser(integer i){
        
     
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='User', Lastname='Deactivated',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           

            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
              //creating state country custom setting data
            TestDataUtility.createStateCountries();

             //creating sales rep here
              TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;
            
           
            acc = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
    acc.Name='acctforremotetest';
   acc.BillingCountry = 'USA';
            acc.BillingCity = 'Augusta';
            acc.BillingState = 'GA';
            acc.BillingStreet = '2044 Forward Augusta Dr';
            acc.BillingPostalCode = '566';
            acc.CC_Shipping_Billing_Address_Same__c = true;
    insert acc;

            //Create contact
            con = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = acc.Id,
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            Database.insert(con);

            //Create user\
            Profile portalProfile = [SELECT Id FROM Profile where name ='CC_PavilionCommunityUser' LIMIT 1];
             testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = portalProfile.id,Alias = 'test1236',ContactId=con.Id,
                        Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                        TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst',
                        Pavilion_Tiles__c = ';Custom Solutions Project Request;CustomerVIEW;Marketing Events;PreApproved Requests;Transportation Requests;Marketing_MyCo-OpAccount;Marketing Hole-In-One');
    insert testUser;
        }
       return testUser; 
    }
}