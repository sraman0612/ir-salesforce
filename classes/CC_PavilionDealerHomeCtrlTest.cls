@isTest
public class CC_PavilionDealerHomeCtrlTest 
{
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
    }
    
    public static testMethod void PavilionDealerHomeCtrl_Test()
    {
        // Creation Of Test Data
        Account acc = TestUtilityClass.createAccount();
        acc.Type='Dealer';
        insert acc ; 
        System.assertEquals('Dealer',acc.Type);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con; 
        System.assertEquals(con.AccountId, acc.Id);
        System.assertNotEquals(null,con.Id);
        
       /* Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        System.assertEquals('Sample Group',grp.Name);
        System.assertNotEquals(null,grp.Id);
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        
        System.assertEquals('PavilionPublicGroupID',secondPavnSettings.Name);
        System.assertNotEquals(null,secondPavnSettings.Id); */
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        u.CC_Pavilion_Navigation_Profile__c = '';
        insert u;
        System.assertEquals(u.ContactId,con.Id);
        System.assertNotEquals(null,u.Id);
        System.runAs(u)
        {
            
            String accountId;
            accountId=[SELECT accountId from User  where id =:u.Id].accountId;
            system.debug('the accountId is'+accountId);
            Contract con1 = new Contract();
            con1.Name='Test';
            con1.CC_Type__c='Dealer/Distributor Agreement';
            con1.AccountId=accountId;
            con1.CC_Sub_Type__c='Industrial Utility';
            insert con1;
            List<CC_Pavilion_Content__c> cont = new List<CC_Pavilion_Content__c>();  
            CC_Pavilion_Content__c content = TestUtilityClass.CreatePavilionContentWithParams('Sales','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content1= TestUtilityClass.CreatePavilionContentWithParams('Customer Experience','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content2= TestUtilityClass.CreatePavilionContentWithParams('Marketing Co-op Advertising','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content3= TestUtilityClass.CreatePavilionContentWithParams('Technical Publications','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content4= TestUtilityClass.CreatePavilionContentWithParams('Custom Solutions','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content5= TestUtilityClass.CreatePavilionContentWithParams('Channel Communications','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content6= TestUtilityClass.CreatePavilionContentWithParams('Dealer Manual Home','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content7= TestUtilityClass.CreatePavilionContentWithParams('Parts Bulletins','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content8= TestUtilityClass.CreatePavilionContentWithParams('Pricing Bulletins','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content9= TestUtilityClass.CreatePavilionContentWithParams('Product Support Technical Services','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content10= TestUtilityClass.CreatePavilionContentWithParams('Warranty Home','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content11= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Battery Testing (6v,8v,12v)','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content12= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Club Car Warranty & Parts Labels','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content13= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Club Car Warranty Management System Claim Processing Overview','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content14= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Warranty Claim submission','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content15= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Warranty Compensation','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content16= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Warranty Introduction','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content17= TestUtilityClass.CreatePavilionContentWithParams('Warranty_Warranty Policies','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content18= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Policies Procedures Intl','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content19= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Policies Procedures','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content20= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Sales Programs International','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content21= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Sales Programs – dealers','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content22= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Sales Programs Distributor','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content23= TestUtilityClass.CreatePavilionContentWithParams('US AFTERMARKET USED & CUSTOM VEHICLE FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content24= TestUtilityClass.CreatePavilionContentWithParams('CANADIAN AFTERMARKET USED & CUSTOM VEHICLES FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content25= TestUtilityClass.CreatePavilionContentWithParams('US & CANADA GOLF CAR DISTRIBUTOR VEHICLE FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content26= TestUtilityClass.CreatePavilionContentWithParams('CANADIAN GOLF CAR DISTRIBUTOR NEW VEHICLE FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content27= TestUtilityClass.CreatePavilionContentWithParams('CANADIAN GOLF TURF UTILITY NEW VEHICLE FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content28= TestUtilityClass.CreatePavilionContentWithParams('INSTRUCTIONS FOR ENTERING AN INVOICE','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content29= TestUtilityClass.CreatePavilionContentWithParams('TO CHECK ON THE STATUS OF A SUBMITTED INVOICE','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content30= TestUtilityClass.CreatePavilionContentWithParams('Printing an Account Statement','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content31= TestUtilityClass.CreatePavilionContentWithParams('Printing an Invoice','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content32= TestUtilityClass.CreatePavilionContentWithParams('Letter of Credit Instructions','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content33= TestUtilityClass.CreatePavilionContentWithParams('Negotiating/accepting/paying bank Instructions','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content34= TestUtilityClass.CreatePavilionContentWithParams('WIRE INSTRUCTIONS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content35= TestUtilityClass.CreatePavilionContentWithParams('Lock Box AddressES','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content36= TestUtilityClass.CreatePavilionContentWithParams('US commercial & industrial utility vehicle finance programs','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content37= TestUtilityClass.CreatePavilionContentWithParams('Canadian commercial & industrial utility vehicle finance programs','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content38= TestUtilityClass.CreatePavilionContentWithParams('CANADIAN XRT NEW VEHICLE FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content39= TestUtilityClass.CreatePavilionContentWithParams('Canada Commercial & Industrial Utility finance programs','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            CC_Pavilion_Content__c content40= TestUtilityClass.CreatePavilionContentWithParams('USA & CANADA AFTERMARKET USED & CUSTOM VEHICLES FINANCE PROGRAMS','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            content.CC_Region__c='Japan';
            content1.CC_Region__c='Japan';
content2.CC_Region__c='Japan';
content3.CC_Region__c='Japan';
content4.CC_Region__c='Japan';
content5.CC_Region__c='Japan';
content6.CC_Region__c='Japan';
content7.CC_Region__c='Japan';
content8.CC_Region__c='Japan';
content9.CC_Region__c='Japan';
content10.CC_Region__c='Japan';
content11.CC_Region__c='Japan';
content12.CC_Region__c='Japan';
content13.CC_Region__c='Japan';
content14.CC_Region__c='Japan';
content15.CC_Region__c='Japan';
content16.CC_Region__c='Japan';
content17.CC_Region__c='Japan';
content18.CC_Region__c='Japan';
content19.CC_Region__c='Japan';
content20.CC_Region__c='Japan';
content21.CC_Region__c='Japan';
content22.CC_Region__c='Japan';
content23.CC_Region__c='Japan';
content24.CC_Region__c='Japan';
content25.CC_Region__c='Japan';
content26.CC_Region__c='Japan';
content27.CC_Region__c='Japan';
content28.CC_Region__c='Japan';
content29.CC_Region__c='Japan';
content30.CC_Region__c='Japan';
content31.CC_Region__c='Japan';
content32.CC_Region__c='Japan';
content33.CC_Region__c='Japan';
content34.CC_Region__c='Japan';
content35.CC_Region__c='Japan';
content36.CC_Region__c='Japan';
content37.CC_Region__c='Japan';
content38.CC_Region__c='Japan';
content39.CC_Region__c='Japan';
content40.CC_Region__c='Japan';
            cont.add(content);
            cont.add(content1);
            cont.add(content2);
            cont.add(content3);
            cont.add(content4);
            cont.add(content5);
            cont.add(content6);
            cont.add(content7);
            cont.add(content8);
            cont.add(content9);
            cont.add(content10);
            cont.add(content11);
            cont.add(content12);
            cont.add(content13);
            cont.add(content14);
            cont.add(content15);
            cont.add(content16);
            cont.add(content17);
            cont.add(content18);
            cont.add(content19);
            cont.add(content20);
            cont.add(content21);
            cont.add(content22);
            cont.add(content23);
            cont.add(content24);
            cont.add(content25);
            cont.add(content26);
            cont.add(content27);
            cont.add(content28);
            cont.add(content29);
            cont.add(content30);
            cont.add(content31);
            cont.add(content32);
            cont.add(content33);
            cont.add(content34);
            cont.add(content35);
            cont.add(content36);
            cont.add(content37);
            cont.add(content38);
            cont.add(content39);
            cont.add(content40);
            insert cont;
            test.startTest();
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
             controller.globalRegion = 'Japan';
            CC_PavilionDealerHomeCtrl dealerHomeCtrl = new CC_PavilionDealerHomeCtrl(controller);
            dealerHomeCtrl.marketingcontentRender = true;
            dealerHomeCtrl.customerexperienceRender = true;
            dealerHomeCtrl.technicalpublicationsRender = true;
            dealerHomeCtrl.customsolutionsRender = true;   
            dealerHomeCtrl.channelcommRender = true;
            dealerHomeCtrl.partsbullRender = true;
            dealerHomeCtrl.ctrlwarrenty1Render = true;
            dealerHomeCtrl.ctrlwarrenty2Render = true;
            dealerHomeCtrl.ctrlwarrenty3Render = true;
            dealerHomeCtrl.ctrlwarrenty4Render = true;
            dealerHomeCtrl.ctrlwarrenty5Render = true;
            dealerHomeCtrl.ctrlwarrenty6Render = true;
            dealerHomeCtrl.ctrlwarrenty7Render = true;
            dealerHomeCtrl.ctrlwarrenty8Render = true;
            dealerHomeCtrl.ctrlwarrenty8Render = true;
            dealerHomeCtrl.prodsuppRender = true;
            dealerHomeCtrl.finance17Render = true;
            dealerHomeCtrl.homedealerRender = true;
            dealerHomeCtrl.pstsRender = true;
            dealerHomeCtrl.finance17 = content40;
            dealerHomeCtrl.prodsupp = content40;
            test.stopTest();
        }
    }
    public static testMethod void PavilionDealerHomeCtrlSecond_Test()
    {
        Account acc = TestUtilityClass.createAccount();
        acc.Type='Distributor';
        insert acc ; 
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con; 
        User u = TestUtilityClass.createUser('CC_PavilionCommunityUser', con.Id);
        u.CC_Pavilion_Navigation_Profile__c = '';
        insert u;
        System.runAs(u)
        {
            
            String accountId;
            accountId=[SELECT accountId from User  where id =:u.Id].accountId;
            system.debug('the accountId is'+accountId);
            Contract con1 = new Contract();
            con1.Name='Test';
            con1.CC_Type__c='Dealer/Distributor Agreement';
            con1.AccountId=accountId;
            con1.CC_Sub_Type__c='Industrial Utility';
            insert con1;
            List<CC_Pavilion_Content__c> cont = new List<CC_Pavilion_Content__c>();  
            CC_Pavilion_Content__c content= TestUtilityClass.CreatePavilionContentWithParams('Service Parts Sales Programs Distributor','Dealer Manual','Golf Car Distributor;Commercial Utility;Industrial Utility');
            content.CC_Region__c='Japan';
            cont.add(content);
            insert cont;
            test.startTest();
             CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
             controller.globalRegion = 'Japan';
            CC_PavilionDealerHomeCtrl dealerHomeCtrl = new CC_PavilionDealerHomeCtrl(controller);
            test.stopTest();
        }
    }
}