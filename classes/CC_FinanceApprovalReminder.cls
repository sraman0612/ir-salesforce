public without sharing class CC_FinanceApprovalReminder implements Database.Batchable<sObject>, Schedulable{

	@TestVisible
    private Integer elapsedTimeInMinutesExpiration;
    
    /* Scheduled job implementation */
    public void execute(SchedulableContext sc) {
        
        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
        
        system.debug('batchControls: ' + batchControls);
        
        if (batchControls != null && String.isNotBlank(batchControls.Finance_Reminder_Template_Map__c)){               
			Database.executeBatch(new CC_FinanceApprovalReminder(), 25);
        }
    }
    
    /* Batch job implementation */
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();   
        
        // Build a set of approval processes that are to be excluded 
        List<String> excludedApprovalProcesses = batchControls != null && String.isNotBlank(batchControls.Finance_Reminder_Approval_Exclusions__c) ? batchControls.Finance_Reminder_Approval_Exclusions__c.split(',') : new String[]{};
        Set<String> cleanedExcludedApprovalProcesses = new Set<String>();
        
        for (String excludedApprovalProcess : excludedApprovalProcesses){
            cleanedExcludedApprovalProcesses.add(excludedApprovalProcess.trim());
        }        
        
        // Need to find all pending approvals that are either 48 hours old or will be 48 hours old by 5 pm EST of the current day      
        DateTime now = DateTime.now();  
        Datetime todayAt5 = DateTime.newInstance(now.year(), now.month(), now.day(), 17, 0, 0);
        
        system.debug('now: ' + now);
        system.debug('todayAt5: ' + todayAt5);
        
        Integer minutesTil5 = todayAt5 > now ? Integer.valueOf(((todayat5.getTime() - now.getTime()) /1000/60)) : 0;
        
        system.debug('minutesTil5: ' + minutesTil5);
        
        // If it is not null then a test class has overridden the value
        if (elapsedTimeInMinutesExpiration == null){
        	
        	// Determine how old approvals must be to be reminded
        	// Reminder hours minus any additional time until 5 pm today
        	Integer reminderHours = batchControls != null && batchControls.Finance_Reminder_Approval_Hours__c != null ? Integer.valueOf(batchControls.Finance_Reminder_Approval_Hours__c) : 48;
        	elapsedTimeInMinutesExpiration = (reminderHours * 60) - minutesTil5;
        }
        
        system.debug('elapsedTimeInMinutesExpiration: ' + elapsedTimeInMinutesExpiration);  
        
        // Get the list of objects that have a reminder template defined to reduce the approvals to process through
        Map<String, String> objectTemplateMap = getFinanceReminderobjectTemplateMap(batchControls);
        Set<String> configuredObjects = objectTemplateMap.keySet();
        
        system.debug('configuredObjects: ' + configuredObjects);
        
        return Database.getQueryLocator('Select ActorId, ElapsedTimeInMinutes, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId From ProcessInstanceWorkitem Where ProcessInstance.ProcessDefinition.TableEnumOrId in :configuredObjects and ProcessInstance.ProcessDefinition.DeveloperName not in :cleanedExcludedApprovalProcesses and ElapsedTimeInMinutes >= :elapsedTimeInMinutesExpiration');
   	}

   	public void execute(Database.BatchableContext BC, List<ProcessInstanceWorkitem> approvals){
  
        // Get the configured objects and the email templates to be used
        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
        Map<String, String> objectTemplateMap = getFinanceReminderobjectTemplateMap(batchControls);
		    ProcessInstanceWorkitem[] approvalsToProcess = new ProcessInstanceWorkitem[]{}; 
			
		    // Find all related orders being approved, need to look up the terms code later
		    Set<Id> orderIds = new Set<Id>();
		    
		    //Mgr will only be included after x number of hours
		    DateTime now = DateTime.now();  
        Datetime todayAt5 = DateTime.newInstance(now.year(), now.month(), now.day(), 17, 0, 0);
		    Integer minutesTil5 = todayAt5 > now ? Integer.valueOf(((todayat5.getTime() - now.getTime()) /1000/60)) : 0;
		    Integer mgrReminderHours = batchControls != null && batchControls.Finance_Reminder_Manager_Approval_Hours__c != null ? Integer.valueOf(batchControls.Finance_Reminder_Manager_Approval_Hours__c) : 8760;
        Integer mgrElapsedTimeInMinutesExpiration = (mgrReminderHours * 60) - minutesTil5;
		
        for (ProcessInstanceWorkitem approval : approvals){
        	        	
        	if (approval.ProcessInstance.TargetObjectId.getSObjectType() == CC_Order__c.getSObjectType()){
        		orderIds.add(approval.ProcessInstance.TargetObjectId);
        	}
        }
        
        system.debug('orderIds: ' + orderIds);
		        
        Set<Id> orderIdsToProcess = new Set<Id>();
        
        if (orderIds.size() > 0){
        	
	        // Find all of the non-floorplan orders (based on terms code)
			Set<String> floorplanTermsCodes = new Set<String>();
			
			for (Club_Car_Payment_Terms__c terms : Club_Car_Payment_Terms__c.getAll().values()){
				
				if (terms.CC_Car_Terms_Method__c == 'Floorplan'){
					floorplanTermsCodes.add(terms.Name);
				}
			}	        	
        	
			for (CC_Order__c order : [Select Id, Terms_Code__c From CC_Order__c Where Id in :orderIds and Terms_Code__c not in :floorplanTermsCodes]){
				orderIdsToProcess.add(order.Id);
			}        	
        }
        
        system.debug('orderIdsToProcess: ' + orderIdsToProcess);
		
		// Filter out approvals on objects that are not configured or orders that are not qualified
        for (ProcessInstanceWorkitem approval : approvals){
        	
        	system.debug('evaluating approval: ' + approval);
        	
        	Id recordId = approval.ProcessInstance.TargetObjectId;
        	String objectName = String.valueOf(recordId.getSObjectType());
        	
        	system.debug('recordId: ' + recordId);
        	system.debug('objectName: ' + objectName);
        	
        	if (objectTemplateMap.keySet().contains(objectName)){
        		
        		// Filter out orders that are not qualified
        		if (objectName == 'CC_Order__c'){
        			
        			if (orderIdsToProcess.contains(recordId)){
        				approvalsToProcess.add(approval);
        			}
        			else{
        				system.debug('order not qualified for notification based on terms code');
        			}
        		}
        		else{
					approvalsToProcess.add(approval);
        		}
        	}
        	else{
        		system.debug('email template not found/configured for this object: ' + objectName);
        	}
        }		
        
        system.debug('approvalsToProcess: ' + approvalsToProcess);
        
        if (approvalsToProcess.size() > 0){

	        Set<Id> actorIds = new Set<Id>();
	        
	        for (ProcessInstanceWorkitem approval : approvalsToProcess){
	            actorIds.add(approval.ActorId);
	        }
	        
	        // Fetch the actor user records so we can determine if they are active and if they have an active manager
	        Map<Id, User> userMap = new Map<Id, User>([Select Id, isActive, ManagerId, Manager.isActive From User Where Id in :actorIds]);  
	        
	        // Build a map of email template names and their Ids to send out the emails
	        Map<String, Id> emailTemplateMap = new Map<String, Id>();
	        
	        for (EmailTemplate template : [Select Id, DeveloperName From EmailTemplate Where DeveloperName in :objectTemplateMap.values()]){
	        	emailTemplateMap.put(template.DeveloperName, template.Id);
	        }
	        
	        system.debug('emailTemplateMap: ' + emailTemplateMap);
	        
	        // Send an email to the approver and their manager
	        Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
	            
	        for (ProcessInstanceWorkitem approval : approvalsToProcess){
	        	
	        	system.debug('processing approval: ' + approval);
	        	
	        	String objectName = String.valueOf(approval.ProcessInstance.TargetObjectId.getSObjectType());
	        	Id recordId = approval.ProcessInstance.TargetObjectId;
	        	String templateName = objectTemplateMap.get(objectName);
	        	Id emailTemplateId = String.isNotBlank(templateName) ? emailTemplateMap.get(templateName) : null;
	        	
	        	system.debug('emailTemplateId: ' + emailTemplateId);
	        	
	        	// No email will be sent if the email template was not found
	        	if (emailTemplateId != null){
	            
		            User actor = userMap.get(approval.ActorId);
		            
		            system.debug('actor: ' + actor);
		            
		            // Make sure the approver is still active
		            if (actor.isActive){
		                
		                String[] ccAddresses = new String[]{};
		                    
		                // CC the approver's manager if they are active and approval is older than the wait period for manager approval
		                if (actor.ManagerId != null && actor.Manager.isActive && 
		                    batchControls.Finance_Approval_Manager_Inclusion__c && approval.ElapsedTimeInMinutes >= mgrElapsedTimeInMinutesExpiration){
		                    ccAddresses.add(actor.ManagerId); 
		                }
		                
    			        	// In order to get around the error "WhatId is not available for sending emails to UserIds.", 
    			        	// the workaround below is in place so that merge fields on the template work and still send to the user records to avoid hitting email limits
    			        	Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(emailTemplateId, approval.ActorId, recordId);
    			        	
    			        	OrgWideEmailAddress[] oweaLst = [select Id from OrgWideEmailAddress where Address = 'noreply@clubcar.com'];
    			        	Id oweaId = oweaLst.size() > 0 ? oweaLst.get(0).Id : null;
        						String emailSubject = email.getSubject();
        						String emailTextBody = email.getPlainTextBody();		                
		                
		                messages.add(EmailService.buildSingleEmailMessage(
		                    approval.ActorId, 
		                    null, 
		                    ccAddresses, 
		                    null,
		                    emailSubject,
		                    emailTextBody, 
		                    null, 
		                    null,
		                    oweaId)
		                );
		            }
	        	}
	        }
	    
	        system.debug('reminders to send: ' + messages.size());
	        
	        if (messages.size() > 0){
	            EmailService.sendSingleEmailMessages(messages);
	        }
        }
   	}
   	
   	private Map<String, String> getFinanceReminderobjectTemplateMap(CC_Batch_Controls__c batchControls){
   		
   		Map<String, String> objectTemplateMap = new Map<String, String>();
   		
        if (batchControls != null && String.isNotBlank(batchControls.Finance_Reminder_Template_Map__c)){
        	
        	try{       
        		 		
        		Map<String, Object> deserializedMap = (Map<String, Object>)JSON.deserializeUntyped(batchControls.Finance_Reminder_Template_Map__c);
        		
        		for (String key : deserializedMap.keySet()){   			
        			objectTemplateMap.put(key, (String)deserializedMap.get(key));
        		}
        	}
        	catch(Exception e){
        		system.debug('error while deserializing batchControls.Finance_Reminder_Template_Map__c: ' + e.getMessage());
	            apexLogHandler logHandler = new apexLogHandler('CC_FinanceApprovalReminder', 'getFinanceReminderobjectTemplateMap', e.getMessage());
            	insert logHandler.logObj;        		
        	}
        }   
        
        system.debug('objectTemplateMap: ' + objectTemplateMap);
        
        return objectTemplateMap;		
   	}   	

   	public void finish(Database.BatchableContext BC){
		
   	}
}