global class CC_Links_StatementController{
    
    public Id Acctid{get;set;}
    public String CustomerName{get;set;}
    public String CustomerNumber{get;set;}
    public String CustomerFinRep{get;set;}
    public String remitAddress{get;set;}  
    public String Address1{get;set;}
    public String customerAddress{get;set;}
    public String customerAddress3{get;set;}
    public String customerAddress4{get;set;}
    public Double approvedOpenOrders{get;set;}
    public Double pendingApprovalOpenOrders{get;set;}
    public Double floorPlansOpenOrders{get;set;}
    public Double CreditLine{get;set;}
    public Double CreditUsed{get;set;}
    public Double CurrentDue{get;set;}
    public Double FutureDue{get;set;}
    public Double PendingCharges{get;set;}
    public Double AvailableCredit{get;set;}
    public Double Approved{get;set;}
    public Double PendingApproval{get;set;}
    public Double FloorPlans{get;set;}
    public List<SelectOption> codeOptions{get;set;}
    
    public List<SelectOption> getDisputeCodes(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = CC_Statements__c.CC_Dispute_Code__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    public CC_Links_StatementController(CC_PavilionTemplateController controller) {
            Acctid = controller.acctid;
            CustomerNumber = controller.CustomerNumber;
            CustomerName = controller.AccountName;
            controllerFunction();
    }
    
    @RemoteAction
    global static List<CC_Statements__c> getStatementData(string aid){
    system.debug('********accid is***'+aid);
    //@Priyanka Baviskar added Installment Number field for Ticket: Adding Installment Number on Statements object on 7/1/2019
      CC_Statements__c [] statementsList = [SELECT id, name,Statement_Key__c,CC_Balance__c,CC_Invoice__c, CC_Period__c, CC_Due_Date__c,CC_Dispute_Code__c ,
                                                   CC_Invoice_Date__c, CC_PO_Number__c,CC_Installment_Number__c
                                              FROM CC_Statements__c 
                                             WHERE CC_Due_Date__c != null  and CC_Entity__c=:aid  
                                          ORDER BY Id];
      return statementsList;
    }
    
    @RemoteAction
    global static List<CC_Invoice2__c> getSelectedInvoiceData(string invKeys){
      List<CC_Invoice2__c > invoiceList =new List<CC_Invoice2__c >();
      List<String> invKeyList =new List<String>();
      set<String> invKeySet =new set<String>();
      invKeyList = invKeys.split(',');
      try{
      for(String s : invKeyList){
          if(s.contains('[')){s=s.replace('[','');}
          if(s.contains(']')){s=s.replace(']','');}
          s=s.replaceAll( '\\s+','');//REMOVE IF THERE ARE ANY BLANK SPACES
          invKeySet.add(s);
      }
      for(CC_Invoice2__c inv : [SELECT id, name,CC_Invoice_Number__c,Customer_Number__c,CC_SHIP_VIA__c,CC_Order_Number__c,CC_Salesman_Num__c,CC_SALESORDER_NO__c,CC_Terms__c, CC_Amount_Due__c,CC_PO_Number__c,CC_sequence__c,
                                CC_Item_Number__c, CC_Invoice_Date__c, CC_Net_Due_Date__c, CC_Item_Description__c,CC_Line_Net_Sale_Amt__c, CC_Terms_Discount__c,
                                CC_Customer_Number__c, CC_Customer_Number__r.CC_Customer_Number__c, CC_Net_Sale_Amt__c,CC_Trade_Discount__c,CC_MISCELLANEOUS_CHARGES__c,CC_Taxes__c,
                                CC_Unit_Measure__c, CC_Qty_Shipped__c, CC_Qty_Back_Ord__c, CC_Unit_Price__c, CC_Amount__c,CC_Sold_To__c,CC_Sold_To_Address_1__c,
                                CC_Sold_To_Address_2__c,CC_Sold_To_Address_3__c,CC_Sold_To_Address_4__c,CC_Sold_To_Address_5__c,CC_Ship_To_Address_1__c,CC_Ship_To_Address_2__c,
                                CC_Ship_To_Address_3__c,CC_Ship_To_Address_4__c,CC_Ship_To_Address_5__c,CC_Ship_To__c
                                from CC_Invoice2__c 
                                where Name =: invKeySet order By CC_sequence__c ASC]){
                                invoiceList.add(inv);
                                
       }
       }
       catch(Exception e){}
       return invoiceList;
    }
    
    @RemoteAction
    global static map<String,List<String>> sendInvAndGetCase(string aid,string invAmtDisText){
      String CustomerFinancingRep;
      List<CC_Invoice2__c> invoiceList = new List<CC_Invoice2__c>();
      List<CC_Statements__c> stmtList = new List<CC_Statements__c>();
      List<String> invKeyList =new List<String>();
      List<Case> caseList =new List<Case>();
      Set<Id> caseIdSet =new Set<Id>();
      Set<String> invKeySet =new set<String>();
      Set<String> caseNumbersSet=new Set<String>();
      Map<String,CC_Invoice2__c> invKeyMap =  new Map<String,CC_Invoice2__c>();
      Map<String,String> appendedAmoutMap =  new Map<String,String>();
      Map<String,String> disputeCodeMap =  new Map<String,String>();
      Map<String,String> appendedDisputeCommentMap =  new Map<String,String>();
      Map<string,List<string>> createdCaseNumberMap= new Map<string,List<string>>();
      system.debug('invAmtDisText****** '+invAmtDisText);
      invKeyList = invAmtDisText.split(';');
      system.debug('invList****** '+invKeyList);
      for(String s : invKeyList ){
          String trimInvKey = s.split(':')[0];
          if(s.split(':')[1] != ''){String trimAmt = s.split(':')[1];appendedAmoutMap.put(trimInvKey,trimAmt);}
          system.debug('***list out of bounds**'+s.split(':')[2]);
          if(s.split(':')[2] != 'NA'){String trimDC = s.split(':')[2];disputeCodeMap.put(trimInvKey,trimDC);}
          if(s.split(':')[3] != 'NA'){String trimDpCmt = s.split(':')[3];appendedDisputeCommentMap.put(trimInvKey,trimDpCmt);}
          invKeySet.add(trimInvKey);
      }
      
          system.debug('disputeCodeMap****** '+disputeCodeMap);
          system.debug('appendedAmoutMap:**** '+appendedAmoutMap);
          system.debug('appendedDisputeCommentMap:**** '+appendedDisputeCommentMap);
      for(CC_My_Team__c mt : [Select Name,User__c,Displayed_Email__c, Id, Account__c 
                             From CC_My_Team__c 
                             where Account__c=:aid and Displayed_My_Team_Role__c = 'Customer Financing Representative' limit 1]){
                             if(mt.User__c !=null){CustomerFinancingRep = mt.User__c;}
                             else{CustomerFinancingRep = PavilionSettings__c.getInstance('Statements Queue').Value__c;}
      }
      system.debug('CustomerFinancingRep**'+CustomerFinancingRep);
      for(CC_Invoice2__c inv : [select id, name,CC_Invoice_Number__c, CC_Amount_Due__c,CC_PO_Number__c,CC_sequence__c 
                                from CC_Invoice2__c where Name IN : invKeySet and CC_sequence__c=1]){
        Case caseobj =new Case();
        caseobj.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName= 'Club_Car_Credit_Center_Dispute' AND SobjectType ='Case' LIMIT 1].Id;
        caseobj.CC_Invoice2__c = inv.id;
        if(appendedDisputeCommentMap.containsKey(inv.Name) && appendedDisputeCommentMap.get(inv.Name) != ''){caseobj.Description = appendedDisputeCommentMap.get(inv.Name);}
        if(CustomerFinancingRep != '')                         {caseobj.OwnerId=CustomerFinancingRep;}
        if(appendedAmoutMap.containsKey(inv.Name) && appendedAmoutMap.get(inv.Name) != '')         {caseobj.Amount__c=Decimal.valueof(appendedAmoutMap.get(inv.Name));}
        if(disputeCodeMap.containsKey(inv.Name) && disputeCodeMap.get(inv.Name) != '') {
            caseobj.Subject=disputeCodeMap.get(inv.Name);
            //caseobj.CC_Root_Cause__c=caseobj.Subject;  11dec2018  - Ben Lorenz - Commented out until statement dispute PL normalized to match case root cause PL values
        }
        caseobj.Reason='Credit Center Dispute';
        caseobj.Sub_Reason__c='Credit Center Dispute';
        caseobj.Origin = 'Community';
        caseobj.ContactId = [SELECT ContactId from User where id =:  UserInfo.getUserId()].ContactId;
        caseList.add(caseobj);
        invKeyMap.put(inv.Name,inv);
      }
      try{
        if(caseList.size()>0){
        insert caseList;
      }}
      catch(Exception e){}
      try{
      for(CC_Statements__c stmt : [Select id, Statement_Key__c ,name,CC_Balance__c,CC_Invoice__c, CC_Period__c, CC_Due_Date__c,CC_Dispute_Code__c ,CC_Invoice_Date__c, CC_PO_Number__c 
                              from CC_Statements__c 
                              where Statement_Key__c IN : invKeyMap.keyset()]){
                                      
                 if(stmt.CC_Dispute_Code__c == null && disputeCodeMap.get(stmt.Statement_Key__c) != '') {
                     stmt.CC_Dispute_Code__c = disputeCodeMap.get(stmt.Statement_Key__c);
                     stmtList.add(stmt);
                 }
                              
      }
      if(stmtList.size()>0){
          update stmtList;
      } 
      for(Case cs : caseList){
               caseIdSet.add(cs.Id);
      }
      for(Case insertCase : [Select id,CaseNumber from Case where id in : caseIdSet]){
                 caseNumbersSet.add(insertCase.CaseNumber);
                 system.debug('******casenumber****'+insertCase.CaseNumber);
      }
      }
      catch(Exception e){}
      List<String> CaseNumbersList =new List<String>(caseNumbersSet);
      createdCaseNumberMap.put('1',CaseNumbersList);
      return createdCaseNumberMap;    
    }
    
    global void controllerFunction(){
      for(CC_My_Team__c customerfinrepmt : [Select Id, Displayed_Name__c, Account__c, User__c, Displayed_Department__c, Displayed_My_Team_Role__c 
                                            from CC_My_Team__c 
                                            Where Account__c =: AcctId AND Displayed_My_Team_Role__c = 'Customer Financing Representative' limit 1]){
        
            system.debug('**custfinrep**'+customerfinrepmt.Displayed_Name__c);
            if(customerfinrepmt.Displayed_Name__c!=null)
            CustomerFinRep = customerfinrepmt.Displayed_Name__c;
        }
            Account acc = [Select Id,Name,CC_Estimated_Credit_Limit__c,Address_1__c,Address_2__c,Address_3__c,CC_Customer_Number__c,CC_Country__c,CC_City__c,CC_State__c,CC_Zip__c,CC_Open_A_R_Balance__c,CC_Open_Orders__c,CC_Approved_Open_Orders__c ,Club_Car_Floor_Plan__c from Account where Id =:AcctId];
            CreditLine=acc.CC_Estimated_Credit_Limit__c;
            CreditUsed=acc.CC_Open_A_R_Balance__c;
            PendingCharges=acc.CC_Approved_Open_Orders__c;
            AvailableCredit=CreditLine-(CreditUsed+PendingCharges);
            approvedOpenOrders = acc.CC_Approved_Open_Orders__c ;
            pendingApprovalOpenOrders=acc.CC_Open_Orders__c ;
            floorPlansOpenOrders =acc.Club_Car_Floor_Plan__c;
            Double balance=0;
            
            for(CC_Statements__c cs:[SELECT CC_Balance__c FROM CC_Statements__c WHERE CC_Period__c = 'Future' and CC_Entity__c=:AcctId]){
                balance=balance+cs.CC_Balance__c;
            }
            
            FutureDue=balance;
            CurrentDue=CreditUsed-FutureDue;
            
            
            CustomerName=acc.Name;
            
            customerAddress='';
            customerAddress3='';
            customerAddress4='';
            
            Address1 = acc.Address_1__c;
            CustomerNumber=acc.CC_Customer_Number__c;
            if(acc.Address_2__c!=null)
                {customerAddress+=acc.Address_2__c+' ';}
            if(acc.Address_3__c!=null)
                {customerAddress3+=acc.Address_3__c+' ';}
            if(acc.CC_City__c!=null)
                {customerAddress4+=acc.CC_City__c+' ';}
            if(acc.CC_State__c!=null)
                {customerAddress4+=acc.CC_State__c+' ';}
            if(acc.CC_Country__c!=null)
                {customerAddress4+=acc.CC_Country__c+' ';}
            if(acc.CC_Zip__c!=null)
                {customerAddress4+=acc.CC_Zip__c;}
            
            // Logic for Remitt Address Customer Number Number ends with 21 = CA and 20 = USA
            if(CustomerNumber!=Null && CustomerNumber!=''){    
                    if(CustomerNumber.right(2) == '21' ){
                        remitAddress = null!=PavilionSettings__c.getInstance('RemitAddress_CA')?PavilionSettings__c.getInstance('RemitAddress_CA').Value__c:'';
                    }
                    else{
                        remitAddress = null!=PavilionSettings__c.getInstance('RemitAddress_US')?PavilionSettings__c.getInstance('RemitAddress_US').Value__c:'';
                    }
            }
    }
    
    
}