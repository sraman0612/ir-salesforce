public with sharing class CC_Send_Service_Contract_Signature_Ctrl {

   	@TestVisible private ApexPages.StandardController stdctrl;
    @TestVisible private Contract contract;
    
    public CC_Send_Service_Contract_Signature_Ctrl(ApexPages.StandardController stdctrl){
        
        this.stdctrl = stdctrl;
        
        if (!Test.isRunningTest()){
            stdctrl.addFields(new List<String>{'ContractNumber','CC_Contract_Status__c'});
        }   
        
        this.contract = (Contract)stdctrl.getRecord();
    }
    
    public PageReference sendForSignature(){
    	
    	// Check validations before sending for signature
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
        
        if (String.isBlank(templates.CC_Service_Contract_Template_ID__c)){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Adobe template ID not specific in custom settings.'));
    		return null;
        }
    	
    	if (contract.CC_Contract_Status__c == 'Signed'){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Contract is already signed.'));
    		return null;
    	}    	
    	else if (contract.CC_Contract_Status__c != 'Approved'){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Contract must be approved before it can be sent for signature.'));
    		return null;
    	}
		
        // Build the PDF form and attach it to the contract
        PageReference agreementPDF = Page.CC_Service_Contract_PDF;
        agreementPDF.getParameters().put('id', stdctrl.getId());      
        Blob pdfContent = Test.isRunningTest() ? Blob.valueOf('Test PDF') : agreementPDF.getContent();
        
        Attachment attach = new Attachment();
        attach.ParentId = stdctrl.getId();
        attach.Body = pdfContent;
        attach.Name = 'Service Contract ' + contract.ContractNumber + '_' + System.now() + '.pdf';
        insert attach;
        
        // Send to Adobe sign      
        PageReference adobeSign = Page.echosign_dev1__AgreementTemplateProcess;
        adobeSign.getParameters().put('masterid', stdctrl.getId());
        adobeSign.getParameters().put('templateId', templates.CC_Service_Contract_Template_ID__c);       
    	return adobeSign;                    
    }
}