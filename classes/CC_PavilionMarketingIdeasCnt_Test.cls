@istest 
Public class CC_PavilionMarketingIdeasCnt_Test
{
        static testmethod void ShowMarketingIdeas()
        {
            CC_PavilionTemplateController controller;
            List<ContentVersion> contentList=new List<ContentVersion>();  
            RecordType ContentRT = [select Id FROM RecordType WHERE Name = 'Marketing Ideas'];           
                //Creating Test Data 
                ContentVersion testContentInsert = new ContentVersion(); 
                testContentInsert.ContentURL='http://www.clubcar.com/'; 
                testContentInsert.Title = 'clubcar.com'; 
                testContentInsert.RecordTypeId = ContentRT.Id;            
                insert testContentInsert;
             
            System.assertEquals(testContentInsert.Title, 'clubcar.com');
            contentList.add(testContentInsert);
            
             Test.StartTest();
                CC_PavilionMarketingIdeasCnt pboIdeas = new CC_PavilionMarketingIdeasCnt(controller);
                pboIdeas.Showmarketingideas();
                System.assertEquals(1, contentList.size());
             Test.StopTest(); 
        }
    
}