@isTest
public class CC_BatchAccountTeamLastUpdated_Test{

    static testMethod void CC_BatchAccountTeamLastUpdatedMethod(){
        
     //creating test Data here
        TestDataUtility testData = new TestDataUtility();
        
         //creating state country custom setting data
         TestDataUtility.createStateCountries();
         
         //creating sales rep here
          CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;    
        
     Account acc= new Account();
     acc.name = 'Test Account';
     acc.RecordtypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Club Car: Channel Partner').getRecordTypeId();
          acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
     insert acc;
        
         Account acc1= new Account();
     acc1.name = 'Test Account';
     acc1.RecordtypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Club Car: Channel Partner').getRecordTypeId();
          acc1.CC_Sales_Rep__c = salesRepacc.Id;
        acc1.CC_Price_List__c='PCOPI';
        acc1.BillingCountry = 'USA';
        acc1.BillingCity = 'Augusta';
        acc1.BillingState = 'GA';
        acc1.BillingStreet = '2044 Forward Augusta Dr';
        acc1.BillingPostalCode = '566';
        acc1.CC_Shipping_Billing_Address_Same__c = true;
     insert acc1;
     User u =new User(ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,LastName = 'last',Email = 'puser000@amamama.com',
                      Username = 'xyz231@amamama.com' + System.currentTimeMillis(), CompanyName = 'TEST',Title = 'title',Alias = 'alias',
                      TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US'
     );
        
     partner pt = new partner();
        pt.AccountFromId = acc.id;
        pt.AccountToId   = acc1.id;
        insert pt;
        
     String teamMemberRole = 'Account Manager';
     AccountTeamMember atm = new AccountTeamMember();
     atm.AccountId = acc.id;
     atm.TeamMemberRole = teamMemberRole;
     atm.UserId= u.id;
     List<AccountTeamMember> atmList = new List<AccountTeamMember>();
     atmList.add(atm);
           Test.startTest();
        		 CC_BatchAccountTeamLastUpdated sh1 = new CC_BatchAccountTeamLastUpdated();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, sh1); 
           Test.stopTest();
    }
}