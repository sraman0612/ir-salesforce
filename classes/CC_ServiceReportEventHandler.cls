public class CC_ServiceReportEventHandler {
  public static void updateWorkOrder(List<ServiceReport> newLst, Map<Id, ServiceReport> oldMap){
    Set<Id> parentIds = new Set<Id>();
    Set<Id> workOrderIds = new Set<Id>();
    String str1 = 's1';
    String str2 = 's2';
    String str3 = 's3';
    String str4 = 's4';
    String str5 = 's5';
    String str6 = 's6';
    for(ServiceReport sr : newLst){
      if (null != sr.ParentId && sr.IsSigned && (null==oldMap || !oldMap.get(sr.Id).IsSigned)){parentIds.add(sr.ParentId);}
    }
    if(!parentIds.isEmpty()){
      for(ServiceAppointment sa : [SELECT ParentRecordId 
                                     FROM ServiceAppointment 
                                    WHERE ParentRecordType='WorkOrder' and
                                          Work_Order__c != null and
                                          Work_Order__r.isServiceReportSigned__c=FALSE and
                                          Id IN :parentIds]){
        workOrderIds.add(sa.ParentRecordId);
      }
      if(!workOrderIds.isEmpty()){
        WorkOrder [] workOrders2Update = [SELECT Id, isServiceReportSigned__c FROM WorkOrder WHERE Id IN :workOrderIds];
        for(WorkOrder wo : workOrders2Update){wo.isServiceReportSigned__c=TRUE;}
        update workOrders2Update;
      }
    }
  }
}