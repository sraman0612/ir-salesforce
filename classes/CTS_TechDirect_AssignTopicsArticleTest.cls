@isTest
public class CTS_TechDirect_AssignTopicsArticleTest {
    @isTest(seeAllData=true)
    public static void test_AssignTopicsToArticles() {
        
        Test.startTest();
        
        User u3 = [SELECT Id FROM User WHERE UserRole.Name LIKE 'CTS TechDirect%' and profile.name like '%Admin%' and isactive = true limit 1];
        System.runAs(u3) 
        {
            Knowledge__kav knowledgeObj = new Knowledge__kav();
            knowledgeObj.RecordTypeId = '0120a0000011AllAAE';
            knowledgeObj.ValidationStatus = 'Private';
            knowledgeObj.Title = 'Test Article';
            knowledgeObj.UrlName = 'Test' + String.valueOf(System.currentTimeMillis());
            knowledgeObj.Summary = 'Test Summary';
            knowledgeObj.CTS_TechDirect_Article_Type__c = 'Tech Flash';
            knowledgeObj.CTS_TechDirect_Question__c = 'Test Question';
            knowledgeObj.CTS_TechDirect_Answer__c = 'Test Question';
            knowledgeObj.CTS_TechDirect_Access_Level__c = 'Standard Products';
            knowledgeObj.IsVisibleInPkb=true;
            knowledgeObj.IsVisibleInCsp=true;
            knowledgeObj.IsVisibleInPrm =true;
            knowledgeObj.language='en_US';
            knowledgeObj.CTS_TechDirect_Author_Reviewer__c = u3.Id;
            insert knowledgeObj;
            
            knowledgeObj.CTS_TechDirect_Article_Type__c = 'Manuals';
            update knowledgeObj;
            
            knowledgeObj.CTS_TechDirect_Article_Type__c = 'FSB';
            update knowledgeObj;
            
            knowledgeObj.CTS_TechDirect_Article_Type__c = 'Tech Tube';
            update knowledgeObj;
            
            
            /*Network authCommunity = [SELECT Id, Name 
                                     FROM Network 
                                     WHERE UrlPathPrefix =: Label.CTS_TechDirect_Community_Path_Name Limit 1];
            
            Topic topicVar = [Select Id, Name From Topic 
                              Where Name = 'Latest Tech Tubes' AND NetworkId =: authCommunity.Id Limit 1];
            
            TopicAssignment topicAssignmnt = [SELECT Id, EntityId, TopicId 
                                              FROM TopicAssignment 
                                              Where EntityId =: knowledgeObj.id AND NetworkId =: authCommunity.id Limit 1];
            
            System.assertEquals(topicVar.id, topicAssignmnt.TopicId);*/
            
            Test.stopTest();
         }   
        
    }
    
}