public class typeAttributes {
    @AuraEnabled
    public fieldLabel label {get;set;}
    @AuraEnabled
    public String target {get;set;}
    
    public typeAttributes(fieldLabel label, String target){
        this.label = label;
        this.target = '_parent';
    }
}