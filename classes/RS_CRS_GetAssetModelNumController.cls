/****************************************************************
Name:      RS_CRS_GetAssetModelNumController
Purpose:   Get Asset Model# from comfort site
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Ashish Takke	02/09/2018		INITIAL DEVELOPMENT
*****************************************************************/
public with sharing class RS_CRS_GetAssetModelNumController {
    
    /*****************************************************************************
	Purpose            :  Method used to get Asset Model# from comfort site
	Parameters         :  Asset Id
	Returns            :  Boolean
	Throws [Exceptions]:                                                          
	******************************************************************************/
    @AuraEnabled
    public static Boolean getAssetModelNum(Id assetId){
        Boolean isRespReceived = false;
        try{
            //Getting Asset Record Type Id through Schema.Describe Class
            Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
            
            //get Asset Serial# based on asset record id
            Asset assetRec = [Select Id, Name From Asset Where Id =: assetId AND RecordTypeId =: assetRecId];
            
            //Get comfort site URL to fetch Asset model#
            RS_CRS_WebService_Data__c webServiceData = RS_CRS_WebService_Data__c.getInstance('RS_CRS_WebService');
            String comfortSiteURL = webServiceData.Get_Asset_Model_Num_URL__c;
            
            //prepare HTTP request
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(comfortSiteURL + '?serial=' + assetRec.Name);
            request.setMethod('GET');
            HttpResponse response = http.send(request);
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                // Deserialize the JSON string into collections of primitive data types.
                List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
                
                if(results.size() > 0){
                    // Cast the values in the List as a Map
                    Map<String,Object> resultMap = (Map<String,Object>) results.get(0);
                    
                    //update Model# on Asset
                    assetRec.Model_Name__c = (String)resultMap.get('ModelNumber');
                    update assetRec;
                    
                    isRespReceived = true;
                }                
            }
            return isRespReceived;
        } catch(Exception ex){
            System.debug('Error occurred while getting Model# in \'RS_CRS_GetAssetModelNumController\' \''+ex.getMessage() + '\' at Line# ' + ex.getLineNumber());
            return false;
        }
        
    }
}