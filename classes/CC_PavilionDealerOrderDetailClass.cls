public class CC_PavilionDealerOrderDetailClass {
  
  public boolean ShowButtonDiv { get; set; }
  public boolean showPricing {get;set;}
  public String orderId { get; set; }
  Public String customerRep { get; set; }
  public List<OrderItemAndFeatureWrapper> wrapperList  {get; set;}
  public Boolean lessThan21Days{get;set;} 
  Public CC_Order__c o {get;set;}
  
  public CC_PavilionDealerOrderDetailClass(CC_PavilionTemplateController controller) {
    Map<Id, Set<String>> featureMap = new Map<Id, Set<String>>();
    wrapperList=new List<OrderItemAndFeatureWrapper>();
    CC_My_Team__c [] myTeamLst = new List<CC_My_Team__c>();
    Id accid = controller.acctid;
    if(accId!=null){
      myTeamLst = [SELECT Displayed_Name__c,User__c, Displayed_Email__c, Custom_Email__c, Contact__r.Email 
                     FROM CC_My_Team__c 
                    WHERE Displayed_My_Team_Role__c = 'Customer Account Specialist' AND Account__c = :accId limit 1];
    }
    customerRep=myTeamLst.size()==1?myTeamLst[0].Displayed_Email__c:'Clubcar@clubcar.com';
    
    if (ApexPages.currentPage().getParameters().get('id') != null) {
      orderId = ApexPages.currentPage().getParameters().get('id');
      Schema.DescribeSObjectResult orderShipment = CC_Order_Shipment__c.sObjectType.getDescribe();
      String orderShipmentKeyPrefix = orderShipment.getKeyPrefix();
     // system.debug('### the page id is ' + pageId);
     /* if(pageId.startsWith(orderShipmentKeyPrefix)){ //it is an order shipment Id
        orderId = [SELECT Order__c FROM CC_Order_Shipment__c WHERE Id = :pageId LIMIT 1].Order__c;
      } else { // it is a car item Id
        orderId = [SELECT Order_Item__r.Order__c FROM CC_Car_Item__c WHERE Id = :pageId LIMIT 1].Order_Item__r.Order__c;
      }*/
      //get order
      system.debug('### the order id is ' + orderId);
      o = [SELECT Order_Number__c,CC_Sales_Person_Number__r.CC_Sales_Person_Number__c,CC_Sales_Person_Number__r.Name,PO__c,CC_Order_Number_Reference__c,
                  CC_Original_Request_Date__c,CC_Status__c,CC_Request_Date__c,CC_Carrier_ID__c,CC_Account__r.Name,CC_Terms__c,CC_Acknowledge_By__c,CC_Address_line_1__c,
                  CC_Account__c,CC_Acknowledge_Date__c,CC_Changes__c,CC_Fax_number__c,Total_Invoice_Amount__c,CC_Telephone_number__c,CC_Addressee_name__c,
                  CC_Address_line_2__c,CC_Country__c,CC_Address_line_3__c,CC_City__c,CC_States__c,CC_Postal_code__c,CC_Contact_name__c,CC_Accept_Status__c,
                  Sold_To_Addressee__c,Sold_To_Address_Line_1__c,Sold_To_Address_Line_2__c,ASAP_Flag__c,Sold_To_Address_Line_3__c,Sold_To_Address_Line_4__c,
                  Sold_To_Address_Line_5__c,Sold_To_City__c,Sold_To_State__c,Sold_To_Postal_Code__c,Sold_To_Country__c,Bill_To_Phone__c,Bill_To_Addressee__c,
                  Bill_To_Address_Line_1__c,Bill_To_Address_Line_2__c,Bill_To_Address_Line_3__c,Bill_To_City__c,Bill_To_State__c,Bill_To_Country__c,Bill_To_Fax__c,
                  Bill_To_Postal_Code__c
            FROM CC_Order__c
           WHERE Id = :orderId 
           LIMIT 1];
      ShowButtonDiv=o.CC_Accept_Status__c=='N'?True:False;
      showPricing=accId==o.CC_Account__c||accId==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
      lessThan21Days=o.CC_Original_Request_Date__c <= Date.today().addDays(21)?TRUE:FALSE;
      //get features
      for (CC_Car_Feature__c cf : [SELECT CC_Car_Item__r.Order_Item__c,Order_Item__c,Product__r.Name FROM CC_Car_Feature__c WHERE Club_Car_Order__c = :orderId]){
        string oItem=null==cf.Order_Item__c?cf.CC_Car_Item__r.Order_Item__c:cf.Order_Item__c;
        if(featureMap.containsKey(oItem)) {
          featureMap.get(oItem).add(cf.Product__r.Name);
        } else {
          featureMap.put(oItem,new Set<String>{cf.Product__r.Name});
        }
      }
      //get order items 
      for(CC_Order_Item__c oi: [SELECT Id,Product__r.ProductCode, Product__r.Name,Product__r.Description,CC_Product_Code__c,CC_Quantity__c,CC_UnitPrice__c,CC_TotalPrice__c,
                                       (SELECT Product__r.Name,Product__r.Description from Car_Features__r)
                                  FROM CC_Order_Item__c 
                                 WHERE Order__c=: orderId]){
        OrderItemAndFeatureWrapper ow = new OrderItemAndFeatureWrapper(oi);
        if(featureMap.containskey(oi.id))
          ow.features = new List<String>(featureMap.get(oi.id));
        wrapperList.add(ow);
      }
    }
  }

  @RemoteAction
  public static PageReference changeStatus(String userAccountId, String OrderId, String Comment, String statusChange) {
    User u=[Select Name,AccountId from User where Id=:userinfo.getUserId()];
    CC_Order__c ordDetail3 = [Select id, CC_Accept_Status__c,Order_Number__c,CC_Acknowledge_By__c,CC_Acknowledge_Date__c,CC_Changes__c,Sold_To_Addressee__c from CC_Order__c where Id = : OrderId];
    if(statusChange=='Y'){ordDetail3.CC_Accept_Status__c = 'Y';}
    ordDetail3.CC_Acknowledge_By__c =u.Name;
    ordDetail3.CC_Acknowledge_Date__c =system.today();
    ordDetail3.CC_Changes__c='Accepted';
    update ordDetail3;
    if (statusChange == 'C') {
      Case cse = new Case();
      cse.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName= 'Club_Car_Case' AND SobjectType ='Case' LIMIT 1].Id;
      cse.CC_Order__c = OrderId;
      cse.Subject='Change Request for ' + ordDetail3.Sold_To_Addressee__c + ' for Order# '+ordDetail3.Order_Number__c;
      cse.Description = Comment;
      cse.Reason='Cars';
      cse.Sub_Reason__c='Order changes';
      cse.Origin = 'Community';
      cse.ContactId = [SELECT ContactId from User where id =:  UserInfo.getUserId()].ContactId;
      cse.ownerId = PavilionSettings__c.getInstance('CASQ').Value__c;
      insert cse;
    }
    PageReference pref = new PageReference('/CC_PavilionDealerInvoiceHistory');
    return pref;
  }
  
  public class OrderItemAndFeatureWrapper {
    public CC_Order_Item__c orderItem{get;set;}
    Public List<string> features {get;set;}
    public OrderItemAndFeatureWrapper (CC_Order_Item__c orditm) {this.orderItem = orditm;}
  }
}