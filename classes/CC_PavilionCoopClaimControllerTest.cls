/*
*
* $Revision: 1.0 $
* $Date: $
* $Author: $
*/
@isTest
public class CC_PavilionCoopClaimControllerTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    static testMethod void UnitTest_CoopClaimController()
    { 
        // Creation Of Test Data
        Account newAccount = TestUtilityClass.createAccount();
        insert newAccount;
        System.assertEquals('Sample Account',newAccount.Name);
        System.assertNotEquals(null, newAccount.Id);
        Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
        insert sampleContact;
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',newAccount.id);
        Insert sampleContract;
        System.assertEquals(newAccount.Id,sampleContract.AccountId);
        System.assertNotEquals(null,sampleContract.Id);
        CC_Co_Op_Account_Summary__c newCoopAccountSummary = TestUtilityClass.createCoopAccountSummary(sampleContract.Id);
        insert newCoopAccountSummary;
        System.assertEquals(sampleContract.id,newCoopAccountSummary.Contract__c);
        System.assertNotEquals(null,newCoopAccountSummary.Id);
        CC_Order__c newOrder = TestUtilityClass.createOrder(newAccount.id);
        insert newOrder;
        System.assertEquals(newOrder.CC_Account__c,newAccount.Id);
        System.assertNotEquals(null,newOrder.Id);
        CC_Coop_Claims__c sampleCoopClaims = TestUtilityClass.createCoopClaimWithAccountSummary(newCoopAccountSummary.Id);
        sampleCoopClaims.CC_Status__c='Incomplete';
        insert sampleCoopClaims;
      
        System.assertEquals(sampleCoopClaims.Co_Op_Account_Summary__c,newCoopAccountSummary.Id);
        System.assertNotEquals(null,sampleCoopClaims.Id);
        //String userNavigationProfile = newUser.CC_Pavilion_Navigation_Profile__c;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
        insert newPavilionNavigationProfile;
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
        insert newUser;
        Attachment at=new Attachment();
        at.name='Test.pdf';
        at.body=blob.valueof('Test.pdf');
        at.parentId=sampleCoopClaims.id;
        insert at;
        CC_PavilionCoopClaimController.invoiceattachment.Name='Test.pdf';
        //Added 10 attachments by @Priyanka Baviskar for ticket #8287533: USERS CAN'T SELECT MORE THAN ONE FILE TO UPLOAD TO CO-OP CLAIM at 4/25/2019
        CC_PavilionCoopClaimController.tearsheetattachment1.Name='sample1.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment2.Name='sample2.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment3.Name='sample3.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment4.Name='sample4.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment5.Name='sample5.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment6.Name='sample6.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment7.Name='sample7.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment8.Name='sample8.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment9.Name='sample9.pdf';
        CC_PavilionCoopClaimController.tearsheetattachment10.Name='sample10.pdf';
        CC_PavilionTemplateController controller;
       System.runAs(newUser){
        controller=new CC_PavilionTemplateController();
       }
        CC_PavilionCoopClaimController newCoopClaimController = new CC_PavilionCoopClaimController();
        
        CC_PavilionCoopClaimController coopClaimController = new CC_PavilionCoopClaimController(controller);
        
        CC_PavilionCoopClaimController.refreshTableBasedOnYear(sampleContract.CC_Sub_Type__c, newAccount.Id);
        System.assertNotEquals(null,CC_PavilionCoopClaimController.refreshTableBasedOnYear(sampleContract.CC_Sub_Type__c, newAccount.Id));
        CC_PavilionCoopClaimController.getAggrementTypes(newAccount.id);
        
        newCoopClaimController.ctID =newCoopAccountSummary.id;
        CC_PavilionCoopClaimController.submitCoopClaimData1(sampleCoopClaims);
        CC_PavilionCoopClaimController.submitCoopClaimData('new vender',String.valueOf(system.today().format()), '0', '123', 'Yes', 'Yes', 'Yes', '123', 'weqwzxv xc', newCoopAccountSummary.Id, 'AttachmentBody'); 
        newCoopClaimController.invoicedate='22/04/1987';
        newCoopClaimController.venderName='Test Venodr';
        newCoopClaimController.numCompBrsnds='2';
        newCoopClaimController.invoiceAmmount='100';
        newCoopClaimController.copyOfInvoice='Yes';
        newCoopClaimController.tearsheetOfAdd='Yes';
        newCoopClaimController.ccLogoInAd='Yes';
        newCoopClaimController.newClaimAmount='1000';
        newCoopClaimController.comments='Test Comments';
        newCoopClaimController.submitPavCoopClaimData();
        newCoopClaimController.invoicedate=Null;
        newCoopClaimController.submitPavCoopClaimData();
        
       
        
    }
}