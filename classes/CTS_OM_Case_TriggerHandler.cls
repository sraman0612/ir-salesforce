public with sharing class CTS_OM_Case_TriggerHandler {
    
    private static Email_Message_Setting__mdt settings;
    private static Set<Id> ctsOMCaseRecordTypeIds = new Set<Id>();
    private static Set<Id> ctsOMContactRecordTypeIds = new Set<ID>();   
    private static List<String> OM_Emailcase_RecordType_Name =  new List<String>();
    
    private static list<String> CTS_OM_Contact_Case_Record_Type_Name =  new List<String>();
    // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -START
    public static Id ETO_SUPPORT_CASE_RT_ID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
    public static Id STANDARD_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
    public static Id NEW_STANDARD_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
    // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -END
    
    static{
        //settings = Email_Message_Settings__c.getOrgDefaults();
        //added by @Capgemini
        //added by @Capgemini on 23 December 2021 
        // settings = Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
        settings = [SELECT DeveloperName,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,CTS_OM_Email_Case_Record_Type_IDs_2__c,CTS_OM_Email_Case_Record_Type_IDs_3__c,CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case'];
        // String CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
        
        //end
        system.debug('settings: ' + settings);
        
        OM_Emailcase_RecordType_Name = String.isNotBlank(settings.CTS_OM_Email_Case_Record_Type_IDs__c) ? new List<String>((List<String>)settings.CTS_OM_Email_Case_Record_Type_IDs__c.split(',')) : new List<String>();
        //system.debug('****Email message OM_case_RecordType_Name****'+ OM_Emailcase_RecordType_Name);
        
        CTS_OM_Contact_Case_Record_Type_Name = String.isNotBlank(settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c) ? new List<String>((List<String>)settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c.split(',')) : new List<String>(); 
        system.debug('****Contact OM_case_RecordType_Name****'+ CTS_OM_Contact_Case_Record_Type_Name);
        
        for(String rt : OM_Emailcase_RecordType_Name){
            if( rt != null){
                ctsOMCaseRecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rt).getRecordTypeId());
            }
        }
        
        for (String con_rt : CTS_OM_Contact_Case_Record_Type_Name){
            system.debug('***con_rt***'+ con_rt);
            ctsOMContactRecordTypeIds.add(Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(con_rt).getRecordTypeId());
        }
        
        
        //system.debug('***set of case record type***'+ ctsOMCaseRecordTypeIds);
        system.debug('***set of contact record type***'+ ctsOMContactRecordTypeIds);
        
        
        
        //ctsOMCaseRecordTypeIds = String.isNotBlank(CTS_OM_Email_Case_Record_Type_IDs) ? new Set<Id>((List<Id>)CTS_OM_Email_Case_Record_Type_IDs.split(',')) : new Set<Id>();
        //ctsOMContactRecordTypeIds = String.isNotBlank(settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c) ? new Set<Id>((List<Id>)settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c.split(',')) : new Set<Id>();   	
    }    
    
    public static void onBeforeInsert(Case[] cases){
        
        if (settings.CTS_OM_Case_Contact_Trigger_Enabled__c && !ctsOMCaseRecordTypeIds.isEmpty() && !ctsOMContactRecordTypeIds.isEmpty()){
            
            // Ensure that any contact assigned via E2C is of the applicable CTS OM contact record types
            Case_TriggerHandlerHelper.assignContacts(cases, ctsOMCaseRecordTypeIds, ctsOMContactRecordTypeIds, 'CTS_OM_Contact_Match_Status__c');
        }
    }
    // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -START
    Public Static Void validateBusiness(List<case> newCases){
        List< id> OpportunityId = new List< id> ();
        Map<id , Opportunity> mapOfOpportunity;
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
        
        for (case cs : newCases) {
            OpportunityId.add(cs.Related_Opportunity__c);
        }
        if(!OpportunityId.isEmpty()){
            mapOfOpportunity = new Map<Id, Opportunity>( [SELECT Id,Business__c,RecordTypeId FROM Opportunity Where id IN : OpportunityId]);
        }
        if(!mapOfOpportunity.isEmpty()){
            for (case cs : newCases) {
                System.debug('Case -->  '+ cs);
                //System.debug('Business__c from Case -->  '+ cs.Business__c);
                // System.debug('Business__c from related  Opportunity--> '+mapOfOpportunity.get(cs.Related_Opportunity__c).Business__c);
                if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){
                    if(cs.RecordTypeId == ETO_SUPPORT_CASE_RT_ID &&  cs.Business__c != null && mapOfOpportunity.get(cs.Related_Opportunity__c).Business__c != null  ) {
                        if (cs.Business__c!= mapOfOpportunity.get(cs.Related_Opportunity__c).Business__c && (STANDARD_OPP_RT_ID == mapOfOpportunity.get(cs.Related_Opportunity__c).RecordTypeId || NEW_STANDARD_OPP_RT_ID == mapOfOpportunity.get(cs.Related_Opportunity__c).RecordTypeId)){
                            cs.addError( System.Label.L_GD_Case_Business_Validation_Message);
                        } 
                    }
                }
            }
        }
    }
    // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -END
}