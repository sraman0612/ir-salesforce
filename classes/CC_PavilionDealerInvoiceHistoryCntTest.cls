@isTest
private class CC_PavilionDealerInvoiceHistoryCntTest {
    public static Account acc;
    public Static Contact con;
    public Static Pavilion_Navigation_Profile__c pnp;
    public Static User PortalUser;
    public Static CC_My_Team__c CustFinRep,FinRep;
    public Static CC_Invoice2__c SampleInv1,SampleInv2;
    public Static CC_Order__c Order1,Order2,Order3;
    public Static List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();
    public Static List<CC_Order__c> OrderList=new List<CC_Order__c>();
    
    static{
        
       List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
       psettingList = TestUtilityClass.createPavilionSettings();
                
       insert psettingList;
        
        //creating test data starts here//
        acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        System.assertEquals('Sample account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        
        con= TestUtilityClass.createContact(acc.id);
        insert con;
        System.assertNotEquals(null, con.Id);
        
        pnp = TestUtilityClass.createPavilionNavigationProfile('US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN');
        insert pnp;
        System.assertNotEquals(null, pnp);
        
        PortalUser= TestUtilityClass.createUserWithTiles('CC_PavilionCommunityUser',con.id);
        insert PortalUser;
        System.assertNotEquals(null, PortalUser.Id);
        
         //AccountTeamMember for Account share error
        
        AccountTeamMember atm = TestUtilityClass.createATM(acc.Id,PortalUser.Id);
        insert atm;
        
        /*AccountShare accShare = TestUtilityClass.createAccShare(acc.Id,PortalUser.Id);
        insert accShare; */
        
        
        CustFinRep =TestUtilityClass.createMyTeam('Customer Financing Representative',acc.id,'Customer Finance');
        insert CustFinRep;
        System.assertEquals('Customer Financing Representative', CustFinRep.My_Team_Role__c);
        System.assertNotEquals(null, CustFinRep.Id);
        
        
        Order1= TestUtilityClass.getOrderItemsForDealerInvoice(acc.Id,'122121',PortalUser.id,'cvcb','Dealer Invoicing');
        Order2= TestUtilityClass.getOrderItemsForDealerInvoice(acc.Id,'122123',PortalUser.id,'cvck','Dealer Invoicing');
        Order3= TestUtilityClass.getOrderItemsForDealerInvoice(acc.Id,'122125',PortalUser.id,'cvcl','Dealer Invoicing');
        OrderList.add(Order1);
        system.debug(Order1);
        OrderList.add(Order2);
        OrderList.add(Order3);
        system.debug(OrderList);
        //creating test data ends here//
        

        
    }
    private static testMethod void testController() {
            System.assertNotEquals(null, OrderList);
            insert OrderList;
            System.assertNotEquals(null, OrderList);
            SampleInv1= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,2,'12320','14593',2000,'14593',System.today(),'Sample');
            SampleInv2= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,2,'12320','74593',2000,'74593',System.today(),'Sample');
            INVLists.add(SampleInv1);
            INVLists.add(SampleInv2);
            System.assertNotEquals(null, INVLists);
            insert INVLists;
            System.assertNotEquals(null, INVLists);
            
         System.runAs(PortalUser)
        {
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            CC_PavilionDealerInvoiceHistoryCnt DealerInvController= new CC_PavilionDealerInvoiceHistoryCnt();
            CC_PavilionDealerInvoiceHistoryCnt DealerInvController1= new CC_PavilionDealerInvoiceHistoryCnt(controller);
            String status1='';
            String status ='Complete';
            CC_PavilionDealerInvoiceHistoryCnt.getOrderStatusData(status);
            CC_PavilionDealerInvoiceHistoryCnt.getOrderStatusData(status1);
        }
    }

    private static testMethod void testController1() {
         SampleInv1= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,1,'12345','14593',2000,'14593',System.today(),'Sample');
         SampleInv1.CC_Reference_Number__c='12345';
         SampleInv2= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,1,'12347','74593',2000,'74593',System.today(),'Sample');
         SampleInv1.CC_Reference_Number__c='12345';
         INVLists.add(SampleInv1);
         INVLists.add(SampleInv2);
         System.assertNotEquals(null, INVLists);
         insert INVLists;
         System.assertNotEquals(null, INVLists);
         System.runAs(PortalUser)
        {
            System.assertNotEquals(null, OrderList);
            insert OrderList;
            System.assertNotEquals(null, OrderList);
            
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            CC_PavilionDealerInvoiceHistoryCnt DealerInvController= new CC_PavilionDealerInvoiceHistoryCnt();
            CC_PavilionDealerInvoiceHistoryCnt DealerInvController1= new CC_PavilionDealerInvoiceHistoryCnt(controller);
            String status ='Complete';
            String status1='';

            CC_PavilionDealerInvoiceHistoryCnt.getOrderStatusData(status);
            CC_PavilionDealerInvoiceHistoryCnt.getOrderStatusData(status1);

        }

    }
    

}