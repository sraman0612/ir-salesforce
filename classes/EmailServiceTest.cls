@isTest
public with sharing class EmailServiceTest {

    @TestSetup
    private static void createData(){
        
        TestDataUtility dataUtil = new TestDataUtility();
        Lead l1 = dataUtil.createLead('New Global Lead', null);
        l1.LastName = 'TestLead1';
        insert l1;
        
        insert dataUtil.createIntegrationUser();
    }
    
    @isTest
    private static void testBuildSingleEmailMessage(){
        
        User testUser = [Select Id From User Where isActive = true LIMIT 1];
        
        Test.startTest();
        
    	Messaging.SingleEmailMessage[] messages = buildSingleEmailMessages(testUser.Id);
    	EmailService.sendSingleEmailMessages(messages);
    	Test.stopTest();
        
        // Validate messages built
        system.assertEquals(testUser.Id, messages[0].getTargetObjectId());
        system.assertEquals('Email Test 1', messages[0].getSubject());
        system.assertEquals('Test 1', messages[0].getPlainTextBody());
    }
      @isTest
    private static void testBuildSingleEmailMessage1(){
        
        User testUser = [Select Id From User Where isActive = true LIMIT 1];
        
        Test.startTest();
        
    	Messaging.SingleEmailMessage[] messages =null;
    	EmailService.sendSingleEmailMessages( messages);
    	Test.stopTest();
        
     
    }
    
    @isTest
    private static void testSendSingleEmailMessages(){
        
        User testUser = [Select Id From User Where isActive = true and Username = 'SampleUser123213@gmail.com' LIMIT 1];
        Messaging.SingleEmailMessage[] messages = buildSingleEmailMessages(testUser.Id);
        
        Test.startTest();
        
    	EmailService.sendSingleEmailMessages(messages);
        
        Test.stopTest();
        
        system.assertEquals(EmailService.emailDeliverabilityEnabled ? messages.size() : 0, EmailService.emailsSent);
    }    
    
    private static Messaging.SingleEmailMessage[] buildSingleEmailMessages(Id testUserId){
     
        Lead testLead = [Select Id From Lead Where LastName = 'TestLead1'];
        EmailTemplate testTemplate = [Select Id From EmailTemplate LIMIT 1];
        
        Messaging.SingleEmailMessage[] messages;   
        
        // Target object ID with plain text body
     	Messaging.SingleEmailMessage msg1 = EmailService.buildSingleEmailMessage(
            testUserId, 
            null, 
            null, 
            null,
            'Email Test 1',
            'Test 1', 
            null, 
            null,
            null
        );
        
        // To and CC addresses with html body
     	Messaging.SingleEmailMessage msg2 = EmailService.buildSingleEmailMessage(
            null, 
            new List<String>{'abc@123.com'}, 
            new List<String>{'xyz@123.com'}, 
            null,
            'Email Test 2',
            null, 
            '<html>Test 2</html>', 
            null,
            null
        );        
        
        // To addresses with email template and what Id
     	Messaging.SingleEmailMessage msg3 = EmailService.buildSingleEmailMessage(
            null, 
            new List<String>{'abc@123.com'}, 
            null, 
            testTemplate.Id,
            'Email Test 3',
            null, 
            null, 
            testLead.Id,
            null
        );         
        
        messages = new Messaging.SingleEmailMessage[]{msg1, msg2, msg3};
        
     	return messages;
    }
}