/**
 * @description       : 
 * @author            : Ben Lorenz
 * @group             : 
 * @last modified on  : 07-29-2020
 * @last modified by  : Ben Lorenz
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   07-29-2020   Ben Lorenz   Initial Version
**/
public class CC_PavilionPartsOrderHistoryController {
    public CC_PavilionPartsOrderHistoryController(CC_PavilionTemplateController controller) {}
         
    @RemoteAction
    public static List <TempWrapperClass> getOrderStatusData(String userAccountId) {
        List <CC_Order__c> lstOrder = new List <CC_Order__c>();
        List <TempWrapperClass> tmpList = new List <TempWrapperClass>();
        Date today = system.today();
        Date currWeekStart = today.toStartofWeek();
        Date nextWeekStart = currWeekStart.addDays(7);
        Date lastWeekStart = currWeekStart.addDays(-7);
        Date lastMonthStart = today.addDays(-30);
        Date filterDate = today.addyears(-3); //only showing 3 years of history
        lstOrder = [SELECT Id,Order_Number__c,Name,CC_Quote_Order_Date__c,CC_Addressee_name__c,PO__c,CC_Shopping_Cart_Name__c,CC_Order_Number_Reference__c,Hold_Code__c,
                           Complete_Date__c,CC_Status__c,LastModifiedDate,CC_Original_Promise_Date__c,CC_Warehouse__c,(SELECT Id FROM Shipments__r)
                      FROM CC_Order__c
                     WHERE CC_Account__c = :userAccountId AND 
                           RecordType.Name = :'Parts Ordering' AND 
                           cc_Status__c in ('OPEN', 'CLOSED', 'Submitted', 'Load Failure') AND
                           CC_Warehouse__c in ('P','PDS','1') AND 
                           CC_Quote_Order_Date__c >= :filterDate];
        for (CC_Order__c order: lstOrder) {
          TempWrapperClass twc = new TempWrapperClass(order);
          twc.StatusShipping=order.Shipments__r.isEmpty()?'N':'Y';
          String dates='';
          if (order.CC_Quote_Order_Date__c >= nextWeekStart){dates='Next Week';}
          else if (order.CC_Quote_Order_Date__c >= currWeekStart) {dates='This Week';}
          else if (order.CC_Quote_Order_Date__c >= lastWeekStart) {dates='Last Week';}
          if (order.CC_Quote_Order_Date__c >= lastMonthStart && order.CC_Quote_Order_Date__c <= today) {dates+='Last Month';}
          twc.dateFilters=dates;
          tmpList.add(twc);
        }
        return tmpList;
    }

    public Class TempWrapperClass {
        public CC_Order__c order {get; set;}
        public String StatusShipping {get; set;}
        public String orderStatus {get;set;}
        public String dateFilters {get;set;}

        public TempWrapperClass(CC_Order__c ordr) {
            order = ordr;
            orderStatus=ordr.CC_Status__c=='Load Failure'?'Submitted':ordr.CC_Status__c;
        }
    }

    @RemoteAction
    public static List <ShipmentWrapperClass> getShipments(String selectedOrderId) {
        List <ShipmentWrapperClass> shipments = new List <ShipmentWrapperClass>();
        for(CC_Order_Shipment__c elem : [SELECT Id,Name,Order__r.Order_Number__c,Order__r.PO__c,Shipment_Header_Number__c,Ship_Date__c,Invoice_Number__c,
                                                Order__r.CC_Addressee_name__c,Carrier__c,Pro_Bill__c,CC_Invoice_Key__c,
                                                (SELECT Id,Order_Item__r.Product__r.ProductCode,Order_Item__r.Product__r.Name,Order_Item__r.CC_Pricing_U_M__c,
                                                 Order_Item__r.CC_Quantity__c,Ship_Quantity__c,Backorder_Quantity__c FROM Order_Shipment_Items__r)
                                          FROM CC_Order_Shipment__c 
                                         WHERE Order__r.Id = :selectedOrderId]) {
            if(null != elem.Pro_Bill__c && null != elem.Carrier__c && null !=CC_Carriers__c.getInstance(elem.Carrier__c))  {
                shipments.add(new ShipmentWrapperClass(elem, String.valueOf(CC_Carriers__c.getInstance(elem.Carrier__c).Website__c).replace('[TRACKING_NUMBER]',elem.Pro_Bill__c)));
            } else {
                shipments.add(new ShipmentWrapperClass(elem, null));
            }
        }
        
        return shipments;
    }
    
    public Class ShipmentWrapperClass {
        public CC_Order_Shipment__c shipment {get; set;}
        public String carrierWebsite {get; set;}
        public List<CC_Order_Shipment_Item__c> shipmentDetails {get;set;}

        public ShipmentWrapperClass(CC_Order_Shipment__c shipment, String carrierWebsite) {
            this.shipment = shipment;
            this.carrierWebsite = carrierWebsite;
            this.shipmentDetails=shipment.Order_Shipment_Items__r;
        }
    }
}