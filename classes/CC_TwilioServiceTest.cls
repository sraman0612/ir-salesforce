@isTest
public without sharing class CC_TwilioServiceTest {

    @testSetup
    private static void createData(){

        upsert CC_Twilio_Settings__c.getOrgDefaults(); // use defaults

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User']; 

        User u = new User(Alias = 'standt', Email='standarduser@irco.com.dpcc', 
                          EmailEncodingKey='UTF-8', LastName='Testing123', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id, MobilePhone='8454001234',
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123@irco.com.dpcc');   
                          
        insert u;
    }

    private static testMethod void testGetRecipientMobile(){

        User u = [Select Id, IsPortalEnabled, MobilePhone, Contact.MobilePhone From User Where isActive = true and UserName='standarduser123@irco.com.dpcc' LIMIT 1];

        Test.startTest();

        System.runAs(u) {
            CC_TwilioService.getRecipientMobilePhone(u);
        }        

        Test.stopTest();
    }

    public static testMethod void testSendSMSSuccess(){

        Test.startTest();

        CC_TwilioService.TwilioApiClientResponseWrapper tt=new CC_TwilioService.TwilioApiClientResponseWrapper();
        CC_TwilioService.sendSMS('8097927903','abc 123');

        Test.stopTest();
    } 
    
    public static testMethod void testSendSMSMessageTooLong(){

        Test.startTest();

        CC_TwilioService.TwilioApiClientResponseWrapper tt=new CC_TwilioService.TwilioApiClientResponseWrapper();
        CC_TwilioService.sendSMS('8097927903','hi111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111shdjlshdljdldgshflgfhdbfjhdfdfgjfgdsfgeyireyrpyreehbfdfbdufgerewpiruyerpieyrpiyrpyereryrehrjbfdbfgfegiripryipryrhefjhewuiryryeoiryrherjrherriruireyeiouryeibfefilfiuhuiehiiuerreouyroiyrueyrryiuerfhoehriureoriroryoyryiuheirherheirhrererhenferhrerherhreuroryerioryroyryeoryryoryeorroueroroeryrejfndfnnfueogungnnugeghgpeugepwewpugepguepgpeugpey89yt8y48tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyytttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttjhdljehlfhdlkjfhldkjfhifdygpr9egyr9[rhgr;kjgng;reoihr[trthyr;iotyoirytiorthrtyioytprytjghfdkjlghfhkjghfjgfhfkhgkjlglihgliufydhgi;ygiyrgirjhbfdjlbfdkjbflibvilughdfgiluiglhrlgiruighrilhhthuirhkjgfjkfvbjvbfhldfhlidhfiluhuierfiugrlfyiuritruitruiytruirtyytirtthienyncobvbevryeryriroebybryeuyroeyrioryenryocynycninoyoiyuvoyibr yoeireebvobiobreyioeyroiryeorybrybyowyiepiyweiwtrwertyuiodfghjkfghjkygkhfdsjkfdgfkgeurtotyroiuytrenvnreitynryvtbroyorovnoinyovboievybrobvbtnvtvnvontotnytrntyetntrtyetyvtroeiyiyvotnvvonvronvinivotytyrtoivnoiynytiuotyo');

        Test.stopTest();
    }       
}