public with sharing class CC_Clone_Short_Term_Lease_Controller {
    
    private class CC_Clone_Short_Term_Lease_ControllerException extends Exception{}
    
    @TestVisible private static String defaultLeaseAprovalStatus = 'Not Yet Submitted';
    @TestVisible private static String defaultLeaseCarsFlag = 'N';
    @TestVisible private static String defaultLeaseVehicleStatus = 'Pending';
            
    @TestVisible
    private class CloneLeaseResponse extends LightningResponseBase{
    	public Id newLeaseId;
    }

    @AuraEnabled
    public static String cloneLeaseRecord(Id leaseId, Boolean isExtension, Date beginDate, Date endDate){
    	
    	CloneLeaseResponse response = new CloneLeaseResponse();
    	System.Savepoint sp = Database.setSavepoint();
    	
    	try{
    		
    		// Get the lease record including all lease fields
			CC_Short_Term_Lease__c lease = Database.query('Select ' + String.join(sObjectService.getAllCreateableFields(CC_Short_Term_Lease__c.getSObjectType()), ',') + ' From CC_Short_Term_Lease__c Where Id = :leaseId');
    		
    		system.debug('lease to clone: ' + lease);
    		
			// Clone the lease
			CC_Short_Term_Lease__c newLease = lease.clone(false, true);

            newLease.Parent_Lease__c = lease.Id;
			
			// Reset some of the fields on the new lease
			newLease.Name = null;
			newLease.LeaseKey__c = null;	
			newLease.Unique_Key_Test_Helper__c = Test.isRunningTest() ? '1234' : null; // Needed for Apex tests to avoid unique field constraints		
			newLease.Approval_Status__c = defaultLeaseAprovalStatus;
			newLease.Approval_Process_Name__c = null;
			newLease.Approval_Submission_Date__c = null;
			newLease.Delivery_Quoted_Rate__c = null;
			newLease.Pickup_Quoted_Rate__c = null;
			newLease.Delivery_Internal_Cost__c = null;
			newLease.Pickup_Internal_Cost__c = null;	
			newLease.Beginning_Date__c = beginDate;
			newLease.Ending_Date__c = endDate;
			newLease.Adjustments_and_Prepping_Fees__c = null;
			newLease.New_Car_Order__c = null;
			
			// Reset integrated fields
			newLease.Cars__c = defaultLeaseCarsFlag;
			newLease.Vehicle_Status__c = defaultLeaseVehicleStatus;
			newLease.Carrier__c = null;
			newLease.Next_Billing_Date__c = null;
			newLease.Remaining_Payments__c = null;
			newLease.Synced_to_MAPICS__c = false;
			
			if (isExtension){
				newLease.Lease_Extension_Count__c = newLease.Lease_Extension_Count__c != null ? newLease.Lease_Extension_Count__c + 1 : 1;
			}
			else{
				newLease.Lease_Extension_Count__c = 0;
				newLease.PO_Number__c = null;
			}
			
			system.debug('inserting new lease: ' + newLease);
			   			
			insert newLease;
							
			response.newLeaseId = newLease.Id;
			
			// Clone all STL Car Info records
			CC_STL_Car_Info__c[] cars = Database.query('Select ' + String.join(sObjectService.getAllCreateableFields(CC_STL_Car_Info__c.getSObjectType()), ',') + ' From CC_STL_Car_Info__c Where Short_Term_Lease__c = :leaseId');
			CC_STL_Car_Info__c[] newCars = new CC_STL_Car_Info__c[]{}; 
			
			system.debug('cars to clone: ' + cars);
			
			for (Integer i = 0; i < cars.size(); i++){
				
				CC_STL_Car_Info__c car = cars[i];
				CC_STL_Car_Info__c newCar = car.clone(false, true);
				newCar.Short_Term_Lease__c = newLease.Id;
				newCar.CarInfoKey__c = null;	
				newCar.Unique_Key_Test_Helper__c = Test.isRunningTest() ? i + '123' : null; // Needed for Apex tests to avoid unique field constraints			
				newCars.add(newCar);
			}				
		
			system.debug('inserting new cars: ' + newCars);
		
			insert newCars;
			
			// Build a map of the old car Ids and their corresponding new cloned car Ids
			Map<Id, Id> carIdMap = new Map<Id, Id>();
			
			for (CC_STL_Car_Info__c newCar : newCars){
				carIdMap.put(newCar.getCloneSourceId(), newCar.Id);
			}
			
			// Clone all STL Car Accessory Line Items
			CC_STL_Car_Accessory_Line_Item__c[] accessoryLineItems = Database.query('Select ' + String.join(sObjectService.getAllCreateableFields(CC_STL_Car_Accessory_Line_Item__c.getSObjectType()), ',') + ' From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c in :cars');
			CC_STL_Car_Accessory_Line_Item__c[] newAccessoryLineItems = new CC_STL_Car_Accessory_Line_Item__c[]{}; 
			
			system.debug('accessoryLineItems to clone: ' + cars);
			
			for (CC_STL_Car_Accessory_Line_Item__c accessoryLineItem : accessoryLineItems){
				
				CC_STL_Car_Accessory_Line_Item__c newAccessoryLineItem = accessoryLineItem.clone(false, true);
				newAccessoryLineItem.STL_Car__c = carIdMap.get(accessoryLineItem.STL_Car__c);				
				newAccessoryLineItems.add(newAccessoryLineItem);							
			}				
			
			system.debug('inserting newAccessoryLineItems: ' + newAccessoryLineItems);
		
			insert newAccessoryLineItems;			
    	}
    	catch(Exception e){
    		system.debug('exception: ' + e);
    		response.success = false;
    		response.errorMessage = e.getMessage();
    		Database.rollback(sp);
    	}
    	  	
    	return JSON.serialize(response);
    }
}