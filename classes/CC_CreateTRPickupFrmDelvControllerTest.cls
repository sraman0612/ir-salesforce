@isTest
public with sharing class CC_CreateTRPickupFrmDelvControllerTest {
    
    private static Id caseTRtypeId = sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car - Transportation Request');
    
    @TestSetup
    private static void createData(){

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;         
        
        Account acct = [Select Id From Account LIMIT 1];

    	// Create a short term lease to assign to the cases
        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        masterLease.Customer__c = acct.Id;
        insert masterLease;
        
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        location.Name = 'Wally World';
        location.Primary_Contact_Name__c = 'Jesse Ventura';
        location.Phone__c = '(555) 555-5555';
        location.Address__c = '555 Main Street';
        location.City__c = 'Chicago';
        location.State__c = 'IL';
        location.Zip__c = '46112';
        insert location;             
    	
    	CC_Short_Term_Lease__c stl = TestDataUtility.createShortTermLease();
        stl.Master_Lease__c = masterLease.Id;
        stl.Del_Sched_Location__c = location.Id;
        stl.Serial_Numbers__c = 'ABC123;23434';
        insert stl;

        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';

        insert new Product2[]{prod1};        
        
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl.Id;
        car1.Quantity__c = 10;
        insert car1;

    	Case trCase = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stl.Id, 500);   
		
		// Set fields that will be used in the clone operation
        trCase.Status = 'Scheduled';
        trCase.CC_Quote_Date__c = Date.today();
        trCase.CC_Car_Quantity__c = 10;
        trCase.CC_STL_Serial_Numbers__c = 'ABC123;DFSE333';
			
		trCase.CC_Stop_1_Name__c = 'Stop 1';		
		trCase.CC_Stop_1_Contact__c = null;		
		trCase.CC_Stop_1_Phone__c = '(432) 555-5555';		
		trCase.CC_Stop_1_Address__c = '123 Rosevelt Str';		
		trCase.CC_Stop_1_City__c = 'Charlotte';		
		trCase.CC_Stop_1_State__c = 'North Carolina';		
		trCase.CC_Stop_1_Zip__c = '85443';		
		trCase.CC_Stop_1_Qty_Type_of_Car__c = '25/Gas';		
		trCase.CC_Stop_1_Instructions__c = 'Ask front desk clerk.';
		 		
		trCase.CC_Stop_2_Name__c = 'Stop 2';		
		trCase.CC_Stop_2_Contact__c = null;		
		trCase.CC_Stop_2_Phone__c = '(432) 888-5555';		
		trCase.CC_Stop_2_Address__c = '125 Main Str';		
		trCase.CC_Stop_2_City__c = 'Indianapolis';		
		trCase.CC_Stop_2_State__c = 'Indiana';		
		trCase.CC_Stop_2_Zip__c = '45883';		
		trCase.CC_Stop_2_Qty_Type_of_Car__c = '50/Electric';		
		trCase.CC_Stop_2_Instructions__c = 'Page manager @555-555-5555.';
    	
    	insert trCase;
    }

    @isTest
    private static void testCloneCaseSuccessStop1(){
    	
    	Case c = Database.query(getCaseQuerySelectClause() + 'Where RecordTypeId = :caseTRtypeId');
    	
    	// Make sure no stop 2 is assigned
		c.CC_Stop_2_Name__c = null;		
		c.CC_Stop_2_Contact__c = null;		
		c.CC_Stop_2_Phone__c = null;		
		c.CC_Stop_2_Address__c = null;		
		c.CC_Stop_2_City__c = null;		
		c.CC_Stop_2_State__c = null;		
		c.CC_Stop_2_Zip__c = null;		
		c.CC_Stop_2_Qty_Type_of_Car__c = null;		
		c.CC_Stop_2_Instructions__c = null;
		
		update c;
   	
   		Test.startTest();
   	
    	// Call the controller to clone the case
    	CC_CreateTRPickupFromDeliveryController.cloneDeliveryCases(JSON.serialize(new Case[]{c}));
    	
    	Test.stopTest();
    	
    	// Validate the results
    	system.assertEquals(2, [Select Count() From Case Where RecordTypeId = :caseTRtypeId]);
    	
        // Retrieve the new case created and validate the field values
        Id caseId = c.Id;
    	Case newCase = Database.query(getCaseQuerySelectClause() + 'Where Id != :caseId');
				  
        CC_Short_Term_Lease__c lease = [Select Ending_Date__c From CC_Short_Term_Lease__c];

	  	system.assertEquals('New', newCase.Status);
	  	system.assertEquals(c.Reason, newCase.Reason);
	  	system.assertEquals('Short Term Lease Pick-up', newCase.Sub_Reason__c);
        system.assertEquals(lease.Ending_Date__c, newCase.CC_Requested_Delivery_Date__c);
        system.assertEquals('ABC123;DFSE333', newCase.CC_STL_Serial_Numbers__c);
        system.assertEquals(10, newCase.CC_Car_Quantity__c);     
        system.assertEquals(Date.today(), newCase.CC_Quote_Date__c);
        system.assertEquals(500, newCase.CC_Quoted_Price__c);        

	  	system.assertEquals(c.CC_Stop_1_Name__c, newCase.CC_Pickup_Name__c);
	  	system.assertEquals(c.CC_Stop_1_Contact__c, newCase.CC_Pickup_Contact__c);
	  	system.assertEquals(c.CC_Stop_1_Address__c, newCase.CC_Pickup_Address__c);
	  	system.assertEquals(c.CC_Stop_1_City__c, newCase.CC_Pickup_City__c);
	  	system.assertEquals(c.CC_Stop_1_State__c, newCase.CC_Pickup_State__c);
        system.assertEquals(c.CC_Stop_1_Zip__c, newCase.CC_Pickup_Zip__c);
        system.assertEquals(c.CC_Stop_1_Qty_Type_of_Car__c, newCase.CC_Pickup_Qty_Type_of_Car__c);
	  	
	  	system.assertEquals('Wally World', newCase.CC_Stop_1_Name__c);
        system.assertEquals('Jesse Ventura', newCase.CC_Stop_1_Contact__c);
        system.assertEquals('(555) 555-5555', newCase.CC_Stop_1_Phone__c);  
	  	system.assertEquals('555 Main Street', newCase.CC_Stop_1_Address__c);
	  	system.assertEquals('Chicago', newCase.CC_Stop_1_City__c);
	  	system.assertEquals('IL', newCase.CC_Stop_1_State__c);
	  	system.assertEquals('46112', newCase.CC_Stop_1_Zip__c);
	  	
	  	system.assertEquals(null, newCase.CC_Stop_2_Name__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Contact__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Address__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_City__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_State__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Zip__c);  		
    }
   
    @isTest
    private static void testCloneCaseSuccessStop2(){
    	
    	Case c = Database.query(getCaseQuerySelectClause() + 'Where RecordTypeId = :caseTRtypeId');
    	
    	Test.startTest();
    	
    	// Call the controller to clone the case
    	CC_CreateTRPickupFromDeliveryController.cloneDeliveryCases(JSON.serialize(new Case[]{c}));
    	
    	Test.stopTest();
    	
    	// Validate the results
    	system.assertEquals(2, [Select Count() From Case Where RecordTypeId = :caseTRtypeId]);    	
    	
        // Retrieve the new case created and validate the field values
        Id caseId = c.Id;
    	Case newCase = Database.query(getCaseQuerySelectClause() + 'Where Id != :caseId');
				  
        CC_Short_Term_Lease__c lease = [Select Ending_Date__c From CC_Short_Term_Lease__c];

	  	system.assertEquals('New', newCase.Status);
	  	system.assertEquals(c.Reason, newCase.Reason);
	  	system.assertEquals('Short Term Lease Pick-up', newCase.Sub_Reason__c);
        system.assertEquals(lease.Ending_Date__c, newCase.CC_Requested_Delivery_Date__c);
        system.assertEquals('ABC123;DFSE333', newCase.CC_STL_Serial_Numbers__c);
        system.assertEquals(10, newCase.CC_Car_Quantity__c);

	  	system.assertEquals(c.CC_Stop_2_Name__c, newCase.CC_Pickup_Name__c);
	  	system.assertEquals(c.CC_Stop_2_Contact__c, newCase.CC_Pickup_Contact__c);
	  	system.assertEquals(c.CC_Stop_2_Address__c, newCase.CC_Pickup_Address__c);
	  	system.assertEquals(c.CC_Stop_2_City__c, newCase.CC_Pickup_City__c);
	  	system.assertEquals(c.CC_Stop_2_State__c, newCase.CC_Pickup_State__c);
	  	system.assertEquals(c.CC_Stop_2_Zip__c, newCase.CC_Pickup_Zip__c);
        system.assertEquals(c.CC_Stop_2_Qty_Type_of_Car__c, newCase.CC_Pickup_Qty_Type_of_Car__c);
          
	  	system.assertEquals('Wally World', newCase.CC_Stop_1_Name__c);
        system.assertEquals('Jesse Ventura', newCase.CC_Stop_1_Contact__c);
        system.assertEquals('(555) 555-5555', newCase.CC_Stop_1_Phone__c);  
	  	system.assertEquals('555 Main Street', newCase.CC_Stop_1_Address__c);
	  	system.assertEquals('Chicago', newCase.CC_Stop_1_City__c);
	  	system.assertEquals('IL', newCase.CC_Stop_1_State__c);
	  	system.assertEquals('46112', newCase.CC_Stop_1_Zip__c);
	  	
	  	system.assertEquals(null, newCase.CC_Stop_2_Name__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Contact__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Address__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_City__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_State__c);
	  	system.assertEquals(null, newCase.CC_Stop_2_Zip__c);	  		  		
    }       

    private static String getCaseQuerySelectClause(){
        return 'Select Id, RecordTypeId, Status, Reason, Sub_Reason__c, CC_Requested_Delivery_Date__c, CC_Short_Term_Lease__c, CC_Car_Quantity__c, CC_STL_Serial_Numbers__c, ' +
        'CC_Pickup_Name__c, CC_Pickup_Contact__c, CC_Pickup_Address__c, CC_Pickup_City__c, CC_Pickup_State__c, CC_Pickup_Zip__c, CC_Pickup_Phone__c, CC_Pickup_Qty_Type_of_Car__c,' +
        'CC_Stop_1_Name__c, CC_Stop_1_Contact__c, CC_Stop_1_Phone__c, CC_Stop_1_Address__c, CC_Stop_1_City__c, CC_Stop_1_State__c, CC_Stop_1_Zip__c, CC_Stop_1_Qty_Type_of_Car__c, CC_Stop_1_Instructions__c,' +
        'CC_Stop_2_Name__c, CC_Stop_2_Contact__c, CC_Stop_2_Phone__c, CC_Stop_2_Address__c, CC_Stop_2_City__c, CC_Stop_2_State__c, CC_Stop_2_Zip__c, CC_Stop_2_Qty_Type_of_Car__c, CC_Stop_2_Instructions__c,' + 
        'CC_Quote_Date__c, CC_Quoted_Price__c ' +
        'From Case ';
    }
}