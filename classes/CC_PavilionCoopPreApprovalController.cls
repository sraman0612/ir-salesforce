global  class CC_PavilionCoopPreApprovalController {

   

    /* member variables */

   
    public Set<String> lstpreApproval{get;set;} /* user for Advertising_Type__c picklist */
    public String UserAccountId{get; set;}
    public Date Today { get { return Date.today(); }}
    private static final String CC_Pre_Approved_Request = 'CC_Pre_Approved_Request';
    /* constructor */    

     public CC_PavilionCoopPreApprovalController(CC_PavilionTemplateController controller) {

System.debug('Line95');

        List<User> lstUser = [SELECT AccountId, ContactId FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        if(lstUser.isEmpty()) return;
        UserAccountId = lstUser[0].AccountId;
        
        
        lstpreApproval = new Set<String>();
        Schema.DescribeFieldResult fieldResult = CC_Hole_in_One__c.Type_of_pre_approval_Request__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            lstpreApproval.add(f.getValue());
        }
        
        
    }
    
    /*** Remote method to be called from CC_PavilionCoopClaim page via JS Remoting **/
    
    @RemoteAction
    global static String refreshTableBasedOnAggrement(String selectedAggrementType,String UserAccountId) {
        List<Contract> listContract = new List<Contract>();
        List<Contract> listContractToParse = new List<Contract>();
        if(String.isNotBlank(selectedAggrementType))
        {
            listContract =[SELECT Id, Name, CC_Contract_End_Date__c, StartDate
                           from Contract where AccountId =: UserAccountId 
                           AND CC_Type__c =: selectedAggrementType 
                           AND CC_Contract_End_Date__c >: System.today()];
        }
        
        for(Contract objContract : listContract)
        {
            objContract.CC_Contract_End_Date__c = objContract.CC_Contract_End_Date__c.addHours(-5).addMinutes(-30);
            listContractToParse.add(objContract);
        }
        
        if(!listContractToParse.isEmpty())
        {
            String jsonData = (String)JSON.serialize(listContractToParse);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
    //method added by tapas for passing the Agrrement Types dynamically via js remote call. Projecet Task 285..
    @RemoteAction
      public Static List<Contract> getAggrementTypes() {
          Set<Contract> setContract = new Set<Contract>();
          List<Contract> agreementList= new List<Contract>();
          String userid = UserInfo.getUserId();
          User U = [select id, ContactId, Contact.Accountid from user where Id = : userid];
          Integer currentYear=System.Today().year();
          Id accId = U.Contact.Accountid;
          For(Contract CCop:[SELECT Id,CC_Sub_Type__c,CC_Type__c
                                                   FROM Contract Where CC_Type__c = 'Dealer/Distributor Agreement' 
                                                   AND AccountId =:accId ]){
                                                    setContract.add(CCop);
                                                }
          
          for(Contract cc: setContract){
              agreementList.add(cc);
          }
          return agreementList;
     }
    
    /**Remote method to submit the Coop Other data**/
    
    
    
    @RemoteAction
    global static String submitCoopClaimData(String eventContact, String PhNum, String email,String eventName, String location, String startDate, 
                                             String endDate,String target, String estimatedAudiance,
                                             String featuredProducts,String competitors, String noOfClubCarDisplay, 
                                             String boothSpaceSize, String boothCost, String totalShowCost, String eventHeldBefore,
                                             String participatedInEventBefore, String whenParticipatedInEventBefore,
                                             String clubCarParticipatedBefore, String whenClubCarParticipatedBefore,
                                             String fundsRequested, String describeEventPlan,String ContractId,String preApprovalType,String BlackandGoldPartner,
                                             String SalesRep,String AnyPriorFundsRequest,String AmountRequesting,String DigitalAdOpportunity, String PriorFundRequest)
    {
   
    
    
        try
        {
            id RTid = [SELECT id FROM RecordType  WHERE DeveloperName =: CC_Pre_Approved_Request].id;
            Date newStartDate = null;
            Date newEndDate = null;
            Date newParticipatedDate = null;
            Date newClubCarParticipatedDate = null;
            if(startDate != null && startDate !='') {
                String[] sd = startDate.split('/');
                if(sd.size()>2 && sd!=null)
                    newStartDate = Date.newInstance(Integer.valueOf(sd[2]),Integer.valueOf(sd[1]),Integer.valueOf(sd[0]));
            }
            if(endDate != null && endDate !='') {
                String[] ed = endDate.split('/');
                if(ed.size()>2 && ed!=null)
                    newEndDate = Date.newInstance(Integer.valueOf(ed[2]),Integer.valueOf(ed[1]),Integer.valueOf(ed[0]));
            }
            if(whenParticipatedInEventBefore != null && whenParticipatedInEventBefore !='') {
                String[] wpd = whenParticipatedInEventBefore.split('/');
                if(wpd.size()>2 && wpd!=null)
                    newParticipatedDate = Date.newInstance(Integer.valueOf(wpd[2]),Integer.valueOf(wpd[1]),Integer.valueOf(wpd[0]));
            }
            if(whenClubCarParticipatedBefore != null && whenClubCarParticipatedBefore !='') {
                String[] wcd = whenClubCarParticipatedBefore.split('/');
                if(wcd.size()>2 && wcd!=null)
                    newClubCarParticipatedDate = Date.newInstance(Integer.valueOf(wcd[2]),Integer.valueOf(wcd[1]),Integer.valueOf(wcd[0]));
            }
            
            System.debug('@@@@eventContact@@@@'+eventContact);
            System.debug('@@@@PhNum@@@@'+PhNum);
            System.debug('@@@@email@@@@'+email);
            CC_Hole_in_One__c objCCCoopClaim = new CC_Hole_in_One__c();
            objCCCoopClaim.recordTypeId = RTid;
            objCCCoopClaim.Booth_Cost__c = ((boothCost!=null && boothCost!='')?Decimal.valueOf(boothCost):0);
            objCCCoopClaim.Booth_Space_Size__c = ((boothSpaceSize!=null && boothSpaceSize!='')?Decimal.valueOf(boothSpaceSize):0);
            objCCCoopClaim.Competitors__c = competitors;
            objCCCoopClaim.End_Date__c = newEndDate;
            objCCCoopClaim.Estimated_Attendance__c = ((estimatedAudiance!=null && estimatedAudiance!='')?Decimal.valueOf(estimatedAudiance):0);
            objCCCoopClaim.Primary_Event_Contact__c = eventContact;
            objCCCoopClaim.Phone_Number__c = PhNum;
            objCCCoopClaim.Email_Address__c = email;
            objCCCoopClaim.Event_Name__c = eventName;
            objCCCoopClaim.Event_Plan_Including_resources_to_commit__c = describeEventPlan;
            objCCCoopClaim.Featured_Products__c = featuredProducts;
            objCCCoopClaim.Has_Club_Car_Participated_Before__c = ((clubCarParticipatedBefore=='Yes')?true:false);
            objCCCoopClaim.Location__c = location;
            objCCCoopClaim.Number_of_Club_Car_display_vehicles__c = ((noOfClubCarDisplay!=null && noOfClubCarDisplay!='')?Decimal.valueOf(noOfClubCarDisplay):0);
            objCCCoopClaim.participated_in_this_event_before__c = ((participatedInEventBefore=='Yes')?true:false);
            objCCCoopClaim.Start_Date__c = newStartDate;
            objCCCoopClaim.Target_Audience__c = target;
            objCCCoopClaim.Contract__c=ContractId;
            objCCCoopClaim.This_Event_has_been_Held_before__c = ((eventHeldBefore=='Yes')?true:false );
            objCCCoopClaim.Total_Show_Cost__c = ((totalShowCost!=null && totalShowCost!='')?Decimal.valueOf(totalShowCost):0);
            objCCCoopClaim.Type_of_pre_approval_Request__c = preApprovalType;
            objCCCoopClaim.When_participated_in_this_event_before__c = newParticipatedDate;
            objCCCoopClaim.When_Club_Car_Participated_Before__c = newClubCarParticipatedDate;
            objCCCoopClaim.CC_Status__c = 'Submitted';
            objCCCoopClaim.Status__c = 'Active';
            objCCCoopClaim.Black_and_Gold_Partner__c = ((BlackandGoldPartner=='on')?true:false);
            objCCCoopClaim.Sales_Rep__c = SalesRep;
            objCCCoopClaim.Any_Prior_Funds_Request__c = ((AnyPriorFundsRequest=='Yes')?true:false );
            objCCCoopClaim.Amount_Requesting__c = ((AmountRequesting!=null && AmountRequesting!='')?Decimal.valueOf(AmountRequesting):0);
            objCCCoopClaim.Digital_Ad_Opportunity__c = ((DigitalAdOpportunity=='on')?true:false);
            objCCCoopClaim.Prior_Fund_Request__c= PriorFundRequest;
            insert objCCCoopClaim;
            system.debug('the objCCCoopClaim is'+objCCCoopClaim);
            String requestNum = [SELECT Pre_Approved_Request__c from CC_Hole_in_One__c WHERE id =:objCCCoopClaim.id].Pre_Approved_Request__c;
            String successMessage = 'Your Pre-Approval Request#'+requestNum+' is submitted for review and approval';
            return successMessage;
        }
        catch(Exception e)
        {
            String s= e.getStackTraceString();
            return 'Failure';
        }
    }
}