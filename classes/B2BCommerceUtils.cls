/**
 * @author           Amit Datta
 * @description      Test Class for B2BCommerceUtils.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  14/01/2024          Original Version
 **/

public with sharing class B2BCommerceUtils {
	// Function to lookup the webstore ID associated with the communityId
	public static String resolveCommunityIdToWebstoreId(String communityId) {
		if (communityId == null || communityId == '') {
			return null;
		}
		String webstoreId = null;
		if (Schema.sObjectType.WebStoreNetwork.fields.WebStoreId.isAccessible() && Schema.sObjectType.WebStoreNetwork.fields.NetworkId.isAccessible()) {
			List<WebStoreNetwork> wsnList = [SELECT WebStoreId FROM WebStoreNetwork WHERE NetworkId = :communityId];
			if (!wsnList.isEmpty()) {
				WebStoreNetwork wsn = wsnList.get(0);
				webstoreId = wsn.WebStoreId;
			}
		}
		return webstoreId;
	}

	public static String getAccountIdFromUser() {
		List<User> userContext = [SELECT Contact.AccountId FROM User WHERE Id = :System.UserInfo.getUserId()];
		return userContext[0]?.Contact?.AccountId;
	}
}