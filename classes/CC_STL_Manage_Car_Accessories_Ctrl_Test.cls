@isTest
public with sharing class CC_STL_Manage_Car_Accessories_Ctrl_Test {

    @TestSetup
    private static void createTestData(){
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';     
        insert new Product2[]{prod1, prod2};
            
    	CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
    	insert masterLease;
    	
    	CC_Short_Term_Lease__c stl = TestDataUtility.createShortTermLease();
    	stl.Master_Lease__c = masterLease.Id;
    	insert stl;
            
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl.Id;
        CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod2.Id);
        car2.Short_Term_Lease__c = stl.Id;
        insert new CC_STL_Car_Info__c[]{car1, car2};   
        
        CC_STL_Car_Accessory__c accessory1 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Deluxe Towbar', 500);
        CC_STL_Car_Accessory__c accessory2 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Towbar', 500);
        CC_STL_Car_Accessory__c accessory3 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Sandbottle', 500);
        CC_STL_Car_Accessory__c accessory4 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Portable Refreshment Center', 500);
        CC_STL_Car_Accessory__c accessory5 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Sandbucket', 500);
        CC_STL_Car_Accessory__c accessory6 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Deluxe Towbar', 500);
        CC_STL_Car_Accessory__c accessory7 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Towbar', 500);
        insert new CC_STL_Car_Accessory__c[]{accessory1, accessory2, accessory3, accessory4, accessory5, accessory6, accessory7};
    }  
    
    @isTest
    private static void testGetInitFailure(){
        
        CC_STL_Manage_Car_Accessories_Controller.forceError = true;
        
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCarInfo = [Select Id, Item__c, (Select Id, Quantity__c, Unit_Labor_Cost__c, STL_Car_Accessory__r.Accessory_Type__c From CC_STL_Car_Accessory_Line_Items__r Order By Accessory_Type__c ASC) From CC_STL_Car_Info__c Where Item__r.Name = 'Club Car 1'];
        
        String responseStr = CC_STL_Manage_Car_Accessories_Controller.getInit(stlCarInfo.Id);
        CC_STL_Manage_Car_Accessories_Controller.InitResponse response = (CC_STL_Manage_Car_Accessories_Controller.InitResponse)JSON.deserialize(responseStr, CC_STL_Manage_Car_Accessories_Controller.InitResponse.class);
    
    	system.assertEquals(false, response.success);
        system.assertEquals('Test Exception', response.errorMessage);
        system.assertEquals(0, response.lineItemOptions.size());        
    }
    
    @isTest
    private static void testGetInitSuccess(){
        
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCarInfo = [Select Id, Item__c, (Select Id, Quantity__c, Unit_Labor_Cost__c, STL_Car_Accessory__r.Accessory_Type__c From CC_STL_Car_Accessory_Line_Items__r Order By Accessory_Type__c ASC) From CC_STL_Car_Info__c Where Item__r.Name = 'Club Car 1'];
        system.assertEquals(0, stlCarInfo.CC_STL_Car_Accessory_Line_Items__r.size());
        
        CC_STL_Car_Accessory__c[] availableAccessories = [Select Id, Name, Accessory_Type__c, Labor_Cost__c 
                                                      From CC_STL_Car_Accessory__c 
                                                      Where Product__c = :stlCarInfo.Item__c
                                                      Order By Accessory_Type__c ASC];
        
        String responseStr = CC_STL_Manage_Car_Accessories_Controller.getInit(stlCarInfo.Id);
        CC_STL_Manage_Car_Accessories_Controller.InitResponse response = (CC_STL_Manage_Car_Accessories_Controller.InitResponse)JSON.deserialize(responseStr, CC_STL_Manage_Car_Accessories_Controller.InitResponse.class);
    
    	system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
        system.assertEquals(availableAccessories.size(), response.lineItemOptions.size());
        
        // The accessories should be sorted alpha
        for (Integer i= 0; i < response.lineItemOptions.size(); i++){
            
            CC_STL_Manage_Car_Accessories_Controller.STL_Car_Accessory_Line_Item_Option option = response.lineItemOptions[i];
                
            system.assertEquals(false, option.isSelected);
            system.assertEquals(null, option.lineItemId);
            system.assertEquals(stlCarInfo.Id, option.carInfoId);
            system.assertEquals(availableAccessories[i].Id, option.carAccessoryId);
            system.assertEquals(availableAccessories[i].Accessory_Type__c, option.accessoryName);
            system.assertEquals(1, option.quantity);
            system.assertEquals(availableAccessories[i].Labor_Cost__c, option.unitLaborCost);
        }
        
        // Add a line item
        CC_STL_Car_Accessory_Line_Item__c newLineItem = TestDataUtility.createSTLCarAccessoryLineItem(stlCarInfo.Id, availableAccessories[0].Id, 500, 2);
        insert newLineItem;
        
        responseStr = CC_STL_Manage_Car_Accessories_Controller.getInit(stlCarInfo.Id);
        response = (CC_STL_Manage_Car_Accessories_Controller.InitResponse)JSON.deserialize(responseStr, CC_STL_Manage_Car_Accessories_Controller.InitResponse.class);
    
    	system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
        system.assertEquals(availableAccessories.size(), response.lineItemOptions.size());
        
        // The accessories should be sorted alpha after any line items already assigned
        for (Integer i= 0; i < response.lineItemOptions.size(); i++){
            
            CC_STL_Manage_Car_Accessories_Controller.STL_Car_Accessory_Line_Item_Option option = response.lineItemOptions[i];
                
            if (i == 0){
                system.assertEquals(true, option.isSelected);
                system.assertEquals(newLineItem.Id, option.lineItemId);
                system.assertEquals(2, option.quantity);
            }
            else{
                system.assertEquals(false, option.isSelected);
                system.assertEquals(null, option.lineItemId);
                system.assertEquals(1, option.quantity);
            }
                                 
            system.assertEquals(stlCarInfo.Id, option.carInfoId);
            system.assertEquals(availableAccessories[i].Id, option.carAccessoryId);
            system.assertEquals(availableAccessories[i].Accessory_Type__c, option.accessoryName);           
            system.assertEquals(availableAccessories[i].Labor_Cost__c, option.unitLaborCost);
        }        
    }
    
    @isTest
    private static void testSaveAccessoriesFailure(){
        
        CC_STL_Manage_Car_Accessories_Controller.forceError = true;
        
        String responseStr = CC_STL_Manage_Car_Accessories_Controller.saveAccessories(null, null);
        LightningResponseBase response = (LightningResponseBase)JSON.deserialize(responseStr, LightningResponseBase.class);
        
        system.assertEquals(false, response.success);
        system.assertEquals('Test Exception', response.errorMessage);
    }
    
    @isTest
    private static void testSaveAccessoriesSuccess(){
        
        Product2 prod = [Select Id From Product2 Where Name = 'Club Car 1'];
        CC_STL_Car_Info__c stlCarInfo = [Select Id, Item__c, (Select Id, Quantity__c, Unit_Labor_Cost__c, STL_Car_Accessory__r.Accessory_Type__c From CC_STL_Car_Accessory_Line_Items__r Order By Accessory_Type__c ASC) From CC_STL_Car_Info__c Where Item__r.Name = 'Club Car 1'];
        system.assertEquals(0, stlCarInfo.CC_STL_Car_Accessory_Line_Items__r.size());
        
        CC_STL_Car_Accessory__c[] availableAccessories = [Select Id, Name, Accessory_Type__c, Labor_Cost__c 
                                                      From CC_STL_Car_Accessory__c 
                                                      Where Product__c = :stlCarInfo.Item__c
                                                      Order By Accessory_Type__c ASC];
        
        CC_STL_Manage_Car_Accessories_Controller.STL_Car_Accessory_Line_Item_Option[] lineItemOptions = new CC_STL_Manage_Car_Accessories_Controller.STL_Car_Accessory_Line_Item_Option []{};
            
        for (CC_STL_Car_Accessory__c accessory : availableAccessories){
        	lineItemOptions.add(new CC_STL_Manage_Car_Accessories_Controller.STL_Car_Accessory_Line_Item_Option(
                false, 
                null, 
                stlCarInfo.Id, 
                accessory.Id, 
                accessory.Accessory_Type__c,
                1, 
                accessory.Labor_Cost__c
            ));          
        } 
        
        // Select the first 2 accessories and change the quantity to 2
        lineItemOptions[0].isSelected = true;
        lineItemOptions[0].quantity = 2;
        lineItemOptions[1].isSelected = true;
        lineItemOptions[1].quantity = 2;
        
        // Send the options to the controller to save the selected accessories
        String responseStr = CC_STL_Manage_Car_Accessories_Controller.saveAccessories(stlCarInfo.Id, JSON.serialize(lineItemOptions));
        LightningResponseBase response = (LightningResponseBase)JSON.deserialize(responseStr, LightningResponseBase.class);
        
        system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
        
		stlCarInfo = [Select Id, Item__c, (Select Id, STL_Car__c, Quantity__c, Unit_Labor_Cost__c, STL_Car_Accessory__r.Accessory_Type__c From CC_STL_Car_Accessory_Line_Items__r Order By Accessory_Type__c ASC) From CC_STL_Car_Info__c Where Item__r.Name = 'Club Car 1'];
        system.assertEquals(2, stlCarInfo.CC_STL_Car_Accessory_Line_Items__r.size());

        for (Integer i = 0; i < stlCarInfo.CC_STL_Car_Accessory_Line_Items__r.size(); i++){
            
            CC_STL_Car_Accessory_Line_Item__c lineItem = stlCarInfo.CC_STL_Car_Accessory_Line_Items__r[i]; 
            
            system.assertEquals(lineItemOptions[i].carInfoId, lineItem.STL_Car__c);
            system.assertEquals(lineItemOptions[i].carAccessoryId, lineItem.STL_Car_Accessory__c);
            system.assertEquals(lineItemOptions[i].quantity, lineItem.Quantity__c);
            system.assertEquals(lineItemOptions[i].unitLaborCost, lineItem.Unit_Labor_Cost__c);
        }     
    }
}