public class exportContentVersionHistoryController{
    public List<ContentVersionHistory> cvhList{get;set;}
    public exportContentVersionHistoryController()
    {
      cvhList = new List<ContentVersionHistory>();
      //code contributed by @Priyanka Baviskar for issue no 7500029.
      for (ContentVersionHistory cvh : [select ContentVersionId, CreatedById, CreatedDate, Field, Id, IsDeleted, NewValue, OldValue from ContentVersionHistory 
                                        Where (Field='CC_Date__c' OR Field='CC_Deliverable_Type__c' OR Field='CC_Language__c' OR Field='Owner' OR Field='CC_Publication_Number__c' OR Field='Content_Title__c' OR Field='CC_Revision_Code__c' OR Field='CC_TechPubs_Status__c' OR Field='CC_TechPubs_Comments__c' OR Field='Description' OR Field='CC_Kit_Part_Number__c' OR Field='CC_SN_Prefix__c' OR Field='CC_Year__c') 
                                        AND ContentVersion.RecordType.Name ='Tech Pubs']){
           cvhList.add(cvh);
      }
    }
}