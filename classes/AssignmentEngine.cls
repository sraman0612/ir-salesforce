public class AssignmentEngine {
    //
    //Check if assignment owner has changed
    //
    
    public static void handleAssignment(sObject[] newList, Map<Id,sObject> oldMap)
    {
        Map<Integer,Id> queueIds = new Map<Integer,Id>();
        Set<String> counties = new Set<String>();
        
        Integer idx = 0;
        for (sObject cs : newList)
        {
            if(oldMap != null) {  
                if(cs.get('OwnerId') <> oldMap.get(cs.id).get('OwnerId')) {
                    queueIds.put(idx, (Id) cs.get('OwnerId'));
                }           
            }else {
                queueIds.put(idx, (Id) cs.get('OwnerId'));
            }
            counties.add((String) cs.get('county__c'));
            idx++;
        }
        System.debug('>>>>>queueIds: '+queueIds);
        if (queueIds.isEmpty()) return;
        
        //
        //Find active Assignment Group for Queue
        //
        Map<Integer,Id> asgnGroupNameIds = new Map<Integer,Id>();
        Map<Id,Assignment_Group_Queues__c> asgnGroupQueues = new Map<Id,Assignment_Group_Queues__c>(); //Queue ID --> Assignment Group Queues
        
        for(Assignment_Group_Queues__c[] agq : [SELECT Assignment_Group_Name__c, QueueId__c
                                              FROM Assignment_Group_Queues__c 
                                              WHERE QueueId__c in :queueIds.values()
                                              AND Active__c = 'True'])
        {
            for (Integer i = 0; i < agq.size() ; i++)
            {
                asgnGroupQueues.put(agq[i].QueueId__c, agq[i]);
            }                                           
        }
        System.debug('>>>>>asgnGroupQueues: '+asgnGroupQueues); 
        if (asgnGroupQueues.isEmpty()) return;
    
        for (Integer i : queueIds.keySet()) {
            Assignment_Group_Queues__c agq = asgnGroupQueues.get(queueIds.get(i));
            
            if (agq <> null) {
                asgnGroupNameIds.put(i, agq.Assignment_Group_Name__c);
            }
            //else no active assignment group queue error
        }
        System.debug('>>>>>asgnGroupNameIds: '+asgnGroupNameIds);
        if (asgnGroupNameIds.isEmpty()) return;
        
        Utility_MapChain groupChain = new Utility_MapChain(); // Assignment Group Name ID --> User ID
        for(Assignment_Groups__c ags : [SELECT Group_Name__c, User__c, State__c, County__c
                                   FROM Assignment_Groups__c 
                                   WHERE Group_Name__c IN :asgnGroupNameIds.values()
                                   AND county__c IN :counties])
        {
            String sState = ags.state__c == null ? null : ags.state__c.toUpperCase();
            String sCounty = ags.county__c == null ? null : ags.county__c.toUpperCase();
            groupChain.putData(new object[] {ags.Group_Name__c, sState, sCounty},ags.user__c);
        }
        
        for (Integer i : queueIds.keySet())
        {
            String sState = newList[i].get('state') == null ? null : ((String) newList[i].get('state')).toUpperCase();
            String sCounty = newList[i].get('county__c') == null ? null : ((String) newList[i].get('county__c')).toUpperCase();
            Id userToAssign = (Id) groupChain.getData(new object[]{asgnGroupNameIds.get(i),sState,sCounty});
            if (userToAssign != null)
                newList[i].put('ownerId',userToAssign);
        }
    }
}