/*********************************************************
 * Description    : Test Class for CC_ContactTriggerHandler
 * **********************************************************/


@isTest
private class CC_ContactTriggerHandler_Test {
  static testMethod void cc_contact_testownerupdate(){
    Id clubcarContactRTId = [SELECT Id FROM RecordType WHERE sObjectType='Contact' and DeveloperName='Club_Car'].Id;
    //Id clubcarAccountRTId = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='Club_Car'].Id;
    
      
     //creating state country custom setting data
     TestDataUtility.createStateCountries();  
      
      TestDataUtility testData = new TestDataUtility();
      
      //creating sales rep here
          CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1111');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
      
      
    account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
     acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        //acc.RecordtypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Club Car: Channel Partner').getRecordTypeId();
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
    insert acc;
    List<contact> conlist = new List<contact>();  
     Test.startTest(); 
    conlist.add(new Contact(firstname='cc',lastname='test',phone='5558675615',RecordTypeId=clubcarContactRTId,AccountId=acc.Id, Primary__c = True));
    conlist.add(new Contact(firstname='cc1',lastname='test1',phone='5558675615',RecordTypeId=clubcarContactRTId,AccountId=acc.Id)); 
     
    
    //Testing owner upate method here  
    insert conlist;
      
    //Testing updatePrimaryFlag method here  
    conlist[1].Primary__c = True;
    update conlist[1];  
      
     
    
    Test.stopTest();  
  }
    
    
    static testmethod void testMergeContacts(){
        Id clubcarContactRTId = [SELECT Id FROM RecordType WHERE sObjectType='Contact' and DeveloperName='Club_Car'].Id;
    //Id clubcarAccountRTId = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='Club_Car'].Id;
    
      
     //creating state country custom setting data
     TestDataUtility.createStateCountries();  
      
      TestDataUtility testData = new TestDataUtility();
      
      //creating sales rep here
          CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1111');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
      
      
    account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
     acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        //acc.RecordtypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Club Car: Channel Partner').getRecordTypeId();
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
    insert acc;
    List<contact> conlist = new List<contact>();  
     Test.startTest(); 
    conlist.add(new Contact(firstname='cc',lastname='test',phone='5558675615',RecordTypeId=clubcarContactRTId,AccountId=acc.Id, Primary__c = True));
    conlist.add(new Contact(firstname='cc1',lastname='test1',phone='5558675615',RecordTypeId=clubcarContactRTId,AccountId=acc.Id)); 
     
    
    //Testing owner upate method here  
    insert conlist;
        
        //Testing afterDelete Method here  
    merge conlist[0] conlist[1];
        Test.stopTest();  
    } 
}