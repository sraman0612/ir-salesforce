@isTest
public with sharing class CC_FinanceApprovalReminderTest {
    
    @testSetup 
    private static void setupData() {
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;
  		
  		CC_Order_Settings__c settings = CC_Order_Settings__c.getOrgDefaults();
  		settings.Trigger_Enabled__c = true;
  		settings.Process_Floorplan_Approvals__c = false;
  		settings.Floorplan_Approval_Record_Type_Names__c = 'CC_Cars_Ordering,CC_Parts_Ordering,CC_Used_Car_Order';
  		insert settings;
  		
  		Club_Car_Payment_Terms__c terms1 = TestDataUtility.createCCPaymentTerms('ABC1', 'Floorplan');
  		Club_Car_Payment_Terms__c terms2 = TestDataUtility.createCCPaymentTerms('ABC2', 'Cash');
  		insert new Club_Car_Payment_Terms__c[]{terms1, terms2};
  		
    	Account a = [Select Id From Account LIMIT 1];
    	
    	// Create an opportunity to run through approval
    	Id oppRecordTypeId = sObjectService.getRecordTypeId(Opportunity.sObjectType, 'NA Air');
    	Opportunity opp = TestDataUtility.createOpportunity(oppRecordTypeId, a.Id, 'TestOpp1', 'Target', Date.today().addDays(10));
    	opp.Request_Delete__c = true;
    	opp.Delete_Reason__c = 'testing';
    	opp.Delete_Approver__c = UserInfo.getUserId();
    	insert opp; 	
    	
    	// Create a few orders to work with
    	Id ccOrdRecordTypeId = sObjectService.getRecordTypeId(CC_Order__c.sObjectType, 'Cars Ordering');
    	
    	CC_Order__c ord1 = TestUtilityClass.createOrder(a.id);
    	ord1.CC_Order_Number_Reference__c = 'Order1';
        ord1.RecordTypeId = ccOrdRecordTypeId;
        ord1.Terms_Code__c = 'ABC1'; // Floorplan
        
    	CC_Order__c ord2 = TestUtilityClass.createOrder(a.id);
    	ord2.CC_Order_Number_Reference__c = 'Order2';
        ord2.RecordTypeId = ccOrdRecordTypeId;
        ord2.Terms_Code__c = 'ABC2'; // Cash
        
        insert new CC_Order__c[]{ord1, ord2};    
        
        TestDataUtility dataUtil = new TestDataUtility();
        
        User testUser = dataUtil.createIntegrationUser();
        insert testUser;
        
        system.runAs(testUser){
        	
			Folder fd = [Select Id From Folder Where Type = 'Email' LIMIT 1];       	
        	insert TestDataUtility.createEmailTemplate('Template Test1', 'Template_Test1', fd.Id, 'Test');  
        }		
    }  
    
    @isTest
    private static void testScheduleBatchEnabled(){
    	
    	CC_Batch_Controls__c settings = CC_Batch_Controls__c.getOrgDefaults();
    	settings.Finance_Reminder_Approval_Exclusions__c = null;
    	settings.Finance_Reminder_Template_Map__c = '{"CC_Order__c" : "Made_Up_Template_Name"}';
    	insert settings;
    	    	
    	Id jobId;
    	String schedule = '0 0 0 1 1 ? 2045';
    	
    	Test.startTest();
    	
    	jobId = System.schedule('Test_Finance_Approval_Reminder', schedule, new CC_FinanceApprovalReminder());
    	
    	Test.stopTest();
    	
    	// Verify the job is scheduled
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :jobID];
    	system.assertEquals(schedule, ct.CronExpression);
    	system.assertEquals(0, ct.TimesTriggered);
    	system.assertEquals('Test_Finance_Approval_Reminder', ct.CronJobDetail.Name);
    	system.assertEquals('7', ct.CronJobDetail.JobType);
    	
    	// Verify the batch job was executed
    	system.assertEquals(1, [Select Count() From AsyncApexJob Where ApexClass.Name = 'CC_FinanceApprovalReminder' and JobType = 'BatchApex']);
    }
    
    @isTest
    private static void testScheduleBatchDisabled(){
    	
    	CC_Batch_Controls__c settings = CC_Batch_Controls__c.getOrgDefaults();
    	settings.Finance_Reminder_Approval_Exclusions__c = null;
    	settings.Finance_Reminder_Template_Map__c = null;
    	insert settings;
    	
    	Id jobId;
    	String schedule = '0 0 0 1 1 ? 2045';
    	
    	Test.startTest();
    	
    	jobId = System.schedule('Test_Finance_Approval_Reminder', schedule, new CC_FinanceApprovalReminder());
    	
    	Test.stopTest();
    	
    	// Verify the job is scheduled
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :jobID];
    	system.assertEquals(schedule, ct.CronExpression);
    	system.assertEquals(0, ct.TimesTriggered);
    	system.assertEquals('Test_Finance_Approval_Reminder', ct.CronJobDetail.Name);
    	system.assertEquals('7', ct.CronJobDetail.JobType);
    	
    	// Verify the batch job was not executed
    	system.assertEquals(0, [Select Count() From AsyncApexJob Where ApexClass.Name = 'CC_FinanceApprovalReminder' and JobType = 'BatchApex']);
    }
    
    @isTest
    private static void testBatch(){
    	
    	CC_Batch_Controls__c settings = CC_Batch_Controls__c.getOrgDefaults();
    	settings.Finance_Reminder_Approval_Exclusions__c = null;
    	settings.Finance_Reminder_Template_Map__c = '{"CC_Order__c" : "Template_Test1"}';
    	settings.Finance_Reminder_Approval_Hours__c = 1;
    	insert settings;
    	
    	// Send orders into approval manually
    	CC_Order__c[] orders = [Select Id, CC_Order_Number_Reference__c From CC_Order__c Order By CC_Order_Number_Reference__c ASC];
    	system.assertEquals(2, orders.size());
    	
    	// Send orders into approval manually (opportunity should already be in approval)    	
		Approval.ProcessSubmitRequest approvalRequest1 = new Approval.ProcessSubmitRequest();
		approvalRequest1.setObjectId(orders[0].Id);
		approvalRequest1.setProcessDefinitionNameOrId('CC_Floorplan_Approval_Process');
		approvalRequest1.setSkipEntryCriteria(true);
		
		Approval.ProcessSubmitRequest approvalRequest2 = new Approval.ProcessSubmitRequest();
		approvalRequest2.setObjectId(orders[1].Id);
		approvalRequest2.setProcessDefinitionNameOrId('CC_Floorplan_Approval_Process');	
		approvalRequest2.setSkipEntryCriteria(true);

		sObjectService.processApprovalRequestsAndLogErrors(new Approval.ProcessSubmitRequest[]{approvalRequest1, approvalRequest2}, 'CC_FinanceApprovalReminderTest', 'testBatch');
		
		// Get the open approval instances (1 per actor/approver)
        ProcessInstanceWorkitem[] openApprovalInstances = [Select ActorId, Actor.Name, ProcessInstance.TargetObjectId, ElapsedTimeInMinutes From ProcessInstanceWorkitem];
		
		// Setup the batch class
		CC_FinanceApprovalReminder batch = new CC_FinanceApprovalReminder();
		batch.elapsedTimeInMinutesExpiration = 0; // Override the default threshold
		
		// Verify no emails have been sent yet
		system.assertEquals(0, EmailService.emailsSent);
		Id jobId;
		
		Test.startTest();
		
		// Fire the batch
		jobId = Database.executeBatch(batch);
		
		Test.stopTest();
		
		// Verify the batch completed successfully
		system.assertEquals('Completed', [Select Status From AsyncApexJob Where ApexClass.Name = 'CC_FinanceApprovalReminder' and JobType = 'BatchApex'].Status);
		
		// Verify an email was sent to each of the approvers
		if (EmailService.emailDeliverabilityEnabled){
			system.assertEquals([Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId = :orders[0].Id], EmailService.emailsSent);	
		}
		
		// Verify no errors were logged
		system.assertEquals(0, [Select Count() From Apex_Log__c WHERE method_name__c != 'submitOrdersForApproval']);
    }          
}