public without sharing class CC_KochLoadUpdateBatch implements Database.Batchable<CC_KochSoapAPI.UpdatedLoadInfo>, Database.AllowsCallouts { 
    
    private DateTime runTime = DateTime.now();

    public CC_KochSoapAPI.UpdatedLoadInfo[] start(Database.BatchableContext BC) {
          
        CC_KochSoapAPI.ArrayOfUpdatedLoadInfo kochLoadInfoResponse = new CC_KochSoapAPI.ArrayOfUpdatedLoadInfo();

        try{

            CC_KochSoapAPI.KochSOAPWebServiceSoap kochWS = new CC_KochSoapAPI.KochSOAPWebServiceSoap();

            kochWS.SecuredTokenHeader.AuthenticationToken = kochWS.AuthenticationUser();        

            CC_Koch_Settings__c kochSettings = CC_Koch_Settings__c.getOrgDefaults();

            kochLoadInfoResponse = kochWS.UpdatedLoadInfoRequest(kochSettings != null ? kochSettings.Last_Load_Update_Batch_Run_Date__c : null); 
        }
        catch(Exception e){            
            logError(e, e.getMessage(), 'start');
        }      
        
        return kochLoadInfoResponse.UpdatedLoadInfo != null ? kochLoadInfoResponse.UpdatedLoadInfo : new CC_KochSoapAPI.UpdatedLoadInfo[]{};
    } 

    public void execute(Database.BatchableContext BC, List<CC_KochSoapAPI.UpdatedLoadInfo> loads) {

        try{

            system.debug('loads: ' + loads.size());

            if (loads != null && loads.size() > 0){

                if (loads.size() == 1 && String.isNotBlank(loads[0].Error)){

                    if (loads[0].Error != CC_LoadTrackingUtilities.KOCH_LOAD_NOT_FOUND_MSG){
                        logError(null, loads[0].Error, 'execute');
                    }
                }
                else{
                    
                    // Build key map for car item association
                    Map<String, CC_KochSoapAPI.UpdatedLoadInfo> loadKeyMap = new Map<String, CC_KochSoapAPI.UpdatedLoadInfo>();

                    for (CC_KochSoapAPI.UpdatedLoadInfo loadInfo : loads){

                        if (String.isNotBlank(loadInfo.OrdRefnum)){
                            loadKeyMap.put(CC_KochUtilities.getKochCarItemUniqueKey(loadInfo.OrdRefnum), loadInfo);                    
                        }
                    }

                    system.debug('loadKeyMap: ' + loadKeyMap);

                    // Look for car item record matches based on the key returned from Koch
                    CC_Car_Item__c[] carItemsToUpdate = new CC_Car_Item__c[]{};

                    for (CC_Car_Item__c carItem : [Select Id, Car_Item_Key__c From CC_Car_Item__c Where Car_Item_Key__c in :loadKeyMap.keySet()]){

                        String key = carItem.Car_Item_Key__c;
                        CC_KochSoapAPI.UpdatedLoadInfo loadInfo = loadKeyMap.get(key);

                        system.debug('load found for carItem (' + key + '): ' + loadInfo);

                        if (loadInfo != null){

                            CC_KochUtilities.updateCarItemKochFields(
                                carItem, 
                                loadInfo.UTCLastUpdate, 
                                loadInfo.ScheduledPickupDateTime,
                                loadInfo.ActualPickupDateTime, 
                                loadInfo.ScheduledDeliveryDateTime, 
                                loadInfo.LatestCalculatedDeliveryDate, 
                                loadInfo.Distance,
                                loadInfo.ActualDeliveryDateTime,
                                loadInfo.TimeOffset,
                                loadInfo.LoadCode
                            );              

                            carItemsToUpdate.add(carItem);
                        }
                    }

                    system.debug('carItemsToUpdate: ' + carItemsToUpdate.size());

                    if (carItemsToUpdate.size() > 0){
                        sObjectService.updateRecordsAndLogErrors(carItemsToUpdate, 'CC_KochLoadUpdateBatch', 'execute');
                    }
                }
            }
        }
        catch(Exception e){
            logError(e, e.getMessage(), 'execute');
        }                
    }

    public void finish(Database.BatchableContext BC) {

        try{

            CC_Koch_Settings__c kochSettings = CC_Koch_Settings__c.getOrgDefaults();
            kochSettings.Last_Load_Update_Batch_Run_Date__c = runTime;
            upsert kochSettings;
        }
        catch(Exception e){
            logError(e, e.getMessage(), 'finish');
        }            
    }

    private void logError(Exception e, String error, String method){

        system.debug('exception during ' + method + ' method: ' + error);

        insert new Apex_Log__c(
            Class_Name__c = 'CC_KochLoadUpdateBatch',
            Method_Name__c = method,
            Message__c = error,
            Exception_Stack_Trace_String__c = (e != null ? e.getStackTraceString() : null)
        );
    }
}