@isTest
public with sharing class CC_Short_Term_Lease_AlertsControllerTest {

    @TestSetup
    private static void createData(){	
    	
    	TestDataUtility dataUtility = new TestDataUtility();
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;    	
        
        CC_Short_Term_Lease_Settings__c leaseSettings = CC_Short_Term_Lease_Settings__c.getOrgDefaults();
        leaseSettings.Restricted_Finance_Approval_Roles__c = 'CC_Finance_Leadership,CC_Finance_Team';
        insert leaseSettings;
    	
    	// Create users to use in the sales hiearchy
    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'xyz123';

    	User ccIntegrationUser = TestUtilityClass.createUser('CC_API_Profile', null);
    	ccIntegrationUser.Alias = 'xyz111';    

    	User salesRepUser1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser1.Alias = 'alias122';

    	User salesRepUser2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser2.Alias = 'alias222';      

    	User salesRepUser3 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser3.Alias = 'alias322';   

    	User salesRepUser4 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser4.Alias = 'alias422';                     
    	
    	User salesRepMgr1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr1.Alias = 'alias123';

    	User salesRepMgr2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr2.Alias = 'alias223';   

    	User salesRepMgr3 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr3.Alias = 'alias323';               
    	
    	User salesRepDir1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir1.Alias = 'alias124';

    	User salesRepDir2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir2.Alias = 'alias224';        
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    
    	
    	insert new User[]{
            salesRepUser1, salesRepUser2, salesRepUser3, salesRepUser4, 
            salesRepMgr1, salesRepMgr2, salesRepMgr3, 
            salesRepDir1, salesRepDir2, 
            salesRepVP, 
            adminUser, ccIntegrationUser
        };
    	
    	salesRepUser1.ManagerId = salesRepMgr1.Id;
    	salesRepMgr1.ManagerId = salesRepDir1.Id;
    	salesRepDir1.ManagerId = salesRepVP.Id;

    	salesRepUser2.ManagerId = salesRepMgr2.Id;
    	salesRepMgr2.ManagerId = salesRepDir2.Id;

        salesRepUser3.ManagerId = salesRepMgr3.Id;
    	
    	update new User[]{salesRepUser1, salesRepUser2, salesRepUser3, salesRepMgr1, salesRepMgr2, salesRepDir1};    	 

        // Create a lease user with a role to ensure sharing rules work and they have read/write access to the leases
        User leaseUser;

    	system.runAs(adminUser){	
    	
    		UserRole leaseRole = [Select Id From UserRole Where DeveloperName = 'CC_Aftermarket_Team' LIMIT 1];            
    	
	    	leaseUser = TestUtilityClass.createUser('CC_Pavilion Admin', null);
	    	leaseUser.FirstName = 'Lease1';
	    	leaseUser.LastName = 'User';
	    	leaseUser.Alias = 'xyz124';  
	    	leaseUser.UserRoleId = leaseRole.Id;

	    	insert leaseUser;

			PermissionSet leasePermSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Leases'];	    	
	    	
			PermissionSetAssignment permSetAssign = new PermissionSetAssignment(
				AssigneeId = leaseUser.Id,
				PermissionSetId = leasePermSet.Id
			);    	
			
			insert permSetAssign;	            
    	}        

    	CC_Sales_Rep__c salesRep1 = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser1.Id);
        CC_Sales_Rep__c salesRep2 = TestUtilityClass.createSalesRep('12345_abc124', salesRepUser2.Id);
        CC_Sales_Rep__c salesRep3 = TestUtilityClass.createSalesRep('12345_abc125', salesRepUser3.Id);
        CC_Sales_Rep__c salesRep4 = TestUtilityClass.createSalesRep('12345_abc126', salesRepUser4.Id);

        insert new CC_Sales_Rep__c[]{salesRep1, salesRep2, salesRep3, salesRep4};
    	
    	// Create Customer Accounts 
    	Account acct1 = TestUtilityClass.createAccount2();
    	acct1.BillingCountry = 'USA';
    	acct1.ShippingCountry = 'USA';
    	insert acct1;
    	
    	// Create Master Lease 
    	CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
        
        // Assign new Account to Master Lease
        ml1.Customer__c = acct1.Id;
        insert ml1;

    	CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
    	
        // Insert leases with different sales reps which have different manager hierarchies
        system.runAs(leaseUser){
        
            CC_Short_Term_Lease__c lease1 = TestDataUtility.createShortTermLease(null, null);
            lease1.Sales_Rep__c = salesRep1.Id;
            lease1.Sales_Rep_User__c = salesRepUser1.Id;
            lease1.Master_Lease__c = ml1.Id;
            leases.add(lease1);

            CC_Short_Term_Lease__c lease2 = TestDataUtility.createShortTermLease(null, null);
            lease2.Sales_Rep__c = salesRep2.Id;
            lease2.Sales_Rep_User__c = salesRepUser2.Id;
            lease2.Master_Lease__c = ml1.Id;
            leases.add(lease2);        

            CC_Short_Term_Lease__c lease3 = TestDataUtility.createShortTermLease(null, null);
            lease3.Sales_Rep__c = salesRep3.Id;
            lease3.Sales_Rep_User__c = salesRepUser3.Id;
            lease3.Master_Lease__c = ml1.Id;
            leases.add(lease3);           
            
            CC_Short_Term_Lease__c lease4 = TestDataUtility.createShortTermLease(null, null);
            lease4.Sales_Rep__c = salesRep4.Id;
            lease4.Sales_Rep_User__c = salesRepUser4.Id;
            lease4.Master_Lease__c = ml1.Id;
            leases.add(lease4);  

            insert leases;   
        }
    }    

    @isTest
    private static void testGetAlertsAllHierarchyAssigned(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];
        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias122'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Director__c);
        system.assertNotEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_VP__c);

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(0, response.alerts.size());        
        }

        Test.stopTest();        
    }

    @isTest
    private static void testGetAlertsNoManagerAssignedNotAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];
        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias422'];

        system.assertEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_Manager__c);
        system.assertEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_Director__c);
        system.assertEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_VP__c);

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(1, response.alerts.size());
            system.assertEquals('warning', response.alerts[0].type);
            system.assertEquals('dismissible', response.alerts[0].mode);
            system.assertEquals(30000, response.alerts[0].duration);
            system.assertEquals('Missing approvers in the sales hiearchy', response.alerts[0].title);
            system.assert(response.alerts[0].message.startsWith('There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a manager assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a director (Manager of Manager) assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a VP (Manager of Director) assigned to their user record.'));          
        }

        Test.stopTest();        
    }

    @isTest
    private static void testGetAlertsNoManagerAssignedAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];

        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias322'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_Director__c);
        system.assertEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_VP__c);

        lease.Sales_Rep_Manager__c = null;
        update lease;

        lease = [Select Id, Sales_Rep_Manager__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

        // Verifiy the manager is still unassigned after the update
        system.assertEquals(null, lease.Sales_Rep_Manager__c);        

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(1, response.alerts.size());
            system.assertEquals('warning', response.alerts[0].type);
            system.assertEquals('dismissible', response.alerts[0].mode);
            system.assertEquals(30000, response.alerts[0].duration);
            system.assertEquals('Missing approvers in the sales hiearchy', response.alerts[0].title);
            system.assert(response.alerts[0].message.startsWith('There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a manager assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a director (Manager of Manager) assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a VP (Manager of Director) assigned to their user record.'));   

            lease = [Select Id, Sales_Rep_Manager__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

            // Verify it assigned the missing manager
            system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        }

        Test.stopTest();        
    }    

    @isTest
    private static void testGetAlertsNoDirectorAssignedNotAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];
        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias322'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_Director__c);
        system.assertEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_VP__c);

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(1, response.alerts.size());
            system.assertEquals('warning', response.alerts[0].type);
            system.assertEquals('dismissible', response.alerts[0].mode);
            system.assertEquals(30000, response.alerts[0].duration);
            system.assertEquals('Missing approvers in the sales hiearchy', response.alerts[0].title);
            system.assert(response.alerts[0].message.startsWith('There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a manager assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a director (Manager of Manager) assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a VP (Manager of Director) assigned to their user record.'));          
        }

        Test.stopTest();        
    }    

    @isTest
    private static void testGetAlertsNoDirectorAssignedAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];

        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias222'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Director__c);
        system.assertEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_VP__c);

        lease.Sales_Rep_Director__c = null;
        update lease;

        lease = [Select Id, Sales_Rep_Director__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

        // Verifiy the director is still unassigned after the update
        system.assertEquals(null, lease.Sales_Rep_Director__c);        

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(1, response.alerts.size());
            system.assertEquals('warning', response.alerts[0].type);
            system.assertEquals('dismissible', response.alerts[0].mode);
            system.assertEquals(30000, response.alerts[0].duration);
            system.assertEquals('Missing approvers in the sales hiearchy', response.alerts[0].title);
            system.assert(response.alerts[0].message.startsWith('There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a manager assigned to their user record.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a director (Manager of Manager) assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a VP (Manager of Director) assigned to their user record.'));   

            lease = [Select Id, Sales_Rep_Director__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

            // Verify it assigned the missing director
            system.assertNotEquals(null, lease.Sales_Rep_Director__c);
        }

        Test.stopTest();        
    }        

    @isTest
    private static void testGetAlertsNoVPAssignedNotAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];
        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias222'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Director__c);
        system.assertEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertEquals(null, lease.Sales_Rep_VP__c);

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(1, response.alerts.size());
            system.assertEquals('warning', response.alerts[0].type);
            system.assertEquals('dismissible', response.alerts[0].mode);
            system.assertEquals(30000, response.alerts[0].duration);
            system.assertEquals('Missing approvers in the sales hiearchy', response.alerts[0].title);
            system.assert(response.alerts[0].message.startsWith('There are missing approvers in the sales hierarchy for the selected sales rep.  If sales will be required for approval, contact your system admin to have the sales rep manager and hierarchy updated.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a manager assigned to their user record.'));          
            system.assert(!response.alerts[0].message.contains('The sales rep selected does not have a director (Manager of Manager) assigned to their user record.'));          
            system.assert(response.alerts[0].message.contains('The sales rep selected does not have a VP (Manager of Director) assigned to their user record.'));          
        }

        Test.stopTest();        
    }        

    @isTest
    private static void testGetAlertsNoVPAssignedAvailable(){

        User leaseUser = [Select Id From User Where isActive = true and Alias = 'xyz124' Order By CreatedDate LIMIT 1];

        CC_Short_Term_Lease__c lease = [Select Id, 
                                            Sales_Rep_User__c, Sales_Rep_User__r.ManagerId,
                                            Sales_Rep_Manager__c, Sales_Rep_Manager__r.ManagerId,
                                            Sales_Rep_Director__c, Sales_Rep_Director__r.ManagerId,
                                            Sales_Rep_VP__c
                                        From CC_Short_Term_Lease__c Where Sales_Rep_User__r.Alias = 'alias122'];

        system.assertNotEquals(null, lease.Sales_Rep_User__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__c);
        system.assertNotEquals(null, lease.Sales_Rep_Manager__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_Director__c);
        system.assertNotEquals(null, lease.Sales_Rep_Director__r.ManagerId);
        system.assertNotEquals(null, lease.Sales_Rep_VP__c);

        lease.Sales_Rep_VP__c = null;
        update lease;

        lease = [Select Id, Sales_Rep_VP__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

        // Verifiy the VP is still unassigned after the update
        system.assertEquals(null, lease.Sales_Rep_VP__c);        

        Test.startTest(); 

        System.runAs(leaseUser){

            // Load the component
            CC_Short_Term_Lease_AlertsController.AlertsResponse response = CC_Short_Term_Lease_AlertsController.getAlerts(lease.Id);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
            system.assertEquals(0, response.alerts.size());

            lease = [Select Id, Sales_Rep_VP__c From CC_Short_Term_Lease__c Where Id = :lease.Id];         

            // Verify it assigned the missing VP
            system.assertNotEquals(null, lease.Sales_Rep_VP__c);
        }

        Test.stopTest();        
    }     
}