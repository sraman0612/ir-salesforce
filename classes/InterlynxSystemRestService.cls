/*******************************************************
* 
* Class : InterlynxSystemRestService
* Purpose : To receive the lead updates from Interlynx System 
* Developer : Manoj Bhujadi
* Organization : Cognizant
* 
********************************************************/
@RestResource(urlMapping='/InterlynxService/*')
global class InterlynxSystemRestService {
    global class Response {
        Boolean success;
        String message;
    }
    global class Payload {
        public String SalesforceLeadID;
        public String RSMID;
        public String LUID;
        public String Brand;
        public String LeadValue;
        public String FirstQuoteDate;
        public String DistributorOverallResponseTime;
        public String ChannelPartnerID ;
        public String ChannelPartnerSalesPerson;
        public String MiltonRoyChannelPartnerAccount;
        public String PartnerComments;
        public String SICDescription;
        public String NAICSDescription;
        public String LeadStatus;//Not required in excel
        public String ErrorAtInterlynx;
        public String Industry;
    }
    global class InsertResponse{
        Boolean success;
        String message;
        Id SalesforceLeadId;
        String RepLocatorId;
    }
    global class InsertPayload{
        public String RSMID;
        public String ChannelPartnerID;
        public String FirstName;
        public String LastName;
        public String Email;
        public String Phone;
        public String Title;
        public string Company;
        public String Street;
        public String City;
        public String County;
        public String State;
        public String PostalCode;
        public String Country;
        public String Industry;
        public String Description;
        public String LeadSource;
        public String Business;
        public String Brand;
        public String ProductType;
        public String ValueStream;
        public String RepLocatorId;
        Public String oppType;

    }
    @HttpPost
    global static List<InsertResponse> insertLeads(){
        RestRequest request = RestContext.request;
        RestResponse result = RestContext.response;
        List<LATAM_Region_Country__mdt> latamCountry = LATAM_Region_Country__mdt.getAll().values();//MPOB custom metadata
        Set<string> latamCountrySet  = new Set<String>();
        for(LATAM_Region_Country__mdt latCountry : latamCountry){
            latamCountrySet.add(latCountry.Country__c);
        }
        Group MiltonRoyQueue = [select Id FROM Group WHERE type = 'Queue' AND developerName = :System.Label.Milton_Roy_Queue LIMIT 1]; 
        Group MpobQueue = [select Id FROM Group WHERE type = 'Queue' AND  developerName = :System.Label.MP_OB_IRP_ROW_Queue LIMIT 1]; 
        Group mpobLATAMQueue = [select Id FROM Group WHERE type = 'Queue' AND  developerName = :System.Label.MP_OB_IRP_LATAM_Queue LIMIT 1]; 
        List<InsertPayload> insertNewLeads = (List<InsertPayload>) JSON.deserialize(request.requestbody.toString(), List<InsertPayload>.class);
        System.debug('Received JSON for Insert '+insertNewLeads);
        List<InsertResponse> responseList = new List<InsertResponse>();
        List<Lead> leadsToInsert = new List<Lead>();
        Set<String> rsmIdSet = new Set<String>();
        Set<String> distIdSet = new Set<String>();
        Set<String> mpobChannelPartnerSet = new Set<String>();
        Map<String, Assignment_Groups__c> rsmMap = new Map<String, Assignment_Groups__c> ();
        Map<String, Assignment_Groups__c> distMap = new Map<String, Assignment_Groups__c> ();
        Set<Id> accWithNoBRs = new Set<Id>();
        Map<String, String> latamLeads = new Map<String, String>();
        for(InsertPayload payload : insertNewLeads){
            if(String.isNotBlank(payload.ChannelPartnerID) && payload.Business == 'Milton Roy')
                distIdSet.add(payload.ChannelPartnerID);
            if(String.isNotBlank(payload.ChannelPartnerID) && payload.Business == 'MP/OB/IRP')
                mpobChannelPartnerSet.add(payload.ChannelPartnerID);
            if(String.isBlank(payload.ChannelPartnerID) && String.isNotBlank(payload.RSMID))
                rsmIdSet.add(payload.RSMID);
        }
        if(rsmIdSet.size() > 0){
            List<Assignment_Groups__c> rsmList = [Select RSMID__c, User__c FROM Assignment_Groups__c 
                                              WHERE Group_Name__c = :System.Label.RSM_Group_Name 
                                              AND RSMID__c IN :rsmIdSet LIMIT 1];
            for(Assignment_Groups__c loadRSMmap : rsmList)
                rsmMap.put(loadRSMmap.RSMID__c, loadRSMmap);
        }
        if(distIdSet.size() > 0){
            List<Assignment_Groups__c> distList = [Select DISTID__c, Distributor_Name__c, Distributor_Email__c,
                                               Distributor_Contact_Name__c, RSMID__c, User__c 
                                               FROM Assignment_Groups__c WHERE Group_Name__c = :System.Label.MR_Distributor_Trio 
                                               AND DISTID__c IN :distIdSet LIMIT 1];
            for(Assignment_Groups__c loadDistmap : distList)
                distMap.put(loadDistmap.DISTID__c, loadDistmap);
        }
        if(mpobChannelPartnerSet.size() > 0){
            List<Assignment_Groups__c> distList = [Select DISTID__c, Distributor_Name__c, Distributor_Email__c,
                                               Distributor_Contact_Name__c, RSMID__c, User__c, Queue_name__c 
                                               FROM Assignment_Groups__c 
                                                   WHERE GroupName__c = :System.Label.MP_OB_IRP_Distributor_Territory 
                                               AND DISTID__c IN :mpobChannelPartnerSet LIMIT 1];
            Set<Id> accountIds = new Set<Id>();
            for(Assignment_Groups__c loadDistmap : distList){
                distMap.put(loadDistmap.DISTID__c, loadDistmap);
                accountIds.add(loadDistmap.Distributor_Name__c);
            }
            List<Account> accList = [Select Id, MP_OB_IRP_Distributor__c, Status__c, RecordType.DeveloperName FROM Account WHERE Id IN :accountIds];
            for(Account acc : accList){
                if((acc.RecordType.DeveloperName == 'GD_Parent' || acc.RecordType.DeveloperName == 'GD_Parent_New') &&
                   acc.MP_OB_IRP_Distributor__c > 0 && acc.Status__c == 'Active'){
                       System.debug('Account has BR '+acc.Id);
                   }else {
                       System.debug('Account does not have BR');
                       accWithNoBRs.add(acc.Id);
                   }
            }
        }
        for(InsertPayload newLead : insertNewLeads){
            Lead thisLead = new Lead();
            thisLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Standard_Lead').getRecordTypeId();
            thisLead.FirstName = newLead.FirstName;
            thisLead.LastName = newLead.LastName;
            thisLead.Email = newLead.Email;
            thisLead.Phone = newLead.Phone;
            thisLead.Title = newLead.Title;
            thisLead.Company = newLead.Company;
            thisLead.Street = newLead.Street;
            if(newLead.City.length() > 40){
                thisLead.City = newLead.City.substring(0, 39);
            }
            else {
                thisLead.City = newLead.City;
            }
            thisLead.County__c = newLead.County;
            thisLead.State = newLead.State;
            thisLead.PostalCode = newLead.PostalCode;
            thisLead.Country = newLead.Country;
            thisLead.Industry = newLead.Industry;
            thisLead.Description = newLead.Description;
            thisLead.LeadSource = newLead.LeadSource;
            thisLead.Business__c = newLead.Business;
            if(newLead.Brand == 'Ingersoll Rand'){
                thisLead.VPTECH_Brand__c = 'Tuthill Pumps';
            }
            else{
                thisLead.VPTECH_Brand__c = newLead.Brand;
            }
            thisLead.CTS_Product_Type__c = newLead.ProductType;
            thisLead.CTS_Value_Stream__c = newLead.ValueStream;
            thisLead.Interlynx_RepLocatorId__c = newLead.RepLocatorId;
            if(String.isNotBlank(newLead.oppType))
              thisLead.Opportunity_Type__c = newLead.oppType;
            if(String.isNotBlank(newLead.ChannelPartnerID) && distMap.containsKey(newLead.ChannelPartnerID)){
                if(String.isNotBlank(distMap.get(newLead.ChannelPartnerID).User__c)){
                    thisLead.OwnerId = distMap.get(newLead.ChannelPartnerID).User__c;
                }
                else{
                    thisLead.OwnerId = distMap.get(newLead.ChannelPartnerID).Queue_name__c;
                }
                if (newLead.Business == 'MP/OB/IRP' && accWithNoBRs.contains(distMap.get(newLead.ChannelPartnerID).Distributor_Name__c)){
                    if(latamCountrySet.contains(newLead.Country)){
                        thisLead.OwnerId = mpobLATAMQueue.Id;
                        thisLead.Interlynx_Integration_Error__c = ':- Lead assigned to MP/OB/IRP LATAM Queue the received Channel Partner is either inactive or the Business Relationship on it is either not Distributor or not present. \n'+
                    									  'Channel Partner Id :'+newLead.ChannelPartnerID+'\n RSM Id : '+newLead.RSMID+' \n Country belongs to '+newLead.Country;
                    }
                    else{
                        thisLead.OwnerId = MpobQueue.Id;
                        thisLead.Interlynx_Integration_Error__c = System.today().format()+ ':- Lead assigned to MP/OB/IRP ROW Queue because the received Channel Partner is either inactive or the Business Relationship on it is either not Distributor or not present. \n'+
                    									  'Channel Partner Id :'+newLead.ChannelPartnerID+'\n RSM Id : '+newLead.RSMID;
                    }
                }
                else{
                    thisLead.RSM_ID__c = distMap.get(newLead.ChannelPartnerID).RSMID__c;
                	thisLead.Dist_ID__c = distMap.get(newLead.ChannelPartnerID).DISTID__c;
                	thisLead.Distributor_Contact_Name__c = distMap.get(newLead.ChannelPartnerID).Distributor_Contact_Name__c;
                	thisLead.Distributor_Email__c = distMap.get(newLead.ChannelPartnerID).Distributor_Email__c;
                	thisLead.Milton_Roy_Distribution_Account__c = distMap.get(newLead.ChannelPartnerID).Distributor_Name__c;
                	thisLead.Indirect_Lead__c = true;
                	thisLead.Ready_to_share_with_Interlynx__c = true;
                }   
            }
            else if(String.isNotBlank(newLead.RSMID) && rsmMap.containsKey(newLead.RSMID)){
                thisLead.RSM_ID__c = rsmMap.get(newLead.RSMID).RSMID__c;
                if(String.isNotBlank(rsmMap.get(newLead.RSMID).User__c))
                    thisLead.OwnerId = rsmMap.get(newLead.RSMID).User__c;
                else
                    thisLead.OwnerId = rsmMap.get(newLead.RSMID).Queue_Name__c;  
            }
            else{
                if(newLead.Business == 'Milton Roy'){
                    thisLead.OwnerId = MiltonRoyQueue.Id;
                    thisLead.Interlynx_Integration_Error__c = System.today().format()+ ':- Lead assigned to Milton Roy Leads Queue because received Channel Partner Id or RSM Id is not Present in Salesforce \n'+
                    									  'Channel Partner Id :'+newLead.ChannelPartnerID+'\n RSM Id : '+newLead.RSMID;
                }
                else if (newLead.Business == 'MP/OB/IRP'){
                    if(latamCountrySet.contains(newLead.Country)){
                        thisLead.OwnerId = mpobLATAMQueue.Id;
                        thisLead.Interlynx_Integration_Error__c = ':- Lead assigned to MP/OB/IRP LATAM Queue because received Channel Partner Id or RSM Id is not Present in Salesforce \n'+
                    									  'Channel Partner Id :'+newLead.ChannelPartnerID+'\n RSM Id : '+newLead.RSMID+' \n Country belongs to '+newLead.Country;
                    }
                    else{
                        thisLead.OwnerId = MpobQueue.Id;
                        thisLead.Interlynx_Integration_Error__c = System.today().format()+ ':- Lead assigned to MP/OB/IRP ROW Queue because received Channel Partner Id or RSM Id is not Present in Salesforce \n'+
                    									  'Channel Partner Id :'+newLead.ChannelPartnerID+'\n RSM Id : '+newLead.RSMID;
                    }
                }
            }
            leadsToInsert.add(thisLead);
        }
        Database.SaveResult[] insertResult = Database.insert(leadsToInsert, false);
        for (Integer i = 0; i < insertResult.size(); i++) {
            InsertResponse wrapper = new InsertResponse();
            wrapper.RepLocatorId = insertNewLeads[i].RepLocatorId;
            if (insertResult[i].isSuccess()) {
                wrapper.salesforceLeadId = insertResult[i].getId();
                wrapper.success = true;
                wrapper.message = 'Lead created successfully';
           } else {
               wrapper.success = false;
               for(Database.Error error : insertResult[i].getErrors()) {
                   wrapper.message = 'The following error has occurred for Lead with Id: ' + insertResult[i].getId() + '. ';
                   wrapper.message += error.getStatusCode() + ': ' + error.getMessage() + '. ';
                   wrapper.message += 'Lead fields that affected this error: ' + error.getFields();
               }
           }
           responseList.add(wrapper);
        }
        return responseList;
    }
    @HttpPatch
    global static List<Response> updateLeads() {
        RestResponse errorCode = RestContext.response;
        RestRequest request = RestContext.request;
        List<Payload> leadToBeUpdated = (List<Payload>) JSON.deserialize(request.requestbody.tostring(), List<Payload>.class);
        System.debug('Received JSON for Update '+leadToBeUpdated);
        List<Response> responseList = new List<Response>();
        List<Lead> newLeads = new List<Lead>();//To store leads for Database.update()
        //Set<Id> leadIdsWithNewDist = new Set<Id>(); //To get the set of Leads where Distributor id is modified
        Set<Id> leadIds = new Set<Id>();//To get the set of all Leads received from interlynx
        List<Interlynx_Lead_Status__mdt> InterlynxData = Interlynx_Lead_Status__mdt.getAll().values();
        Map<String, Interlynx_Lead_Status__mdt> InterlynxLeadStatus = new Map<String, Interlynx_Lead_Status__mdt>();
        for(Interlynx_Lead_Status__mdt status : InterlynxData ){
            InterlynxLeadStatus.put(status.Interlynx_Lead_Status__c, status);
        }
        for(Payload payload : leadToBeUpdated)
            leadIds.add(payload.SalesforceLeadID);
        Map<Id, Lead> leadMap = new Map<Id, Lead>([SELECT Id, OwnerId, Dist_ID__c, Interlynx_Integration_Error__c,
                                                   Partner_Comments__c, LUID__c, CurrencyIsoCode FROM Lead where Id IN :leadIds]);
        List<currencyType> currencyList = [Select conversionrate,isocode from CurrencyType];
        Map<String, Decimal> currencyMap = new Map<String, Decimal>();
        for(currencyType currenci : currencyList)
            currencyMap.put(currenci.isocode, currenci.conversionrate);

        for(Payload payload : leadToBeUpdated){
            Lead thisLead = new Lead();
            thisLead.Id = payload.SalesforceLeadID;
            if(String.isBlank(payload.ErrorAtInterlynx)){
                /*if(leadMap.containsKey(payload.SalesforceLeadID) && String.isNotBlank(payload.ChannelPartnerID) &&
                   leadMap.get(payload.SalesforceLeadID).Dist_ID__c != payload.ChannelPartnerID){
                       leadIdsWithNewDist.add(payload.SalesforceLeadID);
                       flag = true;
                }*/
                if(leadMap.containsKey(payload.SalesforceLeadID)){
                    if(String.isNotBlank(payload.LeadStatus)){
                        if(InterlynxLeadStatus.containsKey(payload.LeadStatus)){
                            thisLead.Status = InterlynxLeadStatus.get(payload.LeadStatus).Salesforce_Lead_Status__c;
                            thisLead.Disposition_Reason__c = InterlynxLeadStatus.get(payload.LeadStatus).Disposition_Reason__c;
                            if(payload.LeadStatus == 'Not My Lead'){
                                thisLead.OwnerId = leadMap.get(payload.SalesforceLeadID).OwnerId;
                                thisLead.Ready_to_share_with_Interlynx__c = false;
                                thisLead.Lead_Shared_To_Interlynx__c = false;
                                thisLead.RSM_ID__c = '';
                            }
                        }
                        else{
                            Response wrapper = new Response();
                            wrapper.success = false;
                            wrapper.message = 'The lead Status '+payload.LeadStatus+' is not mapped in Salesforce '+
                                'Kindly contact to Salesforce administrator';
                            responseList.add(wrapper);
                        }
                    }
                    if(String.isNotBlank(payload.LeadValue)){
                        if(leadMap.get(payload.SalesforceLeadID).CurrencyIsoCode == 'USD')
                            thisLead.AIRD_Closed_Won_Total__c = Decimal.valueOf(payload.LeadValue);
                        else{
                            thisLead.AIRD_Closed_Won_Total__c = currencyMap.get(leadMap.get(payload.SalesforceLeadID).CurrencyIsoCode) * Decimal.valueOf(payload.LeadValue);
                        }   
                    }
                    if(String.isNotBlank(payload.DistributorOverallResponseTime))
                        thisLead.Distributor_overall_response_time__c = Decimal.valueOf(payload.DistributorOverallResponseTime);
                    if(String.isNotBlank(payload.FirstQuoteDate))
                        thisLead.First_Quote_Date__c = Date.valueOf(payload.FirstQuoteDate); //yyyy-mm-dd                    
                    if(String.isBlank(leadMap.get(payload.SalesforceLeadID).LUID__c) || (String.isNotBlank(payload.LUID) && payload.LUID != leadMap.get(payload.SalesforceLeadID).LUID__c))
                        thisLead.LUID__c = payload.LUID; 
                    if(String.isNotBlank(payload.RSMID))
                        thisLead.RSM_Id__c = payload.RSMID; 
                    if(String.isNotBlank(payload.Brand)){
                        if(payload.Brand == 'Ingersoll Rand')
                            thisLead.VPTECH_Brand__c = 'Tuthill Pumps';
                        else
                            thisLead.VPTECH_Brand__c = payload.Brand;
                    }
                    /*if(String.isNotBlank(payload.ChannelPartnerID))
                        thisLead.Dist_ID__c = payload.ChannelPartnerID;*/
                    if(String.isNotBlank(payload.SICDescription))
                        thisLead.SIC_Description__c = payload.SICDescription;
                    if(String.isNotBlank(payload.NAICSDescription))
                        thisLead.NAICS_Description__c = payload.NAICSDescription;
                    if(String.isNotBlank(payload.Industry))
                        thisLead.Industry = payload.Industry;
					if(String.isNotBlank(payload.PartnerComments)){
                        if(String.isNotBlank(leadMap.get(payload.SalesforceLeadID).Partner_Comments__c))
                            thisLead.Partner_Comments__c = System.today().format()+ ':- \n'+payload.PartnerComments + '\n \n '+
                        								leadMap.get(payload.SalesforceLeadID).Partner_Comments__c;
                        else
                            thisLead.Partner_Comments__c = System.today().format()+ ':- \n'+payload.PartnerComments + '\n \n ';
                    }
                    newLeads.add(thisLead);
                }
                else{
                    Response wrapper = new Response();
                    wrapper.success = false;
                    wrapper.message = 'Salesforce Lead Id is Incorrect/Missing, Kindly Validate Lead ID';
                    responseList.add(wrapper);
                }
            }
            else{
                if(String.isNotBlank(payload.ErrorAtInterlynx)){
                    if(String.isNotBlank(leadMap.get(payload.SalesforceLeadID).Interlynx_Integration_Error__c)){
                        thisLead.Interlynx_Integration_Error__c = System.today().format()+ ':- \n'+payload.ErrorAtInterlynx+ '\n \n'+
                    									leadMap.get(payload.SalesforceLeadID).Interlynx_Integration_Error__c+ '\n \n';
                    }
                    else{
                        thisLead.Interlynx_Integration_Error__c = System.today().format()+ ':- \n'+payload.ErrorAtInterlynx+ '\n \n';
                    }
                }
                thisLead.Lead_Shared_To_Interlynx__c = false;
                thisLead.Ready_to_share_with_Interlynx__c = false;
                newLeads.add(thisLead);
            }
        }
        Database.SaveResult[] result = Database.update(newLeads, false);
        for(Database.SaveResult updateResult : result){
            Response wrapper = new Response();
            if (updateResult.isSuccess()) {
                wrapper.success = true;
                wrapper.message = 'Successfully updated Lead with Id: ' + updateResult.getId();
            }
            else{
                wrapper.success = false;
                for(Database.Error error : updateResult.getErrors()) {
                    wrapper.message = 'The following error has occurred for Lead with Id: ' + updateResult.getId() + '. ';
                    wrapper.message += error.getStatusCode() + ': ' + error.getMessage() + '. ';
                    wrapper.message += 'Lead fields that affected this error: ' + error.getFields();
                }
            }
            responseList.add(wrapper);
        }
        /*Confirmed by Julie and Interlynx team that Distributor ID will not be changed n Interlynx 
         * When there is situation of incorrect dist ID or assignment they will be using ‘Not my lead’ functionality 
         * to up date the lead.
         * 
         * if(flag){
            List<Lead> newRSMLead = new List<Lead>();//to modify RSM/Lead Owner
            List<Lead> leadsWithNewRSM = [SELECT Id, Dist_ID__c FROM Lead WHERE Id IN :leadIdsWithNewDist];
            Set<String> newDistId = new Set<String>();
            for(Lead item: leadsWithNewRSM ){
                newDistId.add(item.Dist_id__c);
            }
            Map<String, Assignment_Groups__c> getDistributorInfo = new Map<String, Assignment_Groups__c>();
            List<Assignment_Groups__c> grpMembers = [SELECT DistID__c, Distributor_Contact_Name__c, 
                                                     Distributor_Email__c,Distributor_Name__c, RSMId__c, 
                                                     User__C FROM Assignment_Groups__c WHERE DistID__c IN: newDistId LIMIt 1];
            for(Assignment_Groups__c grpMember : grpMembers){
                getDistributorInfo.put(grpMember.DistID__c, grpMember);
            }
            for(Lead item: leadsWithNewRSM){
                Lead thisLead = new Lead();
                if(getDistributorInfo.containsKey(item.Dist_ID__c)){
                    thisLead.Id = item.Id;
                    thisLead.RSM_ID__c = getDistributorInfo.get(item.Dist_ID__c).RSMID__c;
                    thisLead.OwnerId = getDistributorInfo.get(item.Dist_ID__c).User__C;
                    thisLead.Dist_ID__c = getDistributorInfo.get(item.Dist_ID__c).DistID__c;
                    thisLead.Distributor_Contact_Name__c = getDistributorInfo.get(item.Dist_ID__c).Distributor_Contact_Name__c;
                    thisLead.Distributor_Email__c = getDistributorInfo.get(item.Dist_ID__c).Distributor_Email__c;
                    thisLead.Milton_Roy_Distribution_Account__c = getDistributorInfo.get(item.Dist_ID__c).Distributor_Name__c;
                }
                if(!String.isBlank(thisLead.OwnerId))
                    newRSMLead.add(thisLead);
            }
            Database.SaveResult[] res = Database.update(newRSMLead, false);
        } */
        return responseList;
    }
    
}