/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CreateUserClassController {
    @RemoteAction
    webService static String checkCreteria(String jsonOutput, String filterLogic) {
        return null;
    }
    @RemoteAction
    global static List<KNEssentials.CreateUserClassController.customFieldInnerClass> search(String selOptionType, String objName) {
        return null;
    }
global class customFieldInnerClass {
    global customFieldInnerClass() {

    }
}
global class selectOptionClass {
    global selectOptionClass() {

    }
}
}
