//
// (c) 2015, Appirio Inc.
//
// Helper class for OpportunityStageIndicator inline vf
//
// Apr 14, 2015     Surabhi Sharma      Original
// Apr 29, 2015     Barkha Jain         S-311135: Enhancement to Opportunity Stage Indicator
//
public with sharing class OpportunityStageIndicatorHelper {
    
    public Opportunity                  opp                             {get;set;}
    public list<wrapperStage>           lstStages                       {get;set;}
    public Boolean                      hasQuotes                       {get;set;}
    public Boolean                      hasProposalAttached             {get;set;}
    public Boolean                      hasContacts                     {get;set;}
    public Boolean                      hasStakeHolders                 {get;set;}
    public map<String, Boolean>         taskStatusMap                   {get;set;}
    
    private static Map<String, String> mapSalesActivityToSubject {get;set;}
    static{
        mapSalesActivityToSubject = new Map<String, String>();
        mapSalesActivityToSubject.put('Technical Qualification Worksheet (Discover stage)', 'Tech Qual WS');
        mapSalesActivityToSubject.put('Intelli-Survey (Discover stage)', 'Intelli Survey');
        mapSalesActivityToSubject.put('Identify Potential Solutions (Discover stage)', 'Identify Potential Solutions');
        mapSalesActivityToSubject.put('Review Potenital Solutions (Verify stage)', 'Review Potenital Solutions');
        mapSalesActivityToSubject.put('Reviewed T&C\'s and timing of purchase decision (Propose/Quote stage)', 'Reviewed TCs');
        mapSalesActivityToSubject.put('Reviewed and Addressed all Customer inquiries (Negotiate stage)', 'Reviewed Customer Inquiries');
        mapSalesActivityToSubject.put('Obtained PO and all documentations needed to enter the orders (Negotiate stage)', 'Obtained PO');
    }
    
    // Constructor taking standard controller as a parameter
    public OpportunityStageIndicatorHelper(ApexPages.StandardController controller) {
        taskStatusMap = new map<String, Boolean>();
        for(String act : mapSalesActivityToSubject.values()){
            taskStatusMap.put(act, false);
        }
        
        if (!String.isBlank(controller.getId())) {
            opp = [SELECT Id, AccountId, Account.Contact__c, Amount, StageName, isClosed, CloseDate
                          , Confidence__c, Product_Offerings__c, Intelli_Survey__c, Potential_Solutions__c 
                          , Closed_Lost_Reason__c, Closed_Lost_Reason_Notes__c, Gathering_Data__c, Description  
                          ,Analyze_the_data_gathered__c                                         
                          ,(SELECT Id, Status, Subject FROM Tasks 
                                 WHERE Subject IN :mapSalesActivityToSubject.keySet())
                         ,(SELECT Id FROM Quotes__r)
                         ,(SELECT Id FROM Attachments)
                           FROM Opportunity
                           WHERE Id = :controller.getId()];             
            
            Boolean isCompleted;
            for(Task t : opp.Tasks){
                isCompleted = t.Status == 'Stage 5. Closed Won';
                taskStatusMap.put(mapSalesActivityToSubject.get(t.Subject), isCompleted);               
            } 
              
            hasQuotes = opp.Quotes__r.size() > 0;
            hasProposalAttached = opp.Attachments.size() > 0;
            Integer contactCount = 0;
            if(!String.isBlank(opp.AccountId))            
            {
                contactCount = [SELECT count() FROM Contact WHERE AccountId =:opp.AccountId];
            }
           
            hasContacts = contactCount > 0;
            hasStakeHolders = contactCount > 1;
            
            initializeStages();
        }
    }
    
    // Method to initialize stages to be displayed
    private void initializeStages(){
        list<String> pipelineStages = new list<String>{'Stage 1. Qualify',
                                                     'Stage 2. Discover/Verify',
                                                     'Stage 3. Propose/Quote',
                                                     'Stage 4. Negotiate',
                                                     'Account Management'};                                                 
      
        lstStages = new list<wrapperstage>();                                                     
        for (String stageName : pipelineStages) {
            lstStages.add(new wrapperstage(stageName));
        }

        for (Integer i = 0; i < lstStages.size(); i++) {
            if (opp.StageName.contains(lstStages[i].strStageName) 
                    || (opp.StageName == 'Stage 5. Closed Won' && lstStages[i].strStageName == 'Account Management')) {
                lstStages[i].bolCompleted = true;
                lstStages[i].bolCurrent = true;
                if (i > 1) {
                    lstStages[i - 1].bolCurrent = false;
                }
                break;
            } else if (opp.StageName == 'Stage 5. Closed Lost') { 
                lstStages[i].bolCompleted = false;
                break;
            } else {
                lstStages[i].bolCompleted = true;
            }
        }
    }
  
    // Method to save all changes
    public void  saveChanges(){
        update opp;
    }
  
    // Wrapper class 
    public class wrapperStage {
        public string strStageName {get;set;}
        public boolean bolCompleted {get;set;}
        public boolean bolCurrent {get;set;} 
        public wrapperStage(string y){
            strStageName = y; 
        }
    }   
}