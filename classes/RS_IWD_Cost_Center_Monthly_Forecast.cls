// Ashwini- with sharing - added
public with sharing class RS_IWD_Cost_Center_Monthly_Forecast{

    public String RECORD_TYPE_ID_NEW_BUSINESS { get; set; }
    
    public static final Map<Integer, String> monthNamesMap = new Map<Integer, String>{
                                                                    1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
                                                                    5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
                                                                    9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'};

    public static final string RECORD_TYPE_NEW_BUSINESS_IWD = 'IWD_New_Business';
    //Ashwini added Strategic Type Change 18-DEC-17
    public static final string RECORD_TYPE_STRATEGIC_IWD = 'Strategic_Opportunity_IWD';
    public static string RECORD_TYPE_ID_NEW_BUSINESS_IWD {get; set;}
    public static string RECORD_TYPE_ID_STRATEGIC_IWD {get; set;}
    List<sObject> pipelineAmounts;
    List<Opportunity_Revenue_Schedule__c> revScheds;
    
    // Static initializer
    static{
      RECORD_TYPE_ID_NEW_BUSINESS_IWD = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_NEW_BUSINESS_IWD AND SObjectType = 'Opportunity' LIMIT 1].Id;
      RECORD_TYPE_ID_STRATEGIC_IWD = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_STRATEGIC_IWD AND SObjectType = 'Opportunity' LIMIT 1].Id;
    }

    public RS_IWD_Cost_Center_Monthly_Forecast() {
        getOpportunityStageNames();
        getPipelineAmounts();
        getRevenueSchedules();
    }

       
    public String getCurrentMonthName() {
        Date now = Date.today();
        Integer currentMonth = now.month();
        // S.Colman: I'm changing this part of the code slightly to cover all months with just the line below
        String queryMonth = monthNamesMap.get(currentMonth);
        return queryMonth;
    }

    String revenueScheduleQueryS = 'SELECT Opportunity__r.Account.Name, Opportunity__r.Cost_Center_Name__r.OwnerId,Opportunity__r.AccountId,Opportunity__r.Name, Opportunity__r.Cost_Center_Name__c, '+
                                            'Opportunity__r.Sales_Budget__c, Opportunity__r.Remaining_Amount__c, Opportunity__r.Forecast__c, Opportunity__r.Cost_Center_Name__r.Name, ' +
                                            'Opportunity__r.StageName, Opportunity__r.Amount, Opportunity__r.ExpectedRevenue, ' +
                                            'Budget__c, Forecast_Adjustment__c, Id, Month__c, Year__c, Prior_Year_Sales__c, ' +
                                            'Multi_Family_NC_Actual__c, Multi_Family_NC_Budget__c, Multi_Family_Replacement_Actual__c, Multi_Family_Replacement_Budget__c, '+
                                            'Single_Family_NC_Actual__c, Single_Family_NC_Budget__c, Single_Family_NOO_Actual__c, Single_Family_NOO_Budget__c, '+
                                            'NOO_Actual__c, NOO_Budget__c '+
                                            'FROM Opportunity_Revenue_Schedule__c ';
    String revenueScheduleQueryW = 'WHERE Opportunity__r.Cost_Center_Name__r.OwnerId = \'' + UserInfo.getUserId() + '\' '; //AND Month__c = \'' + getCurrentMonthName() + '\'';
    String revenueScheduleQueryO = 'ORDER BY Opportunity__r.Cost_Center_Name__r.Name ASC, Month__c ASC';

    String pipelineQueryS = 'SELECT Account.Cost_Center_Name__c CostCenterId, SUM(Amount)summedAmount FROM Opportunity ';
   /* String pipelineQueryW = 'WHERE StageName NOT IN(\'Closed Won\',\'Closed Lost\') AND RecordType.Id = \'' + RECORD_TYPE_ID_NEW_BUSINESS_IWD + '\' ' 
                            + 'AND Account.Cost_Center_Name__r.OwnerId = \'' + UserInfo.getUserId() + '\' '; */
    String pipelineQueryW = 'WHERE StageName NOT IN(\'Closed Won\',\'Closed Lost\') AND (RecordType.Id = \'' + RECORD_TYPE_ID_NEW_BUSINESS_IWD + '\' OR RecordType.Id = \'' + RECORD_TYPE_ID_STRATEGIC_IWD + '\')' 
                            + 'AND Account.Cost_Center_Name__r.OwnerId = \'' + UserInfo.getUserId() + '\' ';                      
    String pipelineQueryO = 'GROUP BY Account.Cost_Center_Name__c';

    public List<Opportunity_Revenue_Schedule__c> getRevenueSchedules() {
        revScheds = Database.query(revenueScheduleQueryS + revenueScheduleQueryW + revenueScheduleQueryO);
        return revScheds;
    }
    
    
    

    public List<sObject> getPipelineAmounts() {
        pipelineAmounts = Database.Query(pipelineQueryS + pipelineQueryW + pipelineQueryO);
        
        system.debug(pipelineQueryS + pipelineQueryW + pipelineQueryO);
        return pipelineAmounts;
    }

    public List<String> getOpportunityStageNames() {
      List<String> stageNames = new List<String>();

       Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
       List<Schema.PicklistEntry> stagePickList = fieldResult.getPicklistValues();

       for ( Schema.PicklistEntry s : stagePickList)
       {
          String entry = '[\"' + s.getLabel() + '\",\"' + s.getValue() + '\"]';
          stageNames.add(entry);
       }
       return stageNames;
    }
}