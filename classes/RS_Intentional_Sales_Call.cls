public with sharing class RS_Intentional_Sales_Call {

    // Opportunity Record Type Developer Names
    public static final string RECORD_TYPE_NEW_BUSINESS = 'RS_Business';
    //Ashwini added Strategic Type Change 18-DEC-17
    public static final string RECORD_TYPE_STRATEGIC_DSO = 'Strategic_Opportunity_DSO';
    public static final string RECORD_TYPE_EXISTING_BUSINESS = 'RS_Existing_Business';
    public static string RECORD_TYPE_ID_NEW_BUSINESS {get; set;}
    public static string RECORD_TYPE_ID_STRATEGIC_DSO {get; set;}
    // Task Subjects
    public static string EMAIL {get; set;}
    // Account passed as parameter
    public Account currentAccount {get; set;}
    // Response tasks
    public transient List<Task> responseTasks {get; set;}
    // Development tasks
    public transient List<Task> devTasks {get; set;}
    // Sales Data
    public transient List<Opportunity_Revenue_Schedule__c> SalesData {get; set;}
    // Sales Data Sum
    public Decimal BudgetSum {get; set;}
    public Decimal ActualSum {get; set;}
    public Decimal PriorSalesSum {get; set;}
    // New Business Opprtunities
    public transient List<Opportunity> newBusinessOpportunities {get; set;}
    public transient List<Opportunity> newStrategicOpportunities {get; set;}
    //Account Notes
    public transient List<Note> AccountNotes {get; set;}
    // List with all the Account Initiative junction records
    public transient List<JunctionWrapper> junctionWrappers {get; set;}

    // Static initializer
    static{
      EMAIL = 'Email';
      RECORD_TYPE_ID_NEW_BUSINESS = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_NEW_BUSINESS AND SObjectType = 'Opportunity' LIMIT 1].Id;
      RECORD_TYPE_ID_STRATEGIC_DSO = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_STRATEGIC_DSO AND SObjectType = 'Opportunity' LIMIT 1].Id;
    }

    public RS_Intentional_Sales_Call() {
        currentAccount = [SELECT Id, Name, RS_Development_Plan__c FROM Account WHERE Id = :AccountId LIMIT 1];
        responseTasks = getResponseTaskList();
        devTasks = getDevTaskList();
        SalesData = getSalesData();
        BudgetSum = getBudgetSum();
        ActualSum = getActualSum();
        PriorSalesSum = getPriorSalesSum();
        newBusinessOpportunities = getNewBusinessOpportunityList();
        newStrategicOpportunities = getnewStrategicOpportunitiyList();
        AccountNotes = getAccountNotesList();
        junctionWrappers = getJunctionList();
    }

    String AccountId = ApexPages.currentPage().getParameters().get('id');

    String accountNameQuery = 'SELECT Name FROM Account WHERE Id = \'' + String.escapeSingleQuotes(AccountId) + '\' ';

    String responseTaskQueryS = 'SELECT AccountId, ActivityDate, Id, Description, LastModifiedDate, RS_Development_Plan__c, Status, Subject, WhatId ' +
                                'FROM Task ';
    String responseTaskQueryW = 'WHERE RS_Development_Plan__c = false' + ' AND AccountId = \'' + String.escapeSingleQuotes(AccountId) + '\'' +
                                'AND ( Status IN(\'deferred\',\'Not Started\',\'Waiting on someone else\') ' +
                                'OR (Status = \'Completed\' AND LastModifiedDate = LAST_N_DAYS:30) ) ';
    String responseTaskQueryO = 'ORDER BY ActivityDate DESC, Status ASC ' +
                                'LIMIT 10';

    String devTaskQueryS = 'SELECT AccountId, ActivityDate, Id, Description, LastModifiedDate, RS_Development_Plan__c, Status, Subject, WhatId ' +
                                'FROM Task ';
    String devTaskQueryW = 'WHERE RS_Development_Plan__c = true' + ' AND AccountId = \'' + String.escapeSingleQuotes(AccountId) + '\'' +
                                'AND ( Status IN(\'deferred\',\'Not Started\',\'Waiting on someone else\') ' +
                                'OR (Status = \'Completed\' AND LastModifiedDate = LAST_N_DAYS:14) ) ';
    String devTaskQueryO = 'ORDER BY ActivityDate DESC, Status ASC ' +
                                'LIMIT 10';
    
    String accNotesS = 'SELECT Id, Title, ParentId, OwnerId, Body FROM Note';
    
    String accNotesW = ' WHERE ParentId = \'' + String.escapeSingleQuotes(AccountId) + '\'';
    
    String salesDataS = 'SELECT Month__c, Budget__c, Actual__c, Prior_Year_Sales__c FROM Opportunity_Revenue_Schedule__c ';
    
    String salesDataW = ' where Opportunity__c in (SELECT Id FROM Opportunity where accountid = \'' + String.escapeSingleQuotes(AccountId) + '\'' + ' AND recordtype.developername  = \''
                         + RECORD_TYPE_EXISTING_BUSINESS + '\'' + ' AND Closedate = THIS_YEAR)';
    
    String salesDataO = ' Order by Month__c ASC';
    
    String BudgetSumS = 'SELECT SUM(Budget__c) SaBu FROM Opportunity_Revenue_Schedule__c ';
    
    String ActualSumS = 'SELECT SUM(Actual__c) SaAc FROM Opportunity_Revenue_Schedule__c ';
    
    String PriorSalesSumS = 'SELECT SUM(Prior_Year_Sales__c) SaPr FROM Opportunity_Revenue_Schedule__c ';
    
    String newBusinessQueryS = 'SELECT AccountId, Id, Name, Gross_Sales__c, Amount, StageName ' +
                               'FROM Opportunity ';
    String newBusinessQueryW = 'WHERE (RecordTypeId = \''+ RECORD_TYPE_ID_NEW_BUSINESS +'\' AND AccountId = \'' + String.escapeSingleQuotes(AccountId) + '\')' +
                               'AND (StageName NOT IN (\'Closed Won\', \'Closed Lost\'))';
    String newBusinessQueryU = 'WHERE (RecordTypeId = \''+ RECORD_TYPE_ID_STRATEGIC_DSO +'\' AND AccountId = \'' + String.escapeSingleQuotes(AccountId) + '\')' +
                               'AND (StageName NOT IN (\'Closed Won\', \'Closed Lost\'))';
   /* String newBusinessQueryW = 'WHERE ((RecordTypeId = \'' + RECORD_TYPE_ID_NEW_BUSINESS + '\' OR RecordTypeId = \'' + RECORD_TYPE_ID_STRATEGIC_DSO + '\') AND AccountId = \'' + String.escapeSingleQuotes(AccountId) + '\')' +
                               'AND (StageName NOT IN (\'Closed Won\', \'Closed Lost\'))'; */
                               
    String initiativesQueryS = 'SELECT ContentDocumentId, Description, Id, Title, Account_Initiative__c ' +
                                'FROM ContentVersion ';

    // Adding Date filters on the Account Initiative Junctions
    String initiativesQueryW = 'WHERE Account_Initiative__c IN ' +
                                    '(SELECT Key_Initiative__c FROM Account_Initiatives_Junction__c ' +
                                    'WHERE Account__c = \'' + String.escapeSingleQuotes(AccountId) + '\' AND Status__c = \'Open\' AND ' +
                                      'Key_Initiative__r.Start_Date__c < TODAY AND Key_Initiative__r.End_Date__c > TODAY) AND ' +
                                    'ContentVersion.Title != \'\' ';
    String initiativesQueryO = 'ORDER BY Account_Initiative__c DESC, CreatedDate ASC';

    String junctionsQueryS = 'SELECT Notes__c, Status__c, Key_Initiative__c FROM Account_Initiatives_Junction__c ';
    String junctionsQueryW = 'WHERE Account__c = \'' + String.escapeSingleQuotes(AccountId) + '\' AND Status__c = \'Open\' AND ' +
                              'Key_Initiative__r.Start_Date__c < TODAY AND Key_Initiative__r.End_Date__c > TODAY ';
    String junctionsQueryO = 'ORDER BY Key_Initiative__c DESC';


    public List<Task> getResponseTaskList() {
        List<Task> responseTasks = Database.query(responseTaskQueryS + responseTaskQueryW + responseTaskQueryO);
        return responseTasks;
    }

    public Integer getResponseTaskCount() {
        return responseTasks.size();
    }

    public List<Task> getDevTaskList() {
        List<Task> devTasks = Database.query(devTaskQueryS + devTaskQueryW + devTaskQueryO);
        return devTasks;
    }

    public Integer getDevTaskCount() {
        return devTasks.size();
    }

    public List<Opportunity_Revenue_Schedule__c> getSalesData() {
        List<Opportunity_Revenue_Schedule__c> SalesData = Database.query(salesDataS + salesDataW + salesDataO);
        return SalesData;
    }
    
    public Decimal getBudgetSum() {
        List<aggregateResult> AggBudgetSum = Database.query(BudgetSumS + salesDataW);
        BudgetSum = (Decimal)AggBudgetSum[0].get('SaBu');
        return BudgetSum;
    }
    
    public Decimal getActualSum() {
        List<aggregateResult> AggActualSum = Database.query(ActualSumS + salesDataW);
        ActualSum = (Decimal)AggActualSum[0].get('SaAc');
        return ActualSum;
    }
    
    public Decimal getPriorSalesSum() {
        List<aggregateResult> AggPriorSalesSum = Database.query(PriorSalesSumS + salesDataW);
        PriorSalesSum = (Decimal)AggPriorSalesSum[0].get('SaPr');
        return PriorSalesSum;
    }
    
    public List<Opportunity> getNewBusinessOpportunityList() {
        List<Opportunity> newBusiness = Database.query(newBusinessQueryS + newBusinessQueryW);
        return newBusiness;
    }

    public Integer getNewBusinessOpportunitiesCount() {
        return newBusinessOpportunities.size();
    }
    
    public List<Opportunity> getnewStrategicOpportunitiyList() {
        List<Opportunity> newStrategicBusiness = Database.query(newBusinessQueryS + newBusinessQueryU);
        return newStrategicBusiness;
    }

    public Integer getNewStrategicOpportunitiesCount() {
        return newStrategicOpportunities.size();
    }
    
    public List<Note> getAccountNotesList() {
        List<Note> AccountNotes = Database.query(accNotesS + accNotesW);
        return AccountNotes;    }
    
    
    public Integer getAccountNotesCount()  {
        return AccountNotes.size();
    }
    
    public String getAccountName() {
        return currentAccount != null ? currentAccount.Name : '';
    }

    public String getAccountPlan() {
        return currentAccount != null ? currentAccount.RS_Development_Plan__c : '';
    }

    public List<JunctionWrapper> getJunctionList() {
        List<ContentVersion> contentItems = getContentItems();
        List<Account_Initiatives_Junction__c> junctionRecords = getJunctions();
        Map<Id, JunctionWrapper> wrappersByInitiative = new Map<Id, JunctionWrapper>();
        for (Account_Initiatives_Junction__c aij : junctionRecords){
          if (wrappersByInitiative.get(aij.Key_Initiative__c) == null){
            wrappersByInitiative.put(aij.Key_Initiative__c, new JunctionWrapper(aij.Key_Initiative__c));
          }
          wrappersByInitiative.get(aij.Key_Initiative__c).junctionRecords.add(aij);
        }
        for (ContentVersion c : contentItems){
          if (wrappersByInitiative.get(c.Account_Initiative__c) != null){
            wrappersByInitiative.get(c.Account_Initiative__c).contentId = c.Id;
            wrappersByInitiative.get(c.Account_Initiative__c).contentTitle = c.Title;
          }
        }
        // Iterate over the wrappers to remove any of them that does not have a Content Id related
        Set<Id> allKeys = wrappersByInitiative.keySet();
        for (Id kiId : allKeys){
          if (wrappersByInitiative.get(kiId).contentId == null){
            wrappersByInitiative.remove(kiId);
          }
        }
        return wrappersByInitiative.values();
    }

    public List<ContentVersion> getContentItems() {
        List<ContentVersion> inits = Database.query(initiativesQueryS + initiativesQueryW + initiativesQueryO);
        return inits;
    }

    public Integer getKeyInitiativesCount() {
        return junctionWrappers.size();
    }

    public List<Account_Initiatives_Junction__c> getJunctions() {
        List<Account_Initiatives_Junction__c> keyInitiativeJunctions = Database.query(junctionsQueryS + junctionsQueryW + junctionsQueryO);
        return keyInitiativeJunctions;
    }

    public class JunctionWrapper {
        public Id keyInitiativeId {get; set;}
        public Id contentId {get; set;}
        public String contentTitle {get; set;}
        public List<Account_Initiatives_Junction__c> junctionRecords {get; set;}

        public JunctionWrapper(Id keyInitiative){
          keyInitiativeId = keyInitiative;
          junctionRecords = new List<Account_Initiatives_Junction__c>();
        }
    }
}