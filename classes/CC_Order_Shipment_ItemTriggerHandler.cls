public with sharing class CC_Order_Shipment_ItemTriggerHandler {

    public static Boolean forceError = false;
    public static CC_Load_Tracking_Settings__c settings = CC_Load_Tracking_Settings__c.getOrgDefaults();  

    public static void onAfterInsert(Map<Id, CC_Order_Shipment_Item__c> shipmentItems){

        if (settings != null && settings.Order_Shipment_Item_Trigger_Enabled__c){ 
            pushUpdatesToCarItems(shipmentItems, null);
        }
    }    

    public static void onAfterUpdate(Map<Id, CC_Order_Shipment_Item__c> shipmentItems, Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems){

        if (settings != null && settings.Order_Shipment_Item_Trigger_Enabled__c){
            pushUpdatesToCarItems(shipmentItems, oldShipmentItems);
        }
    }

    @TestVisible
    private class BackorderShipmentItem{

        public CC_Order_Shipment_Item__c shipmentItem;    
        public CC_Order_Item__c orderItem;  
        public CC_Car_Item__c carItem;  
        private Boolean isStackedOrder = false;

        public BackorderShipmentItem(CC_Order_Shipment_Item__c shipmentItem, CC_Order_Item__c orderItem, CC_Car_Item__c carItem, Boolean isStackedOrder){
            this.shipmentItem = shipmentItem;
            this.orderItem = orderItem;
            this.carItem = carItem;
            this.isStackedOrder = isStackedOrder;
        }

        public String getProductName(){return orderItem.Product__r.Name;}
        public Integer getTotalAccessoryQuantity(){return orderItem.CC_Quantity__c != null ? Integer.valueOf(orderItem.CC_Quantity__c) : 0;}
        public Integer getAccessoryShipQuantity(){return shipmentItem.Ship_Quantity__c != null ? Integer.valueOf(shipmentItem.Ship_Quantity__c) : 0;}
        public Integer getLoadCarQuantity(){return carItem.Truck_Quantity__c != null ? Integer.valueOf(carItem.Truck_Quantity__c) : 0;}
        public Integer getTotalCarQuantity(){return carItem.Order_Item__r.CC_Quantity__c != null ? Integer.valueOf(carItem.Order_Item__r.CC_Quantity__c) : 0;}
        public Integer getRatio(){return (getTotalCarQuantity() > 0 ? Integer.valueOf(getTotalAccessoryQuantity() / getTotalCarQuantity()) : 1);}
        public Integer getTotalExpectedShipQuantity(){return Integer.valueOf(getLoadCarQuantity() * getRatio());}
        public Integer getBackorderQuantity(){return isStackedOrder ? Integer.valueOf(shipmentItem.Backorder_Quantity__c) : (getTotalExpectedShipQuantity() - getAccessoryShipQuantity());}         
    }

    public static void pushUpdatesToCarItems(Map<Id, CC_Order_Shipment_Item__c> shipmentItems, Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems){

        system.debug('pushUpdatesToCarItems');

        try{

            // Get a list of all accessory backorder product codes to exclude
            Set<String> excludedBackorderAccessoryProductCodes = new Set<String>();

            if (settings != null && String.isNotBlank(settings.Excluded_Backorder_Accessory_Products__c)){

                for (String productCode : settings.Excluded_Backorder_Accessory_Products__c.split(',')){
                    excludedBackorderAccessoryProductCodes.add(productCode.trim());
                }
            }

            system.debug('excludedBackorderAccessoryProductCodes: ' + excludedBackorderAccessoryProductCodes);

            // Build a map of all parent order items and products
            Set<Id> orderItemIds = new Set<Id>();

            for (CC_Order_Shipment_Item__c shipmentItem : shipmentItems.values()){

                if (shipmentItem.Order_Item__c != null){
                    orderItemIds.add(shipmentItem.Order_Item__c);
                }
            }

            Map<Id, CC_Order_Item__c> orderItemMap = orderItemIds.size() > 0 ? new Map<Id, CC_Order_Item__c>(
                [Select Id, Order__c, CC_Quantity__c, Product__r.Name, Product__r.CC_Item_Class__c From CC_Order_Item__c Where Id in :orderItemIds]
            ) : new Map<Id, CC_Order_Item__c>();

            // Build a set of all orders in the transaction
            Set<Id> orderIds = new Set<Id>();

            for (CC_Order_Item__c orderItem : orderItemMap.values()){
                orderIds.add(orderItem.Order__c);
            }

            // Build a map of all car items by order Id and shipment header number
            Map<Id, Map<Decimal, CC_Car_Item__c>> carItemOrderIdShipmentHeaderMap = new Map<Id, Map<Decimal, CC_Car_Item__c>>();    
            Map<Id, Set<Id>> orderCarItemOrderItems = new Map<Id, Set<Id>>();        

            CC_Car_Item__c[] carItems = 
                [Select Id, Shipment_Header_Number__c, Truck_Quantity__c, Order_Item__r.Order__c, Order_Item__r.Product__r.Name, Order_Item__r.CC_Quantity__c 
                 From CC_Car_Item__c 
                 Where Order_Item__r.Order__c in :orderIds];

            for (CC_Car_Item__c carItem : carItems){

                // Needed for unique match between shipment items and car items on non-stacked orders
                if (carItemOrderIdShipmentHeaderMap.containsKey(carItem.Order_Item__r.Order__c)){
                    carItemOrderIdShipmentHeaderMap.get(carItem.Order_Item__r.Order__c).put(carItem.Shipment_Header_Number__c, carItem);
                }
                else{
                    carItemOrderIdShipmentHeaderMap.put(carItem.Order_Item__r.Order__c, new Map<Decimal, CC_Car_Item__c>{carItem.Shipment_Header_Number__c => carItem});
                }

                if (orderCarItemOrderItems.containsKey(carItem.Order_Item__r.Order__c)){
                    orderCarItemOrderItems.get(carItem.Order_Item__r.Order__c).add(carItem.Order_Item__c);
                }
                else{
                    orderCarItemOrderItems.put(carItem.Order_Item__r.Order__c, new Set<Id>{carItem.Order_Item__c});
                }
            }

            // Identify stacked orders, a stacked order is one where the car items are on different order items
            Set<Id> stackedOrderIds = new Set<Id>(); 

            for (Id orderId : orderCarItemOrderItems.keySet()){

                Set<Id> carItemOrderItems = orderCarItemOrderItems.get(orderId);

                if (carItemOrderItems.size() > 1){
                    stackedOrderIds.add(orderId);
                }
            }

            // Find all accessory shipment items on the orders in the transaction and identify all backorders
            BackorderShipmentItem[] backorderAccessoryShipmentItems = new BackorderShipmentItem[]{};

            for (CC_Order_Shipment_Item__c shipmentItem : 
                [Select Id, Shipment_Header_Number__c, Order_Item__c, Order_Item__r.Order__c, Order_Item__r.CC_Quantity__c, Order_Item__r.Product__r.Name, Backorder_Quantity__c, Ship_Quantity__c
                 From CC_Order_Shipment_Item__c 
                 Where Order_Item__r.Order__c in :orderIds and 
                    Order_Item__r.Product__r.CC_Item_Class__c != 'NEW' and 
                    Order_Item__r.Product__r.ProductCode not in :excludedBackorderAccessoryProductCodes and 
                    Order_Item__r.CC_Quantity__c >= 0]){

                system.debug('processing accessory shipmentItem: ' + shipmentItem);

                CC_Order_Item__c orderItem = shipmentItem.Order_Item__r;
                system.debug('orderItem: ' + orderItem);     

                Boolean isStackedOrder = stackedOrderIds.contains(orderItem.Order__c);
                system.debug('isStackedOrder: ' + isStackedOrder);

                Map<Decimal, CC_Car_Item__c> shipmentHeaderCarItemMap = carItemOrderIdShipmentHeaderMap.get(orderItem.Order__c);
                system.debug('shipmentHeaderCarItemMap: ' + shipmentHeaderCarItemMap);

                CC_Car_Item__c carItem;

                if (shipmentHeaderCarItemMap != null){                                   
                    carItem = shipmentHeaderCarItemMap.get(shipmentItem.Shipment_Header_Number__c);
                }
                
                system.debug('carItem: ' + carItem); 

                BackorderShipmentItem backorderShipmentItem = new BackorderShipmentItem(shipmentItem, orderItem, carItem, isStackedOrder);
                system.debug('accessory product name: ' + backorderShipmentItem.getProductName());

                if (isStackedOrder){                        

                    // A backorder on a stacked order is the actual backorder quantity on each shipment item
                    if (shipmentItem.Backorder_Quantity__c > 0){

                        system.debug('backorder change event found for shipment item on stacked order');
                        backorderAccessoryShipmentItems.add(backorderShipmentItem);                                
                    }
                }
                else if (carItem != null && carItem.Truck_Quantity__c != null){

                    // A backorder on a non-stacked order is one of the identified as one of the following criteria:
                    // 1) the ship quantity on the shipment item is less than the truck quantity on the car item
                    // 2) the ship quantity on the shipment item is greater than the truck quantity because of having multiple accessories per car but is still less than the total of the multiples * truck quantity                                                                
                                            
                    system.debug('totalAccessoryQuantity: ' + backorderShipmentItem.getTotalAccessoryQuantity());
                    system.debug('totalCarQuantity: ' + backorderShipmentItem.getTotalCarQuantity());

                    system.debug('accessoryShipQuantity: ' + backorderShipmentItem.getAccessoryShipQuantity());
                    system.debug('loadCarQuantity: ' + backorderShipmentItem.getLoadCarQuantity());
                    
                    system.debug('ratio: ' + backorderShipmentItem.getRatio());
                    system.debug('totalExpectedShipQuantity: ' + backorderShipmentItem.getTotalExpectedShipQuantity());
                    system.debug('backorderQuantity: ' + backorderShipmentItem.getBackorderQuantity());

                    if (backorderShipmentItem.getBackorderQuantity() > 0){

                        system.debug('backorder change event found for shipment item with backorderQuantity: ' + backorderShipmentItem.getBackorderQuantity());
                        backorderAccessoryShipmentItems.add(backorderShipmentItem);                                          
                    }
                }
            }

            system.debug('backorderAccessoryShipmentItems: ' + backorderAccessoryShipmentItems.size());

            // Go through each car item and build the backorder accessory details
            for (CC_Car_Item__c carItem : carItems){

                system.debug('processing carItem: ' + carItem);

                String backOrderedAccessories;

                Id carItemOrderId = carItem.Order_Item__r.Order__c;

                // Get all partial (i.e. some parts shipped) backorder accessories for this car item
                for (BackorderShipmentItem backOrderShipmentItem : backorderAccessoryShipmentItems){                    

                    if (backOrderShipmentItem.carItem.Id == carItem.Id){

                        system.debug('backorder match with car item: ' + backOrderShipmentItem);

                        CC_Order_Shipment_Item__c shipmentItem = backOrderShipmentItem.shipmentItem;

                        String detail = shipmentItem.Order_Item__r.Product__r.Name + ' - ' + backOrderShipmentItem.getBackorderQuantity();

                        system.debug('backorder detail: ' + detail);

                        if (String.isBlank(backOrderedAccessories)){
                            backOrderedAccessories = detail;
                        }
                        else{
                            backOrderedAccessories += '<br>' + detail;
                        }
                    }
                }

                carItem.Accessory_Backorder_Details__c = backOrderedAccessories;
            }

            // Find any accessories that have not shipped at all (i.e. no shipment item record)
            CC_Order_Item__c[] backorderAccessoryOrderItems= new CC_Order_Item__c[]{};

            system.debug('looking for accessories that have no shipments');

            CC_Order_Item__c[] unshippedAccessories = new CC_Order_Item__c[]{};

            for (CC_Order_Item__c orderItem : 
                [Select Id, Order__c, CC_Quantity__c, Product__r.Name, (Select Shipment_Header_Number__c From Order_Shipment_Items__r)
                 From CC_Order_Item__c 
                 Where Order__c in :orderIds and 
                    Product__r.ProductCode not in :excludedBackorderAccessoryProductCodes and 
                    Product__r.CC_Item_Class__c != 'NEW' and 
                    CC_Quantity__c >= 0]){
                 
                system.debug('orderItem: ' + orderItem);
                system.debug('accessory product name: ' + orderItem.Product__r.Name);

                // Go through each car item calculate the backorder quantity
                for (CC_Car_Item__c carItem : carItems){

                    if (carItem.Order_Item__r.Order__c == orderItem.Order__c){

                        Boolean isStackedOrder = stackedOrderIds.contains(orderItem.Order__c);
                        system.debug('isStackedOrder: ' + isStackedOrder);                        

                        Boolean unshippedOnCarItem = false;

                        // No shipments across all loads for this accessory
                        if (orderItem.Order_Shipment_Items__r.size() == 0){  
                            unshippedOnCarItem = true;
                        }
                        else{

                            // Check if there is a shipment for this accessory on this load
                            Boolean carItemShipmentFound = false;

                            for (CC_Order_Shipment_Item__c shipmentItem : orderItem.Order_Shipment_Items__r){

                                if (shipmentItem.Shipment_Header_Number__c == carItem.Shipment_Header_Number__c){
                                    carItemShipmentFound = true;
                                    break;
                                }
                            }

                            unshippedOnCarItem = !carItemShipmentFound;
                        }

                        system.debug('unshippedOnCarItem: ' + unshippedOnCarItem);

                        if (unshippedOnCarItem){

                            system.debug('processing carItem: ' + carItem);         
                            
                            Integer totalCarQuantity = 0;
                            
                            if (carItem.Order_Item__r.CC_Quantity__c != null){

                                if (isStackedOrder){

                                    // Must total the car quantity on all loads
                                    for (CC_Car_Item__c ci : carItems){

                                        if (ci.Order_Item__r.Order__c == orderItem.Order__c){
                                            totalCarQuantity += Integer.valueOf(ci.Order_Item__r.CC_Quantity__c);
                                        }
                                    }
                                }
                                else{
                                    totalCarQuantity = Integer.valueOf(carItem.Order_Item__r.CC_Quantity__c);
                                }
                            }

                            // If the accessory quantity is more than the total # of cars on the order
                            Integer ratio = totalCarQuantity > 0 ? Integer.valueOf(orderItem.CC_Quantity__c / totalCarQuantity) : 1;
                            Integer backorderQuantity = Integer.valueOf(carItem.Truck_Quantity__c * ratio);
                            String detail = orderItem.Product__r.Name + ' - ' + backorderQuantity;
                            
                            system.debug('ratio: ' + ratio);
                            system.debug('backorderQuantity: ' + backorderQuantity);
                            system.debug('backorder detail: ' + detail);

                            if (String.isBlank(carItem.Accessory_Backorder_Details__c)){
                                carItem.Accessory_Backorder_Details__c = detail;
                            }
                            else{
                                carItem.Accessory_Backorder_Details__c += '<br>' + detail;
                            }
                        }
                    }
                }
            }         

            sObjectService.updateRecordsAndLogErrors(carItems, 'CC_Order_Shipment_ItemTriggerHandler', 'pushUpdatesToCarItems');
        }
        catch(Exception e){

            system.debug('exception: ' + e.getMessage());

            insert new Apex_Log__c(
                Class_Name__c = 'CC_Order_Shipment_ItemTriggerHandler',   
                Method_Name__c = 'pushUpdatesToCarItems',
                Exception_Stack_Trace_String__c = e.getStackTraceString(),
                Message__c = e.getMessage()
            );
        }           
    }    
}