public with sharing class CC_Submit_Service_Contract_Approval_Ctrl {

    public class CC_Submit_Service_Contract_Approval_CtrlException extends Exception{}

    @AuraEnabled
    public static LightningResponseBase submitContractForApproval(Id contractId){

        LightningResponseBase response = new LightningResponseBase();

        try{
            
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setObjectId(contractId);
            approvalRequest.setProcessDefinitionNameOrId('Club_Car_Service_Contract_v2');					

            Approval.ProcessResult submissionResult = sObjectService.processApprovalRequestsAndLogErrors(new Approval.ProcessSubmitRequest[]{approvalRequest}, 'CC_Submit_Service_Contract_Approval_Ctrl', 'submitContractForApproval')[0];
            
            if (!submissionResult.isSuccess()){
                throw new CC_Submit_Service_Contract_Approval_CtrlException(submissionResult.getErrors()[0].getMessage());
            }
        }
        catch(Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}