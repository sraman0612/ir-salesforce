@isTest
public class CC_ProductItemTransactionHandlerTest {
  
  static testmethod void test1(){
    //create service terr
    Id uId = [SELECT Id FROM User Where isActive=TRUE and X18_Digit_User_ID__c!=null and Profile.Name LIKE 'CC_Sys%' LIMIT 1].Id;
    OperatingHours oh  = new OperatingHours(Name='test');
    insert oh;
    ServiceTerritory st = new ServiceTerritory(isActive=TRUE,Name='TestSvcTerr',Product_Transfer_Approver__c=uId,operatingHoursID=oh.Id);
    insert st;  
    //create warehouse 
    Schema.Location whse = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=FALSE,LocationType='Warehouse',Name='testWhse',
                                               TimeZone='America/New_York',Service_Territory__c=st.Id);
    insert whse;
    //create location 1
    Schema.Location loc = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc1',parentlocationId=whse.Id,TimeZone='America/New_York');
    insert loc;
    //create location 2
    Schema.Location loc2 = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc2',parentlocationId=whse.Id,TimeZone='America/New_York');
    insert loc2;
    //create product
    Product2 p = new Product2(name = 'testFSLProduct',Family = 'Club Car Parts');
    insert p;
    Pricebook2 pb = TestUtilityClass.createPriceBook2();
    insert pb;
    //create account
    Account a = TestUtilityClass.createAccount();
    a.ShippingCountry='USA';
    a.CC_Customer_Number__c ='9999920';
    a.Name='CLUB CAR DEPT EXP US';
    insert a;
    CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
    hcs.FS_No_Charge_Acct_Id__c=a.Id;
    hcs.FS_No_Charge_Pricebook_ID__c=pb.Id;
    upsert hcs CCProcessBuilderIDs__c.Id;
    //create work order
    WorkOrder wo = TestUtilityClass.createWorkOrder(a.id,'CC_Service');
    wo.Work_Order_status__c='Work Completed';
    wo.SLAM_Report__c='Completed';
    wo.Subject='Test';
    insert wo;
    //create productitem
    ProductItem pi = new ProductItem(LocationId=loc.Id, Product2id=p.Id, QuantityOnHand=5);
    insert pi;
    //create product consumed
    ProductConsumed pc = new ProductConsumed();
    pc.WorkOrderId = wo.Id;
    pc.QuantityConsumed=1;
    pc.ProductItemId = pi.Id;
    insert pc;
    //validate PIT gets updated with whse and loc
    ProductItemTransaction pit = [SELECT Source_Location__c,Source_Warehouse__c,Work_Order__c FROM ProductItemTransaction WHERE RelatedRecordId = :pc.Id];
    system.assertEquals('testLoc1', pit.Source_Location__c);
    system.assertEquals('testWhse', pit.Source_Warehouse__c);
    //system.assertEquals(wo.Id, pit.Work_Order__c);
    //create product transfer
    ProductTransfer pt  =  New ProductTransfer();
    pt.DestinationLocationId = loc2.Id;
    pt.Product2Id = p.Id;
    pt.QuantityReceived = 1;
    pt.QuantitySent = 1;
    pt.IsReceived = TRUE;
    pt.SourceLocationId = loc.Id;
    pt.Status = 'Completed';
    pt.Work_Order_lookup__c = wo.Id;
    insert pt;
    //validate PIT gets updated with whse and loc and work order
    ProductItemTransaction pit2 = [SELECT Source_Location__c,Source_Warehouse__c,Destination_Location__c,Destination_Warehouse__c,Work_Order__c 
                                    FROM ProductItemTransaction 
                                   WHERE RelatedRecordId = :pt.Id and Quantity > 0];
    system.assertEquals('testLoc1', pit2.Source_Location__c);
    system.assertEquals('testWhse', pit2.Source_Warehouse__c);
    system.assertEquals('testLoc2', pit2.Destination_Location__c);
    system.assertEquals('testWhse', pit2.Destination_Warehouse__c);
    system.assertEquals(wo.Id, pit2.Work_Order__c);
  }
}