public without sharing class PT_OutlookEventCtrl 
{
    @auraEnabled public static Map<String, Object> getContactStatuses(String[] emailAddresses, Id accountId)
    {
		return PT_OutlookEventService.getContactStatuses(emailAddresses, accountId);
    }
    
    @auraEnabled
    public static Map<String,Object> getEvent(String eventContextJson)
    {
		return PT_OutlookEventService.getEvent(eventContextJson);
    }
    
    @auraEnabled public static Map<String,Object> updateEvent(String eventContextJson)
    {
 		return PT_OutlookEventService.updateEvent(eventContextJson);
    }
    
    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds, String anOptionalParam) 
    {
		return PT_OutlookEventService.search(searchTerm, selectedIds, anOptionalParam);
    }

}