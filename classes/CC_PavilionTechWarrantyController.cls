public class CC_PavilionTechWarrantyController {

    public CC_Pavilion_Content__c HomePageText{get;set;}
    
    public CC_PavilionTechWarrantyController(CC_PavilionTemplateController controller) {
    HomePageText=[select CC_Body_1__c,CC_Body_2__c from CC_Pavilion_Content__c where  Title__c='Tech/Warranty Home Text' and  RecordType.Name='Home Page Text'];
    }
    
}