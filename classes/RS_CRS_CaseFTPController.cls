/*****************************************************************
Name:      RS_CRS_CaseFTPController
Purpose:   To Submit Case for FTP
History                                                            
-------                                                            
VERSION    AUTHOR      DATE      DETAIL
1.0      Neela Prasad  02/24/2018    INITIAL DEVELOPMENT
*****************************************************************/
public class RS_CRS_CaseFTPController {
    
    @AuraEnabled
    public static String updateConcessionStatus(String caseId)
    {
        try{
            //Getting Case Record Type Id through Schema.Describe Class
            Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
            
            // Dhilip added below Record Types as per case : 00412649
            Id PSIId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_PSI_Case_Record_Type);
            Id FSRId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_FSR_Case_Record_Type);
            
            //get Account ID based on case record ID
            Case caseRec = [Select Id, RS_CRS_Concession_Status__c
                            From Case 
                            Where  Id =: caseId AND (RecordTypeId =: caseRecId OR RecordTypeId =: PSIId OR RecordTypeId =: FSRId) limit 1];
            
            if(caseRec.RS_CRS_Concession_Status__c == 'Submitted for FTP'){
                return 'ALREADY_SUBMITTED';
            } else if(caseRec.RS_CRS_Concession_Status__c == 'Approved'){
                caseRec.RS_CRS_Concession_Status__c = 'Submitted for FTP';
                update caseRec;
                
                return 'SUBMITTED';
            } else {
                return 'NOT_APPROVED';
            }
            
        } catch(Exception ex){
            System.debug('Error occured while updating concession status in RS_CRS_CaseFTPController: ' + ex.getMessage() + ' at line# ' +ex.getLineNumber());
            return 'EXCEPTION';
        }
        
    }
}