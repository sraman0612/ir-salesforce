public class CC_PavilionCustomerViewHomeController {

 public String dates {get;set;}
 public String times {get;set;}
 public String contents {get;set;}
 public List<CC_Pavilion_Content__c> ListEveryWednesday{get;set;}
 public Set<String> TrainingEventTypeList{get;set;}
 public static CC_Pavilion_Content__c customerViewHeadingContent{get;set;}
 public static CC_Pavilion_Content__c customerViewBodyContent{get;set;}
 
  public string getCustomerVIEWURL(){
    String retStr=null==PavilionSettings__c.getInstance('CustomerVIEWEndpoint')?'#':PavilionSettings__c.getInstance('CustomerVIEWEndpoint').Value__c;
    return retStr;
  }
  
  public string getCCInsightURL(){
    String retStr=null==PavilionSettings__c.getInstance('CCInsightEndpoint')?'#':PavilionSettings__c.getInstance('CCInsightEndpoint').Value__c;
    return retStr;
  }
  
  public string getCustomerVIEWEMAIL(){
    String retStr=null==PavilionSettings__c.getInstance('customerviewEmailAddress')?'#':PavilionSettings__c.getInstance('customerviewEmailAddress').Value__c;
    return retStr;
  }  
  
  
 public CC_PavilionCustomerViewHomeController(CC_PavilionTemplateController controller) {
  customerViewHeadingContent =[select CC_Body_1__c  from CC_Pavilion_Content__c where  Training_Event_Type__c='Every Wednesday' and RecordType.Name = 'CustomerViewTrainingDates'];
  customerViewBodyContent = [select CC_Body_1__c  from CC_Pavilion_Content__c where  Training_Event_Type__c='Every Third Thursday' and RecordType.Name = 'CustomerViewTrainingDates'];   
  }
 public PageReference redirectToDiffPage() {
  Integer currentYear = System.Today().year();
  String calculatedDate = String.valueOf(currentyear) + '-' + dates;
  PageReference newPage = new PageReference('/apex/CC_PavillionCvTrainingReq?date=' + calculatedDate + '&time=' + times + '&contents=' + contents);
  return newPage;
 }
 @RemoteAction
 public Static Set<String> getEventType(){
     Set<String> getEvent=new Set<String>();
     for(CC_Pavilion_Content__c pc:[select id,Training_Event_Type__c,Training_Month_Value_In_Number__c,Training_Month__c,Training_Time__c from CC_Pavilion_Content__c where  RecordType.Name='CustomerViewTrainingDates']){
         getEvent.add(pc.Training_Event_Type__c);
     }
     return getEvent;
 }
 
 
 @RemoteAction
 public static List<String> getEveryWednesdayData(String EventType){
     Integer Month = Date.Today().Month();
     CC_Pavilion_Content__c pc=[select id,Training_Event_Type__c,Training_Month_Value_In_Number__c,Training_Month__c,Training_Time__c from CC_Pavilion_Content__c where  RecordType.Name='CustomerViewTrainingDates' and Training_Event_Type__c=:EventType];
     Attachment att=[select id,name,body from Attachment where parentID=:pc.id];
     Blob bodyBlob = att.Body;

     String bodyStr = bodyBlob.toString();

     List<String> parts= new List<String>();

     parts = bodyStr.split('\n');
     system.debug(parts.size());
     
    return parts;
     
 }
     public String getEveryThursdayData(){
       return null;  
     }
    public static string getCustomerVIEWHeading()
    {
      
      String HeadingText = customerViewHeadingContent.CC_Body_1__c ;
     return HeadingText.escapeEcmaScript();
    }
    
    
    public static string getCustomerVIEWBody()
    {
    
    String BodyText = customerViewBodyContent.CC_Body_1__c;
    return BodyText.escapeEcmaScript();
    }
}