global with sharing class CC_PavilionMarketingVideosCnt
{

    public CC_PavilionMarketingVideosCnt(CC_PavilionTemplateController controller) {

    }

    /*-------retrieves the list of categories of Marketing Videos ----- */        
    @RemoteAction
    public static list<selectOption> getVideoCategories()                         
    {
        list<selectOption> catOpt= new list<selectOption>();
        list<Schema.PicklistEntry> values =
            ContentVersion.Video_Category__c.getDescribe().getPickListValues();                  
        catOpt.add(new SelectOption('','--None--'));
        for (Schema.PicklistEntry a : values)
        { 
            catOpt.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return catOpt;
    }          
    /*-------retrieves the list of Marketing Videos ----- */      
    @RemoteAction
    global static List<ContentVersion> getVideoData(String VideoType)                   //
    {
        List<ContentVersion> contentList = new List<ContentVersion>();
        contentList = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name='Marketing Videos' AND Content_Title__c='Sales&Marketing' ];   
        return contentList ; 
    }
}