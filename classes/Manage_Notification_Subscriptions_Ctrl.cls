public with sharing class Manage_Notification_Subscriptions_Ctrl {

    @TestVisible
    private class InitResponse{
        @AuraEnabled public Boolean isPortalUser = false;
        @AuraEnabled public Id currentUserId = UserInfo.getUserId(); 
        @AuraEnabled public String mobilePhone = '';    
        @AuraEnabled public SubscriptionOption[] subscriptionOptions = new SubscriptionOption[]{};
        @AuraEnabled public sObject record;
        @AuraEnabled public String objectType;
    }

    @TestVisible
    public class SubscriptionOption{
        @AuraEnabled public Notification_Subscription__c subscription = new Notification_Subscription__c();
        @AuraEnabled public Notification_Type__c notificationType = new Notification_Type__c();   
        @AuraEnabled public Boolean subscribed = false;
        @AuraEnabled public Boolean unsubscribed = false;
        @AuraEnabled public Boolean optedOut = false;
        @AuraEnabled public String selectedDeliveryMethods = '';        
        @AuraEnabled public Boolean dailyEmailDigest = false;
        @AuraEnabled public Boolean weeklyEmailDigest = false;
        @AuraEnabled public Notification_Subscription__c overarchingSubscription;
    }

    @AuraEnabled
    public static InitResponse getInit(String recordId){

        InitResponse response = new InitResponse();

        User currentUser = 
            [Select Id, Name, AccountId, IsPortalEnabled, MobilePhone, Contact.MobilePhone
            From User 
            Where Id = :UserInfo.getUserId()];

        Id currentUserId = currentUser.Id;
        response.isPortalUser = currentUser.IsPortalEnabled;
        response.mobilePhone = currentUser.MobilePhone != null ? currentUser.MobilePhone : currentUser.Contact.MobilePhone; 

        if (String.isNotBlank(recordId)){

            response.objectType = String.valueOf(Id.valueOf(recordId).getSObjectType());

            String query = 'Select Id, Name';

            if (response.objectType == 'Account'){
                query += ', Account_Name_Displayed__c';
            }
            else if (response.objectType == 'Asset'){
                query += ', AccountId, Asset_Name_Displayed__c, Helix_Connectivity_Status__c';
            }            

            response.record = Database.Query(query + ' From ' + response.objectType + ' Where Id = :recordId');
        }

        // Get current subscriptions for the given record context (or global if no recordId)
        Map<Id, Notification_Subscription__c> notificationTypeCurrentSubscriptionMap = new Map<Id, Notification_Subscription__c>();

        for (Notification_Subscription__c subscription : NotificationService.getCurrentUserNotificationSubscriptions(recordId)){           
            notificationTypeCurrentSubscriptionMap.put(subscription.Notification_Type__c, subscription);
        }           

        Map<Id, Notification_Type__c> notificationTypesMap = new Map<Id, Notification_Type__c>();
        
        for (Notification_Type__c notificationType : NotificationService.getNotificationTypes()){
            notificationTypesMap.put(notificationType.Id, notificationType);
        }
        
        // Look for overarching subscriptions
        Map<Id, Notification_Subscription__c> notificationTypeOverarchingSubscriptionMap = new Map<Id, Notification_Subscription__c>();   

        if (String.isNotBlank(recordId)){

            Map<Id, Notification_Subscription__c[]> notificationTypeSubscriptionsMap = NotificationService.getSubscriptionsByNotificationTypes(notificationTypesMap.values(), currentUserId);

            for (Id notificationTypeId : notificationTypeSubscriptionsMap.keySet()){

                Notification_Type__c notificationType = notificationTypesMap.get(notificationTypeId);

                if (notificationType.Active__c && !notificationType.Mandatory__c){

                    Notification_Subscription__c currentSubscription = notificationTypeCurrentSubscriptionMap.get(notificationType.Id);

                    Notification_Subscription__c[] notificationTypeSubscriptions = notificationTypeSubscriptionsMap.get(notificationTypeId);

                    if (notificationTypeSubscriptions != null){

                        Set<Id> siteOptedOutNotificationTypeIds = new Set<Id>();

                        // Check grandparents (i.e. site) first                    
                        for (Notification_Subscription__c subscriptionToQualify : notificationTypeSubscriptions){

                            String subscriptionLevel = NotificationService.getSubscriptionLevel(subscriptionToQualify, notificationType);

                            if (subscriptionLevel == 'Grandparent' && (currentSubscription == null || subscriptionToQualify.Id != currentSubscription.Id)){
                                    
                                Map<String, Object> pouplatedFieldsMap = response.record != null ? response.record.getPopulatedFieldsAsMap() : new Map<String, Object>();
                                String grandparentField = notificationType.Parent_Object_Grandparent_Field__c;
                                
                                if (String.isNotBlank(grandparentField) && pouplatedFieldsMap.containsKey(grandparentField) && subscriptionToQualify.Record_ID__c == response.record.get(grandparentField)){

                                    if (subscriptionToQualify.Opt_Out__c){
                                        siteOptedOutNotificationTypeIds.add(notificationTypeId);
                                    }
                                    else{
                                        notificationTypeOverarchingSubscriptionMap.put(notificationTypeId, subscriptionToQualify);                                   
                                    }
                                }  
                            }
                        }                        

                        // Check global last
                        if (notificationTypeOverarchingSubscriptionMap.get(notificationTypeId) == null){

                            for (Notification_Subscription__c subscriptionToQualify : notificationTypeSubscriptions){

                                String subscriptionLevel = NotificationService.getSubscriptionLevel(subscriptionToQualify, notificationType);

                                if (subscriptionLevel == 'Global' && !siteOptedOutNotificationTypeIds.contains(notificationTypeId)){
                                    notificationTypeOverarchingSubscriptionMap.put(notificationTypeId, subscriptionToQualify);
                                    break;
                                }
                            }
                        }                                
                    }
                }
            }
        }

        for (Notification_Type__c notificationType : notificationTypesMap.values()){

            if (notificationType.Active__c && !notificationType.Mandatory__c){

                Notification_Subscription__c currentSubscription = notificationTypeCurrentSubscriptionMap.get(notificationType.Id);
                Notification_Subscription__c overarchingSubscription = notificationTypeOverarchingSubscriptionMap.get(notificationType.Id);
                Notification_Subscription__c subscriptionToUse = (currentSubscription != null && !currentSubscription.Opt_Out__c) ? currentSubscription : (currentSubscription == null && overarchingSubscription != null) ? overarchingSubscription : null;

                SubscriptionOption subscriptionOption = new SubscriptionOption();
                subscriptionOption.notificationType = notificationType;
                subscriptionOption.subscription = currentSubscription;
                subscriptionOption.subscribed = subscriptionToUse != null;
                subscriptionOption.unsubscribed = !subscriptionOption.subscribed;
                subscriptionOption.optedOut = currentSubscription != null ? currentSubscription.Opt_Out__c : false;
                subscriptionOption.overarchingSubscription = overarchingSubscription;

                response.subscriptionOptions.add(subscriptionOption);              
            }        
        }            

        return response;
    }

    public class UpdateSubscriptionsRequest{
        @AuraEnabled public String recordId;
        @AuraEnabled public SubscriptionOption[] subscriptionOptions;
    }

    @AuraEnabled
    public static LightningResponseBase updateSubscriptions(String updateSubscriptionsRequestStr){

        system.debug(updateSubscriptionsRequestStr);

        LightningResponseBase response = new LightningResponseBase();

        try{

            UpdateSubscriptionsRequest updateSubscriptionsRequest = (UpdateSubscriptionsRequest)JSON.deserialize(updateSubscriptionsRequestStr, UpdateSubscriptionsRequest.class);

            String objectType = '';

            if (String.isNotBlank(updateSubscriptionsRequest.recordId)){
                objectType = String.valueOf(Id.valueOf(updateSubscriptionsRequest.recordId).getSObjectType());        
            }           

            system.debug('objectType: ' + objectType); 

            Notification_Subscription__c[] subscriptionsToDelete = new Notification_Subscription__c[]{};
            Notification_Subscription__c[] subscriptionsToUpsert = new Notification_Subscription__c[]{};

            for (SubscriptionOption subscriptionOption : updateSubscriptionsRequest.subscriptionOptions){

                String deliveryFequencies = getDeliveryFrequencies(subscriptionOption);           

                if (!subscriptionOption.subscribed && !subscriptionOption.optedOut && subscriptionOption.subscription != null && subscriptionOption.subscription.Id != null){            
                    subscriptionsToDelete.add(subscriptionOption.subscription);
                }
                else if (subscriptionOption.subscribed || subscriptionOption.optedOut){                

                    if (subscriptionOption.subscription != null && subscriptionOption.subscription.Id != null){

                        subscriptionOption.subscription.Opt_Out__c = subscriptionOption.optedOut;
                        subscriptionOption.subscription.Delivery_Methods__c = subscriptionOption.optedOut ? null : subscriptionOption.selectedDeliveryMethods;
                        subscriptionOption.subscription.Delivery_Frequencies__c = subscriptionOption.optedOut ? null : deliveryFequencies; 
                        subscriptionsToUpsert.add(subscriptionOption.subscription);
                    }
                    else{

                        Boolean createNewSubscription = false;
                        
                        // Compare the delivery options to determine if a new subscription is needed or if the overarching subscription suffices
                        if (!subscriptionOption.optedOut){

                            if (subscriptionOption.overarchingSubscription != null && subscriptionOption.overarchingSubscription.Id != null){
                                
                                if (deliveryFequencies != subscriptionOption.overarchingSubscription.Delivery_Frequencies__c){
                                    createNewSubscription = true;
                                }
                                else if (subscriptionOption.selectedDeliveryMethods != subscriptionOption.overarchingSubscription.Delivery_Methods__c){
                                    createNewSubscription = true;
                                }
                            }
                            else{
                                createNewSubscription = true;
                            }
                        }
                        else{
                            createNewSubscription = true;
                        }

                        system.debug('createNewSubscription: ' + createNewSubscription);

                        if (createNewSubscription){

                            Notification_Subscription__c newSubscription = new Notification_Subscription__c();
                            newSubscription.Opt_Out__c = subscriptionOption.optedOut;
                            newSubscription.Notification_Type__c = subscriptionOption.notificationType.Id;
                            newSubscription.Delivery_Methods__c = subscriptionOption.optedOut ? null : subscriptionOption.selectedDeliveryMethods;
                            newSubscription.Delivery_Frequencies__c = subscriptionOption.optedOut ? null : deliveryFequencies; 
                            newSubscription.Record_ID__c = updateSubscriptionsRequest.recordId;                     
                            subscriptionsToUpsert.add(newSubscription);
                        }
                    }
                }
            } 

            system.debug('subscriptionsToDelete: ' + subscriptionsToDelete.size());

            if (subscriptionsToDelete.size() > 0){
                delete subscriptionsToDelete;
            }   

            system.debug('subscriptionsToUpsert: ' + subscriptionsToUpsert.size());

            if (subscriptionsToUpsert.size() > 0){
                upsert subscriptionsToUpsert;
            }          
        }
        catch(Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }

    private static String getDeliveryFrequencies(SubscriptionOption subscriptionOption){

        String deliveryFequencies = '';

        if (subscriptionOption.dailyEmailDigest){
            deliveryFequencies = 'Daily';
        }  

        if (subscriptionOption.weeklyEmailDigest){

            if (String.isBlank(deliveryFequencies)){
                deliveryFequencies = 'Weekly';
            }
            else{
                deliveryFequencies += ';Weekly';
            }
        }               

        if (String.isNotBlank(subscriptionOption.selectedDeliveryMethods)){

            if (String.isBlank(deliveryFequencies)){
                deliveryFequencies = 'Real-Time';
            }
            else{
                deliveryFequencies += ';Real-Time';
            }
        }  

        return deliveryFequencies;
    }
}