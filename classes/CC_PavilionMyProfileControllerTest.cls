@isTest
public class CC_PavilionMyProfileControllerTest {
    public static Account acc;
    public Static Contact con;
    public static Pavilion_Navigation_Profile__c pnp;
    public static CC_Pavilion_Content__c pavcon;
    public Static User PortalUser,AdminUser;
    public Static List<Contact> listCntDataProvider =new List<Contact>();
    
    static {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;
        
        pnp=new Pavilion_Navigation_Profile__c();
        pnp=TestUtilityClass.createPavilionNavigationProfile('US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN');
        pnp.Display_Name__c='US PRINCIPAL GM ADMIN';
        insert pnp;
        
        acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        System.assertEquals('Sample account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        
        con= TestUtilityClass.createContact(acc.id);
        con.CC_Pavilion_Admin__c=TRUE;
        insert con;
        System.assertNotEquals(null, con.Id);
        System.assertEquals(true,con.CC_Pavilion_Admin__c);
        
        PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        PortalUser.CC_Pavilion_Navigation_Profile__c='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN';
        PortalUser.Pavilion_Tiles__c='Sample;Tiles';
        insert PortalUser;
        System.assertNotEquals(null, PortalUser.Id);
        
        pavcon = TestUtilityClass.createPavilionContent();
        
        listCntDataProvider.add(con);
    }
    
    static testMethod void constructorTest() {
        Test.startTest();
        Pavilion_Navigation_Profile__c navProfile = new Pavilion_Navigation_Profile__c(
            Name = 'admin',
            Display_Name__c = 'test'
        );
        insert navProfile;
        
        Account account1 = TestUtilityClass.createAccount();
         account1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
         account1.name = 'Test';
         account1.CC_Customer_Number__c = '5621';
        insert account1;
        
        Contact contact1 = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN',
            FirstName = 'FS',
            AccountId = account1.Id,
            Email = 'test@test.ingersollrand.com',
            Title = 'test',
            CC_Pavilion_Admin__c = true
        );
        insert contact1;
        
        Contact contact2 = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN2',
            FirstName = 'FS2',
            AccountId = account1.Id,
            Email = 'test2@test.ingersollrand.com',
            Title = 'test2',
            CC_Pavilion_Admin__c = true
        );
        insert contact2;
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%CC_PavilionCommunityUser%' Limit 1];
        
        User testUser = new User(
            Username = System.now().millisecond() + 'test12345@test.ingersollrand.com',
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            CC_Pavilion_Navigation_Profile__c = 'admin'
        );
        insert testUser;
        
        System.runAs(testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionMyProfile'));
            CC_PavilionMyProfileController pavilionMyProfileController = new CC_PavilionMyProfileController(cc_PavilionTemplateController);  
            System.debug('data accId '+pavilionMyProfileController.accId);
            System.debug('data admins '+pavilionMyProfileController.admins);
            System.debug('data adminsEmail '+pavilionMyProfileController.adminsEmail);
            System.debug('data contactId' + pavilionMyProfileController.contactId);
            System.debug('data existing accounts '+ [select id from account]);
            System.debug('data existing contacts '+ [select id from contact]);
            System.debug('data existing user '+ testUser);
            //System.debug('data pavcontactId '+pavilionMyProfileController.pavcontactId);
            System.debug('data ' + [select id,Owner.Name  from Account where RecordType.DeveloperName = 'Club_Car']);
            //System.debug('data '+[Select FirstName,LastName, Email,Title, Department, Phone, MobilePhone, CC_Pavilion_Admin__c from Contact where Id =:testUser.ContactId]);
        }
        Test.stopTest();
    }
    
    static testmethod void testCC_PavilionMyProfileController() {
        System.runAs(PortalUser) {
            CC_PavilionTemplateController ctlr = new CC_PavilionTemplateController();
            CC_PavilionMyProfileController homecont = new CC_PavilionMyProfileController(ctlr);
            system.debug('****contactid***'+homecont.contactId);
            Id accId1=homecont.accId;
            homecont.contactFirstName='hadff';
            homecont.contactLastName='hadff';
            homecont.contactTitle='hadff';
            homecont.strcontactNumber='hadff';
            homecont.contactDepartment='hadff';
            homecont.strcontactCellNumber='sdad';
            
            //homecont.lstCntData=listCntDataProvider;
            //homecont.pavcontactId=listCntDataProvider;
            homecont.Putfullstop= 'test';
            homecont.PutComma= 'test';
            homecont.PutVariable= 'test';
            homecont.contactString= 'test';
            homecont.contactEmail='test@test.ingersollrand.com';
            ApexPages.currentPage().getParameters().put('firstName','test');
            ApexPages.currentPage().getParameters().put('lastName','test');
            ApexPages.currentPage().getParameters().put('Email','test@test.ingersollrand.com');
            ApexPages.currentPage().getParameters().put('Title','test');
            ApexPages.currentPage().getParameters().put('DepartMent','test');
            ApexPages.currentPage().getParameters().put('PhoneNo','9502571296');
            ApexPages.currentPage().getParameters().put('CellPhno','9502571296');
            ApexPages.currentPage().getParameters().put('UserName','testtest@tcs.com');  
            ApexPages.CurrentPage().getParameters().put('idVal',PortalUser.Id);
            homecont.getunselectedvalues();
            System.assertNotEquals(null,homecont.getunselectedvalues());
            homecont.getSelectedValues();
            System.assertNotEquals(null,homecont.getselectedvalues());
            homecont.save();            
            homecont.UpdateInformation();
            Set<Id> userId= new Set<id>();
            userId.add(homecont.usrId);
            Set<Id> contId= new Set<id>();
            contId.add(homecont.contactId);
            CC_PavilionMyProfileController.updateUser(userId,contId);
        }  
    }
    
    static testMethod void testCC_PavilionMyProfileController1() {
        test.StartTest();
        System.runAs(PortalUser) {
        CC_PavilionTemplateController ctlr = new CC_PavilionTemplateController();
        CC_PavilionMyProfileController homecont = new CC_PavilionMyProfileController(ctlr);
        ApexPages.currentPage().getParameters().put('firstName','');
        ApexPages.currentPage().getParameters().put('lastName','');
        ApexPages.currentPage().getParameters().put('Email','');
        ApexPages.currentPage().getParameters().put('Title','');
        ApexPages.currentPage().getParameters().put('DepartMent','');
        ApexPages.currentPage().getParameters().put('PhoneNo','');
        ApexPages.currentPage().getParameters().put('CellPhno','');
        ApexPages.currentPage().getParameters().put('UserName','');  
        homecont.UpdateInformation();
        
        ApexPages.CurrentPage().getParameters().put('idVal',userInfo.getUserId());
        }
        test.StopTest();           
    }
}