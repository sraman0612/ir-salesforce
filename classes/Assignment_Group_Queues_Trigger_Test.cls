//
// (c) 2015, Appirio Inc
//
// Test class for Assignment Group Queues Trigger
//
// Apr 28, 2015     George Acker     original
//
@isTest
private class Assignment_Group_Queues_Trigger_Test {

    static testmethod void testAGQTrigger()
    {
        Group q = new Group(name = 'Test Queue', developername = 'Test_Queue', type = 'Queue');
        Group q2 = new Group(name = 'Test Queue 2', developername = 'Test_Queue_2', type = 'Queue');
        insert new Group[]{q,q2};
        
        Assignment_Group_Name__c agn1 = new Assignment_Group_Name__c(name = 'Test AGN 1');
        Assignment_Group_Name__c agn2 = new Assignment_Group_Name__c(name = 'Test AGN 2');
        insert new sObject[]{agn1,agn2};
        
        Assignment_Group_Queues__c agq1 = new Assignment_Group_Queues__c(Assignment_Group_Name__c = agn1.Id, name = 'Test Queue');
        insert agq1;
        
        agq1 = [SELECT queueId__c,name FROM Assignment_Group_Queues__c WHERE Id = :agq1.id];
        System.assertEquals(q.Id,agq1.queueid__c);
        
        update agq1;
        
        Assignment_Group_Queues__c agq2 = new Assignment_Group_Queues__c(Assignment_Group_Name__c = agn2.Id, name = 'Test Queue');
        boolean iMustFail = false;
        try
        {
            insert agq2;
        }
        catch(Exception e)
        {
            iMustFail = true;
        }
        
        System.assert(iMustFail);
        
        agq2.name = 'I am pretty sure this name does not exist';
        iMustFail = false;
        try
        {
            insert agq2;
        }
        catch(Exception e)
        {
            iMustFail = true;
        }
        
        System.assert(iMustFail);
        
        agq1.name = 'Test Queue 2';
        update agq1;
        
        agq1 = [SELECT queueId__c,name FROM Assignment_Group_Queues__c WHERE Id = :agq1.id];
        System.assertEquals(q2.Id,agq1.queueid__c);
    }
}