@isTest
public with sharing class CC_RGA_Case_Return_Items_ControllerTest {
    
    public static Id ccCaseRecordTypeId = SObjectService.getRecordTypeId(Case.sObjectType, 'Club Car');
    
    @TestSetup 
    private static void setupData() {
    	
        TestDataUtility dataUtility = new TestDataUtility();
     
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;
        
        Account a = [Select Id From Account LIMIT 1];
        
        CC_Order__c ord1 = TestUtilityClass.createOrder(a.id);
        ord1.CC_Order_Number_Reference__c = 'Order1';
        
        CC_Order__c ord2 = TestUtilityClass.createOrder(a.id);
        ord2.CC_Order_Number_Reference__c = 'Order2';
        
        insert new CC_Order__c[]{ord1, ord2};   
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';
        
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';
        prod2.CC_Market_Type__c = 'Consumer';        
        
        insert new Product2[]{prod1, prod2};        
        
        CC_Order_Item__c orderItem1 = TestUtilityClass.createOrderItemWithProduct(ord1.Id, prod1.Id, 500);
        CC_Order_Item__c orderItem2 = TestUtilityClass.createOrderItemWithProduct(ord1.Id, prod2.Id, 500);
        
        insert new CC_Order_Item__c[]{orderItem1, orderItem2};
        
        CC_Car_Item__c carItem1 = TestUtilityClass.createCarItem(orderItem1.Id);
        carItem1.Serial_No_s__c = 'ABC123; ABC456;';
        
        CC_Car_Item__c carItem2 = TestUtilityClass.createCarItem(orderItem1.Id);
        carItem2.Serial_No_s__c = 'XYZ123; XYZ456;';    
        
        insert new CC_Car_Item__c[]{carItem1, carItem2}; 
        
        Case case1 = TestDataUtility.createCase(true, ccCaseRecordTypeId, null, a.Id, 'Test1', null);
        
        CC_Case_Order_Item__c caseOrderItem1 = TestUtilityClass.createCaseOrderItem(case1.Id, orderItem2.Id);  
        caseOrderItem1.Status__c = 'Waiting on Item';
        caseOrderItem1.Serial_Numbers__c = '';
        
        insert caseOrderItem1;
    }    
    
    
    private static testMethod void testGetOrderLineItems(){
    	
    	Case c = [Select Id From Case Where RecordTypeId = :ccCaseRecordTypeId];
    	CC_Order__c o = [Select Id From CC_Order__c Where CC_Order_Number_Reference__c = 'Order1'];
    	    	
    	Test.startTest();
    	
    	CC_RGA_Case_Return_Items_Controller.OrderLineItemResponse response = CC_RGA_Case_Return_Items_Controller.getOrderLineItems(c.Id, o.Id);
    	
    	system.assertEquals(2, response.lineItems.size());
    	
    	for (CC_RGA_Case_Return_Items_Controller.OrderLineItemSelection lineItem : response.lineItems){
    		
    		system.assertEquals(o.Id, lineItem.caseOrderItem.Order_Item__r.Order__c);
    		system.assertEquals(c.Id, lineItem.caseOrderItem.Case__c);
    		
    		if (lineItem.caseOrderItem.Id == null){
    			system.assertEquals(false, lineItem.selected);
    			system.assertEquals(null, lineItem.caseOrderItem.Status__c);
    			system.assertEquals(4, lineItem.serialNumberOptions.size());
    		}
    		else{
    			system.assertEquals(true, lineItem.selected);
    			system.assertEquals('Waiting on Item', lineItem.caseOrderItem.Status__c);
    			system.assertEquals(0, lineItem.serialNumberOptions.size());
    		}
    	}
    	
    	Test.stopTest();
    }
    
    private static testMethod void testUpdateSelections(){
    	
    	Case c = [Select Id From Case Where RecordTypeId = :ccCaseRecordTypeId];
    	CC_Order__c o = [Select Id From CC_Order__c Where CC_Order_Number_Reference__c = 'Order1'];
    	CC_Order_Item__c oi = [Select Id From CC_Order_Item__c Where Order__c = :o.Id LIMIT 1];
    	CC_Case_Order_Item__c caseOrderItem1 = [Select Id, Case__c, Order_Item__r.Order__c, Status__c, Serial_Numbers__c, Order_Item__r.RecordType.Name, Order_Item__r.Product__r.Name, Order_Item__r.Product__r.CC_Market_Type__c From CC_Case_Order_Item__c];
    	    	
    	system.assertEquals('Waiting on Item', caseOrderItem1.Status__c);    	
    	    	
    	// Make a change
    	caseOrderItem1.Status__c = 'Received at Dock';    	
    	    	
    	// Create another non-saved case order item
        CC_Case_Order_Item__c caseOrderItem2 = TestUtilityClass.createCaseOrderItem(c.Id, oi.Id);  
        caseOrderItem2.Status__c = 'Waiting on Item';
        caseOrderItem2.Serial_Numbers__c = 'ABC';    	        
    	    	
    	Test.startTest();
    	
    	LightningResponseBase response = CC_RGA_Case_Return_Items_Controller.updateSelections(c.Id, o.Id, new CC_Case_Order_Item__c[]{caseOrderItem1, caseOrderItem2});
    	
    	system.assertEquals(true, response.success);
    	
    	// Verify the records were updated and saved
    	caseOrderItem1 = [Select Id, Case__c, Order_Item__r.Order__c, Status__c, Serial_Numbers__c, Order_Item__r.RecordType.Name, Order_Item__r.Product__r.Name, Order_Item__r.Product__r.CC_Market_Type__c From CC_Case_Order_Item__c Where Id = :caseOrderItem1.Id];		
		system.assertEquals('Received at Dock', caseOrderItem1.Status__c);	

    	caseOrderItem2 = [Select Id, Case__c, Order_Item__r.Order__c, Status__c, Serial_Numbers__c, Order_Item__r.RecordType.Name, Order_Item__r.Product__r.Name, Order_Item__r.Product__r.CC_Market_Type__c From CC_Case_Order_Item__c Where Id != :caseOrderItem1.Id];		
		system.assertEquals('Waiting on Item', caseOrderItem2.Status__c);
		system.assertEquals('ABC', caseOrderItem2.Serial_Numbers__c);	
    	
    	Test.stopTest();
    }    
}