@isTest
public class CTS_TechDirect_TaxonomyControllerTest {    
    @testSetup     
        public static void loadData() {
        User u3 = [SELECT Id FROM User WHERE UserRole.Name LIKE 'CTS TechDirect%' and profile.name like '%Admin%' and isactive = true and UserPermissionsKnowledgeUser = true limit 1];
        System.runAs(u3) 
        {            
        Knowledge__kav knowledgeObj = new Knowledge__kav();
        knowledgeObj.RecordTypeId = '0120a0000011AllAAE';
        knowledgeObj.ValidationStatus = 'Private';
        knowledgeObj.Title = 'Test Article';
        knowledgeObj.UrlName = 'Test' + String.valueOf(System.currentTimeMillis());
        knowledgeObj.Summary = 'Test Summary';
        knowledgeObj.CTS_TechDirect_Article_Type__c = 'Tech Note';
        knowledgeObj.CTS_TechDirect_Question__c = 'Test Question';
        knowledgeObj.CTS_TechDirect_Answer__c = 'Test Question';
        knowledgeObj.CTS_TechDirect_Access_Level__c = 'Standard Products';
        knowledgeObj.IsVisibleInPkb=true;
        knowledgeObj.IsVisibleInCsp=true;
        knowledgeObj.IsVisibleInPrm =true;
        knowledgeObj.language='en_US';
        knowledgeObj.CTS_TechDirect_Author_Reviewer__c = u3.Id;
        insert knowledgeObj;
    
        System.debug('### knowledgeObj Id ' + knowledgeObj.Id);
        System.assertNotEquals(knowledgeObj, null);
        System.assertNotEquals(knowledgeObj.Id, null);
       
        knowledgeObj = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledgeObj.Id limit 1];
        
        DescribeDataCategoryGroupResult[] results = 
            Schema.describeDataCategoryGroups(new String[] { 'KnowledgeArticleVersion'}); 
        
        //Creating a list of pair objects to use as a parameter for the describe call
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
            
        //Looping throught the first describe result to create the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : results){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);
        }    
            
        Knowledge__DataCategorySelection dataCategoryObj = new Knowledge__DataCategorySelection();
        dataCategoryObj.ParentId = knowledgeObj.Id;
        dataCategoryObj.DataCategoryGroupName = results[0].getName();   
        
        DescribeDataCategoryGroupStructureResult[] singleResult = Schema.describeDataCategoryGroupStructures(pairs, false);
        dataCategoryObj.DataCategoryName = singleResult[0].getTopCategories()[0].getName();
        insert dataCategoryObj;
        
        String articleId = knowledgeObj.KnowledgeArticleId;
        System.assertNotEquals(articleId, null);
        System.assertNotEquals(articleId, '');
    
        KbManagement.PublishingService.publishArticle(articleId, true);                
        System.assertNotEquals(knowledgeObj, null);             
    }
    }
    @isTest
    public static void testMethod1() {  
        User u3 = [SELECT Id FROM User WHERE UserRole.Name LIKE 'CTS TechDirect%' and profile.name like '%Admin%' and isactive = true and UserPermissionsKnowledgeUser = true limit 1];
        System.runAs(u3) 
        {           
            Test.startTest();
                CTS_TechDirect_TaxonomyController.getPicklistValues('Knowledge__kav', 'Language', 'en_US');
                CTS_TechDirect_TaxonomyController.getDataCategoryStructure('KnowledgeArticleVersion', 'IR_Global_Category_Group');
                CTS_TechDirect_TaxonomyController.getDataCategoryStructure('Account', 'No_Category');
                CTS_TechDirect_TaxonomyController.getAllArticles('Private', 'Test', 'en_US', '', '');
                CTS_TechDirect_TaxonomyController.getAllArticles('Publish', 'Test', 'en_US', '', '');
                CTS_TechDirect_TaxonomyController.getAllArticles('Archive', 'Test', 'en_US', 'CTS_TechDirect_Products', 'CTS_TechDirect_Category');
                CTS_TechDirect_TaxonomyController.PicklistEntryWrapper picklistWrapper = new CTS_TechDirect_TaxonomyController.PicklistEntryWrapper();
                CTS_TechDirect_TaxonomyController.DataCategoryWrapper datacategoryWrapper = new CTS_TechDirect_TaxonomyController.DataCategoryWrapper();        
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testMethod2() {    
        //Create portal account owner
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile sysAdmin = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = sysAdmin.Id,
            Username = System.now().millisecond() + 'test@irtest.com',
            Alias = 'tuser',
            email = 'testemail@irtest.com',
            EmailEncodingKey='UTF-8',
            Firstname='Test',
            Lastname='Admin',
            DB_Region__c = 'North America',
            CTS_TechDirect_Service_Sub_Region__c = 'North America',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            // UserPermissionsKnowledgeUser = true,
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        System.assertNotEquals(portalAccountOwner1, null);
        System.assertNotEquals(portalAccountOwner1.Id, null);
      
        System.runAs(portalAccountOwner1) {  
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                Description = 'Standard Products',
                RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId(), //IR Comp TechDirect Account
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(portalAccount1);
            System.assertNotEquals(portalAccount1, null);
            System.assertNotEquals(portalAccount1.Id, null);
                 
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'PortalContact',
                AccountId = portalAccount1.Id,
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Contact').getRecordTypeId(), //IR Comp TechDirect Contact
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
            System.assertNotEquals(contact1, null);
            System.assertNotEquals(contact1.Id, null);        
            
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'CTS_TechDirect_Customer Community Plus User' Limit 1];
            User userObj = new User(
                Username = System.now().millisecond() + 'test@irtest.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'PortalUser',
                CommunityNickname = 'test12345',
                DB_Region__c = 'North America',
                CTS_TechDirect_Service_Sub_Region__c = 'North America',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                UserPermissionsKnowledgeUser = false,
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(userObj);
            System.assertNotEquals(userObj, null);
            System.assertNotEquals(userObj.Id, null);      
        }
        
        User userObj = [SELECT Id, Name FROM User WHERE LastName = 'PortalUser' Limit 1];
        
        System.runAs(userObj) {
            Test.startTest();
                CTS_TechDirect_TaxonomyController1.getAllArticles('Test', 'en_US', 'CTS_TechDirect_Products', 'CTS_TechDirect_Category');
                CTS_TechDirect_TaxonomyController1.getPicklistValues('Knowledge__kav', 'Language', 'en_US');
                CTS_TechDirect_TaxonomyController1.getDataCategoryStructure('KnowledgeArticleVersion', 'IR_Global_Category_Group');
                CTS_TechDirect_TaxonomyController1.getDataCategoryStructure('Account', 'No_Category');
                CTS_TechDirect_TaxonomyController1.PicklistEntryWrapper picklistWrapper1 = new CTS_TechDirect_TaxonomyController1.PicklistEntryWrapper();
                CTS_TechDirect_TaxonomyController1.DataCategoryWrapper datacategoryWrapper1 = new CTS_TechDirect_TaxonomyController1.DataCategoryWrapper();
                datacategoryWrapper1.label = 'CTS_TechDirect_Products';
                datacategoryWrapper1.name = 'Cameron';
                datacategoryWrapper1.expanded = true;
                CTS_TechDirect_TaxonomyController1.DataCategoryWrapper datacategoryWrapper2 = new CTS_TechDirect_TaxonomyController1.DataCategoryWrapper('IR_Global_Category_Group','abc',true);
            Test.stopTest();
        }
    }
}