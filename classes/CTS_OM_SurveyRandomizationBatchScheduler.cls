/* 
 *  Snehal Chabukswar || Date 14/06/2021 
 *  exceute CTS_OM_SurveyRandomizationBatch batch class 
*/
global class CTS_OM_SurveyRandomizationBatchScheduler implements Schedulable{
    //Execute method 
    //@parameter SchedulableContext variable
    global void execute(SchedulableContext ctx) {
        
        Id batchJobId = Database.executeBatch(new CTS_OM_SurveyRandomizationBatch(),25);
    }
}