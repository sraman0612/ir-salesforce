@isTest(seeAllData = false)
private class BatchChangeBdmMdmNewBusOpp_Test{

    // Costcenter
        public static final String COST_CENTER_NAME = 'Test Cost Center';
        public static final String ACCOUNT_NAME = 'Test Account';
    // Developer Name of the RS_HVAC Record type
   public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
   public static final String RS_HVAC_RT_OPPDEV_NAME = 'IWD_New_Business';
   public static User apiUser11;
   public static User apiUser12;
   public static Cost_Center__c costCenter = new Cost_Center__c();
   public static RSHVAC_Properties__c IntegrationUser;
  
  /**
     *  Tests that the Batch executes properly
     */
    static testMethod void testBatchExecution() {
        // Insert a basic Account, all Oppty will be related to it
        Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
        Id rshvacoppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_OPPDEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;
        Id apiProfileId = [select Id from Profile where name like 'API_Profile'].Id;
        
        apiUser11 = new User(LastName = 'testingUser1', profileId=apiProfileId, Username='aaa112@example.ingersollrand.com', Email='aaa@example.com',
         Alias = 'abc1', CommunityNickname='abc11', TimeZoneSidKey = 'GMT', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
         insert apiUser11;
         apiUser12 = new User(LastName = 'testingUser2', profileId=apiProfileId, Username='aaa123@example.ingersollrand.com', Email='aaa@example.com',
         Alias = 'abc1', CommunityNickname='abc12', TimeZoneSidKey = 'GMT', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
         insert apiUser12;
         costCenter = new Cost_Center__c(name = COST_CENTER_NAME, Channel__c = 'IWD', Default_Branch__c = COST_CENTER_NAME,
                                                      BDM__c=apiUser11.id,MDM__c=apiUser12.id);  
        insert costCenter;
        Account acc = new Account(Name = ACCOUNT_NAME,Default_Branch__c=COST_CENTER_NAME,RecordTypeId = rshvacRTID);
        insert acc;
        Integer newBillTotal = 2000;
        Opportunity firstOpp = new Opportunity (Name ='opp1',AccountID = acc.Id, RecordTypeId =rshvacoppRTID,CloseDate=Date.today(), StageName='Request', Type = 'Single Family - NC', Amount=10000,Competition__c = 'IWD', Effective_Date__c =Date.today(),Expiration_Date__c =Date.today(),Exp_Ship_Date__c=Date.today(), Gross_Sales__c= newBillTotal );
        Opportunity  secondOpp = new Opportunity (Name ='Opp2',AccountID = acc.Id, RecordTypeId =rshvacoppRTID,CloseDate=Date.today(), StageName='Request', Type = 'Single Family Non Owner Occupied', Amount=10000,Competition__c = 'IWD', Effective_Date__c =Date.today(),Expiration_Date__c =Date.today(),Exp_Ship_Date__c=Date.today(), Gross_Sales__c= 0 );
        IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
        insert IntegrationUser;
        if(IntegrationUser.Value__c.contains(Userinfo.getName())){
         // Do Nothing
         } else {
        insert new List<Opportunity>{firstOpp, secondOpp};
        }
        Set<Id> oppIds = new Set<Id>{firstOpp.Id, secondOpp.Id};
        Test.startTest();
      // Instantiate and execute the batch
        // Dummy call to add coverage

          costCenter.BDM__c = apiUser12.Id;
          costCenter.MDM__c = apiUser11.Id;
          update costCenter;
        costCenter = [Select Id, BDM__c, MDM__c, OwnerId,Is_BDM_MDM_Changed__c From Cost_Center__c where id = :costCenter.Id];
        system.assert(costCenter.Is_BDM_MDM_Changed__c); 
        BatchChangeBdmMdmNewBusOpp batch1 = new BatchChangeBdmMdmNewBusOpp();
        Database.executeBatch(batch1);
       Test.stopTest();
        // Get the updated Oppty
        for (Opportunity oppty : [SELECT Id, ownerid FROM Opportunity WHERE Id IN :oppIds]){
           if (oppty.Id == firstOpp.Id){
            
                System.assertEquals(costCenter.BDM__c , oppty.ownerid);
            } else {
                
                System.assertEquals(costCenter.MDM__c, oppty.ownerid );
            }
                      
        }
  }
}