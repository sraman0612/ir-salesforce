/*------------------------------------------------------------
Author:       Randhir Kumar
Description:  Batch that will reset opportunity owner based on the BDM/MDM on the respective Cost Center
---------------------------------
Modified On: 13-FEB-18
Modified By: Ashwini - Strategic Opportunity Change
------------------------------------------------------------*/
global class BatchChangeBdmMdmNewBusOpp implements Database.Batchable<sObject> { 

    global String query ;
    
    global BatchChangeBdmMdmNewBusOpp(){
        string RECORD_TYPE_IWD_NEW_BUSINESS = 'IWD_New_Business';
        string RECORD_TYPE_STRATEGIC_IWD = 'Strategic_Opportunity_IWD';
        Id rshvacIwdRTID = OpportunityUtils.OPP_RECORD_TYPES.get(RECORD_TYPE_IWD_NEW_BUSINESS).Id;
        Id rshvacStgIwdRTID = OpportunityUtils.OPP_RECORD_TYPES.get(RECORD_TYPE_STRATEGIC_IWD).Id;
        // SELECT fields
        query = 'SELECT Id, OwnerID, RecordTypeId, Belong_to_BDM__c, Belong_to_MDM__c, Account.Cost_Center_Name__r.BDM__c, Account.Cost_Center_Name__r.MDM__c';
        // FROM object
        query += ' FROM Opportunity';
        // Put condition for fecthing the specific records.
        query += ' WHERE (RecordTypeId = \''+rshvacIwdRTID+'\' OR RecordTypeId = \''+rshvacStgIwdRTID+'\')';
        query += ' AND Account.Cost_Center_Name__r.Is_BDM_MDM_Changed__c = True';
        System.debug('Jaya Query : '+query);
       } 
       
      /* start method */  
      global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('START BEGIN >>>');
        System.debug('Query >>> ' + query);
       // return Database.getQueryLocator('SELECT Id, OwnerID, RecordTypeId FROM Opportunity WHERE Account.Cost_Center_Name__r.Is_BDM_MDM_Changed__c = True');
        return Database.getQueryLocator(query);
       } 
    
     /* execute method */
     global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('EXECUTE BEGIN >>>');
        List<Opportunity> opptyToUpdate = new List<Opportunity>();
        for (SObject sObj : scope){
          Opportunity opp = (Opportunity) sObj;
          If(opp.Belong_to_BDM__c == true){
          opp.OwnerID= opp.Account.Cost_Center_Name__r.BDM__c;
          }
          else if(opp.Belong_to_MDM__c == true){
           opp.OwnerID= opp.Account.Cost_Center_Name__r.MDM__c;
          }
                    
          opptyToUpdate.add(opp);
        }
        update opptyToUpdate;
    } 

    /* finish method */
    global void finish(Database.BatchableContext BC) {
        List<Cost_Center__c> costCenterList = new List<Cost_Center__c>();
        System.debug(' Jaya Finish entered : ');
        for(Cost_Center__c costCenter: [Select Id, Is_BDM_MDM_Changed__c from Cost_Center__c where Is_BDM_MDM_Changed__c = True] ){
          costCenter.Is_BDM_MDM_Changed__c = False;
          costCenterList.add(costCenter);
        }
        if(!costCenterList.isEmpty()){
          update costCenterList;
        }
    } 
      
   }