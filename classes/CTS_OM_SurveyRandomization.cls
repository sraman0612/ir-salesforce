public class CTS_OM_SurveyRandomization {
    public static void updateCase(List<Case> caseList){
        Set<Id>contactIdSet = new Set<Id>();
        Set<Id>addedContactIdSet = new Set<Id>();
        Map<Id,String> conCSRIdMap = new Map<Id,Id>();
        Map<Id,Integer>csrCountMap = new Map<Id,Integer>();
        List<Case>updatedCaseList = new List<Case>();
        List<Contact>updatedContactList = new List<Contact>();
        
        for(Case caseObj:caseList){
            if(caseObj.ContactId!=null){
                contactIdSet.add(caseObj.ContactId); 
            }
        }
        
        /*Created Contact and related CSR map for previous randomization run*/
        for(Contact conObj :[SELECT Id,CTS_OM_CSRId__c FROM Contact WHERE id IN:contactIdSet]){
            system.debug('conObj.CTS_OM_CSRId__c----->'+conObj.CTS_OM_CSRId__c);
            conCSRIdMap.put(conObj.Id,conObj.CTS_OM_CSRId__c);
        }
        
        /*Select only those cases whose csr and contact combination is not consider for previous randomization run*/
        for(Case caseObj:caseList){
            if(conCSRIdMap.containsKey(caseObj.ContactId)
               && conCSRIdMap.get(caseObj.ContactId)!= caseObj.OwnerId
               && (
                   (
                       !addedContactIdSet.isEmpty()
                       && 
                       !addedContactIdSet.contains(caseObj.ContactId)
                   )
                   || addedContactIdSet.isEmpty()
                   )
                ){
                  if((csrCountMap.containsKey(caseObj.OwnerId) && csrCountMap.get(caseObj.OwnerId)<5)
                     ||
                     !csrCountMap.containsKey(caseObj.OwnerId)
                    ){
                      system.debug('Satisfying if condition--------------->'+csrCountMap);
                      caseObj.CTS_OM_Send_Email_to_Contact__c = true;
                      updatedCaseList.add(caseObj);
                      addedContactIdSet.add(caseObj.ContactId);
                      Contact conObj = new Contact(Id= caseObj.ContactId);
                      conObj.CTS_OM_CSRId__c = caseObj.OwnerId;
                      updatedContactList.add(conObj);
                        Integer count = 0;
                        if(!csrCountMap.containsKey(caseObj.OwnerId)||
                           (csrCountMap.containsKey(caseObj.OwnerId) &&
                            csrCountMap.get(caseObj.OwnerId)== null)
                          ){
                            count++;
                        }else{
                            count = csrCountMap.get(caseObj.OwnerId)+1;
                        }
                        
                      csrCountMap.put(caseObj.OwnerId,count);
                  }
                  
              }
        }
        
        if(!updatedCaseList.isEmpty()){
            update updatedCaseList; 
        }
        if(!updatedContactList.isEmpty()){
            update updatedContactList; 
        }
    }
}