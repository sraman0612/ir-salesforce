/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 07-27-2023
 * @last modified by  : RISHAB GOYAL
**/
@isTest
public class CaseToLeadConvertControllerTest {

    @testSetup
    private static void createData(){

        Contact myContact = new Contact(
            FirstName = 'Test',
            LastName = 'Test2'
        );
        insert myContact;

        Case_to_Lead__c objcstolead = new Case_to_Lead__c();
        objcstolead.Lead_Field__c ='CTS_Lead_Region_Mappin__c';
        objcstolead.Name ='CTS_Region__c';
        insert objcstolead;
        
       	Case_to_Lead__c objcstolead1 = new Case_to_Lead__c();
        objcstolead1.Lead_Field__c ='Email';
        objcstolead1.Name ='SuppliedEmail';
        insert objcstolead1;
        
        Account myAccount = new Account(
            Name = 'Test',
            CTS_Global_Region__c  = 'NA',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LFS Account Record Type').getRecordTypeId()
        );
        insert myAccount;

        insert new Case(
        	LFS_Last_Name__c = 'Test',
            LFS_First_Name__c = 'Test1',
            Brand__c = 'Thomas',
            SuppliedCompany = 'Company',
            suppliedEmail = 'test@test.com',
            Origin = 'Email',
            CTS_Region__c = 'NA',
            ContactId = myContact.Id,
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IR Comp OM Account Management').getRecordTypeId(),
            AccountId = myAccount.Id
        );
    }

    @isTest
    public static void caseMappingTestThomas() {
        
        Case c = [Select Id From Case LIMIT 1];

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.Thomas));
        Test.stopTest();
    }

    @isTest
    public static void caseMappingTestTriContinentNA() {
        
        Case c = [Select Id From Case LIMIT 1];
        c.Brand__c = 'TriContinent';
        update c;

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.TriContinent));
        Test.stopTest();
    }

    @isTest
    public static void caseMappingTestTriContinentEU() {
        
        Case c = [Select Id From Case LIMIT 1];
        c.CTS_Region__c = 'EU';
        c.Brand__c = 'TriContinent';
        update c;

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.TriContinent_EMEIA));
        Test.stopTest();
    }

    @isTest
    public static void caseMappingTestILS() {
        
        Case c = [Select Id From Case LIMIT 1];
        c.Brand__c = 'ILS';
        update c;

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.TriContinent));
        Test.stopTest();
    }

    @isTest
    public static void caseMappingTestZinsserAnalytic() {
        
        Case c = [Select Id From Case LIMIT 1];
        c.Brand__c = 'Zinsser Analytic';
        update c;

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.TriContinent));
        Test.stopTest();
    }

    @isTest
    public static void caseMappingTestOINA() {
        
        Case c = [Select Id From Case LIMIT 1];
        c.Brand__c = 'OINA';
        update c;

        Test.startTest();
        validateConversionResults(c.Id, Id.valueOf(Label.Thomas));
        Test.stopTest();
    }



    private static void validateConversionResults(Id caseId, Id expectedLeadOwnerId){

        system.assertEquals('Success', CaseToLeadConvertController.caseMapping(caseId));

        Case c = [Select Id, Type, Status, Sub_Status__c, LFS_First_Name__c, LFS_Last_Name__c, SuppliedCompany From Case Where Id = :caseId];
        Lead l = [Select Id, OwnerId, FirstName, LastName, Company From Lead Order By CreatedDate DESC LIMIT 1];
        
        // Case check
        system.assertEquals('Converted to Lead', c.Type);
        system.assertEquals('Closed', c.Status);

        // Lead check
        system.assertEquals(expectedLeadOwnerId, l.OwnerId);
        system.assertEquals(c.LFS_First_Name__c, l.FirstName);
        system.assertEquals(c.LFS_Last_Name__c, l.LastName);
        system.assertEquals(c.SuppliedCompany, l.Company);
    }
}