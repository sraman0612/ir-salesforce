@isTest
public class holeInOneRequestTriggerHandler_Test {
        @testSetup static void setupData() {
            List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
            psettingList = TestUtilityClass.createPavilionSettings();
            
            insert psettingList;
        }
    static testmethod void UnitTest_holeInOneRequestTrigger()
    {   
        Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        Contact testContact = TestUtilityClass.createContact(sampleAccount.Id);
        insert testContact;
        Contract cnt =new Contract();
        cnt.AccountId=sampleAccount.Id;
        cnt.Name='Sample Contract';
        cnt.CC_type__c='Dealer/Distributor Agreement';
        cnt.CC_Sub_Type__c='Commercial Utility';
        insert cnt;
        Map<String,id> profMap = new Map<String,id>();
        list<Profile> profid = [SELECT id, Name FROM Profile];
        for(Profile p:profid){
            profMap.put(p.Name,p.id);
        }
        /*User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profMap.get('CC_PavilionCommunityUser'),Alias = 'test1236',
                            Email = 'test123456@test.ingersollrand.com',ContactId= testContact.id, EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                            TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='US_CA_SVC_ONLY_DLR__PRINCIPAL_GM_ADMIN');
        insert testUser;*/
        User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profMap.get('CC_System Administrator'),Alias = 'test1236',
                            Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                            TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='US_CA_SVC_ONLY_DLR__PRINCIPAL_GM_ADMIN');
        insert testUser;
        
        CC_Hole_in_One__c coopRequest; 
        Hole_In_One_Claim__c hio1;
        Hole_In_One_Claim__c hio2;
        List<Hole_In_One_Claim__c> hIOCList = new List<Hole_In_One_Claim__c>();
        test.startTest();
        System.runAs (testUser) {
         // creating test data
         
        CC_Pavilion_Co_Op_Product_Mapping__c ccpm =new CC_Pavilion_Co_Op_Product_Mapping__c();
        ccpm.CC_Account_Region__c='United States';
        ccpm.Marketing_Agreement__c='Golf Utility';
        ccpm.Coop_Type__c='Hole-In-One';
        ccpm.Charge_Account__c='ESA AR';
        ccpm.Name='Pavilion Co-Op Product mapping';
        insert ccpm;
        coopRequest= TestUtilityClass.createCoopRequest(cnt.Id);
        insert coopRequest;
        hio1=TestUtilityClass.createHoleInOneClaimApproved(coopRequest.id);
        insert hio1;
        hIOCList.add(hio1);
        hio2=TestUtilityClass.createHoleInOneClaimApproved(coopRequest.id);
        insert hio2;
        hIOCList.add(hio2);
        Product2 p1 =new Product2();
        p1.Name='first product';
        p1.ProductCode='ESA AR';
        insert p1;
        Product2 p2 =new Product2();
        p2.Name='second product';
        p2.ProductCode='COOP SE ASIA';
        insert p2; 
        holeInOneRequestTriggerHandler.createOrderAndOrderitemForHIOC(hIOCList);
        delete hio2;
        undelete hio2;
        test.stopTest();
        }
         
    }
}