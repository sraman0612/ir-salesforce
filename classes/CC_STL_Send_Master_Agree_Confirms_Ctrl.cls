public with sharing class CC_STL_Send_Master_Agree_Confirms_Ctrl {

    public class LeaseWrapper{
        public CC_Short_Term_Lease__c lease {get;set;}
        public Boolean selected {get;set;}

        public LeaseWrapper(CC_Short_Term_Lease__c lease, Boolean selected){
            this.lease = lease;
            this.selected = selected;
        }
    }

    public LeaseWrapper[] leases {get;set;}

    public CC_STL_Send_Master_Agree_Confirms_Ctrl(ApexPages.StandardController controller){

        leases = new LeaseWrapper[]{};

        for (CC_Short_Term_Lease__c lease : [Select Id, Name, Beginning_Date__c, Ending_Date__c, Invoice_Terms__c, Total_Lease_Price__c, Shipping_To_Name__c From CC_Short_Term_Lease__c Where Master_Lease__c = :controller.getId() Order By CreatedDate DESC]){
            leases.add(new LeaseWrapper(lease, false));
        }
    }

    public PageReference buildAndSend(){
            
        Id recordId = ApexPages.currentPage().getParameters().get('id');    
        PageReference pdf = Page.CC_STL_Master_Agree_And_Confirms_PDF;
        pdf.getParameters().put('id', recordId);

        String leaseIdsStr = '';

        for (LeaseWrapper wrapper : leases){

            if (wrapper.selected){
                leaseIdsStr += wrapper.lease.Id + ',';
            }
        }
        
        leaseIdsStr = leaseIdsStr.removeEnd(',');
        pdf.getParameters().put('leaseIds', leaseIdsStr);

        Attachment attach = new Attachment();
        Blob body;
        DateTime now = System.now();
        
        try{
            body = pdf.getContent();
        }
        catch(VisualforceException e){
            body = Blob.valueOf('Exception is thrown while creating Pdf Page');
        }
                
        attach.Body = body;
        attach.Name = 'Master Lease Agreement and Lease Confirmations-' + now + '.pdf';
        attach.IsPrivate = false;
        attach.ParentId = recordId;
        insert attach;
        
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
    
        return new PageReference('/apex/echosign_dev1__AgreementTemplateProcess?masterid=' + recordId + '&templateId=' + templates.CC_Master_Lease_Template_ID__c);
    }    
}