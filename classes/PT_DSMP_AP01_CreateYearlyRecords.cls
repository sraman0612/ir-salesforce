public with sharing class PT_DSMP_AP01_CreateYearlyRecords {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Main class to create child PT_DSMP records for Account 
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 05-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 
	
	
	public static void createRecords1(List<Account> lstAccount, Boolean isNextYear){
		String currentYear = String.valueof(System.Today().year());
		String nextYear = String.valueof(System.Today().year()+1);
		String currentYear_1 = String.valueof(System.Today().year()-1);
		String currentYear_2 = String.valueof(System.Today().year()-2);

		PT_DSMP_CS_Batch__c cs_Batch_config = PT_DSMP_CS_Batch__c.getOrgDefaults();
		Integer numObjectives = Integer.valueOf(cs_Batch_config.Number_of_Objectives__c);
		String defaultObjectivePlaceholder = cs_Batch_config.DSMP_Objective_Placeholder__c;

		List<PT_DSMP_Year__c> lstYears = new List<PT_DSMP_Year__c>([SELECT Id, 
																			Name,
																			Ext_ID__c
																	FROM PT_DSMP_Year__c 
																	WHERE Ext_ID__c =: nextYear OR 
																		  Ext_ID__c =: currentYear OR 
																		  Ext_ID__c =: currentYear_1 OR
																		  Ext_ID__c =: currentYear_2]);

		System.debug('mgr lstYears ' + lstYears);

		/*Pour Ext ID :
		PT_DSMP :  Year-PT_BO_Customer_Number__c
		PT_DSMP_Target: Year-PT_BO_Customer_Number__c-Month
		PT_DSMP_Strategic_Product: Year-PT_BO_Customer_Number__c-Strategic_Product_Name
		PT_DSMP_Objectives : Year-PT_BO_Customer_Number__c-Objective_Number*/

		String nextYearId;
		String currentYearId;
		String currentYear_1_Id;
		String currentYear_2_Id;
		List<PT_DSMP_Year__c> lstYearToInsert = new List<PT_DSMP_Year__c>();

		for(PT_DSMP_Year__c current : lstYears){
			if(current.Name == currentYear)
				currentYearId = current.Id;
			else if(current.Name == nextYear)
				nextYearId = current.Id;
			else if(current.Name == currentYear_1)
				currentYear_1_Id = current.Id;
			else if(current.Name == currentYear_2)
				currentYear_2_Id = current.Id;
		}

		//if(isCurrentYear){

		if(String.isBlank( currentYearId) )
			lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear,Ext_ID__c=currentYear));

		if(String.isBlank( currentYear_1_Id) )
			lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_1,Ext_ID__c=currentYear_1));

		if(String.isBlank( currentYear_2_Id) )
			lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_2,Ext_ID__c=currentYear_2));

		//}

		if(isNextYear){

			if( String.isBlank( nextYearId) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = nextYear,Ext_ID__c=nextYear));

			/*if( String.isBlank( currentYearId) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear));

			if( String.isBlank( currentYear_1_Id) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_1));*/

		}

		System.debug('mgr lstYearToInsert ' + lstYearToInsert);

		if(lstYearToInsert.size() > 0)
			upsert lstYearToInsert Ext_ID__c;

		System.debug('mgr lstYearToInsert ' + lstYearToInsert);

		for(PT_DSMP_Year__c current : lstYearToInsert){

			if(current.Name == currentYear && String.isNotBlank( currentYearId ))
				currentYearId = current.Id;
			else if(current.Name == nextYear && String.isNotBlank( nextYearId ))
				nextYearId = current.Id;
			else if(current.Name == currentYear_1 && String.isNotBlank( currentYear_1_Id ))
				currentYear_1_Id = current.Id;
			else if(current.Name == currentYear_2 && String.isNotBlank( currentYear_2_Id ))
				currentYear_2_Id = current.Id;

		}

		System.debug('mgr nextYearId ' + nextYearId);
		System.debug('mgr currentYearId ' + currentYearId);
		System.debug('mgr currentYear_1_Id ' + currentYear_1_Id);
		System.debug('mgr currentYear_2_Id ' + currentYear_2_Id);

		Map<String,String> mapYear = new Map<String,String>();
		//if(isCurrentYear){

			if(String.isNotBlank( currentYearId) )
				mapYear.put(currentYearId, currentYear);

			if(String.isNotBlank( currentYear_1_Id) )
				mapYear.put(currentYear_1_Id, currentYear_1);

			if(String.isNotBlank( currentYear_2_Id) )
				mapYear.put(currentYear_2_Id, currentYear_2);

		//} else 

		if(isNextYear){

			if( String.isNotBlank( nextYearId) )
				mapYear.put(nextYearId, nextYear);

			/*if( String.isNotBlank( currentYearId) )
				mapYear.put(currentYearId, currentYear);

			if( String.isNotBlank( currentYear_1_Id) )
				mapYear.put(currentYear_1_Id, currentYear_1);*/

		}

		/*if(isCurrentYear && currentYearId != null)
			mapYear.put(currentYearId, currentYear);
		if(isNextYear && nextYearId != null)
			mapYear.put(nextYearId, nextYear);*/

		System.debug('mgr mapYear ' + mapYear);

		//Generate new Account Ext Id
		Set<String> setNewDSMPKey = new Set<String>();

		//Generate DSMP Shared Objectives Ext Id
		Set<String> setSharedObjKey = new Set<String>();

		Map<String,Account> mapAccount = new Map<String ,Account>();
		for(Account current : lstAccount){

			if(current.PT_BO_Customer_Number__c != null){
				//if(isCurrentYear){
					String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent_2 = currentYear_2 +'-'+ current.PT_BO_Customer_Number__c;
					
					setNewDSMPKey.addAll(new List<String>{keyCurrent, keyCurrent_1, keyCurrent_2});
				//}

				if(isNextYear){
					String keyNext = nextYear +'-'+ current.PT_BO_Customer_Number__c;
					//String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
					//String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;

					//setNewDSMPKey.addAll(new List<String>{keyNext, keyCurrent, keyCurrent_1});
					setNewDSMPKey.add(keyNext);
				}
			}

			if(current.PT_Account_Segment__c != null){
				//if(isCurrentYear){
					String sharedObjKeyCurrent = currentYear +'-'+ current.PT_Account_Segment__c;
					setSharedObjKey.add(sharedObjKeyCurrent);
				//}

				if(isNextYear){
					String sharedObjKeyNext = nextYear +'-'+ current.PT_Account_Segment__c;
					setSharedObjKey.add(sharedObjKeyNext);
				}
			}

			mapAccount.put(current.Id, current);
		}
		System.debug('mgr setNewDSMPKey ' + setNewDSMPKey);
		System.debug('mgr setSharedObjKey 0 ' + setSharedObjKey);

		for(String current : setSharedObjKey){

			for(Integer i=1;i<=3;i++){
				String currentObjective = current +'-'+ String.valueOf(i);

				setSharedObjKey.add(currentObjective);
			}

			setSharedObjKey.remove(current);

		}


		Set<String> setDsmpAccountId = new Set<String>();

		if(setNewDSMPKey.size() > 0)
			for(PT_DSMP__c current :  [SELECT Id,
											  Name, 
											  Account__c,
											  Ext_ID__c
										FROM PT_DSMP__c
										WHERE Ext_ID__c IN: setNewDSMPKey]){
				//setDsmpAccountId.add(current.Account__c);
				setDsmpAccountId.add(current.Ext_ID__c);
			}
		System.debug('mgr existing account id ' + setDsmpAccountId);

		List<PT_DSMP__c> lstDSMPToCreate = new List<PT_DSMP__c>();
		Boolean isDSMPCurrentYear = false;
		Boolean isDSMPnextYear = false;

		for(Account current : lstAccount){

			//if(isCurrentYear){
				String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYearId));
					isDSMPCurrentYear = true;
				}

				String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_1)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_1_Id));
					isDSMPCurrentYear = true;
				}

				String keyCurrent_2 = currentYear_2 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_2)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_2_Id));
					isDSMPCurrentYear = true;
				}
			//}

			if(isNextYear){

				String keyNext = nextYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyNext)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = nextYearId));
					isDSMPnextYear = true;
				}

				/*String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYearId));
					isDSMPnextYear = true;
				}

				String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_1)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_1_Id));
					isDSMPnextYear = true;
				}*/

			}

		}

		System.debug('mgr lstDSMPToCreate ' + lstDSMPToCreate);

		if(!lstDSMPToCreate.isEmpty()){
			//insert lstDSMPToCreate;
			
			database.insert(lstDSMPToCreate, false);
		}

		//DSMP NPD -> Create PT_DSMP_Strategic_Product
		List<PT_DSMP_NPD__c> lstDSMPNPD = new List<PT_DSMP_NPD__c>();
		Map<Id, PT_DSMP_NPD__c> mapDSMPNPD = new Map<Id, PT_DSMP_NPD__c>();
	

		Map<String, List<PT_DSMP_NPD__c>> mapYearToNPD = new Map<String, List<PT_DSMP_NPD__c>>();
		if(isDSMPCurrentYear && isDSMPnextYear){
	
			for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name, Hide__c
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: currentYearId OR PT_DSMP_Year__c =: nextYearId]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}
	
		} else if(isDSMPCurrentYear){

		  	for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name, Hide__c
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: currentYearId OR
										  		PT_DSMP_Year__c =: currentYear_1_Id OR 
												PT_DSMP_Year__c =: currentYear_2_Id]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}

		} else if(isDSMPnextYear){

			for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name, Hide__c
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: nextYearId OR
										  		PT_DSMP_Year__c =: currentYearId OR
										  		PT_DSMP_Year__c =: currentYear_1_Id]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}
		
		}

		System.debug('mgr mapYearToNPD ' + mapYearToNPD);

	  	List<PT_DSMP_Strategic_Product__c> lstStraProdToInsert = new List<PT_DSMP_Strategic_Product__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){
			//String yearDSMP = mapYear.get(current.PT_DSMP_Year__c);
			String yearDSMP = current.PT_DSMP_Year__c;

			if(mapYearToNPD.containsKey(yearDSMP)){

				for(PT_DSMP_NPD__c currentProduct : mapYearToNPD.get(yearDSMP)){

	  				lstStraProdToInsert.add(new PT_DSMP_Strategic_Product__c(Name = currentProduct.Name,
	  																		 PT_DSMP__c = current.Id,
	  																		 DSMP_NPD__c = currentProduct.Id,
	  																		 Hide__c = currentProduct.Hide__c));

				}

			}
		}

		System.debug('mgr lstStraProdToInsert ' + lstStraProdToInsert);

	  	if(!lstStraProdToInsert.isEmpty()){
	  		insert lstStraProdToInsert;
	  	}
	  	

		// DSMP Target
		List<PT_DSMP_TARGET__c> lstDSMPTargetToInsert = new List<PT_DSMP_TARGET__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){

			for(Integer i=1;i<=12;i++){
				lstDSMPTargetToInsert.add(new PT_DSMP_TARGET__c(Month__c = String.valueOf(i),
																PT_DSMP__c = current.Id));		
			}

		}

		System.debug('mgr lstDSMPTargetToInsert ' + lstDSMPTargetToInsert);
		if(!lstDSMPTargetToInsert.isEmpty()){
			//insert lstDSMPTargetToInsert;
			database.insert(lstDSMPTargetToInsert, false);
		}


		//Existing Shared Objectives
		Map<String, PT_DSMP_Shared_Objectives__c> mapSharedObjKeyToId = new Map<String, PT_DSMP_Shared_Objectives__c>();
		if(setSharedObjKey.size() > 0)
			for(PT_DSMP_Shared_Objectives__c current : [SELECT Id, Ext_ID__c, Text__c
														FROM PT_DSMP_Shared_Objectives__c
														WHERE Ext_ID__c IN: setSharedObjKey]){

				mapSharedObjKeyToId.put(current.Ext_ID__c, current);
			}
		

		System.debug('mgr mapSharedObjKeyToId ' + mapSharedObjKeyToId);

		// DSMP Objectives
		List<PT_DSMP_Objectives__c> lstDSMPObjectivesToInsert = new List<PT_DSMP_Objectives__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){
			//Shared Objectives External Id

			//if( (isCurrentYear && 
			if( (mapYear.containsKey(current.PT_DSMP_Year__c) && 
				 //mapAccount.get(current.Account__c).PT_Account_Segment__c != null &&
				 mapYear.get(current.PT_DSMP_Year__c) ==  currentYear) || 

				(isNextYear && 
				 mapYear.containsKey(current.PT_DSMP_Year__c) && 
				 //mapAccount.get(current.Account__c).PT_Account_Segment__c != null &&
				 mapYear.get(current.PT_DSMP_Year__c) == nextYear) ){

				Integer countSecondaryObjective = 1;

				for(Integer i=1;i<=numObjectives;i++){
					String key = mapYear.get(current.PT_DSMP_Year__c) +'-'+ mapAccount.get(current.Account__c).PT_Account_Segment__c + '-' + String.valueOf(i);

					if(i<=3 && 
						mapSharedObjKeyToId.containsKey(key) && 
						mapAccount.containsKey(current.Account__c) &&
						mapAccount.get(current.Account__c).PT_Account_Segment__c != null){

						lstDSMPObjectivesToInsert.add(new PT_DSMP_Objectives__c(Objectives_Number__c = String.valueOf(i),
																				PT_DSMP__c = current.Id,
																				Account__c = current.Account__c,
																				Objective__c = mapSharedObjKeyToId.get(key).Text__c,
																				PT_DSMP_Shared_Objectives__c = mapSharedObjKeyToId.get(key).Id
																				));

					} else {

						lstDSMPObjectivesToInsert.add(new PT_DSMP_Objectives__c(Objectives_Number__c = String.valueOf(i),
																				PT_DSMP__c = current.Id,
																				Objective__c = defaultObjectivePlaceholder,
																				Name = defaultObjectivePlaceholder,
																				//Objective__c = 'Objective #' + String.valueOf(countSecondaryObjective),
																				Account__c = current.Account__c));

						countSecondaryObjective++;

					}

				}

			}

		}

		System.debug('mgr lstDSMPObjectivesToInsert ' + lstDSMPObjectivesToInsert);
		if(!lstDSMPObjectivesToInsert.isEmpty()){
			//insert lstDSMPObjectivesToInsert;
			database.insert(lstDSMPObjectivesToInsert, false);

		}

	}


	/*
	public static void createRecords(List<Account> lstAccount,Boolean isCurrentYear, Boolean isNextYear){
		String currentYear = String.valueof(System.Today().year());
		String nextYear = String.valueof(System.Today().year()+1);
		String currentYear_1 = String.valueof(System.Today().year()-1);
		String currentYear_2 = String.valueof(System.Today().year()-2);

		List<PT_DSMP_Year__c> lstYears = new List<PT_DSMP_Year__c>([SELECT Id, 
																			Name,
																			Ext_ID__c
																	FROM PT_DSMP_Year__c 
																	WHERE Ext_ID__c =: nextYear OR 
																		  Ext_ID__c =: currentYear OR 
																		  Ext_ID__c =: currentYear_1 OR
																		  Ext_ID__c =: currentYear_2]);

		System.debug('mgr lstYears ' + lstYears);

		//Pour Ext ID :
		//PT_DSMP :  Year-PT_BO_Customer_Number__c
		//PT_DSMP_Target: Year-PT_BO_Customer_Number__c-Month
		//PT_DSMP_Strategic_Product: Year-PT_BO_Customer_Number__c-Strategic_Product_Name
		//PT_DSMP_Objectives : Year-PT_BO_Customer_Number__c-Objective_Number

		String nextYearId;
		String currentYearId;
		String currentYear_1_Id;
		String currentYear_2_Id;
		List<PT_DSMP_Year__c> lstYearToInsert = new List<PT_DSMP_Year__c>();

		for(PT_DSMP_Year__c current : lstYears){
			if(current.Name == currentYear)
				currentYearId = current.Id;
			else if(current.Name == nextYear)
				nextYearId = current.Id;
			else if(current.Name == currentYear_1)
				currentYear_1_Id = current.Id;
			else if(current.Name == currentYear_2)
				currentYear_2_Id = current.Id;
		}

		if(isCurrentYear){

			if(String.isBlank( currentYearId) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear));

			if(String.isBlank( currentYear_1_Id) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_1));

			if(String.isBlank( currentYear_2_Id) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_2));

		}

		if(isNextYear){

			if( String.isBlank( nextYearId) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = nextYear));

			if( String.isBlank( currentYearId) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear));

			if( String.isBlank( currentYear_1_Id) )
				lstYearToInsert.add(new PT_DSMP_Year__c(Name = currentYear_1));

		}

		System.debug('mgr lstYearToInsert ' + lstYearToInsert);

		if(lstYearToInsert.size() > 0)
			insert lstYearToInsert;

		System.debug('mgr lstYearToInsert ' + lstYearToInsert);

		for(PT_DSMP_Year__c current : lstYearToInsert){

			if(current.Name == currentYear && String.isNotBlank( currentYearId ))
				currentYearId = current.Id;
			else if(current.Name == nextYear && String.isNotBlank( nextYearId ))
				nextYearId = current.Id;
			else if(current.Name == currentYear_1 && String.isNotBlank( currentYear_1_Id ))
				currentYear_1_Id = current.Id;
			else if(current.Name == currentYear_2 && String.isNotBlank( currentYear_2_Id ))
				currentYear_2_Id = current.Id;

		}

		System.debug('mgr nextYearId ' + nextYearId);
		System.debug('mgr currentYearId ' + currentYearId);
		System.debug('mgr currentYear_1_Id ' + currentYear_1_Id);
		System.debug('mgr currentYear_2_Id ' + currentYear_2_Id);

		Map<String,String> mapYear = new Map<String,String>();
		if(isCurrentYear){

			if(String.isNotBlank( currentYearId) )
				mapYear.put(currentYearId, currentYear);

			if(String.isNotBlank( currentYear_1_Id) )
				mapYear.put(currentYear_1_Id, currentYear_1);

			if(String.isNotBlank( currentYear_2_Id) )
				mapYear.put(currentYear_2_Id, currentYear_2);

		} else if(isNextYear){

			if( String.isNotBlank( nextYearId) )
				mapYear.put(nextYearId, nextYear);

			if( String.isNotBlank( currentYearId) )
				mapYear.put(currentYearId, currentYear);

			if( String.isNotBlank( currentYear_1_Id) )
				mapYear.put(currentYear_1_Id, currentYear_1);

		}


		System.debug('mgr mapYear ' + mapYear);

		//Generate new Account Ext Id
		Set<String> setNewDSMPKey = new Set<String>();

		//Generate DSMP Shared Objectives Ext Id
		Set<String> setSharedObjKey = new Set<String>();

		Map<String,Account> mapAccount = new Map<String ,Account>();
		for(Account current : lstAccount){

			if(current.PT_BO_Customer_Number__c != null){
				if(isCurrentYear){
					String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent_2 = currentYear_2 +'-'+ current.PT_BO_Customer_Number__c;
					//setNewDSMPKey.add(keyCurrent);
					setNewDSMPKey.addAll(new List<String>{keyCurrent, keyCurrent_1, keyCurrent_2});
				}

				if(isNextYear){
					String keyNext = nextYear +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
					String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
					//setNewDSMPKey.add(keyNext);
					setNewDSMPKey.addAll(new List<String>{keyNext, keyCurrent, keyCurrent_1});
				}
			}

			if(current.PT_Account_Segment__c != null){
				if(isCurrentYear){
					String sharedObjKeyCurrent = currentYear +'-'+ current.PT_Account_Segment__c;
					setSharedObjKey.add(sharedObjKeyCurrent);
				}

				if(isNextYear){
					String sharedObjKeyNext = nextYear +'-'+ current.PT_Account_Segment__c;
					setSharedObjKey.add(sharedObjKeyNext);
				}
			}

			mapAccount.put(current.Id, current);
		}
		System.debug('mgr setNewDSMPKey ' + setNewDSMPKey);
		System.debug('mgr setSharedObjKey 0 ' + setSharedObjKey);

		for(String current : setSharedObjKey){

			for(Integer i=1;i<=3;i++){
				String currentObjective = current +'-'+ String.valueOf(i);

				setSharedObjKey.add(currentObjective);
			}

			setSharedObjKey.remove(current);

		}

		System.debug('mgr setSharedObjKey 1 ' + setSharedObjKey);


		Set<String> setDsmpAccountId = new Set<String>();

		if(setNewDSMPKey.size() > 0)
			for(PT_DSMP__c current :  [SELECT Id,
											  Name, 
											  Account__c,
											  Ext_ID__c
										FROM PT_DSMP__c
										WHERE Ext_ID__c IN: setNewDSMPKey]){
				//setDsmpAccountId.add(current.Account__c);
				setDsmpAccountId.add(current.Ext_ID__c);
			}
		System.debug('mgr existing account id ' + setDsmpAccountId);

		List<PT_DSMP__c> lstDSMPToCreate = new List<PT_DSMP__c>();
		Boolean isDSMPCurrentYear = false;
		Boolean isDSMPnextYear = false;
		for(Account current : lstAccount){

			if(isCurrentYear){
				String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYearId));
					isDSMPCurrentYear = true;
				}

				String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_1)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_1_Id));
					isDSMPCurrentYear = true;
				}

				String keyCurrent_2 = currentYear_2 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_2)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_2_Id));
					isDSMPCurrentYear = true;
				}
			}

			if(isNextYear){

				String keyNext = nextYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyNext)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = nextYearId));
					isDSMPnextYear = true;
				}

				String keyCurrent = currentYear +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYearId));
					isDSMPnextYear = true;
				}

				String keyCurrent_1 = currentYear_1 +'-'+ current.PT_BO_Customer_Number__c;
				if(!setDsmpAccountId.contains(keyCurrent_1)){
					lstDSMPToCreate.add(new PT_DSMP__c(Account__c = current.Id,
													   Stage__c = 'Draft',
													   Display_Type__c = '1',
													   DVP_Segment__c = current.PT_Account_Segment__c,
													   PT_DSMP_Year__c = currentYear_1_Id));
					isDSMPnextYear = true;
				}

			}

		}

		System.debug('mgr lstDSMPToCreate ' + lstDSMPToCreate);

		if(!lstDSMPToCreate.isEmpty()){
			insert lstDSMPToCreate;
		}

		//DSMP NPD -> Create PT_DSMP_Strategic_Product
		List<PT_DSMP_NPD__c> lstDSMPNPD = new List<PT_DSMP_NPD__c>();
		Map<Id, PT_DSMP_NPD__c> mapDSMPNPD = new Map<Id, PT_DSMP_NPD__c>();
	
		//if(isDSMPCurrentYear && isDSMPnextYear){
	
		//	mapDSMPNPD = new Map<Id, PT_DSMP_NPD__c>([SELECT Id, PT_DSMP_Year__c, Name
		//											  FROM PT_DSMP_NPD__c
		//											  WHERE PT_DSMP_Year__c =: currentYearId OR PT_DSMP_Year__c =: nextYearId]);
	
		//} else if(isDSMPCurrentYear){

		//	mapDSMPNPD = new Map<Id, PT_DSMP_NPD__c>([SELECT Id, PT_DSMP_Year__c, Name
		//											  FROM PT_DSMP_NPD__c
		//											  WHERE PT_DSMP_Year__c =: currentYearId]);
		//} else if(isDSMPnextYear){
		
		//	mapDSMPNPD = new Map<Id, PT_DSMP_NPD__c>([SELECT Id, PT_DSMP_Year__c, Name
		//											  FROM PT_DSMP_NPD__c
		//											  WHERE PT_DSMP_Year__c =: nextYearId]);
		
		//}

		Map<String, List<PT_DSMP_NPD__c>> mapYearToNPD = new Map<String, List<PT_DSMP_NPD__c>>();
		if(isDSMPCurrentYear && isDSMPnextYear){
	
			for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: currentYearId OR PT_DSMP_Year__c =: nextYearId]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}
	
		} else if(isDSMPCurrentYear){

		  	for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: currentYearId OR
										  		PT_DSMP_Year__c =: currentYear_1_Id OR 
												PT_DSMP_Year__c =: currentYear_2_Id]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}

		} else if(isDSMPnextYear){

			for(PT_DSMP_NPD__c current : [SELECT Id, PT_DSMP_Year__c, Name
										  FROM PT_DSMP_NPD__c
										  WHERE PT_DSMP_Year__c =: nextYearId OR
										  		PT_DSMP_Year__c =: currentYearId OR
										  		PT_DSMP_Year__c =: currentYear_1_Id]){

				if(mapYearToNPD.containsKey(current.PT_DSMP_Year__c)){
					mapYearToNPD.get(current.PT_DSMP_Year__c).add(current);
				}else{
					mapYearToNPD.put(current.PT_DSMP_Year__c, new List<PT_DSMP_NPD__c>{current});
				}

			}
		
		}

		System.debug('mgr mapYearToNPD ' + mapYearToNPD);

	  	List<PT_DSMP_Strategic_Product__c> lstStraProdToInsert = new List<PT_DSMP_Strategic_Product__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){
			//String yearDSMP = mapYear.get(current.PT_DSMP_Year__c);
			String yearDSMP = current.PT_DSMP_Year__c;

			System.debug('mgr yearDSMP ' + yearDSMP);

			if(mapYearToNPD.containsKey(yearDSMP)){

				for(PT_DSMP_NPD__c currentProduct : mapYearToNPD.get(yearDSMP)){

	  				lstStraProdToInsert.add(new PT_DSMP_Strategic_Product__c(Name = currentProduct.Name,
	  																		 PT_DSMP__c = current.Id,
	  																		 DSMP_NPD__c = currentProduct.Id));

				}

			}
		}

		System.debug('mgr lstStraProdToInsert ' + lstStraProdToInsert);

	  	if(!lstStraProdToInsert.isEmpty()){
	  		insert lstStraProdToInsert;
	  	}

		// DSMP Target
		List<PT_DSMP_TARGET__c> lstDSMPTargetToInsert = new List<PT_DSMP_TARGET__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){

			for(Integer i=1;i<=12;i++){
				lstDSMPTargetToInsert.add(new PT_DSMP_TARGET__c(Month__c = String.valueOf(i),
																PT_DSMP__c = current.Id));		
			}

		}

		System.debug('mgr lstDSMPTargetToInsert ' + lstDSMPTargetToInsert);
		if(!lstDSMPTargetToInsert.isEmpty()){
			//insert lstDSMPTargetToInsert;
			database.insert(lstDSMPTargetToInsert, false);
		}


		//Existing Shared Objectives
		Map<String, PT_DSMP_Shared_Objectives__c> mapSharedObjKeyToId = new Map<String, PT_DSMP_Shared_Objectives__c>();
		if(setSharedObjKey.size() > 0)
			for(PT_DSMP_Shared_Objectives__c current : [SELECT Id, Ext_ID__c, Text__c
														FROM PT_DSMP_Shared_Objectives__c
														WHERE Ext_ID__c IN: setSharedObjKey]){

				mapSharedObjKeyToId.put(current.Ext_ID__c, current);
			}
		

		System.debug('mgr mapSharedObjKeyToId ' + mapSharedObjKeyToId);

		// DSMP Objectives
		List<PT_DSMP_Objectives__c> lstDSMPObjectivesToInsert = new List<PT_DSMP_Objectives__c>();
		for(PT_DSMP__c current : lstDSMPToCreate){
			//Shared Objectives External Id
			//String key = mapYear.get(current.PT_DSMP_Year__c) +'-'+ mapAccount.get(current.Account__c).PT_Account_Segment__c;

			System.debug('mgr isNextYear ' + isNextYear);
			System.debug('mgr current.PT_DSMP_Year__c ' + current.PT_DSMP_Year__c);
			System.debug('mgr mapYear.containsKey(current.PT_DSMP_Year__c) ' + mapYear.containsKey(current.PT_DSMP_Year__c));
			System.debug('mgr ============================ ' +  mapYear.get(current.PT_DSMP_Year__c) );

			if( (isCurrentYear && 
				 mapYear.containsKey(current.PT_DSMP_Year__c) && 
				 mapYear.get(current.PT_DSMP_Year__c) ==  currentYear) || 

				(isNextYear && 
				 mapYear.containsKey(current.PT_DSMP_Year__c) && 
				 mapYear.get(current.PT_DSMP_Year__c) == nextYear) ){

				for(Integer i=1;i<=6;i++){
					String key = mapYear.get(current.PT_DSMP_Year__c) +'-'+ mapAccount.get(current.Account__c).PT_Account_Segment__c + '-' + String.valueOf(i);
					System.debug('mgr obj key ' + key);

					if(i<=3 && mapSharedObjKeyToId.containsKey(key)){

						lstDSMPObjectivesToInsert.add(new PT_DSMP_Objectives__c(Objectives_Number__c = String.valueOf(i),
																				PT_DSMP__c = current.Id,
																				Account__c = current.Account__c,
																				Objective__c = mapSharedObjKeyToId.get(key).Text__c,
																				PT_DSMP_Shared_Objectives__c = mapSharedObjKeyToId.get(key).Id
																				));

					} else {

						lstDSMPObjectivesToInsert.add(new PT_DSMP_Objectives__c(Objectives_Number__c = String.valueOf(i),
																				PT_DSMP__c = current.Id,
																				Account__c = current.Account__c));
					}

				}

			}

		}

		System.debug('mgr lstDSMPObjectivesToInsert ' + lstDSMPObjectivesToInsert);
		if(!lstDSMPObjectivesToInsert.isEmpty()){
			insert lstDSMPObjectivesToInsert;
		}

	}
	*/
		
}