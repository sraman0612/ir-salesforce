global class PT_DSMP_BAT02_CountTasksRecords implements Schedulable, Database.Batchable<sObject> {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Batch to count Task records : PT_DSMP_BAT02_CountTasksRecords
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 06-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 
	
    String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    	Date yesterday = System.today().addDays(-1);

        query = ' SELECT Id, '+
        		' 		 WhatId,PT_DSMP_Account_Objectives__c, '+
                ' 		 isclosed, '+
        		' 		 isDueTask__c '+
                ' FROM Task '+
                ' WHERE PT_DSMP_Account_Objectives__c != null and ' +
                ' (LastModifiedDate =: yesterday OR ActivityDate =: yesterday) ';

        System.debug('mgr start ' + Database.getQueryLocator(query));

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Task> lstTask) {
        Set<String> setId = new Set<String>();
        System.debug('mgr lstTask  ' + lstTask);

        for(Task current : lstTask){
        		setId.add(current.PT_DSMP_Account_Objectives__c);
        }

        System.debug('mgr setId  ' + setId);

        try{

        	if(!setId.isEmpty())
            	PT_DSMP_AP01_CountTasksRecords.countTaskRecords(setId);

        } catch(Exception e){
            System.debug('mgr Exception e ' + e);
        }
    }
    

    global void finish(Database.BatchableContext BC) {
        
    }
    
     // Method to schedule batch
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new PT_DSMP_BAT02_CountTasksRecords());
    }



	
}