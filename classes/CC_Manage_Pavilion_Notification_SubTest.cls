@isTest
public with sharing class CC_Manage_Pavilion_Notification_SubTest {
    
    @isTest
    private static void testgetInit(){
        
        
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';
        ord1.CC_Carrier_ID__c = 'CPD';
        
        insert ord1;
        System.debug('recordId'+ord1.Id);
        Test.startTest();
        CC_Manage_Pavilion_Notification_SubCtrl.getInit(ord1.Id);
        Test.stopTest();
    }
    
    @isTest
    private static void testgetInit1(){
        
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        insert ord1;
        
        CC_Order_Item__c orditem1 = TestUtilityClass.createOrderItem(ord1.Id);
        insert orditem1;
        
        
        CC_Car_Item__c carItem1 = new CC_Car_Item__c();
        carItem1.Order_Item__c=orditem1.Id;
        
        insert carItem1;
        System.debug('carItem1'+carItem1);
        Test.startTest();
        try{
            CC_Manage_Pavilion_Notification_SubCtrl.getInit(carItem1.Id);
            
        }catch(Exception e){
            
        }
        
        Test.stopTest();
    }
    
    @isTest
    private static void testgetInit2(){
        
        
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        User testUser = [Select Id,IsPortalEnabled From User Where isActive = true LIMIT 1];
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';
        ord1.CC_Carrier_ID__c = 'CPD';
        
        
        Test.startTest();
        CC_Manage_Pavilion_Notification_SubCtrl.getInit(ord1.Id);
        CC_LoadTrackingUtilities.getPortalUserSubscriptionsAvailable();
        CC_Manage_Pavilion_Notification_SubCtrl.SubscriptionOption  ccs=new CC_Manage_Pavilion_Notification_SubCtrl.SubscriptionOption();
        Test.stopTest();
    }
    
    @isTest
    private static void testgetInit3(){
        CC_Manage_Pavilion_Notification_SubCtrl.InitResponse obs = new CC_Manage_Pavilion_Notification_SubCtrl.InitResponse();
        obs.isPortalUser = true;
        
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        User testUser = [Select Id,IsPortalEnabled From User Where isActive = true LIMIT 1];
        
        //Map<String, CC_Pavilion_Notification_Type__c[]> notificationsByObject = CC_LoadTrackingUtilities.getInternalUserSubscriptionsAvailable(); 
        Map<String, CC_Pavilion_Notification_Type__c[]> internalUserSubscriptionsAvailable = new Map<String, CC_Pavilion_Notification_Type__c[]>();
        for (CC_Pavilion_Notification_Type__c notificaitonType :  
                [Select Id, Name, Description__c, Type__c, Object_for_Change_Detection__c, Field_for_Change_Detection__c, Available_Delivery_Frequencies__c, Available_Delivery_Methods__c 
                From CC_Pavilion_Notification_Type__c 
                Where RecordType.DeveloperName = 'Load_Tracking' and Active__c = true
                Order By Sort_Order__c ASC]){
                System.debug('=>'+notificaitonType);
                internalUserSubscriptionsAvailable.put(notificaitonType.Object_for_Change_Detection__c, new CC_Pavilion_Notification_Type__c[]{notificaitonType});
                
         }
        
        System.debug('=>'+internalUserSubscriptionsAvailable);
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';
        ord1.CC_Carrier_ID__c = 'CPD';
        
        Test.startTest();
        CC_Manage_Pavilion_Notification_SubCtrl.getInit(ord1.Id);
        CC_LoadTrackingUtilities.getPortalUserSubscriptionsAvailable();
        CC_Manage_Pavilion_Notification_SubCtrl.SubscriptionOption  ccs=new CC_Manage_Pavilion_Notification_SubCtrl.SubscriptionOption();
        Test.stopTest();
    }
    
}