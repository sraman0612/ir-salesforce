/*****************************************************************
Name:      RS_CRS_Utility
Purpose:   Utility class having static methods
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Neela Prasad	01/10/2018		INITIAL DEVELOPMENT
*****************************************************************/
public class RS_CRS_Utility {

    /******************************************************************************
    Purpose            :  Method used to get record type id by name                                                
    Parameters         :  Object name, Record type name
    Returns            :  Recordtype ID
    Throws [Exceptions]:                                                          
    ******************************************************************************/
	public static Id getRecordTypeIdFromRecordTypeName(String objectName, String recordTypeName) { 
        Id recTypeId = Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return recTypeId;
    }
}