/**
	@author Ben Lorenz
	@date 24JAN19
	@description Test class for CC_GenericListController
*/

@isTest
private class CC_GenericListControllerTest {
	
    @testSetup
    static void setupData() {
    	Knowledge__kav knowledge = CC_Test_Utility.createKnowledge('test Article', 'test-Article1','This is a test article', true);        
        Knowledge__kav resultKnowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        
        Id RTId = [SELECT Id FROM RecordType WHERE sObjectType='ContentVersion' AND Name='Tech Pubs'].Id;        
        ContentVersion contentVersion = CC_Test_Utility.createContentVersion('testcontent.jpg' , 'Test content 1'  , 'test-content',RTId, true);
        String cdId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion.Id].ContentDocumentId;
        ContentDocumentLink docLink = new ContentDocumentLink ();
        docLink.LinkedEntityId = resultKnowledge.Id;
        docLink.ContentDocumentId = cdId;
        docLink.ShareType = 'I';
        insert docLink;
        
        KbManagement.PublishingService.publishArticle(resultKnowledge.KnowledgeArticleId, true);                
    }
    
    static testMethod void testList() {
        Knowledge__kav resultKnowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav];
        CC_GenericListController.getData(resultKnowledge.Id);
    }
}