public with sharing class CC_Case_Trigger_ContactHandler {
    
    private static Email_Message_Settings__c settings;
    private static Set<Id> ccCaseRecordTypeIds;
    private static Set<Id> ccContactRecordTypeIds;    
    
    static{
    	
    	settings = Email_Message_Settings__c.getOrgDefaults();
        system.debug('CC_Case_Trigger_ContactHandler');
    	system.debug('settings: ' + settings);
    	
    	ccCaseRecordTypeIds = String.isNotBlank(settings.CC_Email_Case_Record_Type_IDs__c) ? new Set<Id>((List<Id>)settings.CC_Email_Case_Record_Type_IDs__c.split(',')) : new Set<Id>();
    	ccContactRecordTypeIds = String.isNotBlank(settings.CC_Contact_Record_Type_IDs_to_Match__c) ? new Set<Id>((List<Id>)settings.CC_Contact_Record_Type_IDs_to_Match__c.split(',')) : new Set<Id>();   	
    }    
    
    public static void onBeforeInsert(Case[] cases){
    
    	if (settings.CC_Case_Contact_Trigger_Enabled__c && !ccCaseRecordTypeIds.isEmpty() && !ccContactRecordTypeIds.isEmpty()){
    		
    		// Ensure that any contact assigned via E2C is of the applicable CC contact record types
    		Case_TriggerHandlerHelper.assignContacts(cases, ccCaseRecordTypeIds, ccContactRecordTypeIds, 'CC_Contact_Match_Status__c');
    	}
    }    
}