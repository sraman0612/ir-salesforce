public class CC_PartsOrderDetailPageController {
	public CC_Order__c order {get; set;}
	public CC_PartsOrderDetailPageController(CC_PavilionTemplateController controller) {
	  order = [SELECT Id, Sold_To_Addressee__c,Sold_To_Address_Line_1__c,Sold_To_Address_Line_2__c,Sold_To_Address_Line_3__c,
	                  Sold_To_Address_Line_4__c,Sold_To_Address_Line_5__c,Sold_To_City__c,Sold_To_Contact__c,Sold_To_Country__c,Sold_To_Fax__c,
	                  Sold_To_Phone__c,Sold_To_Postal_Code__c,Sold_To_State__c,Bill_To_Addressee__c,Bill_To_Address_Line_1__c,
	                  Bill_To_Address_Line_2__c,Bill_To_Address_Line_3__c,Bill_To_Address_Line_4__c,Bill_To_Address_Line_5__c,Bill_To_City__c,
	                  Bill_To_Contact__c,Bill_To_Country__c,Bill_To_Fax__c,Bill_To_Phone__c,Bill_To_Postal_Code__c,Bill_To_State__c,Name,
	                  CC_Sales_Person_Number__r.Name,PO__c, CC_Order_Number_Reference__c,CC_Request_Date__c, CC_Delivery_Date__c, CC_LoadDrop__c,
	                  CC_Carrier_ID__c, CC_Terms__c, Total_Invoice_Amount__c,CC_Acknowledge_Date__c,CC_Acknowledge_By__c,CC_Changes__c,
	                  Order_Number__c,CC_Addressee_name__c,CC_Address_line_1__c,CC_Address_line_2__c,CC_Address_line_3__c,Address_line_4__c,
	                  Address_line_5__c,CC_City__c,CC_States__c,CC_Postal_code__c,CC_Country__c,CC_Contact_name__c,CC_Telephone_number__c,CC_Fax_number__c,
	                  (SELECT Id,Name,CC_Description__c,CC_Quantity__c,CC_UnitPrice__c,CC_TotalPrice__c, Product__r.ProductCode,Product__r.Name FROM Order_Item__r)
			           FROM CC_Order__c 
			          WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
	}
}