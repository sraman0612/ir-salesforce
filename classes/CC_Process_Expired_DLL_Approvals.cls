public with sharing class CC_Process_Expired_DLL_Approvals implements Database.Batchable<sObject>, Schedulable{
    
 	/* Scheduled job implementation */
    public void execute(SchedulableContext sc) {        
		Database.executeBatch(new CC_Process_Expired_DLL_Approvals());
    }
    
    /* Batch job implementation */
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        Date today = Date.today();
        
        /* Find all club car orders that meet the following criteria:
        	1) Expired DLL approval
        	2) No cars shipped yet
        	3) No order on hold record in the ERP message queue yet (indicating already processed by the batch job)
    	*/
        
    	return Database.getQueryLocator(
    		'Select Id, Order_Number__c From CC_Order__c ' + 
    		'Where DLL_Approval_Expiration_Date__c < :today and ' + 
    		'Id not in (Select Order__c From CC_Order_Shipment__c Where Ship_Date__c < :today) and ' + 
    		'Id not in (Select CC_Order__c From CC_ERPMQ__c Where Type__c = \'ORDER_HOLD\')'
		);
   	} 
   	
   	// Place each order found in the ERP message queue to put the order on hold in MAPICS
   	public void execute(Database.BatchableContext BC, List<CC_Order__c> orders){
   		
   		CC_ERPMQ__c[] messageQueue = new CC_ERPMQ__c[]{};
   		
   		for (CC_Order__c order : orders){
   			
   			system.debug('processing order: ' + order);
   			
   			messageQueue.add(new CC_ERPMQ__c(
   				Key__c = order.Order_Number__c,
   				Type__c = 'ORDER_HOLD',
   				CC_Order__c = order.id
   			));
   		}
   		
   		system.debug('messageQueue: ' + messageQueue);
   		
   		if (messageQueue.size() > 0){
   			sObjectService.insertRecordsAndLogErrors(messageQueue, 'CC_Process_Expired_DLL_Approvals', 'execute: batch');
   		}
   	}
   	
   	public void finish(Database.BatchableContext BC){
		
   	}   	   
}