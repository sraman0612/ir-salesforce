@isTest
public with sharing class CC_KochLoadUpdateBatchTest {

    @TestSetup 
    private static void createData(){

        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';

        CC_Order__c ord2 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord2.CC_MAPICS_Order_Key__c = '2158784';

        insert new CC_Order__c[]{ord1, ord2};
        
        CC_Order_Item__c orditem1 = TestUtilityClass.createOrderItem(ord1.Id);
        CC_Order_Item__c orditem2 = TestUtilityClass.createOrderItem(ord1.Id);
        insert new CC_Order_Item__c[]{orditem1, orditem2};               

        CC_Car_Item__c carItem1 = TestUtilityClass.createCarItem(orditem1.Id);
        carItem1.Car_Item_Key__c = '01_1_2158783_1_1';

        CC_Car_Item__c carItem2 = TestUtilityClass.createCarItem(orditem2.Id);
        carItem2.Car_Item_Key__c = '01_1_2158784_1_1';

        CC_Car_Item__c carItem3 = TestUtilityClass.createCarItem(orditem2.Id);      
        carItem3.Car_Item_Key__c = '01_1_2158784_1_2';

        insert new CC_Car_Item__c[]{carItem1, carItem2, carItem3};
    }

    @isTest
    private static void testBatchSuccess(){

        Test.startTest(); 
  
        Database.executeBatch(new CC_KochLoadUpdateBatch());

        Test.stopTest();

        // Verify no errors were logged            
        system.assertEquals(0, [Select Count() From Apex_Log__c Where Class_Name__c = 'CC_KochLoadUpdateBatch']);

        // Verify custom settings were updated
        DateTime lastUpdateRun = CC_Koch_Settings__c.getOrgDefaults().Last_Load_Update_Batch_Run_Date__c;
        system.assert(lastUpdateRun != null && lastUpdateRun <= DateTime.now());    
        
        // Verify the 2 car items with a match were updated
        Map<String, CC_Car_Item__c> carItemKeyMap = new Map<String, CC_Car_Item__c>();

        for (CC_Car_Item__c carItem : 
            [Select Id, Car_Item_Key__c, Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c,
                Koch_Scheduled_Delivery_Date__c, Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c
            From CC_Car_Item__c]){

            carItemKeyMap.put(carItem.Car_Item_Key__c, carItem);
        }

        CC_Car_Item__c carItem1 = carItemKeyMap.get('01_1_2158783_1_1');
        system.assertEquals(null, carItem1.Koch_Scheduled_Pickup_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 2:50:00 AM'), carItem1.Koch_Actual_Pickup_Date__c);
        system.assertEquals(null, carItem1.Koch_Scheduled_Delivery_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 9:15:02 AM'), carItem1.Koch_Estimated_Delivery_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 9:15:02 AM'), carItem1.Koch_Actual_Delivery_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/8/2020 11:00:01 PM'), carItem1.Koch_Last_Update_Date__c);
        system.assert(carItem1.Koch_Last_Check_Date__c != null && carItem1.Koch_Last_Check_Date__c <= DateTime.now());

        CC_Car_Item__c carItem2 = carItemKeyMap.get('01_1_2158784_1_1');
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 3:00:00 AM'), carItem2.Koch_Scheduled_Pickup_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 2:50:00 AM'), carItem2.Koch_Actual_Pickup_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 9:15:00 AM'), carItem2.Koch_Scheduled_Delivery_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 9:00:00 AM'), carItem2.Koch_Estimated_Delivery_Date__c);
        system.assertEquals(null, carItem2.Koch_Actual_Delivery_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/6/2020 11:00:00 PM'), carItem2.Koch_Last_Update_Date__c);
        system.assert(carItem2.Koch_Last_Check_Date__c != null && carItem2.Koch_Last_Check_Date__c <= DateTime.now());      
        
        CC_Car_Item__c carItem3 = carItemKeyMap.get('01_1_2158784_1_2');
        system.assertEquals(null, carItem3.Koch_Scheduled_Pickup_Date__c);
        system.assertEquals(null, carItem3.Koch_Actual_Pickup_Date__c);
        system.assertEquals(null, carItem3.Koch_Scheduled_Delivery_Date__c);
        system.assertEquals(null, carItem3.Koch_Estimated_Delivery_Date__c);
        system.assertEquals(null, carItem3.Koch_Actual_Delivery_Date__c);
        system.assertEquals(null, carItem3.Koch_Last_Update_Date__c);
        system.assertEquals(null, carItem3.Koch_Last_Check_Date__c);         
    }     

    @isTest
    private static void testBatchFail(){

        CC_KochSoapAPIMock.forceError = true;

        Test.startTest();   

        Database.executeBatch(new CC_KochLoadUpdateBatch());

        Test.stopTest();
        
        // Verify an error was logged
        system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c = 'CC_KochLoadUpdateBatch' and Method_Name__c = 'execute']);

        // Verify custom settings were updated
        DateTime lastUpdateRun = CC_Koch_Settings__c.getOrgDefaults().Last_Load_Update_Batch_Run_Date__c;
        system.assert(lastUpdateRun != null && lastUpdateRun <= DateTime.now());          
    }       
}