@isTest
private class RshvacQuoteTriggerHandler_Test {

    // Account Name
    public static final String ACCOUNT_NAME = 'Some Name';
    // Developer Name of the RS_HVAC Record type
    private static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
    // Opprtunity Stages
    public static final String REQUEST = 'Request';
    public static final String FULLFIL = RshvacQuoteTriggerHandler.FULLFIL;
    public static final String ACCEPTED = RshvacQuoteTriggerHandler.ACCEPTED;
    public static final String COMMITTED = RshvacQuoteTriggerHandler.COMMITTED;
    public static final String CLOSED_WON = RshvacQuoteTriggerHandler.CLOSED_WON;
    public static String LOSS = 'LOSS';
    public static final String PENDING_REVIEW = RshvacQuoteTriggerHandler.PENDING_REVIEW;
    public static final Set<String> ALL_STAGES = new Set<String>{REQUEST, FULLFIL, CLOSED_WON, PENDING_REVIEW,ACCEPTED,COMMITTED,LOSS};
    RSHVAC_Properties__c IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
    // Sample Extended Gross Sales
    private static Decimal EXT_GROSS = 500;
    
    /**
     *  Sets the data need for the tests
     */
    @testSetup static void mySetup() {
        RSHVAC_Properties__c IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
        insert IntegrationUser;
        List<RSHVAC_Quote__c> quotes = new List<RSHVAC_Quote__c>();
        
        // Get the RS_HVAC Record Type Id
        Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
        // Insert a basic Account, all opportunities will be related to it
        Account baseAccount = new Account(Name = ACCOUNT_NAME, RecordTypeId = rshvacRTID);
        insert baseAccount;
        // Get the Id of the Opportunity Existing BUsiness Record Type
        Id newBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_NEW_BUSINESS).Id;
        // Insert an opportunity for each stage
        List<Opportunity> opps = new List<Opportunity>();
        for (String stage : ALL_STAGES){
            String reason = stage.equalsIgnoreCase(CLOSED_WON) ? 'Cancelled' : '';
            opps.add(new Opportunity(Name = stage, StageName = stage, AccountId = baseAccount.Id, Gross_Sales__c = 1, Amount = 1,
                                    CloseDate = Date.today(), RecordTypeId = newBusinessRecTypeId, Closed_Lost_Reason__c = reason));
        }
        insert opps;
        Opportunity grossOppty = new Opportunity(Name = 'Gross', StageName = 'Request', AccountId = baseAccount.Id, Gross_Sales__c = 0, Amount = 1,
                                    CloseDate = Date.today(), RecordTypeId = newBusinessRecTypeId, Closed_Lost_Reason__c = 'reason');
        insert grossOppty;
        quotes.add(new RSHVAC_Quote__c(Opportunity__c = grossOppty.Id, Extended_Gross_Sales__c = EXT_GROSS));
        insert quotes;
        //System.debug('Jaya :'+ RshvacNewBus_OpportunityTriggerHandler.IntegrationUser.Value__c+'  '+Userinfo.getName());
        //System.assertEquals(IntegrationUser);
        //System.assertNotEquals();
    }

    /**
     *  Tests that the logic on the insert trigger is correct
     */
    static testMethod void testInsertQuote() {
        Integer grossSalesNotZero = 10;
            
        // Query the opportunities created on the setup
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity WHERE Name IN :ALL_STAGES];
        // Create a quote for each of them
        List<RSHVAC_Quote__c> quotes = new List<RSHVAC_Quote__c>();
        for (Opportunity o : opps){
            Integer grossSales = !REQUEST.equalsIgnoreCase(o.Name) ? grossSalesNotZero : 0;
            quotes.add(new RSHVAC_Quote__c(Opportunity__c = o.Id, Extended_Gross_Sales__c = grossSales));
        }
        Test.startTest();
            insert quotes;
        Test.stopTest();
        // Check the values on the related opportunities after the insert
        opps = [SELECT Id, Name, StageName, Gross_Sales__c FROM Opportunity WHERE Name IN :ALL_STAGES];
        for (Opportunity o : opps){
            if (REQUEST.equalsIgnoreCase(o.Name)){
                // The opportunity with original stage equals request should now be Pending Review
                System.assertEquals(REQUEST, o.StageName);
                System.assertEquals(0, o.Gross_Sales__c);
            } else if (PENDING_REVIEW.equalsIgnoreCase(o.Name)){
                System.assertEquals(FULLFIL, o.StageName);
                System.assertEquals(grossSalesNotZero, o.Gross_Sales__c);
            } else if (FULLFIL.equalsIgnoreCase(o.Name) || CLOSED_WON.equalsIgnoreCase(o.Name)){
                System.assertEquals(o.Name, o.StageName);
                System.assertEquals(grossSalesNotZero, o.Gross_Sales__c);
            }
              else if (ACCEPTED.equalsIgnoreCase(o.Name) || CLOSED_WON.equalsIgnoreCase(o.Name)){
                System.assertEquals('Fulfill', o.StageName);
                System.assertEquals(grossSalesNotZero , o.Gross_Sales__c);
            }
              else if (LOSS.equalsIgnoreCase(o.Name) || CLOSED_WON.equalsIgnoreCase(o.Name)){
                System.assertEquals('Fulfill', o.StageName);
           }       
        }
        
        Opportunity grossOppty = [Select id, Gross_Sales__c from Opportunity where name = 'Gross'];
        RSHVAC_Quote__c grossQuote = new RSHVAC_Quote__c(Opportunity__c = grossOppty.Id, Extended_Gross_Sales__c = EXT_GROSS);
        insert grossQuote;
        grossOppty = [Select id, Gross_Sales__c from Opportunity where name = 'Gross'];
        System.assertEquals(grossOppty.Gross_Sales__c , 2*EXT_GROSS);
    }

    /**
     *  Tests that the logic on the update trigger is correct
     */
    static testMethod void testUpdateQuote() {
        Integer grossSalesNotZero = 10;
        // Query the opportunities created on the setup
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity WHERE Name IN :ALL_STAGES];
        // Create a quote for each of them
        List<RSHVAC_Quote__c> quotes = new List<RSHVAC_Quote__c>();
        for (Opportunity o : opps){
            quotes.add(new RSHVAC_Quote__c(Opportunity__c = o.Id, Extended_Gross_Sales__c = 0));
        }
        insert quotes;
        Test.startTest();
            for (RSHVAC_Quote__c q : quotes){
                q.Extended_Gross_Sales__c = 10;
            }
            update quotes;
        Test.stopTest();
        // Check the values on the related opportunities after the Update
        opps = [SELECT Id, Name, StageName, Gross_Sales__c FROM Opportunity WHERE Name IN :ALL_STAGES];
        for (Opportunity o : opps){
            System.assertEquals(grossSalesNotZero, o.Gross_Sales__c);
            if (CLOSED_WON.equalsIgnoreCase(o.Name)){
              System.assertEquals(CLOSED_WON, o.StageName);
                } else if (ACCEPTED.equalsIgnoreCase(o.Name) || CLOSED_WON.equalsIgnoreCase(o.Name)){
                System.assertEquals('Fulfill', o.StageName);
                System.assertEquals(grossSalesNotZero , o.Gross_Sales__c);
                }
                 else if (LOSS.equalsIgnoreCase(o.Name) || CLOSED_WON.equalsIgnoreCase(o.Name)){
                System.assertEquals('Fulfill', o.StageName);
           }       
        } 
        
        Opportunity grossOppty = [Select id, Gross_Sales__c from Opportunity where name = 'Gross'];
        RSHVAC_Quote__c grossQuote = new RSHVAC_Quote__c(Opportunity__c = grossOppty.Id, Extended_Gross_Sales__c = EXT_GROSS);
        insert grossQuote;
        grossQuote = [Select id,Extended_Gross_Sales__c, Opportunity__c from RSHVAC_Quote__c where Id = :grossQuote.Id];
        grossQuote.Extended_Gross_Sales__c = 2*EXT_GROSS;
        update grossQuote;
        grossOppty = [Select id, Gross_Sales__c from Opportunity where name = 'Gross'];
        System.assertEquals(grossOppty.Gross_Sales__c , 3*EXT_GROSS);
        delete grossQuote;
        grossOppty = [Select id, Gross_Sales__c from Opportunity where name = 'Gross'];
        System.assertEquals(grossOppty.Gross_Sales__c , EXT_GROSS);
    }
      
}