@isTest
public class PT_OpportunityTriggerHandlerTest 
{
     @testSetup static void setup() {
        Account testAccount = new Account();
        Id ptAcctRTId = [Select Id, DeveloperName from RecordType where DeveloperName = 'PT_powertools' and SobjectType='Account' limit 1].Id;
        testAccount.RecordTypeId = ptAcctRTId;
        testAccount.BillingCountry='Belgium';
        testAccount.Name = 'TestAccount';
        testAccount.Type = 'Customer';
        testAccount.PT_Status__c = 'New';
        testAccount.PT_IR_Territory__c = 'North';
        testAccount.PT_IR_Region__c = 'EMEA';
        insert testAccount;
         
        PT_Parent_Opportunity__c testParentOpportunity = new PT_Parent_Opportunity__c();
        testParentOpportunity.Account__c = testAccount.Id;
        testParentOpportunity.Lead_SourcePL__c = 'Ingersollrandproducts.com';
        insert testParentOpportunity;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.PT_Parent_Opportunity__c = testParentOpportunity.Id;
        testOpportunity.LeadSource = 'LinkedIn Prospect';
        testOpportunity.name = 'TestUpdateOpportunity';
        testOpportunity.stagename = 'Qualify';
        testOpportunity.amount = 1000000;
        testOpportunity.closedate = system.today();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
        testOpportunity.AccountId = testAccount.Id;
        insert testOpportunity;
     }

	public static testMethod void testTrigger()
    {
		Account testAccount = [Select id from Account where Name = 'TestAccount'];
       
		Opportunity testUpdateOpportunity = [Select id from Opportunity where Name = 'TestUpdateOpportunity'];    
        testUpdateOpportunity.amount = 1500000;
        testUpdateOpportunity.closedate = system.today().addDays(100);
        update testUpdateOpportunity;
        
        delete testUpdateOpportunity;
    }
}