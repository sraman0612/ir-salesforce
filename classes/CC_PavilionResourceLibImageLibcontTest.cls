@isTest
public class CC_PavilionResourceLibImageLibcontTest {
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
                
        insert psettingList;
        
        // Creation Of Test Data
        List<Contract> cntList =new List<Contract>();
        List<ContentVersion> contverList =new List<ContentVersion>();
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c = TestUtilityClass.createContact(acc.id);
        insert c;
        Contract firstcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Commercial Utility', acc.id);
        cntList.add(firstcnt);
        System.assertEquals('Commercial Utility', firstcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,firstcnt.AccountId);
        Contract secondcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Distributor', acc.id);
        cntList.add(secondcnt);
        System.assertEquals('Golf Car Distributor', secondcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,secondcnt.AccountId);
        Contract thirdcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Industrial Utility', acc.id);
        cntList.add(thirdcnt);
        System.assertEquals('Industrial Utility', thirdcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,thirdcnt.AccountId);
        insert cntList;
        RecordType ContentRT = [select Id FROM RecordType WHERE Name = 'Marketing Images']; 
        ContentVersion firstcv = TestUtilityClass.createContentVersionWithAgreementType('Marketing Images','Commercial Utility;Golf Car Distributor;Industrial Utility;XRT Utility');
        System.assertEquals(ContentRT.Id,firstcv.RecordTypeId);
        System.debug('the firstcv agreement type is'+firstcv.Agreement_Type__c);
        ContentVersion secondcv = TestUtilityClass.createContentVersionWithAgreementType('Marketing Images','Commercial Utility;Industrial Utility;Golf Car Distributor;XRT Utility');
        ContentVersion thirdcv = TestUtilityClass.createContentVersionWithAgreementType('Marketing Images','Commercial Utility;Golf Car Distributor;Industrial Utility;XRT Utility');
        contverList.add(firstcv);
        contverList.add(secondcv);
        contverList.add(thirdcv);
        insert contverList;
    }
    
    @isTest
    static void UnitTest_ResourceLibImageLibcont()
    {        

              
        //Account acc=[select id from Account limit 1];
        //Contact ct=[select id from Contact where AccountId=:acc.id];

        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        Account portalAccount1 = TestUtilityClass.createAccount();
        portalAccount1.ownerId = UserInfo.getUserId();
        
      /*  Account portalAccount1 = new Account(
            Name='Setup Sample Account',CC_Price_List__c='PCOPI',
            Address_1__c='test', Address_2__c='test', Address_3__c='test',
            OwnerId = UserInfo.getUserId()
        );*/
        portalAccount1.RecordtypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        Database.insert(portalAccount1);
        
        test.starttest();
        //Create contact
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            Email = System.now().millisecond() + 'test@test.ingersollrand.com',
            AccountId = portalAccount1.Id,Phone ='5435345345',
            CC_Pavilion_Admin__c=true,
            Title ='testcont',MobilePhone ='9874561230',
            Department = 'TestData'
        );
        Database.insert(contact1);
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'CC_PavilionCommunityUser' Limit 1];
        User user1 = new User(
            Username = System.now().millisecond() + 'test12345@testijny.com',
            ContactId = contact1.Id,
            isActive = true,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            CC_Pavilion_Navigation_Profile__c = 'US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN'
        );
       
         System.RunAs(usr)
         {
        
            insert user1; 
           
         }
        
        User uu = [select id,AccountId from User where id=:user1.id limit 1];
        
        Contract Fourthcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Industrial Utility', uu.AccountId);
        insert Fourthcnt;
        System.runAs(uu)
        {
            
            CC_PavilionTemplateController controller;
            CC_PavilionResourceLibImageLibcont ResImgLib =new CC_PavilionResourceLibImageLibcont(controller);
            ResImgLib.getImageDataFilter();
            CC_PavilionResourceLibImageLibcont.getMarkettingTypes();
            CC_PavilionResourceLibImageLibcont.getMarketingImages();
            CC_PavilionResourceLibImageLibcont.getMarkettingCategories();
            CC_PavilionResourceLibImageLibcont.getMarkettingSubTypes();
            CC_PavilionResourceLibImageLibcont.getfiletredMarketingImages('All');
             
        }
        test.stopTest();
        
    }
    
}