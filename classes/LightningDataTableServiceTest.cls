@isTest
public with sharing class LightningDataTableServiceTest {

    @isTest
    private static void testLink(){

        Test.startTest();

        LightningDataTableService.Column column = new LightningDataTableService.Column(
            'Test Column', 
            'Name', 
            'text', 
            true, 
            new LightningDataTableService.ColumnTypeAttributesLink(new LightningDataTableService.ColumnLabel('Name'), '_blank')
        );

        system.assertEquals('Test Column', column.label);
        system.assertEquals('Name', column.fieldName);
        system.assertEquals('text', column.type);

        LightningDataTableService.ColumnLabel label = new LightningDataTableService.ColumnLabel('Name');
        system.assertEquals(label.fieldName, ((LightningDataTableService.ColumnTypeAttributesLink)column.typeAttributes).label.fieldName);
        system.assertEquals('_blank', ((LightningDataTableService.ColumnTypeAttributesLink)column.typeAttributes).target);

        Test.stopTest();
    }

    @isTest
    private static void testButton(){

        Test.startTest();

        LightningDataTableService.Column column = new LightningDataTableService.Column(
            'Test Column', 
            'Name', 
            'button', 
            true, 
            new LightningDataTableService.ColumnTypeAttributesButton('Button Label', 'Button Title', 'brand', 'action', false)
        );

        system.assertEquals('Test Column', column.label);
        system.assertEquals('Name', column.fieldName);
        system.assertEquals('button', column.type);
        system.assertEquals('Button Label', ((LightningDataTableService.ColumnTypeAttributesButton)column.typeAttributes).label);
        system.assertEquals('Button Title', ((LightningDataTableService.ColumnTypeAttributesButton)column.typeAttributes).title);
        system.assertEquals('brand', ((LightningDataTableService.ColumnTypeAttributesButton)column.typeAttributes).variant);
        system.assertEquals('action', ((LightningDataTableService.ColumnTypeAttributesButton)column.typeAttributes).iconName);
        system.assertEquals(false, ((LightningDataTableService.ColumnTypeAttributesButton)column.typeAttributes).disabled);

        Test.stopTest();
    }    

    @isTest
    private static void testAction(){

        Test.startTest();

        LightningDataTableService.Column column = new LightningDataTableService.Column(
            'Test Column', 
            null, 
            'action', 
            false, 
            new LightningDataTableService.ColumnTypeAttributesAction(
                new LightningDataTableService.ColumnTypeAttributesActionRow[]{
                    new LightningDataTableService.ColumnTypeAttributesActionRow('Manager Override', 'manager_override')
                }
            )
        );

        system.assertEquals('Test Column', column.label);
        system.assertEquals(null, column.fieldName);
        system.assertEquals('action', column.type);
        LightningDataTableService.ColumnTypeAttributesAction columnAttributes = ((LightningDataTableService.ColumnTypeAttributesAction)column.typeAttributes);
        system.assertEquals(1, columnAttributes.rowActions.size());
        system.assertEquals('Manager Override', columnAttributes.rowActions[0].label);
        system.assertEquals('manager_override', columnAttributes.rowActions[0].name);

        Test.stopTest();
    }      
}