/**
    @author Providity
    @date 27MAR2019
    @description Test class for CTS_TD_ChatterMentionFollowers
*/

@isTest(SeeAllData=true) //ConnectApi doesn't work in test data silos
private class CTS_TD_ChatterMentionFollowersTest {
    
    @isTest
    static void testMentions() {
        
        Id profileId = [Select Id From Profile Where Name = 'System Administrator' LIMIT 1].Id;
        Id roleId = [Select Id From UserRole Where Name = 'CTS TechDirect BU Global' LIMIT 1].Id; 
        User u = TestUtilityClass.createNonPortalUser(profileId);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {        	           
            u.Username += '1';
            u.FederationIdentifier = '1234560';
            u.DB_Region__c = 'EMEIA';
            u.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';
            u.UserRoleId = roleId;
            u.UserPermissionsKnowledgeUser = true;
            insert u;   
        }        
        
        CTS_Article_Search_Config__c  config = CTS_Article_Search_Config__c.getInstance();
        String ctsTDRTName = config.Record_Type__c != null ? config.Record_Type__c : 'IR Comp TechDirect Knowledge Article';
        
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName();
        Id rtId =  rtMapByName.containsKey(ctsTDRTName) ? rtMapByName.get(ctsTDRTName).getRecordTypeId() : null;
        
        //Create article
      /*  Knowledge__kav akv = new Knowledge__kav();
        akv.UrlName='Test-Url';
        akv.Title='Test Title';
        akv.RecordTypeId = rtId;
        akv.CTS_TechDirect_Author_Reviewer__c = u.Id;
        akv.CTS_Custom_Version_Number__c = 0;*/
        
        
        Knowledge__kav akv = new Knowledge__kav(title = 'Test1', urlName = 'Test1' ,CTS_TechDirect_Article_Type__c = 'Test', language='en_US',CTS_TechDirect_Author_Reviewer__c = u.Id, CTS_Custom_Version_Number__c = 0);
        akv.RecordTypeId = Schema.SObjectType.knowledge__kav.getRecordTypeInfosByName().get('IR Comp TechDirect Knowledge Article').getRecordTypeId();
        
        
        System.runAs(new User(Id = u.Id)) {
          
                insert akv;
        //}
        
        //Publish
        Knowledge__kav kb1 = [SELECT Id,Title,KnowledgeArticleId,PublishStatus,RecordTypeId FROM Knowledge__kav WHERE Id =: akv.Id LIMIT 1];
        KbManagement.PublishingService.publishArticle(kb1.KnowledgeArticleId, true);                
        
        kb1 = [SELECT Id,Title,KnowledgeArticleId,PublishStatus,RecordTypeId FROM Knowledge__kav WHERE Id =: akv.Id LIMIT 1];
        
        String ctsTDNetworkId = [SELECT Id FROM Network WHERE Name = 'IR Comp TechDirect'].Id;
        EntitySubscription es = new EntitySubscription();
        es.NetworkId = ctsTDNetworkId;
        es.SubscriberId = u.id;
        es.ParentId = kb1.KnowledgeArticleId;
        insert es;
        
        //Edit and update
		String kb2id = KbManagement.PublishingService.editOnlineArticle (kb1.KnowledgeArticleId, true);
        Knowledge__kav kb2 = [SELECT Id,Title,KnowledgeArticleId,PublishStatus,RecordTypeId FROM Knowledge__kav WHERE Id =: kb2id LIMIT 1];
        kb2.Title = 'Test Title 2';
        kb2.CTS_Custom_Version_Number__c = 1;
         System.runAs(new User(Id = u.Id)) {
        update kb2;
         }
        KbManagement.PublishingService.publishArticle(kb2.KnowledgeArticleId, true);//Publish again
        }
    }
}