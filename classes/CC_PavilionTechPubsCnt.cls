/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */
global with sharing class CC_PavilionTechPubsCnt {

 public CC_PavilionTechPubsCnt(CC_PavilionTemplateController controller) {

 }

 global String searchStr {
  get;
  set;
 }
 public String dateFrom {
  get;
  set;
 }
 public String dateTo {
  get;
  set;
 }

 public void testdate() {
  system.debug('@@@@@@@@@@@@@@' + dateFrom);
  system.debug('@@@@@@@@@@@@@@' + dateTo);
 }
 
 @RemoteAction
 public static set<string> getSNPrefix() {
  set<string> snPrefixes = new set<string>();
  list < Schema.PicklistEntry > values = ContentVersion.CC_SN_Prefix__c.getDescribe().getPickListValues();
 // snPrefixes.add('--None--');
  for (Schema.PicklistEntry a: values) {
   snPrefixes.add(a.getLabel());  
  }
  
  return snPrefixes;
 }

 @RemoteAction
 public static set<string> getYear() {

  set<string> catOptValues = new set<string>();
  list < Schema.PicklistEntry > values =
   ContentVersion.CC_Year__c.getDescribe().getPickListValues();
  //catOptValues.add('--None--');
  for (Schema.PicklistEntry a: values) {
   catOptValues.add(a.getLabel()); 
  }

  return catOptValues;
 }


 @RemoteAction
 public static set < string > getType() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentType = new List < ContentVersion > ();

  contentType = [select id, ContentDocumentId, CS_Category__c, ContentSize, ContentUrl, Description, Title, ContentModifiedDate, Content_Title__c, Content_Sub_title__c, CC_Year__c, CC_Language__c,
                 CC_SN_Prefix__c, RecordType.Name from ContentVersion
                   where RecordType.Name = 'Tech Pubs' and IsLatest = true
                  ];
  for (ContentVersion version: contentType) {

   options.add(version.Content_Sub_title__c);
  }
  return options;
 }

 @RemoteAction
 public static set < string > getLanguage() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentLang = new List < ContentVersion > ();

  contentLang = [select id,CC_Publication_Number__c, CC_Revision_Code__c, CC_Kit_Part_Number__c, ContentDocumentId, CS_Category__c, ContentSize, ContentUrl, Description, Title, Content_Title__c, Content_Sub_title__c, CC_Year__c, CC_Language__c,
                 CC_SN_Prefix__c,  RecordType.Name from ContentVersion
                   where RecordType.Name = 'Tech Pubs' and IsLatest = true
                  ];
  for (ContentVersion version: contentLang) {

   options.add(version.CC_Language__c);
  }
  return options;
 }

 @RemoteAction
 public static set < string > getCSCategory() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentCateg = new List < ContentVersion > ();

  contentCateg = [select id, CC_Publication_Number__c, CC_Revision_Code__c, CC_Kit_Part_Number__c, ContentDocumentId, CS_Category__c, ContentSize, ContentUrl, Description, Title, Content_Title__c, Content_Sub_title__c, CC_Year__c, CC_Language__c,
                    CC_SN_Prefix__c,RecordType.Name from ContentVersion
                       where RecordType.Name = 'Tech Pubs' and IsLatest = true
                       and CS_Category__c != null
                      ];
  for (ContentVersion version: contentCateg) {
   options.add(version.CS_Category__c);
  }
  return options;
 }

 @RemoteAction
 global static List < ContentVersion > getTechPubsData(String VideoType) {

  List < ContentVersion > contentList = new List < ContentVersion > ();

  contentList = [select id, CC_Publication_Number__c, CC_Revision_Code__c, CC_Kit_Part_Number__c, ContentDocumentId, CS_Category__c, ContentSize, ContentUrl, Description, ContentModifiedDate, Title, Content_Title__c, Content_Sub_title__c, CC_Year__c, CC_Language__c,
                  CC_SN_Prefix__c, RecordType.Name from ContentVersion
                   where RecordType.Name = 'Tech Pubs' and IsLatest = true
                  ];

  return contentList;

 }
 @RemoteAction
 global static List < ContentVersion > getTechPubsSearchData(String SearchType) {

  List < ContentVersion > contentListToDisplay = new List < ContentVersion > ();
  String searchString = String.escapeSingleQuotes(SearchType);

  if (String.isNotBlank(searchString)) {
   List < List < ContentVersion >> contentList = new List < List < ContentVersion >> ();
   String RecordTypeName = 'Tech Pubs';
   String searchQuery = 'FIND \'%' + searchString + '\' IN ALL FIELDS RETURNING  ContentVersion (Title,CC_Publication_Number__c, CC_Revision_Code__c, CC_Kit_Part_Number__c, Content_Title__c,Content_Sub_title__c,CS_Category__c,CC_Language__c,CC_Year__c,CC_SN_Prefix__c,RecordType.Name WHERE RecordType.Name=:RecordTypeName AND IsLatest = TRUE)';

   contentList = search.query(searchQuery);

   for (List < ContentVersion > cList: contentList) {
    contentListToDisplay.addAll(cList);
   }
  } else {
   contentListToDisplay.addAll(getTechPubsData(''));
  }
  /*  List<ContentVersion> techPubsContentList = new List<ContentVersion>();
    
    techPubsContentList = [select id,VersionData,RecordType.Name from ContentVersion 
                            where RecordType.Name ='Tech Pubs' limit 1]; 
                            
     
   for(ContentVersion conVer : techPubsContentList ) {  
      String content = EncodingUtil.base64Encode(conVer.VersionData); // conVer.VersionData.toString();
      if(content.contains(searchString )) {
             contentListToDisplay.add(conVer);
      }
   } */
  return contentListToDisplay;
 }

 @RemoteAction
 global static List < ContentVersion > getTechPubsDateSearchData(String searchString, String fromDate, String toDate) {

  List < ContentVersion > contentListToDisplay = new List < ContentVersion > ();

  List < String > fromDateLiterals = new List < String > ();
  List < String > toDateLiterals = new List < String > ();

  Date fromD;
  Date toD;

  Integer fromYear;
  Integer toYear;

  if (String.isNotBlank(fromDate)) {
   fromDateLiterals = fromDate.split('/');
   fromD = date.newInstance(Integer.valueof(fromDateLiterals[2]), Integer.valueof(fromDateLiterals[0]), Integer.valueof(fromDateLiterals[1]));
   fromYear = Integer.valueof(fromDateLiterals[2]);
  }

  if (String.isNotBlank(toDate)) {
   toDateLiterals = toDate.split('/');
   toD = date.newInstance(Integer.valueof(toDateLiterals[2]), Integer.valueof(toDateLiterals[0]), Integer.valueof(toDateLiterals[1]));
   toYear = Integer.valueof(toDateLiterals[2]);
  }

  if (String.isNotBlank(fromDate)) {
   List < ContentVersion > contentList = new List < ContentVersion > ();

   contentList = [select id, CC_SN_Prefix__c,ContentDocumentId, CS_Category__c, ContentSize, ContentUrl, Description, ContentModifiedDate, Title, Content_Title__c, Content_Sub_title__c, CC_Year__c, CC_Language__c,
                    CC_Publication_Number__c, CC_Revision_Code__c, CC_Kit_Part_Number__c,  CC_Date__c, RecordType.Name from ContentVersion
                    where RecordType.Name = 'Tech Pubs' and IsLatest = true
                    and CC_Date__c > = : fromD and CC_Date__c < = : toD
                   ];

   contentListToDisplay.addAll(contentList);
  } else {
   contentListToDisplay.addAll(getTechPubsData(''));
  }

  return contentListToDisplay;

 }


}