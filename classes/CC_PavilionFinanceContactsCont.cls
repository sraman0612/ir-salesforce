global without sharing class CC_PavilionFinanceContactsCont{
  public CC_PavilionFinanceContactsCont(CC_PavilionTemplateController controller) {}
  global static List<User> getFinUserContacts(){
    User [] userLst = [SELECT id,Name,Email,Phone,CC_Department__c,PS_Business_Unit_Description__c,Title,Email_To_Display__c,Extension
                         FROM User
                        WHERE PS_Business_Unit_Description__c = 'Corporate' and 
                              CC_Department__c='Finance' AND IsActive = TRUE];  
    return userLst; 
  }
}