/**
 * Test class of CTS_IOT_Contact_ListController
 **/
@isTest
private class CTS_IOT_Contact_ListControllerTest {
    
    @testSetup
    static void setup(){
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        acc.Siebel_ID__c = '1234';
        insert acc;
        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', acc.Id, false));
        insert contacts;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
    }

    @isTest
    static void testGetInit(){

        List<Account> accounts = [SELECT id FROM Account WHERE Name = 'Test Account' LIMIT 1];

        if(!accounts.isEmpty()){

           Test.startTest();

           CTS_IOT_Contact_ListController.InitResponse response = CTS_IOT_Contact_ListController.getInit(null, accounts.get(0).Id, null, 0, 0);
           System.assert(response != null);
           System.assert(response.totalRecordCount > 0);

           Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = [Select Id From Contact][0].Id;
           Test.setFixedSearchResults(fixedSearchResults);            

           CTS_IOT_Contact_ListController.InitResponse response2 = CTS_IOT_Contact_ListController.getInit('Test', accounts.get(0).Id, 10, 0, 0);
           System.assert(response2 != null);
           System.assert(response2.totalRecordCount > 0);           

           System.assertEquals(10, CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId()).User_Selected_List_Page_Size__c);

           Test.stopTest();
        }
    }
    
   @isTest
    static void testCreateUser(){

        List<Contact> contacts = [SELECT id, AccountId FROM Contact WHERE lastName = 'Contact123' LIMIT 1];

        if(!contacts.isEmpty()){

           Test.startTest();

            CTS_IOT_Contact_ListController.CreateUserRequest userRequest = new CTS_IOT_Contact_ListController.CreateUserRequest();
            CTS_IOT_Contact_ListController.NewUser newUser = new CTS_IOT_Contact_ListController.NewUser();
            newUser.FirstName = 'test';
            newUser.LastName = 'New User';
            newUser.Email = 'testcts@test.com.' + String.valueOf(Math.random()*1000000);
            newUser.Phone = 'New User';
            newUser.Title = 'New User';
            userRequest.newUser = newUser;
            userRequest.siteId = contacts.get(0).accountId;

            String userStr = (String)JSON.Serialize(userRequest);
            LightningResponseBase lResponse = CTS_IOT_Contact_ListController.createUser(userStr);
            System.assert(lResponse != null);   
            // Exception Handling testing 
            lResponse = CTS_IOT_Contact_ListController.createUser('{contact:null}');
            Test.stopTest();
       }
    }
    
    @isTest
    static void testActivateUser(){

        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];

        if(!contacts.isEmpty()){

            Test.startTest();
            LightningResponseBase responseBase = CTS_IOT_Contact_ListController.activateUser(contacts.get(0).Id);
            System.assert(responseBase != null);

            // Exception Handling testing 
            responseBase = CTS_IOT_Contact_ListController.activateUser('003000000awe123WAE');
            System.assert(responseBase != null); 
            Test.stopTest();
        }
    }

    @isTest
    static void testdeActivateUser(){

        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];

        if(!contacts.isEmpty()){

            Test.startTest();
            LightningResponseBase responseBase = CTS_IOT_Contact_ListController.deactivateUser(contacts.get(0).Id);
            System.assert(responseBase != null);

            // Exception Handling testing 
            responseBase = CTS_IOT_Contact_ListController.deactivateUser('003000000awe123WAE');
            System.assert(responseBase != null); 
            Test.stopTest();
        }
    }
}