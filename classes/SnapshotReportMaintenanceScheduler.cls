/*
  * Simple scheduler to call the Batch apex class
  *  CREATED FEB 18 2016: POC SGL
  */
 
 
 global class SnapshotReportMaintenanceScheduler Implements Schedulable{
     
     
 global void execute(SchedulableContext sc)
           {
                SnapshotReportMaintenance sr = new SnapshotReportMaintenance();
				database.executeBatch(sr);
            }
}