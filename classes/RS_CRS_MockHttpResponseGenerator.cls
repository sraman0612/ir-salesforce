@isTest
public class RS_CRS_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint and method.
        String endPointURL = 'https://www.comfortsite.com/ebiz/Webservices/TraneSupply/TsApi.svc/GetProductBySerial';
        System.assertEquals(endPointURL + '?serial=16273YUK2F', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String respBody = '[{"HasWarranty":true,"ImageUrl":null,"ModelName":null,"ModelNumber":"4TTR7060A1000BA","SerialNumber":"16273YUK2F"}]';
        res.setBody(respBody);
        res.setStatusCode(200);
        return res;
    }
}