/*****************************************************************
Name:      RS_CRS_AssignRegionQueueBatch_Test
Purpose:   Test class for RS_CRS_AssignRegionQueueBatch
History                                                            
-------                                                            
VERSION     AUTHOR          DATE            DETAIL
1.0         Ashish Takke    02/06/2018      INITIAL DEVELOPMENT
*****************************************************************/
@isTest
public class RS_CRS_AssignRegionQueueBatch_Test {
    
    /*****************************************************************************
	Purpose            :  test mehod for batch class
	******************************************************************************/
    static testMethod void RS_CRS_AssignRegionQueueBatch_Test(){
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        //Create test user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type);
        
        //Getting Asset Record Type Id through Schema.Describe Class
        Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        //get RS CRS Escalation Queue ID
        List<Group> queueList = [SELECT Id, Name 
                                 FROM Group 
                                 WHERE Name =: Label.RS_CRS_Escalation_Queue ];
        
        System.runAs(userRec){
            //insert test Account
            Account acc = new Account();
            acc.recordTypeId = accRecId;
            acc.LastName = 'Test Account 1';
            acc.Phone = '123456789';
            acc.Email_ID__c = 'first.last@email.com';
            acc.BillingState = 'IL';
            acc.BillingCountry = 'USA';
            insert acc;  
            
            //insert test Asset
            Asset assetRec = new Asset();
            assetRec.recordTypeId = assetRecId;
            assetRec.Name = 'Test Asset';
            assetRec.SerialNumber = '12345678';
            assetRec.accountId = acc.Id;
            assetRec.Manufacturer__c = 'Trane';
            insert assetRec;
            
            ////insert test Cases and assign to RS CRS Escalation queue
            List<Case> caseList = new List<Case>();
            for(Integer i=1; i<= 200; i++){
                Case caseObj = new Case();
                caseObj.RecordTypeId = caseRecId;
                caseObj.OwnerId = queueList.get(0).Id;
                caseObj.Subject = 'Test Case' + i;
                caseObj.AccountId = acc.Id;
                caseObj.AssetId = assetRec.Id;
                caseObj.Status = 'Customer Action';
                
                caseList.add(caseObj);
            }
            insert caseList;
        }
        
        Test.StartTest();
        
        RS_CRS_AssignRegionQueue_Scheduler scheduler = new RS_CRS_AssignRegionQueue_Scheduler();
        String sch1 = '0 15 * * * ?';
        System.schedule('Test Assign Region Queue Job 15 minutes', sch1, scheduler);
        
        Test.stopTest();
    }
}