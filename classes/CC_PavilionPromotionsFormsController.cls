/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */
global class CC_PavilionPromotionsFormsController {

 public String PromotionId {
  get;
  set;
 }
 //************ edited by tapas tripathi **********
 public Boolean enableMarkettingManager{get;set;}//this will be used as a flag which will disable some of the fields if the user not a Marketing Managaer.
 //************************************************
 

 public CC_PavilionPromotionsFormsController() {
     //*****************************added by tapas to set the flag **************************************//
    enableMarkettingManager=false;
    Id profileId = UserInfo.getProfileId();
    Profile profileName = [Select Name from Profile where Id =:profileId];
    if(profileName.name=='CC_Marketing Manager'){
        enableMarkettingManager=true;
    }
    //***************************************************************************************************//
 }


 /**Remote method to submit the Golf Car Credit Certification Form **/

 @RemoteAction
 global static String submitGolfCarFormData(String firstName, String lastName, String phone, String email, String buyerFirstName,
  String buyerLastName, String address, String city, String state,
  String zip, String buyerPhone, String buyerEmail, String dateOfSale,
  String dealerName, String vehicleModel, String serialNumber, String modelYear,
  String make, String amount, String billAttachmentName, String billAttachmentBody, String warrantyAttachmentName, String warrantyAttachmentBody) {
  try {
   Marketing_Promotion_Form__c objForm = new Marketing_Promotion_Form__c();
   String recordTypeId = Schema.SObjectType.Marketing_Promotion_Form__c.RecordTypeInfosByName.get('Golf Car').RecordTypeId;
   if (recordTypeId != null) {
    objForm.RecordTypeId = recordTypeId;
   }
   objForm.First_Name__c = firstName;
   objForm.Last_Name__c = lastName;
   objForm.Phone__c = phone;
   objForm.Email__c = email;
   objForm.Buyer_First_Name__c = buyerFirstName;
   objForm.Buyer_Last_Name__c = buyerLastName;
   objForm.Address__c = address;
   objForm.City__c = city;
   objForm.State__c = state;
   objForm.Zip_Code__c = zip;
   objForm.Buyer_Phone__c = buyerPhone;
   objForm.Buyer_Email__c = buyerEmail;
   if (String.isNotBlank(dateOfSale))
    objForm.Date_Of_Sale__c = Date.parse(dateOfSale);
   objForm.Dealer_Name__c = dealerName;
   objForm.Vehicle_Model__c = vehicleModel;
   objForm.Serial_Number__C = serialNumber;
   objForm.Model_Year__c = modelYear;
   objForm.Make__c = make;
   if (String.isNotBlank(amount))
    objForm.Amount__c = Double.valueOf(amount);

   insert objForm;

   List < Attachment > lstAttachment = new List < Attachment > ();

   if (String.isNotBlank(billAttachmentName) && String.isNotBlank(billAttachmentBody)) {
    Attachment objBillAttachement = new Attachment();
    objBillAttachement.body = EncodingUtil.base64Decode(billAttachmentBody);
    objBillAttachement.name = billAttachmentName;
    objBillAttachement.parentId = objForm.Id;
    lstAttachment.add(objBillAttachement);
   }
   if (String.isNotBlank(warrantyAttachmentName) && String.isNotBlank(warrantyAttachmentBody)) {
    Attachment objWarAttachement = new Attachment();
    objWarAttachement.body = EncodingUtil.base64Decode(warrantyAttachmentBody);
    objWarAttachement.name = warrantyAttachmentName;
    objWarAttachement.parentId = objForm.Id;
    lstAttachment.add(objWarAttachement);
   }

   if (!lstAttachment.isEmpty()) {
    insert lstAttachment;
   }
   sendmail(objForm.Id);//method for sending method calling added by tapas
   return objForm.Id;
  } catch (Exception e) {
   system.debug(e);
   return 'Failure';

  }
 }

 /**Remote method to submit the LSV Credit Certification Form **/

 @RemoteAction
 global static String submitLSVFormData(String firstName, String lastName, String phone, String email, String buyerFirstName,
  String buyerLastName, String address, String city, String state,
  String zip, String buyerPhone, String buyerEmail, String dateOfSale,
  String dealerName, String vehicleModel, String serialNumber, String modelYear,
  String amount, String billAttachmentName, String billAttachmentBody, String warrantyAttachmentName, String warrantyAttachmentBody) {
  try {
   Marketing_Promotion_Form__c objForm = new Marketing_Promotion_Form__c();
   String recordTypeId = Schema.SObjectType.Marketing_Promotion_Form__c.RecordTypeInfosByName.get('LSV').RecordTypeId;
   if (recordTypeId != null) {
    objForm.RecordTypeId = recordTypeId;
   }
   objForm.First_Name__c = firstName;
   objForm.Last_Name__c = lastName;
   objForm.Phone__c = phone;
   objForm.Email__c = email;
   objForm.Buyer_First_Name__c = buyerFirstName;
   objForm.Buyer_Last_Name__c = buyerLastName;
   objForm.Address__c = address;
   objForm.City__c = city;
   objForm.State__c = state;
   objForm.Zip_Code__c = zip;
   objForm.Buyer_Phone__c = buyerPhone;
   objForm.Buyer_Email__c = buyerEmail;
   if (String.isNotBlank(dateOfSale))
    objForm.Date_Of_Sale__c = Date.parse(dateOfSale);
   objForm.Dealer_Name__c = dealerName;
   objForm.Vehicle_Model__c = vehicleModel;
   objForm.Serial_Number__C = serialNumber;
   objForm.Model_Year__c = modelYear;
   if (String.isNotBlank(amount))
    objForm.Amount__c = Double.valueOf(amount);

   insert objForm;

   List < Attachment > lstAttachment = new List < Attachment > ();

   if (String.isNotBlank(billAttachmentName) && String.isNotBlank(billAttachmentBody)) {
    Attachment objBillAttachement = new Attachment();
    objBillAttachement.body = EncodingUtil.base64Decode(billAttachmentBody);
    objBillAttachement.name = billAttachmentName;
    objBillAttachement.parentId = objForm.Id;
    lstAttachment.add(objBillAttachement);
   }
   if (String.isNotBlank(warrantyAttachmentName) && String.isNotBlank(warrantyAttachmentBody)) {
    Attachment objWarAttachement = new Attachment();
    objWarAttachement.body = EncodingUtil.base64Decode(warrantyAttachmentBody);
    objWarAttachement.name = warrantyAttachmentName;
    objWarAttachement.parentId = objForm.Id;
    lstAttachment.add(objWarAttachement);
   }

   if (!lstAttachment.isEmpty()) {
    insert lstAttachment;
   }

   sendmail(objForm.Id);//method for sending method calling added by tapas
   return objForm.Id;
  } catch (Exception e) {
   system.debug(e);
   return 'Failure';

  }
 }

 /**Remote method to submit the XRT Credit Certification Form **/

 @RemoteAction
 global static String submitXRTFormData(String firstName, String lastName, String phone, String email, String buyerFirstName,
  String buyerLastName, String address, String city, String state,
  String zip, String buyerPhone, String buyerEmail, String dateOfSale,
  String dealerName, String vehicleModel, String serialNumber, String modelYear,
  String amount, String billAttachmentName, String billAttachmentBody, String warrantyAttachmentName, String warrantyAttachmentBody) {
  try {
   Marketing_Promotion_Form__c objForm = new Marketing_Promotion_Form__c();
   String recordTypeId = Schema.SObjectType.Marketing_Promotion_Form__c.RecordTypeInfosByName.get('XRT').RecordTypeId;
   if (recordTypeId != null) {
    objForm.RecordTypeId = recordTypeId;
   }
   objForm.First_Name__c = firstName;
   objForm.Last_Name__c = lastName;
   objForm.Phone__c = phone;
   objForm.Email__c = email;
   objForm.Buyer_First_Name__c = buyerFirstName;
   objForm.Buyer_Last_Name__c = buyerLastName;
   objForm.Address__c = address;
   objForm.City__c = city;
   objForm.State__c = state;
   objForm.Zip_Code__c = zip;
   objForm.Buyer_Phone__c = buyerPhone;
   objForm.Buyer_Email__c = buyerEmail;
   if (String.isNotBlank(dateOfSale))
    objForm.Date_Of_Sale__c = Date.parse(dateOfSale);
   objForm.Dealer_Name__c = dealerName;
   objForm.Vehicle_Model__c = vehicleModel;
   objForm.Serial_Number__C = serialNumber;
   objForm.Model_Year__c = modelYear;
   if (String.isNotBlank(amount))
    objForm.Amount__c = Double.valueOf(amount);

   insert objForm;

   List < Attachment > lstAttachment = new List < Attachment > ();

   if (String.isNotBlank(billAttachmentName) && String.isNotBlank(billAttachmentBody)) {
    Attachment objBillAttachement = new Attachment();
    objBillAttachement.body = EncodingUtil.base64Decode(billAttachmentBody);
    objBillAttachement.name = billAttachmentName;
    objBillAttachement.parentId = objForm.Id;
    lstAttachment.add(objBillAttachement);
   }
   if (String.isNotBlank(warrantyAttachmentName) && String.isNotBlank(warrantyAttachmentBody)) {
    Attachment objWarAttachement = new Attachment();
    objWarAttachement.body = EncodingUtil.base64Decode(warrantyAttachmentBody);
    objWarAttachement.name = warrantyAttachmentName;
    objWarAttachement.parentId = objForm.Id;
    lstAttachment.add(objWarAttachement);
   }

   if (!lstAttachment.isEmpty()) {
    insert lstAttachment;
   }

   sendmail(objForm.Id);//method for sending method calling added by tapas
   return objForm.Id;
  } catch (Exception e) {
   system.debug(e);
   return 'Failure';

  }
 }
 
 
 
 /**Remote method to submit the Commercial Certification Form **/

 @RemoteAction
 global static String submitCommercialFormData(String firstName, String lastName, String phone, String email, String buyerFirstName,
  String buyerLastName, String address, String city, String state,
  String zip, String buyerPhone, String buyerEmail, String dateOfSale,
  String dealerName, String vehicleModel, String serialNumber, String modelYear,
  String amount, String billAttachmentName, String billAttachmentBody, String warrantyAttachmentName, String warrantyAttachmentBody) {
  try {
   Marketing_Promotion_Form__c objForm = new Marketing_Promotion_Form__c();
   String recordTypeId = Schema.SObjectType.Marketing_Promotion_Form__c.RecordTypeInfosByName.get('Commercial').RecordTypeId;
   if (recordTypeId != null) {
    objForm.RecordTypeId = recordTypeId;
   }
   objForm.First_Name__c = firstName;
   objForm.Last_Name__c = lastName;
   objForm.Phone__c = phone;
   objForm.Email__c = email;
   objForm.Buyer_First_Name__c = buyerFirstName;
   objForm.Buyer_Last_Name__c = buyerLastName;
   objForm.Address__c = address;
   objForm.City__c = city;
   objForm.State__c = state;
   objForm.Zip_Code__c = zip;
   objForm.Buyer_Phone__c = buyerPhone;
   objForm.Buyer_Email__c = buyerEmail;
   if (String.isNotBlank(dateOfSale))
    objForm.Date_Of_Sale__c = Date.parse(dateOfSale);
   objForm.Dealer_Name__c = dealerName;
   objForm.Vehicle_Model__c = vehicleModel;
   objForm.Serial_Number__C = serialNumber;
   objForm.Model_Year__c = modelYear;
   if (String.isNotBlank(amount))
    objForm.Amount__c = Double.valueOf(amount);

   insert objForm;

   List < Attachment > lstAttachment = new List < Attachment > ();

   if (String.isNotBlank(billAttachmentName) && String.isNotBlank(billAttachmentBody)) {
    Attachment objBillAttachement = new Attachment();
    objBillAttachement.body = EncodingUtil.base64Decode(billAttachmentBody);
    objBillAttachement.name = billAttachmentName;
    objBillAttachement.parentId = objForm.Id;
    lstAttachment.add(objBillAttachement);
   }
   if (String.isNotBlank(warrantyAttachmentName) && String.isNotBlank(warrantyAttachmentBody)) {
    Attachment objWarAttachement = new Attachment();
    objWarAttachement.body = EncodingUtil.base64Decode(warrantyAttachmentBody);
    objWarAttachement.name = warrantyAttachmentName;
    objWarAttachement.parentId = objForm.Id;
    lstAttachment.add(objWarAttachement);
   }

   if (!lstAttachment.isEmpty()) {
    insert lstAttachment;
   }

   sendmail(objForm.Id);//method for sending method calling added by tapas
   return objForm.Id;
  } catch (Exception e) {
   system.debug(e);
   return 'Failure';

  }
 }
 
 
 
 
 
 //************************************************ added by tapas to send mail immediately after creating Marketting Promotion Forms****************************************************
 @RemoteAction
 public static string sendmail(String promotionId) { //added by tapas to send mail after uploading the claim req
  system.debug(promotionId);
  try {
       Marketing_Promotion_Form__c mProm = new Marketing_Promotion_Form__c();
       mProm = [select id, First_Name__c, Last_Name__c, Phone__c,
        Email__c, Buyer_First_Name__c, Buyer_Last_Name__c,
        Address__c, State__c, City__c, Zip_Code__c, Buyer_Phone__c,
        Buyer_Email__c, Date_Of_Sale__c, Dealer_Name__c, Model_Year__c,
        Vehicle_Model__c, Amount__c from Marketing_Promotion_Form__c where id = : promotionId];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {mProm.Email__c};
       mail.setToAddresses(toAddresses);
       mail.setSenderDisplayName('Pavilion Admin');
       mail.setSubject('Greetings from Pavilion');
       mail.setBccSender(false);
       mail.setUseSignature(false);
       String messageBody = '<html><body>Hi ' + mProm.First_Name__c + ',<br/>Thank you for submiiting claim request. We will get back to you shortly.<br/><br/>' +
        '<Strong><h1><u>Here are the details you submitted:</u></h1></Strong><br/>' + '<Strong>Your Details:</Strong><br/>' + 'Name:' + mProm.First_Name__c + ' ' + mProm.Last_Name__c + '<br/>' + 'Phone: ' + mProm.Phone__c + '<br/>' + 'Email: ' + mProm.Email__c + '<br/><Strong>Buyer Details:</Strong><br/>' + 'Buyer Name: ' + mProm.Buyer_First_Name__c + ' ' + mProm.Buyer_Last_Name__c + '<br/>' + 'Buyer Address: ' + mProm.Address__c + '<br/>State:' + mProm.State__c + '<br/>City: ' + mProm.City__c + '<br/>Zip Code: ' + mProm.Zip_Code__c +
        'Buyer Phone: ' + mProm.Buyer_Phone__c + '<br/>Buyer Email: ' + mProm.Buyer_Email__c + '<br/>Date Of Sale: ' + Date.valueOf(mProm.Date_Of_Sale__c) +
        '<br/>Dealer Name: ' + mProm.Dealer_Name__c + '<br/>Model Year: ' + mProm.Model_Year__c + '<br/>Vehicle Model: ' + mProm.Vehicle_Model__c;
    
    
    
       messageBody = messageBody + '<br/>Amount:' + mprom.Amount__c;
    
       String Footer = '<br/><Strong>N.B.: For uploaded Bill details please find the below attachments.</Strong></body></html>';
       mail.setHtmlBody(messageBody + Footer);
       mail.saveAsActivity = false;
       //Set email file attachments
    
       List < Messaging.Emailfileattachment > fileAttachments = new List < Messaging.Emailfileattachment > ();
       List < Attachment > att = [select Name, Body, BodyLength from Attachment where ParentId = : promotionId];
       system.debug(att);
       for (Attachment a: att) {
        // Add to attachment file list
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        system.debug(a.Name);
        efa.setFileName(a.Name);
        efa.setBody(a.Body);
        fileAttachments.add(efa);
       }
       mail.setFileAttachments(fileAttachments);
       //Send email
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
        mail
       });
       system.debug('email sent');
       return 'Success';
  } catch (Exception e) {
       system.debug(e);
       return 'Failure';
  }


 }

}