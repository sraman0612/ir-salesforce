/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Trigger Handler for the "RSHVAC Quote" SObject.
              This class should only be called by the trigger "rshvacQuoteTrigger"
              And should be without sharing since triggers execute in System mode
------------------------------------------------------------*/

public without sharing class RshvacQuoteTriggerHandler{

  // Instance of the helper class OpportunityUtils
  OpportunityUtils oppUtils;
  // Map that will contain all the Opportunities related to the Quotes being processed
  public Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();
  // Map that will contain all the Opportunities that need to be updated
  // The key of this map will be the Id of the Opportunity
  public Map<Id, Opportunity> opportunitiesToUpdate = new Map<Id, Opportunity>();
  // Map that will contain the amount to be reduced from opportunity
  // Key will be opportunity Id and value will be Sum of Extended Gross Sales of to be deleted Quotes. 
  public Map<Id, Decimal> deletedQuoteAmountByOppties = new Map<Id, Decimal>();
  // Map that will contain the roll up of extended gross sales of rshvac quotes.
  // key will be opportunity Id and value will be the sum of Extended Gross Sales
  public Map<Id, Decimal> extendedGrossSalesByOppties = new Map<Id, Decimal>();
  // Opprtunity Stages
  public static String FULLFIL = 'Fulfill';
  public static String CLOSED_WON = 'Closed Won';
  public static String PENDING_REVIEW = 'Pending Sales Review';
  public static String PEND_PRICING_REV = 'Pending Pricing Review';
  public static String LOSS = 'Loss';
  public static String CLOSED_LOST = 'Closed Lost';
  public static String ACCEPTED = 'Accepted';
  public static String COMMITTED = 'Committed';
  public static String NEGOTIATE_WIN = 'Negotiate/Win';
  
  // Opportunity RecordType Ids
  
  public Id newBusinessRecTypeId;
  public Id newBusinessIwdRecTypeId;
  public Id DSOStrategicRecTypeId;
  public Id IWDStrategicRecTypeId;

  public RshvacQuoteTriggerHandler(){
    // Get an instance of Oppo
    oppUtils = new OpportunityUtils();
  }

  public void bulkBefore(){
    if (Trigger.isDelete){
      Set<Id> relatedOpportunities = new Set<Id>();
      // Iterate over the Quotes to collect the Opportunity Ids
      for (SObject sObj : Trigger.Old){
        RSHVAC_Quote__c q = (RSHVAC_Quote__c) sObj;
        relatedOpportunities.add(q.Opportunity__c);
      }
      // Get the Id of the Opportunity Existing BUsiness Record Type
      newBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_NEW_BUSINESS).Id;
      newBusinessIwdRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_IWD_NEW_BUSINESS).Id;
      DSOStrategicRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_STRATEGIC_DSO).Id;
      IWDStrategicRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_STRATEGIC_IWD).Id;
      opportunitiesMap = new Map<Id, Opportunity>([SELECT Id, StageName, RecordTypeId, Gross_Sales__c FROM Opportunity WHERE Id IN :relatedOpportunities AND (RecordTypeId = :newBusinessRecTypeId OR RecordTypeId = :newBusinessIwdRecTypeId OR RecordTypeId = :DSOStrategicRecTypeId OR RecordTypeId = :IWDStrategicRecTypeId)]);
      // Preparing Map deletedQuoteAmountByOppties 
      for(RSHVAC_Quote__c quote: [Select Id, Opportunity__c, Extended_Gross_Sales__c From RSHVAC_Quote__c Where Id IN :Trigger.old AND Extended_Gross_Sales__c <> Null]){       
        if(!deletedQuoteAmountByOppties.containsKey(quote.Opportunity__c)){
          deletedQuoteAmountByOppties.put(quote.Opportunity__c, quote.Extended_Gross_Sales__c);
        }  
        else{
          decimal amount = deletedQuoteAmountByOppties.get(quote.Opportunity__c) + quote.Extended_Gross_Sales__c;
          deletedQuoteAmountByOppties.put(quote.Opportunity__c, amount);      
        }
      }
    }
  }    
  
  
  public void bulkAfter(){
    // On insert and updates, we'll need to update the Opportunities of the current year
    // Plus the appropiate Revenue Schedule
    if (Trigger.isUpdate || Trigger.isInsert){
      Set<Id> relatedOpportunities = new Set<Id>();
      // Iterate over the Quotes to collect the Opportunity Ids
      for (SObject sObj : Trigger.new){
        RSHVAC_Quote__c q = (RSHVAC_Quote__c) sObj;
        relatedOpportunities.add(q.Opportunity__c);
      }
      // Get the Id of the Opportunity Existing BUsiness Record Type
      newBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_NEW_BUSINESS).Id;
      newBusinessIwdRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_IWD_NEW_BUSINESS).Id;
      DSOStrategicRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_STRATEGIC_DSO).Id;
      IWDStrategicRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_STRATEGIC_IWD).Id;
      opportunitiesMap = new Map<Id, Opportunity>([SELECT Id, StageName, RecordTypeId, Gross_Sales__c FROM Opportunity WHERE Id IN :relatedOpportunities AND (RecordTypeId = :newBusinessRecTypeId OR RecordTypeId = :newBusinessIwdRecTypeId OR RecordTypeId = :DSOStrategicRecTypeId OR RecordTypeId = :IWDStrategicRecTypeId)]);
      
      // Preparing Map extendedGrossSalesByOppties 
      for(RSHVAC_Quote__c quote: [Select Id, Opportunity__c, Extended_Gross_Sales__c From RSHVAC_Quote__c Where Opportunity__c IN :opportunitiesMap.keySet() AND Extended_Gross_Sales__c <> Null]){       
        if(!extendedGrossSalesByOppties.containsKey(quote.Opportunity__c)){
          if(Trigger.NewMap.containsKey(quote.Id)){
            decimal newAmount= ((RSHVAC_Quote__c)Trigger.NewMap.get(quote.Id)).Extended_Gross_Sales__c;
            extendedGrossSalesByOppties.put(quote.Opportunity__c, newAmount);
          }
          else
            extendedGrossSalesByOppties.put(quote.Opportunity__c, quote.Extended_Gross_Sales__c);
        }  
        else{
          if(Trigger.NewMap.containsKey(quote.Id)){
            decimal newAmount= ((RSHVAC_Quote__c)Trigger.NewMap.get(quote.Id)).Extended_Gross_Sales__c;
            decimal amount = extendedGrossSalesByOppties.get(quote.Opportunity__c) + newAmount;
            extendedGrossSalesByOppties.put(quote.Opportunity__c, amount);     
          }
          else{
            decimal amount = extendedGrossSalesByOppties.get(quote.Opportunity__c) + quote.Extended_Gross_Sales__c;
            extendedGrossSalesByOppties.put(quote.Opportunity__c, amount);
          }
        }
      }
    }
  }

  public void afterInsert(RSHVAC_Quote__c newQuote){
    // Get the opportunity related to this particular Quote
    // There should always be one, but just in case
    Id oppId = newQuote.Opportunity__c;
    if (opportunitiesMap.get(oppId) != null){
      // Update the Opportunity Amount value
      // If it was already on the ToUpdate Map get the Opportunity from it
      // That way we can make sure that we're updating the proper value
      Opportunity opp = opportunitiesToUpdate.get(oppId) != null ? opportunitiesToUpdate.get(oppId) : opportunitiesMap.get(oppId);
      // First check if the Opportunity is in Stage Fullfil or Closed Won
      if ((PENDING_REVIEW.equalsIgnoreCase(newQuote.Status__c) || (ACCEPTED.equalsIgnoreCase(newQuote.Status__c) && !LOSS.equalsIgnoreCase(newQuote.Win_Loss__c))) && !FULLFIL.equalsIgnoreCase(opp.StageName) && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !CLOSED_LOST.equalsIgnoreCase(opp.StageName) && !COMMITTED.equalsIgnoreCase(opp.StageName))
      {
        //Ashwini: Strategic Opportunity Change  
        if(opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId)
        {
            // If it isn't set is Pending Sales Review
            opp.StageName = PENDING_REVIEW;
        }
        else {
            opp.StageName = NEGOTIATE_WIN;
        }
      }
      // Now, if Extended Gross Sales is greater than zero and the stage is not closed won
      if (opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId && newQuote.Extended_Gross_Sales__c > 0 && !CLOSED_WON.equalsIgnoreCase(opp.StageName)){
        // Set the stage as Fullfil
        opp.StageName = FULLFIL;
      }
      if (newQuote.Extended_Gross_Sales__c != null){
        opp.Gross_Sales__c = extendedGrossSalesByOppties.get(newQuote.Opportunity__c);
      }
      if(opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId) 
      {
          //Dhilip Added -- Start-- If the Quote is in Pending Pricing Review and Oppty is not in Closed Won, Fulfill, Committed -- then update the Oppty Stage to PEND_PRICING_REV
          if(PEND_PRICING_REV.equalsIgnoreCase(newQuote.Status__c) && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !FULLFIL.equalsIgnoreCase(opp.StageName))
          {
             opp.StageName = PEND_PRICING_REV;
          }
          
          if(opp.RecordTypeId != newBusinessRecTypeId && ACCEPTED.equalsIgnoreCase(newQuote.Status__c) && !LOSS.equalsIgnoreCase(newQuote.Win_Loss__c)  && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !FULLFIL.equalsIgnoreCase(opp.StageName) && !CLOSED_LOST.equalsIgnoreCase(opp.StageName))
          {
             opp.StageName = COMMITTED;
          }
          //Dhilip Added -- End -- If the Quote is in Pending Pricing Review and Oppty is not in Closed Won, Fulfill, Committed -- then update the Oppty Stage to PEND_PRICING_REV
      }
      //Ashwini: Start_Strategic type change
      if(opp.RecordTypeId == DSOStrategicRecTypeId || opp.RecordTypeId == IWDStrategicRecTypeId) 
      {
          opp.RHVAC_Quote_Status__c = newQuote.Status__c;
      }
      //Ashwini: End_Strategic type change
      //Dhilip Added -- Start -- If all Quotes are closed, then close the Opportunity
      if(LOSS.equalsIgnoreCase(newQuote.Win_Loss__c))
      {
        Integer LossQuoteCount = [SELECT count() FROM RSHVAC_Quote__c WHERE Win_Loss__c != 'Loss' AND Opportunity_Id__c = :oppId];
        if(LossQuoteCount == 0 && !CLOSED_LOST.equalsIgnoreCase(opp.StageName))
        {
            opp.StageName = CLOSED_LOST;
            opp.Closed_Lost_Reason__c = newQuote.Reason_For_Loss__c;
        }
      }
      //Dhilip Added -- End -- If all Quotes are closed, then close the Opportunity
      
      opportunitiesToUpdate.put(oppId, opp);
    }
  }

  public void afterUpdate(RSHVAC_Quote__c oldQuote, RSHVAC_Quote__c newQuote){
    Id oppId = newQuote.Opportunity__c;
    // Only do this if the Gross Sales has Changed or if the Opportunity Stage isn't already Fullfil
    if (oldQuote.Extended_Gross_Sales__c != newQuote.Extended_Gross_Sales__c ||
        (opportunitiesMap.get(oppId) != null && !FULLFIL.equalsIgnoreCase(opportunitiesMap.get(oppId).StageName))){
        // Get the opportunity related to this particular Quote
        // There should always be one, but just in case
        if (opportunitiesMap.get(oppId) != null){
            // If it was already on the ToUpdate Map get the Opportunity from it
            // That way we can make sure that we're updating the proper value
            Opportunity opp = opportunitiesToUpdate.get(oppId) != null ? opportunitiesToUpdate.get(oppId) : opportunitiesMap.get(oppId);
            
            // First check if the Opportunity is in Stage Fullfil or Closed Won
            if ((PENDING_REVIEW.equalsIgnoreCase(newQuote.Status__c) || (ACCEPTED.equalsIgnoreCase(newQuote.Status__c) && !LOSS.equalsIgnoreCase(newQuote.Win_Loss__c))) && !FULLFIL.equalsIgnoreCase(opp.StageName) && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !CLOSED_LOST.equalsIgnoreCase(opp.StageName) && !COMMITTED.equalsIgnoreCase(opp.StageName))
            {
                //Ashwini: Strategic Opportunity Change  
                if(opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId)
                {
                    // If it isn't set is Pending Sales Review
                    opp.StageName = PENDING_REVIEW;
                }
                else {
                    opp.StageName = NEGOTIATE_WIN;
                }
            }
            
          // If Extended Gross Sales is greater than zero and the stage is not closed won
          if (opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId && newQuote.Extended_Gross_Sales__c > 0 && !CLOSED_WON.equalsIgnoreCase(opp.StageName)){
            // Set the stage as Fullfil
            opp.StageName = FULLFIL;
          }
          
          if(opp.RecordTypeId != DSOStrategicRecTypeId && opp.RecordTypeId != IWDStrategicRecTypeId) 
          {
              //Dhilip Added -- Start-- If the Quote is in Pending Pricing Review and Oppty is not in Closed Won, Fulfill, Committed -- then update the Oppty Stage to PEND_PRICING_REV
              if (PEND_PRICING_REV.equalsIgnoreCase(newQuote.Status__c) && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !FULLFIL.equalsIgnoreCase(opp.StageName) && !COMMITTED.equalsIgnoreCase(opp.StageName))
              {
                  opp.StageName = PEND_PRICING_REV;
              }
              if(opp.RecordTypeId != newBusinessRecTypeId && ACCEPTED.equalsIgnoreCase(newQuote.Status__c) && !LOSS.equalsIgnoreCase(newQuote.Win_Loss__c)  && !CLOSED_WON.equalsIgnoreCase(opp.StageName) && !FULLFIL.equalsIgnoreCase(opp.StageName) )
              {
                opp.StageName = COMMITTED;
              }
          }
          //Dhilip Added -- End -- If the Quote is in Pending Pricing Review and Oppty is not in Closed Won, Fulfill, Committed -- then update the Oppty Stage to PEND_PRICING_REV
          //Ashwini: Start_Strategic type change
          if(opp.RecordTypeId == DSOStrategicRecTypeId || opp.RecordTypeId == IWDStrategicRecTypeId) 
          {
              opp.RHVAC_Quote_Status__c = newQuote.Status__c;
          }
          //Ashwini: End_Strategic type change
          //Dhilip Added -- Start -- If all Quotes are closed, then close the Opportunity
          if(LOSS.equalsIgnoreCase(newQuote.Win_Loss__c))
          {
            Integer LossQuoteCount = [SELECT count() FROM RSHVAC_Quote__c WHERE Win_Loss__c != 'Loss' AND Opportunity_Id__c = :oppId];
            if(LossQuoteCount == 0 && !CLOSED_LOST.equalsIgnoreCase(opp.StageName))
            {
                opp.StageName = CLOSED_LOST;
                opp.Closed_Lost_Reason__c = newQuote.Reason_For_Loss__c;
            }
          }
          //Dhilip Added -- End -- If all Quotes are closed, then close the Opportunity
      
            opp.Gross_Sales__c = extendedGrossSalesByOppties.get(newQuote.Opportunity__c);
            opportunitiesToUpdate.put(oppId, opp);
        }
    }
  }

  public void beforeDelete(RSHVAC_Quote__c newQuote){
    Id oppId = newQuote.Opportunity__c; 
    if(opportunitiesMap.get(oppId) != null && deletedQuoteAmountByOppties.containsKey(oppId)){
      // If it was already on the ToUpdate Map get the Opportunity from it
      // That way we can make sure that we're updating the proper value
      Opportunity opp = opportunitiesToUpdate.get(oppId) != null ? opportunitiesToUpdate.get(oppId) : opportunitiesMap.get(oppId);          
      opp.Gross_Sales__c -= deletedQuoteAmountByOppties.get(oppId);
      opportunitiesToUpdate.put(opp.Id, opp);
    }      
  }
    
  public void andFinally(){
    if (!opportunitiesToUpdate.isEmpty()){
      update opportunitiesToUpdate.values();
    }
  }
}