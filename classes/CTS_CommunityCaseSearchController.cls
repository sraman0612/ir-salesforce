/**
@author Providity
@date 20FEB19
@description Controller class CTS_CommunityCaseSearch.cmp
*/

public with sharing class CTS_CommunityCaseSearchController {
    
    @AuraEnabled
    public static List<Object> getAllCases(String strObjectName, String strFieldSetName, String listType,
                                           String searchType, String qfKeyword, String qfAccountId, 
                                           String qfSubmitter, String qfStatusFilter, String qfRTFilter) { 
    	
		//Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
        
        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Fields to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
        
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
        
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ) {
            String fieldName = String.valueOf(eachFieldSetMember.getFieldPath());
            lstFieldsToQuery.add(fieldName);
            //Create a wrapper instance and store label, fieldname and type.                    
            if(String.valueOf(eachFieldSetMember.getType()) == 'REFERENCE') {
                if(fieldName != 'CreatedById' && fieldName.endsWith('Id')) {
                    lstFieldsToQuery.add(fieldName.replace('Id', '.Name'));                    
                    DataTableColumns datacolumnName = new DataTableColumns( 
                        (String.valueOf(eachFieldSetMember.getLabel())).remove(' ID') , 
                        (String.valueOf(eachFieldSetMember.getFieldPath())).replace('Id', '.Name') , 
                        'text', false
                    );
                    lstDataColumns.add(datacolumnName);  
                }                 
            } else {
                DataTableColumns datacolumns = new DataTableColumns( 
                    String.valueOf(eachFieldSetMember.getLabel()) , 
                    String.valueOf(eachFieldSetMember.getFieldPath()), 
                    String.valueOf(eachFieldSetMember.getType()).toLowerCase(), false
                );
                lstDataColumns.add(datacolumns);
            } 
        }
		User cUser = [SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId()];
		CTS_Community_Search_Settings__c setting = CTS_Community_Search_Settings__c.getInstance();
		Integer limitRecords = setting.Limit_Records__c.intValue();
		System.debug('cUser**' + cUser);
        //Form a SOQL to fetch the data - Set the wrapper instance and return as response
        String rts = setting.CTS_TD_Case_Record_Types__c;
		System.debug('rts**' + rts);
        List<String> rtList = String.isBlank(rts) ? null : rts.split(',');
		System.debug('rtList**' + rtList);
        if(! lstDataColumns.isEmpty()) {            
            response.lstDataTableColumns = lstDataColumns;
            String query = '';            
            try {
                if(searchType == 'allOnInit') {
                    query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM ' + strObjectName + ' WHERE RecordType.Name IN :rtList ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                    System.debug('SOQL query**' + query);
                    response.lstDataTableData = Database.query(query);
                } else {                                        
                    if(listType == 'search') {
                        query += 'SELECT ';
                        query += String.join(lstFieldsToQuery, ','); 
                        query += ' FROM Case ';
                        query += ' WHERE RecordType.Name IN (\'' + String.join(rtList, '\',\'') + '\')';
                        if(!String.isBlank(qfAccountId)) query += ' AND AccountId =\'' + qfAccountId + '\'';
                        if(!String.isBlank(qfSubmitter)) {
                            if(qfSubmitter == 'mine') {
                                if(cUser.ContactId == null) query += ' AND (CreatedById =\'' + cUser.Id + '\') ';
                                else {
                                    query += ' AND (CreatedById =\'' + cUser.Id + '\'';
                                    if(cUser.ContactId != null) query += ' OR ContactId =\'' + cUser.ContactId + '\'';
                                    query += ' )';
                                }
                            } else {
                                if(cUser.ContactId == null) query += ' AND (CreatedById !=\'' + cUser.Id + '\') ';
                                else {
                                    query += ' AND (CreatedById !=\'' + cUser.Id + '\' AND ContactId !=\'' + cUser.ContactId + '\') ';
                                    /*Commented for case 00991795*/
                                    //if(cUser.ContactId != null) query += ' AND ContactId !=\'' + cUser.ContactId + '\'';
                                    //query += ' )';
                                }
                            }
                        }
                        system.debug('query------>'+query);
                        if(!String.isBlank(qfStatusFilter)) query += ' AND Status =\'' + qfStatusFilter + '\'';
                        if(!String.isBlank(qfRTFilter)) query += ' AND RecordTypeId =\'' + qfRTFilter + '\'';
                        
                        if(!String.isEmpty(qfKeyword)) {
                            query += ' AND (CaseNumber LIKE \'%' + qfKeyword + '%\' OR Asset.Name LIKE \'%' + qfKeyword + '%\'' + ' OR Subject LIKE \'%' + qfKeyword + '%\'' + ' OR Account.Name LIKE \'%' + qfKeyword + '%\'' + ' OR Contact.Name LIKE \'%' + qfKeyword + '%\'' + ')';
                        }
                        
                       // query += ' ORDER BY CaseNumber LIMIT ' + limitRecords;
                        query += ' ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                        System.debug('query**' + query);
                        response.lstDataTableData = Database.query(query);                       
                    } else if(listType == 'open') {
                        List<String> openStats = new List<String>();
                        for(CaseStatus s: [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = false]) {
                            openStats.add(s.MasterLabel);
                        }
                        query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM ' + strObjectName + ' WHERE RecordType.Name IN :rtList';
                        query += ' AND Status IN (\'' + String.join(openStats, '\',\'') + '\') ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                        response.lstDataTableData = Database.query(query);
                    } else if(listType == 'closed') {
                        List<String> closedStats = new List<String>();
                        for(CaseStatus s: [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true]) {
                            closedStats.add(s.MasterLabel);
                        }
                        query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM ' + strObjectName + ' WHERE RecordType.Name IN :rtList';
                        query += ' AND Status IN (\'' + String.join(closedStats, '\',\'') + '\') ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                        response.lstDataTableData = Database.query(query);
                    } else if(listType == 'recent') {
                        Integer recentDays = setting.Recent_Days_Count__c.intValue();
                        query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM ' + strObjectName + ' WHERE RecordType.Name IN :rtList';
                        query += ' AND CreatedDate = LAST_N_DAYS:' + recentDays + ' ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                        response.lstDataTableData = Database.query(query);
                    } else if(listType == 'collaborator') {
                        if(cUser.ContactId == null ) response.lstDataTableData = new List<sObject>();
                        else {
                        	query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' FROM ' + strObjectName + ' WHERE RecordType.Name IN :rtList';
                            query += ' AND (CTS_Case_Collaborator_1__c =: cUser.ContactId OR CTS_Case_Collaborator_2__c =: cUser.ContactId OR CTS_Case_Collaborator_3__c =: cUser.ContactId) ORDER BY CaseNumber DESC LIMIT ' + limitRecords;
                            response.lstDataTableData = Database.query(query);   
                        }                        
                    }
                    System.debug('query**' + query);
                }                                
            } catch(Exception e) {
                response = null;
                throw new AuraException();
            }
        }
        try {            
            return new List<Object>{
                response
            };
        } catch(Exception e) {
            response = null;
            throw new AuraException();
        }
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {        
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String fieldName {get;set;}
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public Boolean sortable {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type, Boolean sortable){
            this.label = label;
            this.fieldName = fieldName;
            if(type.equalsIgnoreCase('dateTime')) {
                type = 'date';
            }
            this.type = type;
            this.sortable = sortable;
        }
    }
    
    //Wrapper class to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {        
        @AuraEnabled public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled public List<sObject> lstDataTableData {get;set;}
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
    
    /**
     * Given an API object and field name, returns list of the picklist values for use in select input.
     */
    @AuraEnabled
    public static List<Object> getPicklistOptions(String objectName, String fieldName, String recordTypes) {
        return PicklistSelectController.getPicklistOptions(objectName, fieldName, recordTypes, null);
    }        
    
    /**
     * Get Other search options
     */
    @AuraEnabled
    public static List<Object> getSearchParamsOnLoad() {
        CTS_Community_Search_Settings__c setting = CTS_Community_Search_Settings__c.getInstance();
        String rts = setting.CTS_TD_Case_Record_Types__c;
        Integer limitRecords = setting.Limit_Records__c.intValue();
        List<String> rtList = String.isBlank(rts) ? null : rts.split(',');
        
        try {            
            return new List<Object>{
                [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true], 
                [SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId()],
                [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND Name IN :rtList],                
                Database.query('SELECT Id, Name FROM Account LIMIT ' + limitRecords),
                setting,
                Database.countQuery('SELECT count() FROM Case LIMIT ' + (limitRecords+1) )
            };
        } catch(Exception e) {
            throw new AuraException();
        }
    }
}