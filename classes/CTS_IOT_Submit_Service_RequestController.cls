public with sharing class CTS_IOT_Submit_Service_RequestController {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public User user;
        @AuraEnabled public Account site = new Account();
        @AuraEnabled public CTS_IOT_Data_Service.AssetDetail assetDetail = new CTS_IOT_Data_Service.AssetDetail();
        @AuraEnabled public SelectOption[] assetOptions = new SelectOption[]{};
    }

    public class SelectOption {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
    }

    @AuraEnabled
    public static InitResponse getInit(String assetID, String siteID){

        InitResponse response = new InitResponse();

        try{
            
            response.user = [Select Id, FirstName, LastName, Name, AccountId, Account.Name From User Where Id = :UserInfo.getUserId()];
            response.site = response.user.Account;

            if (String.isBlank(assetID)){

                CTS_IOT_Data_Service.AssetDetail[] assets = CTS_IOT_Data_Service.getCommunityUserAssets(String.isNotBlank(siteID) ? siteID : null);

                // If no assets are available for the site selected, show all assets available for the customer
                if (assets.size() == 0){
                    assets = CTS_IOT_Data_Service.getCommunityUserAssets(null);
                }

                for (CTS_IOT_Data_Service.AssetDetail assetDetail : assets){
                    
                    SelectOption option = new SelectOption();
                    option.label = assetDetail.asset.Name;
                    option.value = assetDetail.asset.Id;

                    response.assetOptions.add(option);
                }
            }
            else{
                response.assetDetail = CTS_IOT_Data_Service.getCommunityUserAsset(assetID);
            }
        }
        catch(Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }

    @AuraEnabled
    public static LightningResponseBase submitServiceRequest(String serviceRequestJSON){

        LightningResponseBase response = new LightningResponseBase();

        try{

            CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();        
            User user = [Select Id, FirstName, LastName, Name, Email, Phone, ContactId, AccountId, Account.Name, Account.OwnerId, Account.Owner.isActive From User Where Id = :UserInfo.getUserId()];

            CTS_IOT_Service_Request__c serviceRequest = (CTS_IOT_Service_Request__c)JSON.deserialize(serviceRequestJSON, CTS_IOT_Service_Request__c.class);                                    
            serviceRequest.Status__c = 'Open';
            serviceRequest.Sub_Status__c = 'Unassigned';
            serviceRequest.Sub_Area__c = 'Remote Monitoring';
            serviceRequest.Account__c = user.AccountId;
            serviceRequest.Opened_Date__c = DateTime.now();
            serviceRequest.Contact__c = user.ContactId;

            if (user.AccountId != null && user.Account.Owner.isActive){
                serviceRequest.OwnerId = user.Account.OwnerId;
            }

            if (serviceRequest.Asset__c != null){

                Asset asset = [Select Id, Name, Siebel_ID__c From Asset Where Id = :serviceRequest.Asset__c];
                
                serviceRequest.Name = user.Account.Name + ' ' + asset.Name + ': ' + serviceRequest.Type__c;
                serviceRequest.SR_Number__c = asset.Siebel_ID__c;
                serviceRequest.Siebel_ID__c = asset.Siebel_ID__c;               
            }        

            insert serviceRequest;
        }
        catch(Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}