/**
	@author Ben Lorenz
	@date 5AUG2019
	@description Controller class for CC_LeadDisposition page
*/

public without sharing class CC_LeadDispositionController {
	
    public String userId {get;set;}
    public Boolean internalOwner {get;set;}
    
    public String iconName { get; set; }
    public String leadId { get; set; }
    
    public String msgs { get; set; }
    public String err { get; set; }
    public String lName { get; set; }
    public String serNum { get; set; }
    public String satAmount { get; set; }
    public String satDate { get; set; }
    public String satDesc { get; set; }
    public String comments { get; set; }
    public String fValues { get; set; }
    public List<SelectOption> altUsers { get; set; }
    
    public static Map<String,List<FieldValue>> fieldValues = new Map<String,List<FieldValue>>();
    static {
        fieldValues.put('message', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'In Process'), 
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now()),
                    new FieldValue('CC_purchase_nextstep__c', 'What is the next step in your purchase?', 'Follow up on Lead/Call or email')
                });
        fieldValues.put('quote', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Contacted'), 
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now()),
                    new FieldValue('CC_purchase_nextstep__c', 'What is the next step in your purchase?', 'Follow up on Quote')
        		});
        
        fieldValues.put('information', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Unqualified - Closed'), 
                    new FieldValue('CC_Close_Reasons__c', 'Close Reason', 'Information Only'),
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now())
        		});
        
        fieldValues.put('not_interested', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Unqualified - Closed'),
            		new FieldValue('CC_Close_Reasons__c', 'Close Reason', 'Unqualified'),
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now())
        		});
        
        fieldValues.put('config', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Unqualified - Closed'), 
                    new FieldValue('CC_Close_Reasons__c', 'Close Reason', 'Configurator Sales Tool'),
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now())
        		});
        
        fieldValues.put('duplicate', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Unqualified - Closed'), 
                    new FieldValue('CC_Close_Reasons__c', 'Close Reason', 'Duplicate'),
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now()) //Time to contact??
        		});
        
        fieldValues.put('sold', new List<FieldValue>{
            		new FieldValue('Status', 'Status', 'Unqualified - Closed'), 
                    new FieldValue('CC_Close_Reasons__c', 'Close Reason', 'Sold From Stock'),
                    new FieldValue('CC_First_Contact__c', 'First Contact', System.now()), //Time to contact??
                        
                    new FieldValue('CC_Serial_Number_of_Car_Sold__c', 'Serial Number of Car Sold', null),
                    new FieldValue('CC_SAT_Amount__c', 'Sales Action Taken $ Amount', null),
                    new FieldValue('CC_SAT_Date__c', 'Sales Action Taken Date', null),
                    new FieldValue('CC_SAT_Description__c', 'Sales Action Taken Description', null)
        		});                
    }
    
	public CC_LeadDispositionController() {
    	Set<String> convertedStatuses = new Set<String>();
        for(LeadStatus ls: [SELECT ApiName FROM LeadStatus WHERE IsConverted = true]) {
            convertedStatuses.add(ls.ApiName);
        }
        
        internalOwner = false;
        Map<String,String> params = ApexPages.currentPage().getParameters();
        altUsers = new List<SelectOption>();
        try {
            if(params.containsKey('lid')) leadId = (Id) params.get('lId');
            if(params.containsKey('icon')) iconName = params.get('icon');
            Lead l = [SELECT Id, Name, Status, OwnerId, CC_First_Contact__c, CC_Serial_Number_of_Car_Sold__c, CC_SAT_Amount__c, CC_SAT_Date__c, CC_SAT_Description__c, CC_Comments__c FROM Lead WHERE Id =: leadId];            
            User owner;                        
            
            if( ((String)l.OwnerId).startsWith('005')) {
                owner = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id =: l.OwnerId];
                if(owner.ContactId == null) {
                    //err = 'This lead is not owned by a Partner and cannot be updated from this process.';
                    internalOwner = true;
                } else {
                    internalOwner = false;
                }
            } else {
                err = 'This lead is not owned by a user and cannot be updated from this process.';
            }
            
            lName = l.Name;
            serNum = l.CC_Serial_Number_of_Car_Sold__c == null ? '' : l.CC_Serial_Number_of_Car_Sold__c;
            satAmount = String.valueOf(l.CC_SAT_Amount__c);
            satDate = l.CC_SAT_Date__c == null ? '' : String.valueOf(l.CC_SAT_Date__c).split(' ')[0]; //+ 'T' + String.valueOf(l.CC_SAT_Date__c).split(' ')[1];
            satDesc = l.CC_SAT_Description__c == null ? '' : l.CC_SAT_Description__c;
            comments = l.CC_Comments__c == null ? '' : l.CC_Comments__c;
            if(owner.ContactId != null) {
                Set<Id> frozenUsers = new Set<Id>();
                for(UserLogin ul : [select userid from UserLogin where isfrozen=true]){
                    frozenUsers.add(userid);
                }
                for(User u: [SELECT Id, Name 
                               FROM User 
                              WHERE Contact.AccountId =: owner.Contact.AccountId 
                                    AND Id !=: owner.Id 
                                    AND isActive=TRUE 
                                    AND Id != :frozenUsers
                           ORDER BY Name]) {
                    altUsers.add(new SelectOption(u.Id, u.Name));
                }
            }
            
            fValues = JSON.serialize(CC_LeadDispositionController.fieldValues);
            
            if(convertedStatuses.contains(l.Status) || l.Status=='Unqualified - Closed') {
				err = 'This Lead is closed. No further action required.';                
            }
        } catch(Exception e) {
            err = e.getMessage();
        }
    }
    
    @RemoteAction
    public static void updateLead(String lId, String icon, String newOwner, String serNum, String satdate, String satAmount, String satDesc, String comments) {
        System.debug('satDate**' + satDate);
        Lead l = [SELECT Id, Name, OwnerId, CC_First_Contact__c FROM Lead WHERE Id =: lId]; 
        try {
            if(String.isNotBlank(newOwner)) {
                l.OwnerId = newOwner;
            } else {
                if(fieldValues.containsKey(icon)) {
                    for(FieldValue fv: fieldValues.get(icon)) {
                        if(fv.field == 'CC_First_Contact__c') {
                            if(l.CC_First_Contact__c == null) l.put(fv.field, fv.value);
                        } else if(fv.field == 'CC_Serial_Number_of_Car_Sold__c') {
                            if(String.isNotBlank(serNum)) l.put(fv.field, serNum);
                        } else if(fv.field == 'CC_SAT_Date__c') {                                                                                    
                            if(String.isNotBlank(satDate)) {                                
                                DateTime satDateTime = DateTime.parse(satDate.replace(':00 ', ' '));
                            	Long satDateTimeLong = satDateTime.getTime() - UserInfo.getTimeZone().getOffset(satDateTime);
                                l.put(fv.field, DateTime.newInstance(satDateTimeLong));
                            }
                        } else if(fv.field == 'CC_SAT_Amount__c') {
                            if(String.isNotBlank(satAmount)) l.put(fv.field, Decimal.valueOf(satAmount));
                        } else if(fv.field == 'CC_SAT_Description__c') {
                            if(String.isNotBlank(satDesc)) l.put(fv.field, satDesc);
                        } else {
                            l.put(fv.field, fv.value);
                        }
                    }
                }                
            }
            l.put('CC_Comments__c', comments);
            System.debug('l***' + l);
            update l;            
        } catch(Exception e) {
            System.debug('e**' + e.getMessage());
            String err = e.getMessage();
            //if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) err = err.
            throw new DmlException(e.getMessage());            
        }
    }
    
    public class FieldValue {
        public String field;
        public String label;
        public Object value;
        public FieldValue(String f, String l, Object v) {
            this.field = f;
            this.label = l;
            this.value = v;
        }
    }
    
}