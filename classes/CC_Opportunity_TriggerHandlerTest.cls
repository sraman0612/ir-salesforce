/************************************************************************
* Description      : test class for CC_Opportunity_TriggerHandler
* Created Date     : 02/06/2017
* ***********************************************************************/
@isTest
public class CC_Opportunity_TriggerHandlerTest {
    
    static testmethod void testbeforeInsertUpdate(){
        
        //creating test Data
        TestDataUtility testData = new TestDataUtility();
        //creating partner community user
       // user u = testData.createPartnerUser();
        
        //system.runAs(u){
            
            //creating state country custom setting data
         TestDataUtility.createStateCountries();
            
            //creating sales rep here
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1101');
            salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
            insert salesRepacc;
            
            account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
            acc.CC_Sales_Rep__c = salesRepacc.Id;
            acc.CC_Price_List__c='PCOPI';
            acc.BillingCountry = 'USA';
            acc.BillingCity = 'Augusta';
            acc.BillingState = 'GA';
            acc.BillingStreet = '2044 Forward Augusta Dr';
            acc.BillingPostalCode = '566';
            acc.CC_Shipping_Billing_Address_Same__c = true;
            insert acc;
            
            //creating product
            Product2 prod =  TestUtilityClass.createProduct2();
            prod.CC_Market_Type__c = 'Golf';
            prod.CC_Vendor__c      = 'Test';
            prod.Name              = 'Visage';
            prod.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Club_Car'].Id;
            insert prod;
            
            
            CC_Asset_Group__c assetGroup = TestDataUtility.createAssetGroup(acc.id,prod.Id);
            insert assetGroup;
            
            opportunity opp = TestDataFactory.createOpportunity('testopty', 'CC Dealer Contract', acc.id, 'No','Budgetary','Qualify','Pipeline');
            insert opp;
            
            
            
            CC_Trade_In__c tradein  = testData.createTradeIn(assetGroup.id, opp.Id);
            insert tradein;
            Test.startTest();
            opp.CC_Actual_Ship_Date__c = date.today();
            checkRecursive.run         = true;
            update opp;
            
            delete opp;
            Test.stopTest();
        //}
        
    }
    
    
    static testmethod void testcreateOptyProducts(){
         //creating test Data
        TestDataUtility testData = new TestDataUtility();
        
        //creating Integration user
        user u = testData.createIntegrationUser();
        
        system.runAs(u){
            
               //creating state country custom setting data
              TestDataUtility.createStateCountries();
            
             //creating sales rep here
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1001');
            salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
            insert salesRepacc;
            
            account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
            acc.CC_Sales_Rep__c = salesRepacc.Id;
            acc.CC_Price_List__c='PCOPI';
            acc.BillingCountry = 'USA';
            acc.BillingCity = 'Augusta';
            acc.BillingState = 'GA';
            acc.BillingStreet = '2044 Forward Augusta Dr';
            acc.BillingPostalCode = '566';
            acc.CC_Shipping_Billing_Address_Same__c = true;
            insert acc;
            
            // Create a custom price book
        	Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        	insert customPB;
            
             //creating product
            Product2 prod =  TestUtilityClass.createProduct2();
            prod.CC_Market_Type__c = 'Golf';
            prod.CC_Vendor__c      = 'Test';
            prod.Name              = 'Visage';
            prod.ERP_Item_Number__c = 'Oppty PE';
            prod.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Club_Car'].Id;
            insert prod;
            
            // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        //insert standardPrice;
        
          // 2. Insert a price book entry with a custom price.
           PricebookEntry customPrice = new PricebookEntry(
                    Pricebook2Id = customPB.Id, Product2Id = prod.Id,CurrencyIsoCode ='USD',
                    UnitPrice = 12000, IsActive = true);
            insert customPrice;
            
            
            TestDataUtility.createPurchaseTypeToProductMap();
            
            PavilionSettings__c pricebookidsetting = new PavilionSettings__c(Name = 'UtilitySalesPricebookID', Value__c=customPB.id);
            insert pricebookidsetting;
            
            Test.startTest();
                List<Opportunity> opplist = new List<Opportunity>();
                Opportunity opp = TestDataFactory.createOpportunity('testopty', 'CC Dealer Contract', acc.id, 'No','Budgetary','Qualify','Pipeline');
                opp.CurrencyIsoCode   = 'USD';
                opp.CC_Siebel_Purchase_Type__c = 'Ele';
                opp.CC_Siebel_Units__c  = 10;
                insert opp;
            
            Test.stopTest();
            
            
        }
        
        
        
    }
    
}