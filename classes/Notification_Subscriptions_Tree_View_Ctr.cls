public with sharing class Notification_Subscriptions_Tree_View_Ctr {

    @TestVisible private static Boolean forceError = false;
    
    public virtual class Node{
        @AuraEnabled public String label;
        @AuraEnabled public String manageIconTitle;
        @AuraEnabled public String detailsLinkTitle;
        @AuraEnabled public Id value;
        @AuraEnabled public String variant = ''; 
        @AuraEnabled public Boolean subscribed = false;       
        @AuraEnabled public Boolean expanded = false;
        @AuraEnabled public Boolean subscriptionEnabled = false;
        @AuraEnabled public Boolean fullOptOut = false;
        @AuraEnabled public Boolean partialOptOut = false;
    }

    public class SiteNode extends Node{  

        @AuraEnabled public Node[] assets = new Node[]{};        

        @AuraEnabled 
        public Boolean getHasAssets(){
            return assets.size() > 0;
        }
    }

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public Boolean hasGlobalSubscriptions = false;
        @AuraEnabled public SiteNode[] sites = new SiteNode[]{};
    }

    @AuraEnabled
    public static InitResponse getInit(Boolean showHelixDisabled){

        InitResponse response = new InitResponse();

        try{

            if (Test.isRunningTest() && forceError){
                throw new System.NullPointerException();
            }

            Map<Id, Notification_Type__c> notificationTypesMap = new Map<Id, Notification_Type__c>();
        
            for (Notification_Type__c notificationType : NotificationService.getNotificationTypes()){

                if (notificationType.Active__c && !notificationType.Mandatory__c){
                    notificationTypesMap.put(notificationType.Id, notificationType);
                }
            }

            Set<Id> globalSubscriptionNotificationTypeIds = new Set<Id>();
            Map<Id, Set<Id>> recordSubscriptionNotificationTypeIds = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> recordOptOutSubscriptionNotificationTypeIds = new Map<Id, Set<Id>>();

            Notification_Subscription__c[] allSubscriptions =  NotificationService.getCurrentUserNotificationSubscriptions(null, true);

            for (Notification_Subscription__c subscription : allSubscriptions){

                if (subscription.Scope__c == 'All Sites'){
                    response.hasGlobalSubscriptions = true;
                    globalSubscriptionNotificationTypeIds.add(subscription.Notification_Type__c);
                }
                else if (String.isNotBlank(subscription.Record_ID__c)){
                
                    if (subscription.Opt_Out__c){
                        
                        if (recordOptOutSubscriptionNotificationTypeIds.containsKey(subscription.Record_ID__c)){
                            recordOptOutSubscriptionNotificationTypeIds.get(subscription.Record_ID__c).add(subscription.Notification_Type__c);
                        }
                        else{
                            recordOptOutSubscriptionNotificationTypeIds.put(subscription.Record_ID__c, new Set<Id>{subscription.Notification_Type__c});
                        }                        
                    }
                    else{
                       
                        if (recordSubscriptionNotificationTypeIds.containsKey(subscription.Record_ID__c)){
                            recordSubscriptionNotificationTypeIds.get(subscription.Record_ID__c).add(subscription.Notification_Type__c);
                        }
                        else{
                            recordSubscriptionNotificationTypeIds.put(subscription.Record_ID__c, new Set<Id>{subscription.Notification_Type__c});
                        }                       
                    }                
                }
            }

            Map<Id, CTS_IOT_Data_Service.AssetDetail[]> siteAssetMap = new Map<Id, CTS_IOT_Data_Service.AssetDetail[]>();
            
            for (CTS_IOT_Data_Service.AssetDetail assetDetail : CTS_IOT_Data_Service.getCommunityUserAssets(null)){

                // Skip non Helix connected assets if the user selected to not show them
                if (!showHelixDisabled && !assetDetail.asset.IRIT_RMS_Flag__c){
                    continue;
                }

                if (siteAssetMap.containsKey(assetDetail.asset.AccountId)){
                    siteAssetMap.get(assetDetail.asset.AccountId).add(assetDetail);
                }
                else{
                    siteAssetMap.put(assetDetail.asset.AccountId, new CTS_IOT_Data_Service.AssetDetail[]{assetDetail});
                }
            }

            Account[] sites = CTS_IOT_Data_Service.getCommunityUserSites(null);            
            Map<Id, Boolean> remoteMonitoringEnabledSiteMap = CTS_IOT_Data_Service.getRemoteMonitoringSiteMap(sites);

            for (Account site : sites){

                system.debug('checking site: ' + site.Account_Name_Displayed__c);

                Boolean rmsEnabled = remoteMonitoringEnabledSiteMap.get(site.Id);

                // Skip non Helix connected sites if the user selected to not show them
                if (!showHelixDisabled && !rmsEnabled){
                    continue;
                }

                Set<Id> siteSubscriptionNotificationTypeIds = recordSubscriptionNotificationTypeIds.get(site.Id) != null ? recordSubscriptionNotificationTypeIds.get(site.Id) : new Set<Id>();
                Integer siteSubscriptionCount = siteSubscriptionNotificationTypeIds.size();
                Set<Id> siteOptOutSubscriptionNotificationTypeIds = recordOptOutSubscriptionNotificationTypeIds.get(site.Id) != null ? recordOptOutSubscriptionNotificationTypeIds.get(site.Id) : new Set<Id>();
                Integer siteOptOutSubscriptionCount = siteOptOutSubscriptionNotificationTypeIds.size();

                system.debug('rmsEnabled: ' + rmsEnabled);
                system.debug('siteSubscriptionCount: ' + siteSubscriptionCount);
                system.debug('siteOptOutSubscriptionCount: ' + siteOptOutSubscriptionCount);

                SiteNode siteNode = new SiteNode();
                siteNode.label = site.Account_Name_Displayed__c;
                siteNode.manageIconTitle = 'Manage Subscriptions For ' + siteNode.label;
                siteNode.detailsLinkTitle = 'View Site Details For ' + siteNode.label;
                siteNode.value = site.Id;
                siteNode.subscriptionEnabled = rmsEnabled;                                                

                // Determine if all global subscription notification types of been opted out at the site level
                Integer globalNotificationTypeSiteOptOutCount = 0;

                for (Id notificationTypeId : notificationTypesMap.keyset()){

                    if (globalSubscriptionNotificationTypeIds.contains(notificationTypeId) && siteOptOutSubscriptionNotificationTypeIds.contains(notificationTypeId)){
                        globalNotificationTypeSiteOptOutCount++;
                    }
                }

                system.debug('globalSubscriptionNotificationTypeIds: ' + globalSubscriptionNotificationTypeIds.size());
                system.debug('globalNotificationTypeSiteOptOutCount: ' + globalNotificationTypeSiteOptOutCount);                

                siteNode.fullOptOut = false;

                if (siteSubscriptionCount == 0){

                    siteNode.fullOptOut =
                        globalSubscriptionNotificationTypeIds.size() == 0 ? 
                        (siteOptOutSubscriptionCount > 0 ? true : false) :
                        globalNotificationTypeSiteOptOutCount >= globalSubscriptionNotificationTypeIds.size();
                }

                siteNode.partialOptOut = 
                    siteNode.fullOptOut ?
                    false :
                    siteOptOutSubscriptionCount > 0;

                siteNode.subscribed = !siteNode.fullOptOut && (siteSubscriptionCount > 0 || response.hasGlobalSubscriptions);
                siteNode.variant = siteNode.subscribed ? 'success' : '';

                CTS_IOT_Data_Service.AssetDetail[] assetDetails = siteAssetMap.get(site.Id);

                if (assetDetails != null){

                    for (CTS_IOT_Data_Service.AssetDetail assetDetail : assetDetails){

                        system.debug('checking asset: ' + assetDetail.asset.Asset_Name_Displayed__c);

                        Set<Id> assetSubscriptionNotificationTypeIds = recordSubscriptionNotificationTypeIds.get(assetDetail.asset.Id) != null ? recordSubscriptionNotificationTypeIds.get(assetDetail.asset.Id) : new Set<Id>();
                        Integer assetSubscriptionCount = assetSubscriptionNotificationTypeIds.size();  
                        Set<Id> assetOptOutSubscriptionNotificationTypeIds = recordOptOutSubscriptionNotificationTypeIds.get(assetDetail.asset.Id) != null ? recordOptOutSubscriptionNotificationTypeIds.get(assetDetail.asset.Id) : new Set<Id>();
                        Integer assetOptOutSubscriptionCount = assetOptOutSubscriptionNotificationTypeIds.size();

                        system.debug('assetSubscriptionCount: ' + assetSubscriptionCount);
                        system.debug('assetOptOutSubscriptionCount: ' + assetOptOutSubscriptionCount);                        

                        Node assetNode = new Node();
                        assetNode.label = assetDetail.asset.Asset_Name_Displayed__c;
                        assetNode.manageIconTitle = 'Manage Subscriptions For ' + assetNode.label;
                        assetNode.detailsLinkTitle = 'View Asset Details For ' + assetNode.label;                        
                        assetNode.value = assetDetail.asset.Id;
                        assetNode.subscriptionEnabled = assetDetail.asset.IRIT_RMS_Flag__c;                                               

                        // Determine if all site subscription notification types of been opted out at the asset level
                        Integer siteAndGlobalNotificationTypeAvailableCount = 0;
                        Integer siteAndGlobalNotificationTypeAssetOptOutCount = 0;                        

                        for (Id notificationTypeId : notificationTypesMap.keyset()){

                            // Site subscription
                            if (siteSubscriptionNotificationTypeIds.contains(notificationTypeId)){

                                siteAndGlobalNotificationTypeAvailableCount++;

                                // Opt out of site subscription
                                if (assetOptOutSubscriptionNotificationTypeIds.contains(notificationTypeId)){
                                    siteAndGlobalNotificationTypeAssetOptOutCount++;
                                }
                            }
                            // Global subscription that was not opted out at the site level 
                            else if (globalSubscriptionNotificationTypeIds.contains(notificationTypeId) && !siteOptOutSubscriptionNotificationTypeIds.contains(notificationTypeId)){

                                siteAndGlobalNotificationTypeAvailableCount++;

                                // Opt out of global subscription with no site subscription
                                if (assetOptOutSubscriptionNotificationTypeIds.contains(notificationTypeId)){
                                    siteAndGlobalNotificationTypeAssetOptOutCount++;
                                }
                            }
                        }
        
                        system.debug('siteAndGlobalNotificationTypeAvailableCount: ' + siteAndGlobalNotificationTypeAvailableCount);
                        system.debug('siteAndGlobalNotificationTypeAssetOptOutCount: ' + siteAndGlobalNotificationTypeAssetOptOutCount);                              

                        assetNode.fullOptOut = false;

                        if (assetSubscriptionCount == 0){

                            assetNode.fullOptOut =
                                globalSubscriptionNotificationTypeIds.size() == 0 && siteSubscriptionNotificationTypeIds.size() == 0 ?
                                (assetOptOutSubscriptionCount > 0 ? true : false) :
                                siteAndGlobalNotificationTypeAssetOptOutCount == siteAndGlobalNotificationTypeAvailableCount;
                        }
                        
                        assetNode.partialOptOut = 
                            !assetNode.fullOptOut ? 
                            assetOptOutSubscriptionCount > 0 : 
                            false;

                        assetNode.subscribed = !assetNode.fullOptOut && (assetSubscriptionCount > 0 || siteNode.subscribed || response.hasGlobalSubscriptions);
                        assetNode.variant = assetNode.subscribed ? 'success' : ''; 

                        siteNode.assets.add(assetNode);
                    }
                }

                response.sites.add(siteNode);
            }            
        }
        catch(Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}