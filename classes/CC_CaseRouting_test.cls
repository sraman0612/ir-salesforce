@isTest
public class CC_CaseRouting_test {


    static testmethod void testAssignmentByTeamRole() {
        insert new StatesCountries__c(name     = 'USA',
                                      type__c  = 'C',
                                      code__c  = 'US',
                                      country__c = '',
                                      cc_PostalCode_Exclusive__c = true,
                                      cc_PostalCode_Proximity__c = true, 
                                      cc_PostalCode_Size__c = 5);
        insert new StatesCountries__c(name     = 'Georgia',type__c  = 'S',code__c  = 'GA',country__c = 'USA');
        
        CC_Sales_Rep__c sr = new CC_Sales_Rep__c(Name = 'Test',   CC_Sales_Person_Number__c = '11111');
        insert sr;
        
        Id rtId = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName LIKE 'Club_Car%' LIMIT 1].Id;        
        Account a = new Account (Name = 'Case Routing Test',
                                RecordTypeId = rtId,
                                CC_Sales_Rep__c = sr.Id,
                                CC_Shipping_Billing_Address_Same__c = true,
                                ShippingStreet = '2044 Forward Augusta Dr',
                                ShippingCity   = 'Augusta',
                                ShippingState  = 'GA',
                                ShippingPostalCode = '00000',
                                ShippingCountry = 'USA',
                                BillingStreet = '2044 Forward Augusta Dr',
                                BillingCity   = 'Augusta',
                                BillingState  = 'GA',
                                BillingPostalCode = '00000',
                                BillingCountry = 'USA',
                                BillingLatitude = 50,
                                BillingLongitude = 50);
        insert a;
        
        CC_Queue_Name_To_Role_Name__mdt q2r = [SELECT Queue_Id__c, Role_Name__c FROM CC_Queue_Name_To_Role_Name__mdt LIMIT 1];

        CC_My_Team__c mt = New CC_My_Team__c(Account__c = a.Id,User__c = UserInfo.getUserId(),My_Team_Role__c = q2r.Role_Name__c);
        insert mt;
                                                                                          
        rtId = [SELECT Id FROM RecordType WHERE sObjectType='Case' and DeveloperName LIKE 'Club_Car%' LIMIT 1].Id;                                                                                 
        Case c= new Case(AccountId = a.id,OwnerId = q2r.Queue_Id__c,RecordTypeId = rtId);
        
        System.assert(c.OwnerId != UserInfo.getUserId());
        CC_CaseRouting.AssignByTeamRole(new Case[] {c}); 
        System.assert(c.OwnerId == UserInfo.getUserId()); 
    }
    
    static testmethod void assignCommunityCreatedCases() {
        insert new StatesCountries__c(name     = 'USA',
                                      type__c  = 'C',
                                      code__c  = 'US',
                                      country__c = '',
                                      cc_PostalCode_Exclusive__c = true,
                                      cc_PostalCode_Proximity__c = true, 
                                      cc_PostalCode_Size__c = 5);
        insert new StatesCountries__c(name     = 'Georgia',type__c  = 'S',code__c  = 'GA',country__c = 'USA');
        
        CC_Sales_Rep__c sr = new CC_Sales_Rep__c(Name = 'Test',   CC_Sales_Person_Number__c = '11111');
        insert sr;
        
        Id rtId = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName LIKE 'Club_Car%' LIMIT 1].Id;        
        Account a = new Account (Name = 'Case Routing Test',
                                RecordTypeId = rtId,
                                CC_Sales_Rep__c = sr.Id,
                                CC_Shipping_Billing_Address_Same__c = true,
                                ShippingStreet = '2044 Forward Augusta Dr',
                                ShippingCity   = 'Augusta',
                                ShippingState  = 'GA',
                                ShippingPostalCode = '00000',
                                ShippingCountry = 'USA',
                                BillingStreet = '2044 Forward Augusta Dr',
                                BillingCity   = 'Augusta',
                                BillingState  = 'GA',
                                BillingPostalCode = '00000',
                                BillingCountry = 'USA',
                                BillingLatitude = 50,
                                BillingLongitude = 50);
        insert a;
        
        CC_Community_Case_Create_Map__mdt c5map = [SELECT MasterLabel,OwnerId__c, Reason__c, Sub_Reason__c, Root_Cause__c 
                                                     FROM CC_Community_Case_Create_Map__mdt
                                                    LIMIT 1];
        
                                                                                          
        rtId = [SELECT Id FROM RecordType WHERE sObjectType='Case' and DeveloperName LIKE 'Club_Car%' LIMIT 1].Id;                                                                                 
        Case c= new Case(AccountId = a.id,Origin='Community',CC_PavilionReason_for_Case__c=c5Map.MasterLabel,RecordTypeId = rtId);
        
        System.assert(c.OwnerId != c5map.OwnerId__c);
        CC_CaseRouting.assignCommunityCreatedCases(new Case[] {c}); 
        System.assert(c.OwnerId == c5map.OwnerId__c); 
    }

}