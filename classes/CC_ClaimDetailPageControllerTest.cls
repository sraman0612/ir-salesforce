@isTest
public class CC_ClaimDetailPageControllerTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    
    static testMethod void UnitTest_CoopClaimDetailPageController()
    { 
        // Creation Of Test Data
        Account newAccount = TestUtilityClass.createAccount();
        insert newAccount;
        Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
        insert sampleContact;
        //String userNavigationProfile = newUser.CC_Pavilion_Navigation_Profile__c;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
        insert newPavilionNavigationProfile;
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
        insert newUser;
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',newAccount.id);
        Insert sampleContract;
        CC_Co_Op_Account_Summary__c newCoopAccountSummary = TestUtilityClass.createCoopAccountSummary(sampleContract.Id);
        insert newCoopAccountSummary;
        CC_Coop_Claims__c sampleCoopClaims = TestUtilityClass.createCoopClaimWithAccountSummary(newCoopAccountSummary.Id);
        sampleCoopClaims.CC_Status__c='Pending';
        insert sampleCoopClaims;
        system.debug('coop clam is '+sampleCoopClaims);
        system.debug('current page is'+ApexPages.currentPage().getParameters().put('Id',sampleCoopClaims.Id));
        System.runAs(newUser){
        
        ApexPages.currentPage().getParameters().put('Id',sampleCoopClaims.Id);
        CC_PavilionTemplateController controller=new CC_PavilionTemplateController(); 
        CC_ClaimDetailPageController cliamObj =new CC_ClaimDetailPageController(controller);
        }     
    }
}