public without sharing class CC_STL_Send_To_MAPICS_Controller {
	
	private class CC_STL_Approval_Override_ControllerException extends Exception{}
		    
    @AuraEnabled
    public static LightningResponseBase sendLeaseToMAPICS(Id stlId){
    	
    	LightningResponseBase response = new LightningResponseBase();
    	System.Savepoint sp = Database.setSavepoint();
    	
    	try{
    		
            CC_Short_Term_Lease__c lease = [Select Id, Synced_to_MAPICS__c From CC_Short_Term_Lease__c Where Id = :stlId];           

            // Check the message queue history for the lease
            Integer messageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :lease.Id];            

            if (messageCount > 0){

                // Remove any update message queued that has not been processed yet
                delete [Select Id From CC_ERPMQ__c Where Key__c = :lease.Id and Processed__c = false and Type__c = 'STL_UPDATE'];                
            }

            if (lease.Synced_to_MAPICS__c){
                
                // Reset the sync flag so validation rules re-evaluate the lease
                lease.Synced_to_MAPICS__c = false;   
                update lease; 
            }

            // Set the sync flag
            lease.Synced_to_MAPICS__c = true;    	
            update lease; 				                         

            insert new CC_ERPMQ__c(
                Key__c = lease.Id,     
                Type__c = messageCount == 0 ? 'STL_NEW' : 'STL_UPDATE',              
                Processed__c = false
            );          
    	}
    	catch (Exception e){
    		
    		system.debug('exception: ' + e.getMessage());
    		response.success = false;
    		response.errorMessage = e.getMessage();
    		
    		// Roll back any DML
    		Database.rollback(sp);
    	}
    	
    	return response;
    }
}