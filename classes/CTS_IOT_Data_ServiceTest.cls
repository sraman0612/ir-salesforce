/**
 * Test class of CTS_IOT_Data_Service
 **/
@isTest
private class CTS_IOT_Data_ServiceTest {
    
    @testSetup
    private static void createData(){

UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(false);
        setting.Account_Matching_Record_Types__c = acc.recordTypeId;
        setting.Asset_Matching_Record_Types__c = assetRecTypeId;
        setting.Asset_Matching_Record_Types2__c = assetRecTypeId;
        insert setting;

        Asset asset = CTS_TestUtility.createAsset('Test Asset1', '12345', acc.Id, assetRecTypeId, false);
        asset.IRIT_RMS_Flag__c = true;
        insert asset;

        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', acc.Id, false));

        for(Contact con : contacts){
            con.CTS_IOT_Community_Status__c = 'Approved';
        }   
        insert contacts;

        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(contacts.get(0).Id, profileId, true);
    }
}
    @isTest
    private static void testGetRecordIds(){

        List<Account> accounts = [select id, Name from Account where Name = 'Test Account'];

        if(!accounts.isEmpty()){

            Test.startTest();

            Set<Id> accRTIds = CTS_IOT_Data_Service.getAccountRecordTypeIds();
            System.assert(accRTIds != null);
            Set<Id> assetRTIds = CTS_IOT_Data_Service.getAssetRecordTypeIds();
            System.assert(assetRTIds != null);

            Test.stopTest();
        }
    }

    @isTest
    private static void testGetCommunityUserSites(){

        List<Account> accounts = [select id, Name from Account where Name = 'Test Account'];

        if(!accounts.isEmpty()){

            Test.startTest();
            Account[] sites = CTS_IOT_Data_Service.getCommunityUserSites(accounts.get(0).Id);
            System.assert(sites != null);

            CTS_IOT_Data_Service.SiteHierarchyResponse siteHierarchyResponse = CTS_IOT_Data_Service.getCommunityUserSiteHierarchy(null, 10, 0);
            System.assert(siteHierarchyResponse != null);
            Integer siteCount = CTS_IOT_Data_Service.getCommunityUserSitesCount(null);
            System.assert(siteCount > 0);

            Id [] fixedSearchResults= new Id[1];
            fixedSearchResults[0] = accounts[0].Id;
            Test.setFixedSearchResults(fixedSearchResults);
            
            CTS_IOT_Data_Service.SiteHierarchyResponse siteHierarchyResponse2 = CTS_IOT_Data_Service.getCommunityUserSiteHierarchy('Test', 10, 0);
            System.assert(siteHierarchyResponse2 != null);
            Integer siteCount2 = CTS_IOT_Data_Service.getCommunityUserSitesCount('Test');
            System.assert(siteCount2 > 0);

            Test.stopTest();
        }
    }

    @isTest
    private static void testGetCommunityUserAssets(){

        Test.startTest();

        List<Asset> assets = [Select id, Name From Asset Where Name = 'Test Asset1'];

        CTS_IOT_Data_Service.AssetDetail assetDetails = CTS_IOT_Data_Service.getCommunityUserAsset(assets.get(0).Id);
        System.assert(assetDetails != null);

        Integer assetCount = CTS_IOT_Data_Service.getCommunityUserAssetsCount(null);
        System.assertEquals(1, assetCount);        

        List<Account> accounts = [Select Id, Name From Account Where Name = 'Test Account'];

        CTS_IOT_Data_Service.AssetDetail[] assetDetailList = CTS_IOT_Data_Service.getCommunityUserAssets(accounts.get(0).Id);
        CTS_IOT_Data_Service.AssetDetail[] assetDetailList2 = CTS_IOT_Data_Service.getCommunityUserAssets(null, accounts.get(0).Id, 10, 0);
        CTS_IOT_Data_Service.AssetDetail[] assetDetailList3 = CTS_IOT_Data_Service.getCommunityUserAssets('Test', accounts.get(0).Id, 10, 0);
        System.assert(assetDetailList != null);
        System.assert(assetDetailList2 != null);
        System.assert(assetDetailList3 != null);

        Map<Id, Boolean> monitoringSiteMap = CTS_IOT_Data_Service.getRemoteMonitoringSiteMap(accounts);
        System.assert(monitoringSiteMap != null);

        Test.stopTest();
    }

    @isTest
    private static void testGetContactSiteAccess(){
        List<Contact> contacts = [SELECT id, AccountId FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
           Test.startTest();
           CTS_IOT_Data_Service.UserSiteAccess[] siteAccess =  CTS_IOT_Data_Service.getContactSiteAccess(contacts.get(0).Id);
           System.assert(siteAccess != null);
           Test.stopTest();
        }
    }  

    @isTest
    private static void testGetRecords1(){

        createGetRecordsData();

        Test.startTest();

        sObject[] records = CTS_IOT_Data_Service.getRecords('Asset', 'Id, Name', 'Where IRIT_RMS_Flag__c = True', 'Order By Name ASC', null, 1, 0);
        system.assertEquals(1, records.size());
        system.assertEquals('Test Asset1', (String)records[0].get('Name'));

        Test.stopTest();
    }       

    @isTest
    private static void testGetRecords2(){

        createGetRecordsData();

        Test.startTest();

        sObject[] records = CTS_IOT_Data_Service.getRecords('Asset', 'Id, Name', 'Where IRIT_RMS_Flag__c = False', 'Order By Name ASC', null, 5, 0);
        system.assertEquals(1, records.size());
        system.assertEquals('Test Asset4', (String)records[0].get('Name'));

        Test.stopTest();
    }     
    
    @isTest
    private static void testGetRecords3(){

        createGetRecordsData();

        Test.startTest();

        sObject[] records = CTS_IOT_Data_Service.getRecords('Asset', 'Id, Name', 'Where IRIT_RMS_Flag__c = True', 'Order By Name ASC', null, 5, 1);
        system.assertEquals(2, records.size());
        system.assertEquals('Test Asset2', (String)records[0].get('Name'));
        system.assertEquals('Test Asset3', (String)records[1].get('Name'));

        Test.stopTest();
    }    

    @isTest
    private static void testGetRecords4(){

        createGetRecordsData();

        Test.startTest();

        Asset testAsset = [Select Id, Name From Asset Where Name = 'Test Asset2'];
        Test.setFixedSearchResults(new Id[]{testAsset.Id});

        sObject[] records = CTS_IOT_Data_Service.getRecords('Asset', 'Id, Name', 'Where IRIT_RMS_Flag__c = True', 'Order By Name ASC', 'Test Asset2', 5, 0);
        system.assertEquals(1, records.size());
        system.assertEquals('Test Asset2', (String)records[0].get('Name'));

        Test.stopTest();
    }     
    
    @isTest
    private static void testGetRecordCount1(){

        createGetRecordsData();

        Test.startTest();

        Integer recordCount = CTS_IOT_Data_Service.getRecordCount('Asset', 'Where IRIT_RMS_Flag__c = True', null);
        system.assertEquals(3, recordCount);

        Test.stopTest();
    } 
    
    @isTest
    private static void testGetRecordCount2(){

        createGetRecordsData();

        Test.startTest();

        Asset testAsset = [Select Id, Name From Asset Where Name = 'Test Asset2'];
        Test.setFixedSearchResults(new Id[]{testAsset.Id});        

        Integer recordCount = CTS_IOT_Data_Service.getRecordCount('Asset', 'Where IRIT_RMS_Flag__c = True', 'Test Asset2');
        system.assertEquals(1, recordCount);

        Test.stopTest();
    }     

    private static void createGetRecordsData(){

        Account acct = [Select Id, Name From Account Where Name = 'Test Account' LIMIT 1];

        Asset[] assets = [select Id, Name, RecordTypeId, AccountId, IRIT_RMS_Flag__c from Asset];
        system.assertEquals(1, assets.size());

        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '456142', acct.Id, assets[0].RecordTypeId, false);
        asset2.IRIT_RMS_Flag__c = true;  
        
        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '456142', acct.Id, assets[0].RecordTypeId, false);
        asset3.IRIT_RMS_Flag__c = true; 
        
        Asset asset4 = CTS_TestUtility.createAsset('Test Asset4', '855226', acct.Id, assets[0].RecordTypeId, false);
        asset4.IRIT_RMS_Flag__c = false;      
        
        insert new Asset[]{asset2,asset3, asset4};        
    }
}