/*------------------------------------------------------------
Author:       Mahesh Karadkar(Cognizant)
Description:  This is the test class for 'SendVerificationEmailAction'.
Case #03236104 
------------------------------------------------------------*/
@isTest
public class SendVerificationEmailActionTest {
    @testSetup
    static void setupData(){
        User TestUser1 = TestUtilityClass.createTestUserinternal();    
    }
    
    @isTest
    static void testSendEmail(){
        User user1 = [select id,email,FederationIdentifier from user where email='standarduser@testorg.com'];
        SendVerificationEmailAction.FlowInput input = new SendVerificationEmailAction.FlowInput();
        input.userId = user1.id;
        
        list<SendVerificationEmailAction.FlowInput> inputsList = new list<SendVerificationEmailAction.FlowInput>();
        inputsList.add(input);
        
        Test.startTest();
        SendVerificationEmailAction.sendEmail(inputsList);
        Test.stopTest();
        
        system.assertEquals(True,SendVerificationEmailAction.isEmailSent);
    }
    
}