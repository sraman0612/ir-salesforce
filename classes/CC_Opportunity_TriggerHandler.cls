public class CC_Opportunity_TriggerHandler {
    
    public static void beforeInsertUpdate(Map<String, List<Opportunity>> trgNewMap,Map<String, Map<Id,Opportunity>> trgOldMap){
        List<Opportunity> CC_OppLst = new List<Opportunity>();
        Map<Id,Opportunity> CC_OppOldMap = new Map<Id,Opportunity>();
        
        for(String recType : trgNewMap.keySet()){
            if(recType.startsWith('CC') || recType.startsWith('Club')){
             CC_OppLst.addAll(trgNewMap.get(recType));   
            }
        }
        
        for(String recType : trgOldMap.keySet()){
            if(recType.startsWith('CC') || recType.startsWith('Club')){
             CC_OppOldMap.putAll(trgOldMap.get(recType));   
            }
        }
       
        if(!CC_OppLst.isEmpty()){
            uniqueNameValidation(CC_OppLst,CC_OppOldMap);
            if(Trigger.isBefore && Trigger.isInsert){setOwnerFromSiebel(CC_OppLst);}
            CC_Opportunity_TriggerHandler.updateOrganization(CC_OppLst,CC_OppOldMap);
        }
    }
    
    /******************************************************************************************
     * Description     : Opportunity that prevents the user from making the .name field 
     *                   the same as any other opportunity for that same account.  
     *                    If the .name does match another opp for that account we should throw an error 
     *                    that says ‘Opportunity Name must be unique within this Account.  Please choose another Opportunity Name.’
     * Created Date    : 14/09/2017
     * ****************************************************************************************/
    
    public static void uniqueNameValidation(List<Opportunity> oppList,Map<Id,Opportunity>OppOldMap){
        
        Set<String>uniqueNamesSet       = new Set<String>();
        Set<ID>accIds                   = new Set<Id>();
        Set<ID>oppIds                   = new Set<Id>();
        Set<String>uniqueNamequeriedSet = new Set<String>();
        
        
        for(opportunity opp: oppList){
            if(Trigger.isInsert || (Trigger.isUpdate && OppOldMap.get(opp.Id) !=null && OppOldMap.get(opp.Id).Name != opp.Name)){
              uniqueNamesSet.add(opp.AccountId + opp.Name);
                accIds.add(opp.AccountId);
                oppIds.add(opp.Id);
            }
            
        }
        
        if(!accIds.isEmpty()){
            for(opportunity opp: [select id,name,accountId from opportunity
                                  where AccountId IN:accIds and (NOT Id IN:oppIds)]){
                    uniqueNamequeriedSet.add(opp.AccountId + opp.Name);                  
            }
        }
        for(Opportunity opp : oppList){
            if(!uniqueNamequeriedSet.isEmpty() && uniqueNamequeriedSet.contains(opp.AccountId + opp.Name)){
                opp.addError('Opportunity Name must be unique within this Account.  Please choose another Opportunity Name.');
            }
        }
        
        
    }

    public static void setOwnerFromSiebel(List<Opportunity> oLst){
      boolean isIntUsr = isClubCarIntegrationUser();
      Set<Id> opps2Update = new Set<Id>();
      Map<String, Id> oppOwnersMap = new Map<String,Id>();
      for (Opportunity o : oLst){
        if(null!=o.CC_Siebel_Opp_Owner__c && ''!=o.CC_Siebel_Opp_Owner__c && isIntUsr ){
          oppOwnersMap.put(o.CC_Siebel_Opp_Owner__c.toLowerCase(),null);
          opps2Update.add(o.Id);
        }
      }
      if(!oppOwnersMap.isEmpty()){
        for(User u : [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier IN :oppOwnersMap.keySet()]){
          oppOwnersMap.put(u.FederationIdentifier.toLowerCase(),u.Id);
        }
      }
      for (Opportunity o : oLst){
        if(isIntUsr && opps2Update.contains(o.Id)){
          o.OwnerId= oppOwnersMap.get(o.CC_Siebel_Opp_Owner__c.toLowerCase());
        }
      }
    }
    
    public static void afterInsert(Map<String, List<Opportunity>> trgNewMap){
        List<Opportunity> CC_OppLst = new List<Opportunity>();
        for(String recType : trgNewMap.keySet()){
            if(recType.startsWith('CC') || recType.startsWith('Club')){
               CC_OppLst.addAll(trgNewMap.get(recType));   
            }
        }
        
        if(!CC_OppLst.isEmpty()){
            createOpptyProducts(CC_OppLst);
        }
    }
    
    
    /************************************************************************
     * Description   : Auto Create Opportunity Line Items when Integration user creates a new Opportunity
     * Created Date  : 02/09/2017
     * **********************************************************************/
    public static void createOpptyProducts(List<Opportunity> OppList){
        system.debug('@@@@@ IN createOpptyProducts');
        Boolean isClubCarIntegrationUser = isClubCarIntegrationUser();
        Set<String>erpItemNumbersSet       = new Set<String>();
        Set<String> currencyIsoCodesSet    = new Set<String>();
        Map<String,List<PriceBookEntry>> keyVsPriceBookentries = new Map<String,List<PriceBookEntry>>();
        List<OpportunityLineItem> olistoInsert = new List<OpportunityLineItem>();
        
        
        for(opportunity opp : opplist){
            if( isClubCarIntegrationUser && opp.CC_Siebel_Purchase_Type__c != NULL && opp.CurrencyIsoCode != NULL){
                if(CC_Purchase_Type_to_Product_Map__c.getValues(opp.CC_Siebel_Purchase_Type__c ) != NULL){
                    erpItemNumbersSet.add(CC_Purchase_Type_to_Product_Map__c.getValues(opp.CC_Siebel_Purchase_Type__c ).Product_ERP_Item_Number__c);
                    currencyIsoCodesSet.add(opp.CurrencyIsoCode);
                }
            }
        }
        system.debug('@@@@erpItemNumbersSet'+erpItemNumbersSet);
        system.debug('@@@@currencyIsoCodesSet'+currencyIsoCodesSet);
        
        if(!erpItemNumbersSet.isEmpty()){
            for(PriceBookEntry pbe : [select id ,Name, Product2Id, Product2.ERP_Item_Number__c,
                                             CurrencyIsoCode, UnitPrice
                                             From PriceBookEntry
                                             where Product2.ERP_Item_Number__c IN:erpItemNumbersSet
                                      AND CurrencyIsoCode IN:currencyIsoCodesSet
                                      AND Pricebook2Id = :PavilionSettings__c.getInstance('UtilitySalesPricebookID').Value__c]  ){
                  string key = pbe.Product2.ERP_Item_Number__c + pbe.CurrencyIsoCode;
                  if(keyVsPriceBookentries.containsKey(key)){
                      keyVsPriceBookentries.get(key).add(pbe);                          
                  }
                  else{
                      keyVsPriceBookentries.put(key, new List<PriceBookEntry>{pbe});                          
                  }
             }
        }
        
        OpportunityLineItem oli;
        if(!keyVsPriceBookentries.isEmpty()){
            for(opportunity opp : oppList){
                oli = new OpportunityLineItem();
               
                if(CC_Purchase_Type_to_Product_Map__c.getValues(opp.CC_Siebel_Purchase_Type__c ) != NULL){
                    String key = CC_Purchase_Type_to_Product_Map__c.getValues(opp.CC_Siebel_Purchase_Type__c ).Product_ERP_Item_Number__c + opp.CurrencyIsoCode;
                    if(keyVsPriceBookentries.containsKey(key)){
                        oli.Quantity         = opp.CC_Siebel_Units__c;
                        PriceBookEntry pbe   = keyVsPriceBookentries.get(key)[0];
                        oli.PricebookEntryId = pbe.id;
                        oli.OpportunityId    = opp.id;
                        oli.UnitPrice        = pbe.UnitPrice;
                        //oli.Product2Id       = pbe.Product2Id;
                        olistoInsert.add(oli);
                        
                    }
                }
                
            }
        }
        
        if(!olistoInsert.isEmpty()){
            insert olistoInsert;
        }
        
        
    }
    
     private static Boolean isClubCarIntegrationUser() {
        return UserInfo.getName() == 'Clubcar Integration';
    }
    
    
    public static void updateOrganization(List<Opportunity> oppLst, Map<Id,Opportunity> OppOldMap){
        User usrRecord = [SELECT Id,Name,Account.IsPartner,Account.CC_Organization__c 
                          FROM User WHERE Id =: UserInfo.getUserId()];
                         
            for(Opportunity opp : oppLst){
                if(Trigger.isInsert || (Trigger.isUpdate && OppOldMap.get(opp.Id) !=null && OppOldMap.get(opp.Id).OwnerId != opp.OwnerId)){
                    if(usrRecord.Account.IsPartner){
                        opp.CC_Organization__c = usrRecord.Account.CC_Organization__c;
                    }else{
                      opp.CC_Organization__c = 'Club Car Organization';
                    }
                }
            }
    }
    
    
    
    /***********************************************************************
     * Description   : Account, Contact and Opportunity are integrated to Siebel. 
     *                 When one of these is deleted in Salesforce we need to send 
     *                 an outbound message to Siebel so they can delete on their side.
     * Created Date  : 14/06/2017
     * **********************************************************************/
    public static void afterDelete(Map<String, Map<Id,Opportunity>> oldMap){
        
        List<Opportunity> clubcaroptys      = new List<Opportunity>();
        list<OBM_Notifier__c> notifiersList = new list<OBM_Notifier__c>();
        
         for(String recType : oldMap.keySet()){
            if((recType.startsWith('CC') || recType.startsWith('Club')) && !oldMap.get(recType).isEmpty() ){
                clubcaroptys.addAll(oldMap.get(recType).values());   
            }
        }
        
         OBM_Notifier__c notifier;
        for(Opportunity opp : clubcaroptys){  
            notifier = CC_Utility.createOBMNotifier(opp, 'Opportunity');
            notifiersList.add(notifier);                
        }
        
        if(!notifiersList.isEmpty()){
            insert notifiersList;
        }
        
    }
    
    
    /***********************************************************************
     * Description   : When the actual ship date is updated we need to retire any trade in assets
     * Created Date  : 08/08/2017
     * **********************************************************************/
    public static void retireAssets(Map<String, List<Opportunity>> trgNewMap,Map<String, Map<Id,Opportunity>> trgOldMap){
        List<Opportunity> CC_OppLst = new List<Opportunity>();
        Map<Id,Opportunity> CC_OppOldMap = new Map<Id,Opportunity>();
        List<Opportunity> requiredopties = new List<Opportunity>();
        
        for(String recType : trgNewMap.keySet()){
            if((recType.startsWith('CC') || recType.startsWith('Club')) && !trgNewMap.get(recType).isEmpty()){
             CC_OppLst.addAll(trgNewMap.get(recType));   
            }
        }
        
        for(String recType : trgOldMap.keySet()){
            if((recType.startsWith('CC') || recType.startsWith('Club')) && !trgOldMap.get(recType).isEmpty()){
             CC_OppOldMap.putAll(trgOldMap.get(recType));   
            }
        }
        
        if(!CC_OppLst.isEmpty()){
            for(opportunity opp : CC_OppLst){
                if(opp.CC_Actual_Ship_Date__c != NULL && null == CC_OppOldMap.get(opp.id).CC_Actual_Ship_Date__c){
                    requiredopties.add(opp);
                }
            }
        }
        
        if(!requiredopties.isEmpty()){
            updateassetstoRetire(requiredopties);
        }
        
    }
    
    
    public static void updateassetstoRetire(List<opportunity> oppList){

        Set<Id> allOppSet = new Set<Id>();
        for (Opportunity o : oppList){allOppSet.add(o.Id);}
        Task [] retirmentReminderTaskLst = new Task [] {};
        
        List<CC_Asset_Group__c> assetstoUpdate  = new List<CC_Asset_Group__c>();
        for(CC_Trade_In__c tradein : [select id, Asset__c, Opportunity__c 
                                             From CC_Trade_In__c
                                             Where Opportunity__c IN:oppList 
                                      AND Asset__r.Fleet_Status__c != 'Retired']){
              assetstoUpdate.add(new CC_Asset_Group__c(Id = tradein.Asset__c, Fleet_Status__c = 'Retired'));                            
              allOppSet.remove(tradein.Opportunity__c);
        }
        if(!assetstoUpdate.isEmpty()){
            update assetstoUpdate;
        }
        /*** we only create reminder task when Course type is not null on the account and Product Market Type = Golf and Opportunity is Golf record type ***/
        for(Opportunity o : [SELECT Id 
                               FROM Opportunity 
                              WHERE Id IN :allOppSet  AND 
                                    (RecordType.DeveloperName != 'CC_Golf_Sales' OR 
                                     CC_Includes_Golf_Products_Checkbox__c=FALSE OR 
                                     Account.CC_Course_Type__c = '' OR 
                                     Account.CC_Course_Type__c = null)]){
          allOppSet.remove(o.Id);    
        }    
        if(!allOppSet.isEmpty()){
          for(Opportunity opp : oppList){
            if(allOppSet.contains(opp.Id)){
              Task t = new Task();
              t.OwnerId = opp.OwnerId;
              t.Subject = 'Please Retire All Fleets/Cars For Your Recently Won Opportunity.';
              t.Status = 'Open';
              t.Priority = 'Normal';
              t.WhatId = opp.Id;
              retirmentReminderTaskLst.add(t);
            }
          }
          if(!retirmentReminderTaskLst.isEmpty()){
            Database.DMLOptions dmlo = new Database.DMLOptions();
            dmlo.EmailHeader.triggerUserEmail = true;
            database.insert(retirmentReminderTaskLst, dmlo);
          }
        }
    }    
}