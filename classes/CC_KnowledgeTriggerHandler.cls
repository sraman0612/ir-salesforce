public class CC_KnowledgeTriggerHandler {
    private static Id cc_recTypeId;    
    
    public static void SetCCDataCategory(Knowledge__kav[] kList) {
        Knowledge__DataCategorySelection[] newRecords = new Knowledge__DataCategorySelection[] {};
        Set<Id> existingRecordIds = new Set<Id>();   
        Set<Id> articleIds = new Set<Id>();
        for (Knowledge__kav k :Klist) {
            If (null != k.id) articleIds.add(k.Id);    
        }
        
        for (Knowledge__DataCategorySelection dcs :[SELECT Id, ParentId, DataCategoryGroupName, DataCategoryName
                                                    FROM Knowledge__DataCategorySelection
                                                    WHERE ParentId IN :articleIds]) {
            if ((dcs.DataCategoryGroupName == 'IR_Global_Category_Group') &&
                (dcs.DataCategoryName == 'Club_Car')) {
                existingRecordIds.add(dcs.ParentId);        
            }                                                       
        }
        System.Debug('@@@ Existing DataCategory Records: ' + existingRecordIds);
        
        for (Knowledge__kav k :kList) {
            if ((null != k.id) &&
                (!existingRecordIds.contains(k.Id)) && 
                (k.RecordTypeId == getCCRecordTypeId())) {
                newRecords.add(new Knowledge__DataCategorySelection(ParentId = k.Id,
                                                                    DataCategoryGroupName = 'IR_Global_Category_Group',
                                                                    DataCategoryName = 'Club_Car'));    
            }
        }
        System.Debug('@@@ New DataCategory Records: ' + newRecords);
        if (!newRecords.isEmpty()){
            insert newRecords; 
            System.Debug('@@@ Inserted Records: ' + newRecords);
        }        
    }
    
    public static void BuildSearchText(Knowledge__kav[] kList) {      
        String[] picklists = new String[]  {'CC_Accessories__c',
                                            'CTS_TechDirect_Article_Type__c',
                                            'CC_Gasoline_Diesel_Powerplant__c',
                                            'CC_Market_Segment__c',
                                            'CC_System__c',
                                            'CC_Vehicles_Diesel__c',
                                            'CC_Vehicles_Electric__c',
                                            'CC_Vehicles_Gasoline__c',
                                            'CC_Vehicles_Lithium_ion__c'};
                
        String searchText = '';
        for (Knowledge__kav k :kList) {
            if((k.RecordTypeId != null) && (k.RecordTypeId == getCCRecordTypeId())) {
                for (String field :pickLists) {
                    if (null != k.get(field) && '' != k.get(field)) {
                        searchText = searchText + k.get(field) + ' | ';
                     }
                }    
            }
            k.CC_Search_Terms__c = searchText;
        }     
    }    
    
    public static Id getCCRecordTypeId() {
        if (null == cc_recTypeId){
            RecordType[] rt = [SELECT Id 
                             FROM RecordType WHERE 
                             SObjectType = 'Knowledge__kav'
                             AND DeveloperName = 'Club_Car'
                             LIMIT 1];
            if (!rt.isEmpty()) cc_recTypeId = rt[0].Id;
        }
        return cc_recTypeId;
    }
}