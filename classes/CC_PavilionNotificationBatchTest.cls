@isTest
public with sharing class CC_PavilionNotificationBatchTest {
    
    private static Id loadTrackingNotificationRecordTypeId = [Select Id From RecordType Where sObjectType = 'CC_Pavilion_Notification_Type__c' and DeveloperName = 'Load_Tracking'].Id;
    
    public static testMethod void TestExecute(){
        
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        update loadTrackingSettings;
        
        loadTrackingSettings.Default_Email_Subject__c = 'Test Message';
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;       
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
        type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';
        type.Available_Delivery_Frequencies__c = 'Per Occurrence';
        insert type;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');
        
        CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId = UserInfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now().addHours(-1);
        pc.Delivery_Frequency__c = 'Per Occurrence';
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.Delivery_Methods__c = 'Email';
        pc.SMS_Delivered_Date__c = null;
        insert pc;        
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        
        //Map<String, CC_Pavilion_Content__c[]> notificationsByFrequency = new Map<String, CC_Pavilion_Content__c[]>();
        //notificationsByFrequency.put('Test',notifications);
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        DataBase.executeBatch(obj);
        Test.stopTest();
        String deliveryMethod = 'Test';
         //obj.buildGroupNotificationsByFrequencyRecipient(notificationsByFrequency,'Test');
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
    public static testMethod void TestExecute1(){
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        loadTrackingSettings.Default_Email_Subject__c = 'Test Message';
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');                
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
        type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Frequencies__c = 'Daily';
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';
        insert type;
        
		CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId = UserInfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now();
        pc.Delivery_Frequency__c = 'Daily';
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.SMS_Delivered_Date__c = null;        
        pc.Delivery_Methods__c = 'Notifications and Alerts';        
        pc.Delivery_Frequency__c = 'Per Occurrence';
        insert pc;  
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        CC_Pavilion_Content__c[] cc_pc = [SELECT id, Name, RecordType.DeveloperName from CC_Pavilion_Content__c];
        //CC_Pavilion_Content__c pc1 = TestUtilityClass.createCustomPavilionContent('Home Page Bulletin');
        
        List<CC_Pavilion_Content__c> notifications = new List<CC_Pavilion_Content__c>();
        CC_Pavilion_Content__c cc = new CC_Pavilion_Content__c();
        cc.CC_Body_1__c = 'Test';
        insert cc;
        
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        DataBase.executeBatch(obj);
        Test.stopTest();
        //List<CC_PavilionNotificationBatch.GroupedNotification> lst = new list<CC_PavilionNotificationBatch.GroupedNotification>();
                    
       // obj.allGroupedNotificationsMap = 
        String deliveryMethod = 'Test';
        obj.groupRecipientNotifications(notifications,'Test');
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
    public static testMethod void TestExecute2(){
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');   
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
        type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Frequencies__c = 'Daily';
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';
        insert type;
        
        CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId = UserInfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now();
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.SMS_Delivered_Date__c = null;       
        pc.Delivery_Methods__c = 'SMS';
        pc.Delivery_Frequency__c = 'Weekly';
        insert pc;         
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        CC_Pavilion_Content__c[] cc_pc = [SELECT id, Name, RecordType.DeveloperName from CC_Pavilion_Content__c];
        //CC_Pavilion_Content__c pc1 = TestUtilityClass.createCustomPavilionContent('Home Page Bulletin');
        
        List<CC_Pavilion_Content__c> notifications = new List<CC_Pavilion_Content__c>();
        CC_Pavilion_Content__c cc = new CC_Pavilion_Content__c();
        cc.CC_Body_1__c = 'Test';
        insert cc;
        
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        DataBase.executeBatch(obj);
        Test.stopTest();
        String deliveryMethod = 'Test';
        //obj.groupRecipientNotifications(notifications,'SMS');
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
    public static testMethod void TestExecute3(){
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
        type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Frequencies__c = 'Daily';
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';
        insert type;
        
        CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId = UserInfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now();
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.SMS_Delivered_Date__c = null;        
        pc.Delivery_Methods__c = 'Email';
        pc.Delivery_Frequency__c = 'Per Occurrence';
        insert pc;        
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        CC_Pavilion_Content__c[] cc_pc = [SELECT id, Name, RecordType.DeveloperName from CC_Pavilion_Content__c];
        //CC_Pavilion_Content__c pc1 = TestUtilityClass.createCustomPavilionContent('Home Page Bulletin');
        
        List<CC_Pavilion_Content__c> notifications = new List<CC_Pavilion_Content__c>();
        CC_Pavilion_Content__c cc = new CC_Pavilion_Content__c();
        cc.CC_Body_1__c = 'Test';
        insert cc;
        
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        DataBase.executeBatch(obj);
        Test.stopTest();
        String deliveryMethod = 'Test';
        obj.groupRecipientNotifications(notifications,'Test');
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
    
    public static testMethod void TestExecute4(){
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        loadTrackingSettings.Default_Email_Subject__c = 'Test Message';
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
        type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Frequencies__c = 'Daily';
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';
        insert type;
        
        CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId = UserInfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now();
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.SMS_Delivered_Date__c = null;        
        pc.Delivery_Methods__c = 'Notifications and Alerts';
        pc.Delivery_Frequency__c = 'Per Occurrence';
        insert pc;
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        CC_Pavilion_Content__c[] cc_pc = [SELECT id, Name, RecordType.DeveloperName from CC_Pavilion_Content__c];
        //CC_Pavilion_Content__c pc1 = TestUtilityClass.createCustomPavilionContent('Home Page Bulletin');
        
        List<CC_Pavilion_Content__c> notifications = new List<CC_Pavilion_Content__c>();
        CC_Pavilion_Content__c cc = new CC_Pavilion_Content__c();
        cc.CC_Body_1__c = 'Test';
        insert cc;
        
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        CC_PavilionNotificationBatch.forceFail = true;
        CC_PavilionNotificationBatch.success = true;
        CC_PavilionNotificationBatch.alreadyAdded = true;
        DataBase.executeBatch(obj);
        Test.stopTest();
        String deliveryMethod = 'Test';
        obj.groupRecipientNotifications(notifications,'Test');
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
    
     public static testMethod void TestExecute5(){
        CC_PavilionNotificationBatch ccp = new CC_PavilionNotificationBatch();
        CC_Load_Tracking_Settings__c loadTrackingSettings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        insert loadTrackingSettings;
        System.debug('loadTrackingSettings-->' + loadTrackingSettings);
        loadTrackingSettings.Notify_Batch_Job_Email_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_SMS_Enabled__c = true;
        loadTrackingSettings.Notify_Batch_Job_Alerts_Enabled__c = true;
        loadTrackingSettings.Enable_Pav_Notify_Batch_Job_Sandbox__c = true;
        loadTrackingSettings.Enable_Pavilion_Notify_Batch_Job__c = true;
        update loadTrackingSettings;
        
        loadTrackingSettings.Default_Email_Subject__c = 'Test Message';
        update loadTrackingSettings;
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c ='CC_PavilionNotificationBatch';
        insert log;
        
        Schema.DescribeSObjectResult d = Schema.SObjectType.CC_Pavilion_Content__c; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByDeveloperName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('CC_Alerts_Notifications');
        
        CC_Pavilion_Notification_Type__c type = new CC_Pavilion_Notification_Type__c();
         type.RecordTypeId = loadTrackingNotificationRecordTypeId;
        type.Type__c = 'Delay';
        type.Active__c = true;
        type.Available_Delivery_Methods__c = 'Email';
        type.Description__c = 'Description';        
        type.Available_Delivery_Frequencies__c = 'Per Occurrence';
        insert type;
         
        CC_Pavilion_Content__c pc = new CC_Pavilion_Content__c();
        pc.OwnerId =userinfo.getUserId();
        pc.RecordTypeId = rtByName.getRecordTypeId();
        pc.CC_Pavilion_Notification_Type__c = type.Id;
        pc.Scheduled_Delivery_Date__c = DateTime.now();
        pc.Delivery_Frequency__c = 'Daily';
        pc.Delivery_Methods__c = 'Email';
        pc.Chatter_Delivered_Date__c = null;
        pc.Email_Delivered_Date__c = null;
        pc.SMS_Delivered_Date__c = null;
        pc.Delivery_Frequency__c = 'Per Occurrence';
        insert pc;
         
        CC_Pavilion_Content__c pc1 = new CC_Pavilion_Content__c();
        pc1.OwnerId =userinfo.getUserId();
        pc1.RecordTypeId = rtByName.getRecordTypeId();
        pc1.CC_Pavilion_Notification_Type__c = type.Id;
        pc1.Scheduled_Delivery_Date__c = DateTime.now();
        pc1.Delivery_Frequency__c = 'Daily';
        pc1.Delivery_Methods__c = 'SMS';
        pc1.Chatter_Delivered_Date__c = null;
        pc1.Email_Delivered_Date__c = null;
        pc1.SMS_Delivered_Date__c = null;
        pc1.Delivery_Frequency__c = 'Per Occurrence';
        insert pc1;         
        
        CC_Pavilion_Content__c pcontent = [SELECT Id, OwnerId, Delivery_Methods__c FROM CC_Pavilion_Content__c where Id=:pc.Id  LIMIT 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User();
        u.Alias = 'standt';
        u.Email='standarduserq123@testorg.com'; 
        u.EmailEncodingKey='UTF-8'; 
        u.LastName='Testing';
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_US'; 
        u.ProfileId = p.id;
        u.TimeZoneSidKey='America/Los_Angeles';
        u.UserName='standarduserq123@testorg.com';
        insert u;
        
        CC_PavilionNotificationBatch.GroupedNotification grn = new CC_PavilionNotificationBatch.GroupedNotification();
        grn.getKey();
        CC_PavilionNotificationBatch.GroupedNotificationMessage gnm = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        CC_PavilionNotificationBatch.FeedItemWrapper wraper = new CC_PavilionNotificationBatch.FeedItemWrapper();
        
        List<CC_Pavilion_Content__c> notifications = new List<CC_Pavilion_Content__c>();
        CC_Pavilion_Content__c cc = new CC_Pavilion_Content__c();
        cc.CC_Body_1__c = 'Test';
        //cc.ownerid=userinfo.getUserId();
        notifications.add(pc);
          notifications.add(pc1);
        //insert notifications;
        
        //Map<String, CC_Pavilion_Content__c[]> notificationsByFrequency = new Map<String, CC_Pavilion_Content__c[]>();
        //notificationsByFrequency.put('Test',notifications);
        Test.startTest();
        CC_PavilionNotificationBatch obj = new CC_PavilionNotificationBatch(); // Add you Class Name
        //DataBase.executeBatch(obj);
        CC_PavilionNotificationBatch.GroupedNotification notPav = new CC_PavilionNotificationBatch.GroupedNotification();
         notPav.deliveryFrequency = 'Per Occurrence';
         notPav.recipient = new user(id = userinfo.getUserId());
         notpav.order = null;
          notPav.getKey();
         List<CC_PavilionNotificationBatch.GroupedNotification> lstPav = new List<CC_PavilionNotificationBatch.GroupedNotification>();
         lstPav.add(notPav);
         
         Map<String,List<CC_PavilionNotificationBatch.GroupedNotification>> mapPav = new Map<string,List<CC_PavilionNotificationBatch.GroupedNotification>>();
         mapPav.put('Per Occurrence',lstPav);
         obj.allGroupedNotificationsMap = mapPav;
         
         
        Test.stopTest();
        String deliveryMethod = 'Test';
         obj.groupRecipientNotifications(notifications,'Per Occurrence');
         CC_PavilionNotificationBatch.FeedItemWrapper fed= new CC_PavilionNotificationBatch.FeedItemWrapper();
         fed.feedItem = new Feeditem();
         fed.groupedNotificationMessage = new CC_PavilionNotificationBatch.GroupedNotificationMessage();
        try{
            obj.logError(null, 'method');
        }catch(Exception e){
            obj.logError(e, 'method');
        }
    }
}