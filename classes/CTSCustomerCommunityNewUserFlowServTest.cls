@isTest
public with sharing class CTSCustomerCommunityNewUserFlowServTest {

    @testSetup
    private static void createData(){

		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Account acc = CTS_TestUtility.createAccount('Test Account', true);
        
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);

        CTS_TestUtility.createContact('Contact123', 'Test', 'testcts@gmail.com', acc.Id, true);  
    }
    }
    @isTest
    private static void testRegisterUserAM(){

        CTS_IOT_Community_Administration__c setting = CTS_IOT_Community_Administration__c.getOrgDefaults();
        Contact ct = [Select Id, Name From Contact LIMIT 1];
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;      
        
        CTSCustomerCommunityNewUserFlowService.RegisterContactRequest request = new CTSCustomerCommunityNewUserFlowService.RegisterContactRequest();        
        request.contactId = ct.Id;
        request.email = 'abc123@abc.com';
        request.mobilePhone = '555-555-5555';
        request.profileId = profileId;
        request.timezone = 'America/Los_Angeles';        

        Test.startTest();

        CTSCustomerCommunityNewUserFlowService.registerUser(new List<CTSCustomerCommunityNewUserFlowService.RegisterContactRequest>{request});

        Test.stopTest();

        User newUser = [Select Id, Email, MobilePhone, ProfileId, TimezoneSidKey From User Where ContactId = :ct.Id];
        ct = [Select Id, CTS_IOT_Community_Status__c From Contact Where Id = :ct.Id];

        system.assertEquals('Internal Submission', ct.CTS_IOT_Community_Status__c);        
        system.assertEquals(request.email, newUser.Email);
        system.assertEquals(request.mobilePhone, newUser.MobilePhone);
        system.assertEquals(request.profileId, newUser.ProfileId);
        system.assertEquals(request.timezone, newUser.TimezoneSidKey);
    }   
    
    @isTest
    private static void testRegisterUserDistributor(){

        CTS_IOT_Community_Administration__c setting = CTS_IOT_Community_Administration__c.getOrgDefaults();
        Contact ct = [Select Id, Name From Contact LIMIT 1];
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;      
        
        CTSCustomerCommunityNewUserFlowService.RegisterContactRequest request = new CTSCustomerCommunityNewUserFlowService.RegisterContactRequest();        
        request.contactId = ct.Id;
        request.email = 'abc123@abc.com';
        request.mobilePhone = '555-555-5555';
        request.profileId = profileId;
        request.timezone = 'America/Los_Angeles';   
        request.communityApprovalStatus = 'Distributor Submission';     

        Test.startTest();

        CTSCustomerCommunityNewUserFlowService.registerUser(new List<CTSCustomerCommunityNewUserFlowService.RegisterContactRequest>{request});

        Test.stopTest();

        User newUser = [Select Id, Email, MobilePhone, ProfileId, TimezoneSidKey From User Where ContactId = :ct.Id];
        ct = [Select Id, CTS_IOT_Community_Status__c From Contact Where Id = :ct.Id];

        system.assertEquals('Distributor Submission', ct.CTS_IOT_Community_Status__c);
        system.assertEquals(request.email, newUser.Email);
        system.assertEquals(request.mobilePhone, newUser.MobilePhone);
        system.assertEquals(request.profileId, newUser.ProfileId);
        system.assertEquals(request.timezone, newUser.TimezoneSidKey);
    }       
}