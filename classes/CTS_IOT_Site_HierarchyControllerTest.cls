/**
 * Test class of CTS_IOT_Site_HierarchyController
 **/
@isTest
private class CTS_IOT_Site_HierarchyControllerTest {
	
    //@testSetup
    static void setup(){
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
    }

    @isTest
    static void testGetInit(){

        setup();

        Test.startTest();

        CTS_IOT_Site_HierarchyController.InitResponse initRes = CTS_IOT_Site_HierarchyController.getInit(null, 10, 0, 0);
        System.assert(initRes != null);
        System.assert(initRes.siteHierarchy != null);
        System.assert(initRes.totalRecordCount > 0);

        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = [Select Id From Account][0].Id;
        Test.setFixedSearchResults(fixedSearchResults);  

        CTS_IOT_Site_HierarchyController.InitResponse initRes2 = CTS_IOT_Site_HierarchyController.getInit('Test', 10, 0, 0);
        System.assert(initRes2 != null);
        System.assert(initRes2.siteHierarchy != null);
        System.assert(initRes2.totalRecordCount > 0);        

        System.assertEquals(10, CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId()).User_Selected_List_Page_Size__c);

        Test.stopTest();
    }

    @isTest
    static void testGetInitException(){

        setup();

        Test.startTest();

        CTS_IOT_Site_HierarchyController.forceError = true;
        CTS_IOT_Site_HierarchyController.InitResponse initRes = CTS_IOT_Site_HierarchyController.getInit(null, 10, 0, 0);
        System.assert(initRes != null);
        Test.stopTest();
    }    
}