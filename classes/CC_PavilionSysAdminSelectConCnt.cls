/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */

public class CC_PavilionSysAdminSelectConCnt {
 public String accountid {
  get;
  set;
 }

 public CC_PavilionSysAdminSelectConCnt(CC_PavilionTemplateController controller) {
 }
 public List < SelectOption > getListOfContacts() {
      accountid = [SELECT accountId from User where id = : UserInfo.getUserId()].accountId;
      List < Contact > ContactList = new List < Contact > ();
      List < SelectOption > SelectContactList = new List < SelectOption > ();
      
      //get All Contacts
      //get all users for contact
      //if users are not present for contacts list them only
      List < User > contactUsers = new List < User >();
      
      ContactList = [Select id, name, accountid  from contact where accountid = : accountid  ];
      System.debug('^^^^^^^^^^^^^^ContactList'+ContactList.size());
      //contactUsers = [select Id ,contactid ,Contact.Name from user where contactid IN :ContactList ];
      contactUsers = [select Id ,Name, ContactId, Contact.Name   from user where contactid IN :ContactList ORDER BY NAME]; 
      System.debug('^^^^^^^^^^^^^^contactUsers'+contactUsers.size());
      Map<Id,Id> useContactMap  = new Map<Id,Id>();  // ContactId, userId
      for(User U : contactUsers){
          if(u.ContactId != null){
              useContactMap.put(U.ContactId,U.Id);
          }
      }
      System.debug('^^^^^^^^^^^^^^^^^Map'+useContactMap.size() + useContactMap);
      if(ContactList.size() > 0){
      for(Contact c :ContactList){
          String userids = useContactMap.get(c.Id);
          System.debug('^^^^^^^^^^^^^^^^^Map'+userids);
          if(userids == null ||  userids == '' ){
               SelectContactList.add(new SelectOption(c.Id, c.name));
          }
      }
          
      }
      
      System.debug('^^^^^^^^^^^^^^^^^Map'+SelectContactList);
      return SelectContactList;
 }

}