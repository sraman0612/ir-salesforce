@isTest
public with sharing class CC_STL_Approval_Override_ScheduleTest {
    
    @TestSetup
    private static void createData(){

        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
        batchControls.Mgr_Approval_Override_Supported_Objects__c = 'CC_Order__c,CC_Short_Term_Lease__c';
        upsert batchControls;        
    }

    @isTest
    private static void testSchedule(){
    	    	
    	Id jobId;
    	String schedule = '0 0 0 1 1 ? 2045';
    	
    	Test.startTest();
    	
    	jobId = System.schedule('Override_Job_Test123', schedule, new CC_STL_Approval_Override_Schedule());
    	
    	Test.stopTest();
    	
    	// Verify the job is scheduled
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :jobID];
    	system.assertEquals(schedule, ct.CronExpression);
    	system.assertEquals(0, ct.TimesTriggered);
    	system.assertEquals('Override_Job_Test123', ct.CronJobDetail.Name);
    	system.assertEquals('7', ct.CronJobDetail.JobType);
    	
    	// Verify the batch jobs were executed
    	system.assertEquals(1, [Select Count() From AsyncApexJob Where ApexClass.Name = 'CC_Resubmit_Service_Contract_Batch' and JobType = 'BatchApex']);
        system.assertEquals(2, [Select Count() From AsyncApexJob Where ApexClass.Name = 'CC_Approval_Override_Batch' and JobType = 'BatchApex']);
    }    
}