public  class CC_PavilionShippingcnt {
  public Integer currentHour {get;set;}
  public Integer cartSize {get;set;}
  public Id orderId {get;set;}
  public List<String> getAllCountriesInSet(){
    List<String> cLst=new List<String>();
    for (StatesCountries__c sc : StatesCountries__c.getAll().values()){if(sc.Type__c=='C'){cLst.add(sc.Name);}}
    cLst.sort();
    return cLst;
  }
  public Set<String> getCountriesWithStates() {
    Set<String> allowedStateCountries = new Set<String>();
    for(StatesCountries__c sc : StatesCountries__c.getAll().values()){
      if(sc.Type__c=='S'){
        allowedStateCountries.add(sc.Country__c.toUpperCase());
      }
    }
    return allowedStateCountries;
  }
    
  public CC_PavilionShippingcnt(CC_PavilionTemplateController controller) {
    orderId= ApexPages.currentPage().getParameters().get('id');
    CC_Order_Item__c[] cartLst = [SELECT Id FROM CC_Order_Item__c WHERE Order__c =:orderId]; //@TODO MOVE THIS TO REMOTE IF LOAD PERFORMANCE IMPACTED 
    cartSize=cartLst.size();
    String EDTCurrDateTimeStr = DateTime.now().format('yyyy-MM-dd HH:mm:ss','America/New_York');
    DateTime EDTCurrDateTime = Datetime.valueof(EDTCurrDateTimeStr);
    currentHour = EDTCurrDateTime.hour();
  }
    
  @remoteaction 
  Public static CC_Account_Address__c newaddress(string addID,string aId,string Addresseename,string address1,string address2,string address3,string zipcode,string city,
                                                 string ShippingState,string country,string Contactname,string Telephonenumber,string Faxnumber){

    CC_Account_Address__c objadr = new CC_Account_Address__c();
    if(addID!='')
      objadr.Id=addID;
    objadr.CC_Address_1__c=Address1 ;
    System.debug('**Address1'+objadr.CC_Address_1__c);
    objadr.CC_Address_2__c=Address2;
    System.debug('**Address2'+objadr.CC_Address_2__c);
    objadr.CC_Address_3__c=Address3;
    System.debug('**Address3'+objadr.CC_Address_3__c);
    objadr.CC_Addressee_name__c=Addresseename;
    System.debug('**Address name'+objadr.CC_Addressee_name__c);
    objadr.CC_Contact_name__c=Contactname;
    objadr.CC_Telephone_number__c=Telephonenumber;
    objadr.CC_Fax_number__c=Faxnumber;
    objadr.CC_City__c=City;
    objadr.CC_Country__c=country;
    objadr.CC_State__c=ShippingState;
    objadr.CC_ZIp_Code__c=null!=zipcode?zipcode:'';
    objadr.CC_Account__c=aId;
    upsert objadr;
    return objadr;
  }
    
  @remoteaction
  public static List<String> getStatesList(String Country){
    List<String> sLst=new List<String>();
    for (StatesCountries__c sc : StatesCountries__c.getAll().values()){
      if(sc.Type__c=='S' && sc.Country__c==Country)
        sLst.add(sc.Name+ '::' + sc.CODE__c);
    }
    sLst.sort();
    return sLst;
  }
  
  @remoteaction
  public static String[] getShipDates() {
      String[] returnLst = new String[]{};
      String dateString = '';
      for (CC_Parts_Calendar__c d : [SELECT Date__c FROM CC_Parts_Calendar__c where Date__c >= :system.today() ORDER BY Date__c]){
          /*** Must prefix month and day with leading zero because Internet Explorer's javascript will not interpret '2018-1-1' as a valid date string value. ***/
          dateString = string.valueOf(d.Date__c.year());
          dateString += (d.Date__c.month() < 10) ? '-0' : '-';
          dateString += d.Date__c.month();
          dateString += (d.Date__c.day() < 10) ? '-0' : '-';
          dateString += d.Date__c.day();
          returnLst.add(dateString);
      }
      return returnLst;
  }

  @remoteAction
  public static String checkTaxStatus(String state,String country,String AcctId){
    String billCountry = [SELECT BillingCountry FROM Account WHERE Id = :AcctId].BillingCountry;
    if (billCountry=='USA' || billCountry.toUpperCase()=='CANADA'){
      if (null!= PavilionSettings__c.getInstance(state+'exemptSuffix'))
        return 'OK::' + PavilionSettings__c.getInstance(state+'exemptSuffix').value__c;
      for(CC_Tax_Certificate__c tc : [SELECT Tax_Suffix__c FROM CC_Tax_Certificate__c WHERE CC_State__c =:state and CC_End_Date__c >=:system.Today() and CC_Account__c =:AcctId]){
        return 'OK::' + tc.Tax_Suffix__c;
      }
    } else {
      return 'OK::' + PavilionSettings__c.getInstance('INTNLexemptSuffix').value__c; 
    }
    return 'NOTEXEMPT';
  }
  
  @remoteAction
  public static String getShipAddress(String addId){
    //expected order of elements in response is id,addressee,Contact,add1,add2,add3,city,state,zip,country,phone,fax
    string retMsg='';
    string objKey=addId.substring(0,3);
    Schema.DescribeSObjectResult r = CC_Account_Address__c.sObjectType.getDescribe();
    String acctAddkey = r.getKeyPrefix();
    Schema.DescribeSObjectResult r2 = CC_Order__c.sObjectType.getDescribe();
    String ordkey = r2.getKeyPrefix();
    Id acctId;
    if(objKey==acctAddkey){
      for (CC_Account_Address__c  aa : [SELECT Id,CC_Telephone_number__c,CC_Fax_number__c,CC_Address_1__c,CC_Address_2__c,CC_Address_3__c,CC_Addressee_name__c,
                                               CC_Contact_name__c,CC_City__c,CC_Country__c,CC_State__c,CC_ZIp_Code__c
                                          FROM CC_Account_Address__c
                                         WHERE id=:addId]){
        retMsg=aa.Id+'::'+aa.CC_Addressee_name__c+'::'+aa.CC_Contact_name__c+'::'+aa.CC_Address_1__c+'::'+aa.CC_Address_2__c+'::'+aa.CC_Address_3__c+'::'+aa.CC_City__c+'::';
        retMsg+=aa.CC_State__c+'::'+aa.CC_ZIp_Code__c+'::'+aa.CC_Country__c+'::'+aa.CC_Telephone_number__c+'::'+aa.CC_Fax_number__c;
      }
      return retMsg;
    } else if(objKey==ordKey){
      CC_Order__c o=[SELECT Id,CC_Address_line_1__c,CC_Address_line_2__c,CC_Address_line_3__c,CC_Addressee_name__c,CC_Contact_name__c,CC_Fax_number__c, 
                            CC_Telephone_number__c,CC_City__c,CC_States__c,CC_Postal_code__c,CC_Country__c,CC_Account__c
                       FROM CC_Order__c 
                      WHERE id =: addId
                      LIMIT 1];
      if(null!=o.CC_Country__c && ''!=o.CC_Country__c){
        
        retMsg=o.Id+'::'+o.CC_Addressee_name__c+'::'+o.CC_Contact_name__c+'::'+o.CC_Address_Line_1__c+'::'+o.CC_Address_LIne_2__c+'::'+o.CC_Address_Line_3__c+'::'+o.CC_City__c+'::';
        retMsg+=o.CC_States__c+'::'+o.CC_Postal_code__c+'::'+o.CC_Country__c+'::'+o.CC_Telephone_number__c+'::'+o.CC_Fax_number__c;
        retMsg+='::ORDER';
        return retMsg;
      }
      addId=o.CC_Account__c;
      
    }
    //return default ship add from acct
    //code contributed by @Priyanka Baviskar for issue no 7562156.
    Account a = [SELECT Id,Name,CC_Shipping_Address_1__c,CC_Shipping_Address_2__c,CC_Shipping_Address_3__c,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry FROM Account WHERE Id =:addId LIMIT 1];
    if(a.CC_Shipping_Address_1__c==null||a.CC_Shipping_Address_1__c=='')
    {
    a.CC_Shipping_Address_2__c=null;
    a.CC_Shipping_Address_3__c=null;
    retMsg=a.Id+'::'+a.Name+'::'+''+'::'+a.ShippingStreet+'::'+a.CC_Shipping_Address_2__c+'::'+a.CC_Shipping_Address_3__c+'::'+a.ShippingCity+'::';
    }
    else
    {
    retMsg=a.Id+'::'+a.Name+'::'+''+'::'+a.CC_Shipping_Address_1__c+'::'+a.CC_Shipping_Address_2__c+'::'+a.CC_Shipping_Address_3__c+'::'+a.ShippingCity+'::';
    }
    
    retMsg+=a.ShippingState+'::'+a.ShippingPostalCode+'::'+a.ShippingCountry+'::'+''+'::'+'';
    return retMsg;
  }
    
  @remoteAction
  public static Boolean updateOrder(String oid, String addId, String shipMethod, String taxSuffix, String shipAcct, String reqDate){
    CC_Order__c o = new CC_Order__c();
    o.Id=oid;
    o.CC_Tax_Suffix__c=taxSuffix;
    o.CC_Shipping_Methods__c=shipMethod;
    o.CC_Carrier_ID__c=CC_Shipping_Methods__c.getInstance(shipMethod).CC_Carrier_ID__c;
    String shipInstruct = CC_Shipping_Methods__c.getInstance(shipMethod).Shipping_Instruction__c;
    o.CC_Shipping_Instructions__c=shipAcct=='null'||shipAcct==''?shipInstruct:shipInstruct + ' ' + shipAcct;
    o.CC_Priority_ID__c=CC_Shipping_Methods__c.getInstance(shipMethod).CC_Priority_ID__c;
    o.CC_Request_Date__c=Date.parse(reqDate);
    string addKey=addId.substring(0,3);
    Schema.DescribeSObjectResult r = CC_Account_Address__c.sObjectType.getDescribe();
    String addObjKey = r.getKeyPrefix();
    Schema.DescribeSObjectResult r2 = CC_Order__c.sObjectType.getDescribe();
    String ordkey = r2.getKeyPrefix();
    if (addKey==ordKey){
      //no address update, it was already being pulled from the order
    } else if (addKey==addObjKey){
      CC_Account_Address__c  aa = [SELECT Id,CC_Telephone_number__c,CC_Fax_number__c,CC_Address_1__c,CC_Address_2__c,CC_Address_3__c,CC_Addressee_name__c,
                                          CC_Contact_name__c,CC_City__c,CC_Country__c,CC_State__c,CC_ZIp_Code__c
                                     FROM CC_Account_Address__c
                                    WHERE id=:addId];
      o.CC_Address_line_1__c=aa.CC_Address_1__c;
      o.CC_Address_line_2__c=aa.CC_Address_2__c;
      o.CC_Address_line_3__c=aa.CC_Address_3__c;
      o.CC_Addressee_name__c=aa.CC_Addressee_name__c;
      o.CC_Contact_name__c=aa.CC_Contact_name__c;
      o.CC_Fax_number__c=aa.CC_Fax_number__c;
      o.CC_Telephone_number__c=aa.CC_Telephone_number__c;
      o.CC_City__c=aa.CC_City__c;
      o.CC_States__c=aa.CC_State__c;
      o.CC_Postal_code__c=aa.CC_ZIp_Code__c;
      o.CC_Country__c=aa.CC_Country__c;
    } else {
      //get default address from acct
      Account a = [SELECT Id,Name,CC_Shipping_Address_1__c,CC_Shipping_Address_2__c,CC_Shipping_Address_3__c,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry FROM Account WHERE Id =:addId LIMIT 1];
      o.CC_Address_line_1__c=a.ShippingStreet;
      o.CC_Address_line_2__c=a.CC_Shipping_Address_2__c;
      o.CC_Address_line_3__c=a.CC_Shipping_Address_3__c;
      o.CC_Addressee_name__c=a.Name;
      o.CC_Contact_name__c=null;
      o.CC_Fax_number__c=null;
      o.CC_Telephone_number__c=null;
      o.CC_City__c=a.ShippingCity;
      o.CC_States__c=a.ShippingState;
      o.CC_Postal_code__c=a.ShippingPostalCode;
      o.CC_Country__c=a.ShippingCountry;
    }
    upsert o;
    return true;
  }

  @remoteaction
  public static  Map<String,String> theShipMethods(string orderId,string shipClass,string shipCountry) {
    Set<String> orderItems = new Set<String>();
    Map<String,String> selectMap = new Map<String,String>();
    selectMap.put('-- Select One --','');
    Boolean domMF=true;
    Boolean intnlMF=true;
    Boolean domParcel=true;
    Boolean intnlParcel=true;
    Boolean caMF=true;
    Boolean caParcel=true;
    
    for (CC_Order_Item__c oi : [SELECT CC_Product_Code__c FROM CC_Order_Item__c WHERE Order__c =:orderId]){orderItems.add(oi.CC_Product_Code__c);}
    for (Product2 p: [SELECT CC_Domestic_MF__c,CC_Domestic_Parcel__c,CC_International_MF__c,CC_International_Parcel__c FROM Product2 WHERE ProductCode in:orderItems and RecordType.DeveloperName='Club_Car']){
      if(!p.CC_Domestic_MF__c){domMF=false;}
      if(!p.CC_Domestic_Parcel__c){domParcel=false;}
      if(!p.CC_International_MF__c){intnlMF=false;}
      if(!p.CC_International_Parcel__c){intnlParcel=false;}
    }
    for(CC_Shipping_Methods__c sm: CC_Shipping_Methods__c.getAll().values()){
      if(shipClass=='US'){
        if(sm.CC_Is_Domestic__c){
          if(sm.CC_Eligible_for_Parcel__c && !domParcel){}  // NOT ALL ITEM ELIGIBLE FOR PARCEL, DONT ADD
          else if (sm.CC_Eligible_for_Motorfreight__c && !domMF){} // NOT ALL ITEMS ELIGIBLE FOR MF, DONT ADD
          else {selectMap.put(sm.name,sm.CC_Carrier_ID__c);}
        }
      } else if(shipClass=='CA'){
        if(sm.CC_Canada_Only__c){
          if(sm.CC_Eligible_for_Parcel__c && !domParcel){}  // NOT ALL ITEM ELIGIBLE FOR PARCEL, DONT ADD
          else if (sm.CC_Eligible_for_Motorfreight__c && !domMF){} // NOT ALL ITEMS ELIGIBLE FOR MF, DONT ADD
          else {selectMap.put(sm.name,sm.CC_Carrier_ID__c);}
        }
      } else { //  IS INTNL
        if(sm.CC_Is_International__c){
          if (sm.CC_Eligible_for_Parcel__c && !intnlParcel){} // NOT ALL ITEM ELIGIBLE FOR PARCEL, DONT ADD
          else if (sm.CC_Eligible_for_Motorfreight__c && !intnlMF){} // NOT ALL ITEMS ELIGIBLE FOR MF, DONT ADD
          else  if (shipCountry!='USA'&&sm.CC_US_Forwarder__c){} // NON US ADDRESSES CANNOT USE FORWARDERS
          else {selectMap.put(sm.name,sm.CC_Carrier_ID__c);}
        }
      }
    }
    return selectMap;
  }
  
  @remoteAction
  public static List<addressWrapper> getExistingAddresses(String AcctId){
    addressWrapper [] addressWrapperLst = new List<addressWrapper>();
    Account a = [SELECT Id,CC_Shipping_Address_1__c,CC_Shipping_Address_2__c,CC_Shipping_Address_3__c,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Name FROM Account WHERE Id = :AcctId];
    addressWrapper defaultShipAdd = new addressWrapper();
    defaultShipAdd.uid=a.Id;
    defaultShipAdd.addressee=a.Name;
    defaultShipAdd.ct=''; 
     //code contributed by @Priyanka Baviskar for issue no 7562156.
    if(a.CC_Shipping_Address_1__c==null||a.CC_Shipping_Address_1__c=='')
    {
    defaultShipAdd.a1=a.ShippingStreet;
    defaultShipAdd.a2=null;
    defaultShipAdd.a3=null;

    }
    else
    {
    defaultShipAdd.a1=a.CC_Shipping_Address_1__c;
    defaultShipAdd.a2=a.CC_Shipping_Address_2__c;
    defaultShipAdd.a3=a.CC_Shipping_Address_3__c;
    }
    
    
    
    defaultShipAdd.city=a.ShippingCity;
    defaultShipAdd.state=a.ShippingState;
    defaultShipAdd.zip=a.ShippingPostalCode;
    defaultShipAdd.country=a.ShippingCountry;
    defaultShipAdd.phone='';
    defaultShipAdd.fax='';
    addressWrapperLst.add(defaultShipAdd);
    for (CC_Account_Address__c aa : [SELECT Id, CC_Address_1__c, CC_Address_2__c, CC_Address_3__c, CC_Addressee_name__c, CC_City__c, CC_Contact_name__c, CC_Country__c, 
                                           CC_Fax_number__c, CC_State__c, CC_Telephone_number__c, CC_ZIp_Code__c
                                      FROM CC_Account_Address__c
                                     WHERE CC_Account__c = :AcctId]){
      addressWrapperLst.add(new addressWrapper(aa));
    }
    return addressWrapperLst;
  }
  
  @Remoteaction
  public static void deleteAddress(Id addId){
    CC_Account_Address__c address2Delete = [SELECT Id FROM CC_Account_Address__c WHERE Id = :addId];
    delete address2Delete;
  }

  class addressWrapper{
    public string uid {get;set;}
    public string addressee{get;set;}
    public string ct{get;set;}
    public string a1{get;set;}
    public string a2{get;set;}
    public string a3{get;set;}
    public string city{get;set;}
    public string state{get;set;}
    public string zip{get;set;}
    public string country{get;set;}
    public string phone{get;set;}
    public string fax{get;set;}
    
    public addressWrapper(){}

    public addressWrapper(CC_Account_Address__c a){
      uid=a.Id;
      addressee=a.CC_Addressee_name__c;
      ct=a.CC_Contact_name__c;
      a1=a.CC_Address_1__c;
      a2=a.CC_Address_2__c;
      a3=a.CC_Address_3__c;
      city=a.CC_City__c;
      state=a.CC_State__c;
      zip=a.CC_ZIp_Code__c;
      country=a.CC_Country__c;
      phone=a.CC_Telephone_number__c;
      fax=a.CC_Fax_number__c;
    }
  }
}