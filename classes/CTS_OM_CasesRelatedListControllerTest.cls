@isTest
public class CTS_OM_CasesRelatedListControllerTest {
	@TestSetup
    private static void createData(){
    	Account acct = TestDataUtility.createAccount(true, 'Test Account', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId()); 
        Contact con =  new contact(FirstName = 'test', LastName = 'Test', Title ='Mr', AccountId = acct.id, email='test@gmail.com', Phone = '555-555-5555');
        insert con;
        List<Case> cases = new List<Case>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
        cases.add(TestDataUtility.createCase(false, recordTypeId, con.id, acct.id, 'Test Case subject', 'New'));
        cases.add(TestDataUtility.createCase(false, recordTypeId, con.id, acct.id, 'Test Case subject', 'New'));
        cases.add(TestDataUtility.createCase(false, recordTypeId, con.id, acct.id, 'Test Case subject', 'New'));
        cases.add(TestDataUtility.createCase(false, recordTypeId, con.id, acct.id, 'Test Case subject', 'New'));
        cases.add(TestDataUtility.createCase(false, recordTypeId, con.id, acct.id, 'Test Case subject', 'New'));
        insert cases;
    }   
    
    @isTest
    private static void testCaseRelatedLists(){
        Case cs ;
        Id recordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Case' Order By Name ASC LIMIT 1].Id;
        
        for(Case c : [select id, ContactEmail from Case limit 1]){
            cs = c;
        }
        CTS_OM_CasesRelatedListController.getData(cs.Id, recordTypeId, NULL, 'Subject,Description', 'Account.Name','CaseNumber'); 
    }
}