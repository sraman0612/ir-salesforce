@isTest
private class PT_OppPusherTests {
    
    static testMethod void myOppUnitTest() {

        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;

        Account a = new Account(
            RecordTypeId = ptAcctRTID,
            Name = 'TestAccoppusher',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );
        insert a;

        PT_CreateScheduleOpp.testMonths = 1;

        PT_Parent_Opportunity__c pOpp = TestUtilityClass.createParentOpp(a.Id);
        pOpp.Lead_SourcePL__c='Other';
        insert pOpp;
        
        // create an oppty
        Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();     
        Opportunity oTestOppty =  new Opportunity(name= 'testOppty',closedate= date.newinstance(2015,11,11),stagename='Stage 1. Qualify', PT_parent_Opportunity__c=pOpp.Id,RecordTypeId=PT_OpptyRecordTypeId);
        insert oTestOppty;

        Test.startTest();
        
        // change the month pushcount+1
        oTestOppty.CloseDate=date.newinstance(2015,12,11);
        oTestOppty.PT_parent_Opportunity__c = pOpp.Id;
        //Opportunity oTestUpdateOppty=[select Id from Opportunities where Id=oTestOppty.Id]
        update oTestOppty;

        Opportunity oReadOppty=[select Id,PT_PushCount__c from Opportunity where id=:oTestOppty.Id];
        System.debug(oReadOppty.PT_PushCount__c);
        //System.assert(oReadOppty.PT_PushCount__c==1); // by CG 
        // make it later this month, no change
        oTestOppty.CloseDate=date.newinstance(2015,12,31);
        update oTestOppty;

        oReadOppty=[select Id,PT_PushCount__c from Opportunity where id=:oTestOppty.Id];
        System.debug(oReadOppty.PT_PushCount__c);
        //System.assert(oReadOppty.PT_PushCount__c==1); // by CG 
        // make it next year, but an earlier month, pushcount+1
        oTestOppty.CloseDate=date.newInstance(2016, 1,1);
        update oTestOppty;

        oReadOppty=[select Id,PT_PushCount__c from Opportunity where id=:oTestOppty.Id];
        System.debug(oReadOppty.PT_PushCount__c);
        //System.assertEquals(oReadOppty.PT_PushCount__c, 2); // by CG
        // don't change the date, no change
        oTestOppty.CloseDate=date.newInstance(2016,1,1);
        update oTestOppty;

       oReadOppty=[select Id,PT_PushCount__c from Opportunity where id=:oTestOppty.Id];
        System.debug(oReadOppty.PT_PushCount__c);
        //System.assertEquals(oReadOppty.PT_PushCount__c,2); // by CG 
        
        Test.stopTest();
    }
}