@isTest
public class CC_PavilionPartsAndPriceBulletinsTest 
{
    static testmethod void UnitTest_CC_PavilionPartsAndPriceBulletinsCnt() {
        List<ContentVersion> contentVersionList =new  List<ContentVersion>();
        CC_PavilionTemplateController controller;
        // Creation of Test Data
        Account newAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert newAccount;
        System.assertEquals('Sample Account',newAccount.Name);
        System.assertNotEquals(null, newAccount.Id);
        Account secondAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert secondAccount;
        System.assertEquals('Sample Account',secondAccount.Name);
        System.assertNotEquals(null, secondAccount.Id);
        Contract newContract =TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Commercial Utility',newAccount.Id);
        insert newContract;
        System.assertEquals('Commercial Utility', newContract.CC_Sub_Type__c);
        System.assertNotEquals(null, newContract.Id);
        ContentVersion contentVersionInsertFirst = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Parts');
        contentVersionList.add(contentVersionInsertFirst); 
        ContentVersion contentVersionInsertFourth = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Parts Pricing');
        contentVersionList.add(contentVersionInsertFourth);
        insert contentVersionList;  
        PavilionSettings__c newPavilionSettings = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',secondAccount.Id);
        insert newPavilionSettings;
        Test.startTest();
        CC_PavilionPartsAndPriceBulletinscnt pavBulletins = new CC_PavilionPartsAndPriceBulletinscnt(controller);
        CC_PavilionPartsAndPriceBulletinscnt.getMarkets(newAccount.id);
        CC_PavilionPartsAndPriceBulletinscnt.getYears();
        CC_PavilionPartsAndPriceBulletinscnt.getBulletins(true, true,'Japan', 'USD', newAccount.id);
        try
        {
            CC_PavilionPartsAndPriceBulletinscnt.searchBulletins(true, true,'Japan', 'USD', newAccount.id, 'Parts');
            CC_PavilionPartsAndPriceBulletinscnt.searchBulletins(true, true,'Japan', 'USD', newAccount.id, 'Parts Pricing');
        }
        Catch(Exception e){}
        Test.stopTest();
    }
}