/*****************************************************************
Name:       RS_CRS_CustomerSearch_Controller
VF Page:    WIDP_CustomerSearchPanel
Purpose:    Class used for Customer search.
History:
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Ashish Takke    12/26/2017  INITIAL DEVELOPMENT
*****************************************************************/
public with sharing class RS_CRS_CustomerSearch_Controller {

    public String firstName    {get; set;}
    public String lastName     {get; set;}
    public String phone        {get; set;}
    public String email        {get; set;}
    public String street       {get; set;}
    public String city         {get; set;}
    public String state        {get; set;}
    public String country      {get; set;}
    public String zipCode      {get; set;}
    public String serialNumber {get; set;}
    public List<Account> accList {get; set;}
    
    /******************************************************************************
    Constructor
    ******************************************************************************/
    public RS_CRS_CustomerSearch_Controller(ApexPages.StandardController controller) {
        //Initialise list to avoid null error
        accList = new List<Account>();
    }

    /******************************************************************************
    Purpose            :  Method used to Search accounts in salesforce                                                
    Parameters         :  None
    Returns            :  null
    Throws [Exceptions]:                                                          
    ******************************************************************************/
    public PageReference searchCustomer(){
    
        List<Id> accIdList = new List<Id>();
        List<String> orWhereClauses = new List<String>();

        //get list of RS CRS person and business account record types
        List<ID> accountRecordTypeIDList = new List<ID>();
        accountRecordTypeIDList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Business_Account_Record_Type));
        accountRecordTypeIDList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type));
        accountRecordTypeIDList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_IWD_Dealer_Account_Record_Type));
        
        String recordTypeIDStr = '(';
        for(String recordTypeID: accountRecordTypeIDList){
            recordTypeIDStr = recordTypeIDStr  + '\'' + recordTypeID + '\', ';
        }
        recordTypeIDStr = recordTypeIDStr + '\'\')';
        
        //get list of account Ids matching Asset serial number
        if(serialNumber != null && serialNumber != ''){
            serialNumber = String.escapeSingleQuotes(serialNumber.trim()) + '%' ;
            
            List<Asset> assetList = [Select Name, AccountId From Asset Where Name LIKE : serialNumber ];
            for(Asset assetRec: assetList){
                accIdList.add(assetRec.AccountId);
            }
        }
        
        //get list of account Ids matching other search parameters on page
        String query = 'Select Id From Account ';
        
        if(lastName != null && lastName != ''){
            lastName = String.escapeSingleQuotes(lastName.trim());
            
            orWhereClauses.add('Name LIKE \'%' + lastName + '%\'');
            orWhereClauses.add('LastName LIKE \'' + lastName + '%\'');
        }
        
        if(firstName != null && firstName != ''){
            firstName = String.escapeSingleQuotes(firstName.trim());
            
            orWhereClauses.add('Name LIKE \'%' + firstName + '%\'');
            orWhereClauses.add('firstName LIKE \'' + firstName + '%\'');
        }

        if(phone != null && phone != ''){
            phone = String.escapeSingleQuotes(phone.trim());
            
            orWhereClauses.add('Phone LIKE \'' + phone + '%\'');
            orWhereClauses.add('PersonMobilePhone LIKE \'' + phone + '%\'');
        }
        
        if(email != null && email != ''){
            email = String.escapeSingleQuotes(email.trim());
            
            orWhereClauses.add('Email_ID__c = \'' + email + '\'');
        }
        
        if(street != null && street != ''){
            street = String.escapeSingleQuotes(street.trim());
            
            orWhereClauses.add('PersonMailingStreet LIKE \'' + street + '%\'');
        }
        
        if(city != null && city != ''){
            city = String.escapeSingleQuotes(city.trim());
            
            orWhereClauses.add('PersonMailingCity = \'' + city + '\'');
        }
        
        if(state != null && state != ''){
            state = String.escapeSingleQuotes(state.trim());
            
            orWhereClauses.add('PersonMailingState = \'' + state + '\'');
        }
        
        if(country != null && country != ''){
            country = String.escapeSingleQuotes(country.trim());
            
            orWhereClauses.add('PersonMailingCountry = \'' + country + '\'');
        }
                
        if(zipCode != null && zipCode != ''){
            zipCode = String.escapeSingleQuotes(zipCode.trim());
            
            orWhereClauses.add('PersonMailingPostalCode = \'' + ZipCode + '\' '); 
        } 
        
        String whereClause;
        if (!orWhereClauses.isEmpty()) {
            whereClause = String.join(orWhereClauses, ' OR ');
        }
            
        if (whereClause != null) {
            query = query + 'Where RecordTypeId IN ' + recordTypeIDStr + ' AND (' + whereClause + ')';

            System.debug('##Query::-'+query);
            
            List<Account> searchAccList = Database.query(query);
            
            for(Account acc: searchAccList){
                accIdList.add(acc.Id);
            }
        }
        
        if(accIdList!= null){
            //get list of account details based on matched serial number and other search parameters
            accList = [Select Id, Name, FirstName, LastName, Type, Phone, Email_ID__c 
                       From Account 
                       Where RecordtypeId IN: accountRecordTypeIDList AND Id IN: accIdList];
        }
 
        return null;
    }  
    
}