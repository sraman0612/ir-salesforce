@isTest
public class ProductEventHandlerTest{
    static testmethod void createPriceBook(){
        Product2 p = new Product2();
        p.name = 'Test Trigger';
        p.Family = 'Club Car Parts';
        insert p;
        List<PricebookEntry> listPriceBookToReturn = [SELECT Id FROM PricebookEntry WHERE Product2Id =: p.Id ];
        System.assertEquals(1, listPriceBookToReturn.size() );
        p.Description='Sample Product';
        update p;
        delete p;
        undelete p;
    }
    
    static testmethod void updateLocationInventory(){
        //create warehouse
        Schema.Location whse = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=FALSE,LocationType='Warehouse',Name='testWhse',TimeZone='America/New_York');
        insert whse;
        //create location
        Schema.Location loc = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc',parentlocationId=whse.Id,TimeZone='America/New_York');
        insert loc;
        //create product
        Product2 p = new Product2(name = 'testFSLProduct',Family = 'Club Car Parts',ERP_Item_Number__c='testFSLProduct');
        insert p;
        //create productitem w qty 1
        ProductItem pi = new ProductItem(LocationId=loc.Id, Product2id=p.Id, QuantityOnHand=1);
        insert pi;
        system.assertEquals(1, [SELECT QuantityOnHand FROM ProductItem WHERE Id =:pi.Id].QuantityOnHand);
        //inventory update via product
        p.CC_Location__c='testLoc';
        p.CC_Warehouse__c='testWhse';
        p.CC_Location_Quantity__c=2;
        update p;
        system.assertEquals(2, [SELECT QuantityOnHand FROM ProductItem WHERE Id =:pi.Id].QuantityOnHand);
        //non inventory update on product should not change inventory
        p.CC_Item_Accounting_Class__c='xyz';
        update p;
        system.assertEquals(2, [SELECT QuantityOnHand FROM ProductItem WHERE Id =:pi.Id].QuantityOnHand);
    }
    
    static testmethod void createLocationInventory(){
        //create warehouse
        Schema.Location whse = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=FALSE,LocationType='Warehouse',Name='testWhse',TimeZone='America/New_York');
        insert whse;
        //create location
        Schema.Location loc = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc',parentlocationId=whse.Id,TimeZone='America/New_York');
        insert loc;
        //create product
        Product2 p = new Product2(name = 'testFSLProduct',Family = 'Club Car Parts',ERP_Item_Number__c='testFSLProduct');
        insert p;
        //inventory update via product
        p.CC_Location__c='testLoc';
        p.CC_Warehouse__c='testWhse';
        p.CC_Location_Quantity__c=2;
        update p;
        //new PI should be created with quantity 2
        system.assertEquals(2, [SELECT QuantityOnHand FROM ProductItem WHERE Product2Id =:p.Id].QuantityOnHand);
    }
    
    static testmethod void createNewLocation(){
        //create warehouse
        Schema.Location whse = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=FALSE,LocationType='Warehouse',Name='testWhse',TimeZone='America/New_York');
        insert whse;
        //create product
        Product2 p = new Product2(name = 'testFSLProduct',Family = 'Club Car Parts',ERP_Item_Number__c='testFSLProduct');
        insert p;
        //inventory update via product
        p.CC_Location__c='testLoc';
        p.CC_Warehouse__c='testWhse';
        p.CC_Location_Quantity__c=2;
        update p;
        //new locaiton and PI created with quantity 2
        system.assertEquals(2, [SELECT QuantityOnHand FROM ProductItem WHERE Product2Id =:p.Id].QuantityOnHand);
    }
}