/**
 * Created by CloudShapers on 11/19/2020.
 */

public class AssignedResourceTriggerHandler {

    public class NoLocationException extends Exception {}
    static final Id CTS_RECORDTYPE = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('CTS_Service_Appointment_Record_Type').getRecordTypeId();

    /*
* @Name	: createProductRequired
* @Description: Creates Product Item Record for that particular Location
* @Date   	: 19-November-2020
* @Author 	: CloudShapers
*/
    public static void createProductItems(List<AssignedResource> assignedResources,
            Map<Id, AssignedResource> oldAssignedResourceMap) {
        try {
            //Declaring Collections
            Set<String> serviceAppointmentIdSet = new Set<String>();

            Map<String, Set<String>> SAIdToProductIdMap = new Map<String, Set<String>>();
            Map<String, Set<String>> WOLIIdToSAIdMap = new Map<String, Set<String>>();
            Map<String, String> SAToLocationIdMap = new Map<String, String>();

            List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment>();

            //Check if New Assigned Resource/Service Resource Change and Service Appointment Not Null
            for (AssignedResource assignedResourceRec : assignedResources) {
                if (assignedResourceRec.ServiceAppointmentId != null && (oldAssignedResourceMap == null ||
                        (assignedResourceRec.ServiceResourceId != oldAssignedResourceMap.get(assignedResourceRec.Id).ServiceResourceId))) {
                    serviceAppointmentIdSet.add(assignedResourceRec.ServiceAppointmentId);
                }
            }


            if (!serviceAppointmentIdSet.isEmpty()) {
                //Process only CTS Service Appointments
                serviceAppointments = [
                        SELECT Id
                        FROM ServiceAppointment
                        WHERE Id IN :serviceAppointmentIdSet
                        AND RecordTypeId = :CTS_RECORDTYPE
                ];

                if (!serviceAppointments.isEmpty()) {
                    //Get WOLI related to Service Appointments
                    WOLIIdToSAIdMap = getWOLIToSAMap(serviceAppointments);

                    //Get Locations related to service Appointments
                    SAToLocationIdMap = getAssignedResourceToLocationMap(serviceAppointments);

                    if (!WOLIIdToSAIdMap.isEmpty() && !SAToLocationIdMap.isEmpty()) {
                        //Get Product Required from Service Appointment to WOLI Map
                        SAIdToProductIdMap = getSAToProductRequiredMap(WOLIIdToSAIdMap);

                        //Insert Product Items
                        if (!SAIdToProductIdMap.isEmpty())
                            insertProductItems(SAToLocationIdMap, SAIdToProductIdMap);
                    }
                }
            }
        } catch (NoLocationException er) {
            throw new NoLocationException(System.Label.Service_Location_Missing_Error);
        }catch (Exception er) {
            System.debug('Error in Creating Product Items' + er.getMessage());
        }
    }

    /*
* @Name	: getWOLIToSAMap
* @Description: Fetches the Work Order Line to Service Appointment Map
* @Date   	: 19-November-2020
* @Author 	: CloudShapers
*/
    private static Map<String, Set<String>> getWOLIToSAMap(List<ServiceAppointment> serviceAppointmentRecords) {
        List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment>();
        Map<String, Set<String>> WOLIIdToSAIdMap = new Map<String, Set<String>>();

        serviceAppointments = [
                SELECT Id,CTS_Work_Order_Line_Item__c
                FROM ServiceAppointment
                WHERE Id IN :serviceAppointmentRecords
        ];

        for (ServiceAppointment serviceAppointmentRec : serviceAppointments) {
            if (!WOLIIdToSAIdMap.containsKey(serviceAppointmentRec.CTS_Work_Order_Line_Item__c))
                WOLIIdToSAIdMap.put(serviceAppointmentRec.CTS_Work_Order_Line_Item__c, new Set<String>());
            WOLIIdToSAIdMap.get(serviceAppointmentRec.CTS_Work_Order_Line_Item__c).add(serviceAppointmentRec.Id);
        }

        return WOLIIdToSAIdMap;
    }

    /*
* @Name	: getSAToProductRequiredMap
* @Description: Fetches the Service Appointment to Product Required Map
* @Date   	: 19-November-2020
* @Author 	: CloudShapers
*/
    private static Map<String, Set<String>> getSAToProductRequiredMap(Map<String, Set<String>> WOLIIdToSAIdMap) {
        List<ProductRequired> productsRequired = new List<ProductRequired>();
        Map<String, Set<String>> SAIdToProductIdMap = new Map<String, Set<String>>();

        productsRequired = [
                SELECT Id,Product2Id,ParentRecordId
                FROM ProductRequired
                WHERE ParentRecordId IN :WOLIIdToSAIdMap.keySet()
        ];

        for (ProductRequired productRequiredRec : productsRequired) {
            for (String serviceAppointmentRecordId : WOLIIdToSAIdMap.get(productRequiredRec.ParentRecordId)) {
                if (!SAIdToProductIdMap.containsKey(serviceAppointmentRecordId))
                    SAIdToProductIdMap.put(serviceAppointmentRecordId, new Set<String>());
                SAIdToProductIdMap.get(serviceAppointmentRecordId).add(productRequiredRec.Product2Id);
            }
        }

        return SAIdToProductIdMap;
    }

    /*
* @Name	: getAssignedResourceToLocationMap
* @Description: Fetches the Assigned Resource to Location Map
* @Date   	: 19-November-2020
* @Author 	: CloudShapers
*/
    private static Map<String, String> getAssignedResourceToLocationMap(List<ServiceAppointment> serviceAppointmentRecords) {
        List<AssignedResource> assignedResources = new List<AssignedResource>();
        Map<String, String> SAToLocationIdMap = new Map<String, String>();

        assignedResources = [
                SELECT Id,ServiceResource.LocationId,ServiceAppointmentId
                FROM AssignedResource
                WHERE ServiceAppointmentId IN :serviceAppointmentRecords
        ];

        for (AssignedResource assignedResourceRec : assignedResources) {
            if (String.isNotBlank(assignedResourceRec.ServiceResource.LocationId)) {
                SAToLocationIdMap.put(assignedResourceRec.ServiceAppointmentId,
                        assignedResourceRec.ServiceResource.LocationId);
            }else{
                throw new NoLocationException(System.Label.Service_Location_Missing_Error);
            }
        }

        return SAToLocationIdMap;
    }

    /*
* @Name	: insertProductItems
* @Description: Inserts the Product Items for the Location
* @Date   	: 19-November-2020
* @Author 	: CloudShapers
*/
    private static void insertProductItems(Map<String, String> SAToLocationIdMap, Map<String, Set<String>> SAIdToProductIdMap) {
        List<ProductItem> productItems = new List<ProductItem>();
        for (String ServiceAppointmentId : SAToLocationIdMap.keySet()) {
            if (SAIdToProductIdMap.containsKey(ServiceAppointmentId)) {
                for (String ProductId : SAIdToProductIdMap.get(ServiceAppointmentId)) {
                    productItems.add(new ProductItem(QuantityOnHand = 10000, Product2Id = ProductId,
                            LocationId = SAToLocationIdMap.get(ServiceAppointmentId)));
                }
            }

            if (!productItems.isEmpty())
                Database.insert(productItems, false);
        }
    }
}