// Handler Class for OpportunityWinPlan_Trigger
//
// Created on Dec 11th,2018 by Harish K.J

public class OptyWinPlan_Trigger_TriggerHandler{
    
    public static void beforeUpdate(List<OpportunityWinPlan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
    }
    
    public static void beforeInsert(List<OpportunityWinPlan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
    }
    
    public static void afterUpdate(List<OpportunityWinPlan__c> newList)
    {
        UpdateOpportunityAttributes(newList);
    }
    
    public static void afterInsert(List<OpportunityWinPlan__c> newList)
    {
        UpdateOpportunityAttributes(newList);
    }
    
    public static void afterDelete(List<OpportunityWinPlan__c> newList)
    {
        UpdateOpportunityAttributes(newList);
    }
    
    //method to update Opportunity Attributes
    public static void UpdateOpportunityAttributes(List<OpportunityWinPlan__c> newList)
    {
        List<Id> OpportunityWinPlanOptyIds = new List<Id> ();
        for (OpportunityWinPlan__c ast : newList)
        {
            OpportunityWinPlanOptyIds.add(ast.Opportunity__c);
        }
        
        List<Opportunity> OpportunityList = new List<Opportunity> ();
        
        OpportunityList = [Select Id, Opportunity_Win_Plan_Count__c from Opportunity where Id in :OpportunityWinPlanOptyIds];
        //System.debug('OpportunityList == ' + OpportunityList + '\n');        
        Map<Id,AggregateResult> results = new Map<Id,AggregateResult>([Select Opportunity__r.Id Id,Count(Id) OptyWinPlanCount from OpportunityWinPlan__c where Opportunity__r.Id != null and Opportunity__r.Id in :OpportunityWinPlanOptyIds Group By Opportunity__r.Id]);
        //System.debug('results == ' + results + '\n');       
        for(Opportunity op:OpportunityList)
        {   
            if (results.get(op.Id) == null)
                op.Opportunity_Win_Plan_Count__c = 0; 
            else
                op.Opportunity_Win_Plan_Count__c = Decimal.ValueOf(String.ValueOf(results.get(op.Id).get('OptyWinPlanCount')));
                       
        }
        
        update(OpportunityList);
    }
}