@isTest
private class ScheduleBatchResetDailyBillsOnOrd_Test {

  static testmethod void testSchedule() {
    // Set a job name
    String jobName = 'testSchedule';
    Test.startTest();
      // Schedule the job
      String jobId = ScheduleBatchResetDailyBillsOnOrders.scheduleMe(jobName);
    Test.stopTest();
    // Verify that a job Id was returned
    System.assertEquals(true, jobId != null);

    // Get the information from the CronTrigger API object
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
    // Verify the expressions are the same
    System.assertEquals(ScheduleBatchResetDailyBillsOnOrders.SCHEDULE_INTERVAL, ct.CronExpression);
  }
}