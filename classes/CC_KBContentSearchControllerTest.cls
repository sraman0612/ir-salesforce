/**
    Test class for CC_KBContentSearchController, CC_KBContentSearchConfig, CC_KBContentSearchHelper, CC_KBContent_Constants, and CC_KBVoteController
*/

@isTest
public class CC_KBContentSearchControllerTest {

    @isTest
    private static void testSearch(){ 
        createTestData(true);
        Test.StartTest();
        List<CC_KBContentSearchController.ObjectWrapper> objectWrappers = CC_KBContentSearchController.search('test', new List<String>{'Knowledge', 'Bulletins'});
        System.assert(objectWrappers.size() == 2);
        System.assert(CC_KBContentSearchController.updateViewStat(objectWrappers[0].sobj.Id));
        Test.StopTest();
    }
    
    @isTest
    private static void testGetSearchConfig(){
        createTestData(false);
        CC_KBContentSearchController.SearchConfigWrapper searchConfig = CC_KBContentSearchController.getSearchConfigDetails();
        CC_kb_content_search_config__c searchCon = CC_kb_content_search_config__c.getInstance();
        CC_KBContentSearchHelper.stringToSet(searchCon.Image_files__c, 'AI,AB');
        System.assert(searchConfig != null);
    }
    
    @isTest 
    private static void testCaseNumber(){
        Case cs = TestUtilityClass.createCustomCase(null,'New','test case');
        String caseNumber = CC_KBContentSearchController.getCaseNumber(cs.id);
        System.assert(caseNumber != null);
    }
    
    private static void createTestData(Boolean isSearch){
        if(isSearch){
            Knowledge__kav knowledge = CC_Test_Utility.createKnowledge('test Article', 'test-Article1','This is a test article', true);
            System.debug('::knowledge'+knowledge.id);
            System.debug('## kb is ' + knowledge);
            Knowledge__kav resultKnowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
            KbManagement.PublishingService.publishArticle(resultKnowledge.KnowledgeArticleId, true);
            List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
            psettingList = TestUtilityClass.createPavilionSettings();
            insert psettingList;
            Id RTId = [SELECT Id FROM RecordType WHERE sObjectType='ContentVersion' AND Name='Tech Pubs'].Id;
            ContentVersion contentVersion = CC_Test_Utility.createContentVersion('testcontent.jpg' , 'Test content 1'  , 'test-content',RTId, true);
            Id [] fixedSearchResults= new Id[2];
            fixedSearchResults[0] = knowledge.id;
            fixedSearchResults[1] = contentVersion.id;
            Test.setFixedSearchResults(fixedSearchResults);
        }
        CC_kb_content_search_config__c searchConfig = CC_Test_Utility.createSearchConfig();
    }
    
    @IsTest(SeeAllData=true)
    private static void testVote(){ 
        Knowledge__kav knowledge = CC_Test_Utility.createKnowledge('test Article', 'test-Article1','This is a test article', true);
        Knowledge__kav resultKnowledge = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :knowledge.Id];
        KbManagement.PublishingService.publishArticle(resultKnowledge.KnowledgeArticleId, true);
        Test.StartTest();
        CC_KBVoteController.followUnfollow(resultKnowledge.KnowledgeArticleId,'follow');
        System.assert(CC_KBVoteController.vote(resultKnowledge.KnowledgeArticleId, 'Up'));
        CC_KBVoteController.getInit(resultKnowledge.KnowledgeArticleId);
        Test.StopTest();
    }
    
    @isTest
    private static void testRecommendedArticles(){
        createTestData(false);
        CC_KBContentSearchController.ObjectWrapper [] owLst = CC_KBContentSearchController.getRecArticles('dingdongthewitchisdead',5);
    }
    
    @isTest
    private static void testgetArticlesOnLoad(){
        createTestData(false);
        CC_KBContentSearchController.ObjectWrapper [] owLst = CC_KBContentSearchController.getArticlesOnLoad('ORDER BY LastModifiedDate DESC');
        CC_KBContentSearchController.ObjectWrapper [] owLst1 = CC_KBContentSearchController.getArticlesOnLoad('ORDER BY ArticleTotalViewCount DESC');
        CC_KBContentSearchController.ObjectWrapper [] owLst2 = CC_KBContentSearchController.getArticlesOnLoad('EntitySubscription');
        CC_KBContentSearchController.ObjectWrapper [] owLst3 = CC_KBContentSearchController.getArticlesOnLoad('mostHelpfulArticles');
    }
}