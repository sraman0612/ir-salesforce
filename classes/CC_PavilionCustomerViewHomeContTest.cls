@isTest
public class CC_PavilionCustomerViewHomeContTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testmethod void UnitTest_CustomerViewHome()
    {
        // Creation Of Test Data
        List<CC_Pavilion_Content__c> pclist = new List<CC_Pavilion_Content__c>();
        Set<String> TrainingEventTypList =new Set<String>();
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c=TestUtilityClass.createContact(acc.id);
        insert c;
        System.assertEquals(c.AccountId,acc.Id);
        System.assertNotEquals(null, c.Id);
     
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(u.ContactId,c.Id);
        System.assertNotEquals(null, u.Id);
        System.runAs(u)
        {
            CC_Pavilion_Content__c pc = TestUtilityClass.createCustomPavilionContentWithEventType('CustomerViewTrainingDates','Every Wednesday');
            CC_Pavilion_Content__c newpc = TestUtilityClass.createCustomPavilionContentWithEventType('CustomerViewTrainingDates','Every Third Thursday');
            pc.CC_Body_1__c = 'Test Body Content 11';
            newpc.CC_Body_1__c = 'Test Body Content 1';
            pclist.add(pc);
            pclist.add(newpc);
            insert pclist;
            TrainingEventTypList.add('Every Wednesday');
            TrainingEventTypList.add('Every Third Thursday');
            Attachment att = TestUtilityClass.createAttachment(pc.Id);
            insert att;
            System.assertEquals(att.ParentId,pc.Id);
            System.assertNotEquals(null, att.Id);
            CC_PavilionTemplateController controller;
            CC_PavilionCustomerViewHomeController custviewhome =new CC_PavilionCustomerViewHomeController(controller);    
            Test.startTest();
            custviewhome.ListEveryWednesday=pclist;
            custviewhome.TrainingEventTypeList=TrainingEventTypList;
            CC_PavilionCustomerViewHomeController.getEventType();
            System.assertNotEquals(null,CC_PavilionCustomerViewHomeController.getEventType());
            CC_PavilionCustomerViewHomeController.getCustomerVIEWBody();
            CC_PavilionCustomerViewHomeController.getCustomerVIEWHeading();
            CC_PavilionCustomerViewHomeController.getEveryWednesdayData('Every Wednesday');
            System.assertNotEquals(null,CC_PavilionCustomerViewHomeController.getEveryWednesdayData('Every Wednesday'));
            custviewhome.redirectToDiffPage();
            custviewhome.getEveryThursdayData();
            System.assertEquals(null,custviewhome.getEveryThursdayData());
            Test.stopTest();
        }
        
    }
    
}