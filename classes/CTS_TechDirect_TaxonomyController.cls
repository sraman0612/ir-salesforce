public with sharing class CTS_TechDirect_TaxonomyController {   
    @AuraEnabled
    public static List< PicklistEntryWrapper > getPicklistValues(String object_name, String field_name, String default_val) {
        List<PicklistEntryWrapper> options = new List<PicklistEntryWrapper>(); //new list for holding all of the picklist options       
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get(object_name); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list       
            if(a.isActive()) {
                if(a.getValue() == default_val) {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), true)); //add the value and label to our final list    
                } else {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), false)); //add the value and label to our final list    
                }    
            }                        
        }
        return options; //return the List
    }
    
    @AuraEnabled
    public static List <Knowledge__kav> getAllArticles(String validationStatus, String searchKeyword, String language, String product, String category) {
        String publishStatus = 'Online';
        if(validationStatus == 'Private' || validationStatus == 'Proposed' || validationStatus == 'Rework' || validationStatus == 'Review') {
            publishStatus = 'Draft';
        } else if(validationStatus == 'Archive') {
            publishStatus = 'Archived';
        } else if(validationStatus == 'Publish') {
            publishStatus = 'Online';
        }
        String query = 'SELECT Id, ArticleNumber, Title, URLName, ValidationStatus, PublishStatus, CTS_TechDirect_Article_Type__c, Language FROM Knowledge__kav WHERE PublishStatus = \'' + publishStatus + '\'';        
        query = query + ' AND IsLatestVersion = true'; 
        
        if(String.isNotBlank(validationStatus)) {
            query = query + ' AND validationStatus = \'' + validationStatus + '\'';        
        }
        if(String.isNotBlank(language)) {
            query = query + ' AND language = \'' + language + '\'';        
        }
        if(String.isNotBlank(searchKeyword)) {
            query = query + ' AND ( title like \'%' + searchKeyword.replaceAll(' ', '%') + '%\'';
            query = query + ' OR  CTS_Techdirect_Keyword__c like \'%' + searchKeyword + '%\'';
            query = query + ' OR UrlName like \'%' + searchKeyword  + '%' +  '\' )';        
        }
        if(String.isNotBlank(product) || String.isNotBlank(category)) {
            query = query + '  WITH DATA CATEGORY';
            Boolean isAndCondition = false;
            if(String.isNotBlank(product)) {                
                query = query + ' IR_Global_Products_Group__c ABOVE_OR_BELOW (' + product + '__c' + ')';
                isAndCondition = true;
            }
            if(String.isNotBlank(category)) {
                if(isAndCondition) {
                    query = query + ' AND';    
                }
                query = query + ' IR_Global_Category_Group__c BELOW (' + category + '__c' + ')';
            }
        }
        System.debug('Query -- ' + query);
        return Database.query(query);
    }
    
    @AuraEnabled
    public static List < DataCategoryWrapper > getDataCategoryStructure(String object_name, String data_category_group_name) {
        List < DataCategoryWrapper > dataCategoryList = new List < DataCategoryWrapper > ();
        DataCategoryWrapper outerWrapObj;
        try {
            //Making the call to the describeDataCategoryGroups to get the list of category groups associated
            List<DescribeDataCategoryGroupResult> describeCategoryResult = new List<DescribeDataCategoryGroupResult> ();            
            for(DescribeDataCategoryGroupResult obj: Schema.describeDataCategoryGroups(new List < String > {object_name})) {
                if(obj.getName() == data_category_group_name) {
                    describeCategoryResult.add(obj);
                }
            }            
            
            //Creating a list of pair objects to use as a parameter for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
            
            //Looping throught the first describe result to create the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }
            
            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : Schema.describeDataCategoryGroupStructures(pairs, false)){                
                for(Schema.DataCategory category: singleResult.getTopCategories()) {
                    outerWrapObj = new DataCategoryWrapper(category.getLabel(), category.getName(), false);  
                    outerWrapObj = prepareChildCategories(outerWrapObj, category.getChildCategories());
                }
                dataCategoryList.add(outerWrapObj);
            }
        } catch (Exception ex){
            System.debug('### Exeception ### ' + ex.getMessage() + ' ### at line# '+ex.getLineNumber());
        }
        return dataCategoryList;
    }
    
    private static DataCategoryWrapper prepareChildCategories(DataCategoryWrapper parentWrapper, DataCategory[] categories) {
        for(DataCategory subCategory: categories) {
            DataCategoryWrapper innerWrapObj = new DataCategoryWrapper(subCategory.getLabel(), subCategory.getName(), false);            
            parentWrapper.items.add(innerWrapObj);             
            if(subCategory.getChildCategories() != null && subCategory.getChildCategories().size() > 0) {
                prepareChildCategories(innerWrapObj, subCategory.getChildCategories());
            }
            
        }                
        return parentWrapper;
    }
    
    public class PicklistEntryWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean selected { get; set; }      
        
        // Default Constructor
        public PicklistEntryWrapper() { }
        
        // Parameterized Constructor
        public PicklistEntryWrapper(String label, String value, Boolean selected) {
            this.label = label;
            this.value = value;
            this.selected = selected;
        }
    }
    
    public class DataCategoryWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean expanded { get; set; }           
        @AuraEnabled
        public List < DataCategoryWrapper > items { get {
            if(items == null) {
                items = new List < DataCategoryWrapper > ();
            }    
            return items;
        } set; } 
        
        // Default Constructor
        public DataCategoryWrapper() { }
        
        // Parameterized Constructor
        public DataCategoryWrapper(String label, String name, Boolean expanded) {
            this.label = label;
            this.name = name;
            this.expanded = expanded;
        }
    }
}