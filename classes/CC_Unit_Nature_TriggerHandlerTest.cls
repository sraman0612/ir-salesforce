@isTest
public with sharing class CC_Unit_Nature_TriggerHandlerTest {
    
    @TestSetup
    private static void createData(){
    	
        Profile ccAdminProf = [Select Id From Profile Where Name = 'CC_System Administrator'];
        
        User ccAdminUser = TestUtilityClass.createNonPortalUser(ccAdminProf.Id);
        ccAdminUser.LastName = 'Test123456789';
        ccAdminUser.userName += 'abc123';

    	User salesRepUser1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser1.Alias = 'alias122'; 
    	
    	User salesRepUser2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser2.Alias = 'alias125';       	       
        
        insert new User[]{ccAdminUser, salesRepUser1, salesRepUser2};   
        
    	CC_Sales_Rep__c salesRep1 = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser1.Id);
    	CC_Sales_Rep__c salesRep2 = TestUtilityClass.createSalesRep('12345_abc124', salesRepUser2.Id);
    	insert new CC_Sales_Rep__c[]{salesRep1, salesRep2};        	
    }
    
    private static testMethod void testNatureSync(){
    	
    	User testUser = [Select Id From User Where LastName = 'Test123456789'];
    	
    	system.runAs(testUser){
    		
    		// Create a few natures first
    		CC_Nature__c nature1 = TestUtilityClass.createNature(true, 'Nature1', 'Description1');
    		CC_Nature__c nature2 = TestUtilityClass.createNature(true, 'Nature2', 'Description2');
    		CC_Nature__c nature3 = TestUtilityClass.createNature(false, 'Nature3', 'Description3');
    		
    		insert new CC_Nature__c[]{nature1, nature2, nature3};
    		
    		CC_Sales_Rep__c[] units = [Select Id From CC_Sales_Rep__c];
    		
    		Test.startTest();
    		
    		// Active combination with no matching nature
    		CC_Unit_Nature__c unitNature1 = TestUtilityClass.createUnitNature(true, 'Nature4', 'Description4', units[0].Id);
    		
			// Inactive combination with no matching nature
    		CC_Unit_Nature__c unitNature2 = TestUtilityClass.createUnitNature(false, 'Nature5', 'Description5', units[0].Id);
    		
			// Active combination with a matching active nature
    		CC_Unit_Nature__c unitNature3 = TestUtilityClass.createUnitNature(true, 'Nature1', 'Description1a', units[1].Id);    
    		
			// Inactive combination with a matching nature
    		CC_Unit_Nature__c unitNature4 = TestUtilityClass.createUnitNature(false, 'Nature2', 'Description2a', units[1].Id);   
    		
    		// Active combination with a matching inactive nature
    		CC_Unit_Nature__c unitNature5 = TestUtilityClass.createUnitNature(true, 'Nature3', 'Description3a', units[1].Id);   
    		
    		// Active combination with a matching inactive nature, same nature code as #5 with different unit
    		CC_Unit_Nature__c unitNature6 = TestUtilityClass.createUnitNature(true, 'Nature3', 'Description3a', units[0].Id);      		 		 				    		
    		
    		system.debug('--round1 test');
    		
    		insert new CC_Unit_Nature__c[]{unitNature1, unitNature2, unitNature3, unitNature4, unitNature5, unitNature6};
    		
    		// Verify the updates to CC_Nature__c
    		CC_Nature__c[] natures = [Select Id, Active__c, Nature_Code__c, Nature_Description__c From CC_Nature__c Order by Nature_Code__c];
    		
    		system.assertEquals(4, natures.size());   
    		
    		system.assertEquals(true, natures[0].Active__c);
    		system.assertEquals('Nature1', natures[0].Nature_Code__c);
    		system.assertEquals('Description1a', natures[0].Nature_Description__c);
    		
    		system.assertEquals(true, natures[1].Active__c);
    		system.assertEquals('Nature2', natures[1].Nature_Code__c);
    		system.assertEquals('Description2', natures[1].Nature_Description__c);
    		
    		system.assertEquals(true, natures[2].Active__c);
    		system.assertEquals('Nature3', natures[2].Nature_Code__c);
    		system.assertEquals('Description3a', natures[2].Nature_Description__c);
    		
    		system.assertEquals(true, natures[3].Active__c);
    		system.assertEquals('Nature4', natures[3].Nature_Code__c);
    		system.assertEquals('Description4', natures[3].Nature_Description__c);
    		
    		// Change the nature code rendering the old code as obselete
    		unitNature4.Nature__c = 'Nature2a';    		
    		
    		// Deactivate 1 of the 2 combinations for Nature3
    		unitNature6.isActive__c = false;
    		
    		system.debug('--round2 test');
    		
    		update new CC_Unit_Nature__c[]{unitNature4, unitNature6};	
    		
    		// Verify the updates to CC_Nature__c
    		natures = [Select Id, Active__c, Nature_Code__c, Nature_Description__c From CC_Nature__c Order by Nature_Code__c];
    		
    		system.assertEquals(5, natures.size());   
    		
    		system.assertEquals(true, natures[0].Active__c);
    		system.assertEquals('Nature1', natures[0].Nature_Code__c);
    		system.assertEquals('Description1a', natures[0].Nature_Description__c);
    		
    		system.assertEquals(false, natures[1].Active__c);
    		system.assertEquals('Nature2', natures[1].Nature_Code__c);
    		system.assertEquals('Description2', natures[1].Nature_Description__c);
    		
    		system.assertEquals(true, natures[2].Active__c);
    		system.assertEquals('Nature2a', natures[2].Nature_Code__c);
    		system.assertEquals('Description2a', natures[2].Nature_Description__c);
    		
    		system.assertEquals(true, natures[3].Active__c);
    		system.assertEquals('Nature3', natures[3].Nature_Code__c);
    		system.assertEquals('Description3a', natures[3].Nature_Description__c);
    		
    		system.assertEquals(true, natures[4].Active__c);
    		system.assertEquals('Nature4', natures[4].Nature_Code__c);
    		system.assertEquals('Description4', natures[4].Nature_Description__c);      		    			
    		
    		// Deletions
    		system.debug('--round3 test');
    		
    		delete new CC_Unit_Nature__c[]{unitNature5, unitNature6}; // No remaining combinations for Nature3
    		
    		// Verify the updates to CC_Nature__c
    		natures = [Select Id, Active__c, Nature_Code__c, Nature_Description__c From CC_Nature__c Order by Nature_Code__c];
    		
    		system.assertEquals(5, natures.size());   
    		
    		system.assertEquals(true, natures[0].Active__c);
    		system.assertEquals('Nature1', natures[0].Nature_Code__c);
    		system.assertEquals('Description1a', natures[0].Nature_Description__c);
    		
    		system.assertEquals(false, natures[1].Active__c);
    		system.assertEquals('Nature2', natures[1].Nature_Code__c);
    		system.assertEquals('Description2', natures[1].Nature_Description__c);
    		
    		system.assertEquals(true, natures[2].Active__c);
    		system.assertEquals('Nature2a', natures[2].Nature_Code__c);
    		system.assertEquals('Description2a', natures[2].Nature_Description__c);
    		
    		system.assertEquals(false, natures[3].Active__c);
    		system.assertEquals('Nature3', natures[3].Nature_Code__c);
    		system.assertEquals('Description3a', natures[3].Nature_Description__c);
    		
    		system.assertEquals(true, natures[4].Active__c);
    		system.assertEquals('Nature4', natures[4].Nature_Code__c);
    		system.assertEquals('Description4', natures[4].Nature_Description__c);    		
    		
    		Test.stopTest();
    	}    	
    }
}