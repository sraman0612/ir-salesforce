@isTest
private class CTSCompTechEmailServiceTest {
    @isTest
    static void validateCTSCompTechEmailService() {

        // Create Contact record
        Contact cont = new Contact();
        cont.FirstName = 'John';
        cont.LastName = 'Contact';
        cont.Phone =  '3143013675';
        cont.Title = 'Any Title';
        cont.Email = 'mgcantoni.es@gmail.com';
        insert cont;

        // Email
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.fromAddress = 'mgcantoni.es@gmail.com';
        email.subject = 'Compressor 4 Pre-sale Missed Contact';
        String [] htmlText = new List<String>();
        htmlText.add('<P>You have a new voice message from a caller from 3143013675</P>');
        htmlText.add('');
        htmlText.add('<P>Contact ID: 167814331699</P');
        htmlText.add('<P>Date/Time: 10/19/2021 4:30:04 PM</P><P>Skill: 846941 - </P>');
        htmlText.add('<P>Number Dialed: 8338718075</P>');
        htmlText.add('<P>CallerID: 3143013675</P>');
        email.htmlBody = string.join(htmlText,'\n');

        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        String blobValue = 'This is a test of the Emergency Broadcast System';
        attachment.body = blob.valueOf(blobValue.repeat(60000));
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        CTSCompTechEmailService testCTSCompTechEmailService = new CTSCompTechEmailService();
        testCTSCompTechEmailService.handleInboundEmail(email, env);
        
        //Added:for case data
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId();
		/*Division__c divn = new Division__c();
        divn.Name = 'Test Division';
        divn.Division_Type__c = 'Customer Center';
        divn.EBS_System__c = 'Oracle 11i';     
        insert divn; */
        Account a = new Account();
        a.Name = 'Test Acct for Oracle Quote Item Deletion';
        a.BillingCity = 'srs_!city';
        a.BillingCountry = 'USA';
        a.BillingPostalCode = '674564569';
        a.BillingState = 'CA';
        a.BillingStreet = '12, street1678';
        a.Siebel_ID__c = '123456';
        a.ShippingCity = 'city1';
        a.ShippingCountry = 'USA';
        a.ShippingState = 'CA';
        a.ShippingStreet = '13, street2';
        a.CTS_Global_Shipping_Address_1__c = '13';
        a.CTS_Global_Shipping_Address_2__c = 'street2';
        a.ShippingPostalCode = '123';  
        a.County__c = 'testCounty';
        a.RecordTypeId = AirNAAcctRT_ID;
        //a.Account_Division__c = divn.Id;
        insert a;
        
        contact con = new contact(FirstName = 'test', LastName = 'Test', Title ='Mr', AccountId = a.Id);
        //con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IR Comp MEIA Contact').getRecordTypeId();
        insert con;
        
        Case newCase = new Case();
        //newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CTS TechDirect Ask a Question').getRecordTypeId();
        newCase.ContactId = con.Id;
        newCase.AccountId = a.Id;
        newCase.Subject = 'Test subject';
        newCase.Status = 'new';
        insert newCase;
        List<Case> createdCases = [Select Id from Case where subject='Test subject'];

        System.assert(!createdCases.isEmpty(),'List should not be empty');

        System.assert(createdCases.size() == 1,'1 case record should be returned');

    }
}