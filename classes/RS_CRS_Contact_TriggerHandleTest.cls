/******************************************************************************
Name:       RS_CRS_Contact_TriggerHandleTest
Purpose:    Test class for RS_CRS_Contact_TriggerHandler. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Neela Prasad    02/02/2018  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_Contact_TriggerHandleTest {
    /******************************************************************************
  Method Name : CheckContactOnCase_Test
  Arguments   : No Arguments
  Purpose     : Method for Testing CheckContactOnCase_Test Method
  ******************************************************************************/
    Static testmethod void CheckContactOnCase_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Business_Account_Record_Type);
        Id conRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Contact', Label.RS_CRS_Contact_Record_Type);
        
        System.runAs(userRec){
            //insert test case
        Case caseObj = new Case();
        caseObj.RecordTypeId = caseRecId;
        caseObj.Status = 'Customer Action';
        insert caseObj;
            //insert test Account
        Account acc = new Account();
        acc.RecordTypeId = accRecId;
        acc.Name = 'test Account1';
        acc.BillingState = 'NC';
        acc.BillingCountry = 'USA';
        insert acc;
            //Insert Contact
        Contact con = New Contact();
        con.RecordTypeId = conRecId;
        con.FirstName = 'test First1';
        con.LastName =  'test Last1';
        con.RS_CRS_Account_Name__c = acc.Id;
        con.RS_CRS_Account_Name__c = acc.Id;
        con.RS_CRS_Case__c = caseObj.Id;
        con.Email = 'test.user@irco.com';
        insert con;
            test.startTest();
            try{
            delete con;
            //catching Error messages and ccomparing
            }Catch (Exception ex){
            System.assertEquals(ex.getMessage().contains('Cannot delete Contact with related Cases.'), TRUE);
            test.stopTest();
          }
        }
    } 
    Static testmethod void CaseOnContactRelatedList_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Business_Account_Record_Type);
        Id conRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Contact', Label.RS_CRS_Contact_Record_Type);
        
        System.runAs(userRec){
            //insert test Account
        Account acc = new Account();
        acc.RecordTypeId = accRecId;
        acc.Name = 'test Account2';
        acc.BillingState = 'CA';
        acc.BillingCountry = 'USA';
        insert acc;
            //Insert Contact
        Contact con1 = New Contact();
        con1.RecordTypeId = conRecId;
        con1.FirstName = 'test First1';
        con1.LastName =  'test Last1';
        con1.RS_CRS_Account_Name__c = acc.Id;
        con1.Email = 'test.user@irco.com';
        insert con1;
        //insert test case
        Case caseObj1 = new Case();
        caseObj1.RecordTypeId = caseRecId;
        caseObj1.Status = 'Customer Action1';
        caseObj1.AccountId =  acc.id;
        caseObj1.RS_CRS_Case_Contact_Name__c = con1.Id;
        insert caseObj1;
        upsert con1;
            test.startTest();
            try{
            delete con1;
            //catching Error messages and ccomparing
            }Catch (Exception ex){
            System.assertEquals(ex.getMessage().contains('Cannot delete Contact with related Cases.'), TRUE);
            test.stopTest();
          }
        }
    }
}