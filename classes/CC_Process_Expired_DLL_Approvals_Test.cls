@isTest
public with sharing class CC_Process_Expired_DLL_Approvals_Test {
    
    @testSetup 
    private static void setupData() {
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;
        
    	// Create a few orders to work with
    	Id ccOrdRecordTypeId = sObjectService.getRecordTypeId(CC_Order__c.sObjectType, 'Cars Ordering');
    	Account a = [Select Id From Account LIMIT 1];
    	
    	CC_Order__c ord1 = TestUtilityClass.createOrder(a.id);
    	ord1.CC_Order_Number_Reference__c = 'Order1';
    	ord1.CC_MAPICS_Order_Key__c = 'abc1';
        ord1.RecordTypeId = ccOrdRecordTypeId;
        ord1.DLL_Approval_Expiration_Date__c = Date.today().addDays(-1); // Expired
        
    	CC_Order__c ord2 = TestUtilityClass.createOrder(a.id);
    	ord2.CC_Order_Number_Reference__c = 'Order2';
    	ord2.CC_MAPICS_Order_Key__c = 'abc2';
        ord2.RecordTypeId = ccOrdRecordTypeId;
        ord2.DLL_Approval_Expiration_Date__c = Date.today(); // Expiring today       
        
    	CC_Order__c ord3 = TestUtilityClass.createOrder(a.id);
    	ord3.CC_Order_Number_Reference__c = 'Order3';
    	ord3.CC_MAPICS_Order_Key__c = 'abc3';
        ord3.RecordTypeId = ccOrdRecordTypeId;
        ord3.DLL_Approval_Expiration_Date__c = Date.today().addDays(1); // Not expired
        
    	CC_Order__c ord4 = TestUtilityClass.createOrder(a.id);
    	ord4.CC_Order_Number_Reference__c = 'Order4';
    	ord4.CC_MAPICS_Order_Key__c = 'abc4';
        ord4.RecordTypeId = ccOrdRecordTypeId;
        ord4.DLL_Approval_Expiration_Date__c = Date.today().addDays(-1); // Expired        
        
        insert new CC_Order__c[]{ord1, ord2, ord3, ord4};  
        
        // Insert car shipments
        CC_Order_Shipment__c shipment1 = TestUtilityClass.createOrderShipment(ord1.Id); 
        shipment1.Ship_Date__c = Date.today().addDays(1); // Not shipped yet
        
        CC_Order_Shipment__c shipment2 = TestUtilityClass.createOrderShipment(ord4.Id); 
        shipment2.Ship_Date__c = Date.today().addDays(-1); // Already shipped 
        
        insert new CC_Order_Shipment__c[]{shipment1, shipment2};       
    } 
    
    @isTest
    private static void testSchedule(){
    	
    	Id jobId;
    	String schedule = '0 0 0 1 1 ? 2045';
    	
    	Test.startTest();
    	
    	jobId = System.schedule('Test_CC_Process_Expired_DLL_Approvals', schedule, new CC_Process_Expired_DLL_Approvals());
    	
    	Test.stopTest();
    	
    	// Verify the job is scheduled
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :jobID];
    	system.assertEquals(schedule, ct.CronExpression);
    	system.assertEquals(0, ct.TimesTriggered);
    	system.assertEquals('Test_CC_Process_Expired_DLL_Approvals', ct.CronJobDetail.Name);
    	system.assertEquals('7', ct.CronJobDetail.JobType);
    	
    	// Verify the batch job was executed
    	system.assertEquals(1, [Select Count() From AsyncApexJob Where ApexClass.Name = 'CC_Process_Expired_DLL_Approvals' and JobType = 'BatchApex']);
    }   
    
    @isTest
    private static void testBatch(){
    	
    	CC_Order__c[] orders = [Select Id, Order_Number__c, CC_Order_Number_Reference__c From CC_Order__c Order By CC_Order_Number_Reference__c ASC];
    	system.assertEquals(4, orders.size());
    	
    	Test.startTest();
    	
    	Database.executeBatch(new CC_Process_Expired_DLL_Approvals());
    	
    	Test.stopTest();
    	
    	// Verify that the first order was placed in the ERP message queue, the others should not qualify
    	CC_ERPMQ__c[] messageQueue = [Select Key__c, Type__c, CC_Order__c From CC_ERPMQ__c];
    	
    	system.assertEquals(1, messageQueue.size());
    	system.assertEquals(orders[0].Id, messageQueue[0].CC_Order__c);
    	system.assertEquals(orders[0].Order_Number__c, messageQueue[0].Key__c);
    	system.assertEquals('ORDER_HOLD', messageQueue[0].Type__c);
    }    
}