@isTest
public class apexLogHandlerTest {
	 static testmethod void UnitTest_apexLogHandler()
    {
        /*//Commented as part of EMEIA Cleanup
        // Creation Of Test Data
        apexLogHandler.For_Testing_Force_Exception e = new apexLogHandler.For_Testing_Force_Exception();
        e.setMessage('This is an exception!');
        Account a = TestUtilityClass.createAccount();
        //Commented as part of EMEIA Cleanup
        //a.CC_Customer_Number__c ='9999920';
        a.Name='CLUB CAR DEPT EXP US';
        insert a;
       //Commented as part of EMEIA Cleanup
        CC_Order__c ord = TestUtilityClass.createOrder(a.id);
        insert ord;
        Pricebook2 pb = TestUtilityClass.createPriceBook2();
        insert pb;  
        insert new CCProcessBuilderIDs__c(setupownerid=UserInfo.getOrganizationId(), FS_No_Charge_Acct_Id__c=a.Id, FS_No_Charge_Pricebook_ID__c=pb.Id);
        WorkOrder wo = TestUtilityClass.createWorkOrder(a.Id,'CC_Service_Contract');
        wo.Work_Order_Status__c = 'WORK COMPLETED';
        wo.Subject = 'TESTING';
        wo.SLAM_Report__c = 'Completed';
        insert wo;
        test.startTest();
        apexLogHandler ApexLogHandlr = new apexLogHandler('CC_PavilionTechPubsCnt','getSNPrefix()','test');
        ApexLogHandlr.logRequestInfo('Sample request Info');
        ApexLogHandlr.logResponseInfo('Sample response Info');
        ApexLogHandlr.logStatusCode(1);
        ApexLogHandlr.saveLog();
        ApexLogHandlr.logMessage('sample');
        ApexLogHandlr.logException(e);        
        ApexLogHandlr.addOrder(ord.Id);
        ApexLogHandlr.addWorkOrder(wo.Id);
        test.stopTest();*/
        apexLogHandler.For_Testing_Force_Exception e = new apexLogHandler.For_Testing_Force_Exception();
        e.setMessage('This is an exception!');
         test.startTest();
        apexLogHandler ApexLogHandlr = new apexLogHandler('CC_PavilionTechPubsCnt','getSNPrefix()','test');
        ApexLogHandlr.logRequestInfo('Sample request Info');
        ApexLogHandlr.logResponseInfo('Sample response Info');
        ApexLogHandlr.logStatusCode(1);
        ApexLogHandlr.saveLog();
        ApexLogHandlr.logMessage('sample');
        ApexLogHandlr.logException(e);        
        //ApexLogHandlr.addOrder(ord.Id);
        //ApexLogHandlr.addWorkOrder(wo.Id);
        test.stopTest();

    }   
}