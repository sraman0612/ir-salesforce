global class ShareContentWithCommunityUsers_Batch implements Database.Batchable<sObject>{
    
    global String query = 'SELECT Id, Title FROM ContentDocument WHERE CreatedDate > 2018-04-01T00:00:00.000Z';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<ContentDocument> contentDocList){
        List<Id> ctsCaseRecordTypeIdList = new List<Id>();
        ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Ask_a_Question').getRecordTypeId());
        ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Issue_Escalation').getRecordTypeId());
        ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Start_Up').getRecordTypeId());
        
        //get content document ids
        List<Id> cdIdList = new List<Id>();
        for(ContentDocument cd: contentDocList){
            cdIdList.add(cd.Id);
        }
        
        //get content document links to set visibilty
        List<ContentDocumentLink> cdlList = [Select Id, LinkedEntityId, ContentDocumentId 
                                             From ContentDocumentLink 
                                             Where ContentDocumentId IN: cdIdList];
        
        List<Id> caseIdList = new List<Id>();
        
        //check if parent is case record
        Schema.DescribeSObjectResult r = Case.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();        
        for(ContentDocumentLink cdl: cdlList){
            if((String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix)){
                caseIdList.add(cdl.LinkedEntityId);
                
            }         
        }
        
        Map<Id, case> ctsCaseMap = new Map<Id, Case>([Select Id, RecordTypeId  From Case  Where Id IN: caseIdList AND RecordTypeId IN: ctsCaseRecordTypeIdList]);
        
        Set<Id> caseIdsList = ctsCaseMap.keySet();
        
        //get Content Documents attached to CTS cases
        List<ContentDocumentLink> ctsCDLList = [Select Id, LinkedEntityId, ShareType, Visibility   
                                                From ContentDocumentLink 
                                                Where LinkedEntityId IN: caseIdsList];
        
        System.debug('##ctsCDLList size::-'+ctsCDLList);
        
        //update visibility of CTS attachments
        for(ContentDocumentLink ctsCDL: ctsCDLList){
            ctsCDL.ShareType = 'I';
            ctsCDL.Visibility = 'AllUsers';            
        }
        update ctsCDLList;
    }
    
    global void finish(Database.BatchableContext BC){ 
        
    }
}