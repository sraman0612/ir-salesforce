public with sharing class CC_Short_Term_Lease_Trigger_Handler {
	
	@TestVisible private static String leaseDeletionErrorMsg = 'This lease cannot be deleted because it has either been submitted for approval or synced to MAPICS already.';
	@TestVisible private static String leaseApprovalOverrideErrorMsg = 'Lease approval override is restricted for finance.';
	@TestVisible private static Boolean leaseOverridePermmissionOverride = false;
    
    public static void onBeforeInsert(CC_Short_Term_Lease__c[] leases){
    	assignSalesHiearchy(leases, null);
    	lookupMyTeamUsers(leases);
    }
    
    public static void onBeforeUpdate(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){
    	assignSalesHiearchy(leases, oldMap);
    	validateApprovalOverride(leases, oldMap); 
        validateMapicsSync(leases, oldMap);       
    }
    public static void onAfterUpdate(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){
        processCancelledLeases(leases, oldMap);
        updateCases(leases, oldMap);
    }
    
    public static void onBeforeDelete(CC_Short_Term_Lease__c[] oldLeases){
		preventDeletions(oldLeases);
    }    

    private static void validateMapicsSync(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){        	
        	        
        system.debug('--validateMapicsSync');	        

        // Skip the validation rule if the user has the bypass setting
        IR_API_Bypass__c bypassSettings = IR_API_Bypass__c.getInstance();
        
        if (bypassSettings != null && bypassSettings.CC_STL_Integration_Bypass__c){
            return;
        }
        
        Schema.FieldSet allLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_All_Leases;
        Schema.FieldSet revenueLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_Revenue_Leases;

        // Go through the updated leases and look for ones that are being synced with MAPICS
        for (CC_Short_Term_Lease__c lease : leases){

            CC_Short_Term_Lease__c old = oldMap != null ? oldMap.get(lease.Id) : null;

            if (old != null && lease.Synced_to_MAPICS__c && !old.Synced_to_MAPICS__c){

                system.debug('lease is being synced with MAPICS: ' + lease);

                Set<String> missingRequiredFields = new Set<String>();

                // Check to see if any of the required fields in the field set are not populated
                for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){

                    String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));

                    if (String.isBlank(fieldStringVal) || fieldStringVal == '0.00'){
                        missingRequiredFields.add(fsm.getLabel());
                    }
                }

                // Check for required fields on revenue leases that
                if (lease.Lease_Category_Code__c == 'R'){
                    
                    for (FieldSetMember fsm : revenueLeasesRequiredFieldSet.getFields()){
                        
                        String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));

                        if (String.isBlank(fieldStringVal) || fieldStringVal == '0.00'){
                            missingRequiredFields.add(fsm.getLabel());
                        }
                    }

                    if (lease.Total_Lease_Price__c == 0 && lease.Total_Transportation_Quoted_Rate__c == 0){
                        missingRequiredFields.add('Total Transportation Quoted Rate');
                    }

                    if (lease.Total_Car_Cost__c == 0 && lease.Total_Transportation_Quoted_Rate__c == 0){
                        missingRequiredFields.add('Total Car Price');
                    }
                }

                system.debug('missingRequiredFields: ' + missingRequiredFields);

                if (missingRequiredFields.size() > 0){
                    lease.addError('\nThe following fields are required to sync this lease to MAPICS: \n' + String.valueOf(missingRequiredFields));
                }
            }
        }
    }    

    private static void updateCases(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){
        
        system.debug('--updateCases');

        Map<Id, CC_Short_Term_Lease__c> updatedSerialNumberLeases = new Map<Id, CC_Short_Term_Lease__c>();
    
        for (CC_Short_Term_Lease__c lease : leases){
            
            CC_Short_Term_Lease__c old = oldMap != null ? oldMap.get(lease.Id) : null;

            if (old != null && lease.Serial_Numbers__c != old.Serial_Numbers__c){
                updatedSerialNumberLeases.put(lease.Id, lease);
            }
        } 

        system.debug('updatedSerialNumberLeases: ' + updatedSerialNumberLeases);

        // Update transportation request cases with the updated serial numbers on the leases
        if (updatedSerialNumberLeases.size() > 0){

            Case[] casesToUpdate = new Case[]{};

            for (Case c : [Select Id, CC_Short_Term_Lease__c From Case Where RecordType.DeveloperName = 'Club_Car_Transportation_Request' and CC_Short_Term_Lease__c in :updatedSerialNumberLeases.keySet()]){

                CC_Short_Term_Lease__c lease = updatedSerialNumberLeases.get(c.CC_Short_Term_Lease__c);

                if (lease != null){

                    c.CC_STL_Serial_Numbers__c = lease.Serial_Numbers__c;
                    casesToUpdate.add(c);
                }                
            } 

            system.debug('casesToUpdate: ' + casesToUpdate);

            if (casesToUpdate.size() > 0){
                sObjectService.updateRecordsAndLogErrors(casesToUpdate, 'CC_Short_Term_Lease_Trigger_Handler', 'updateCases');
            }            
        }
    }    
    
    // Only allow lease deletions under certain conditions
    private static void preventDeletions(CC_Short_Term_Lease__c[] leases){
    	
    	/* 
			Do not allow deletion if any of the following applies:
			
			1) Lease is under approval review
			2) There is a mapics order linked
			3) There are cars on the lease
		*/
		
		for (CC_Short_Term_Lease__c lease : leases){
			
			if ((lease.Approval_Status__c != null && lease.Approval_Status__c != 'Not Yet Submitted') || lease.New_Car_Order__c != null || lease.Cars__c != null){
				lease.addError(leaseDeletionErrorMsg);
			}
		}
    }

    private static void processCancelledLeases(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){
        
        system.debug('--processCancelledLeases');

        CC_Short_Term_Lease__c[] cancelledLeases = new CC_Short_Term_Lease__c[]{};
    
        for (CC_Short_Term_Lease__c lease : leases){
            
            CC_Short_Term_Lease__c old = oldMap != null ? oldMap.get(lease.Id) : null;

            if (old != null && lease.Vehicle_Status__c != old.Vehicle_Status__c && lease.Vehicle_Status__c == 'Cancelled'){
                cancelledLeases.add(lease);
            }
        }      

        system.debug('cancelledLeases: ' + cancelledLeases);  

        if (cancelledLeases.size() > 0){

            // Check to see if any of the cancelled leases are in approval
            Approval.ProcessWorkitemRequest[] leaseApprovalsToRecall = new Approval.ProcessWorkitemRequest[]{};

            for (ProcessInstanceWorkitem piw : [Select Id, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinitionId From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :cancelledLeases]){               
                
                Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                pwr.setAction('Removed');
                pwr.setWorkItemId(piw.id);
                pwr.setComments('Cancelled lease.'); 
                leaseApprovalsToRecall.add(pwr);        
            }

            system.debug('leaseApprovalsToRecall: ' + leaseApprovalsToRecall);

            if (leaseApprovalsToRecall.size() > 0){

                // Recall approval requests for the cancelled leases
                Approval.ProcessResult[] results = sObjectService.processApprovalRequestsAndLogErrors(leaseApprovalsToRecall, 'CC_Short_Term_Lease_Trigger_Handler', 'processCancelledLeases');
                system.debug('recall results: ' + results);
            }
        }
    }
    
    // Do not allow the lease team to override finance approval
    private static void validateApprovalOverride(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){        	
        	        
        system.debug('--validateApprovalOverride');	        
        	        
        CC_Short_Term_Lease_Settings__c leaseSettings = CC_Short_Term_Lease_Settings__c.getOrgDefaults();
        system.debug('restricted roles: ' + leaseSettings.Restricted_Finance_Approval_Roles__c);
        
        Id userRoleId = UserInfo.getUserRoleId();
        system.debug('userRoleId: ' + userRoleId);
                
        if (String.isNotBlank(leaseSettings.Restricted_Finance_Approval_Roles__c) && userRoleId != null){
    
	    	CC_Short_Term_Lease__c[] leasesToValidate = new CC_Short_Term_Lease__c[]{};
	    
	    	for (CC_Short_Term_Lease__c lease : leases){
	    		
	    		CC_Short_Term_Lease__c old = oldMap != null ? oldMap.get(lease.Id) : null;
	    		
	    		if ((lease.Approval_Status__c == 'Approved' || lease.Approval_Status__c == 'Rejected') && (old == null || lease.Approval_Status__c != old.Approval_Status__c)){
	    			leasesToValidate.add(lease);
	    		}
	    	}
		    	
	    	system.debug('leasesToValidate: ' + leasesToValidate.size());
	    	
	    	if (leasesToValidate.size() > 0){
	    		
	    		// Check the logged in user and if they are assigned to the STL lease permission set with modify all permission on the lease object
	    		Boolean leaseOverridePermmission = [Select Count() From PermissionSetAssignment Where AssigneeId = :UserInfo.getUserId() and PermissionSet.Name = 'CC_Short_Term_Leases'] > 0;
	    		
	    		if (Test.isRunningTest() && leaseOverridePermmissionOverride){
	    			leaseOverridePermmission = false;
	    		}
	    		
	    		system.debug('leaseOverridePermmission? ' + leaseOverridePermmission);
	    		
	    		if (leaseOverridePermmission){
		    			
	    			// Get the current approval step
	    			Map<Id, ProcessInstanceWorkitem> pendingLeaseApprovalMap = new Map<Id, ProcessInstanceWorkitem>();
	    			Set<Id> approverIds = new Set<Id>();
	    			
	    			for (ProcessInstanceWorkitem workItem : [Select ProcessInstance.TargetObjectId, ActorId, OriginalActorId, ProcessInstanceId From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leasesToValidate]){
	    				pendingLeaseApprovalMap.put(workItem.ProcessInstance.TargetObjectId, workItem);
	    				approverIds.add(workItem.OriginalActorId);
	    			}
		    			
	    			system.debug('pendingLeaseApprovalMap: ' + pendingLeaseApprovalMap);
	    			system.debug('approverIds: ' + approverIds);
	    			
	    			// Get the roles of the assigned approvers
	    			Map<Id, String> approverRoleNames = new Map<Id, String>();  
	    			
					for (User u : [Select Id, UserRole.DeveloperName From User Where Id in :approverIds]){
						approverRoleNames.put(u.Id, u.UserRole.DeveloperName);
					}	
						
					system.debug('approverRoleNames: ' + approverRoleNames);

	    			// If the original approver is not one of the sales users on the lease, throw an error
	    			for (CC_Short_Term_Lease__c lease : leasesToValidate){
		    				
	    				ProcessInstanceWorkitem pendingApproval = pendingLeaseApprovalMap.get(lease.Id);
	    				String approverRole = pendingApproval != null ? approverRoleNames.get(pendingApproval.OriginalActorId) : null;
	    				
	    				system.debug('pendingApproval: ' + pendingApproval);
	    				system.debug('approverRole: ' + approverRole);
		    				
	    				if (pendingApproval != null && pendingApproval.OriginalActorId != UserInfo.getUserId() && String.isNotBlank(approverRole) && leaseSettings.Restricted_Finance_Approval_Roles__c.contains(approverRole)){    							    				    				
							lease.Approval_Status__c.addError(leaseApprovalOverrideErrorMsg);
	    				}
	    			}
    			}	    		
    		}
    	}
    }
    
    // If the sales rep is changing, update the sales related hierarchy fields
    private static void assignSalesHiearchy(CC_Short_Term_Lease__c[] leases, Map<Id, CC_Short_Term_Lease__c> oldMap){
    	
    	system.debug('--assignSalesHiearchy');
    	
    	Set<Id> salesRepIds = new Set<Id>();
    	CC_Short_Term_Lease__c[] leasesToProcess = new CC_Short_Term_Lease__c[]{};
    	
    	for (CC_Short_Term_Lease__c lease : leases){
    		
    		CC_Short_Term_Lease__c old = oldMap != null ? oldMap.get(lease.Id) : null;
    		
    		if ( (old == null && lease.Sales_Rep__c != null) || (old != null && lease.Sales_Rep__c != old.Sales_Rep__c)){
    			
    			leasesToProcess.add(lease);
    			
    			if (lease.Sales_Rep__c != null){
    				salesRepIds.add(lease.Sales_Rep__c);
    			}
    		}
    	}    	    	
    	
    	system.debug('leasesToProcess: ' + leasesToProcess.size());
    	
    	if (leasesToProcess.size() > 0){
    	
    		Map<Id, CC_Sales_Rep_Service.SalesHierarchy> salesHierarchyMap = CC_Sales_Rep_Service.lookupSalesHiearchy(salesRepIds);
    		
    		for (CC_Short_Term_Lease__c lease : leasesToProcess){
    			
    			system.debug('processing lease: ' + lease);
    			
    			if (lease.Sales_Rep__c == null){
    				
    				system.debug('no sales rep assigned, clearing related sales rep fields');
    				
    				lease.Sales_Rep_Manager__c = null;
    				lease.Sales_Rep_Director__c = null;
    				lease.Sales_Rep_VP__c = null;
    			}
    			else{
    				
    				CC_Sales_Rep_Service.SalesHierarchy salesHierarchy = salesHierarchyMap.get(lease.Sales_Rep__c);
    				
    				system.debug('salesHierarchy found: ' + salesHierarchy);
    				
     				lease.Sales_Rep_Manager__c = salesHierarchy.salesManager != null ? salesHierarchy.salesManager.Id : null;
    				lease.Sales_Rep_Director__c = salesHierarchy.salesDirector != null ? salesHierarchy.salesDirector.Id : null;
    				lease.Sales_Rep_VP__c = salesHierarchy.salesVP != null ? salesHierarchy.salesVP.Id : null;   				
    			}
    		}
    	}
    }
    
    // Process new short Term leases and try to find users in specific roles, i.e. 'CAS' under the account's My Team records
    private static void lookupMyTeamUsers(CC_Short_Term_Lease__c[] newList){
        
        try{
        
	        // Get a set of account Ids to work with
	        Set<Id> accountIds = new Set<Id>();
			CC_Short_Term_Lease__c[] stlsToProcess = new CC_Short_Term_Lease__c[]{};
			
			for (CC_Short_Term_Lease__c stl : newList){
				
				if (stl.CustomerId__c != null){
				
					Id cid = Id.valueof(stl.CustomerId__c);
					
					stlsToProcess.add(stl);
		            accountIds.add(cid);
				}
	        }
	        
	        system.debug('stlsToProcess: ' + stlsToProcess.size());
	        
	        if (stlsToProcess.size() > 0){           
	                	            
	            // Find the assigned finance rep on each account
				Map<Id, Map<String, Id>> accountUserRoleMap = 
                CC_My_Team_Service.lookupUsersByAccountAndRole(new Set<String>{CC_My_Team_Service.customerAcctSpecRole, CC_My_Team_Service.financeRepRole}, accountIds);
				CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
					                                
	            // Go through each order and assign the user if found
	            for (CC_Short_Term_Lease__c stl : newList){
	            	
	                Id cid = Id.valueof(stl.CustomerId__c);
                    system.debug('stl: ' + stl);
                    
	                Map<String, Id> roleMap = accountUserRoleMap.get(cid);
	                Id customerAcctSpecId = roleMap != null && roleMap.containsKey(CC_My_Team_Service.customerAcctSpecRole) ? roleMap.get(CC_My_Team_Service.customerAcctSpecRole) : null;
	                Id financeRepId = roleMap != null && roleMap.containsKey(CC_My_Team_Service.financeRepRole) ? roleMap.get(CC_My_Team_Service.financeRepRole) : null;
	                
	                if (financeRepId == null && contractSettings != null && String.isNotBlank(contractSettings.CC_Default_Finance_Approver_ID__c)){
	                	financeRepId = contractSettings.CC_Default_Finance_Approver_ID__c;
	                }
	                
	                system.debug('customerAcctSpecId: ' + customerAcctSpecId);
	                system.debug('financeRepId: ' + financeRepId);
	                
	                stl.COM__c = customerAcctSpecId;
	                stl.Customer_Finance_Rep__c = financeRepId;	                
	            }
	        }
        }
		catch (Exception e){
			
			system.debug('exception caught: ' + e.getMessage());
			apexLogHandler logHandler = new apexLogHandler('OrderEventHandler', 'lookupMyTeamUsers', e.getMessage());
            insert logHandler.logObj;
		}        
    } 
}