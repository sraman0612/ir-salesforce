/* 
*  Snehal Chabukswar || Date 02/07/2020 
*  Called from Lead_Trigger on before update and Insert event
*  Update Lead's AssignedDateTime  on Lead Owner Change
*  
*/
public class CTS_LeadAssignedDateTimeCalculation {
    
    public static void calculateLeadAssignedDateTime(List<Lead>newLeadList,Map<Id,Lead>oldLeadMap){
        //Changes done for #01231444 CTS Enhancement 
        string defaultBusinessHourId = RecordTypeUtilityClass.defaultBusinessHour.Id;
        
        //RecordType Id set for CTS Lead record types
        Id ctsLeadRecTypeId,hibbonLeadRecTypeId;
        Set<Id> CTSRecordtypeSet = new Set<Id>();
        for(RecordType recType:RecordTypeUtilityClass.getRecordTypeList('Lead')){
            if(recType.DeveloperName=='NA_Air' || recType.DeveloperName=='Standard_Lead' || recType.DeveloperName=='AIRD_Lead' 
               || recType.DeveloperName=='GES_Lead' || recType.DeveloperName=='CTS_MEIA_Lead'){
               
                CTSRecordtypeSet.add(recType.Id);
                ctsLeadRecTypeId = recType.Id;
            }
            if(recType.DeveloperName =='Hibon_Lead'){
                CTSRecordtypeSet.add(recType.Id);
                hibbonLeadRecTypeId = recType.Id;
            }
        }                
        Map<String,Id>bhsNameIdMap = new Map<String,Id>();
        Set<Id>userIdSet = new Set<Id>();
        Set<Id>leadIdSet = new Set<Id>();
        Set<Id>queueIdSet = new Set<Id>();//to store Milton Roy queue ids
        //Changes done for #01231444 CTS Enhancement 
        //for(BusinessHours bhs:[SELECT Id,Name, IsDefault, IsActive FROM BusinessHours]){
        for(BusinessHours bhs:RecordTypeUtilityClass.businessHoursList){
            /*if(bhs.IsDefault == true){
defaultBusinessHourId = bhs.Id;
}*/  
            bhsNameIdMap.put(bhs.Name,bhs.Id);
        }
        
        //Iterate over lead list to get owenerId set and Leadset which stisfy condition
        for(Lead leadObj:newLeadList){
            if((leadObj.Business__c == 'Milton Roy' || leadObj.Business__c == 'MP/OB/IRP' || leadObj.Business__c == 'ARO') && leadObj.Owner_is_Queue__c)
                queueIdSet.add(leadObj.ownerId);

            Lead oldLead;
            system.debug('leadObj.ownerId-------->'+leadObj.ownerId);
            if(!CTSRecordtypeSet.isEmpty() && leadObj.ownerId!=null &&  
               String.ValueOf((leadObj.ownerId).getsobjecttype())=='User'  && 
               CTSRecordtypeSet.contains(leadObj.RecordTypeId) )
            {
                system.debug('In First if loop--->');
                // check Owner Changes if Lead is updated 
                if(oldLeadMap!=null){
                    oldLead = oldLeadMap.get(LeadObj.Id); 
                    system.debug('In Update--->'+leadObj.CTS_isAssignedToOwner__c);
                    if(oldLead.OwnerId!= leadObj.OwnerId){
                        userIdSet.add(leadObj.OwnerId);
                        leadIdSet.add(leadObj.Id);
                    }
                    if(!leadObj.CTS_isAssignedToOwner__c){
                        userIdSet.add(leadObj.OwnerId);
                        leadIdSet.add(leadObj.Id);
                    }
                }else{
                    system.debug('In New Lead Trigger--->');
                    userIdSet.add(leadObj.OwnerId);
                    leadIdSet.add(leadObj.Id);
                }
                
            }
        }
        //Map to get BusinessHour Id for respective Lead Owner 
        Map<Id,Id>userIdBusinessHourIdMap = new Map<Id,Id>();
        if(!leadIdSet.isEmpty()){
            for(User userObj:[SELECT Id,Business_Hours__c FROM User WHERE Id IN:userIdSet]){
                if(userObj.Business_Hours__c!=null && bhsNameIdMap.containsKey(userObj.Business_Hours__c)){
                    system.debug('In Not null Business Hours----->'+userObj.Business_Hours__c);
                    userIdBusinessHourIdMap.put(userObj.Id,bhsNameIdMap.get(userObj.Business_Hours__c));
                }
                
            } 
        }
        //Milton Roy Code Starts here
        List<Group> groupList = [Select Id, Developername from Group WHERE type = 'Queue' and Id in :queueIdSet];
        Map<Id, String> groupMap = new Map<Id, String>();
        for(Group grp : groupList)
            groupMap.put(grp.Id, grp.Developername);
        
        Map<String, Queues_and_Business_Hour_Mapping__mdt>  mrQueueBHSMap = Queues_and_Business_Hour_Mapping__mdt.getAll();

        for(Lead leadObj:newLeadList){
            if((leadObj.Business__c == 'Milton Roy' || leadObj.Business__c == 'MP/OB/IRP' || leadObj.Business__c == 'ARO') && leadObj.Owner_is_Queue__c){
                if(groupMap.containsKey(leadObj.OwnerId) && mrQueueBHSMap.containsKey(groupMap.get(leadObj.OwnerId)) && String.isNotBlank(mrQueueBHSMap.get(groupMap.get(leadObj.OwnerId)).Business_Hours_Record_Id__c)){
                    userIdBusinessHourIdMap.put(leadObj.OwnerId, mrQueueBHSMap.get(groupMap.get(leadObj.OwnerId)).Business_Hours_Record_Id__c);
                    leadIdSet.add(leadObj.Id);
                }
            }
        }     
        //End of Milton Roy Code ends here

        
        //Iterate over lead List and update assigned date time if it is within business hours or next start date of Business hours
        for(Lead leadObj:newLeadList){
            String businessHoursId; 
            system.debug('leadIdSet.contains(leadObj.Id)--------------->'+leadIdSet.contains(leadObj.Id));
            if(leadIdSet.contains(leadObj.Id)  ){
                if(userIdBusinessHourIdMap.containsKey(leadObj.OwnerId)){
                    businessHoursId = userIdBusinessHourIdMap.get(leadObj.OwnerId);    
                }else{
                    system.debug('in Default Business Hours-------->');
                    businessHoursId = defaultBusinessHourId;
                }
                leadObj.Business_Hours__c = businessHoursId;
                
                /*Assign assignDateTime as now if current time is within business hours of Lead owners*/
                if(BusinessHours.isWithin(businessHoursId, system.now())){
                    leadObj.Assigned_Date_Time__c = system.now();
                }else{
                    leadObj.Assigned_Date_Time__c = BusinessHours.nextStartDate(businessHoursId, system.now()); 
                }
                
                
                //Initialize sla interval of 5 mins in miliseconds
                Long slaInterval; 
                if(leadObj.CTS_Lead_Region_Mappin__c == 'EU' && leadObj.RecordTypeId != hibbonLeadRecTypeId){
                    slaInterval = 8*60*60*1000; 
                    Datetime newDate = BusinessHours.add(businessHoursId,system.now(),slaInterval);
                    leadObj.CTS_NA_SLA__c = newDate;
                }
                system.debug('system.now()------->'+system.now());
                if(!leadObj.CTS_isAssignedToOwner__c){
                    
                    if(leadObj.CTS_Lead_Region_Mappin__c != 'EU'){
                        slaInterval = 5*60*1000;
                    }
                    Datetime newDate = BusinessHours.add(businessHoursId,system.now(),slaInterval);
                    leadObj.CTS_NA_SLA__c = newDate;
                    system.debug('newDate---------->'+newDate.format());
                }
                
                leadObj.CTS_isAssignedToOwner__c = true;
            }
            system.debug('leadObj.CTS_isAssignedToOwner__c--------->'+leadObj.CTS_isAssignedToOwner__c);
            
            
            if(!CTSRecordtypeSet.isEmpty() &&   leadObj.ownerId!=null &&
               String.ValueOf((leadObj.ownerId).getsobjecttype())!='User'  && 
               CTSRecordtypeSet.contains(leadObj.RecordTypeId) && !leadObj.CTS_isAssignedToOwner__c){
                   system.debug('In New but assigned to Queue------>');
                   leadObj.Assigned_Date_Time__c = Null;
                   leadObj.CTS_NA_SLA__c =  Null;
                   leadObj.Business_Hours__c = defaultBusinessHourId;
               }
        }
    }
    
    public static void updateElapsedTime(List<Lead>newLeadList,Map<Id,Lead>oldLeadMap){
        //RecordType Id set for CTS Lead record types
        Set<Id> CTSRecordtypeSet = new Set<Id>();
        for(RecordType recType:RecordTypeUtilityClass.getRecordTypeList('Lead')){
            if(recType.DeveloperName=='NA_Air'|| recType.DeveloperName =='Hibon_Lead' || recType.DeveloperName=='Standard_Lead'){
                CTSRecordtypeSet.add(recType.Id);
            }
        } 
        //Changes done for #01231444 CTS Enhancement 
        //String defaultBusinessHourId = [SELECT Id,Name, IsDefault, IsActive FROM BusinessHours WHERE IsDefault=true limit 1].Id;
        String defaultBusinessHourId = RecordTypeUtilityClass.defaultBusinessHour.Id;
        for(Lead leadObj:newLeadList){
            Lead oldLead = oldLeadMap.get(leadObj.Id);
            if(leadObj.Status != oldLead.Status && 
               leadObj.Status!='New' && 
               oldLead.Status=='New' &&
               CTSRecordtypeSet.contains(leadObj.RecordTypeId) &&
               leadObj.Assigned_Date_Time__c!= null  ){
                   leadObj.StageChangeDate__c = system.now();
                   if(system.now()>=leadObj.Assigned_Date_Time__c){
                       Long diffMs;
                       if(leadObj.Business_Hours__c!=null){
                           diffMs= BusinessHours.diff(leadObj.Business_Hours__c ,leadObj.Assigned_Date_Time__c ,system.now());
                           system.debug('diffMs-->'+diffMs);
                       }else{
                           diffMs= BusinessHours.diff(defaultBusinessHourId ,leadObj.Assigned_Date_Time__c ,system.now());
                           system.debug('default BH diffMs-->'+diffMs);
                       }
                       leadObj.d2subd2__c = diffMs/86400000.0000;
                       system.debug('diffMs/(60*60*24*1000)------>'+leadObj.d2subd2__c);
                   }else{
                       system.debug('In else');
                       leadObj.d2subd2__c = 0;
                   }
                   
                   
               }  
        }
    }

    }