/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioSendSMS {
    global TwilioSendSMS() {

    }
    global static void sendSMSToNumber(String toNumber, String message) {

    }
    global static void sendSMSToNumber(String toNumber, String message, Id recordId) {

    }
    global static void sendSMSToNumber(String toNumber, String message, String mediaUrl) {

    }
    global static void sendSMSToNumber(String toNumber, String message, String mediaUrl, Id recordId) {

    }
    global static void sendSMSToNumber(String toNumber, String message, String mediaUrl, Id recordId, String fromNumber) {

    }
    @InvocableMethod(label='Twilio Send SMS Message' description='Sends SMS message to a phone number')
    global static void sendSMSToNumber(List<TwilioSF.TwilioSendSMS.SmsDetail> smsDetails) {

    }
global class SmsDetail {
    @InvocableVariable(label='From Mobile Number' description='The Mobile Number Sending the message.' required=false)
    global String fromNumber;
    @InvocableVariable(label='Media URL' description='Link to an image that will be sent with the message.' required=false)
    global String mediaUrl;
    @InvocableVariable(label='Message' description='Message that will be sent.' required=true)
    global String message;
    @InvocableVariable(label='RecordId' description='Record Id to create a related task.' required=false)
    global Id recordId;
    @InvocableVariable(label='To Mobile Number' description='The Mobile Number Receiving the message.' required=true)
    global String toNumber;
    global SmsDetail() {

    }
    global SmsDetail(String toNum, String mesBody, Id recId) {

    }
    global SmsDetail(String toNum, String mesBody, Id recId, String fromNum) {

    }
    global SmsDetail(String toNum, String mesBody, String mediaUrl, Id recId) {

    }
    global SmsDetail(String toNum, String mesBody, String mediaUrl, Id recId, String fromNum) {

    }
}
}
