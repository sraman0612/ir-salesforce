global class CC_addAssets2FleetAnalysis {
  
  @InvocableMethod
  global static void addAssets(List <CC_Fleet_Analysis__c> faLst) {
    CC_Fleet_Analysis__c fa = [SELECT Id,Asset__c FROM CC_Fleet_Analysis__c WHERE Id = :faLst[0].Id];
    CC_Asset__c [] newAssetDetailLst = new CC_Asset__c[]{};
    CC_Asset_Analysis__c [] newAssetAnalysisLst = new CC_Asset_Analysis__c[]{};
    Set<Id> assetsForExistingAnalyses = new Set<Id>();
    CC_Asset_Group__c ag = [SELECT Id,Account__c,Acquired_Date__c,Color__c,Model_Year__c,Product__c,Product__r.P3__c,Quantity__c,(SELECT Id FROM Assets__r)
                             FROM CC_Asset_Group__c
                            WHERE Id = :fa.Asset__c];
    for(CC_Asset_Analysis__c aa : [SELECT Asset_Detail__c FROM CC_Asset_Analysis__c WHERE Fleet_Analysis__c = :fa.Id]){
      assetsForExistingAnalyses.add(aa.Asset_Detail__c);
    }
    if(ag.Assets__r.isEmpty()){
      for (Integer i = 1; i <= ag.Quantity__c; i++) {
        CC_Asset__c a = new CC_Asset__c();
        a.Account__c = ag.Account__c;
        a.Acquired_Date__c = ag.Acquired_Date__c;
        a.Asset_Group__c = ag.Id;
        a.Color__c = ag.Color__c;
        a.Decal_Number__c = String.valueOf(i);
        a.Model_Year__c = ag.Model_Year__c;
        a.Product__c = ag.Product__c;
        a.Status__c = 'Active';
        newAssetDetailLst.add(a);
      }
    }
    if(!newAssetDetailLst.isEmpty()){
      insert newAssetDetailLst;
    }                          
    for(CC_Asset__c a : [SELECT Id,Product__c FROM CC_Asset__c WHERE Asset_Group__c = :ag.Id]){
      if(!assetsForExistingAnalyses.contains(a.Id)){
        
        CC_Asset_Analysis__c aa = new CC_Asset_Analysis__c();
        aa.Asset_Detail__c = a.Id;
        aa.Fleet_Analysis__c = faLst[0].Id;
        aa.Model__c = a.Product__c;
        newAssetAnalysisLst.add(aa);
      }
    }
    if(!newAssetAnalysisLst.isEmpty()){
      insert newAssetAnalysisLst;
    }
  }
}