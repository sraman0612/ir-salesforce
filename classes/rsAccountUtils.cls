public class rsAccountUtils {

    // Account Record Type Developer Names
    public static final string RECORD_TYPE_RSHVAC = 'RS_HVAC';
    public static final string RECORD_TYPE_NAAIR = 'Site_Account_NA_Air';
    
    // Map that contains all  Record Types by Developer Name
    public static Map<String, RecordType> ACCOUNT_RECORD_TYPES = new Map<String, RecordType>();
    
    // Static initializer
    static{
      for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Account']){
        ACCOUNT_RECORD_TYPES.put(rt.DeveloperName, rt);
      }
    }
    
    public static final string PROFILE_RHVAC = 'RHVAC Sales'; 
    
    public void assignRshvacUsersToAccounts(){
        System.debug('assignRshvacUsersToAccounts top');
        Profile prof =  [SELECT Id, Name FROM Profile WHERE Name = :PROFILE_RHVAC];
        Id profileId = prof.Id;
        List<User> rshvacUsers = [SELECT Id, Name, Sales_Person_Id__c FROM User WHERE profileId = :profileId];
        Map<String, User> salespersonMap = new Map<String, User>();
        for(User usr : rshvacUsers) { 
            salespersonMap.put(usr.Sales_Person_Id__c, usr);
        }
        
        List<Account> accountRecordsOut = new List<Account>();
        Id rtype = ACCOUNT_RECORD_TYPES.get(RECORD_TYPE_RSHVAC).Id;
        List<Account> accts = 
            [SELECT Id, Name, OwnerId, Sales_Person_Id__c FROM Account WHERE RecordTypeId = :rtype AND OwnerId = '00511000003yN7r' AND Sales_Person_Id__c != NULL LIMIT 1000];
        for(Account acc : accts) {
            System.debug('looking at account ' + acc.Name);
            if(acc.Sales_Person_Id__c != null) {
                System.debug('looking for salesperson ' + acc.Sales_Person_Id__c);
                User salesperson = salespersonMap.get(acc.Sales_Person_Id__c);
                if (salesperson != null) {
                    System.debug('Found Salesperson');
                    acc.OwnerId = salesperson.Id;
                    accountRecordsOut.add(acc);
                }
            }
        }
        
        if (!accountRecordsOut.isEmpty()) {
            try {
                update accountRecordsOut;
            } catch (DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
        }
    }
        
    public void assignRshvacUser(String salespersonId){
        System.debug('assignRshvacUser top');
        Profile prof =  [SELECT Id, Name FROM Profile WHERE Name = :PROFILE_RHVAC];
        Id profileId = prof.Id;
        List<User> rshvacUsers = [SELECT Id, Name, Sales_Person_Id__c FROM User WHERE profileId = :profileId];
        Map<String, User> salespersonMap = new Map<String, User>();
        for(User usr : rshvacUsers) { 
            salespersonMap.put(usr.Sales_Person_Id__c, usr);
        }
            
        List<Account> accountRecordsOut = new List<Account>();
        Id rtype = ACCOUNT_RECORD_TYPES.get(RECORD_TYPE_RSHVAC).Id;
        List<Account> accts = 
            [SELECT Id, Name, OwnerId, Sales_Person_Id__c FROM Account WHERE RecordTypeId = :rtype AND Sales_Person_Id__c = :salespersonId];
        for(Account acc : accts) {
            System.debug('looking at account ' + acc.Name);
            if(acc.Sales_Person_Id__c != null) {
                System.debug('looking for salesperson ' + salespersonId);
                User salesperson = salespersonMap.get(salespersonId);
                if (salesperson != null) {
                    System.debug('Found Salesperson');
                    acc.OwnerId = salesperson.Id;
                    accountRecordsOut.add(acc);
                }
            }
        }
            
        if (!accountRecordsOut.isEmpty()) {
            try {
                update accountRecordsOut;
            } catch (DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
        }
        
    }
    
}