@isTest
public with sharing class CC_Short_Term_Lease_Trigger_Test {
    
    @TestSetup
    private static void createData(){	
    	
    	TestDataUtility dataUtility = new TestDataUtility();
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;    	
        
        CC_Short_Term_Lease_Settings__c leaseSettings = CC_Short_Term_Lease_Settings__c.getOrgDefaults();
        leaseSettings.Restricted_Finance_Approval_Roles__c = 'CC_Finance_Leadership,CC_Finance_Team';
        insert leaseSettings;
    	
    	// Create users to use in the sales hiearchy
    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'xyz123';

    	User ccIntegrationUser = TestUtilityClass.createUser('CC_API_Profile', null);
    	ccIntegrationUser.Alias = 'xyz111';        

    	User salesRepUser = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser.Alias = 'alias122';
    	
    	User salesRepMgr = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr.Alias = 'alias123';
    	salesRepMgr.PS_Purchasing_LOA__c = 50000;
    	
    	User salesRepDir = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir.Alias = 'alias124';
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    
    	    	
    	User casUser = TestUtilityClass.createUser('CC_General Admin', null); 	
    	casUser.LastName = 'CASTestUser';
    	
    	insert new User[]{salesRepUser, salesRepMgr, salesRepDir, salesRepVP, casUser, adminUser, ccIntegrationUser};
    	
    	salesRepUser.ManagerId = salesRepMgr.Id;
    	salesRepMgr.ManagerId = salesRepDir.Id;
    	salesRepDir.ManagerId = salesRepVP.Id;
    	
    	update new User[]{salesRepUser, salesRepMgr, salesRepDir};    	
    	
    	User financeUser;
    	
    	// Avoid mixed DML since role will be assigned to the finance user
    	system.runAs(adminUser){	
    	
    		UserRole leaseRole = [Select Id From UserRole Where DeveloperName = 'CC_Aftermarket_Team' LIMIT 1];            
    	
	    	User leaseUser = TestUtilityClass.createUser('CC_Pavilion Admin', null);
	    	leaseUser.FirstName = 'Lease1';
	    	leaseUser.LastName = 'User';
	    	leaseUser.Alias = 'xyz124';  
	    	leaseUser.UserRoleId = leaseRole.Id;
    	
	        financeUser = TestUtilityClass.createUser('CC_General Admin', null);
	        financeUser.LastName = 'FinanceTestUser';    	
	        financeUser.UserRoleId = [Select Id From UserRole Where DeveloperName = 'CC_Finance_Team' LIMIT 1].Id;

	    	insert new User[]{financeUser, leaseUser};
	    	
    		// Assign the lease permission set to the lease user
			PermissionSet leasePermSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Leases'];	    	
	    	
			PermissionSetAssignment permSetAssign = new PermissionSetAssignment(
				AssigneeId = leaseUser.Id,
				PermissionSetId = leasePermSet.Id
			);    	
			
			insert permSetAssign;	    
            
            UserRole ccRole = [Select Id From UserRole Where DeveloperName = 'Club_Car' LIMIT 1];
            ccIntegrationUser.UserRoleId = ccRole.Id;
            update ccIntegrationUser;	
    	}
    	
    	// Create a sales rep that has a manager, director, and VP in the hierarchy
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser.Id);
    	insert salesRep;
    	
    	// Create Customer Accounts 
    	Account acct1 = TestUtilityClass.createAccount2();
    	acct1.BillingCountry = 'USA';
    	acct1.ShippingCountry = 'USA';
    	insert acct1;
    	
    	CC_Order__c order1 = TestUtilityClass.createNatAcctOrder(acct1.Id, acct1.Id);
    	insert order1;
    	
    	// Create Master Lease 
    	CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
        
        // Assign new Account to Master Lease
        ml1.Customer__c = acct1.Id;
        insert ml1;
        
        // Create My Team roles
        CC_My_Team__c role1 = TestDataUtility.createMyTeam(acct1.Id, casUser.Id, CC_My_Team_Service.customerAcctSpecRole);
        CC_My_Team__c role2 = TestDataUtility.createMyTeam(acct1.Id, financeUser.Id, CC_My_Team_Service.financeRepRole);
    	insert new CC_My_Team__c[]{role1, role2};
    	
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();
        insert location;

    	CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
    	
        for (Integer i = 0; i < 5; i++){
        	
        	CC_Short_Term_Lease__c lease = TestDataUtility.createShortTermLease(null, location.Id);
        	lease.Sales_Rep__c = salesRep.Id;
            lease.Sales_Rep_User__c = salesRepUser.Id;
        	lease.Master_Lease__c = ml1.Id;
            lease.Carrier__c = 'UC LS OUT';
			leases.add(lease);
        } 	
        
        insert leases;     

        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
	    prod1.CC_Item_Class__c = 'LCAR';
        insert prod1;

        CC_STL_Car_Info__c[] cars = new CC_STL_Car_Info__c[]{};

        for (CC_Short_Term_Lease__c lease : leases){
	     
            CC_STL_Car_Info__c car = TestDataUtility.createSTLCarInfo(prod1.Id);
	        car.Short_Term_Lease__c = lease.Id;
	        car.Quantity__c = 10;
	        car.Per_Car_Cost__c = 25; 
            cars.add(car);          
        }

        insert cars;
    }

    @isTest
    private static void testUpdateCases(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c Where Vehicle_Status__c != 'Cancelled'];
    	system.assertEquals(5, leases.size());

        Account a = [Select Id From Account LIMIT 1];

        Test.startTest();

        // Create some cases and assign them to the leases
        Id ccCaseRecordTypeId = SObjectService.getRecordTypeId(Case.sObjectType, 'Club Car');

        Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', leases[0].Id, 5000);
        
        Case c2 = TestDataUtility.createCase(false, ccCaseRecordTypeId, null, a.Id, 'Test2', null);
        c2.CC_Short_Term_Lease__c = leases[0].Id;

        Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', leases[1].Id, 5000);

        insert new Case[]{c1, c2, c3};        
	
        // Update the serial numbers on some of the leases
		for (Integer i = 0; i < 3; i++){

            CC_Short_Term_Lease__c lease = leases[i];
            lease.Serial_Numbers__c = 'ABC' + i;
		}	        

        // Update the leases to fire the case updates
        update leases;

        // Verify the transportation case serial numbers were updated appropriately
        c1 = [Select CC_STL_Serial_Numbers__c From Case Where Id = :c1.Id];
        system.assertEquals('ABC0', c1.CC_STL_Serial_Numbers__c);

        c2 = [Select CC_STL_Serial_Numbers__c From Case Where Id = :c2.Id];
        system.assertEquals(null, c2.CC_STL_Serial_Numbers__c);

        c3 = [Select CC_STL_Serial_Numbers__c From Case Where Id = :c3.Id];
        system.assertEquals('ABC1', c3.CC_STL_Serial_Numbers__c);                

        Test.stopTest();
    }    

    @isTest
    private static void testValidateMapicsSyncNonRevenueSuccess(){

        Schema.FieldSet allLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_All_Leases;

        String query = 'Select Id';

        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        }

        query += ' From CC_Short_Term_Lease__c Where Lease_Category__c = \'Non-Revenue\' LIMIT 1';

        CC_Short_Term_Lease__c lease = Database.query(query);

        User leaseUser = [Select Id From User Where Alias = 'xyz124'];  

        // Go through each required field and make sure it has a value
        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){

            system.debug('fsm: ' + fsm);

            Schema.DisplayType fieldtype = fsm.getType();

            String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));   

            if (String.isBlank(fieldStringVal)){

                if (fieldType == Schema.DisplayType.PICKLIST || fieldType == Schema.DisplayType.STRING){
                    lease.put(fsm.getFieldPath(), 'ABC');
                }
            }   
            else if (fieldStringVal == '0.00' && (fieldType == Schema.DisplayType.DOUBLE || fieldType == Schema.DisplayType.CURRENCY)){
                lease.put(fsm.getFieldPath(), 50);
            }
        }  

        update lease;

        Test.startTest();

        System.runAs(leaseUser){

            // Send the lease to MAPICS
            lease.Synced_to_MAPICS__c = true;
            update lease;            
        }

        Test.stopTest();
    }

    @isTest
    private static void testValidateMapicsSyncNonRevenueFailure(){

        Schema.FieldSet allLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_All_Leases;

        String query = 'Select Id';

        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        }

        query += ' From CC_Short_Term_Lease__c Where Lease_Category__c = \'Non-Revenue\' LIMIT 1';

        CC_Short_Term_Lease__c lease = Database.query(query);

        User leaseUser = [Select Id From User Where Alias = 'xyz124'];  

        // Go through each required field and make sure at least one of the required fields is empty
        Set<String> missingRequiredFields = new Set<String>();

        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){

            system.debug('fsm: ' + fsm);

            Schema.DisplayType fieldtype = fsm.getType();

            String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));   

            if (String.isNotBlank(fieldStringVal)){

                if (fieldType == Schema.DisplayType.PICKLIST || fieldType == Schema.DisplayType.STRING){

                    Schema.DescribeFieldResult dfr = fsm.getSObjectField().getDescribe();

                    lease.put(fsm.getFieldPath(), null);
                    missingRequiredFields.add(dfr.getLabel());
                }
            }   
        }  

        update lease;

        Test.startTest();

        System.runAs(leaseUser){

            // Send the lease to MAPICS
            Boolean fail = false;

            try{
                lease.Synced_to_MAPICS__c = true;
                update lease;            
            }
            catch(Exception e){
                
                fail = true;
                
                for (String missingField : missingRequiredFields){
                    system.assert(e.getMessage().contains(missingField));
                }
            }

            system.assert(true, fail);
        }

        Test.stopTest();
    }   

    @isTest
    private static void testValidateMapicsSyncRevenueSuccess(){

        Schema.FieldSet allLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_All_Leases;
        Schema.FieldSet revenueLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_Revenue_Leases;

        String query = 'Select Id';

        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        }

        for (FieldSetMember fsm : revenueLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        }        

        query += ' From CC_Short_Term_Lease__c Where Lease_Category__c = \'Non-Revenue\' LIMIT 1';

        CC_Short_Term_Lease__c lease = Database.query(query);

        User leaseUser = [Select Id From User Where Alias = 'xyz124'];  

        // Go through each required field and make sure it has a value
        FieldSetMember[] fsms = new FieldSetMember[]{};
        fsms.addAll(allLeasesRequiredFieldSet.getFields());
        fsms.addAll(revenueLeasesRequiredFieldSet.getFields());

        for (FieldSetMember fsm : fsms){

            system.debug('fsm: ' + fsm);

            Schema.DisplayType fieldtype = fsm.getType();

            String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));   

            if (String.isBlank(fieldStringVal)){

                if (fieldType == Schema.DisplayType.PICKLIST || fieldType == Schema.DisplayType.STRING){
                    lease.put(fsm.getFieldPath(), 'ABC');
                }
            }   
            else if (fieldStringVal == '0.00' && (fieldType == Schema.DisplayType.DOUBLE || fieldType == Schema.DisplayType.CURRENCY)){
                lease.put(fsm.getFieldPath(), 50);
            }
        }  

        lease.Lease_Category__c = 'Revenue';
        update lease;

        Test.startTest();

        System.runAs(leaseUser){

            // Send the lease to MAPICS
            lease.Synced_to_MAPICS__c = true;
            update lease;            
        }

        Test.stopTest();
    }     

    @isTest
    private static void testValidateMapicsSyncRevenueFailure(){

        Schema.FieldSet allLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_All_Leases;
        Schema.FieldSet revenueLeasesRequiredFieldSet = Schema.SObjectType.CC_Short_Term_Lease__c.fieldSets.CTS_Sync_Required_Fields_Revenue_Leases;

        String query = 'Select Id';

        for (FieldSetMember fsm : allLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        }

        for (FieldSetMember fsm : revenueLeasesRequiredFieldSet.getFields()){
            query += ',' + fsm.getFieldPath();  
        } 

        query += ' From CC_Short_Term_Lease__c Where Lease_Category__c = \'Non-Revenue\' LIMIT 1';

        CC_Short_Term_Lease__c lease = Database.query(query);

        User leaseUser = [Select Id From User Where Alias = 'xyz124'];  

        // Go through each required field and make sure at least one of the required fields is empty
        Set<String> missingRequiredFields = new Set<String>();

        for (FieldSetMember fsm : revenueLeasesRequiredFieldSet.getFields()){

            system.debug('fsm: ' + fsm);

            Schema.DisplayType fieldtype = fsm.getType();

            String fieldStringVal = String.valueOf(lease.get(fsm.getFieldPath()));   

            if (String.isNotBlank(fieldStringVal)){

                if (fieldType == Schema.DisplayType.PICKLIST || fieldType == Schema.DisplayType.STRING){

                    Schema.DescribeFieldResult dfr = fsm.getSObjectField().getDescribe();

                    lease.put(fsm.getFieldPath(), null);
                    missingRequiredFields.add(dfr.getLabel());
                }
            }   
        }  

        lease.Lease_Category__c = 'Revenue';
        update lease;

        Test.startTest();

        System.runAs(leaseUser){

            // Send the lease to MAPICS
            Boolean fail = false;

            try{
                lease.Synced_to_MAPICS__c = true;
                update lease;            
            }
            catch(Exception e){
                
                fail = true;
                
                for (String missingField : missingRequiredFields){
                    system.assert(e.getMessage().contains(missingField));
                }
            }

            system.assert(true, fail);
        }

        Test.stopTest();
    }       

    @isTest
    private static void testProcessCancelledLeases(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c Where Vehicle_Status__c != 'Cancelled'];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('Revenue_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testProcessCancelledLeases');	
	
        // Verify no errors were logged for the approval requests
        Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testProcessCancelledLeases'];
        
        system.assertEquals(0, errorsLogged.size());

		// Get the user to test with (CC Integration)
		User ccIntegrationUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz111'];	

		system.runAs(ccIntegrationUser){
	
			system.assertEquals(leases.size(), [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);
	
			Test.startTest();

            CC_Short_Term_Lease__c[] leasesToCancel = new CC_Short_Term_Lease__c[]{};

            // Cancel all of the leases except 1
            for (Integer i = 0; i < leases.size() - 1; i++){
                CC_Short_Term_Lease__c lease = leases[i];
                lease.Vehicle_Status__c = 'Cancelled';
                leasesToCancel.add(lease);
            }

            update leasesToCancel;

			Test.stopTest();	
		}
		
		// Verify 1 approval remains
		system.assertEquals(1, [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);

        // Verify the other approvals were recalled        
        system.assertEquals(4, [Select Count() From ProcessInstanceStep Where ProcessInstance.TargetObjectId in :leases and ProcessInstance.Status = 'Removed']);
        
		// Verify no errors were logged
		Apex_Log__c[] errorsLogged2 = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Handler' and Method_Name__c = 'processCancelledLeases'];
				
		system.assertEquals(0, errorsLogged2.size());
    }
       
    @isTest
    private static void testValidateLeaseApprovalOverrideRevenueLease(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('Revenue_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet1');	
	
		// Get the lease user to test with
		User leaseUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz124'];	

		system.runAs(leaseUser){
	
			// Approve for sales manager
			Approval.ProcessWorkitemRequest[] approvalRequests2 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests2.add(approvalRequest);
			}	
			
			system.debug('-approving for sales manager');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests2, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet2');
					
			// Approve for lease mgmt
			Approval.ProcessWorkitemRequest[] approvalRequests3 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id, ProcessInstanceId From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
	
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests3.add(approvalRequest);
			}	
			
			system.debug('-approving for lease mgmt');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests3, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet3');						
			
			// Verify no errors were logged for the approval requests
			Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet3'];
			
			system.assertEquals(0, errorsLogged.size());			
			
			Test.startTest();
	
			// Try to approve for finance
			Approval.ProcessWorkitemRequest[] approvalRequests4 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id, ProcessInstanceId From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests4.add(approvalRequest);
			}	
			
			system.debug('-approving for finance');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests4, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet4');		
			
			Test.stopTest();	
		}
		
		// Verify the approvals are still there
		system.assertEquals(leases.size(), [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);
		
		// Verify errors were logged for the approval requests
		Apex_Log__c[] errorsLogged2 = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet4'];
				
		system.assertEquals(leases.size(), errorsLogged2.size());
		
		for (Apex_Log__c log : errorsLogged2){
			system.assertEquals(CC_Short_Term_Lease_Trigger_Handler.leaseApprovalOverrideErrorMsg, log.Message__c);
		}
    }
    
    @isTest
    private static void testValidateLeaseApprovalOverrideNewCarLease(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('New_Car_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet1');	
	
		// Get the lease user to test with
		User leaseUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz124'];	

		system.runAs(leaseUser){	
	
			// Approve for sales VP
			Approval.ProcessWorkitemRequest[] approvalRequests2 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests2.add(approvalRequest);
			}	
			
			system.debug('-approving for sales VP');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests2, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet2');
		
			// Verify no errors were logged for the approval requests
			Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet2'];
			
			system.assertEquals(0, errorsLogged.size());			
			
			Test.startTest();
			
			// Approve for finance VP (role is not in the custom settings finance role list)
			Approval.ProcessWorkitemRequest[] approvalRequests3 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id, ProcessInstanceId From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
	
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests3.add(approvalRequest);
			}	
			
			system.debug('-approving for finance VP');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests3, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet3');			
			
			Test.stopTest();	
		}
		
		// Verify the approvals are no longer there
		system.assertEquals(0, [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);
		
		// Verify errors were logged for the approval requests
		Apex_Log__c[] errorsLogged2 = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSe3'];
				
		system.assertEquals(0, errorsLogged2.size());
    } 
    
    @isTest
    private static void testValidateLeaseApprovalOverrideCharityLease(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('Charity_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet1');	
	
		// Get the lease user to test with
		User leaseUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz124'];	

		system.runAs(leaseUser){	
		
			// Verify no errors were logged for the approval requests
			Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet1'];
			
			system.assertEquals(0, errorsLogged.size());			
			
			Test.startTest();
			
			// Approve for Ray Bentley
			Approval.ProcessWorkitemRequest[] approvalRequests2 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests2.add(approvalRequest);
			}	
			
			system.debug('-approving for Ray Bentley');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests2, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet2');		
			
			Test.stopTest();	
		}
		
		// Verify the approvals are no longer there
		system.assertEquals(0, [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);
		
		// Verify errors were logged for the approval requests
		Apex_Log__c[] errorsLogged2 = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet2'];
				
		system.assertEquals(0, errorsLogged2.size());
    }
    
    @isTest
    private static void testValidateLeaseApprovalOverrideNonRevenueLease(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('Non_Revenue_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet1');	
	
		// Get the lease user to test with
		User leaseUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz124'];	

		system.runAs(leaseUser){	
		
			// Verify no errors were logged for the approval requests
			Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet1'];
			
			system.assertEquals(0, errorsLogged.size());			
			
			Test.startTest();
			
			// Approve for sales manager
			Approval.ProcessWorkitemRequest[] approvalRequests2 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Approve');
				approvalRequests2.add(approvalRequest);
			}	
			
			system.debug('-approving for sales manager');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests2, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet2');		
			
			Test.stopTest();	
		}
    }
		
    @isTest
    private static void testValidateLeaseApprovalOverrideNonRevenueLeaseRejection(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_User__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
	
		// Submit each lease for approval
		Approval.ProcessSubmitRequest[] approvalRequests1 = new Approval.ProcessSubmitRequest[]{};
		
		for (CC_Short_Term_Lease__c lease : leases){
												
			Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
			approvalRequest.setSkipEntryCriteria(true);
			approvalRequest.setObjectId(lease.Id);
			approvalRequest.setProcessDefinitionNameOrId('Non_Revenue_Lease_Approval');
			approvalRequests1.add(approvalRequest);
		}	
		
		sObjectService.processApprovalRequestsAndLogErrors(approvalRequests1, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet1');	
	
		// Get the lease user to test with
		User leaseUser = [Select Id, Name, UserRoleId, UserRole.DeveloperName From User Where isActive = true and Alias = 'xyz124'];	

		system.runAs(leaseUser){	
		
			// Verify no errors were logged for the approval requests
			Apex_Log__c[] errorsLogged = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet1'];
			
			system.assertEquals(0, errorsLogged.size());			
			
			Test.startTest();
			
			// Approve for sales manager
			Approval.ProcessWorkitemRequest[] approvalRequests2 = new Approval.ProcessWorkitemRequest[]{};
			
			for (ProcessInstanceWorkitem workItem : [Select Id From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]){
				
				Approval.ProcessWorkitemRequest approvalRequest = new Approval.ProcessWorkitemRequest();												
				approvalRequest.setWorkItemId(workItem.Id);
				approvalRequest.setAction('Reject');
				approvalRequests2.add(approvalRequest);
			}	
			
			system.debug('-approving for sales manager');
			
			sObjectService.processApprovalRequestsAndLogErrors(approvalRequests2, 'CC_Short_Term_Lease_Trigger_Test', 'testValidateApprovalOverrideWithPermissionSet2');		
			
			Test.stopTest();	
		}		
		
		// Verify the approvals are no longer there
		system.assertEquals(0, [Select Count() From ProcessInstanceWorkitem Where ProcessInstance.TargetObjectId in :leases]);
		
		// Verify errors were logged for the approval requests
		Apex_Log__c[] errorsLogged2 = [Select Id, Message__c From Apex_Log__c Where Class_Name__c = 'CC_Short_Term_Lease_Trigger_Test' and Method_Name__c = 'testValidateApprovalOverrideWithPermissionSet2'];
				
		system.assertEquals(0, errorsLogged2.size());
    }                                                  
    
    @isTest
    private static void testAssignSalesHiearchy(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Sales_Rep__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
    	
    	// Verify the sales hierarchy assigned
    	User salesRepUser = [Select Id, ManagerId, Manager.ManagerId, Manager.Manager.ManagerId From User Where Alias = 'alias122']; 
		
		for (CC_Short_Term_Lease__c lease : leases){
			system.assertEquals(salesRepUser.ManagerId, lease.Sales_Rep_Manager__c);
			system.assertEquals(salesRepUser.Manager.ManagerId, lease.Sales_Rep_Director__c);
			system.assertEquals(salesRepUser.Manager.Manager.ManagerId, lease.Sales_Rep_VP__c);
		}    	
		
		// Clear the sales rep from a lease
		leases[0].Sales_Rep__c = null;
		update leases[0];
		
		leases = [Select Id, Sales_Rep__c, Sales_Rep_Manager__c, Sales_Rep_Director__c, Sales_Rep_VP__c From CC_Short_Term_Lease__c];
		
		system.assertEquals(null, leases[0].Sales_Rep_Manager__c);
		system.assertEquals(null, leases[0].Sales_Rep_Director__c);
		system.assertEquals(null, leases[0].Sales_Rep_VP__c);		
    }
    
    @isTest
    private static void testLookupMyTeamUsers(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, CustomerId__c, COM__c, Customer_Finance_Rep__c From CC_Short_Term_Lease__c];
    	system.assertEquals(5, leases.size());
        
    	User casRep = [Select Id From User Where LastName = 'CASTestUser'];
    	User financeRep = [Select Id From User Where LastName = 'FinanceTestUser'];

		for (CC_Short_Term_Lease__c lease : leases){
			system.assertEquals(casRep.Id, lease.COM__c);
			system.assertEquals(financeRep.Id, lease.Customer_Finance_Rep__c);
		}		
    }
    
    @isTest
    private static void tesPreventDeletions(){
    	
    	CC_Short_Term_Lease__c[] leases = [Select Id, Approval_Status__c, New_Car_Order__c, Cars__c From CC_Short_Term_Lease__c Order By CreatedDate ASC];
    	system.assertEquals(5, leases.size());
    	
    	CC_Order__c ord = [Select Id From CC_Order__c LIMIT 1];
        
        // Allow deletion
        leases[0].Approval_Status__c = 'Not Yet Submitted';
        leases[0].New_Car_Order__c = null;
        leases[0].Cars__c = null;	
        
        // Allow deletion
        leases[1].Approval_Status__c = null;
        leases[1].New_Car_Order__c = null;
        leases[1].Cars__c = null;	
        
        // Do not allow deletion - approval status
        leases[2].Approval_Status__c = 'Routing';
        leases[2].New_Car_Order__c = null;
        leases[2].Cars__c = null;	
        
        // Do not allow deletion - order
        leases[3].Approval_Status__c = null;
        leases[3].New_Car_Order__c = ord.Id;
        leases[3].Cars__c = null;	
        
        // Do not allow deletion - cars
        leases[4].Approval_Status__c = null;
        leases[4].New_Car_Order__c = null;
        leases[4].Cars__c = 'A';
        
        update leases;	
        
        Database.DeleteResult[] deletionResults;
        
        Test.startTest();
        
        deletionResults = Database.delete(leases, false);
        
        Test.stopTest();      
        
        // Verify results
        leases = [Select Id, Approval_Status__c, New_Car_Order__c, Cars__c From CC_Short_Term_Lease__c Order By CreatedDate ASC];   
        
        for (Integer i = 0; i < deletionResults.size(); i++){
        	
        	Database.DeleteResult result = deletionResults[i];
        	
        	if (i < 2){
        		system.assertEquals(true, result.isSuccess());
        	}
        	else{
        		system.assertEquals(false, result.isSuccess());
        		system.assertEquals(CC_Short_Term_Lease_Trigger_Handler.leaseDeletionErrorMsg, result.getErrors()[0].getMessage());
        	}
        }                       
    }    
}