/**
* Test Class x3
* 
**/
@isTest
public class PT_CreateScheduleOppTest {
    
    static testmethod void testSchedules(){
        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
        Account a = new Account(
            RecordTypeId = ptAcctRTID,
            Name = 'TestAcc',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );
        
        insert a;
        
        PT_Parent_Opportunity__c p = TestUtilityClass.createParentOpp(a.Id);
        p.Lead_SourcePL__c='Other';
        insert p;
        
        List<Id> ids = new List<Id>();
        ids.add(p.Id);
        
        Test.startTest();
        PT_CreateScheduleOpp.createOpps(ids);
        Test.stopTest();
    }
}