@isTest
public class CC_PavilionEventsCalendarController_Test 
{
    static testMethod void EventsCalendarTest()
    {
       User testUser = TestUtilityClass.createPortalUserCC();
       insert testUser;
       Account acnt = [SELECT id, CC_Global_Region__c FROM Account WHERE Name ='Setup Sample Account' LIMIT 1];
       acnt.CC_Global_Region__c = 'USA';
       update acnt;
       Contract c=TestUtilityClass.createContract(acnt.id);
       c.CC_Contract_Status__c ='Active';
       c.CC_Type__c = 'Dealer/Distributor Agreement';
       c.CC_Sub_Type__c = 'Commercial Utility';
       insert c;
       Test.startTest();
       system.runAs(testUser){
       //CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
       //CC_PavilionEventsCalendarController objCtrl1 = new CC_PavilionEventsCalendarController(controller);
       CC_PavilionEventsCalendarController objCtrl = new CC_PavilionEventsCalendarController();
       CC_Pavilion_Content__c objContent = TestUtilityClass.createCustomPavilionContentwithtype('Events Calendar','Events');
       objContent.Agreement_Type__c ='Commercial Utility';
       objContent.CC_Region__c = 'USA';
       insert objContent;
       system.assertEquals(objContent.Type__c, 'Events Calendar');
       CC_PavilionEventsCalendarController.getPavilionEventsContent();
       }
       Test.stopTest();
    }
}