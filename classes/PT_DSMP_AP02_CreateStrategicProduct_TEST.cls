@IsTest
public with sharing class PT_DSMP_AP02_CreateStrategicProduct_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for PT_DSMP_AP02_CreateStrategicProduct
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 	
	
	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static Account testAcc = new Account();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_TARGET__c> lstPTDsmpTarget = new  List<PT_DSMP_TARGET__c>();
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd = new list<PT_DSMP_Strategic_Product__c>();
    static Id rtAccPowertools;

	static {
		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();

		mainUser = PT_DSMP_TestFactory.createAdminUser('LC01_ProductTarget_TEST@test.COM', 
														PT_DSMP_Constant.getProfileIdAdmin());
        insert mainUser;
	}


    @isTest static void test_runBatchNPD_error(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            insert lstPTDsmpYear;

            testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');//IR Comp OEM EU
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }

            insert lstPTDsmpStratagicProd;

            PT_DSMP_BAT03_InsertStrategicProducts batch = new PT_DSMP_BAT03_InsertStrategicProducts(lstPTDsmpStratagicProd);
            Database.executebatch(batch);

            Test.stopTest();
        }
    }


    @isTest static void test_runBatchNPD(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            insert lstPTDsmpYear;

            testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');//IR Comp OEM EU
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }

            PT_DSMP_BAT03_InsertStrategicProducts batch = new PT_DSMP_BAT03_InsertStrategicProducts(lstPTDsmpStratagicProd);
            Database.executebatch(batch);

            Test.stopTest();
        }
    }


	@isTest static void test_insertNPD(){
        System.runAs(mainUser){ 
        	Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            insert lstPTDsmpYear;

            testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');//IR Comp OEM EU
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            PT_DSMP_NPD__c npd1 = PT_DSMP_TestFactory.createDSMPNPD('test', lstPTDsmpYear[0].Id);
            insert npd1;

        	Test.stopTest();
        }
    }

    @isTest static void test_insertNPD_year2(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            insert lstPTDsmpYear;

            testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');//IR Comp OEM EU
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            PT_DSMP_NPD__c npd1 = new PT_DSMP_NPD__c(Name = 'test',
                                                     Year2__c = String.valueOf( System.today().year() ));
            insert npd1;

            Test.stopTest();
        }
    }
}