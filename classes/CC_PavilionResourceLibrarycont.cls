public with sharing class CC_PavilionResourceLibrarycont
{

    public CC_PavilionResourceLibrarycont(CC_PavilionTemplateController controller) {

    }

    /* member variables */
    public List<ContentVersion> contentResource = new List<ContentVersion>();
    /* constructor */    
    Public CC_PavilionResourceLibrarycont(){          
    }
    /* remote action to add order items*/
    @RemoteAction
    public static List<ContentVersion> getorderItems(string lastentryid) {        
        List<ContentVersion> contentResource = new List<ContentVersion>();
        contentResource = [select id, Content_Title__c,
                           Marketing_Image_Category__c,Marketing_Image_Type__c,
                           Marketing_Image_Sub_Type__c, Content_Sub_title__c, 
                           Content_Body__c, Vehicle__c, CS_Category__c, 
                           RecordType.Name from ContentVersion
                           where RecordType.Name='Marketing Images'];
        return contentResource ;
    }
    /* remote action add marketing categories */
    @RemoteAction
    public static list<selectOption> getmarketingCategories()
    {
        list<selectOption> catOpt= new list<selectOption>();
        list<Schema.PicklistEntry> values =
            ContentVersion.Marketing_Image_Category__c.getDescribe().getPickListValues();
        catOpt.add(new SelectOption('Category','--None--'));
        for (Schema.PicklistEntry a : values)
        { 
            catOpt.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return catOpt;
    }
    /* remote action to display Marketing Types based on image */
    @RemoteAction
    public static list<selectOption> getmarketingtype()
    {
        list<selectOption> catOpt= new list<selectOption>();
        list<Schema.PicklistEntry> values =
            ContentVersion.Marketing_Image_Type__c.getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values)
        { 
            catOpt.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return catOpt;
    }
    /* remote action to display marketing subtype based on image */
    @RemoteAction
    public static list<selectOption> getmarketingsubtype()
    {
        list<selectOption> catOpt= new list<selectOption>();
        list<Schema.PicklistEntry> values =
            ContentVersion.Marketing_Image_Sub_Type__c.getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values)
        { 
            catOpt.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
        return catOpt;
    }
}