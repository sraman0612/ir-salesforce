global class CC_PavilionCarsPartsHistoryController {

    public String userAccountId { get; set; }
    public String actionId {get;set;}

    /* constructor */
    public CC_PavilionCarsPartsHistoryController() {
    actionId = ApexPages.currentPage().getParameters().get('actionId');
    System.debug('****actionId **'+actionId);
    }
    
    /* Remoteaction Method Used to pass Car Order Records to Page */
    
   @RemoteAction
   global static List<CC_Car_Item__c > getOrderStatusData(String actionId,String userAccountId) {
   List < CC_Car_Item__c > Lstcaritem = new List < CC_Car_Item__c > ();
   String contactId = [SELECT ContactId from User where id = : UserInfo.getUserId()].ContactId;
   Set<Id> oiIdSet=new Set<Id>();
   System.debug('****actionId**'+actionId);
   
   if (contactId != null && contactId != '')
    userAccountId = [Select AccountId from Contact where id = : contactId].AccountId;
    System.debug('****userAccountId**'+userAccountId);
   if (userAccountId != null && userAccountId != '') {

                    String query ='SELECT Id,Order_Item__r.Order__r.CC_Quote_Order_Date__c,Estimated_Ship_Date__c,Order_Item__r.Order__r.CC_Ship_To__c,Order_Item__r.Order__r.Order_Number__c,Order_Item__r.Order__r.CC_CRT_Status__c,Order_Item__r.Order__r.Hold_Code__c,Order_Item__r.Order__r.CC_Quantity__c, ';
                   
                    query += 'Order_Item__r.Order__r.Total_Invoice_Amount__c,Order_Item__r.Order__r.Terms_Code__c,Order_Item__r.Order__r.Recordtype.Name,Order_Item__r.Order__r.CC_Priority_ID__c FROM CC_Car_Item__c Where  Order_Item__r.Order__r.CC_Account__c=:userAccountId and  (Order_Item__r.Order__r.CC_Status__c = \'Open\' OR Order_Item__r.Order__r.CC_Status__c = \'OPEN\') and (Order_Item__r.Order__r.RecordType.Name =\'Parts Ordering\' OR Order_Item__r.Order__r.RecordType.Name =\'Cars Ordering\')';
                    
                     
                     
                      if (actionId == 'FloorPlans') {
                         query += 'and (Order_Item__r.Order__r.CC_Priority_ID__c = \'P\'';
                         query += 'or Order_Item__r.Order__r.CC_Priority_ID__c = \'O\')';
                        } 
                    
                       if (actionId == 'Pending') {
                         query += 'and Order_Item__r.Order__r.Hold_Code__c = \'Y\'';
                        } 
                        
                        if (actionId == 'Approved') {
                         query += 'and Order_Item__r.Order__r.Hold_Code__c = \'N\'';
                        } 
                 
                        
            system.debug(query);           
            Lstcaritem = Database.query(query);
            System.debug('****ListCarItem**'+Lstcaritem);
            }
            return Lstcaritem;
      }
}