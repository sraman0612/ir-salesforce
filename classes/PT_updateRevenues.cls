global class PT_updateRevenues implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
  global final string queryStr;
  
  public PT_updateRevenues(string qry){
    queryStr = qry;
  }
  
  public Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(queryStr);
  }
  
  public void execute(Database.BatchableContext BC, List<PT_Revenue__c> revenues){
    
    PT_Revenue__c revenueToUpdate = revenues[0];
    Date startDate = revenueToUpdate.Date__c.toStartofMonth();
    Date endDate = revenueToUpdate.Date__c.addMonths(1).toStartofMonth().addDays(-1);
    Set <String> eligibleStages = new Set<String>{'2.Qualify','3.Discover','4.Proposal','5.Negotiate'};
    Decimal openRevenue = 0.00;
    Boolean isPriorMonth = startDate <= system.Today() && startDate.Month()!=system.today().Month()?TRUE:FALSE;
    if(!isPriorMonth){
      for(Opportunity o : [SELECT Amount
                             FROM Opportunity 
                            WHERE OwnerId =:revenueToUpdate.OwnerId and
                                  PT_Invoice_Date__c >= :startDate and PT_Invoice_Date__c <= :endDate and
                                  StageName IN :eligibleStages and
                                  Amount != null]){
        openRevenue += o.Amount;
      }
    }
    revenueToUpdate.B_T__c = openRevenue;
    update revenueToUpdate;
  }

  public void finish(Database.BatchableContext BC){}
  
  global void execute(SchedulableContext sc) {
    string qstring = 'SELECT Date__c, B_T__c, OwnerId FROM PT_Revenue__c';
    PT_updateRevenues ptur = new PT_updateRevenues(qstring);
    Database.executebatch(ptur,1);
   }
}