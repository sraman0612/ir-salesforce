@isTest
public class CC_Links_StatementController_Test {
    public static Account acc;
    public Static Contact con;
    public Static User PortalUser;
    public Static Pavilion_Navigation_Profile__c pnp;
    public Static CC_My_Team__c CustFinRep,FinRep;
    public Static CC_Invoice2__c SampleInv1,SampleInv2;
    public Static CC_Statements__c SampleStmt1,SampleStmt2,SampleStmt3,SampleStmt4,SampleStmt5,SampleStmt6;
    public  Static List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();
    public  Static List<CC_Statements__c> stmtLists=new List<CC_Statements__c>();
    
    static{
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
                
        insert psettingList;
    
        //creating test data starts here//
        test.startTest();
        //creating test data starts here//
        acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        System.assertEquals('Sample account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        
        con= TestUtilityClass.createContact(acc.id);
        insert con;
        System.assertNotEquals(null, con.Id);
        
        PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        insert PortalUser;
        System.assertNotEquals(null, PortalUser.Id);
        
        pnp= TestUtilityClass.createPavilionNavigationProfile(PortalUser.CC_Pavilion_Navigation_Profile__c);
        insert pnp;
        
        CustFinRep =TestUtilityClass.createMyTeam('Customer Financing Representative',acc.id,'Customer Finance');
        insert CustFinRep;
        System.assertEquals('Customer Financing Representative', CustFinRep.My_Team_Role__c);
        System.assertNotEquals(null, CustFinRep.Id);
        
        SampleInv1= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test',2000,'14593');
        SampleInv2= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test1',2000,'74593');
        INVLists.add(SampleInv1);
        INVLists.add(SampleInv2);
        SampleStmt1= TestUtilityClass.getStatesmentData(100,'Future',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'14593');
        SampleStmt2= TestUtilityClass.getStatesmentData(100,'Current',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'74593');
        SampleStmt3= TestUtilityClass.getStatesmentData(100,'61-90',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'74533');
        SampleStmt4= TestUtilityClass.getStatesmentData(100,'91-120',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'74531');
        SampleStmt5= TestUtilityClass.getStatesmentData(100,'31-60',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'74532');
        SampleStmt6= TestUtilityClass.getStatesmentData(100,'1-30',System.today(),'Damaged delivery',System.today(),'123',acc.Id,'74537');
        stmtLists.add(SampleStmt1);
        stmtLists.add(SampleStmt2);
        stmtLists.add(SampleStmt3);
        stmtLists.add(SampleStmt4);
        stmtLists.add(SampleStmt5);
        stmtLists.add(SampleStmt6);
        //system.debug(INVList);
        //system.debug(stmtList);
        System.assertNotEquals(null, INVLists);
        System.assertNotEquals(null, stmtLists);
        insert INVLists;
        insert stmtLists;
        System.assertNotEquals(null, INVLists);
        System.assertNotEquals(null, stmtLists);
        //creating test data ends here//
        test.stopTest(); 
        //creating test data ends here//
        
    }
    
    static testMethod void callRemoteMethods()
    {
        System.runAs(PortalUser)
        {
            CC_PavilionTemplateController controller =new CC_PavilionTemplateController();
            controller.acctid= acc.id;
            controller.CustomerNumber='12345';
            controller.AccountName=acc.Name;
            CC_Links_StatementController objstatement1= new CC_Links_StatementController(controller);
            objstatement1.controllerFunction();
            CC_Links_StatementController.getStatementData(acc.id);
            CC_Links_StatementController.getSelectedInvoiceData(INVLists[0].Name);
            string key=INVLists[0].Name+':'+'500'+':'+'Damaged delivery'+':'+'text1'+';';
            CC_Links_StatementController.sendInvAndGetCase(acc.id,key);
            
     }
     
     }
}