@isTest
public class CC_PavilionDownloadPricingUtilCtrlTest {

     static testMethod void constructorTest() {        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;
        
        Account acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        System.assertEquals('Sample account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        
        Contact con= TestUtilityClass.createContact(acc.id);
        con.CC_Pavilion_Admin__c=TRUE;
        insert con;
        System.assertNotEquals(null, con.Id);
        System.assertEquals(true,con.CC_Pavilion_Admin__c);
        
        //User thisUser=[Select id from user where Id=:UserInfo.getUserId() limit 1];
        User PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        PortalUser.CC_Pavilion_Navigation_Profile__c='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN';
        PortalUser.Pavilion_Tiles__c='Sample;Tiles';
        insert PortalUser;
        System.assertNotEquals(null, PortalUser.Id); 
        
        //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true limit 1];
        //String pbId=Test.getStandardPricebookId();
        Product2 prod = TestUtilityClass.createProduct2();
        prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        insert prod;
        
        Pricebook2 custpb = TestUtilityClass.createPriceBook2();
        custpb.Name = 'PLIST';        
        insert custpb;
        
        PricebookEntry pde =new PricebookEntry(Pricebook2Id=custpb.id,Product2Id=prod.id,UnitPrice = 1000, 
                                              IsActive = true);
        insert pde;
        /*PricebookEntry pbEntry1 = TestUtilityClass.createPriceBookEntryStandard(standardPB.Id,prod.id);
        insert pbEntry1 ;*/
        
        Pricebook2 pb = TestUtilityClass.createPriceBook2();
        insert pb;
        
        PricebookEntry pbEntry = TestUtilityClass.createPriceBookEntry(pb.id,prod.Id);
        insert pbEntry;
       
        
        Test.startTest();
        
        System.runAs(PortalUser) {
            CC_PavilionTemplateController controller;
            CC_PavilionDownloadPricingUtilCtrl ccDwldPricingCtrl  = new CC_PavilionDownloadPricingUtilCtrl(controller);
            String xlsHder = ccDwldPricingCtrl.XlsHeader ;
            List<PricebookEntry> pbEntryList = new List<PricebookEntry>(); 
            pbEntryList.add(pbEntry);
            ccDwldPricingCtrl.customerPriceBookEntries = pbEntryList;
            
        }
        
        
        Test.stopTest(); 
     }   
}