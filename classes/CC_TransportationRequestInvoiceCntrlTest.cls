@isTest
public with sharing class CC_TransportationRequestInvoiceCntrlTest {

    @TestSetup
    private static void createData(){
        
        Account a = TestUtilityClass.createAccount();
        a.ShippingStreet = '1234 Main Street';
        a.ShippingCity = 'Indianapolis';
        a.ShippingState = 'IN';
        a.ShippingPostalCode = '46175';
        a.ShippingCountry = 'USA';
        insert a;    
        
        Case c = TestUtilityClass.createCaseWithRecordType(null, 'Transportation Request', 'Club Car - Transportation Request');
        c.Status = 'New';
        c.AccountId = a.Id;
        c.CC_Quoted_Price__c = 5000;
        insert c;

        insert TestUtilityClass.createProduct2WithProductCode('BACKHAUL UC');
    }

    @isTest
    private static void testCreateOrderInvoiceSuccess(){

        Case c = [Select Id, AccountId, Status, CC_Order__c, CC_Quoted_Price__c From Case LIMIT 1];
        Account a = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account LIMIT 1];

        system.assertEquals('New', c.Status);
        system.assertEquals(a.Id, c.AccountId);
        system.assertEquals(null, c.CC_Order__c);
        system.assertEquals(5000, c.CC_Quoted_Price__c);

        Test.startTest();

        LightningResponseBase response = CC_TransportationRequestInvoiceCntrl.createOrderInvoice(c.Id);

        system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);

        c = [Select Id, AccountId, Status, CC_Order__c, CC_Quoted_Price__c From Case LIMIT 1];
        system.assertEquals('Invoiced', c.Status);
        system.assert(c.CC_Order__c != null);        

        CC_Order__c o = [Select Id, isSystemCreated__c, CC_Account__c, CC_Status__c, PO__c, RecordTypeId, CC_Address_line_1__c, CC_Addressee_name__c,
                CC_City__c, CC_States__c, CC_Postal_code__c, CC_Country__c, CC_Request_Date__c, CC_Warehouse__c
             From CC_Order__c 
             Where Id = :c.CC_Order__c];

        system.assertEquals(True, o.isSystemCreated__c);
        system.assertEquals(a.Id, o.CC_Account__c);
        system.assertEquals('Submitted', o.CC_Status__c);
        system.assertEquals('BACKHAUL ORDER-*', o.PO__c);
        system.assertEquals([SELECT Id FROM RecordType WHERE SObjectType = 'CC_Order__c' AND DeveloperName = 'CC_Parts_Ordering'].Id, o.RecordTypeId);
        system.assertEquals(a.ShippingStreet, o.CC_Address_line_1__c);
        system.assertEquals(a.Name, o.CC_Addressee_name__c);
        system.assertEquals(a.ShippingCity, o.CC_City__c);
        system.assertEquals(a.ShippingState, o.CC_States__c);
        system.assertEquals(a.ShippingPostalCode, o.CC_Postal_code__c);
        system.assertEquals(a.ShippingCountry, o.CC_Country__c);
        system.assertEquals(system.today(), o.CC_Request_Date__c);
        system.assertEquals('1', o.CC_Warehouse__c);        

        Product2 p = [Select Id, Description From Product2 Where ProductCode = 'BACKHAUL UC'];

        CC_Order_Item__c oli = [Select CC_Quantity__c, CC_Product_Code__c, Product__c, CC_Description__c, CC_UnitPrice__c, CC_TotalPrice__c, RecordTypeId
            From CC_Order_Item__c 
            Where Order__c = :o.Id];

        system.assertEquals(1, oli.CC_Quantity__c);
        system.assertEquals('BACKHAUL UC', oli.CC_Product_Code__c);            
        system.assertEquals(p.Id, oli.Product__c); 
        system.assertEquals(p.Description, oli.CC_Description__c);
        system.assertEquals(c.CC_Quoted_Price__c, oli.CC_UnitPrice__c);
        system.assertEquals(oli.CC_UnitPrice__c, oli.CC_TotalPrice__c);
        system.assertEquals([SELECT Id FROM RecordType WHERE SObjectType = 'CC_Order_Item__c' AND DeveloperName = 'CC_Parts_Ordering'].Id, oli.RecordTypeId);        

        Test.stopTest();
    }

    @isTest
    private static void testCreateOrderInvoiceNoQuotedPrice(){

        Case c = [Select Id, AccountId, Status, CC_Order__c, CC_Quoted_Price__c From Case LIMIT 1];
        Account a = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account LIMIT 1];

        system.assertEquals('New', c.Status);
        system.assertEquals(a.Id, c.AccountId);
        system.assertEquals(null, c.CC_Order__c);
        system.assertEquals(5000, c.CC_Quoted_Price__c);

        c.CC_Quoted_Price__c = null;
        update c;

        Test.startTest();

        LightningResponseBase response = CC_TransportationRequestInvoiceCntrl.createOrderInvoice(c.Id);

        system.assertEquals(false, response.success);
        system.assert(response.errorMessage.contains('Quoted Price'));

        c = [Select Id, AccountId, Status, CC_Order__c From Case LIMIT 1];
        system.assertEquals('New', c.Status);
        system.assert(c.CC_Order__c == null);

        Test.stopTest();
    }    

    @isTest
    private static void testCreateOrderInvoiceNoAccount(){

        Case c = [Select Id, AccountId, Status, CC_Order__c, CC_Quoted_Price__c From Case LIMIT 1];
        Account a = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account LIMIT 1];

        system.assertEquals('New', c.Status);
        system.assertEquals(a.Id, c.AccountId);
        system.assertEquals(null, c.CC_Order__c);
        system.assertEquals(5000, c.CC_Quoted_Price__c);

        c.AccountId = null;
        update c;

        Test.startTest();

        LightningResponseBase response = CC_TransportationRequestInvoiceCntrl.createOrderInvoice(c.Id);

        system.assertEquals(false, response.success);
        system.assert(response.errorMessage.contains('Account'));

        c = [Select Id, AccountId, Status, CC_Order__c From Case LIMIT 1];
        system.assertEquals('New', c.Status);
        system.assert(c.CC_Order__c == null);

        Test.stopTest();
    }   
    
    @isTest
    private static void testCreateOrderInvoiceAlreadyInvoiced(){

        Case c = [Select Id, AccountId, Status, CC_Order__c, CC_Quoted_Price__c From Case LIMIT 1];
        Account a = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry From Account LIMIT 1];

        system.assertEquals('New', c.Status);
        system.assertEquals(a.Id, c.AccountId);
        system.assertEquals(null, c.CC_Order__c);
        system.assertEquals(5000, c.CC_Quoted_Price__c);

        c.Status = 'Invoiced';
        update c;

        Test.startTest();

        LightningResponseBase response = CC_TransportationRequestInvoiceCntrl.createOrderInvoice(c.Id);

        system.assertEquals(false, response.success);
        system.assertEquals('This case has already been invoiced.', response.errorMessage);

        c = [Select Id, AccountId, Status, CC_Order__c From Case LIMIT 1];
        system.assert(c.CC_Order__c == null);

        Test.stopTest();
    }     
}