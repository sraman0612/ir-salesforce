// (c) 2015, Appirio Inc.
// Handler Class for Opportunity Trigger
// April 16,2015    Surabhi Sharma    T-377640
// Nov. 29, 2016    Ben Lorenz   Merged in class OppyTriggerHandler and logic from body of AIRupdateoppyquota
// Feb. 23, 2020    Ben Lorenz   Default currency and bill to for NA Air opps
// December,2024    Mansi Agnihotri  Added a method to share opportunoity records with channel partner

public class Opportunity_TriggerHandler{
  //sales_rep_quota__c field Commented by Yadav Vineet capgemini for descoped field

  public static Boolean isFirstTime = true;
  public static Final String ROW_CAUSE = 'Manual';
  public static Final String OPPSHARE_ACCESS_LEVEL = 'Edit';
  public static void beforeInsertUpdate(List<Opportunity> oppLst, Boolean isInsert, Map<Id,Opportunity> oldMap){ 
    if(Opportunity_TriggerHandler.isFirstTime) { 
      Opportunity_TriggerHandler.isFirstTime = false;
      if(!oppLst.IsEmpty()){
        string yearStr = string.valueof(System.Today().year());
        Map<Id,Id> ownerquotamap = new Map<Id,Id>();
        Map<Id, String> accountMap = new Map<Id, String>();
        for(Opportunity o : oppLst){
          ownerquotamap.put(o.OwnerId,null);
          accountMap.put(o.AccountId,null);
            //commented as a part of EMEIA-17
            /*
          if(!isInsert && null != o.CTS_BillToAccount__c){
            accountMap.put(o.CTS_BillToAccount__c,null);
          }*/
        }
        for(Account a : [SELECT Id, Bill_To_Account__c, Bill_To_Account__r.Currency__c, Currency__c FROM Account WHERE ID IN :accountMap.keyset()]){
          if(isInsert && null != a.Bill_To_Account__c){
            accountMap.put(a.Id,a.Bill_To_Account__c + '::' + a.Bill_To_Account__r.Currency__c);
          } else if (!isInsert) {
            accountMap.put(a.Id,a.Currency__c);
          } else {
            accountMap.remove(a.Id);
          }
        }
        /*for(AIR_Rep_Quota__c o2 : [SELECT Id, Sales_Rep__c 
                                   FROM AIR_Rep_Quota__c 
                                   WHERE Sales_Rep__c IN: ownerquotamap.keySet() and Year__c = : yearStr and activate__c = true]){
          ownerquotamap.put(o2.Sales_Rep__c,o2.id);
        }*/
        for(Opportunity opp : oppLst){
          //opp.Sales_Rep_Quota__c = ownerquotamap.get(opp.OwnerId);
          system.debug('#### acct map is ' + accountMap);
          if(isInsert && null != opp.AccountId && accountMap.keyset().contains(opp.AccountId)){
            String [] acctArr = accountMap.get(opp.AccountId).split('::');
            system.debug('### info is ' +  acctArr);
               //commented as a part of EMEIA-17
          //  opp.CTS_BillToAccount__c = acctArr[0];
            opp.CurrencyISOCode = acctArr[1];
            //code commented by Yadav Vineet Capgemini for descoped field as per Prashanth 
            //opp.CTS_OVERRIDE_BILL_TO__c = TRUE;
          } 
             //commented as a part of EMEIA-17
            /*
             * else if(!isInsert && accountMap.keyset().contains(opp.CTS_BillToAccount__c) && (opp.CTS_BillToAccount__c != oldMap.get(opp.Id).CTS_BillToAccount__c))  {
            opp.CurrencyISOCode = accountMap.get(opp.CTS_BillToAccount__c);*/

          }
        }  
      }
    }
 

  public static void afterUpdate(List<Opportunity> newList){ 
      if(!newList.IsEmpty()){
        UtilityClass.deleteApprovedRecords(newList,'Opportunity',UtilityClass.GENERIC_APPROVAL_API_NAME);
      }
  }
     /*------------------------------------------------------------------------------------------------------------
      Author : Mansi Agnihotri
      Date : 10/12/2024
      Description : This method shares the Opportunity
records with the user currently assigned as channel partner sales manager
                    and removes access for the previously assigned channel partner sales manager
      Case : 03339512
    --------------------------------------------------------------------------------------------------------------*/
    public static void shareOppsWithChannelPartner(Map<Id,Opportunity> newOppMap, Map<Id, Opportunity> oldOppMap){
        Id previousChannelPartnerUserId;
        List<OpportunityShare> lstOppShareRecordsToInsert = new List<OpportunityShare>();
        List<OpportunityShare> lstOppShareRecordsToDelete = new List<OpportunityShare>();
        map<Id,OpportunityShare> mapPreviousUserVsOpp = new map<Id,OpportunityShare>();
        for(OpportunityShare OppShareVar:[Select Id,UserOrGroupId,OpportunityId,OpportunityAccessLevel from OpportunityShare where
                                                               OpportunityId IN:newOppMap.keySet()]){
                mapPreviousUserVsOpp.put(OppShareVar.UserOrGroupId,OppShareVar);
         }
        for(Opportunity oppVar:newOppMap.values()){
            if(
               oldOppMap.get(oppVar.Id).Channel_Partner_Sales_Manager__c!=oppVar.Channel_Partner_Sales_Manager__c
               && !mapPreviousUserVsOpp.containsKey(oppVar.Channel_Partner_Sales_Manager__c)              
               && oldOppMap.get(oppVar.Id).OwnerId!=oppVar.Channel_Partner_Sales_Manager__c
            ){
                previousChannelPartnerUserId = oldOppMap.get(oppVar.Id).Channel_Partner_Sales_Manager__c;
                   OpportunityShare oShare = new OpportunityShare() ;
                   oShare.OpportunityAccessLevel = OPPSHARE_ACCESS_LEVEL;
                   oShare.OpportunityId = oppVar.Id;
                   oShare.UserOrGroupId = oppVar.Channel_Partner_Sales_Manager__c;
                   oShare.RowCause = ROW_CAUSE;
                   lstOppShareRecordsToInsert.add(oShare);
                   if(mapPreviousUserVsOpp.containsKey(previousChannelPartnerUserId)){
                      lstOppShareRecordsToDelete.add(mapPreviousUserVsOpp.get(previousChannelPartnerUserId));
                    }
            }
        }
         Database.SaveResult[] listSaveResults = Database.insert(lstOppShareRecordsToInsert,false);
        try{
            if(lstOppShareRecordsToDelete!=null){
                Delete lstOppShareRecordsToDelete;
            }
         }
         catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
         }
    }
}