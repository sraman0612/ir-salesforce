/**********************************************************************************
* Name		: CC_ViewAssetDetailsCnt
* Req#		: Add CC CE Certification to CC Links
* Purpose	: Add CC CE Certification to CC Links
* History	:
* ===========
* VERSION	AUTHOR				     DATE		   DETAIL
* 	1.0		TCS(Priyanka Baviskar)	04/12/2019	Initial Development
************************************************************************************/
public class CC_ViewAssetDetailsCnt {
    
    public CC_ViewAssetDetailsCnt(CC_PavilionTemplateController controller) {
        
    }
    
/**********************************************************************
* Purpose		: Add CC CE Certification to CC Links
* Parameters	: CC Car Order Id
* Returns		: Asset Detail 'lstpreapprequest' list 
***********************************************************************/
    @RemoteAction
    public static List<CC_Asset__c> getAssset(String ordid)
    {
        
        List<CC_Asset__c> lstpreapprequest = [SELECT Name, Serial__c,Account__c,Account__r.name,Order__r.Order_Number__c,Order__c,(SELECT Id FROM Attachments where name like 'CE_CERT%') FROM CC_Asset__c
                                              WHERE Order__c =: ordid];
        
        return lstpreapprequest;
    }
    
}