@isTest
public class ContractRejectControllerTest {
	@isTest
    public static void testInvoke(){
        Account aroAccount = new Account();
        aroAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ARO_Account').getRecordTypeId();
        aroAccount.ShippingCity = 'Test City1 ';
        aroAccount.ShippingCountry = 'US';
        aroAccount.ShippingPostalCode = '658776';
        aroAccount.ShippingStreet = 'test street';
        aroAccount.Name = 'Test ARO Account';
        aroAccount.Type = 'Prospect';
        aroAccount.CTS_Channel__c = 'NIS';
        insert aroAccount; 
        String aroContractRT = [SELECT Id FROM RecordType WHERE SobjectType = 'Contract' AND DeveloperName = 'ARO_Contract' LIMIT 1].Id;
        Contract contr = new Contract(Name = 'Test Contract 1',Status= 'Draft',Account_Type__c = 'NIS',AccountId = aroAccount.Id, RecordTypeId = aroContractRT);
        insert contr;
        
        Test.startTest();
        List<ContractRejectController.FlowInput> testInputList = new List<ContractRejectController.FlowInput>();
        List<ContractRejectController.FlowOutput> testOutputList = new List<ContractRejectController.FlowOutput>();
        ContractRejectController.FlowInput testInput = new ContractRejectController.FlowInput();
        testInput.ContractId = contr.Id;
       	testInput.operatingMode = 'Serialize';
        testInputList.add(testInput);
        testOutputList = ContractRejectController.invoke(testInputList);
        contr.Prior_Contract_JSON__c = testOutputList[0].jsonOutputString;
       	update contr;
        System.debug(contr);
        testInput.operatingMode = 'Deserialize';
        ContractRejectController.invoke(testInputList);
        Test.stopTest();
        
    }
}