public with sharing class FileUploadInvocable {

    public class InputVariables {
        @InvocableVariable(Required = true)
        public String contentVersionId;

        @InvocableVariable(Required = true)
        public String recordId;
    }

    @InvocableMethod(label = 'Attach file to record invocable method' Description = 'To attach the provided content document with the record')
    public static void attachFileToRecord(List<InputVariables> inputVariables) {

        for (InputVariables variables : inputVariables) {
            FileUploadAtCheckoutController.createContentLink(variables.contentVersionId, variables.recordId);
        }
    }

}