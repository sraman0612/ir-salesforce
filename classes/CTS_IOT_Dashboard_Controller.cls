public with sharing class CTS_IOT_Dashboard_Controller {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
    }

    @AuraEnabled
	public static InitResponse getInit(){
		return new InitResponse();
	}   

    public class AssetInitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public CTS_IOT_Data_Service.AssetDetail assetDetail = new CTS_IOT_Data_Service.AssetDetail();
    }

    @AuraEnabled
	public static AssetInitResponse getAssetInit(Id assetId){

        AssetInitResponse response = new AssetInitResponse();

        try{
            response.assetDetail = CTS_IOT_Data_Service.getCommunityUserAsset(assetId);
        }
        catch (Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

		return response;
	}    

    public class SiteInitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public Account site = new Account();
        @AuraEnabled public Boolean hasConnectedAssets = false;
    }    

    @AuraEnabled
	public static SiteInitResponse getSiteInit(Id siteId){

        SiteInitResponse response = new SiteInitResponse();

        try{
            response.site = CTS_IOT_Data_Service.getCommunityUserSites(siteId)[0];
            response.hasConnectedAssets = CTS_IOT_Site_Helix_MessageController.hasConnectedAssets(siteId);
        }
        catch (Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

		return response;
	}         
}