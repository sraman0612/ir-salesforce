// Handler Class for OpportunitySalesCallPlan_Trigger
//
// Created on April 16, 2019 by Ronnie Stegall

public class OpportunitySalesCallPlan_TriggerHandler {
    
    public static void beforeUpdate(List<Opportunity_Sales_Call_Plan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
    }
    
    public static void beforeInsert(List<Opportunity_Sales_Call_Plan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
        // Sales call plan map discovery section on clone
        updateDiscoverySectionOnClone(newList);
    }
    
    public static void afterInsert(List<Opportunity_Sales_Call_Plan__c> newList)
    {
        salesCallPlanNotesLastMeetingUpdate(newList);
    }
    
    public static void salesCallPlanNotesLastMeetingUpdate (List<Opportunity_Sales_Call_Plan__c> newList){
        map<Id, List<Opportunity_Sales_Call_Plan__c>> salesCallPlanByOppId = new map<Id, List<Opportunity_Sales_Call_Plan__c>>();
        List<Opportunity_Sales_Call_Plan__c> discardedSalesCallPlan = new List<Opportunity_Sales_Call_Plan__c>();
                
        for(Opportunity_Sales_Call_Plan__c currRecord : newList){
            //Will look under the most recent sales call plan assigned to opportunity
            if(!salesCallPlanByOppId.containsKey(currRecord.Opportunity__c)){
                List<Opportunity_Sales_Call_Plan__c> salesCallPlanList = new List<Opportunity_Sales_Call_Plan__c>();
                salesCallPlanList.add(currRecord);
                salesCallPlanByOppId.put(currRecord.Opportunity__c, salesCallPlanList);
                discardedSalesCallPlan.add(currRecord);
            }else{
                salesCallPlanByOppId.get(currRecord.Opportunity__c).add(currRecord);
                discardedSalesCallPlan.add(currRecord);
            }
        }
        
        if(salesCallPlanByOppId.size() > 0){            
            List<Opportunity_Sales_Call_Plan__c> salesCallPlanListCloned = [SELECT Id, createdDate, Opportunity__c FROM Opportunity_Sales_Call_Plan__c where Opportunity__c IN:salesCallPlanByOppId.keyset() AND Id NOT IN:discardedSalesCallPlan order by createdDate DESC];   
            
            if(salesCallPlanListCloned.size() > 0){
                map<Id, id> oppIdByMostRecentSalesCallPlanId = new map<Id, Id>();
                
                for(Opportunity_Sales_Call_Plan__c currSalesCallPlan : salesCallPlanListCloned){
                    if(!oppIdByMostRecentSalesCallPlanId.containsKey(currSalesCallPlan.Opportunity__c)){
                        oppIdByMostRecentSalesCallPlanId.put(currSalesCallPlan.Opportunity__c, currSalesCallPlan.Id);
                    }
                }
                                
                //Get the list of related records to the following ContentVersion
                List<ContentDocumentLink> cdlist = [SELECT ContentDocumentId, LinkedEntityId, SystemModstamp FROM ContentDocumentLink WHERE LinkedEntityId IN:oppIdByMostRecentSalesCallPlanId.values() order by SystemModstamp DESC];
                map<Id, set<Id>> salesCallPlanByContDocId = new map<Id, set<Id>>();
                                
                for(ContentDocumentLink currDocLink : cdlist){
                    set<Id> localList = new set<Id> ();
                    if(!salesCallPlanByContDocId.containsKey(currDocLink.LinkedEntityId)){
                        localList.add(currDocLink.ContentDocumentId);
                        salesCallPlanByContDocId.put(currDocLink.LinkedEntityId, localList);
                    }else{
                        salesCallPlanByContDocId.get(currDocLink.LinkedEntityId).add(currDocLink.ContentDocumentId);
                    }
                }
                
                if(salesCallPlanByContDocId.size() > 0 ){                    
                    //Gathers the content version fields sorted by newest version
                    List<ContentVersion> cvList = [SELECT Id, versiondata, ContentDocumentId, FileType FROM ContentVersion where ContentDocumentId IN:salesCallPlanByContDocId.values()[0] AND FileType = 'SNOTE' order by VersionNumber DESC];
                    
                    map<Id, Blob> contentIdByRespectiveBlob = new map<Id, Blob>();
                    
                    for(ContentVersion currVersion : cvList){
                        contentIdByRespectiveBlob.put(currVersion.ContentDocumentId, currVersion.VersionData);
                    }
                    
                    map<Id, Id> finalOpportunityByContentDocId = new map<Id, Id>();
                    
                    for(Id oppId : salesCallPlanByContDocId.keySet()){
                        for(Id docId : salesCallPlanByContDocId.get(oppId)){
                            if(contentIdByRespectiveBlob.keyset().contains(docId)){
                                finalOpportunityByContentDocId.put(oppId, docId);
                            }
                        }
                    }
                    
                    //Gather all content documents that are linked to sales call plan
                    List<ContentDocument> contentDocumentList = [SELECT Id, CreatedDate FROM ContentDocument where Id IN:contentIdByRespectiveBlob.keySet()];
                    
                    //Stores content document id with created date
                    map<Id, String> contectDocIdByCreatedDate = new map<Id, String>();
                    
                    //Stores targeted document id with respective created date
                    for(ContentDocument ctVar: contentDocumentList ){
                        if(!contectDocIdByCreatedDate.keyset().contains(ctVar.Id)){
                            contectDocIdByCreatedDate.put(ctVar.Id, String.valueOf(ctVar.CreatedDate));
                        }
                    }
                        
                    if(contentIdByRespectiveBlob.size() > 0){
                    //Avoiding System.FinalException: Record is read-only
                    List<Opportunity_Sales_Call_Plan__c> targetSalesCallPlanCloned = [SELECT Id, name, Notes_From_Last_Meeting__c, createdDate, Opportunity__c FROM Opportunity_Sales_Call_Plan__c where Id IN:salesCallPlanByOppId.values()[0]];   
                    for(Id currId : salesCallPlanByOppId.keySet()){
                        for(Opportunity_Sales_Call_Plan__c currSalesPlan : targetSalesCallPlanCloned){
                            String textPreview = contentIdByRespectiveBlob.get(finalOpportunityByContentDocId.get(oppIdByMostRecentSalesCallPlanId.get(currSalesPlan.Opportunity__c))).toString();
                            textPreview = textPreview.replaceAll( '<[^>]+>',' ' );
                            textPreview = textPreview.unescapeHtml4();
                            currSalesPlan.Notes_From_Last_Meeting__c = contectDocIdByCreatedDate.get(finalOpportunityByContentDocId.get(oppIdByMostRecentSalesCallPlanId.get(currSalesPlan.Opportunity__c))) + ': ' + textPreview;
                        }
                    }
                    update targetSalesCallPlanCloned;
                }
                }
                
            }
            
        }
    }
    
    public static void updateDiscoverySectionOnClone (List<Opportunity_Sales_Call_Plan__c> newList){
        map<Id, Opportunity_Sales_Call_Plan__c> sourceClonedByClonedRecords = new map<Id, Opportunity_Sales_Call_Plan__c>();
        for(Opportunity_Sales_Call_Plan__c currRecord : newList){
            if(currRecord.isClone()){
                sourceClonedByClonedRecords.put(currRecord.getCloneSourceId(), currRecord);
            }

        }
        
        List<Opportunity_Sales_Call_Plan__c> salesCallPlanClonedReferenceList = [SELECT id, Discovery__c FROM Opportunity_Sales_Call_Plan__c WHERE id IN:sourceClonedByClonedRecords.keySet()];
        
        for(Opportunity_Sales_Call_Plan__c scpRecord : salesCallPlanClonedReferenceList){
            sourceClonedByClonedRecords.get(scpRecord.Id).Discovery__c = scpRecord.Discovery__c;
        }
        
    }
    
}