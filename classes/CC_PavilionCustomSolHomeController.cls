public class CC_PavilionCustomSolHomeController {

    public CC_PavilionCustomSolHomeController(CC_PavilionTemplateController controller) {

    }  
    Public class ContentHistory{
        Public String versionId{get;set;}
        Public String count{get;set;}
        Public ContentHistory(String vId,String cnt)
        {
           versionId=vId;
           count=cnt; 
        }
    }
   Public List<ContentHistory> ContHist=new List<ContentHistory>();
    Public List<ContentHistory> getContentHist()
    {
       AggregateResult[] conAgr =[SELECT ContentVersionId,COUNT(Id) FROM ContentVersionHistory WHERE Field='contentVersionDownloaded' AND ContentVersion.RecordType.Name='Custom Solutions'
                                                   GROUP BY ContentVersionId ORDER By COUNT(ID) DESC LIMIT 5];
       for (AggregateResult histList : conAgr) {
             ContHist.add(new ContentHistory(String.ValueOf(histList.get('ContentVersionId')),String.ValueOf(histList.get('expr0'))));  
        }
        return ContHist;
    }
    
   
}