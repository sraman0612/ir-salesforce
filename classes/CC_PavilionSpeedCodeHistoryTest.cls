@istest 
public with sharing class CC_PavilionSpeedCodeHistoryTest
{   
    @testSetup
    private static void createData(){

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>(); 
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;

        Account acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        
        Contact con= TestUtilityClass.createContact(acc.id);
        insert con;
        
        User PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        insert PortalUser;

        List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();

        CC_Invoice2__c SampleInv1= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test',2000,'14593');
        SampleInv1.CC_Item_Number__c='PASSCODE';
        SampleInv1.CC_Order_Number__c='12345';

        CC_Invoice2__c SampleInv2= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test',2000,'14593');
        SampleInv2= TestUtilityClass.getTestInvoiceData(acc.Id,1,'12345','Sample test1',2000,'74593');
        SampleInv2.CC_Item_Number__c='PASSCODE';
        SampleInv1.CC_Order_Number__c='54321';
        INVLists.add(SampleInv1);
        INVLists.add(SampleInv2);
       
        insert INVLists;
                
        CC_SpeedCodeHistory__c objH = new CC_SpeedCodeHistory__c();
        objH.CC_Customer_Name__c='TestCustName';        
        objH.CC_Purchase_Order_Number__c='123456';
        objH.Name='TestName';
        objH.CC_Speed_Code_A__c=1;
        objH.CC_Speed_Code_B__c=2;
        objH.CC_Speed_Code_C__c=3;
        objH.CC_Controller_Serial_Numbr__c='23568978';
        objH.CC_Account__c=acc.Id;
        objH.CC_Order_Date__c=system.today();
        insert objH;

        Product2 prod2 = TestUtilityClass.createProduct2();
        prod2.ProductCode='PASSCODE';
        insert prod2;
    }
    
    private static testmethod void speedCodeHistoryMethod(){

        CC_SpeedCodeHistory__c history = 
            [Select Id, CC_Order_Number__r.Name,CC_Order_Number__r.CC_MAPICS_Order_Key__c,CC_Order_Number__r.Order_Number__c,CC_Order_Date__c,CC_Customer_Name__c,
                CC_Purchase_Order_Number__c,Name,CC_Controller_Serial_Numbr__c,CC_Account__r.CC_Customer_Number__c,CC_Order_Number__r.CC_Order_Number_Reference__c
            From CC_SpeedCodeHistory__c];

        Account acc = [Select Id From Account Where Id = :history.CC_Account__c];

        Test.startTest();

        CC_PavilionTemplateController controller;
        CC_PavilionSpeedCodeHistoryController objcnt = new CC_PavilionSpeedCodeHistoryController(controller);  
        CC_PavilionSpeedCodeHistoryController.getspeedcodehistory(acc.id);
        CC_PavilionSpeedCodeHistoryController.schWrapper twrp = new CC_PavilionSpeedCodeHistoryController.schWrapper(history);

        Test.stopTest();
    }
}