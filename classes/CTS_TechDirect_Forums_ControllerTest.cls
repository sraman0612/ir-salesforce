@isTest
public with sharing class CTS_TechDirect_Forums_ControllerTest {
    
    private static testMethod void testGetInitSuccess(){
    
    	CTS_TechDirect_Community_Forums_TC__c settings = CTS_TechDirect_Community_Forums_TC__c.getOrgDefaults();
    	User userRecord = [Select Id, CTS_TechDirect_T_C_Accepted__c, CTS_TechDirect_T_C_Accepted_Version__c, CTS_TechDirect_T_C_Accepted_DateTime__c, CTS_TechDirect_T_C_Accepted_IP__c From User Where Id = :UserInfo.getUserId()];    
    	String responseStr = CTS_TechDirect_Forums_Controller.getInit();
    	
    	CTS_TechDirect_Forums_Controller.Init response = (CTS_TechDirect_Forums_Controller.Init)JSON.deserialize(responseStr, CTS_TechDirect_Forums_Controller.Init.class);
    	
		system.assertEquals(settings, response.termsForumSettings);
		system.assertEquals(userRecord.Id, response.userRecord.Id);
		system.assertEquals(Label.CTS_TechDirect_ChatterForum_Heading, response.splashHeading);
		system.assertEquals(Label.CTS_TechDirect_ChatterForum_Terms + Label.CTS_TechDirect_ChatterForum_Terms2, response.splashTerms);
		system.assertEquals(Label.CTS_TechDirect_ChatterForum_Instructions, response.splashInstructions);
		system.assertEquals(Label.CTS_TechDirect_ChatterForum_AcceptButton, response.splashAcceptButtonLabel);    	
    }
    
    private static testMethod void testGetInitFail(){

		CTS_TechDirect_Forums_Controller.forceError = true;
		
    	String responseStr = CTS_TechDirect_Forums_Controller.getInit();
    	
    	CTS_TechDirect_Forums_Controller.Init response = (CTS_TechDirect_Forums_Controller.Init)JSON.deserialize(responseStr, CTS_TechDirect_Forums_Controller.Init.class);
    	
    	system.assertEquals(false, response.success);	
    	system.assert(String.isNotBlank(response.errorMessage));	
    }    
    
    private static testMethod void testAcceptTermsSuccess(){
    	
    	String responseStr = CTS_TechDirect_Forums_Controller.acceptTerms();
    	
    	CTS_TechDirect_Forums_Controller.ResponseBase response = (CTS_TechDirect_Forums_Controller.ResponseBase)JSON.deserialize(responseStr, CTS_TechDirect_Forums_Controller.ResponseBase.class);    	
    	
    	system.assertEquals(true, response.success);
    	system.assertEquals('', response.errorMessage);
    	
    	User updatedUser = [Select CTS_TechDirect_T_C_Accepted__c, CTS_TechDirect_T_C_Accepted_Version__c, CTS_TechDirect_T_C_Accepted_DateTime__c, CTS_TechDirect_T_C_Accepted_IP__c From User Where Id = :UserInfo.getUserId()];
    	LoginHistory LastLogin = [Select SourceIp, APIType, Browser, LoginTime From LoginHistory Where UserId = :UserInfo.getUserId() Order By LoginTime DESC LIMIT 1];
    	
		system.assertEquals(true, updatedUser.CTS_TechDirect_T_C_Accepted__c);
		system.assertEquals(CTS_TechDirect_Community_Forums_TC__c.getOrgDefaults().Current_Version__c, updatedUser.CTS_TechDirect_T_C_Accepted_Version__c);
		system.assert(updatedUser.CTS_TechDirect_T_C_Accepted_DateTime__c >= DateTime.now().addMinutes(-1));
		system.assertEquals(LastLogin.SourceIp, updatedUser.CTS_TechDirect_T_C_Accepted_IP__c);    	
    }
    
    private static testMethod void testAcceptTermsFail(){

		CTS_TechDirect_Forums_Controller.forceError = true;
		
    	String responseStr = CTS_TechDirect_Forums_Controller.acceptTerms();
    	
    	CTS_TechDirect_Forums_Controller.ResponseBase response = (CTS_TechDirect_Forums_Controller.ResponseBase)JSON.deserialize(responseStr, CTS_TechDirect_Forums_Controller.ResponseBase.class);
    	
    	system.assertEquals(false, response.success);	
    	system.assert(String.isNotBlank(response.errorMessage));	
    }       
}