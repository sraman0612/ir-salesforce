@isTest
public with sharing class AssignmentGroupControllerTest {
    
    @isTest
    public static void testInvoke(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(200));
        List<Lead> testLeads= new List<Lead>();
        for(Integer i = 0; i <= 2; i++){
            Lead testLead= new Lead();
            testLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Standard_Lead').getRecordTypeId();
            testLead.FirstName = 'test';
            testLead.LastName = 'lead ';
            
            testLead.Assigner_Comments__c = 'Test Assigner Comments ';
            testLead.Lead_Source_1__c = 'Marketing ';
            testLead.Lead_Source_2__c = 'advertisement ';
            testLead.Lead_Source_3__c = 'Social media ';
            testLead.Lead_Source_4__c = 'Friend ';
            testLead.Email = 'test@gmail.com';
            testLead.Title = 'Mr';
            testLead.Website = 'www.google.com';
            testLead.LeadSource = 'Identified by Sales Rep';
            testLead.Description = 'Description ';
            testLead.Phone = '9898989898';
            testLead.Ready_to_share_with_Interlynx__c = true;
            testLead.Country = 'US';
            testLead.Industry = 'Industrial';
            testLead.Opportunity_Type__c = 'Budgetary';
            if(i == 0){
                testLead.VPTECH_Brand__c = 'Milton Roy';
                testLead.Business__c = 'Milton Roy';
            }
                
            else if(i == 1){
                 testLead.VPTECH_Brand__c = 'MP Pumps';
                 testLead.Business__c = 'MP/OB/IRP';
            }
               
            else if(i == 2){
                testLead.VPTECH_Brand__c = 'ARO';
                testLead.Business__c = 'ARO';
            }
                
            testLeads.add(testLead);
        }
        
        insert testLeads;

        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account').getRecordTypeId();
        acc.ShippingCity = 'Test City';
        acc.ShippingCountry = 'US';
        acc.ShippingPostalCode = '658776';
        acc.ShippingStreet = 'test street';
        acc.Name = 'Test Account';
        insert acc;
        
        Account aroAccount = new Account();
        aroAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ARO_Account').getRecordTypeId();
        aroAccount.ShippingCity = 'Test City1 ';
        aroAccount.ShippingCountry = 'US';
        aroAccount.ShippingPostalCode = '658776';
        aroAccount.ShippingStreet = 'test street';
        aroAccount.Name = 'Test ARO Account';
        aroAccount.Type = 'Bill To';
        insert aroAccount;
        
        
        Assignment_Group_Name__c MRRSMGroup = new Assignment_Group_Name__c(Name=System.Label.MR_RSM_Territory);
       	insert MRRSMGroup;
        
        Assignment_Group_Name__c MRDistributorGroup = new Assignment_Group_Name__c(Name=System.Label.MR_Distributor_Territory);
       	insert MRDistributorGroup;
        
        Assignment_Group_Name__c MPOBRSMGroup = new Assignment_Group_Name__c(Name =System.Label.MP_OB_IRP_RSM_Group_Name);
        insert MPOBRSMGroup;
        
        Assignment_Group_Name__c MPOBDistributorGroup = new Assignment_Group_Name__c(Name =System.Label.MP_OB_IRP_RSM_Group_Name);
       	insert MPOBDistributorGroup;
        
        Assignment_Group_Name__c ARORSMGroup = new Assignment_Group_Name__c(Name =System.Label.MP_OB_IRP_RSM_Group_Name);
		insert ARORSMGroup;
        
        Assignment_Group_Name__c ARODistributorGroup = new Assignment_Group_Name__c(Name =System.Label.MP_OB_IRP_RSM_Group_Name);
		insert ARODistributorGroup;
        
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'RSMUser', Email='RSM@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='RSMuser1@IR.com');
        insert u;
        List<Assignment_Groups__c> groupMembers = new List<Assignment_Groups__c>();
        for(Integer i = 0; i<=5; i++){
            Assignment_Groups__c groupMember = new Assignment_Groups__c();
            groupMember.User__c = u.Id;
            groupMember.Active__c = 'true';
            if(i == 0){
                groupMember.Group_Name__c = MRRSMGroup.Id;
                groupMember.Brand__c = 'Milton Roy';
                groupMember.Country__c = 'US';
                groupMember.RSMID__c = '192';
               
            }
            else if( i == 1){
                groupMember.Group_Name__c = MRDistributorGroup.Id;
                groupMember.DISTID__c = '110';
                groupMember.Brand__c = 'Milton Roy';
                groupMember.Country__c = 'US';
                groupMember.Distributor_Name__c = acc.Id;
                
            }
            else if(i == 2){
                groupMember.Group_Name__c = MPOBRSMGroup.Id;
                groupMember.Brand__c = 'MP Pumps';
                groupMember.Country__c = 'US';
                groupMember.RSMID__c = '152';  
            }
            else if(i == 3) {
                groupMember.Group_Name__c = MPOBDistributorGroup.Id;
                groupMember.DISTID__c = '120';
                groupMember.Brand__c = 'MP Pumps';
                groupMember.Country__c = 'US';
                groupMember.Distributor_Name__c = acc.Id;
            }     
            else if(i == 4){
                groupMember.Group_Name__c = ARORSMGroup.Id;
                groupMember.Brand__c = 'ARO';
                groupMember.Country__c = 'US';
                groupMember.RSMID__c = '154';
            }
            else if (i == 5){
                groupMember.Group_Name__c = ARODistributorGroup.Id;
                groupMember.DISTID__c = '150';
                groupMember.Brand__c = 'ARO';
                groupMember.Country__c = 'US';
                groupMember.Distributor_Name__c = aroAccount.Id;
            }
          
            groupMembers.add(groupMember);
        }
        insert groupMembers;

        Test.startTest();   
        List<AssignmentGroupController.Wrapper> theList = new List<AssignmentGroupController.Wrapper>();
        AssignmentGroupController.Wrapper wrpr = new AssignmentGroupController.Wrapper();
        wrpr.leadId = testLeads[0].Id;
        wrpr.territoryName = 'RSM Territory';
        theList.add(wrpr);
       
        List<List<Id>> MRDistResult = AssignmentGroupController.invoke(theList);//testLeads[1].Id
        
        theList[0].leadId = testLeads[0].Id;
        theList[0].territoryName = 'Distributor Territory';
        List<List<Id>> MRRSMResult = AssignmentGroupController.invoke(theList);
        
        theList[0].leadId = testLeads[1].Id;
        theList[0].territoryName = 'RSM Territory';
        List<List<Id>> resultRSMMpob = AssignmentGroupController.invoke(theList);
        
        theList[0].leadId = testLeads[1].Id;
        theList[0].territoryName = 'Distributor Territory';
        List<List<Id>> resultDistMpob = AssignmentGroupController.invoke(theList);
        
        theList[0].leadId = testLeads[2].Id;
        theList[0].territoryName = 'RSM Territory';
        List<List<Id>> resultRSMARO = AssignmentGroupController.invoke(theList);
        
        theList[0].leadId = testLeads[2].Id;
        theList[0].territoryName = 'Distributor Territory';
        List<List<Id>> resultDistARO = AssignmentGroupController.invoke(theList);
        
       
        Test.stopTest();
    }
}