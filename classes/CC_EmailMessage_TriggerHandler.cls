public with sharing class CC_EmailMessage_TriggerHandler {
    
    public static void beforeInsert(List<EmailMessage> emails) {
    	
    	system.debug('--CC_EmailMessage_TriggerHandler');
    	
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	system.debug('settings: ' + settings);
    	Set<Id> recordTypeIds = settings != null && String.isNotBlank(settings.CC_Email_Case_Record_Type_IDs__c) ? new Set<Id>((List<Id>)settings.CC_Email_Case_Record_Type_IDs__c.split(',')) : new Set<Id>();
    	Id defaultCaseOwnerId = settings != null && String.isNotBlank(settings.CC_Default_Case_Owner_ID__c) ? settings.CC_Default_Case_Owner_ID__c : null;    	
    	
    	if (!recordTypeIds.isEmpty()){
	   		EmailMessage_TriggerHandlerHelper.preventDuplicateEmails(emails, recordTypeIds);
	   		EmailMessage_TriggerHandlerHelper.createNewCaseAndReparentEmail(emails, null, recordTypeIds, defaultCaseOwnerId, EmailMessage_TriggerHandlerHelper.CompanyDivision.CLUB_CAR);
    	}
    }     
}