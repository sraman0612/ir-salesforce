/*
*
* $Revision: 1.0 $
* $Date: $
* $Author: $
*/
@isTest
public class CC_PavilionPartsHomeControllerTest {
    static testMethod void UnitTest_PartsHome()
    {    
        
        CC_Pavilion_Content__c pc = TestUtilityClass.createPavilionContentWithRecordType('Parts Home Text','Home Page Text');
        insert pc;
        Id recordtypid = [select id from RecordType where RecordType.Name='Home Page Text'].Id;
        System.assertEquals(recordtypid, pc.RecordTypeId);
        System.assertNotEquals(null, pc.Id);
        CC_PavilionTemplateController controller;
        CC_PavilionPartsHomeController CredCentHome = new CC_PavilionPartsHomeController(controller);        
    }
}