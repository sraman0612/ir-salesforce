/*
- This Class is created to support CC_PavilionAdminCreateUser Page.
- $Revision: 1.0 $
- $Date: $6/5/2016
- $Author: $
*/

public without sharing class CC_PavilionAdminCreateUsercnt {
    
    // All Variables declaration
    public User singleuser {get;set;}
    public User workingUser {get;set;}
    public String Selectedroleid {get;set;}
    public string selectedcontactid {get;set;}
    public Boolean mailExist {get;set;}
    public string SelectedUserid {get;set;}
    public string selectedExistcontactid {get;set;}
    public User newuser {get;set;}
    public User user {get;set;}
    public Contact ContObj {get;set;}
    public boolean errorMessage {get;set;}
    public string AccountName {get;set;}
    public Id contactId;
    public id accountid;
    public Id licenseid;
    public String PageMessage {get;set;}
    public String selectedProfile {get;set;} 
    public String selectedRole {get;set;}
    public String selectedTimeZone {get;set;}
    public String CntId {get;set;}
    public boolean makeReadOnly {get;set;}
    public boolean makehideOnly {get;set;}
    public map<String,String> mapForNavProfile{get;set;}
    public id createdContactID{get;set;}
    public Boolean SuccessStat{get;set;}
    public ID CreatedUserID{get;set;}
    public boolean CheckBox {get;set;}
    public String listToShow {get;set;}
    public List < User > userList {get;set;}
    public map < string, string > accountmap {get;set;}
    public List < string > agrType {get;set;}
    public Set < string > profileSet {get;set;}
    
    
    public CC_PavilionAdminCreateUsercnt(){
        constructorFunction2();
    }
    public CC_PavilionAdminCreateUsercnt(CC_PavilionTemplateController controller) {
        constructorFunction2();
    } 
    public  void constructorFunction2(){
        workingUser = new User();
        newuser = new User(); // creating new instance of User object to take values from the VF page.
        ContObj = new Contact();  // creating new instance of Contact object to take values from the VF page.
        PageMessage = '';
        SuccessStat=false;
        //Fetch all the relavent information of the current logged in user.
        workingUser = [Select id, accountid, contactid, CC_View_Parts_Bulletins__c, Person_Type__c, Name, Phone, MobilePhone, Email, CC_CustomerView__c,CC_View_Sales_Pricing__c,CC_View_Parts_Pricing__c,  CC_View_Sales_Bulletins__c, CC_View_Service_Bulletins__c from User where id = : UserInfo.getUserId() ]; 
        if(workingUser.accountid != null){
            for (Account ac: [Select Id, Name from Account Where id = : workingUser.accountId limit 1]) {
                AccountName = ac.Name;
            }
        }
        accountmap = new map < string, string > (); //this map will be used to collect Contract subtypes related to a account and that will be used to populate Navigation Profile list in Vf Page.
        if(workingUser.accountid != null){
            for (Contract c: [SELECT id, name, CC_Sub_Type__c, CC_Type__c from Contract where AccountId = : workinguser.accountId AND CC_Type__c = 'Dealer/Distributor Agreement']) { //collecting the subtype of contract to a map.
                if (c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '') {
                    accountmap.put(c.CC_Sub_Type__c, '');
                }
            }
        }
        // this will used to store page messages and show using a popup modal window.
        // this will populate the navigation profile to the page.
        if(ApexPages.currentPage().getParameters().get('cntid')!= null)
            contactverify();
    }
    
    // Logic to verify contacts associated with respective accounts and autopopulate contact fields on New User Creation if the exisitng contact is selected
    public void contactverify(){
        
        CntId = ApexPages.currentPage().getParameters().get('cntid'); // receiving the contactid at the time of loading the page,which indicates whether we will create a new contact or use that existing one.
        if (CntId != null) { //if there is a exsiting contact id then it will populate email,name,phone fields and will disable those fields.
            makeReadOnly = true;
        } else {
            makeReadOnly = false; //if there is no exsiting contact id then all the fields will be blank.
        }
        
        if (CntId != null) { //if we have existing contact id then we will populate the data in the email, name field and contact fields as well
            ContObj = [select firstname, lastname, email, phone, accountId, mobilephone, title, department from Contact where id = : CntId];
            newuser.username=ContObj.email;
            newuser.email = ContObj.email;
            newuser.firstname = ContObj.firstname;
            newuser.lastname = ContObj.lastname;
        } 
        else { //if we don't have existing contact id then we will are creating a new instance to catch new values.
            errorMessage = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'You are not associated with any Account');
            PageMessage = 'Error: unable to get valid Account details';
            ContObj = new Contact();
        }
    }
    
    //this method will populate user to replace list in customer view popup window
    public list < selectoption > getpavilionUsers() { 
        list < selectoption > options = new list < selectoption > ();
        for (User usercont: [select id, Name, IsActive from User where IsActive = true limit 10]) {
            if(usercont != Null){
                options.add(new selectoption(usercont.id, usercont.Name));
            }
        }
        return options;
    }
    
    //this method populates the list of navigation profiles based on the criteria: which nav profile's  agreement subtypes contains Contract subtypes of that related accounts.
    public List < selectoption > getpavilionNavProfiles() { 
        mapForNavProfile=new Map<String,String>();
        list<SelectOption> listOptions = new list<SelectOption>();
        set < selectoption > navprofile = new set < selectoption > ();// edited by Hurmat Jahan
        accountId = [SELECT AccountId from User  where id =:userinfo.getUserId()].AccountId;
        System.debug('@@@@@accountId @@@@@@'+accountId);
        Account acc=[Select CC_Global_Region__c from Account where id=:accountId];
        System.debug('@@@@@acc@@@@@@'+acc);
        accountmap = new map < string, string > (); 
        //fetch the subtype of the contracts associated with the relavant accounts
        for (Contract c: [SELECT id, name, CC_Sub_Type__c, CC_Type__c from Contract where AccountId = : accountId AND CC_Type__c = 'Dealer/Distributor Agreement']) { 
            System.debug('@@@@@c@@@@@@'+c);
            if (c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '') {
                accountmap.put(c.CC_Sub_Type__c, '');
            }
        }
        
        if(accountmap.size() > 0 && acc != Null){
            for (Pavilion_Navigation_Profile__c usercont: [SELECT Id, Name, Description__c, Region__c, Agreement_Sub_Type__c,Display_Name__c from Pavilion_Navigation_Profile__c Where Region__c includes (:acc.CC_Global_Region__c) ]) {
                System.debug('@@@@@usercont@@@@@@'+usercont);
                if (usercont.Agreement_Sub_Type__c != null) {
                    System.debug('@@@@@usercont.Agreement_Sub_Type__c@@@@@@'+usercont.Agreement_Sub_Type__c);
                    for (string s: usercont.Agreement_Sub_Type__c.split(';')) {
                        if(s != null || s != ''){
                            if (accountmap.containskey(s)) {
                                if(usercont.Description__c != null && usercont.Name != null){
                                    mapForNavProfile.put(usercont.Description__c, usercont.Name);
                                    navprofile.add(new selectoption(usercont.Description__c, usercont.Display_Name__c));
                                }
                            }
                        }
                    }
                }
            }
        }
        if(navprofile.size() > 0)
            listOptions.addAll(navprofile);
        return listOptions;
    }
    
    public List<selectoption> getroles(){
        list<SelectOption> listOptions = new list<SelectOption>();
        for(UserRole ur : [Select Id, Name, PortalRole 
                           from UserRole where PortalAccountId = :workingUser.accountid]){
                  listOptions.add(new selectoption(ur.Id, ur.Name));             
          }
        return listOptions;
    }
    
    public list<SelectOption> gettimezones(){
      list<SelectOption> listOptions = new list<SelectOption>();
      Schema.DescribeFieldResult dfr = Schema.User.TimeZoneSidKey.getDescribe();
      List<Schema.PicklistEntry> pleLst = dfr.getPicklistValues();
      for( Schema.PicklistEntry  ple : pleLst) {
        listOptions.add(new SelectOption(ple.getValue(),ple.getLabel()));
      }
      return listOptions;
    }
    
    public PageReference insertUserandContact(){
        Id caseID;
        PageReference pref;
        String SelectedProfileid;
        
        if(CntId == null) {
            pref = new PageReference('/CC_PavilionAdminCreateUser');
        } else {
            pref = new PageReference('/CC_PavilionAdminCreateUser?cntid=' + CntId);
        }
        
        try {
            user createuser = new user();
            Map < String, Id > duplicateContactMap = new Map < String, Id > ();
            Map < String, user > usernameMap = new Map < String, user > ();
            List < Contact > lstInstConts = new List < Contact > ();
            boolean isuseractive = false;
            String mailerId = '';
            string SelectedValue = ApexPages.currentPage().getParameters().get('selectedYesRadio');
            string selectedEnabled = ApexPages.currentPage().getParameters().get('enableUser');
            string SelectedYesContRadio = ApexPages.currentPage().getParameters().get('SelectedYesContRadio');
            string PartsCheck=ApexPages.currentPage().getParameters().get('PartsCheck');
            String ServiceCheck=ApexPages.currentPage().getParameters().get('ServiceCheck');
            String SalesCheck=ApexPages.currentPage().getParameters().get('SalesCheck');
            String custViewCheckBox=ApexPages.currentPage().getParameters().get('custViewCheckBox');
            String PartsPricingCheck=ApexPages.currentPage().getParameters().get('PartsPricingCheck');
            String PartsOrderInvoicePricingCheck=ApexPages.currentPage().getParameters().get('PartsOrderInvoicePricingCheck');
            String PartCartCheck=ApexPages.currentPage().getParameters().get('PartCartCheck');
            String WarrantyTavantCheck=ApexPages.currentPage().getParameters().get('WarrantyTavantCheck');
            system.debug('@@@'+PartsOrderInvoicePricingCheck);
            
            system.debug('@@@'+WarrantyTavantCheck);
            String SalesPricingCheck=ApexPages.currentPage().getParameters().get('SalesPricingCheck');
            
            if (SelectedValue == null || SelectedValue == '')
                SelectedValue = 'no';
            
            id licenseid = [SELECT Id FROM UserLicense where name = 'Partner Community'][0].id;
            SelectedProfileid = PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c;
            
            
            if (selectedEnabled == 'yes') {
                isuseractive = true;
            } else {
                isuseractive = false;
            }
            //checking whether the email id exists with a user or not.
            try {
                mailerId = [select username from User where username = : newuser.username limit 1][0].username;
                mailExist = (mailerid != null && mailerid != '') ? true : false;
            } catch(Exception e) {
                mailExist = false;
            }
            
            if (CntId == null) { //as through parameter we havn't got any contactId so we will create a new contact with the new values.
                contact cont = new contact();
                RecordType rt = [select id, Name from RecordType where SobjectType = 'Contact' and Name = 'Club Car'  Limit 1 ];
                system.debug('@@@@new user firstname'+newuser.firstName);
                cont.FirstName = newuser.firstname;
                cont.LastName =  newuser.lastname;
                cont.AccountId = workingUser.accountid;
                
                if (cont.AccountId == null) { //if the AccountId is null then generating a error message 
                    errorMessage = true; // it will enable the popup window output panel.
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'You are not associated with any Account');
                    ApexPages.addMessage(myMsg);
                    PageMessage = 'Error:  You are not associated with any Account'; // this error message will be shown.
                }
                
                cont.Email = newuser.email;
                cont.Title = ContObj.Title;
                cont.Phone = ContObj.Phone;
                cont.Department = ContObj.Department;
                cont.recordTypeId = rt.id;
                lstInstConts.add(cont);
                
                insert lstInstConts; //inserting the new contact first.
                PageMessage = 'Contact Is created'+lstInstConts[0].Id+'\n' ;
                System.debug('^^^^^^^^^^' +'Contact Is created'+lstInstConts[0].Id);
                createdContactID=lstInstConts[0].Id;
                
                createuser.contactid = lstInstConts[0].id; // assigning the recently inserted contact id to the new user field.
            } else { //here we have the existing contactid.
                
                List < user > userList = new List < user > ();
                createuser.contactid = CntId; //assigning the new user that existing contactid.
                Contact con = [select id, name, email from contact where id = : CntId]; //getting existing contact credentials.
                userList = [Select id, name, email from user where email = : con.email and contactid != null]; //getting the list of user having the email id of that contact.
                
                if (userList.size() > 0) { //if there is existing user with the same email then generating eroor message pop up.
                    errorMessage = true;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: A Pavilion Portal user  already exists with this Email');
                    ApexPages.addMessage(myMsg);
                    PageMessage = 'Error: A Pavilion Contact already exists with the same Username. Please try with different one.';
                    //return null;
                }
            }
            
            
            createuser.firstname = newuser.firstname; //assigning values to the user object.
            createuser.lastname = newuser.lastname;
            createuser.username = newuser.username;
            string lastname=newuser.lastname;
            string firstname=newuser.firstname;
            createuser.Alias = lastname.left(4) + firstname.left(4);
            createuser.ProfileId = SelectedProfileid; //role assignment.
            System.debug(Selectedroleid + 'contactid' + selectedcontactid);
            
            //assigning user manadatory fields
            createuser.title=ContObj.Title;
            createuser.Department=ContObj.Department;
            createuser.MobilePhone=ContObj.MobilePhone;
            createuser.Phone=ContObj.Phone;
            createuser.email = newuser.email;
            createuser.LanguageLocaleKey = 'en_US';
            createuser.LocaleSidKey = 'en_US';
            createuser.TimeZoneSidKey = selectedTimeZone;//'America/Los_Angeles';
            createuser.EmailEncodingKey = 'UTF-8';
            createuser.isActive = isuseractive;
            
            //Added by Tapas Tripathi assigning some extra permission sets.
            If(PartsCheck=='true')
                createuser.CC_View_Parts_Bulletins__c=true;
            else
                createuser.CC_View_Parts_Bulletins__c=false;
            If(SalesCheck=='true')
                createuser.CC_View_Sales_Bulletins__c=true;
            else
                createuser.CC_View_Sales_Bulletins__c=false;
            If(ServiceCheck=='true')
                createuser.CC_View_Service_Bulletins__c=true;
            else
                createuser.CC_View_Service_Bulletins__c=false;

            If(SalesPricingCheck=='true')
                createuser.CC_View_Sales_Pricing__c=true;
            else
                createuser.CC_View_Sales_Pricing__c=false;
            If(PartsPricingCheck=='true')
                createuser.CC_View_Parts_Pricing__c=true;
            else
                createuser.CC_View_Parts_Pricing__c=false;
            If(PartsOrderInvoicePricingCheck=='true')
                createuser.CC_View_Parts_Order_Invoice_Pricing__c=true;
            else
                createuser.CC_View_Parts_Order_Invoice_Pricing__c=false;
            If(PartCartCheck=='true')
                createuser.CC_Part_Cart__c=true;
            else
                createuser.CC_Part_Cart__c=false;
            
            If(WarrantyTavantCheck=='true')
                createuser.Warranty_Tavant__c=true;
            else
                createuser.Warranty_Tavant__c=false;
            
            createuser.CC_Pavilion_Navigation_Profile__c = mapForNavProfile.get(selectedProfile);
            String currStr=[select id, currencyISOCode from contact where id = :createuser.contactid].currencyISOCode;
            createUser.CurrencyISOCode = currStr; 
            createUser.DefaultCurrencyISOCode = currStr; 
            system.debug('###'+createuser);
            if (mailExist) { 
                errorMessage = true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: A Club Car Links user already exists with the same username. Add “.cc” at the end to process the request.');
                ApexPages.addMessage(myMsg);
                PageMessage = 'Error: A Club Car Links user already exists with the same username. Add “.cc” at the end to process the request.';
                
            } else {
                insert createuser;
                
                CreatedUserID=createuser.Id;
                if (createuser.Id != null) { 
                    
                    if (selectedRole instanceOf Id) {
                        updateRole(createuser.id, selectedRole);
                    }
                    Case newSuccessCase = new Case();
                    newSuccessCase.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Club_Car_Case'].id;
                    newSuccessCase.CC_user__c = createuser.id;
                    newSuccessCase.Reason='User Admin';
                    newSuccessCase.Sub_Reason__c = 'New User';
                    newSuccessCase.Origin = 'Community';
                    newSuccessCase.ContactId = workingUser.ContactId;
                    newSuccessCase.Subject = 'User Admin';
                    newSuccessCase.Description = 'User has been created';
                    newSuccessCase.cc_createCustomerViewUser__c='true'==custViewCheckBox?TRUE:FALSE;
                    newSuccessCase.cc_createPartCartUser__c='true'==PartCartCheck?TRUE:FALSE;
                    newSuccessCase.cc_createTavantUser__c='true'==WarrantyTavantCheck?TRUE:FALSE; 
                    newSuccessCase.ownerId = PavilionSettings__c.getInstance('New_User_Creation_Queue').Value__c;
                    
                    insert newSuccessCase;
                    Case insertedCase = [Select CaseNumber, id from Case where id = : newSuccessCase.id];
                    if(insertedCase!=null){
                        SuccessStat=true;
                        system.debug('Here inside if');
                    }
                    
                    errorMessage = true;
                    system.debug(SuccessStat);
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'User is created Id = ' + createuser.Id);
                    PageMessage = ' Your new user request has been received. Your case number is #'+insertedCase.CaseNumber+'. A member of the Club Car Links Team will contact you within 48 hours to issue the new credentials.';
                    ApexPages.addMessage(myMsg);
                    caseID=newSuccessCase.id;
                }
            }
            
            return null;
            
        } catch (DmlException e) { // if apart from previous conditions if any system exception occurrs then this error message will popup.
            System.debug('^^^^^^^^^^^^^^Error' + e.getStackTraceString());
            if(CntId==null){
                try{
                    List<Contact> cntTempList=new List<Contact>();
                    cntTempList=[select Id from Contact where ID=:createdContactID];
                    delete cntTempList;
                }
                catch(exception e1){
                    System.debug(e1.getStackTraceString()) ;  
                }
            }
            errorMessage = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error' + e);
            ApexPages.addMessage(myMsg);
            String error=''+e;
            if(error.contains('DUPLICATE_USERNAME'))
                PageMessage = 'Error: A Club Car Links user already exists with the same username. Add “.cc” at the end to process the request.' ;
            else if(error.contains('PORTAL_USER_ALREADY_EXISTS_FOR_CONTACT'))
                PageMessage = 'Error: Unable To create User: A Portal User already exists with this contact. Please choose another contact.' ; 
            else if(caseID==null && CreatedUserID!=null){
                PageMessage='Error: User created but case associated with it not created.'+e.getDmlMessage(0);
            }
            else
                PageMessage='Error: Unable to create user. Please try again later.'+e.getDmlMessage(0);
            return null;
        }
        //catch(Exception e){
        // PageMessage='Error: Unable to create user. Please try again later.'+e.getMessage();
        //  return null;
        //} 
    }
    
    //method added by tapas to Assign permision set to the newly Created user if the SalesPricing and Parts Pricing both two checkboxes are checked.
    @future
    public static void updateRole(Id uid, String roleId){
        User u = new user(id = uid, UserRoleId = roleId);
        update u;
    }
}