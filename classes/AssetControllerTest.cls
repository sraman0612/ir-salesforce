@isTest
public class AssetControllerTest{
    @isTest static void assetUserLookup()
    {
        Product2 product=new Product2();
        product.RecordTypeId = System.Label.Product_Record_Type;
        product.Name = 'Centac';
        product.Customizable__c=FALSE;
        insert product;
        
        Asset asset = new Asset(); 
        asset.Name = 'Asset 1';
        asset.AccountId = System.Label.End_Customer_Account_APAC;
        asset.CTS_TechDirect_Model_Number__c = 'M-123';
        asset.Product2Id = product.Id;
        //insert asset;
       
        User user = new User(alias = 'hasrole', email='userwithrole@roletest1.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com',
                                    DB_Region__c='APAC');
        insert user;
        
        test.startTest();
        AssetController.createRecord(asset);
        AssetController.fetchAsset('Centac');
        AssetController.fetchUser();        
        test.stopTest();
    }

}