public class CTS_SubmitCaseController {
	//Submit Feedback
    @auraEnabled
    public static List<Object> submitArticleFeedback(String message, String articleId,String caseType, String subject){
        String title = '';
        String articleNumber = '';
        String articleURL = null;
        System.debug('::articleId'+articleId);
        if(articleId != null){
            for(KnowledgeArticleVersion knowledge : [select id, title,ArticleNumber from KnowledgeArticleVersion where id = :articleId]){
                title = knowledge.title;
                articleNumber = knowledge.articleNumber;
                articleURL = System.Label.CTS_Knowledge_URL + knowledge.id + '/view';
            }
            if(title.length() > 180){
                title = title.substring(0,180) + '..';
            }
            Subject = Subject + title +' (' + articleNumber + ')';
        }
        String contactId;
        for(User u : [select id, contactId from User where id = : userInfo.getUserId()]){
            contactId = u.contactId;
        }
        String caseId = CTS_CaseHelper.createCase(message,Subject,contactId,caseType,articleURL);
        if(caseId != null && caseId.startsWith('500')){
            Case c = [SELECT Id, CaseNumber FROM Case WHERE Id =: caseId LIMIT 1];
            return new List<Object>{'SUCCESS', c};
        }else{
            return new List<Object>{'FAILURE'};
        }
    }
}