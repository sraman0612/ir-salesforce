@isTest
public class OpportunityUtilsTest {

    public static testMethod void testExistingBusiness() {
        OpportunityUtils u = new OpportunityUtils();
        
        ID existingRecordType = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        ID rshvacAccountRecordType = [Select Id, DeveloperName from RecordType where DeveloperName = 'Champion_TechKnowledgy_Account' and SobjectType='Account' limit 1].Id;

        Account tstAcc1 = new Account();
        tstAcc1.name = 'OppTestAcc1';
        tstAcc1.RecordTypeId = rshvacAccountRecordType;
        tstAcc1.Type = 'Customer';
        tstAcc1.Current_Volume__c = 1000000;
        tstAcc1.AnnualRevenue = 1000;

        insert tstAcc1;
        u.createExistingBusinessOpportunity(tstAcc1);
		//commented by CG as part of descoped object
        /*RSHVAC_Order__c tstOrder1 = new RSHVAC_Order__c();
        tstOrder1.Account__c = tstAcc1.ID;
        tstOrder1.Amount__c = 25000;
        tstOrder1.Ordered_Date__c = Date.newInstance(2016, 1, 1);
        insert tstOrder1;*/

        List<Opportunity> opps = [SELECT ID, recordTypeId, Amount, AccountId, StageName, Type FROM Opportunity WHERE recordTypeId = :existingRecordType];

        for(Opportunity opp : opps) {
            if (opp.accountID == tstAcc1.ID) {
                //u.updateExistingOpportunityAmount(opp);
            }

        }

    }

    private static testMethod void testNewBusiness() {
        OpportunityUtils u = new OpportunityUtils();
       
        ID newBusinessRecordType = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        ID rshvacAccountRecordType = [Select Id, DeveloperName from RecordType where DeveloperName = 'Champion_TechKnowledgy_Account' and SobjectType='Account' limit 1].Id;
        //Commented as part of EMEIA Cleanup
        /*RSHVAC_Properties__c IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
        insert IntegrationUser;*/
        Account tstAcc2 = new Account();
        tstAcc2.name = 'OppTestAcc1';
        tstAcc2.RecordTypeId = rshvacAccountRecordType;
        tstAcc2.Type = 'Prospect';


        insert tstAcc2;

        Opportunity newOpp2 = new Opportunity();
        newOpp2.name = 'Test New Biz';
        newOpp2.recordTypeId = newBusinessRecordType;
        newOpp2.accountId = tstAcc2.Id;
        newOpp2.CloseDate = Date.newInstance(2027, 7, 18);
        newopp2.StageName='Stage 1. Qualify';
        newOpp2.Type = 'Prospect';

        insert newOpp2;

		//commented by CG as part of descoped object
        /*RSHVAC_Quote__c tstQuote1 = new RSHVAC_Quote__c();
        tstQuote1.Opportunity__c = newOpp2.ID;
        tstQuote1.Extended_Gross_Sales__c = 10000;
        tstQuote1.Siebel_ID__c = 'fake123';
        insert tstQuote1;*/
        OpportunityUtils.createIwdRevenueSchedule();
     //   u.updateNewBusinessOpportunityAmount(tstQuote1);

    }

}