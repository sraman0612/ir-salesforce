/*
	@Author: Ben Jenkin
	@Date: 2020-03-13
	@Description: PT_OpportunityStageDatesBatchTest.apxc
*/
@isTest
public class PT_OpportunityStageDatesBatchTest 
{
	private static testMethod void testFixStatgeDates()
    {
        
        RecordType powerToolsRecordType = [Select id from recordtype where name = 'PT_powertools' and sobjecttype = 'Opportunity'];
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'testOpportunity';
        testOpportunity.CloseDate = Date.Today();
        testOpportunity.StageName = '1.Target';
        testOpportunity.RecordTypeId = powerToolsRecordType.Id;
        testOpportunity.PT_date_target__c = Date.Today();
		insert testOpportunity;
        
        testOpportunity.StageName = '2.Qualify';
        update testOpportunity;
        
        PT_OpportunityStageDatesBatch batchJob = new PT_OpportunityStageDatesBatch();
        batchJob.queryOpportunityIds = new Set<Id>{testOpportunity.Id};
        batchJob.queryLimit = 1;
        batchJob.debug = true;
        DataBase.executeBatch(batchJob); 
    }
}