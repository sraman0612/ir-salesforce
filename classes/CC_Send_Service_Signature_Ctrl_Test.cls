@isTest
private with sharing class CC_Send_Service_Signature_Ctrl_Test {

	@TestSetup
	private static void createData(){
		
		echosign_dev1__Agreement_Template__c template = TestDataUtility.createAdobeAgreementTemplate('Test Template');
		insert template;
		
		Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
		templates.CC_Service_Contract_Template_ID__c = template.Id;
		upsert templates;
		
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;		
		
		TestDataUtility dataUtility = new TestDataUtility();	

        Account a1 = dataUtility.createAccount('Club Car');
        a1.Name = 'Account1';	
        insert a1;
        
        insert TestDataUtility.createCCServiceContract(a1.Id, 'Monthly');
	}
	
	@isTest
    private static void testSendForSignatureNoTemplateID() {

		Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
		templates.CC_Service_Contract_Template_ID__c = null;
		upsert templates;
        
        Contract c = [Select Id, CC_Contract_Status__c From Contract];
        
        Test.startTest();
        
        CC_Send_Service_Contract_Signature_Ctrl ctrl = new CC_Send_Service_Contract_Signature_Ctrl(new ApexPages.StandardController(c));
        
        system.assertEquals(c.Id, ctrl.stdctrl.getId());
        system.assertEquals(c.Id, ctrl.contract.Id);        
        system.assertEquals(null, ctrl.sendForSignature());
        system.assertEquals(ApexPages.Severity.Error, ApexPages.getMessages()[0].getSeverity());
        system.assertEquals('Adobe template ID not specific in custom settings.', ApexPages.getMessages()[0].getSummary());
        
        Test.stopTest();
    }	

	@isTest
    private static void testSendForSignatureDraft() {
        
        Contract c = [Select Id, CC_Contract_Status__c From Contract];
        
        Test.startTest();
        
        CC_Send_Service_Contract_Signature_Ctrl ctrl = new CC_Send_Service_Contract_Signature_Ctrl(new ApexPages.StandardController(c));
        
        system.assertEquals(c.Id, ctrl.stdctrl.getId());
        system.assertEquals(c.Id, ctrl.contract.Id);        
        system.assertEquals(null, ctrl.sendForSignature());
        system.assertEquals(ApexPages.Severity.Error, ApexPages.getMessages()[0].getSeverity());
        system.assertEquals('Contract must be approved before it can be sent for signature.', ApexPages.getMessages()[0].getSummary());
        
        Test.stopTest();
    }
    
	@isTest
    private static void testSendForSignatureApproved() {
        
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
        
        Contract c = [Select Id, ContractNumber, CC_Contract_Status__c From Contract];
        c.CC_Contract_Status__c = 'Approved';
        update c;
        
        Test.startTest();
        
        CC_Send_Service_Contract_Signature_Ctrl ctrl = new CC_Send_Service_Contract_Signature_Ctrl(new ApexPages.StandardController(c));
        
        system.assertEquals(c.Id, ctrl.stdctrl.getId());
        system.assertEquals(c.Id, ctrl.contract.Id); 
        
        PageReference pg = ctrl.sendForSignature();
        
        Attachment att = [Select Name, Body From Attachment Where ParentId = :c.Id and Name Like 'Service Contract%'];
        system.assertEquals(Blob.valueOf('Test PDF'), att.Body);       
        
        PageReference testPg = Page.echosign_dev1__AgreementTemplateProcess;
        testPg.getParameters().put('masterid', String.valueOf(c.Id).substring(0, 15));
        testPg.getParameters().put('templateId', templates.CC_Service_Contract_Template_ID__c);                   
        system.assertEquals(testPg.getUrl(), pg.getUrl());        
        system.assertEquals(0, ApexPages.getMessages().size());
        
        Test.stopTest();
    }     
    
	@isTest
    private static void testSendForSignatureSigned() {
        
        Contract c = [Select Id, CC_Contract_Status__c From Contract];
        c.CC_Contract_Status__c = 'Signed';
        update c;
        
        Test.startTest();
        
        CC_Send_Service_Contract_Signature_Ctrl ctrl = new CC_Send_Service_Contract_Signature_Ctrl(new ApexPages.StandardController(c));
        
        system.assertEquals(c.Id, ctrl.stdctrl.getId());
        system.assertEquals(c.Id, ctrl.contract.Id);        
        system.assertEquals(null, ctrl.sendForSignature());
        system.assertEquals(ApexPages.Severity.Error, ApexPages.getMessages()[0].getSeverity());
        system.assertEquals('Contract is already signed.', ApexPages.getMessages()[0].getSummary());
        
        Test.stopTest();
    }    
}