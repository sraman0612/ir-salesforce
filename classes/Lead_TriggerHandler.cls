// 
// (c) 2015 Appirio, Inc.
//
//Generate an Handler Class to Handle trigger on Lead
//
// 31 March 2015  Surabhi Sharma 
// April 16,2015    Surabhi Sharma    T-377640(delete the record)
// 14-Nov-2024 : Update code against Case #03261931 - AMS Team: Mansi

public class Lead_TriggerHandler {
  
    private static Final String IR_COMP_EU = 'IR Comp Business Hours - EU + Africa';
    private static Final String IR_COMP_EMEIA = 'IR Comp Business Hours - ME + India';
    private static Final String IR_COMP_APAC = 'IR Comp TechDirect_APAC_Business_Hours';
    private static Final String GD_COMP_APAC = 'IR Comp Business Hours - APAC';
    private static Final String GD_COMP_NA = 'IR Comp Business Hours - NA';
    private static Final String WEEKDAYS_EU = 'Weekday Hours EU + Africa';
    private static Final String WEEKDAYS_EMEIA = 'Weekday Hours ME + India';
    private static Final String WEEKDAYS_APAC = 'Weekday Hours APAC';
    private static Final String WEEKDAYS_NA = 'Weekday Hours NA';
    private static Final String DEFAULT_BH = 'Default';
   
    
    public static void beforeInsert(Lead[] newList)
    {
        AssignmentEngineV2.handleAssignment(newList, null);
    }
    
    public static void beforeUpdate(Lead[] newList, Map<Id,Lead> oldMap)
    {
        AssignmentEngineV2.handleAssignment(newList, oldMap);
    }

     // Method to delete the record once Approved by Approver
    public static void afterUpdate(List<Lead> newList, Map<Id,Lead> oldMap){
        if(newList[0].isConverted == true && newList[0].ConvertedAccountId <> null){
        }
        UtilityClass.deleteApprovedRecords(newList,'Lead',UtilityClass.GENERIC_APPROVAL_API_NAME);//Record deleted
       
    }
    
      
    // Changes by Capgemini Aman Kumar Date 6/3/2023. START VPTECH-1272
    public static void setResponseTime(List<Lead> newLeads, Map<Id, Lead> oldLeadMap) {
        system.debug('setResponseTime Menthod>>>');
        List<Lead> updatedLead = new List<Lead>();
        List<String> businessHourList = new List<String>{WEEKDAYS_EU,
            WEEKDAYS_EMEIA,WEEKDAYS_APAC,IR_COMP_EU,
            IR_COMP_EMEIA,IR_COMP_APAC,DEFAULT_BH,GD_COMP_APAC,GD_COMP_NA,WEEKDAYS_NA};
        
        List<BusinessHours> weekdayHrs = [SELECT Id,Name FROM BusinessHours WHERE Name IN :businessHourList];
        Map<String,BusinessHours> leadToWeekDayBusinessHoursAll = new Map<String,BusinessHours>();
        Map<Id,String> IdToName = new Map<Id,String>();
        
        for(BusinessHours bh : weekdayHrs){
            leadToWeekDayBusinessHoursAll.put(bh.Name,bh);
			IdToName.put(bh.Id, bh.Name);           
        }
       
        for(Lead lead : newLeads) {
            if(lead.Response_Date__c != null) {
                BusinessHours weekdayHr = getBusinessHour(lead,leadToWeekDayBusinessHoursAll,IdToName);

                if(oldLeadMap.get(lead.Id).Response_Date__c == null && weekdayHr != null) {
                   
                    Decimal timeDiff = BusinessHours.diff(weekdayHr.Id, lead.CreatedDate, lead.Response_Date__c);
                    lead.Response_Time__c = timeDiff.divide(3600000, 3);
                    
                    if(lead.Business_Hours__c != null) {
                        Decimal busTimeDiff = BusinessHours.diff(lead.Business_Hours__c, lead.CreatedDate, lead.Response_Date__c);
                        lead.Response_Time_Hours__c = busTimeDiff.divide(3600000, 3);
                    }
                }

            } 
            updatedLead.add(lead);
        }
       
    }
      
    public static BusinessHours getBusinessHour(Lead ld, Map<String,BusinessHours> leadToWeekDayBusinessHoursAll, Map<Id,String> IdToName){
        BusinessHours weekdayHr;
        
            if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(IR_COMP_EU)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(WEEKDAYS_EU);
            }
            else if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(IR_COMP_EMEIA)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(WEEKDAYS_EMEIA);
            }
            else if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(IR_COMP_APAC)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(WEEKDAYS_APAC);
            }
        	 else if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(GD_COMP_APAC)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(WEEKDAYS_APAC);
            }
         	else if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(GD_COMP_NA)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(WEEKDAYS_NA);
            }
        	else if(IdToName.get(ld.Business_Hours__c) !=null && IdToName.get(ld.Business_Hours__c).equalsIgnoreCase(DEFAULT_BH)){
                weekdayHr = leadToWeekDayBusinessHoursAll.get(DEFAULT_BH);
            }
        return weekdayHr;
    }
    // Changes by Capgemini Aman Kumar Date 6/3/2023. END VPTECH-1272
    
    /*------------------------------------------------------------------------------------------------------------
      Author : Mansi Agnihotri
      Date : 14/11/2024
      Description : This method shares the lead records with the user currently assigned as channel partner sales manager
                    and removes access for the previously assigned channel partner sales manager
      Case : 03261931
    --------------------------------------------------------------------------------------------------------------*/
    public static void shareLeadsWithChannelPartner(Map<Id,Lead> newLeadMap, Map<Id, Lead> oldLeadMap){
        Id previousChannelPartnerUserId;
        List<LeadShare> lstLeadShareRecordsToInsert = new List<LeadShare>();
        List<LeadShare> lstLeadShareRecordsToDelete = new List<LeadShare>();
        map<Id,LeadShare> mapPreviousUserVslead = new map<Id,LeadShare>();
        for(LeadShare lShareVar:[Select Id,UserOrGroupId,LeadId,LeadAccessLevel from LeadShare where
                                                               LeadId IN:newLeadMap.keySet()]){
                mapPreviousUserVslead.put(lShareVar.UserOrGroupId,lShareVar);
         }
        for(Lead leadVar:newLeadMap.values()){
            if(
               oldLeadMap.get(leadVar.Id).Channel_Partner_Sales_Manager__c!=leadVar.Channel_Partner_Sales_Manager__c
               && !mapPreviousUserVslead.containsKey(leadVar.Channel_Partner_Sales_Manager__c)              
               && oldLeadMap.get(leadVar.Id).OwnerId!=leadVar.Channel_Partner_Sales_Manager__c
            ){
                   previousChannelPartnerUserId = oldLeadMap.get(leadVar.Id).Channel_Partner_Sales_Manager__c;
                   LeadShare lShare = new LeadShare() ;
                   lShare.LeadAccessLevel = 'Read';
                   lShare.LeadId = leadVar.Id;
                   lShare.UserOrGroupId = leadVar.Channel_Partner_Sales_Manager__c;
                   lShare.RowCause = 'Manual';
                   lstLeadShareRecordsToInsert.add(lShare);
                   if(mapPreviousUserVslead.containsKey(previousChannelPartnerUserId)){
                      lstLeadShareRecordsToDelete.add(mapPreviousUserVslead.get(previousChannelPartnerUserId));
                    }
              }
        }
        Database.SaveResult[] listSaveResults = Database.insert(lstLeadShareRecordsToInsert,false);
        try{
            if(lstLeadShareRecordsToDelete!=null){
                Delete lstLeadShareRecordsToDelete;
            }
         }
         catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
         }
      }
   }