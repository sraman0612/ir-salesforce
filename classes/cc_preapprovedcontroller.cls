// Apex Controller created by @Priyanka Baviskar for Request 7364036. 
public class cc_preapprovedcontroller {
    public static User Usr {get;set;}
   
        public static Contact contct{get;set;}
         public static Account  account{get;set;}
  
   public CC_Hole_in_One__c requset {get; set;}
    public string accid;
    
    public cc_preapprovedcontroller (CC_PavilionTemplateController controller) {
    
    accid = Controller.acctid;
      
      requset = [SELECT Id, Type_of_pre_approval_Request__c, Primary_Event_Contact__c, Phone_Number__c, Email_Address__c, 
                 Black_and_Gold_Partner__c, Sales_Rep__c, Any_Prior_Funds_Request__c,Prior_Fund_Request__c, Amount_Requesting__c, 
                 Event_Plan_Including_resources_to_commit__c, Event_Name__c, Location__c, Start_Date__c, End_Date__c, Target_Audience__c,
                 Estimated_Attendance__c, Featured_Products__c, Competitors__c, Digital_Ad_Opportunity__c, Total_Show_Cost__c, This_Event_has_been_Held_before__c,
                 participated_in_this_event_before__c, When_participated_in_this_event_before__c, Has_Club_Car_Participated_Before__c,
                 When_Club_Car_Participated_Before__c FROM CC_Hole_in_One__c
                      WHERE Contract__r.Account.id =: accid AND 
                       Id = : ApexPages.currentPage().getParameters().get('Id')];
       System.debug('@@@@requset'+requset);

    }
    
    @RemoteAction
        public static List<CC_Hole_in_One__c> getprerequest(String preid)

    {
          
            usr = new User();
            contct =   new Contact();
            account = new Account();
            Id userid = UserInfo.getUserId();
            usr = (User)[select Id,Contact.Id, Name, Phone,Extension,MobilePhone from User where Id =:UserInfo.getUserId() limit 1];
            Id cid = usr.Contact.Id;
            if(cid!= null)
            contct = (Contact)[select Id, Account.Id, Email,  HomePhone, MobilePhone, OtherPhone, Phone from Contact where ID =:cid limit 1];
            Id accid = contct.Account.Id;
            if(contct.Account.Id != null) 
            account = (Account)[select Id ,AccountNumber,CC_Customer_Number__c from Account where Id =:accid limit 1];
            Id acctid = account.Id;
            system.debug('###acctid'+acctid);
            
           String caseNumb=preid;

        
                                     
       List<CC_Hole_in_One__c> lstpreapprequest = [SELECT Id, Type_of_pre_approval_Request__c, Primary_Event_Contact__c, Phone_Number__c, Email_Address__c, 
                 Black_and_Gold_Partner__c, Sales_Rep__c, Any_Prior_Funds_Request__c,Prior_Fund_Request__c, Amount_Requesting__c, 
                 Event_Plan_Including_resources_to_commit__c, Event_Name__c, Location__c, Start_Date__c, End_Date__c, Target_Audience__c,
                 Estimated_Attendance__c, Featured_Products__c, Competitors__c, Digital_Ad_Opportunity__c, Total_Show_Cost__c, This_Event_has_been_Held_before__c,
                 participated_in_this_event_before__c, When_participated_in_this_event_before__c, Has_Club_Car_Participated_Before__c,
                 When_Club_Car_Participated_Before__c FROM CC_Hole_in_One__c
                      WHERE Contract__r.Account.id =: acctid  AND 
                      Id =:preid];
                
        system.debug('**lstpreapprequest'+lstpreapprequest);
        
        
    return lstpreapprequest;
    
    }

}