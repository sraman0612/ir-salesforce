@isTest
private class SnapshotReportMaintenanceTest{
   /**
   *  Tests that the Batch executes properly
   */
  static testMethod void testBatchExecution() {
    User u =  [select id from User where id =: UserInfo.getUserId()  LIMIT 1];
    List<Snapshot_Test__c>  listSn = new List<Snapshot_Test__c>();
    
    Snapshot_Test__c sn = new Snapshot_Test__c();
    sn.Opportunity_OwnerTest__c = UserInfo.getUserId();
    insert sn;
    listSn.add(sn);
     
      Test.startTest();
          SnapshotReportMaintenance cb = New SnapshotReportMaintenance();
          Database.QueryLocator ql = cb.start(null);
          cb.execute(null,listSn);
          cb.Finish(null);
      Test.stopTest();
      System.assert(listSn!=null);
    }
      
  static testMethod void testSchedule() {
      String CRON_EXP = '0 0 1 * * ?';
      
      Test.startTest();
          // Schedule the test job
          String jobId = System.schedule('SnapshotReportMaintenanceScheduler', CRON_EXP, new SnapshotReportMaintenanceScheduler());
          // Get the information from the CronTrigger API object
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
          System.assertEquals(CRON_EXP,  ct.CronExpression);
      Test.stopTest();
      }
}