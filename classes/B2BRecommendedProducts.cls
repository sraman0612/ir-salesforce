/**
 * @author           Amit Datta
 * @description      manage All logic to support the Recommended Products.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  14/01/2024          Original Version
 **/

public with sharing class B2BRecommendedProducts {
	//private static final String RobB2BWEBSTOREURLPATHPREFIX = 'IRB2BWebstoreUrlPathPrefix';
	private static final String RobB2BWEBSTOREURL = B2B_Robuschi_Store_Setting__mdt.getInstance('RobuschiB2BWebstoreUrl').value__c;
	private static final String RobLOGOURL = B2B_Robuschi_Store_Setting__mdt.getInstance('Robuschi_logo_CMSUrl').value__c;

	public class CartItemWrapper {
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public String recId { get; set; }
		@AuraEnabled
		public String stockKeepingUnit { get; set; }
		@AuraEnabled
		public String description { get; set; }
		/*@AuraEnabled
		public String inletFilter { get; set; }
		@AuraEnabled
		public String serviceInterval { get; set; }*/
		@AuraEnabled
		public String fullUrl { get; set; }
		@AuraEnabled
		public String defaultImageUrl { get; set; }
		@AuraEnabled
		public String defaultImageTitle { get; set; }
		@AuraEnabled
		public String unitPrice { get; set; }
		@AuraEnabled
		public String currencyIsoCode { get; set; }
		@AuraEnabled
		public String quantity { get; set; }
		/*@AuraEnabled
		public String accountId { get; set; }
		@AuraEnabled
		public String webStoreId { get; set; }*/
	}

	private static List<Cross_Sell_Recommendations__c> getCrossSellProductsByProductId(String productId) {
		return [
			SELECT Id, Recommended_ProductId__c
			FROM Cross_Sell_Recommendations__c
			WHERE Recommended_Product_ParentId__c = :productId
			ORDER BY createdDate DESC
			LIMIT 6
		];
	}

	//@AuraEnabled(cacheable=true)
	public static ConnectApi.ProductDetail getProduct(String webstoreId, String productId, String effectiveAccountId) {
		ConnectApi.ProductDetail pd = null;
		if (!Test.isRunningTest()) {
			pd = ConnectApi.CommerceCatalog.getProduct(webstoreId, productId, effectiveAccountID, null, false, null, false, true, false);
		}
		return pd;
	}
	//@AuraEnabled(cacheable=true)
	public static ConnectApi.ProductPrice getProductPrice(String webstoreId, String productId, String effectiveAccountId) {
		ConnectApi.ProductPrice price = null;
		if (!Test.isRunningTest()) {
			price = ConnectApi.CommerceStorePricing.getProductPrice(webstoreId, productId, effectiveAccountId);
		}
		return price;
	}
	private static CartItemWrapper getCartItemWrapper(
		ConnectApi.ProductDetail productDetail,
		ConnectApi.ProductPrice productPrice,
		String effectiveAccountID,
		String webstoreID
	) {
		CartItemWrapper ciw = new CartItemWrapper();
		ciw.name = productDetail?.fields?.get('Name');
		ciw.recId = productDetail?.Id;
		ciw.stockKeepingUnit = productDetail?.fields?.get('StockKeepingUnit');
		ciw.description = productDetail?.fields?.get('Short_Product_Description__c');
		//ciw.inletFilter = productDetail?.fields?.get('Inlet_Filter__c');
		//ciw.serviceInterval = productDetail?.fields?.get('Service_Interval__c');
		ciw.fullUrl = RobB2BWEBSTOREURL + '/product/' + productDetail?.Id;
		ciw.defaultImageUrl = productDetail?.defaultImage.url == '/img/b2b/default-product-image.svg' ? RobLOGOURL : productDetail?.defaultImage.url;
		ciw.defaultImageTitle = productDetail?.defaultImage.title;
		ciw.unitPrice = productPrice?.unitPrice;
		ciw.currencyIsoCode = productPrice?.currencyIsoCode;
		//ciw.accountId = effectiveAccountID;
		//ciw.webStoreId = webstoreID;
		ciw.quantity = '1';
		return ciw;
	}

	/*@AuraEnabled
    public static ConnectApi.CartItem addToCart(String webStoreId, String productId, String quantity, String effectiveAccountId) {
        ConnectApi.CartItemInput cartInput = new ConnectApi.CartItemInput();
        cartInput.productId = productId;
        cartInput.quantity = quantity;
        cartInput.type = ConnectApi.CartItemType.PRODUCT;
        ConnectApi.CartItem cartItem = null;
        if(!Test.isRunningTest()){
            cartItem = ConnectApi.CommerceCart.addItemToCart(webstoreId, effectiveAccountId, 'active', cartInput);
        }
        return cartItem;
    }*/

	@AuraEnabled(cacheable=true)
	public static List<CartItemWrapper> getCSRecommendationProductsByProductId(String productId, String communityId) {
		if (productId == null || String.isBlank(productId)) {
			return null;
		}
		List<CartItemWrapper> returnList = new List<CartItemWrapper>();

		String webStoreId = null;
		String effectiveAccountId = B2BCommerceUtils.getAccountIdFromUser();

		List<ConnectApi.ProductDetail> myProductsInformations = new List<ConnectApi.ProductDetail>();
		Set<Id> recomProductIdSet = new Set<Id>();

		//B2B_IR_Store_Setting__mdt customPermissionIR = B2B_IR_Store_Setting__mdt.getInstance(IRB2BWEBSTOREURLPATHPREFIX);
		webStoreId = B2BCommerceUtils.resolveCommunityIdToWebstoreId(communityId);

		for (Cross_Sell_Recommendations__c csr : getCrossSellProductsByProductId(productId)) {
			recomProductIdSet.add(csr.Recommended_ProductId__c);
		}
		System.debug('recomProductIdSet>>>' + recomProductIdSet);
		ConnectApi.ProductDetail productDetail;
		ConnectApi.ProductPrice productPrice;

		for (Id prdId : recomProductIdSet) {
			productDetail = B2BRecommendedProducts.getProduct(webstoreID, prdId, effectiveAccountID);
			productPrice = B2BRecommendedProducts.getProductPrice(webstoreID, prdId, effectiveAccountID);
			returnList.add(getCartItemWrapper(productDetail, productPrice, effectiveAccountID, webstoreID));
			//myProductsInformations.add(productDetail);
		}
		System.debug('returnList>>>' + returnList);

		return returnList;
	}
}