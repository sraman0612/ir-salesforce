public with sharing class AssignmentGroupController {
    @InvocableMethod(label='Find unique distributors' description='This method will return the unique distributors')
    public static List<List<Id>> invoke(List<Wrapper> theList){
        Lead oLead = [Select Id, Country, VPTECH_Brand__c, Business__c FROM Lead where Id = :theList[0].leadId];
        List<Assignment_Groups__c> grpMembers = new List<Assignment_Groups__c>();
        if(oLead.Business__c == 'Milton Roy' && theList[0].territoryName == 'Distributor Territory'){
            grpMembers = [Select Id, Active__c, Group_Name__c, Country__c, Brand__c, DISTID__c
                                                 FROM Assignment_Groups__c 
                                                 WHERE Active__c = 'true' AND GroupName__c = :System.label.MR_Distributor_Territory 
                                                 AND Brand__c = :oLead.VPTECH_Brand__c AND Country__c = :oLead.Country];
            
        }
        else if(oLead.Business__c == 'Milton Roy' && theList[0].territoryName == 'RSM Territory'){
            grpMembers = [Select Id, Active__c, GroupName__c, Country__c, Brand__c, RSMID__c
                                                 FROM Assignment_Groups__c 
                                                 WHERE Active__c = 'true' AND GroupName__c = :System.label.MR_RSM_Territory 
                                                 AND Brand__c = :oLead.VPTECH_Brand__c AND Country__c = :oLead.Country];
        }
        else if(oLead.Business__c == 'MP/OB/IRP' && theList[0].territoryName == 'Distributor Territory'){
            grpMembers = [Select Id, Active__c, Group_Name__c, Country__c, Brand__c, DISTID__c
                                                 FROM Assignment_Groups__c 
                                                 WHERE Active__c = 'true' AND GroupName__c = :System.label.MP_OB_IRP_Distributor_Territory 
                                                 AND Brand__c = :oLead.VPTECH_Brand__c AND Country__c = :oLead.Country];
        }
        else if(oLead.Business__c == 'ARO' && theList[0].territoryName == 'Distributor Territory'){
            grpMembers = [Select Id, Active__c, Group_Name__c, Country__c, Brand__c, DISTID__c
                                                 FROM Assignment_Groups__c 
                                                 WHERE Active__c = 'true' AND GroupName__c = :System.label.ARO_Distributor_Territory 
                                                 AND Brand__c = :oLead.VPTECH_Brand__c AND Country__c = :oLead.Country];
            
        }
        else if(oLead.Business__c == 'ARO' && theList[0].territoryName == 'RSM Territory'){
            grpMembers = [Select Id, Active__c, Group_Name__c, Country__c, Brand__c, RSMID__c
                                                 FROM Assignment_Groups__c 
                                                 WHERE Active__c = 'true' AND GroupName__c = :System.label.ARO_RSM_Territory
                                                 AND Brand__c = :oLead.VPTECH_Brand__c AND Country__c = :oLead.Country];
        }
        
        List<Id> uniqueGrpMembers = new List<Id>();
        Set<String> uniqueIdSet = new Set<String>();
        if(grpMembers.size() > 0){
            
            if(theList[0].territoryName == 'Distributor Territory'){
                for(Assignment_Groups__c grpMem : grpMembers){
                if(!uniqueIdSet.contains(grpMem.DISTID__c)){
                    uniqueIdSet.add(grpMem.DISTID__c);
                    uniqueGrpMembers.add(grpMem.Id);
                }
            }
           }
            else if (theList[0].territoryName == 'RSM Territory'){
                 for(Assignment_Groups__c grpMem : grpMembers){
                if(!uniqueIdSet.contains(grpMem.RSMID__c)){
                    uniqueIdSet.add(grpMem.RSMID__c);
                    uniqueGrpMembers.add(grpMem.Id);
                }
            }
            }
            
            List<List<Id>> uniqueGrpSet = new List<List<Id>>();
            uniqueGrpSet.add(uniqueGrpMembers);
            return uniqueGrpSet;
        }else{
            return Null;
        }
    }
    
    public class Wrapper {
        @InvocableVariable(required= true)
        public String leadId;
        @InvocableVariable(required= true)
        public String territoryName;
       
    }
}