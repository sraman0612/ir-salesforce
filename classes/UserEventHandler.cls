/*
General Purpose class to consolidate business logic for trigger events on User
Created: 21Jan2016
*/

public class UserEventHandler {
    public User[] newLst = new User[]{};
        public User[] oldLst = new User[]{};
            public Map <ID, User> newMap = new Map<ID, User>{};
                public Map <ID, User> oldMap = new Map<ID, User>{};
                    	
                    /* constructor */
                    public UserEventHandler(User[] triggered, User[] triggeredFrom,
                                            Map<ID,User> triggerNewMap,Map<ID,User> triggerOldMap){
                                                newLst = triggered;
                                                oldLst = triggeredFrom;
                                                newMap = triggerNewMap;
                                                oldMap = triggerOldMap;
                                            }
    
    public void pavilionGroupInsert() {
//Commented as part of EMEIA cleaup    
/*
        List<Id> pavilionUserIDs = new List<Id>();
        Set<Id> ccCommProfSet = new Set<Id>();
        if(null!=PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId') && null!=PavilionSettings__c.getInstance('PavillionCommunityProfileId')){
           // ccCommProfSet.add(PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c);
            //ccCommProfSet.add(PavilionSettings__c.getInstance('PavillionCommunityProfileId').Value__c);
            for (User u: newLst) {
                if(ccCommProfSet.contains(u.profileid))
                    pavilionUserIDs.add(u.id);
            }
            if (!pavilionUserIDs.isEmpty() && !System.isBatch() && !System.isFuture()){
                UserEventHandler.insertPublicGroupMembers(pavilionUserIDs);
               UserEventHandler.shareAccountsWithPavilionUsers(pavilionUserIDs);  
            }  
        }*/
    }
    
    public void updateUserFromEmployee(){
        //Commented as part of EMEIA cleaup
        // Map<Id, Profile> ccProfileMap = new Map<Id, Profile>([select Name, Id from Profile where name like 'CC_%']);
       /* Set<string> corpIds = new Set<string>();
        for (User u : newLst) {
            if (!u.isPortalEnabled && null != u.FederationIdentifier && ccProfileMap.containsKey(u.ProfileId) && (null==oldMap || u.FederationIdentifier != oldMap.get(u.id).FederationIdentifier )){
                corpIds.add(u.FederationIdentifier.toUpperCase());
            }
        }
        if (!corpIds.isEmpty()) {IREmployeeEventHandler.updateUserFromEmployee(corpIds);}
*/
    }
    
    public void assignPermissionSets(){
        String pricingPermSetId = null == PavilionSettings__c.getInstance('PricingPermissionSetID') ? '': PavilionSettings__c.getInstance('PricingPermissionSetID').Value__c;
        String SFAPermSetId = null == PavilionSettings__c.getInstance('SFAPermissionSetID') ? '' : PavilionSettings__c.getInstance('SFAPermissionSetID').Value__c;
        List<String> permissionSetActionLst = new List<String>();
        /*Commented as part of EMEIA cleaup
        for (User u : newLst) {
            if (u.isActive){
                //check for pricing permission
                if (u.CC_View_Parts_Order_Invoice_Pricing__c && null==oldMap){
                    permissionSetActionLst.add('add::'+pricingPermSetId+'::'+u.Id);
                } else if (null!=oldMap && u.CC_View_Parts_Order_Invoice_Pricing__c!= oldMap.get(u.Id).CC_View_Parts_Order_Invoice_Pricing__c){
                    if(u.CC_View_Parts_Order_Invoice_Pricing__c){
                        permissionSetActionLst.add('add::'+pricingPermSetId+'::'+u.Id);
                    } else {
                        permissionSetActionLst.add('revoke::'+pricingPermSetId+'::'+u.Id);
                    }
                }
                //check for SFA permission
                if (u.CC_Sales__c && null==oldMap){
                    permissionSetActionLst.add('add::'+SFAPermSetId+'::'+u.Id);
                } else if (null!=oldMap && u.CC_Sales__c!= oldMap.get(u.Id).CC_Sales__c){
                    if(u.CC_Sales__c){
                        permissionSetActionLst.add('add::'+SFAPermSetId+'::'+u.Id);
                    } else {
                        permissionSetActionLst.add('revoke::'+SFAPermSetId+'::'+u.Id);
                    }
                }
            }
        }
        if (!permissionSetActionLst.isEmpty()){updatePermissionSets(permissionSetActionLst);}*/
    }
    
    public void addCORPId2Case(){
        //Commented as part of EMEIA cleaup
        /*Set<Id> ccCommProfSet = new Set<Id>();
        Map<Id,String> linksUserMap = new Map<Id, String>();
        if(null!=PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId') && null!=PavilionSettings__c.getInstance('PavillionCommunityProfileId')){
           // ccCommProfSet.add(PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c);
           // ccCommProfSet.add(PavilionSettings__c.getInstance('PavillionCommunityProfileId').Value__c);
            for (User u: newLst) {
                if(ccCommProfSet.contains(u.profileid) && null != u.FederationIdentifier && u.FederationIdentifier != oldMap.get(u.id).FederationIdentifier)
                    linksUserMap.put(u.Id,u.FederationIdentifier);
            }
            if (!linksUserMap.isEmpty()){
                UserEventHandler.updateCases(linksUserMap);
            }  
        }*/
    }

    //Method added by @Priyanka Baviskar on 7/16/2019 for Ticket 8271781: To add the CC Community Users to AccountTeam on user update. 
    public void addAccTeamMember(Map<Id, User> newUserMap, Map<Id, User> oldUserMap){
        //Commented as part of EMEIA cleaup
       /* system.debug('---addAccTeamMember');
        String jsonOldMap = JSON.serialize(oldUserMap);
        String jsonNewMap = JSON.serialize(newUserMap);
        List<Id> pavilionUserIDs = new List<Id>();
       // Set<Id> ccCommProfSet = new Set<Id>();

        if(null != PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId') && null != PavilionSettings__c.getInstance('PavillionCommunityProfileId')){

           // ccCommProfSet.add(PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c);
           // ccCommProfSet.add(PavilionSettings__c.getInstance('PavillionCommunityProfileId').Value__c);
            
            for (User u: newLst) {
            
                if(ccCommProfSet.contains(u.profileid))
                    pavilionUserIDs.add(u.id);
            }          

            if (!pavilionUserIDs.isEmpty()){                
                UserEventHandler.addAccTeamMemFuture(pavilionUserIDs, jsonOldMap, jsonNewMap);  
            }  
        }*/
    }
    
    //Method added by @Priyanka Baviskar on 7/16/2019 for Ticket 8271781: To add the CC Community Users to AccountTeam on user update. 
    @Future
    public static void addAccTeamMemFuture(List<Id> users,String stroldMap, String strnewMap){
//Commented as part of EMEIA cleaup
        system.debug('---addAccTeamMemFuture');
       /* Map<Id, User> oldMap = (Map<id, User>) JSON.deserialize(stroldMap, Map<Id, User>.class);
        Map<Id, User> newMap = (Map<id, User>) JSON.deserialize(strnewMap, Map<Id, User>.class);
        
        // Only process active users with Customer View (CC_CustomerVIEW__c) or SFA (CC_Sales__c) permissions 
        List<User> paviliionUsersList = [Select Id, AccountId From User Where Id IN : users and IsActive = True and (CC_CustomerVIEW__c = true or CC_Sales__c = true)]; 
        List<AccountTeamMember> tobeaddedAccountTeamMember = new List<AccountTeamMember>();
        
        for(User usr : paviliionUsersList) {

            // Adding Account Team for default access
            for (User us : newMap.values()) {

                User oldUs = oldMap.get(us.Id);

                // Only add to account team if the Siebel ID is changing
                if (us.CC_Siebel_Id__c != oldUs.CC_Siebel_Id__c) {
                    tobeaddedAccountTeamMember.add(new AccountTeamMember(AccountId = usr.AccountId, TeamMemberRole = 'Pavilion User', UserId = usr.Id));
                }
            }
        }
        
        Database.SaveResult[] atmSaveResultList = Database.insert(tobeaddedAccountTeamMember, false);   
        system.debug('---atmSaveResultList: ' + atmSaveResultList);   */     
    }
    /*
    @future
    public static void insertPublicGroupMembers(List<Id> users){
        List<GroupMember> groupmembers = new List<GroupMember>();
        string groupID = PavilionSettings__c.getAll().get('PavilionPublicGroupID').Value__c;
        for (Id UID : users) {
            GroupMember member = new GroupMember();
            member.GroupId = groupID;
            member.UserOrGroupId =  UID ;
            groupmembers.add(member);
        }
        if(!test.isrunningtest())
            INSERT groupmembers;
    }
    
    
    @future
    public static void shareAccountsWithPavilionUsers(List<Id> users) {
        
        List<User> paviliionUsersList = new List<User>();
        //List<AccountTeamMember> tobeaddedAccountTeamMember = new List<AccountTeamMember>();
        List<AccountShare> newAccountShares = new List<AccountShare>();
        
        paviliionUsersList = [Select Id,AccountId from User where Id IN : users]; 
        
        for(User usr : paviliionUsersList) {
            // Adding Account Team for deafult access
            // Code Commented by @Priyanka Baviskar on 7/16/2019 for Ticket 8271781: To add the CC Community User to AccountTeam on user update rather than on insert. 
            //AccountTeamMember atm = new AccountTeamMember(AccountId = usr.AccountId,TeamMemberRole = 'Pavilion User',UserId = usr.Id);
            //tobeaddedAccountTeamMember.add(atm);
            // Adding Account Shares to extend Edit access on Accounts
            newAccountShares.add(new AccountShare(UserOrGroupId=usr.Id, AccountId=usr.AccountId, 
                                                  AccountAccessLevel='Edit',OpportunityAccessLevel='Read'));
        }
        //Commented by @Priyanka Baviskar on 7/16/2019 for Ticket 8271781: To add the CC Community User to AccountTeam on user update rather than on insert. 
        //Database.SaveResult[] atmSaveResultList = Database.insert(tobeaddedAccountTeamMember,false);   
        Database.SaveResult[] accShareSaveResultList = Database.insert(newAccountShares,false); 
    }
    */
    /* Method to send email notifications */
    @future
    public static void sendNotificationEmail(boolean SelectEmail,string username,string contactid){
        try{
            string strUsrBody='';
            string strMailTo='';
            System.debug('SelectEmail==='+SelectEmail);
            System.debug('username==='+username);
            System.debug('contactid==='+contactid);
            if(SelectEmail==true)
                strUsrBody='Give the Grant Access to the user '+username+' ';   
            else
                strUsrBody='Remove the Grant Access to the user '+username+' '; 
            
            string OrderId='';
            List<Contact> lstcontactMail =new List<Contact>();
            //Commented as part of EMEIA cleaup
            /*List<CC_Order__c> lstMailOrders=new List<CC_Order__c>();
            lstcontactMail =[Select AccountId ,Email,CC_Contact_Types__c from contact where Id=:contactid and CC_Contact_Types__c='Sales Manager' limit 1];
            
            if(lstcontactMail !=null && lstcontactMail .size()>0) 
            { 
                if(lstcontactMail[0].Email!=null && lstcontactMail[0].Email!='')           
                    strMailTo=lstcontactMail[0].Email;
            }           
            */
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'EMEIACLean.123@gmail.com'};  
                mail.setToAddresses(toAddresses);
            
            mail.setSubject('Grant Access to the User  : ' + username); 
            
            mail.setHtmlBody(strUsrBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }        
        catch(Exception ex){
        }
        
    }
    /*
    @future 
    public static void updatePermissionSets(List<String> permSetActionLst){
        Set<Id> userIDs = new Set<Id>();
        Set<Id> permSetIDs = new Set<Id>();
        Set<String> revokeKeys = new Set<String>();
        PermissionSetAssignment [] permissionSetAssignments2Delete = new List<PermissionSetAssignment>();
        PermissionSetAssignment [] permissionSetAssignments2Create = new List<PermissionSetAssignment>();
        for (String s : permSetActionLst){
            String[] strArr = s.split('::');
            String type = strArr[0];
            if(type=='revoke' && '' != strArr[1]){
                permSetIDs.add(strArr[1]);
                userIDs.add(strArr[2]);
                revokeKeys.add(strArr[1]+''+strArr[2]);
            } else if(type=='add' && '' != strArr[1]) {
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = strArr[1], AssigneeId=strArr[2]);
                permissionSetAssignments2Create.add(psa);
            }
        }
        for (PermissionSetAssignment psa : [SELECT Id,PermissionSetId,AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permSetIDs and AssigneeId In :userIDs]){
            system.debug('### key from assignmetn set is ' + psa.PermissionSetId+''+psa.AssigneeId);
            if(revokeKeys.contains(psa.PermissionSetId+''+psa.AssigneeId)){
                permissionSetAssignments2Delete.add(psa);
            }
        }
        if(!permissionSetAssignments2Delete.isEmpty()){delete permissionSetAssignments2Delete;}
        if(!permissionSetAssignments2Create.isEmpty()){insert permissionSetAssignments2Create;}
    }
*/
    /*
    @future
    public static void updateCases(Map<Id, String> usrMap){
        Case [] casesToUpdate = new List<Case>();
        for (Case c: [SELECT Id, CC_CorpId__c,CC_User__c FROM CASE WHERE isClosed=FALSE and Reason='User Admin' and Sub_Reason__c='New User' and CC_CorpId__c=null and CC_User__c IN :usrMap.keySet()]){
           // c.CC_CorpId__c = usrMap.get(c.CC_User__c);
            c.Status = 'Closed';
            //c.CC_Root_Cause__c='User Complete';
            c.CTS_TechDirect_Disposition__c='Answer or Solution Provided';
            casesToUpdate.add(c);
        }
        if (!casesToUpdate.isEmpty())
            update casesToUpdate;
    }
    */
    //Code Contributed by @Priyanka Baviskar for ticket #8077192: Please add the Active and Partner User Checkmarks on Contact layout on 3/15/2019 
    public static void updateUsrIdonCont(Map<Id, User> newUserMap, Map<Id, User> oldUserMap ){
        Set<Id> contIdList = new Set<Id>();
        if(oldUserMap == null){
            for(User newUserRec: newUserMap.values()){
                if(newUserRec.contactId != null && newUserRec.IsPortalEnabled){
                    contIdList.add(newUserRec.contactId);
                }
            }
        } else {
            for(User newUserRec: newUserMap.values()){
                User oldUser = oldUserMap.get(newUserRec.Id);
                if(oldUser.contactId == null && 
                   newUserRec.contactId != null && 
                   newUserRec.IsPortalEnabled){
                       contIdList.add(newUserRec.contactId);
                   }
            }
        }
        //Commented as part of EMEIA cleaup
        if(contIdList != null){
            Map<Id, Contact> contMap = new Map<Id, Contact> (
                [Select Id/*, CC_Partner_Usr__c*/
                 From Contact Where Id In: contIdList]);
            List<Contact> contsToBeUpdated = new List<Contact>(); 
            for(User newUserRec: newUserMap.values()){
                
                Contact cont = contMap.get(newUserRec.contactId);
                if (cont != null) 
                {
                    //cont.CC_Partner_Usr__c= newUserRec.Id;
                    contsToBeUpdated.add(cont);
                }
            }
            
            if(contsToBeUpdated != null && contsToBeUpdated.size()>0 && !System.isBatch() && !System.isFuture()){
                updateContact(JSON.serialize(contsToBeUpdated),Test.isRunningTest());            
            }
        }
    }
    
    @future
    public static void updateContact(String updateContactList,Boolean testRunningCheck){
        List<Contact> updateContactList1 = new List<Contact>();
        if(updateContactList != null)
            updateContactList1 = (List<Contact>)JSON.deserialize(updateContactList, List<Contact>.class);
        if(!testRunningCheck)
            update updateContactList1;
    } // End of code Contributed by @Priyanka Baviskar for ticket #8077192.
    //updateContact method's arguments are changed to also used for PTL Contact's Partner flag update operation in updatePartnerFlagonContact method.
    
    
    //Code Contributed by Sairaman Koraveni based on JIRA SPTL-463. updates is_PTL_Partner_user__c field on Contact.
    public static void updatePartnerFlagonContact(List<User> newUsers) {
          
            if(newUsers != null){
            List<User> UserList = [SELECT Id,Name,isActive,Profile.Name,ContactId,Contact.RecordTypeName__c,Business__c FROM User WHERE Id IN :newUsers];
            List<Contact> contactsList = new List<Contact>();
          
            for(User us : UserList){
               
            	if(us.Profile.Name == 'Standard Partner'  && us.ContactId != null  && us.Contact.RecordTypeName__c == 'PTL_Contact')
                {	
                    Contact con = new Contact(Id = us.ContactId);
                    if(us.isActive == TRUE){
                        con.is_PTL_Partner_User__c = TRUE;
                    }
                    else{
                       con.is_PTL_Partner_User__c = FALSE;
                    	}
                    contactsList.add(con);
                }
            }
            if(contactsList != null && !System.isBatch() && !System.isFuture()){
                System.debug(contactsList);
                updateContact(JSON.serialize(contactsList),false);
            }
           
            
        }
       }
    
    
    
           
    
}