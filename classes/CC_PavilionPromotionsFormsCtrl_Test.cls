@isTest
public class CC_PavilionPromotionsFormsCtrl_Test {
    
    static testMethod void submitFormDataTest()
    {
        // Creation of Test Data
        Account Acc = TestUtilityClass.createAccount();
        insert Acc;
        System.assertEquals('Sample Account',Acc.Name);
        System.assertNotEquals(null, Acc.Id);
        Contact con = TestUtilityClass.createContact(Acc.Id);
        insert con;
        system.debug('the contact created is'+con.Id);
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(Acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
        // PavilionSettings__c records are created to handle User Events trigger
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        System.assertEquals('PavillionCommunityProfileId',firstPavnSettings.Name);
        System.assertNotEquals(null,firstPavnSettings.Id);
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        System.assertEquals('Sample Group',grp.Name);
        System.assertNotEquals(null,grp.Id);
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        System.assertEquals('PavilionPublicGroupID',secondPavnSettings.Name);
        System.assertNotEquals(null,secondPavnSettings.Id);
        PavilionSettings__c PavilionSettings=TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert PavilionSettings;
        User portaUser = TestUtilityClass.createUserBasedOnPavilionSettings(PavilionSettings.value__c, con.Id);
        insert portaUser;
        System.assertEquals(portaUser.ContactId,con.Id);
        System.assertNotEquals(false, portaUser.IsActive);
        system.runAs(portaUser)
        {
            String PromotionId;
            CC_PavilionPromotionsFormsController objCtrl = new CC_PavilionPromotionsFormsController();
            String attachmentName='Unit Test Attachment';
            String attachmentBody= EncodingUtil.base64Encode(Blob.valueOf('Testing base 64 encode'));
            
            String status = CC_PavilionPromotionsFormsController.submitGolfCarFormData('test', 'lname','101234',
                                                                                       'test@test.ingersollrand.com','test buyer', 
                                                                                       'buyerLastName','address','city',
                                                                                       'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                                       'serialNumber', '2012', 'make', '2000',
                                                                                       'Zip','word', 'CSV','word');    
            
            
            status = CC_PavilionPromotionsFormsController.submitGolfCarFormData('test', 'lname','101234',
                                                                                'test@test.ingersollrand.com','test buyer', 
                                                                                'buyerLastName','address','city',
                                                                                'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                                'serialNumber', '012012', 'make', '2000',
                                                                                'attachmentName','attachmentBody', 'attachmentName','attachmentBody');    
            
            status = CC_PavilionPromotionsFormsController.submitLSVFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '2012', '2000',
                                                                            'Zip','word', 'CSV','word');    
            
            
            status = CC_PavilionPromotionsFormsController.submitLSVFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '012012', '2000',
                                                                            'attachmentName','attachmentBody', 'attachmentName','attachmentBody');    
            
            status = CC_PavilionPromotionsFormsController.submitXRTFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '2012', '2000',
                                                                            'attachmentName','attachmentBody', 'attachmentName','attachmentBody');    
            
            
            status = CC_PavilionPromotionsFormsController.submitXRTFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '012012', '2000',
                                                                            'attachmentName','attachmentBody', 'attachmentName','attachmentBody'); 
                                                     
            status = CC_PavilionPromotionsFormsController.submitCommercialFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '2012', '2000',
                                                                            'attachmentName','attachmentBody', 'attachmentName','attachmentBody');  
                                                                            
                                                                            
            status = CC_PavilionPromotionsFormsController.submitCommercialFormData('test', 'lname','101234',
                                                                            'test@test.ingersollrand.com','test buyer', 
                                                                            'buyerLastName','address','city',
                                                                            'state', '12345' ,'123456789', 'test@test.ingersollrand.com', '03/02/2015', 'dealerName', 'vehicleModel',
                                                                            'serialNumber', '012012', '2000',
                                                                            'attachmentName','attachmentBody', 'attachmentName','attachmentBody');  
                                                                            
                                                                            
                                                                               
            status=CC_PavilionPromotionsFormsController.sendmail(promotionId);
            Messaging.SingleEmailMessage mail; 
            
        }
    }
}