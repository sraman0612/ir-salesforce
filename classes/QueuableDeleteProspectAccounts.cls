//Created by Capgemini Aman Kumar Date 8/1/2023 for US SFAVPTECH-105.  
public class QueuableDeleteProspectAccounts implements Queueable,Database.AllowsCallouts{
    public Business_Relationship__c brForDeletion = new Business_Relationship__c();
    public Account accForDeletion = new Account();
    
    public QueuableDeleteProspectAccounts(Business_Relationship__c bussinessRelationship, Account acc){
        brForDeletion = bussinessRelationship;
        accForDeletion = acc;
    }
    
    public void execute(QueueableContext context) {
        List<Sobject> recordsToBeDeleted =new List<Sobject>();
        
        if(brForDeletion !=null && accForDeletion != null){
            recordsToBeDeleted.add(accForDeletion);
            
        }else if(brForDeletion !=null && accForDeletion == null){
            recordsToBeDeleted.add(brForDeletion);
        }
        try{
            database.DeleteResult[] myResult=database.delete(recordsToBeDeleted,false);
        }catch(Exception e){
            ApexLogHandler logHandler = new ApexLogHandler('DeleteProspectAccounts -> catch', 'execute', e.getMessage());
            insert logHandler.logObj;
            
        }
        
    }
}