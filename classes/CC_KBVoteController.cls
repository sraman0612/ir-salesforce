/**************************
* CC_KBVoteController
****************************/
public class CC_KBVoteController { 
    //update the number of up and down votes for the given article
    @auraEnabled
    public static Object getInit(String recordId){
        List<Vote> votes = [SELECT Id, Type, LastModifiedById FROM Vote 
                            WHERE ParentId =: recordId AND LastModifiedById =: UserInfo.getUserId()];
        String vType = '';
        if(!votes.isEmpty()){
            vType = votes[0].Type;
        }
        
        String communityId = System.Label.CC_Community_ID == 'null' ? null : System.Label.CC_Community_ID;
        Boolean following = false;
		EntitySubscription[] subs = [SELECT Id FROM EntitySubscription WHERE NetworkId =: communityId AND ParentId =: recordId AND SubscriberId =: UserInfo.getUserId()];
        if(subs.size() > 0) following = true;
        
        return JSON.serialize(new List<Object>{vType, following});
    }
    /**
     * Up/Down Vote an Article
     * @param articleId
     * @param type
     * @return Boolean
     */
	@AuraEnabled
    public static Boolean vote(String aId, String type) {        
        try {
        	List<Vote> votes = [SELECT Id, Type, ParentId FROM Vote 
                                WHERE ParentId =: aId AND LastModifiedById =: UserInfo.getUserId()];
          if(!votes.isEmpty()) delete votes;
          Vote v = new Vote(ParentId = aId, Type = type);
          insert v;
          //create chatter post to knowledge creator
          ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
          ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
          ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
          ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
          messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
          mentionSegmentInput.id = [SELECT CreatedbyId FROM Knowledge__kav WHERE id = :aid OR KnowledgeArticleId = :aId LIMIT 1].CreatedById;
          messageBodyInput.messageSegments.add(mentionSegmentInput);
          textSegmentInput.text = ' Your article received a voted ' + type + '.';
          messageBodyInput.messageSegments.add(textSegmentInput);
          feedItemInput.body = messageBodyInput;
          feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
          feedItemInput.subjectId = aId;
          ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
          
        } catch(Exception e) {            
            System.debug('e**' + e.getMessage());
            return false;
        }      
        return true;
    }
    
    @AuraEnabled
    public static Boolean followUnfollow(String recId, String action) {
        String communityId = System.Label.CC_Community_ID == 'null' ? null : System.Label.CC_Community_ID;
        try {
            if(action == 'follow') {
            	//ConnectApi.ChatterUsers.follow(communityId, UserInfo.getUserId(), recId);
            	ConnectApi.ChatterUsers.follow(null, UserInfo.getUserId(), recId);
            } else {
            	EntitySubscription[] subs = [SELECT Id FROM EntitySubscription WHERE NetworkId =: communityId AND ParentId =: recId AND SubscriberId =: UserInfo.getUserId()];
                ConnectApi.Chatter.deleteSubscription(null, subs[0].Id);
            }
        } catch(Exception e) {
            throw new AuraException(e.getMessage());
        }
        return true;
    }
}