/*
Added Recordtype for PT_powertools
Arjun 6Feb2018
*/

public class PT_CreateScheduleOpp {

    @TestVisible private static Integer testMonths;
    
    @InvocableMethod
    public static void createOpps(List<Id> ParentIds){
        // Added PT_Lead__c for Case # 01212031
        List<PT_Parent_Opportunity__c> parents = [SELECT Id, Opportunity_Number__c, Account__c, Sales_Rep__c, Type__c, Year__c, OwnerId, Product_Group__c, 
                                                         Converted_from_PT_Lead__c, Lead_SourcePL__c, PT_Lead__c
                                                    FROM PT_Parent_Opportunity__c
                                                   WHERE Id in :ParentIds ];
        List<Opportunity> schedules = new List<Opportunity>();
        integer startMonth = [select FiscalYearStartMonth from Organization].FiscalYearStartMonth;
        // 8feb2018 CG:PT Org merge: added PT record id      
        Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();        
        for (PT_Parent_Opportunity__c p : parents){
            date startDate = date.newInstance(integer.valueOf(p.Year__c), startMonth, 1);
            
            Integer months = (Test.isRunningTest() && testMonths != null ? testMonths : 12);
            for(integer i=1;i<=months;i++){
                Opportunity o = new Opportunity(                    
                    Name = p.Opportunity_Number__c+'_'+(i<10 ?'0' : '')+i,
                    AccountId = p.Account__c,
                    CloseDate = (startDate.addMonths(i).toStartOfMonth())-1,
                    PT_Origin_Close_Date__c = (startDate.addMonths(i).toStartOfMonth())-1,
                    PT_Sales_Activity__c = p.Type__c,
                    PT_IR_Sales_Rep__c = p.Sales_Rep__c,
                    PT_Schedule__c = i, 
                    PT_parent_Opportunity__c = p.Id,
                    OwnerId = p.OwnerId,
                    PT_Product_Group__c = p.Product_Group__c,
                    RecordtypeId=PT_OpptyRecordTypeId, // 8feb2018 CG:PT Org merge: added PT record id
                    LeadSource = p.Lead_SourcePL__c,
                    PT_Lead__c = p.PT_Lead__c
                );
                if(i<10){
                    o.Name = p.Opportunity_Number__c+'_0'+i;
                } else o.Name = p.Opportunity_Number__c+'_'+i;
                if(p.Converted_from_PT_Lead__c){
                  o.StageName = '0.Plan';
                } else {
                  o.StageName = '0.Plan';
                }
                schedules.add(o);
            }
        }
        
        if(schedules.size()>0){
            insert schedules;
        }
    }
}