@isTest
public class CC_Lead_TriggerHandlerTest {
    static testmethod void testLeadAssignments(){

        TestDataUtility testData = new TestDataUtility();
        Id clubcarQueueId = [SELECT id,Name 
                               FROM Group
                              WHERE type='Queue' and 
                                    Name=:StringConstantsFinalClass.clubCarqueueName 
                              LIMIT 1].id;
        TestDataUtility.createStateCountries();
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1111');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        account acc = testData.createAccount(StringConstantsFinalClass.accountclubCarnewCustomerRectypename);
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        insert acc;
        contact con = testData.createContact(acc.id);
        con.recordtypeid = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        insert con;
        List<contract> contractsList = new List<contract>();
        contract contr1 = testData.createContract(acc.Id,StringConstantsFinalClass.productAuthorizationGolfUtility);
        contr1.CC_Primary_Sales_Reporting__c = '1111'; 
        contract contr2 = testData.createContract(acc.Id,'Retail Dealer');
        contr2.CC_Lead_Owner__c = userinfo.getUserId();
        contract contr3 = testData.createContract(acc.Id,'Commercial Utility');
        contractsList.add(contr1);
        contractsList.add(contr2);
        contractsList.add(contr3);
        insert contractsList;
        CC_Sales_Rep__c salesRep = testData.createSalesRep('12345');
        insert salesRep;
        List<CC_Territory__c> teritorriesList = new List<CC_Territory__c>();
        for(contract cont :contractsList){
            CC_Territory__c ter = testData.CreateTerritory(cont.Id);
            teritorriesList.add(ter);
        }
        insert teritorriesList;
        Test.startTest();
            List<lead> leadsList = new List<lead>();
            lead ld   = testData.createLead(StringConstantsFinalClass.clubCarRecordTypeName, StringConstantsFinalClass.productAuthorizationGolfUtility);
            ld.OwnerId = clubcarQueueId;
            leadsList.add(ld);
            lead ld1   = testData.createLead(StringConstantsFinalClass.clubCarRecordTypeName, 'Retail Dealer');
            ld1.OwnerId = clubcarQueueId;
            ld1.PostalCode = NULL;
            leadsList.add(ld1); 
            lead ld2   = testData.createLead(StringConstantsFinalClass.clubCarRecordTypeName, 'Commercial Utility');
            ld2.OwnerId = clubcarQueueId;
            ld2.PostalCode = NULL;
            ld2.State = NULL;
            leadsList.add(ld2);
            insert leadsList;
        Test.stopTest();
    }
}