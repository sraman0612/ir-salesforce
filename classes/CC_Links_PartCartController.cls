public class CC_Links_PartCartController {
  
  public static CC_Pavilion_Content__c pavContentPartCartBody1{get;set;}
  
  public static string getPartCartBody1() {
    String HeadingText= pavContentPartCartBody1.CC_Body_1__c;
    return HeadingText;
  }
    
  public static string getPartCartBody2() {
    String BodyText = pavContentPartCartBody1.CC_Body_2__c;
    return BodyText;
  }  
  
  public string getPartCartURL(){
    String retStr=null==PavilionSettings__c.getInstance('PartCartEndpoint')?'#':PavilionSettings__c.getInstance('PartCartEndpoint').Value__c;
    retstr += ('/' == retstr.right(1) ? '' : '/') +  '?remote_user=' + EncodingUtil.urlEncode(getPartCartAuthText(), 'UTF-8');
    return retStr;
  }
  
  public static String getPartCartAuthText(){
    User u = CC_PavilionTemplateController.getUserFields();
    String authToken = (null == PavilionSettings__c.getInstance('PartCartAuthToken')) ? 'NO-TOKEN': PavilionSettings__c.getInstance('PartCartAuthToken').Value__c; 
    String authKey   = (null == PavilionSettings__c.getInstance('PartCartAuthKey')) ? EncodingUtil.base64Encode(Crypto.generateAesKey(128)) : PavilionSettings__c.getInstance('PartCartAuthKey').Value__c;
    String authIV    = (null == PavilionSettings__c.getInstance('PartCartAuthIV')) ? '0000000000000000' :PavilionSettings__c.getInstance('PartCartAuthIV').Value__c;
    string fedID = null == u.FederationIdentifier ? 'CCDefault' : u.FederationIdentifier;
    String authStr = fedID + '|||' +
                     u.FirstName + '|||' +
                     u.Lastname + 
                     '|||clubcar|||' +
                     u.email +
                     '|||CCDealerViewOnly|||' + 
                     'Club Car' + 
                     '|||0|||0|||0|||0|||0|||0|||0|||0|||' +
                     authToken + '|||' +
                     datetime.now().format('MM/dd/yyyy hh:mm:ss a', 'America/New_York');
    authStr = EncodingUtil.base64Encode(Crypto.encrypt('AES128', 
                                                        EncodingUtil.base64Decode(authKey),
                                                        Blob.valueOf(authIV), 
                                                        Blob.valueOf(authStr)));
    return authStr;
  }  
    
  public CC_Links_PartCartController(CC_PavilionTemplateController controller) {
    pavContentPartCartBody1 = [SELECT CC_Body_1__c, CC_Body_2__c  FROM CC_Pavilion_Content__c WHERE Title__c = 'Part Cart Page' and RecordType.Name = 'Part Cart'];
  }
}