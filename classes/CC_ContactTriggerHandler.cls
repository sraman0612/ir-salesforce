public class CC_ContactTriggerHandler {
  
    /***********************************************************************
     * Description   : Account, Contact and Opportunity are integrated to Siebel. 
     *                 When one of these is deleted in Salesforce we need to send 
     *                 an outbound message to Siebel so they can delete on their side.
     * Created Date  : 14/06/2017
     * **********************************************************************/
    public static void afterDelete(Map<String, List<Contact>> oldMap){
        List<Contact> clubcarContacts       = new List<Contact>();
        list<OBM_Notifier__c> notifiersList = new list<OBM_Notifier__c>();
        Set<Id> masterIdSet = new set<Id>();
        for(String recType : oldMap.keySet()){
            if(recType.contains('Club_Car') && !oldMap.get(recType).isEmpty() ){
                clubcarContacts.addAll(oldMap.get(recType));   
            }
        }
        for(contact con:clubcarContacts){
            if(con.MasterRecordId != null){masterIdSet.add(con.MasterRecordId);}
        }
        map<Id, String> masterExtIdMap = new map<Id, String>();
        if(!masterIdSet.isEmpty()){
            for(Contact masterContact : [SELECT Id, Siebel_ID__c,CC_Siebel_Id__c 
                                         FROM Contact
                                         WHERE  Id IN :masterIdSet]){
                masterExtIdMap.put(masterContact.Id, masterContact.CC_Siebel_Id__c);
            }   
        }        
        OBM_Notifier__c notifier;
        for(Contact con : clubcarContacts){  
            notifier = CC_Utility.createOBMNotifier(con, 'Contact');
            if(con.MasterRecordId != null && masterExtIdMap.containsKey(con.MasterRecordId)){
                notifier.SFDC_Id__c = con.MasterRecordId;
                notifier.Master_External_Id__c = masterExtIdMap.get(con.MasterRecordId);
            }            
            notifiersList.add(notifier);                
        }
        
        if(!notifiersList.isEmpty()){insert notifiersList;}
    }
    
    
     /***********************************************************************
     * Description   : when a contact is marked as primary 
     *                 we need to uncheck the primary flag 
     *                 on any other Contacts for that Account
     * Created Date  : 29/06/2017
     * **********************************************************************/
    
  
    public static void updatePrimaryFlag(Map<String, List<Contact>> newMap,Map<String, List<Contact>> oldMap){
        
        Map<Id,Contact> conNewMap = new Map<Id,Contact>();
        Map<Id,Contact> conOldMap = new Map<Id,Contact>();
        set<Id> accIds            = new Set<Id>();
        List<contact> contactstoUpdate = new List<contact>();
        
        if(newMap.containsKey('Club_Car') && !newMap.get('Club_Car').isEmpty()){
            conNewMap = new Map<Id,Contact>(newMap.get('Club_Car'));
        }
        if(oldMap.containsKey('Club_Car') && !oldMap.get('Club_Car').isEmpty()){
            conOldMap = new Map<Id,Contact>(oldMap.get('Club_Car'));
        }
        
        if(!conNewMap.isEmpty()){
            for(contact con : conNewMap.values()){
                if((trigger.isInsert || (trigger.isUpdate  && con.Primary__c != conOldMap.get(con.id).Primary__c)) && con.Primary__c){
                    accIds.add(con.AccountId);
                }
            }
        }
        
        if(!accIds.isEmpty()){
            for(contact con : [select id,Primary__c from contact 
                               where AccountID IN:accIds And Id Not IN:conNewMap.keySet()
                                     and RecordType.DeveloperName='Club_Car']){
                     con.Primary__c = false;
                     contactstoUpdate.add(con);              
              }
        }
        
        if(!contactstoUpdate.isEmpty()){
            try{
                update contactstoUpdate;
            }
            catch(exception ex){
                system.debug('@@@@exception'+ex);
            }
        }

    }
    
    
    
    
}