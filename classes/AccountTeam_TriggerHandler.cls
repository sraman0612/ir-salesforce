//
// (c) 2015, Appirio Inc.
//
// Handler class for AccountTeam Trigger
//
// August 27,2015    Siddharth Varshneya    T-429865
//
public class AccountTeam_TriggerHandler { 
    //public static void afterInsert(List<Account_Team__c> accTeam){
   //     createNewAccountTeamMember(accTeam);
   // }
    
    public static void beforeInsert(List<Account_Team__c> accTeam){
        createNewAccountTeamMember(accTeam);
    }
    
    public static void beforeUpdate(List<Account_Team__c> newList, Map<Id,Account_Team__c> oldMap){
        updateAccountTeamMember(newList);
    }
    
    public static void afterDelete(List<Account_Team__c> accList){   
        deleteAccountTeamMember(accList);
    }
    
    public static void updateAccountTeamMember(List<Account_Team__c> accTeamMembers){
        map<Id,Account_Team__c> mapAccTeamIds = new map<Id,Account_Team__c>();
        map<String,AccountShare> mapSharing = new map<String,AccountShare>();
        List<AccountShare> shareTeamList = new List<AccountShare>();
        set<ID> accIds = new set<Id>();
        set<ID> userIds = new set<Id>();
        List<AccountTeamMember> accTeamMemList = new List<AccountTeamMember>(); 
        for(Account_Team__c accTeam : accTeamMembers){
            mapAccTeamIds.put(accTeam.AccountTeamMemberId__c,accTeam);
            userIds.add(accTeam.Team_Member__c);
            accIds.add(accTeam.Acct__c);
        }
        
        List<AccountShare> lstSharing = [Select AccountId, UserOrGroupId, Id, OpportunityAccessLevel,CaseAccessLevel,ContactAccessLevel, AccountAccessLevel
                                            From AccountShare 
                                            WHERE AccountId IN : accIds AND UserOrGroupId IN : userIds];
        for(AccountShare shr : lstSharing){
            mapSharing.put(shr.AccountId+'_'+shr.UserOrGroupId,shr);
        }
                                            
        system.debug('==lstSharing=='+lstSharing);
        List<AccountTeamMember> lstATM = [Select UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel
                                            From AccountTeamMember 
                                            WHERE Id IN : mapAccTeamIds.keySet()];
        for(AccountTeamMember accTeam : lstATM){
            if(accTeam.TeamMemberRole != mapAccTeamIds.get(accTeam.Id).Role__c || accTeam.AccountId != mapAccTeamIds.get(accTeam.Id).Acct__c || accTeam.UserId != mapAccTeamIds.get(accTeam.Id).Team_Member__c 
             || mapSharing.get(accTeam.AccountId+'_'+accTeam.UserId).AccountAccessLevel != mapAccTeamIds.get(accTeam.Id).Acct_Access_Level__c 
             || mapSharing.get(accTeam.AccountId+'_'+accTeam.UserId).OpportunityAccessLevel != mapAccTeamIds.get(accTeam.Id).Opportunity_Access_Level__c 
             || mapSharing.get(accTeam.AccountId+'_'+accTeam.UserId).CaseAccessLevel != mapAccTeamIds.get(accTeam.Id).Case_Access_Level__c 
             ||mapSharing.get(accTeam.AccountId+'_'+accTeam.UserId).ContactAccessLevel != mapAccTeamIds.get(accTeam.Id).Contact_Access_Level__c){
                accTeam.TeamMemberRole = mapAccTeamIds.get(accTeam.Id).Role__c;
                accTeamMemList.add(accTeam);
                
                AccountShare acctShare = new AccountShare();
                acctShare.AccountId = mapAccTeamIds.get(accTeam.Id).Acct__c; 
                acctShare.UserOrGroupId = mapAccTeamIds.get(accTeam.Id).Team_Member__c;
                acctShare.AccountAccessLevel = mapAccTeamIds.get(accTeam.Id).Acct_Access_Level__c;
                acctShare.OpportunityAccessLevel = mapAccTeamIds.get(accTeam.Id).Opportunity_Access_Level__c;
                acctShare.CaseAccessLevel = mapAccTeamIds.get(accTeam.Id).Case_Access_Level__c;
               //acctShare.ContactAccessLevel = mapAccTeamIds.get(accTeam.Id).Contact_Access_Level__c;
                shareTeamList.add(acctShare);
            }
        }
        
        if(accTeamMemList.size() > 0){
            update accTeamMemList;
        }
        if(shareTeamList.size() > 0){
            insert shareTeamList;
        }
    }
    
    public static void createNewAccountTeamMember(List<Account_Team__c> accTeamMembers){
        List<Account_Team__c> accTeamList = new List<Account_Team__c>();
        List<AccountShare> shareTeamList = new List<AccountShare>();
        map<String,Account_Team__c> mapAccTeamList = new map<String,Account_Team__c>();
        List<AccountTeamMember> accTeamMemList = new List<AccountTeamMember>();
        Set<Id> accTeamId = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        map<Id,Id> mapAccOwnerId = new map<Id,Id>();
        for(Account_Team__c accTeam : accTeamMembers){
            accIds.add(accTeam.Acct__c);
        }
        
        list<Account> lstAcc= [SELECT Id,OwnerId FROM Account WHERE Id IN: accIds];
        for(Account acc: lstAcc){
            mapAccOwnerId.put(acc.Id,acc.OwnerId);
        }
        
        for(Account_Team__c accTeam : accTeamMembers){
            mapAccTeamList.put(accTeam.Acct__c +'_'+accTeam.Team_Member__c,accTeam);
            
            AccountTeamMember accTeamMember = new AccountTeamMember();
                
            accTeamMember.AccountId = accTeam.Acct__c;  
            accTeamMember.UserId = accTeam.Team_Member__c;
            accTeamMember.TeamMemberRole = accTeam.Role__c;
            if(accTeam.Role__c == null){
                accTeamMember.TeamMemberRole = 'Sales Rep';
            }
            accTeamMemList.add(accTeamMember);  
            
            if(mapAccOwnerId.get(accTeam.Acct__c) != accTeam.Team_Member__c){
                AccountShare acctShare = new AccountShare();
                acctShare.AccountId = accTeam.Acct__c; 
                acctShare.UserOrGroupId = accTeam.Team_Member__c;
                acctShare.AccountAccessLevel = accTeam.Acct_Access_Level__c;
                acctShare.OpportunityAccessLevel = accTeam.Opportunity_Access_Level__c;
                acctShare.CaseAccessLevel = accTeam.Case_Access_Level__c;
               // acctShare.ContactAccessLevel = accTeam.Contact_Access_Level__c;
                shareTeamList.add(acctShare);
                System.debug('==acctShare==' +acctShare); 
                System.debug('new account team member ------------------------' +accTeamMember.AccountId); 
            }
        }
        
        if(accTeamMemList.size() > 0){
            insert accTeamMemList;
        }
        if(shareTeamList.size() > 0){
            insert shareTeamList;
        }
        
        for(AccountTeamMember accTeam : accTeamMemList){
            mapAccTeamList.get(accTeam.AccountId +'_'+accTeam.UserId).AccountTeamMemberId__c = accTeam.Id;
        }
    } 
    
    public static void deleteAccountTeamMember(List<Account_Team__c> accTeamMembers){
        Set<Id> accTeamIds = new Set<Id>();
        for(Account_Team__c accTeam : accTeamMembers){
            accTeamIds.add(accTeam.AccountTeamMemberId__c);
        }
         System.debug('==accTeamIds==' +accTeamIds); 
        List<AccountTeamMember> lstATM = [Select UserId, TeamMemberRole, Id, AccountId, AccountAccessLevel From AccountTeamMember WHERE Id IN : accTeamIds];
        System.debug('==lstATM==' +lstATM); 
        if(lstATM.size() > 0){
            delete lstATM;
        }
    }
}