global with sharing class CC_PavilionWarrPoliciesCont
{

    public CC_PavilionWarrPoliciesCont(CC_PavilionTemplateController controller) {

    }

    /*-------retrieves the list of Warranty Policies since 2009 ----- */             
    
    global static List<ContentVersion> getPolicyList()
    {
        List<ContentVersion> policyList = new List<ContentVersion>();
        policyList = [select id, ContentDocumentId, Title, CC_Year__c, Content_title__c, Content_Sub_title__c, FileType,ContentUrl,
                       RecordType.Name from ContentVersion 
                       where RecordType.Name='Warranty' and Content_title__c= 'Warranty Policies'];   
        return policyList ; 
    }
}