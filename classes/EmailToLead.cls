global class EmailToLead implements Messaging.InboundEmailHandler {

    global class EmailToLeadException extends Exception {}
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        // Create a new lead
        Lead newLead = new Lead();
        newLead.FirstName = email.fromName;
        if(newLead.firstName.length()>39)
        {
            newLead.firstName = newLead.firstName.substring(0, 40);
        }
        newLead.LastName = 'Lead';
        newLead.Company = email.fromAddress;
        newLead.Email = email.fromAddress;
        
        // Set other lead fields as needed
        RecordType[] recordTypes = [Select Id From RecordType Where	Name = 'Standard Lead'];
        if(recordTypes.isEmpty()) throw new EmailToLeadException('No recordtype named Standard Lead found');
        
		newLead.RecordTypeId = recordTypes[0].id;  
        newLead.Business__c = 'ADI';
        newLead.CTS_Product_Type__c = 'Medical';
        newLead.Brand__c = 'ADI';
		newLead.OwnerId =  [SELECT Id, Name, DeveloperName, Email, Type FROM Group where Type='Queue' And Name ='LFS ADI' limit 1].Id;        
        insert newLead;

        // Save email body to lead description
        String emailBody = email.plainTextBody;
        if (emailBody != null) {
            // Check if the email body is too long for the description field
            Integer maxLength = Lead.Description.getDescribe().getLength();
            if (emailBody.length() > maxLength) {
                emailBody = emailBody.substring(0, maxLength); // Truncate to fit
            }
            newLead.Description = emailBody;
        }
        
        update newLead;

       
         //Save any Binary Attachment
        
        if (email.binaryAttachments != null)
        {
           /* for(Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                Attachment attachment = new Attachment();
                
                attachment.Name = bAttachment.fileName;
                attachment.Body = bAttachment.body;
                attachment.ParentId = newLead.Id;
                insert attachment;
            }*/
            List<ContentVersion> contentVersions = new List<ContentVersion>();
            
            for (Messaging.InboundEmail.BinaryAttachment bAttachment : email.binaryAttachments) {
                // Create a new ContentVersion record
                ContentVersion contentVersion = new ContentVersion();
                
                // Set the necessary fields for the ContentVersion
                contentVersion.Title = bAttachment.fileName;
                contentVersion.VersionData = bAttachment.body;
                contentVersion.FirstPublishLocationId = newLead.Id;
                contentVersion.PathOnClient = '/' + bAttachment.fileName; // You can set the appropriate path
                
                // Add the ContentVersion to the list
                contentVersions.add(contentVersion);
            }
            
            // Insert all the ContentVersions
            if (!contentVersions.isEmpty()) {
                insert contentVersions;
            }
        }
        
        result.success = true;
        return result;
    }
}