global class faceToFaceBatchUpdate implements Database.Batchable<sObject>, Database.Stateful{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        //Query all NA and Global Accounts
        
        string queryStr = 'SELECT Id FROM Account WHERE ((RecordTypeName__c = \'CTS_EU\') OR '+
                          '(RecordTypeName__c = \'CTS_EU_INDIRECT\') OR (RecordTypeName__c = \'CTS_LATAM\') '+ 
                          'OR (RecordTypeName__c = \'CTS_LATAM_INDIRECT\') OR (RecordTypeName__c = \'CTS_MEIA\') ' +
                          'OR (RecordTypeName__c = \'CTS_MEIA_INDIRECT\')OR (RecordTypeName__c = \'Site_Account_NA_Air\')OR '+
                          '(RecordTypeName__c = \'CTS_Direct_Sales_and_Service\')) AND ((Type = \'Ship To\') OR (Type = \'Prospect\')) '+
                          'AND (Status__c = \'Active\' AND (Max_Event_Date_Field__c >= today)) ';
                   system.debug('Query' +queryStr);
                   return Database.getQueryLocator(queryStr); 
                   }
                   
                   public void execute(Database.BatchableContext BC, List<Account> scope){
                       Map<id, Date> MaxActDateMap = new Map<id, Date>();
                       Map<id, Date> MaxActDateMap1 = new Map<id, Date>();
                       for(Account a : scope){
                           MaxActDateMap.put(a.Id,null);
                           MaxActDateMap1.put(a.Id,null);
                           
                       }      
                       MaxActDateMap = TaskMethods.setFace2FaceDatefromEvent(MaxActDateMap , 'Face To Face Call');
                       MaxActDateMap = TaskMethods.setFace2FaceDatefromTask(MaxActDateMap ,'Face To Face Call');
                       MaxActDateMap1 = TaskMethods.setFace2FaceDatefromEvent(MaxActDateMap1 , 'Phone Call');    
                       MaxActDateMap1 = TaskMethods.setFace2FaceDatefromTask(MaxActDateMap1 ,'Phone Call');
                       TaskMethods.updateAccountFace2FaceDate(MaxActDateMap , MaxActDateMap1);
                   }
                   
                   public void finish(Database.BatchableContext BC){}
                   }