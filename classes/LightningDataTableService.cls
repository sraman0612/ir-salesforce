public with sharing class LightningDataTableService {

    /*  The schema in the Column class and all sub-classes it contains needs to mirror the structure here (see "Working with Column Properties")
        https://developer.salesforce.com/docs/component-library/bundle/lightning:datatable/documentation */

    public class Column{

        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean sortable;    
        @AuraEnabled public ColumnTypeAttributesBase typeAttributes;

        public Column(String label, String fieldName, String type, Boolean sortable, ColumnTypeAttributesBase typeAttributes){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
            this.sortable = sortable;
            this.typeAttributes = typeAttributes;
        }
    }

    /* Reference section "Formatting with Data Types" in the URL noted at the top */
    public virtual class ColumnTypeAttributesBase{

    }

    public class ColumnTypeAttributesAction extends ColumnTypeAttributesBase{

        @AuraEnabled public ColumnTypeAttributesActionRow[] rowActions;

        public ColumnTypeAttributesAction(ColumnTypeAttributesActionRow[] rowActions){
            this.rowActions = rowActions;
        }
    }   

    public class ColumnTypeAttributesActionRow {

        @AuraEnabled public String label;
        @AuraEnabled public String name;

        public ColumnTypeAttributesActionRow(String label, String name){
            this.label = label;
            this.name = name;
        }
    } 

    public class ColumnTypeAttributesLink extends ColumnTypeAttributesBase{

        @AuraEnabled public ColumnLabel label;
        @AuraEnabled public String target;

        public ColumnTypeAttributesLink(ColumnLabel label, String target){
            this.label = label;
            this.target = target;
        }
    }

     public class ColumnTypeAttributesButton extends ColumnTypeAttributesBase{

        @AuraEnabled public String label;
        @AuraEnabled public String title;
        @AuraEnabled public String variant;
        @AuraEnabled public String iconName;
        @AuraEnabled public Boolean disabled;

        public ColumnTypeAttributesButton(String label, String title, String variant, String iconName, Boolean disabled){
            this.label = label;
            this.title = title;
            this.variant = variant;
            this.iconName = iconName;
            this.disabled = disabled;
        }
    }   

    /* If specified, the data row column value displayed will be driven by the referenced record's field name specified here */
    public class ColumnLabel{     

        @AuraEnabled public String fieldName;
        
        public ColumnLabel(String fieldName){
            this.fieldName = fieldName;
        }    
    }    
}