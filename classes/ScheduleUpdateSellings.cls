public class ScheduleUpdateSellings implements Schedulable{
    
    public static String scheduleJob(string CRON_EXP) {
        string jobId = System.schedule('Update-Sellings', CRON_EXP, new ScheduleUpdateSellings());
        return jobId;
    }
	public void execute(SchedulableContext sc){
        MassUpdateSellings t = new MassUpdateSellings();
		Database.executeBatch(t);
    }
}