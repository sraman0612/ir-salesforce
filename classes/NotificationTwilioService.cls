public with sharing class NotificationTwilioService {

    @TestVisible private static Boolean forceFail = false;
    private static DateTime runTime = DateTime.now();
    private static Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
    
    public class TwilioApiClientResponseWrapper{
        public Boolean hasError = false;
        public String errorCode = '';
        public String errorMessage = '';
    }

    public class SMSFutureRequest{
        public User recipient; 
        public NotificationDeliveryService.GroupedNotificationMessage message;
    }

    @future(callout=true)
    public static void sendSMSFuture(String smsFutureRequestStr){

        SMSFutureRequest request = (SMSFutureRequest)JSON.deserialize(smsFutureRequestStr, SMSFutureRequest.class);

        String phone = getRecipientMobilePhone(request.recipient);

        TwilioApiClientResponseWrapper response = sendSMS(phone, NotificationService.convertHtmlToPlainText(request.message.messageBody));                         
        request.message.errorMessage = response.errorMessage;

        Boolean unsubscribeUserFromSMS = false;

        if (response.hasError && String.isNotBlank(response.errorCode) && getTwilioUnsubscribeErrorCodes().contains(response.errorCode)){
            unsubscribeUserFromSMS = true;
        }        

        // Since this is asynchronous, must only update SMS related fields due to possible race conditions where other fields (i.e. email results) have been updated in real-time
        Notification_Message__c[] smsMessages = new Notification_Message__c[]{};

        for (Notification_Message__c smsMessage : request.message.groupedNotification.notifications){

            Notification_Message__c smsMessageClone = new Notification_Message__c(Id = smsMessage.Id);
            smsMessageClone.SMS_Future_Delivery__c = false; // Turn this off regardless of the delivery outcome so the batch job can process it if needed
            smsMessageClone.SMS_Delivered_Date__c = response.hasError ? null : runTime;
            smsMessageClone.SMS_Delivery_Error__c = response.hasError ? NotificationDeliveryService.truncateDeliveryErrorMessage(response.errorMessage) : null;

            if (response.hasError){

                if (unsubscribeUserFromSMS && notificationSettings.Maximum_Notification_Retry_Attempts__c != null){
                    smsMessageClone.SMS_Delivery_Failure_Count__c = notificationSettings.Maximum_Notification_Retry_Attempts__c;
                }
                else{
                    smsMessageClone.SMS_Delivery_Failure_Count__c = (smsMessage.SMS_Delivery_Failure_Count__c != null ? smsMessage.SMS_Delivery_Failure_Count__c+1 : 1);
                }
            }             
            
            smsMessages.add(smsMessageClone);
        }        

        sObjectService.updateRecordsAndLogErrors(smsMessages, 'NotificationTwilioService', 'sendSMSFuture');

        if (unsubscribeUserFromSMS){
            NotificationService.unsubscribeUsersFromSMS(new Set<Id>{request.recipient.Id});
        }
    }

    public static TwilioApiClientResponseWrapper sendSMS(String toPhone, String message){

        TwilioApiClientResponseWrapper response = new TwilioApiClientResponseWrapper();

        if (notificationSettings != null && String.isNotBlank(notificationSettings.Twilio_Account_SID__c) && String.isNotBlank(notificationSettings.Twilio_From_Phone_Number__c)){

            try{

                if (Test.isRunningTest() && forceFail){
                    throw new System.NullPointerException();
                }

                // Truncate the message if it exceeds the SMS message size limit of 1600 characters
                if (String.isNotBlank(message) && message.length() > 1600){

                    system.debug('SMS message too long, truncating request.message..');
                    String truncateMessage = '...';
                    message = message.subString(0, 1600 - truncateMessage.length()) + truncateMessage;
                }

               /* TwilioSF.TwilioApiClient client = new TwilioSF.TwilioApiClient();
                client.addUrlPart('Accounts');
                client.addUrlPart(notificationSettings.Twilio_Account_SID__c);
                client.addUrlPart('Messages.json');
                
                client.addParam('To', toPhone);
                client.addParam('From', notificationSettings.Twilio_From_Phone_Number__c);
                client.addParam('Body', message);
            
                TwilioSF.TwilioApiClientResponse TWresponse = client.doPost();

                system.debug('response code: ' + (String)TWresponse.toMap().get('code'));
                system.debug('message send successfull? ' + !TWresponse.hasError());
                system.debug('response body: ' + TWresponse.getResponseBody());
                system.debug('error message: ' + TWresponse.getErrorMessage());  

                response.hasError = NotificationDeliveryService.forceFail ? true : TWresponse.hasError();     
                response.errorCode = NotificationDeliveryService.forceFail ? '555555' : (String)TWresponse.toMap().get('code');
                response.errorMessage = NotificationDeliveryService.forceFail ? 'force fail' : TWresponse.getErrorMessage();
          */  }
            catch(Exception e){
                system.debug('caught exception while sending Twilio SMS: ' + e.getMessage());
                response.hasError = true;     
                response.errorMessage = e.getMessage();
            }
        }
        else{
            response.hasError = true; 
            response.errorMessage = 'Twilio is not enabled in Notification notificationSettings.';
        }

        return response;
    }

    public static String getRecipientMobilePhone(User recipient){

        String phone = '';
                                                                                                                       
        if (String.isNotBlank(recipient.MobilePhone)){          
            phone = recipient.MobilePhone;                         
        }
        else if (recipient.Contact != null && String.isNotBlank(recipient.Contact.MobilePhone)){
            phone = recipient.Contact.MobilePhone;
        }           

        return phone;
    }

    public static Set<String> getTwilioUnsubscribeErrorCodes(){

        Set<String> unsubscribeCodes = new Set<String>();

        if (notificationSettings != null && String.isNotBlank(notificationSettings.Twilio_Error_Codes_for_Unsubscribe__c)){

            for (String errorCode : notificationSettings.Twilio_Error_Codes_for_Unsubscribe__c.split(',')){
                unsubscribeCodes.add(errorCode.trim());
            }
        }

        return unsubscribeCodes;
    }
}