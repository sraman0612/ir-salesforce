@isTest
public with sharing class sObjectServiceBatchTest {
  //Commented as part of EMEIA Cleanup
	@TestSetup
	private static void createTestData(){    
	
    TestDataUtility.createStateCountries();
    TestDataUtility testData = new TestDataUtility();
    //Commented as part of EMEIA Cleanup
	//CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
    //salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
    //insert salesRepacc;
        
		Id rTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        
		
		Account[] accounts = new Account[]{};
		
		for (Integer i = 0; i < 50; i++){
		
	        Account acct = new Account();
	        acct.Name = 'Test Account' + i;	        
	        acct.BillingCountry = 'USA';
	        acct.BillingCity = 'Augusta';
	        acct.BillingState = 'GA';
	        acct.BillingStreet = '2044 Forward Augusta Dr';
	        acct.BillingPostalCode = '566';
	        //Commented as part of EMEIA Cleanup
			//acct.CC_Sales_Rep__c = salesRepacc.Id;
	        acct.RecordTypeId = rTypeId;
	        accounts.add(acct);
		}
        
        insert accounts;		
	}
	
	private static testMethod void testMissingParamsNoOperationType(){
	 	
	 	String errorMessage = '';
	 	
	 	Test.startTest();
	 	
	 	try{
	 		sObjectServiceBatch batch = new sObjectServiceBatch(null, new Account[]{}, 'testMissingParamsNoOperationType');
	 	}
	 	catch (Exception e){
	 		errorMessage = e.getMessage();
	 	}
	 	
	 	Test.stopTest();
	 	
	 	//system.assertEquals(sObjectServiceBatch.missingParamsError, errorMessage);
	}
	
	private static testMethod void testMissingParamsNoRecords(){
	 	
	 	String errorMessage = '';
	 	
	 	Test.startTest();
	 	
	 	try{
	 		sObjectServiceBatch batch = new sObjectServiceBatch(sObjectServiceBatch.OperationType.UPDTE, null, 'testMissingParamsNoQuery');
	 	}
	 	catch (Exception e){
	 		errorMessage = e.getMessage();
	 	}
	 	
	 	Test.stopTest();
	 	
	 	system.assertEquals(sObjectServiceBatch.missingParamsError, errorMessage);
	}
	
	private static testMethod void testMissingParamsNoJobName(){
	 	
	 	String errorMessage = '';
	 	
	 	Test.startTest();
	 	
	 	try{
	 		sObjectServiceBatch batch = new sObjectServiceBatch(sObjectServiceBatch.OperationType.UPDTE, new Account[]{}, '');
	 	}
	 	catch (Exception e){
	 		errorMessage = e.getMessage();
	 	}
	 	
	 	Test.stopTest();
	 	
	 	system.assertEquals(sObjectServiceBatch.missingParamsError, errorMessage);
	}			
    
    private static testMethod void testBatchUpdate(){
    
    	Account[] accounts = [Select Id, Name From Account Order By Name ASC];
    	//system.assertEquals(50, accounts.size());
    	
    	for (Integer i = 0; i < accounts.size(); i++){
    		
    		Account a = accounts[i];
    		
    		system.assert(a.Name.startsWith('Test Account'));
    		system.assertEquals(false, a.Name.contains('B'));
    		a.Name += 'B';
    	}
    	
    	// Setup the last account to fail
    	accounts[49].Name = null;

		Test.startTest();
		{
			sObjectServiceBatch job = new sObjectServiceBatch(sObjectServiceBatch.OperationType.UPDTE, accounts, 'testBatchUpdate');
			Database.executeBatch(job);	
		}
    	Test.stopTest();  	
		
        
    	//system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c Like '%testBatchUpdate%']);
    	
    	accounts = [Select Id, Name From Account Order By Name ASC];
    	
    	Integer updatedAccounts = 0;
    	
    	for (Account a : accounts){
			
			if (a.Name.contains('B')){
				updatedAccounts++;
			}
    	}    
    	
    	//system.assertEquals(49, updatedAccounts);		
    }
    /*//Commented as part of EMEIA Cleanup
    private static testMethod void testBatchInsert(){
    
		Id rTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
		
		Account[] accounts = new Account[]{};
		

		
		for (Integer i = 0; i < 5; i++){
		
	        Account acct = new Account();
	        acct.Name = 'BTest Account' + i;	        
	        acct.BillingCountry = 'USA';
	        acct.BillingCity = 'Augusta';
	        acct.BillingState = 'GA';
	        acct.BillingStreet = '2044 Forward Augusta Dr';
	        acct.BillingPostalCode = '566';
	        //Commented as part of EMEIA Cleanup
			//acct.CC_Sales_Rep__c = [SELECT Id FROM CC_Sales_Rep__c LIMIT 1].Id;
	        acct.RecordTypeId = rTypeId;
	        accounts.add(acct);
		}
    	
    	// Setup the last account to fail
    	accounts[4].Name = null;

		Test.startTest();
		{
			sObjectServiceBatch job = new sObjectServiceBatch(sObjectServiceBatch.OperationType.INSRT, accounts, 'testBatchInsert');
			Database.executeBatch(job);	
		}
    	Test.stopTest();  	

    	system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c Like '%testBatchInsert%']);
    	
    	accounts = [Select Id, Name From Account Where Name Like 'BTest Account%' Order By Name ASC];
    	
    	system.assertEquals(4, accounts.size());		
    }        */
}