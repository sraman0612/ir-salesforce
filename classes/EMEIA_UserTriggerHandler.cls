public class EMEIA_UserTriggerHandler {

    public static void beforeUpdate(List<User> newuser){
        Set<Id> accIds=new Set<Id>();
        for (User uu : newuser){
            if(uu.AccountId!=null){
                accIds.add(uu.AccountId);
            }
        }
        
        if(accIds!= null){
            List<Account> accs=[select id, OwnerId,currencyisocode from Account where id in :accIds];
            Map<Id, Account> accById=new Map<Id, Account>();
            accById.putall(accs);
            for (User uu : newuser){
                if(uu.AccountId!=null){
                    uu.currencyisocode = accById.get(uu.AccountId).currencyisocode;
                    uu.DefaultCurrencyIsoCode = accById.get(uu.AccountId).currencyisocode;
                }
            }
        }
    }
    
    public static void afterInsert(List<User> newuser,List<User> olduser, Map<Id,User> newusermap, Map<Id,User> oldusermap){
    UserEventHandler ueh = new UserEventHandler(newuser, olduser, newusermap,oldusermap);
      ueh.pavilionGroupInsert();
      ueh.updateUserFromEmployee();
      ueh.assignPermissionSets();
      UserEventHandler.updateUsrIdonCont(newusermap, oldusermap);  
        System.debug('inside after insert trigger');
      UserEventHandler.updatePartnerFlagonContact(newuser);
        
    }
    
    public static void afterUpdate(List<User> newuser,List<User> olduser, Map<Id,User> newusermap, Map<Id,User> oldusermap){
        UserEventHandler ueh = new UserEventHandler(newuser, olduser, newusermap,oldusermap);
        ueh.updateUserFromEmployee();
        ueh.assignPermissionSets();
        ueh.addCORPId2Case();
        if(!System.isFuture() && !System.isBatch()){
            ueh.addAccTeamMember(newusermap, oldusermap);
           
        }
       
        UserEventHandler.updateUsrIdonCont(newusermap, oldusermap);    
        UserEventHandler.updatePartnerFlagonContact(newuser);
    }

}