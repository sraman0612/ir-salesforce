/**
@author Ben Lorenz
@date 1AUG2019
@description Handler Class to send email upon Losant Event Case creation.
*/

public class CTS_LosantEventCaseEmailHandler {
    
    @InvocableMethod
    public static void sendEmails(List<Case> cList) {
        
        List<String> sNums = new List<String>();
        for(Case c: cList) {
            sNums.add(c.AssetId);
        }        
        Map<String,Asset> assetId2AssetMap = new Map<String,Asset>();
        for(Asset a: [SELECT Id, SerialNumber, Division__c FROM Asset WHERE Id IN: sNums]) {
            assetId2AssetMap.put(a.Id, a);
        }
        
        Map<String,Asset> caseId2AssetMap = new Map<String,Asset>();
        for(Case c: cList) {
            if(assetId2AssetMap.containsKey(c.AssetId)) {
                caseId2AssetMap.put(c.Id, assetId2AssetMap.get(c.AssetId));
            } 
        }
        
        Map<String,List<String>> caseId2EmailsMap = getEmailsOrUsers(cList, caseId2AssetMap);
        
        LosantSettings__c hcs = LosantSettings__c.getOrgDefaults();
        String orgWideEmailId = hcs.OrgWideEmailId__c;
        String emailTemplateName = hcs.EmailTemplateName__c;
        
        List<Messaging.SendEmailResult> listEmailResult = null;
        List<Messaging.SingleEmailmessage> listSingleEmailMessages = new List<Messaging.SingleEmailmessage>();
        
        EmailTemplate emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = :emailTemplateName];    
        
        for (Case c : cList) {            
            List<String> emailAddresses = caseId2EmailsMap.containsKey(c.Id) ? caseId2EmailsMap.get(c.Id) : null;
            if(Test.isRunningTest()) emailAddresses = new List<String>{'test@test.com'};
            if(emailAddresses == null) continue;
            String threadId = getThreadId(c.Id);            
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(orgWideEmailId);
            mail.setToAddresses(emailAddresses);            
            String subject = emailTemplate.Subject;
            subject = subject.replace('{!Case.CaseNumber}', c.CaseNumber);
            subject = subject.replace('{!Case.Thread_Id}', threadId);
            mail.setSubject(subject);
            
            String plainBody = emailTemplate.Body;
            plainBody = plainBody.replace('{!Case.CaseNumber}', c.CaseNumber);
            plainBody = plainBody.replace('{!Case.Subject}', c.Subject);
            plainBody = plainBody.replace('{!Case.Thread_Id}', threadId);            
            mail.setPlainTextBody(plainBody);
            
            listSingleEmailMessages.add(mail);
            
        }
        if(!Test.isRunningTest()) {
            try {
                listEmailResult = Messaging.sendEmail(listSingleEmailMessages);
            } catch(Exception e) {
                System.debug('e**' + e.getMessage());
                Apex_Log__c log = new Apex_Log__c();
                log.class_name__c = 'CTS_LosantEventCaseEmailHandler';
                log.method_name__c = 'sendEmails';
                log.Message__c = JSON.serialize('Failed: ' + e.getMessage());
                insert log;
            }
        }
    }        
    
    //Returns a map of case id to email addresses based on asset id    
    public static Map<String,List<String>> getEmailsOrUsers(List<Case> cList, Map<String,Asset> caseId2AssetMap) {        
        Map<String,List<String>> caseId2EmailsMap = new Map<String,List<String>>();
        for(Case c: cList) {
            if(c.AssetId != null) {
                if(caseId2AssetMap.containsKey(c.Id)) {
                    String regionOrDivision = caseId2AssetMap.get(c.Id).Division__c;
                    
                    for(CTS_RMS_Division_Email_Mapping__mdt mp: [SELECT Division__c, Emails__c FROM CTS_RMS_Division_Email_Mapping__mdt]) {
                        if(String.isNotBlank(mp.Division__c) && String.isNotBlank(regionOrDivision) && mp.Division__c.contains(regionOrDivision)) {
                            caseId2EmailsMap.put(c.Id, String.isNotBlank(mp.Emails__c) ? mp.Emails__c.split(',') : null);
                        }
                    }                    
                }
            }
        }
        return caseId2EmailsMap;
    }
    
    public static String getThreadId(String caseId) {
        return '[ ref:_'
            + UserInfo.getOrganizationId().left(5)
            + UserInfo.getOrganizationId().mid(10,5) + '._'
            + caseId.left(5)
            + caseId.mid(10,5) + ':ref ]';
    }     
}