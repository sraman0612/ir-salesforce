@isTest
public with sharing class NotificationTwilioServiceTest {

    @testSetup
    private static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Notification_Settings__c notificationSettings = NotificationsTestDataService.createNotificationSettings();
        notificationSettings.Twilio_Error_Codes_for_Unsubscribe__c = 'abc, 123';
        update notificationSettings;

        Account shipTo1 = CTS_TestUtility.createAccount('Test Ship To1', true);

        Contact standCommContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts1@gmail.com', shipTo1.Id, false);
        standCommContact.CTS_IOT_Community_Status__c = 'Approved';
        standCommContact.MobilePhone = '555-555-4444';  
        insert standCommContact;      

        User internalUser = CTS_TestUtility.createUser(false);    
        internalUser.isActive = true;
        internalUser.LastName = 'Internal_User_Test1';        
        internalUser.MobilePhone = '555-555-5555';

        User standCommUser = CTS_TestUtility.createUser(standCommContact.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id, false);    
        standCommUser.LastName = 'Comm_Stand_User_Test1';        

        insert new User[]{internalUser, standCommUser}; 

        Notification_Message__c message1 = NotificationsTestDataService.createNotificationMessage(true, 
            standCommUser.Id, null, null, null, DateTime.now(), 'Real-Time', 'SMS');        
    }
}
    @isTest
    private static void sendSMSTest1(){

        Test.startTest();

        // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); SGOTHAND 3/30/2022 SGOM-44

        NotificationTwilioService.TwilioApiClientResponseWrapper response = NotificationTwilioService.sendSMS('555-555-5555', 'test');
        system.assertEquals(false, response.hasError);
        system.assertEquals('', response.errorCode);
        system.assertEquals('', response.errorMessage);

        Test.stopTest();
    }

    @isTest
    private static void sendSMSTest2(){

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());--SGOTHAND 3/30/2022 SGOM-44

        String tooLongMessage = 'test';

        while (tooLongMessage.length() <= 1600){
            tooLongMessage += 'test';
        }

        NotificationTwilioService.TwilioApiClientResponseWrapper response = NotificationTwilioService.sendSMS('555-555-5555', tooLongMessage);
        system.assertEquals(false, response.hasError);
        system.assertEquals('', response.errorCode);
        system.assertEquals('', response.errorMessage);

        Test.stopTest();
    }    

    @isTest
    private static void sendSMSTest3(){

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        NotificationTwilioService.forceFail = true;

        NotificationTwilioService.TwilioApiClientResponseWrapper response = NotificationTwilioService.sendSMS('555-555-5555', 'test');
        system.assertEquals(true, response.hasError);
        system.assertEquals('', response.errorCode);
        system.assert(String.isNotBlank(response.errorMessage));

        Test.stopTest();
    }    
    
    @isTest
    private static void sendSMSTest4(){

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); SGOTHAND 3/30/2022 SGOM-44

        delete Notification_Settings__c.getOrgDefaults();

        NotificationTwilioService.TwilioApiClientResponseWrapper response = NotificationTwilioService.sendSMS('555-555-5555', 'test');
        system.assertEquals(true, response.hasError);
        system.assertEquals('', response.errorCode);
        system.assertEquals('Twilio is not enabled in Notification notificationSettings.', response.errorMessage);

        Test.stopTest();
    }      

    @isTest
    private static void sendSMSFutureTest1(){

        User u = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        Notification_Message__c[] messages = [Select Id From Notification_Message__c];

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());--SGOTHAND 3/30/2022 SGOM-44

        NotificationDeliveryService.GroupedNotificationMessage message = new NotificationDeliveryService.GroupedNotificationMessage();
        message.subject = 'test subject';
        message.messageBody = 'test message';
        message.groupedNotification = new NotificationDeliveryService.GroupedNotification();
       // message.groupedNotification.deliveryFrequency = NotificationService.REAL_TIME;
        message.groupedNotification.recipient = u;
        message.groupedNotification.parent = [Select Id, Name From Account LIMIT 1];
        message.groupedNotification.notifications = messages;

        NotificationTwilioService.SMSFutureRequest request = new NotificationTwilioService.SMSFutureRequest();
        request.recipient = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        request.message = message;

        NotificationTwilioService.sendSMSFuture(JSON.serialize(request));

        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messages);
    }
    
    @isTest
    private static void sendSMSFutureTest2(){

        User u = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        Notification_Message__c[] messages = [Select Id From Notification_Message__c];

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());--SGOTHAND 3/30/2022 SGOM-44
        NotificationTwilioServiceMock.forceError = true;

        NotificationDeliveryService.GroupedNotificationMessage message = new NotificationDeliveryService.GroupedNotificationMessage();
        message.subject = 'test subject';
        message.messageBody = 'test message';
        message.groupedNotification = new NotificationDeliveryService.GroupedNotification();
        message.groupedNotification.deliveryFrequency = NotificationService.REAL_TIME;
        message.groupedNotification.recipient = u;
        message.groupedNotification.parent = [Select Id, Name From Account LIMIT 1];
        message.groupedNotification.notifications = messages;

        NotificationTwilioService.SMSFutureRequest request = new NotificationTwilioService.SMSFutureRequest();
        request.recipient = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        request.message = message;

        NotificationTwilioService.sendSMSFuture(JSON.serialize(request));

        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messages, true);
    }       

    @isTest
    private static void getTwilioUnsubscribeErrorCodesTest(){

        Test.startTest();

        List<String> unsubscribeCodes = new List<String>(NotificationTwilioService.getTwilioUnsubscribeErrorCodes());

        system.assertEquals(2, unsubscribeCodes.size());
        system.assertEquals('abc', unsubscribeCodes[0]);
        system.assertEquals('123', unsubscribeCodes[1]);

        Test.stopTest();
    }

    @isTest
    private static void getRecipientMobilePhoneInternalUserTest(){

        User u = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Internal_User_Test1' LIMIT 1];

        Test.startTest();

        String phone = NotificationTwilioService.getRecipientMobilePhone(u);

        system.assertEquals(u.MobilePhone, phone);

        Test.stopTest();
    }    

    @isTest
    private static void getRecipientMobilePhoneCommUserTest(){

        User u = [Select Id, MobilePhone, Contact.MobilePhone From User Where LastName = 'Comm_Stand_User_Test1' LIMIT 1];

        Test.startTest();

        String phone = NotificationTwilioService.getRecipientMobilePhone(u);

        system.assertEquals(u.Contact.MobilePhone, phone);

        Test.stopTest();
    }      
}