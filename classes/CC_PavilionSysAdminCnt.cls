public class CC_PavilionSysAdminCnt {

    public List<User> userList{get;set;}
    public List<Pavilion_Navigation_Profile__c> pavNavList {get; set;}
    Public Id accId{ get; set; }
    public List<UserListAndPavilionDisplayWrapper> wrapList{get;set;} 
    public set<String> pavStringSet{get; set;}
    
    public CC_PavilionSysAdminCnt(CC_PavilionTemplateController controller) {
              accid = controller.acctid;
              
              wrapList=new List<UserListAndPavilionDisplayWrapper>();
              pavStringSet=new Set<String>();
              pavNavList =new List<Pavilion_Navigation_Profile__c>();
              
              if (userList == null) {
                Set<Id> ccCommProfSet = new Set<Id>();
                ccCommProfSet.add(PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c);
                ccCommProfSet.add(PavilionSettings__c.getInstance('PavillionCommunityProfileId').Value__c);
                
                userList = [SELECT id, CC_CustomerVIEW__c, CC_Pavilion_Navigation_Profile__c, FederationIdentifier, 
                            FirstName, LastName, Title, UserName, Name, Email, IsActive
                            FROM User where(ProfileId in :ccCommProfSet AND AccountId = : accId)];
              }
              
                for(User u : userList){
                pavStringSet.add(u.CC_Pavilion_Navigation_Profile__c);
                }
                if(pavStringSet.size()>0){
                pavNavList=[select id,Name,Display_Name__c from Pavilion_Navigation_Profile__c where Name IN :pavStringSet];
                }
                for(User us:userList){
                Boolean flag=false;
                   for(Pavilion_Navigation_Profile__c pv : pavNavList){
                        if(pv.Name==us.CC_Pavilion_Navigation_Profile__c){
                            flag=true;
                            String disp=pv.Display_Name__c;
                            UserListAndPavilionDisplayWrapper uw1=new UserListAndPavilionDisplayWrapper(us,disp);
                            wrapList.add(uw1);
                            }
                    }
                     if(flag=false){
                        UserListAndPavilionDisplayWrapper uw=new UserListAndPavilionDisplayWrapper(us,null);
                        wrapList.add(uw);
                    }
                    
                }
    }
    
    public class UserListAndPavilionDisplayWrapper {

    public User UserListW{get;set;}
    public String pavilionDisplayName{get;set;}
    public UserListAndPavilionDisplayWrapper(User us, String st) {

        this.UserListW= us;
        this.pavilionDisplayName= st;
    }
   } 
}