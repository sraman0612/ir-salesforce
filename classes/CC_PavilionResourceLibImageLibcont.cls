global with sharing class CC_PavilionResourceLibImageLibcont {
    Public String zipLogoURL{get;set;}

    /* constructor */  
    public CC_PavilionResourceLibImageLibcont(CC_PavilionTemplateController controller) {
        try{
            zipLogoURL='servlet/servlet.FileDownload?file='+[SELECT Id FROM Document WHERE Name = 'ZipFileLogo'].id;
        }
        catch(Exception e){
            system.debug(e);
        }

    }
    
    /* remote action to display ContentVersion records*/
    @RemoteAction
    public static Set<ContentVersion> getMarketingImages() {  
        Id  usrId  = userinfo.getUserId();
        List<Contract> cont = new List<Contract>();
        List<ContentVersion> contentResource = new List<ContentVersion>();
        Set<ContentVersion> finalcontentResource = new Set<ContentVersion>();
        List<string> agrContent= new List<string>();
        map<string,string> accountmap  = new map<string,string>();
        Set<Id> acctid = new Set<Id>();
         Id accId = [SELECT AccountId from User  where id =:usrId].AccountId;  
         Id internalaccid = PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c;
         acctid.add(accId);
         acctid.add(internalaccid);
         if(acctId!=null)
         {
         Account acc = [select CC_Global_Region__c,Currency__c from Account where id=:acctid];
         
        cont = [SELECT id,name,CC_Sub_Type__c,CC_Type__c from Contract where AccountId=:acctid and CC_Type__c='Dealer/Distributor Agreement'];
        //Set<string> contractType=  new set<string>();
        
         for(Contract c: cont){
            if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '')
              {
                accountmap.put(c.CC_Sub_Type__c,''); 
                
              }
          } 
             
         
        contentResource = [select id, Content_Title__c,FileExtension,FileType,
                           Marketing_Image_Category__c,Marketing_Image_Type__c,
                           Marketing_Image_Sub_Type__c, Content_Sub_title__c, 
                           Content_Body__c, Vehicle__c, CS_Category__c,CC_Region__c,Currency__c,Agreement_Type__c, 
                           RecordType.Name from ContentVersion
                           where RecordType.Name='Marketing Images' and IsLatest = true];
                           }
         for(ContentVersion c: contentResource){//--------------
           if(c.Agreement_Type__c != null){
           agrContent = c.Agreement_Type__c.split(';');
               for(string s:agrContent ){
                   if(accountmap.containskey(s)){
                       finalcontentResource.add(c); 
                   }
                   
               }
           }
          }
        return finalcontentResource ;
        
    }
   
     /* remote action to get marketing subtype */
    public set<string> getImageDataFilter()
    {
    
        Id  usrId  = userinfo.getUserId();
        List<Contract> cont = new List<Contract>();
        Set<Id> acctid = new Set<Id>();
         Id accId = [SELECT AccountId from User  where id =:usrId].AccountId;  
         Id internalaccid = PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c;
         acctid.add(accId);
         acctid.add(internalaccid);
        cont = [SELECT id,name,CC_Sub_Type__c,CC_Type__c from Contract where AccountId=:acctId and CC_Type__c='Dealer/Distributor Agreement'];
        Set<string> contractType=  new set<string>();
        
         for(Contract c: cont){
            if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '')
              {
                contractType.add(c.CC_Sub_Type__c);
              }
          }
        return contractType;
    }
    /* remote action to filtered ContentVersion Records*/
    @Remoteaction
    Public static Set<ContentVersion> getfiletredMarketingImages(string lastentryid){
        String lastid ='';
         Id  usrId  = userinfo.getUserId();
        List<ContentVersion> ListcontentResource = new List<ContentVersion>();
        List<ContentVersion> contentResourceForAll = new List<ContentVersion>();
        set<ContentVersion> finalcontentResource = new set<ContentVersion>();
        List<Contract> cont= new List<Contract>(); 
        List<string> agrContent= new List<string>();
        Set<string> contractType=  new set<string>();
        map<string,string> accountmap  = new map<string,string>();
        Set<Id> acctid = new Set<Id>();
         Id accId = [SELECT AccountId from User  where id =:usrId].AccountId;  
         Id internalaccid = PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c;
         acctid.add(accId);
         acctid.add(internalaccid); 
        Account acc =[SELECT Id,CC_Agreement_Type__c, CC_Global_Region__c,Currency__c  from Account where Id=:acctId];
        cont = [SELECT id,name,CC_Sub_Type__c from Contract where AccountId=:acctId  and CC_Type__c='Dealer/Distributor Agreement'];
         for(Contract c: cont){
            if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '')
              {
                accountmap.put(c.CC_Sub_Type__c,''); 
                contractType.add(c.CC_Sub_Type__c);
              }
          }
      
        
        string contentresource ='select id, Content_Title__c,FileExtension,FileType,Marketing_Image_Category__c,Marketing_Image_Type__c,Marketing_Image_Sub_Type__c,Agreement_Type__c, Content_Sub_title__c, Content_Body__c, Vehicle__c, CS_Category__c,Currency__c,CC_Region__c, RecordType.Name from ContentVersion'
                                +' where RecordType.Name =\'Marketing Images\'  AND IsLatest = true';
                        if(lastentryid != 'All'){
                            contentresource += ' and Agreement_Type__c INCLUDES( \''+lastentryid+'\') ';
                            }
                           
                           ListcontentResource =  database.query(contentresource); 
                           for(ContentVersion c: ListcontentResource){//--------------
                               if(c.Agreement_Type__c != null){
                               agrContent = c.Agreement_Type__c.split(';');
                                   for(string s:agrContent ){
                                       if(accountmap.containskey(s)){
                                           finalcontentResource.add(c); 
                                       }
                                       
                                   }
                               } 
                            
                        }   
        return finalcontentResource;
    }
/** added by tapas to provide list of strings for filtering  on 13/7/2016 **/
    @RemoteAction
    public Static List<String> getMarkettingCategories(){
        set<String> tempContentList=new Set<String>();
        for(ContentVersion cv:getMarketingImages()){
            tempContentList.add(cv.Marketing_Image_Category__c);
            
        }
        List<String> listsOfCategories=new List<String>(tempContentList);
        return listsOfCategories;
    }
     @RemoteAction
    public Static List<String> getMarkettingTypes(){
        set<String> tempContentList=new Set<String>();
        for(ContentVersion cv:getMarketingImages()){
            tempContentList.add(cv.Marketing_Image_Type__c);
            
        }
        List<String> listsOfCategories=new List<String>(tempContentList);
        return listsOfCategories;
    }
     @RemoteAction
    public Static List<String> getMarkettingSubTypes(){
        set<String> tempContentList=new Set<String>();
        for(ContentVersion cv:getMarketingImages()){
            tempContentList.add(cv.Marketing_Image_Sub_Type__c);
            
        }
        List<String> listsOfCategories=new List<String>(tempContentList);
        return listsOfCategories;
    }
    /** till here **/
    
}