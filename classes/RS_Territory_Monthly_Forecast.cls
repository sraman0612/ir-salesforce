public with sharing class RS_Territory_Monthly_Forecast {
    
    public Account currentAccount {get; set;}
    String month;
    String year;

    public PageReference GetForecast() {

        getOpportunityStageNames();
        getPipelineAmounts();
        getRevenueSchedules();
        
        return null;
    }

    public List<SelectOption> getMonthOptions(){
        List<SelectOption> options = new List<SelectOption>();
        String monthOption;
        
        for (Integer i = 1; i<= 12; i++)
        {
            monthOption = string.valueof(i);
            options.add(new SelectOption(monthOption,getMonthName(i)));
        }
        
        return options;
    }
  
    public String getMonth(){
        return month;
    }
    
    public void setMonth(String month){
        this.month = month;
    }
  
    
    public List<SelectOption> getYearOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Date now = Date.today();
        Integer currentYear = now.Year();
        String yearOption;

        for (Integer i = -2; i <= 2; i++)
        {
            yearOption = string.valueof(currentYear + i);
            options.add(new SelectOption(yearOption, yearOption));
        }
        
        return options;
    }
    
    public String getYear(){
        return year;
    }
    
    public void setYear(String year){
        this.year = year;
    }
    
    public static final Map<Integer, String> monthNamesMap = new Map<Integer, String>{
                                                                    1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
                                                                    5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
                                                                    9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'};

    public static final string RECORD_TYPE_NEW_BUSINESS = 'RS_Business';
    //Ashwini added Strategic Type Change 18-DEC-17
    public static final string RECORD_TYPE_STRATEGIC_DSO = 'Strategic_Opportunity_DSO';
    public static string RECORD_TYPE_ID_NEW_BUSINESS {get; set;}
    public static string RECORD_TYPE_ID_STRATEGIC_DSO {get; set;}
    
    // Static initializer
    static{
      RECORD_TYPE_ID_NEW_BUSINESS = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_NEW_BUSINESS AND SObjectType = 'Opportunity' LIMIT 1].Id;
      RECORD_TYPE_ID_STRATEGIC_DSO = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_STRATEGIC_DSO AND SObjectType = 'Opportunity' LIMIT 1].Id;
      //RECORD_TYPE_ID_NEW_BUSINESS = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND (DeveloperName = :RECORD_TYPE_NEW_BUSINESS OR DeveloperName = :RECORD_TYPE_STRATEGIC_DSO) LIMIT 1].Id;
    }

    public RS_Territory_Monthly_Forecast() {
        Date now = Date.today();
        this.month = now.Month().format();
        this.year = string.valueof(now.Year());
        
        getOpportunityStageNames();
        getPipelineAmounts();
        getRevenueSchedules();
    }

    public String getCurrentMonthName() {
        Date now = Date.today();
        Integer currentMonth = now.month();
        // S.Colman: I'm changing this part of the code slightly to cover all months with just the line below
        String queryMonth = monthNamesMap.get(currentMonth);
        return queryMonth;
    }

    public String getMonthName(String month) {

        String queryMonth = monthNamesMap.get(Integer.valueOf(month));
        return queryMonth;
    }
    
    public String getMonthName(Integer month) {

        String queryMonth = monthNamesMap.get(month);
        return queryMonth;
    }
    /*
    String revenueScheduleQueryS = 'SELECT Opportunity__r.Account.Name, Opportunity__r.Account.OwnerId, ' +
                                            'Opportunity__r.AccountId, Opportunity__r.Amount, Opportunity__r.Name, Opportunity__r.Sales_Budget__c, Opportunity__r.StageName, ' +
                                            'Budget__c, Forecast_Adjustment__c, Id, Month__c, Prior_Year_Sales__c ' +
                                            'FROM Opportunity_Revenue_Schedule__c ';
    String revenueScheduleQueryW = 'WHERE Opportunity__r.Account.OwnerId = \'' + UserInfo.getUserId() + '\' ' + 'AND Month__c = \'' + getMonthName(this.month) + '\' ' + 'AND Year__c = \'' + this.year + '\'';
    //String revenueScheduleQueryW = 'WHERE Opportunity__r.Account.OwnerId = \'' + UserInfo.getUserId() + '\' ' + 'AND Forecast_Month__c = THIS_MONTH '; //ND Month__c = \'' + getCurrentMonthName() + '\'';
    String revenueScheduleQueryO = 'ORDER BY Opportunity__r.Account.Name ASC, Month__c ASC';
*/
    String pipelineQueryS = 'SELECT AccountId, SUM(Amount)summedAmount FROM Opportunity ';
   /* String pipelineQueryW = 'WHERE StageName NOT IN(\'Closed Won\',\'Closed Lost\') AND RecordType.Id = \'' + RECORD_TYPE_ID_NEW_BUSINESS + '\'' +
                            'AND Account.OwnerId = \'' + UserInfo.getUserId() + '\' ';*/
    String pipelineQueryW = 'WHERE StageName NOT IN(\'Closed Won\',\'Closed Lost\') AND (RecordType.Id = \'' + RECORD_TYPE_ID_NEW_BUSINESS + '\' OR RecordType.Id = \'' + RECORD_TYPE_ID_STRATEGIC_DSO + '\')' +
                            'AND Account.OwnerId = \'' + UserInfo.getUserId() + '\' ';
    String pipelineQueryO = 'GROUP BY AccountId';

    public List<Opportunity_Revenue_Schedule__c> getRevenueSchedules() {
        String revenueScheduleQueryS = 'SELECT Opportunity__r.Account.Name, Opportunity__r.Account.OwnerId, ' +
                                            'Opportunity__r.AccountId, Opportunity__r.Amount, Opportunity__r.Name, Opportunity__r.Sales_Budget__c, Opportunity__r.StageName, ' +
                                            'Budget__c, Forecast_Adjustment__c, Id, Month__c, Prior_Year_Sales__c ' +
                                            'FROM Opportunity_Revenue_Schedule__c ';
        String revenueScheduleQueryW = 'WHERE Opportunity__r.Account.OwnerId = \'' + UserInfo.getUserId() + '\' ' + 'AND Month__c = \'' + getMonthName(this.month) + '\' ' + 'AND Year__c = ' + Integer.valueof(this.year) + ' ';
        //String revenueScheduleQueryW = 'WHERE Opportunity__r.Account.OwnerId = \'' + UserInfo.getUserId() + '\' ' + 'AND Forecast_Month__c = THIS_MONTH '; //ND Month__c = \'' + getCurrentMonthName() + '\'';
        String revenueScheduleQueryO = 'ORDER BY Opportunity__r.Account.Name ASC, Month__c ASC';
        
        system.debug('-------'+revenueScheduleQueryS + revenueScheduleQueryW + revenueScheduleQueryO);
        List<Opportunity_Revenue_Schedule__c> revScheds = Database.query(revenueScheduleQueryS + revenueScheduleQueryW + revenueScheduleQueryO);
        return revScheds;
    }

    public List<sObject> getPipelineAmounts() {
        List<sObject> pipelineAmounts = Database.Query(pipelineQueryS + pipelineQueryW + pipelineQueryO);
        return pipelineAmounts;
    }

    public List<String> getOpportunityStageNames() {
      List<String> stageNames = new List<String>();

       Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
       List<Schema.PicklistEntry> stagePickList = fieldResult.getPicklistValues();

       for ( Schema.PicklistEntry s : stagePickList)
       {
          String entry = '[\"' + s.getLabel() + '\",\"' + s.getValue() + '\"]';
          stageNames.add(entry);
       }
       return stageNames;
    }

}