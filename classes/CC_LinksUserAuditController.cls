Global class CC_LinksUserAuditController {
  public static List<UserLogin> userListFreez {get; set;}
  public static Map<Id, UserLogin> allusersMapf {get; set;}
  public static UserLogin SelectedSingleUserl{get;set;}


  public CC_LinksUserAuditController(CC_PavilionTemplateController controller) {}
  
  @RemoteAction
  public static  CC_Partner_User_Compliance__c [] getUserComplianceAuditRecords(String aId){
    CC_Partner_User_Compliance__c[] lst = new CC_Partner_User_Compliance__c[]{};
    lst = [SELECT Id, Status__c, User__r.Name, User__c, Account__c, CC_Business_Number__c, CC_Department__c, CC_Email__c,
                  CC_First_Name__c, CC_Job_Title__c, CC_Last_Name__c, CC_Mobile_Number__c, CC_Part_Cart__c, CC_Parts_Bulletins__c,
                  CC_Parts_Order_Invoice_Pricing__c, CC_Parts_Pricing_Bulletins__c, CC_Profile__c, CC_Role__c, CC_Sales_Bulletins__c,
                  CC_Sales_Pricing_Bulletins__c, CC_Service_Bulletins__c, CC_SFA_CustomerVIEW__c, CC_Warranty_Tavant__c, Name
             FROM CC_Partner_User_Compliance__c 
            WHERE Account__c = :aId and Status__c = 'In Process'];           
    return lst;
  }

  @RemoteAction
  public static String setStatus(String uid,String pucid,String status){
    CC_Partner_User_Compliance__c puc = new CC_Partner_User_Compliance__c (id=pucid,Status__c=status);
    upsert puc;
    if(status=='Deactivated'){
     
   //Code Contributed by @Priyanka Baviskar for Request 8114798: Freeze the user after clicking on Deactivate user button at 12/24/2018.

    userListFreez = [SELECT IsFrozen,UserId 
                         FROM UserLogin 
                        WHERE UserId =:uid];
     allusersMapf = new Map<Id,UserLogin>();
     for(UserLogin ul : userListFreez){
        allusersMapf.put(ul.UserId,ul);
        }
     SelectedSingleUserl = allusersMapf.get(uid);
     SelectedSingleUserl.IsFrozen= true;
     update SelectedSingleUserl;
  //End of code for Request 8114798: Freeze the user after clicking on Deactivate user button.
  
      Case cs = new Case();
      cs.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName= 'Club_Car_Case' AND SobjectType ='Case' LIMIT 1].Id;
      cs.Subject = 'User Deactivated';
      cs.Description = 'Please Deactivate the Associated User';
      cs.Reason='User Admin';
      cs.Origin = 'Community';
      cs.ContactId = [SELECT ContactId from User where id = :UserInfo.getUserId()].ContactId;
      cs.CC_User__c = uid;
      cs.CC_CorpId__c = [SELECT Id, FederationIdentifier FROM User WHERE Id = :uid].FederationIdentifier;
      cs.Sub_Reason__c='Deactivate User';
      cs.ownerId = PavilionSettings__c.getInstance('UserDeactivationQ').Value__c;
      insert cs;
    }
    return 'OK';
  }
}