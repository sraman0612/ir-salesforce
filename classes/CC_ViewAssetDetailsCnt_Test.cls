/**********************************************************************************
* Name		: CC_ViewAssetDetailsCnt_Test
* Req#		: Add CC CE Certification to CC Links
* Purpose	: Test Class for CC_ViewAssetDetailsCnt
* History	:
* ===========
* VERSION	AUTHOR				     DATE		   DETAIL
* 	1.0		TCS(Priyanka Baviskar)	04/15/2019	Initial Development
************************************************************************************/
@isTest
public class CC_ViewAssetDetailsCnt_Test {
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    static testMethod void UnitTest_CoopClaimDetailPageController()
    { 
        
        Account newAccount = TestUtilityClass.createAccount();
        insert newAccount;
        Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
        insert sampleContact;
        
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
        insert newPavilionNavigationProfile;
        
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
        insert newUser;
        CC_Order__c sampleOrder = TestUtilityClass.createCustomOrder(newAccount.id,'OPEN','N');
        Insert sampleOrder;
        
        CC_Asset__c  sampleAssetDeatils = TestUtilityClass.createAssetDetails(sampleOrder.Id);
        sampleAssetDeatils.Order__c = sampleOrder.Id;
        
        sampleAssetDeatils.Serial__c='Test1236954';
        //sampleAssetDeatils.Account__r.name= 'Defiance Equipment';
        
        Account a=TestUtilityClass.createAccount();
        insert a;
        System.assertNotEquals(null,a);
        Contact c=TestUtilityClass.createContact(a.id);
        insert c;
        System.assertNotEquals(null,c);
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(c.Id,u.ContactId);
        test.startTest();
        System.runAs(newUser){
            
            CC_PavilionTemplateController controller=new CC_PavilionTemplateController(); 
            CC_ViewAssetDetailsCnt reqObj =new CC_ViewAssetDetailsCnt(controller);
            CC_ViewAssetDetailsCnt.getAssset(sampleOrder.Id);
            
        }   
        test.stopTest();
    }
    
}