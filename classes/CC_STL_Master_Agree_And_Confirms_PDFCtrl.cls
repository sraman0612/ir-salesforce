public with sharing class CC_STL_Master_Agree_And_Confirms_PDFCtrl {

    public class LeaseWrapper{
        public CC_Short_Term_Lease__c lease {get;set;}
        public CC_STL_Car_Info__c[] carsOnLease {get;set;}
    }

    public LeaseWrapper[] leases {get;set;}

    public CC_STL_Master_Agree_And_Confirms_PDFCtrl(ApexPages.StandardController controller){
        
        leases = new LeaseWrapper[]{};

        String leaseIdsStr = ApexPages.currentPage().getParameters().get('leaseIds');
        system.debug('leaseIdsStr: ' + leaseIdsStr);

        if (String.isNotBlank(leaseIdsStr)){

            Id[] leaseIds = leaseIdsStr.split(',');
            
            Map<Id, CC_STL_Car_Info__c[]> leaseCarsMap = new Map<Id, CC_STL_Car_Info__c[]>();

            for (CC_STL_Car_Info__c car : [Select Id, Short_Term_Lease__c, Quantity__c, Description__c, Base_Cost__c, Total_Cost__c, Per_Car_Cost__c, Internal_Cost__c, Part_Number__c
                        From CC_STL_Car_Info__c 
                        Where Short_Term_Lease__r.Id in :leaseIds and Quantity__c > 0]){
                
                if (leaseCarsMap.containsKey(car.Short_Term_Lease__c)){
                    leaseCarsMap.get(car.Short_Term_Lease__c).add(car);
                }
                else{
                    leaseCarsMap.put(car.Short_Term_Lease__c, new CC_STL_Car_Info__c[]{car});
                }
            }

            for (CC_Short_Term_Lease__c lease : [Select Id, Name, Master_Lease__r.Name,Master_Lease__r.Customer__r.Name, 
                            Master_Lease__r.Customer__r.Address_1__c,Master_Lease__r.Customer__r.Address_2__c,Master_Lease__r.Customer__r.Address_3__c,
                            Master_Lease__r.Customer__r.CC_City__c,Master_Lease__r.Customer__r.CC_MAPICS_City__c,
                            CreatedDate, Billing_Contact__r.Name, Billing_Contact__r.Phone,Billing_Contact__r.Email,
                            Master_Lease__r.Bill_To_Name__c, Master_Lease__r.Bill_To_Address__c, Master_Lease__r.Bill_To_City__c, Master_Lease__r.Bill_To_State__c, Master_Lease__r.Bill_To_Zip__c, Master_Lease__r.Bill_To_Country__c,
                            Shipping_Address__c, Shipping_City__c, Shipping_State__c, Shipping_Zip__c, Shipping_Country__c, Beginning_Date__c, Shipping_Contact__r.Name,Shipping_Contact__r.Phone,Shipping_Contact__r.Email,
                            Ending_Date__c, Total_Car_Cost__c, Invoice_Terms__c, Remaining_Payments__c, 
                            Service_Contract_Fee__c, Options_on_Cars__c, Liability_End_Date__c,
                            Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Total_Car_Internal_Cost__c, Total_Number_of_Cars__c,
                            Number_of_Days__c, Total_Quote_Price__c, Adj_Cost_Before_Transportation__c,
                            Confirmation_Subtotal__c, Total_Lease_Price__c, Estimated_Tax__c, Standard_Monthly_Billing__c, 
                            Num_of_Payments__c, First_Bill__c, Shipping_To_Name__c, Additional_Discount__c , One_time_Charge__c
                        From CC_Short_Term_Lease__c 
                        Where Id in :leaseIds]){

                CC_STL_Car_Info__c[] carsOnLease = leaseCarsMap.get(lease.Id);
            
                LeaseWrapper wrapper = new LeaseWrapper();
                wrapper.lease = lease;
                wrapper.carsOnLease = carsOnLease != null ? carsOnLease : new CC_STL_Car_Info__c[]{};
                leases.add(wrapper);
            }
        }
    }    
}