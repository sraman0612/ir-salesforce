@isTest
public with sharing class CC_Order_LoadDetailsControllerTest {

    @TestSetup 
    private static void createData(){

        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';
        ord1.CC_Carrier_ID__c = 'CPD';

        CC_Order__c ord2 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord2.CC_MAPICS_Order_Key__c = '2158784';
        ord2.CC_Carrier_ID__c = 'CPD';
        ord2.CC_Shipping_Instructions__c = 'Arrive 10 minutes early';
        ord2.CC_Addressee_name__c = 'Coca Cola';
        ord2.CC_Address_line_1__c = '123 Coca Cola Drive';
        ord2.CC_City__c = 'Atlanta';        

        insert new CC_Order__c[]{ord1, ord2};
        
        CC_Order_Item__c orditem1 = TestUtilityClass.createOrderItem(ord1.Id);
        CC_Order_Item__c orditem2 = TestUtilityClass.createOrderItem(ord2.Id);
        insert new CC_Order_Item__c[]{orditem1, orditem2};               

        CC_Car_Item__c carItem1 = TestUtilityClass.createCarItem(orditem1.Id);
        carItem1.Car_Item_Key__c = '01_1_2158783_1_1';
        carItem1.Load_Code__c = '034';
        carItem1.Serial_No_s__c = 'ABC1234;ABC1245';

        CC_Car_Item__c carItem2 = TestUtilityClass.createCarItem(orditem2.Id);
        carItem2.Car_Item_Key__c = '01_1_2158784_1_1';
        carItem2.Ship_Date__c = Date.today();
        carItem2.Load_Code__c = '035';
        carItem2.Serial_No_s__c = 'ABC5678';

        insert new CC_Car_Item__c[]{carItem1, carItem2};
    }
    
    @isTest
    private static void testGetLoadInfoFromCarItemWithKochMatch(){

        CC_Car_Item__c carItem = [Select Id, Car_Item_Key__c, Order_Item__r.Order__c, Order_Item__r.Order__r.Order_Number__c, 
            Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c,
            Koch_Scheduled_Delivery_Date__c, Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c
        From CC_Car_Item__c
        Where Car_Item_Key__c = '01_1_2158783_1_1'];

        Test.startTest(); 
  
        validateResponseWithKochMatch(carItem, null);        
        
        Test.stopTest();        
    }

    @isTest
    private static void testGetLoadInfoFromOrderWithKochMatch(){

        CC_Car_Item__c carItem = [Select Id, Car_Item_Key__c, Order_Item__r.Order__c, Order_Item__r.Order__r.Order_Number__c, 
            Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c,
            Koch_Scheduled_Delivery_Date__c, Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c
        From CC_Car_Item__c
        Where Car_Item_Key__c = '01_1_2158783_1_1'];

        Test.startTest(); 
  
        validateResponseWithKochMatch(carItem, carItem.Order_Item__r.Order__r);        
        
        Test.stopTest();        
    }    

    @isTest
    private static void testGetLoadInfoFromCarItemWithNoKochMatch(){

        CC_Car_Item__c carItem = [Select Id, Car_Item_Key__c, Order_Item__r.Order__c,
            Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c, 
            Koch_Scheduled_Delivery_Date__c, Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c,
            LastModifiedDate, Delivery_Date_Status__c, Scheduled_Ship_Date__c, Ship_Date__c
        From CC_Car_Item__c
        Where Car_Item_Key__c = '01_1_2158784_1_1'];

        CC_Order__c order = [Select Id, Order_Number__c, CC_Shipping_Instructions__c, CC_Addressee_name__c, CC_Address_line_1__c, CC_City__c 
            From CC_Order__c 
            Where Id = :carItem.Order_Item__r.Order__c];

        CC_KochSoapAPIMock.returnNoLoads = true;

        Test.startTest(); 
  
        CC_Order_LoadDetailsController.LoadInfoResponse response = CC_Order_LoadDetailsController.getLoadInfo(carItem.Id);

        system.assertEquals(true, response.success);
        system.assert(String.isBlank(response.errorMessage));

        system.assertEquals(order.Id, response.order.Id);
        system.assertEquals(order.Id, response.orderId); 
        system.assertEquals(false, response.isPortalUser);
        system.assertEquals(1, response.loads.size());
        system.assertEquals(false, response.kochLoadDataFound);
        system.assertEquals(order.Order_Number__c, response.OrderNumber);
        system.assertEquals('2158784', response.OrdRefnum);
        system.assertEquals('Arrive 10 minutes early', response.ShippingInstructions);   
        
        CC_Order_LoadDetailsController.LoadInfo loadInfo = response.loads[0];
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StpRefnum);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StpDepartureDate);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StpArrivalDate);
        system.assertEquals(carItem.Delivery_Date_Status__c.toLowerCase().capitalize(), loadInfo.StpStatus);     
        system.assertEquals('Coca Cola', loadInfo.CmpName);
        system.assertEquals('123 Coca Cola Drive', loadInfo.StpAddress);
        system.assertEquals('Atlanta', loadInfo.CtyName);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.Distance);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StartCmpName);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StartAddress);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.StartCityName);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.LinkURL);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.Quantity);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.ScheduledPickupDateTime);
        system.assertEquals(carItem.Ship_Date__c.format(), loadInfo.ActualPickupDateTime);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.NearestGPSLocation);
        system.assertEquals('', loadInfo.Latitude);
        system.assertEquals('', loadInfo.Longitude);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.ScheduledDeliveryDateTime);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.ActualDeliveryDateTime);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.LatestCalculatedDeliveryDate);
        //system.assertEquals(Datetime.format('MM/dd/yyyy HH:mm:ss'), loadInfo.LastUpdate);     
        system.assertEquals('035', loadInfo.LoadID);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.CarName);
        system.assertEquals('ABC5678', loadInfo.SerialNumbers);                       
        
        Test.stopTest();        
    }    

    private static void validateResponseWithKochMatch(CC_Car_Item__c carItem, CC_Order__c order){

        CC_Order_LoadDetailsController.LoadInfoResponse response = CC_Order_LoadDetailsController.getLoadInfo(order != null ? order.Id : carItem.Id);

        system.assertEquals(true, response.success);
        system.assert(String.isBlank(response.errorMessage));

        system.assertEquals(carItem.Order_Item__r.Order__c, response.order.Id);
        system.assertEquals(carItem.Order_Item__r.Order__c, response.orderId); 
        system.assertEquals(false, response.isPortalUser);
        system.assertEquals(1, response.loads.size());
        system.assertEquals(true, response.kochLoadDataFound);
        system.assertEquals(carItem.Order_Item__r.Order__r.Order_Number__c, response.OrderNumber);
        system.assertEquals('2158783-1-1', response.OrdRefnum);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, response.ShippingInstructions);   
        
        CC_Order_LoadDetailsController.LoadInfo loadInfo = response.loads[0];
        system.assertEquals('2158783', loadInfo.StpRefnum);
        system.assertEquals('09/28/2020 15:27 GMT', loadInfo.StpDepartureDate);
        system.assertEquals('09/28/2020 00:30 GMT', loadInfo.StpArrivalDate);
        system.assertEquals('Complete', loadInfo.StpStatus);
        system.assertEquals('LAKE SHORE CAMPGROUND', loadInfo.CmpName);
        system.assertEquals('2353 N LAKESHORE ROAD', loadInfo.StpAddress);
        system.assertEquals('CARSONVILLE,MI/', loadInfo.CtyName);
        system.assertEquals('0.00', loadInfo.Distance);
        system.assertEquals('CLUB CAR, INC.', loadInfo.StartCmpName);
        system.assertEquals('4125 WASHINGTON RD', loadInfo.StartAddress);
        system.assertEquals('EVANS,GA', loadInfo.StartCityName);
        system.assertEquals('https://tracking.kochcompanies.com/tracking/lookup.aspx?link=', loadInfo.LinkURL);
        system.assertEquals('0.00', loadInfo.OrderNumber);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.Quantity);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.ScheduledPickupDateTime);
        system.assertEquals('10/07/2020 02:50 GMT', loadInfo.ActualPickupDateTime);
        system.assertEquals('4 m N of ADAIRSVILLE, GA ', loadInfo.NearestGPSLocation);
        system.assertEquals('34.42', loadInfo.Latitude);
        system.assertEquals('-84.92', loadInfo.Longitude);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.ScheduledDeliveryDateTime);
        system.assertEquals('10/07/2020 09:15 GMT', loadInfo.ActualDeliveryDateTime);
        system.assertEquals('10/07/2020 09:15 GMT', loadInfo.LatestCalculatedDeliveryDate);
        system.assertEquals('10/08/2020 23:00 GMT', loadInfo.LastUpdate);        
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.LoadID);
        system.assertEquals(CC_KochUtilities.EMPTY_VALUE, loadInfo.CarName);
        system.assertEquals('ABC1234;ABC1245', loadInfo.SerialNumbers);          

        // Verify the Koch fields on the car item were updated
        carItem = [Select Id, Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c,
            Koch_Scheduled_Delivery_Date__c, Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c
        From CC_Car_Item__c
        Where Car_Item_Key__c = '01_1_2158783_1_1'];

        //system.assertEquals(CC_KochUtilities.getDateTimeFromKochData(loadInfo.LastUpdate), carItem.Koch_Last_Update_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData(loadInfo.ScheduledPickupDateTime), carItem.Koch_Scheduled_Pickup_Date__c);
        //system.assertEquals(CC_KochUtilities.getDateTimeFromKochData(loadInfo.ActualPickupDateTime), carItem.Koch_Actual_Pickup_Date__c);
        system.assertEquals(CC_KochUtilities.getDateTimeFromKochData(loadInfo.ScheduledDeliveryDateTime), carItem.Koch_Scheduled_Delivery_Date__c);
        //system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('2020-10-07 09:15:00'), carItem.Koch_Estimated_Delivery_Date__c);
        //system.assertEquals(CC_KochUtilities.getDateTimeFromKochData(loadInfo.ActualDeliveryDateTime), carItem.Koch_Actual_Delivery_Date__c);
        system.assert(carItem.Koch_Last_Check_Date__c <= DateTime.now());
    }
}