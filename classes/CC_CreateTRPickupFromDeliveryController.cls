public with sharing class CC_CreateTRPickupFromDeliveryController {
    	
	@future	    
    public static void cloneDeliveryCases(String casesStr){
    	    	
    	try{
    		
    		Case[] cases = (Case[])JSON.deserialize(casesStr, Case[].class);
    		
			Case[] newCases = new Case[]{};
	
            // Lookup lease information needed on the new pickup cases
            Set<Id> leaseIds = new Set<Id>();

            for (Case c : cases){
                
                if (c.CC_Short_Term_Lease__c != null){
                    leaseIds.add(c.CC_Short_Term_Lease__c);
                }
            }

            Map<Id, CC_Short_Term_Lease__c> leaseMap = new Map<Id, CC_Short_Term_Lease__c>();

            if (leaseIds.size() > 0){
                leaseMap = new Map<Id, CC_Short_Term_Lease__c>([Select Id, Ending_Date__c From CC_Short_Term_Lease__c Where Id in :leaseIds]);
            }

			for (Case c : cases){
	
				// Clone the case
                Case newCase = c.clone(false, true);
                
                system.debug('delivery case: ' + JSON.serialize(newCase));
				
				// Reset some of the fields
				newCase.Status = 'New';
                newCase.Sub_Reason__c = 'Short Term Lease Pick-up';    
                
                // Copy over any quoted information
                newCase.CC_Quote_Date__c = c.CC_Quote_Date__c;
                newCase.CC_Quoted_Price__c = c.CC_Quoted_Price__c;
				
				// Move the delivery location information into the pickup location information
				Boolean useStop2 = String.isNotBlank(c.CC_Stop_2_Name__c);
                newCase.Create_Pickup_Case__c = false;
                newCase.CC_Pickup_Qty_Type_of_Car__c = useStop2 ? c.CC_Stop_2_Qty_Type_of_Car__c : c.CC_Stop_1_Qty_Type_of_Car__c;
				newCase.CC_Pickup_Name__c = useStop2 ? c.CC_Stop_2_Name__c : c.CC_Stop_1_Name__c;
				newCase.CC_Pickup_Contact__c = useStop2 ? c.CC_Stop_2_Contact__c : c.CC_Stop_1_Contact__c;
				newCase.CC_Pickup_Phone__c = useStop2 ? c.CC_Stop_2_Phone__c : c.CC_Stop_1_Phone__c;
				newCase.CC_Pickup_Address__c = useStop2 ? c.CC_Stop_2_Address__c : c.CC_Stop_1_Address__c;
				newCase.CC_Pickup_City__c = useStop2 ? c.CC_Stop_2_City__c : c.CC_Stop_1_City__c;
				newCase.CC_Pickup_State__c = useStop2 ? c.CC_Stop_2_State__c : c.CC_Stop_1_State__c;				
				newCase.CC_Pickup_Zip__c = useStop2 ? c.CC_Stop_2_Zip__c : c.CC_Stop_1_Zip__c;
                
                // Move the pickup location information into the stop 1 delivery location information
                newCase.CC_Stop_1_Name__c = c.CC_Pickup_Name__c;
				newCase.CC_Stop_1_Contact__c = c.CC_Pickup_Contact__c;
				newCase.CC_Stop_1_Phone__c = c.CC_Pickup_Phone__c;
				newCase.CC_Stop_1_Address__c = c.CC_Pickup_Address__c;
				newCase.CC_Stop_1_City__c = c.CC_Pickup_City__c;
				newCase.CC_Stop_1_State__c = c.CC_Pickup_State__c;
				newCase.CC_Stop_1_Zip__c = c.CC_Pickup_Zip__c;

				// Clear out the stop 2 delivery information from the original case
				newCase.CC_Stop_2_Name__c = null;
				newCase.CC_Stop_2_Contact__c = null;
				newCase.CC_Stop_2_Phone__c = null;
				newCase.CC_Stop_2_Address__c = null;
				newCase.CC_Stop_2_City__c = null;
				newCase.CC_Stop_2_State__c = null;
				newCase.CC_Stop_2_Zip__c = null; 
				newCase.CC_Stop_2_Qty_Type_of_Car__c = null;  
				newCase.CC_Stop_2_Instructions__c = null; 	

                // Set the requested delivery date to the lease end date for lease requests
                if (newCase.CC_Short_Term_Lease__c != null){

                    CC_Short_Term_Lease__c lease = leaseMap.get(newCase.CC_Short_Term_Lease__c);

                    system.debug('lease: ' + lease);

                    if (lease != null){
                        newCase.CC_Requested_Delivery_Date__c = lease.Ending_Date__c;
                    }
                }
            
                system.debug('pickup case: ' + JSON.serialize(newCase));

				newCases.add(newCase);
			}
			
			if (newCases.size() > 0){			
				sObjectService.insertRecordsAndLogErrors(newCases, 'CC_CreateTRPickupFromDeliveryController', 'cloneDeliveryCases');
			}
    	}
        catch(Exception e){
        	apexLogHandler log = new apexLogHandler('CC_CreateTRPickupFromDeliveryController', 'cloneDeliveryCases', e.getMessage());
        	log.saveLog();
        }
    }
}