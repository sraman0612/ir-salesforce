@isTest
public class CC_addAssets2FleetAnalysis_Test {
  
  static testmethod void test1(){
    //create an account
    TestDataUtility testData = new TestDataUtility();
    TestDataUtility.createStateCountries();
    CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
    salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
    insert salesRepacc;
    List<account> acclist = new List<account>();
    account acc = testData.createAccount('Club Car: Channel Partner');
    acc.CC_Sales_Rep__c = salesRepacc.Id;
    acc.CC_Price_List__c='PCOPI';
    acc.BillingCountry = 'USA';
    acc.BillingCity = 'Augusta';
    acc.BillingState = 'GA';
    acc.BillingStreet = '2044 Forward Augusta Dr';
    acc.BillingPostalCode = '566';
    acc.CC_Shipping_Billing_Address_Same__c = true;
    insert acc;
    Product2 prod =  TestUtilityClass.createProduct2();
    prod.CC_Market_Type__c = 'Golf';
    prod.CC_Vendor__c      = 'TEST GOLF VENDOR';
    prod.Name              = 'AGOLFCAR';
    prod.M3__c             = 'Golf';
    prod.P3__c = 'Competitor';
    prod.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Club_Car'].Id;
    insert prod;
    CC_Asset_Group__c assetGroup = TestDataUtility.createAssetGroup(acc.id,prod.id);
    assetGroup.Acquired_Date__c = date.today()-1;
    assetGroup.Fleet_Status__c    ='Lease-New';
    assetGroup.Quantity__c = 20;
    insert assetGroup;
    CC_Asset__c [] assetDetailLst = [SELECT Id FROM CC_Asset__c WHERE  Asset_Group__c = :assetGroup.Id];
    system.assert(assetDetailLst.isEmpty());
    CC_Fleet_Analysis__c fa = new CC_Fleet_Analysis__c();
    fa.Account__c=acc.id;
    fa.Asset__c = assetGroup.Id;
    insert fa;
    CC_Asset_Analysis__c [] assetAnalysisLst = [SELECT Id FROM CC_Asset_Analysis__c WHERE Fleet_Analysis__c = :fa.Id];
    system.assert(assetAnalysisLst.isEmpty());
    test.startTest();
    CC_addAssets2FleetAnalysis.addAssets(new CC_Fleet_Analysis__c[]{fa});
    test.stopTest();
    CC_Asset_Analysis__c [] assetAnalysisLst2 = [SELECT Id FROM CC_Asset_Analysis__c WHERE Fleet_Analysis__c = :fa.Id];
    system.assertEquals(assetAnalysisLst2.size(),20);
    CC_Asset__c [] assetDetailLst2 = [SELECT Id FROM CC_Asset__c WHERE  Asset_Group__c = :assetGroup.Id];
    system.assertEquals(assetDetailLst2.size(),20);
  }
}