global with sharing class CC_PavilionTrainingTools {

         public CC_PavilionTrainingTools(CC_PavilionTemplateController controller) {
        
         }
         
         global static List < ContentVersion > getContentList() {
           Id  usrId  = userinfo.getUserId();
           Id accId = [SELECT AccountId from User  where id =:usrId].AccountId;
          //code contributed by @Priyanka Baviskar for issue no 7627734.
          
           if(accId ==null)
           {
           List < ContentVersion > contentList = new List < ContentVersion > ();
           contentList = [select id, Title, Content_Title__c, Content_Sub_title__c, Content_Body__c, ContentUrl, Description, FileType, CC_Date__c,
           RecordType.Name from ContentVersion where RecordType.DeveloperName = 'CC_System_Process_Training' and IsLatest = true];
           return contentList;

           }
           else
           {  
           Account acc = [select Type,CC_Global_Region__c from Account where id=:accId];
           List < ContentVersion > contentList = new List < ContentVersion > ();
           contentList = [select id, Title, Content_Title__c, Content_Sub_title__c, Content_Body__c, ContentUrl, Description, FileType, CC_Date__c,
           RecordType.Name from ContentVersion where RecordType.DeveloperName = 'CC_System_Process_Training' and CC_Type__c includes (:acc.Type)
                                    and  CC_Region__c INCLUDES(:acc.CC_Global_Region__c) and IsLatest = true];
          return contentList;
          }
         // return contentList;
        }
}