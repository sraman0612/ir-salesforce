Global class CC_PavilionOrderHistoryController {
    
    public String AccountName {get;set;}
    public String userAccountId {get;set;}
    public Boolean subscriptionsEnabled {get {return CC_LoadTrackingUtilities.getSubscriptionsEnabled();} set;}
    public CC_PavilionOrderHistoryController(){} 
    
    public CC_PavilionOrderHistoryController(CC_PavilionTemplateController controller) {userAccountId = controller.acctid;}
    
    @RemoteAction 
    global static List<orderRowWrapper> getOrders(String userAccountId){
        List<orderRowWrapper> wrapperList=new List<orderRowWrapper>();
        Id UsedCarRecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Used_Car_Order').getRecordTypeId();
        Id carRecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Cars_Ordering').getRecordTypeId();
        Set<Id> orderIds = new Set<Id>();
        for(CC_Order__c o : [SELECT Id
                               FROM CC_Order__c
                              WHERE CC_Quote_Order_Date__c >= LAST_YEAR AND
                                    (
                                        (CC_Account__c = :userAccountId AND (RecordTypeId = :carRecordTypeId OR RecordTypeId = :UsedCarRecordTypeId) ) OR
                                        (RecordTypeId = :carRecordTypeId AND CC_Servicing_Dealer__c != null AND CC_Servicing_Dealer__c = :userAccountId)
                                    )]){
            orderIds.add(o.Id);
        }
        for(CC_Order_Item__c oi : [SELECT Product__r.Product_Line__c,CC_Quantity__c, Order__r.CC_Accept_Status__c, Order__r.CC_Quote_Order_Date__c, Order__r.CC_Request_Date__c,
                                          Order__r.CC_Addressee_name__c, Order__r.Order_Number__c, Order__r.CC_Status__c, Order__r.PO__c, 
                                          Order__r.CC_Priority_ID__c, Order__r.Hold_Code__c, Order__r.CC_Carrier_ID__c, Order__r.ASAP_Flag__c, 
                                          Order__c, Order__r.CC_Account__c, Order__r.RecordType.DeveloperName, Order__r.CC_Original_Request_Date__c, LastModifiedDate,
                                          (SELECT Id,Estimated_Ship_Date__c,Delivery_Date_c__c,Drop_Ship_Sequence__c,Load_Code__c,Truck_Quantity__c,
                                                  Scheduled_Ship_Date__c, Shipment_Header_Number__c, Delivery_Date_Status__c, Delivery_Date_Display__c, Scheduled_Ship_Date_Display__c,
                                                  Last_Delivery_Date_Change__c, LastModifiedDate, Delivered_Date_Calc__c, 
                                                  Koch_Load_Code_Out_Of_Sync__c, Koch_Actual_Pickup_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Last_Update_Date__c, Koch_Time_Zone_Offset__c, Koch_Scheduled_Delivery_Date__c, Original_Delivery_Date_Display__c                                                  
                                             FROM Car_items__r),
                                          (SELECT Order_Shipment__r.id, Order_Shipment__r.Ship_Date__c,Order_Shipment__r.CC_Invoice_Key__c, Order_Shipment__r.Carrier__c,
                                                  Order_Shipment__r.Shipment_Header_Number__c
                                             FROM Order_Shipment_Items__r)
                                     FROM CC_Order_Item__c
                                    WHERE Order__c IN :orderIds AND
                                          Product__r.CC_Item_Class__c IN ('NEW','REFB','USED','LCAR')]){
            if(oi.CC_Quantity__C > 0){                                            
                if(null != oi.Car_items__r && !oi.Car_items__r.isempty()){
                  for(CC_Car_Item__c ci : oi.Car_items__r){
                      if(null != oi.Order_Shipment_Items__r){
                          Boolean orwCreated = false;
                          for(CC_Order_Shipment_Item__c osi : oi.Order_Shipment_Items__r){
                              if(ci.Shipment_Header_Number__c == osi.Order_Shipment__r.Shipment_Header_Number__c){
                                  orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,osi,ci);
                                  wrapperList.add(orw);
                                  orwCreated = TRUE;
                                  break;
                              }
                          }
                          if(!orwCreated){
                              orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,null,ci);
                              wrapperList.add(orw);
                          }
                      } else {
                          orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,null,ci);
                          wrapperList.add(orw);
                      }
                  }
                } else if (null != oi.Order_Shipment_Items__r && !oi.Order_Shipment_Items__r.isempty()){
                      if(oi.Order_Shipment_Items__r.size() ==1){
                          orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,oi.Order_Shipment_Items__r[0],null);
                          wrapperList.add(orw);
                      } else {
                          for(CC_Order_Shipment_Item__c osi : oi.Order_Shipment_Items__r){
                              orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,osi,null);
                              wrapperList.add(orw);  
                          }
                      }
                } else {
                    orderRowWrapper orw = new orderRowWrapper(oi,userAccountId,null,null);
                    wrapperList.add(orw);    
                }
            }
        }
        return wrapperList; 
    }
  
    global class orderRowWrapper {
        public Id orderId {get;set;} 
        public Id carItemId {get;set;}
        public CC_Car_Item__c carItem {get;set;}
        public String lastModifiedDate {get;set;}
        public String recordTypeName {get;set;}
        public string acceptStatus {get;set;} 
        public date orderDate {get;set;} 
        public date estShipWeek {get;set;} 
        public String schShipDate {get;set;}
        public String delDate {get;set;} 
        public Boolean isKochLoad {get;set;}
        public Boolean delDateChanged {get;set;}
        public String delStatus {get;set;}
        public string dropSeq {get;set;}
        public string load {get;set;}
        public string shipTo {get;set;} 
        public string orderNum {get;set;} 
        public string poNum {get;set;} 
        public string crtsts {get;set;}
        public string holdCode {get;set;} 
        public string qty {get;set;} 
        public string model {get;set;} 
        public string shipVIA {get;set;} 
        public string shipASAP {get;set;} 
        public string assetLnk {get;set;}
        public string orderStatus {get;set;}
        public string orderFilters {get;set;}
        public String deliveryDateDisplay{get;set;}    
        public Date requestedShipDate {get;set;}  
        public orderRowWrapper(CC_Order_Item__c oi, String usrAcctId, CC_Order_Shipment_Item__c osi, CC_Car_Item__c ci){
            this.orderId = oi != null ? oi.Order__c : null;
            this.recordTypeName = oi != null && oi.Order__c != null ? oi.Order__r.RecordType.DeveloperName : '';
            this.carItemId = ci != null ? ci.Id : null;
            this.carItem = ci != null ? ci : null;
            this.delStatus = ci != null ? ci.Delivery_Date_Status__c : '';
            this.delDateChanged = CC_LoadTrackingUtilities.getDeliveryDateChanged(ci);            
            this.isKochLoad = ci != null ? (ci.Koch_Last_Update_Date__c != null && !ci.Koch_Load_Code_Out_Of_Sync__c) : false;

            Datetime lastModifiedDate = ci != null ? (ci.Last_Delivery_Date_Change__c != null ? ci.Last_Delivery_Date_Change__c : ci.LastModifiedDate) : oi.LastModifiedDate;
            
            String lastModifiedDateStr = '';

            if (lastModifiedDate != null){

                if (ci != null && ci.Koch_Time_Zone_Offset__c != null){

                    // Apply time zone offset
                    lastModifiedDate = lastModifiedDate.addHours(Integer.valueOf(ci.Koch_Time_Zone_Offset__c));
                }
                
                lastModifiedDateStr = lastModifiedDate.formatGMT('MM/dd/yyyy hh:mm a');
            }
            
            this.lastModifiedDate = lastModifiedDateStr;            
            this.acceptStatus = null != oi.Order__r.CC_Accept_Status__c ? oi.Order__r.CC_Accept_Status__c : '';
            this.requestedShipDate = oi != null && oi.Order__c != null ? oi.Order__r.CC_Request_Date__c : null;
            this.orderDate = oi.Order__r.CC_Quote_Order_Date__c;
            this.orderStatus = oi.Order__r.CC_Status__c == 'CLOSED' ? 'CLOSED' : 'OPEN'; 
            this.orderFilters = this.orderStatus;
            this.shipTo = oi.Order__r.CC_Addressee_name__c;
            
            if(this.orderStatus == 'CLOSED'){

                if(null == osi) {
                    this.orderNum = oi.Order__r.Order_Number__c;
                } 
                else {
                    this.orderNum = '<a target="_blank" href="' + site.getPathPrefix() + '/CC_PavilionInvoicedetail?invid=' + osi.Order_Shipment__r.CC_Invoice_Key__c + '">' + oi.Order__r.Order_Number__c+'</a>';
                }
            } 
            else {
                this.orderNum = '<a href="' + site.getPathPrefix() + '/CC_PavilionOrderDetail?id=' + oi.Order__c + '" target="_blank">' + oi.Order__r.Order_Number__c + '</a>';
            }

            this.poNum = null != oi.Order__r.PO__c ? oi.Order__r.PO__c : '';
            this.crtsts = null != oi.Order__r.CC_Priority_ID__c ? '<a href="#" onclick="OpenCreditStatusDiv();">' + oi.Order__r.CC_Priority_ID__c + '</a>' : '';
            this.holdCode = null != oi.Order__r.Hold_Code__c ? oi.Order__r.Hold_Code__c : '';
            this.model = oi.Product__r.Product_Line__c;
            this.shipVIA = CC_LoadTrackingUtilities.getCarrier(ci, (oi != null ? oi.Order__r : null), (osi != null ? osi.Order_Shipment__r : null));
            this.shipASAP = oi.Order__r.ASAP_Flag__c ? 'Y': 'N';
            this.assetLnk = '<a  target="_blank"  href="' + Site.getPathPrefix() + '/CC_ViewAssetDetails?OrdId=' + oi.Order__r.Id + '">View Asset Details</a>';
            this.qty = null != ci && null != ci.Truck_Quantity__c ? string.valueOf(ci.Truck_Quantity__c) : string.valueOf(oi.CC_Quantity__c);
            this.delDate = ci != null && ci.Delivery_Date_Display__c != null ? ci.Delivery_Date_Display__c : '';
            this.schShipDate = ci != null && ci.Scheduled_Ship_Date_Display__c != null ? ci.Scheduled_Ship_Date_Display__c : '';
            this.dropSeq='';

            if(null != ci && null != ci.Drop_Ship_Sequence__c){
                
                String ds = string.valueOf(ci.Drop_Ship_Sequence__c);
                
                if (ds.length()==3){
                    ds = ' 0' + ds.left(2) + ':' + ds.substring(1,2);
                } 
                else if (ds.length()==4){
                    ds = ' ' + ds.left(3) + ':' + ds.substring(2,2);
                }

                this.dropSeq=ds;
            }

            this.load = null != ci && null != ci.Load_Code__c ? ci.Load_Code__c : '';
            this.estShipWeek = ci != null ? ci.Estimated_Ship_Date__c : null; 

            Date requestDate = oi.Order__r.CC_Original_Request_Date__c; 

            if (ci != null && ci.Koch_Actual_Pickup_Date__c != null && !ci.Koch_Load_Code_Out_Of_Sync__c){
                requestDate = ci.Koch_Actual_Pickup_Date__c.date();
            }            
            else if (osi != null && osi.Order_Shipment__r.Ship_Date__c != null){
                requestDate = osi.Order_Shipment__r.Ship_Date__c;
            }  
            else if (ci != null && ci.Koch_Scheduled_Pickup_Date__c != null && !ci.Koch_Load_Code_Out_Of_Sync__c){
                requestDate = ci.Koch_Scheduled_Pickup_Date__c.date();
            }                                                 
            else if (ci != null && ci.Scheduled_Ship_Date__c != null){
                requestDate = ci.Scheduled_Ship_Date__c;
            }    
            else if (ci != null && ci.Estimated_Ship_Date__c != null){
                requestDate = ci.Estimated_Ship_Date__c;
            }                  
            
            Date deliveryDate; // Actual or scheduled

            if (ci != null){

                if (ci.Delivered_Date_Calc__c != null){
                    deliveryDate = ci.Delivered_Date_Calc__c.date();
                }
                else{
                    
                    if (ci.Koch_Scheduled_Delivery_Date__c != null && !ci.Koch_Load_Code_Out_Of_Sync__c){
                        deliveryDate = ci.Koch_Scheduled_Delivery_Date__c.date();
                    }
                    else if (ci.Delivery_Date_c__c != null){
                        deliveryDate = ci.Delivery_Date_c__c;
                    }
                }
            }

            Date d = system.today();

            if (this.orderDate.year() == d.year() || (requestDate != null && requestDate.year() == d.year())){
                this.orderFilters += 'THISY';
            } 
            else if (this.orderDate.year() == (d.year()-1) || (requestDate != null && requestDate.year() == (d.year()-1))){
                this.orderFilters += 'LASTY';
            }
            
            if(this.orderDate <= d && this.orderDate >= d.addDays(-120)){
                this.orderFilters += 'LAST120D';
            }

            if (requestDate != null){
                
                if(requestDate <= d && requestDate >= d.addDays(-30)){
                    this.orderFilters += 'LASTM';
                }   
                
                if (requestDate.month() == d.month() && requestDate.year() == d.year()){
                    this.orderFilters += 'THISM';
                }             

                if (requestDate >= d.toStartOfWeek()){

                    if (requestDate < d.toStartOfWeek().addDays(7)){
                        this.orderFilters += 'THISW';
                        this.orderFilters += 'LAST_THIS_NEXT';
                    } 
                    else if (requestDate < d.toStartOfWeek().addDays(14)) {
                        this.orderFilters += 'NEXTW';
                        this.orderFilters += 'LAST_THIS_NEXT';
                    }
                }  
                else if (requestDate > d.toStartOfWeek().addDays(-7)){
                    this.orderFilters += 'LASTW';
                    this.orderFilters += 'LAST_THIS_NEXT';
                }
            }

            if (deliveryDate != null){

                if (deliveryDate >= d.toStartOfWeek()){

                    if (deliveryDate < d.toStartOfWeek().addDays(7)){                    
                        this.orderFilters += 'DELV_THIS';
                    }
                    else if (deliveryDate < d.toStartOfWeek().addDays(14)) {
                        this.orderFilters += 'DELV_NEXT';
                    }                    
                }
                else if (deliveryDate > d.toStartOfWeek().addDays(-7)){
                    this.orderFilters += 'DELV_LAST';
                }                
            }

            if(oi.Order__r.RecordType.DeveloperName =='CC_Used_Car_Order'){ 
                this.orderFilters += 'USED';
            } 
            else if (usrAcctId == oi.Order__r.CC_Account__c){
                this.orderFilters += 'CAR';
            } 
            else {
                this.orderFilters += 'NATACCT';
            }
        }
    }
}