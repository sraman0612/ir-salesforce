//Club Car
//Cal Steele 11-15-17
//Ben Lorenz Consulting LLC

@isTest
public class CC_CreateComplianceRecordsTest{
        
        @testSetup static void setupData() {
            List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
            psettingList = TestUtilityClass.createPavilionSettings();
        
            insert psettingList;
    }
    
    static testMethod void testComplianceRecords() { 
        
        // Creation Of Test Data
        Account newAccount = TestUtilityClass.createAccount();
        insert newAccount;
        
        Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
        sampleContact.CC_User_Validation__c = TRUE;
        insert sampleContact;
        
        //String userNavigationProfile = newUser.CC_Pavilion_Navigation_Profile__c;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
        insert newPavilionNavigationProfile;
        
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
        insert newUser;
            
        User u1 = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
        
        system.runAs(u1){
        Test.startTest();
       
        //Create test compliance record
        CC_CreateComplianceRecords.createComplianceRecords();
        
        Test.stopTest();
        
        }
    }

}