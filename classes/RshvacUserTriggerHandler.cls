/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Trigger Handler for the "User" SObject.
              This class should only be called by the trigger "RshvacUserTrigger"
              And should be without sharing since triggers execute in System mode
------------------------------------------------------------*/

public without sharing class RshvacUserTriggerHandler {

  // Map that will contain all the Accounts owned by an User
  public Map<String, List<Account>> accountsBySalesPerson = new Map<String, List<Account>>();
  // Map that will contain all the Accounts that need to be updated
  public Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
  // Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';

  public RshvacUserTriggerHandler(){
  }

  public void bulkAfter(){
    if (Trigger.isInsert || Trigger.isUpdate){
      Set<String> salesPersonIds = new Set<String>();
      // Iterate over the User to collect the Sales Person Ids
      for (SObject sObj : Trigger.new){
        User newUser = (User) sObj;
        // Get the old record if it's an update
        User oldUser = Trigger.isUpdate ? (User) Trigger.oldMap.get(newUser.Id) : new User();
        // We check here if the Sales Person Id is not blank of if it actually changed
        // To avoid any possible governor limit error in the account query
        if (String.isNotBlank(newUser.Sales_Person_Id__c) && newUser.Sales_Person_Id__c != oldUser.Sales_Person_Id__c){
          salesPersonIds.add(newUser.Sales_Person_Id__c);
          accountsBySalesPerson.put(newUser.Sales_Person_Id__c, new List<Account>());
        }
      }
      // If it's needed, get the accounts that should have a new Owner
      if (!salesPersonIds.isEmpty()){
        // Get the RS_HVAC Record Type Id
        Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;

        for (Account a : [SELECT Id, OwnerId, Sales_Person_Id__c FROM Account WHERE Sales_Person_Id__c IN :salesPersonIds AND RecordTypeId = :rshvacRTID]){
          accountsBySalesPerson.get(a.Sales_Person_Id__c).add(a);
        }
      }
    }
  }

  public void afterInsert(User newUser){
    // Check if we need to assign any existing Account to this new User
    if (String.isNotBlank(newUser.Sales_Person_Id__c)){
      for (Account a : accountsBySalesPerson.get(newUser.Sales_Person_Id__c)){
        a.OwnerId = newUser.Id;
        accountsToUpdate.put(a.Id, a);
      }
    }
  }

  public void afterUpdate(User oldUser, User newUser){
    // Check if we need to assign any existing Account to this new User
    if (String.isNotBlank(newUser.Sales_Person_Id__c) && newUser.Sales_Person_Id__c != oldUser.Sales_Person_Id__c){
      for (Account a : accountsBySalesPerson.get(newUser.Sales_Person_Id__c)){
        a.OwnerId = newUser.Id;
        accountsToUpdate.put(a.Id, a);
      }
    }
  }

  public void andFinally(){
    if (!accountsToUpdate.isEmpty()){
      update accountsToUpdate.values();
    }
  }
}