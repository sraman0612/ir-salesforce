global class CC_PavilionCoopHOIClaimRequestController {

    /* member variable */

    public String userAccountId{get; set;}
    
        
    /* constructor */    

    public CC_PavilionCoopHOIClaimRequestController()
    {    
        List<User> lstUser = [SELECT AccountId, ContactId FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        if(lstUser.isEmpty()) return;
         UserAccountId = lstUser[0].AccountId;
        
    }
    
    /*Remote method to be called from CC_PavilionCoopHOIClaimRequests page via JS Remoting*/
    
    @RemoteAction
    global static String getCoopHOIClaimRequestJson(String userAccountId)
    {
        set<String> hoiName = new set<String>();
        Map<String,String> hoiMap = new Map<String,String>();
        list<orderWrapper> orderwithIVllist = new list<orderWrapper>();
        List<RecordType> rt = [select id, Name from RecordType where SobjectType = 'Hole_In_One_Claim__c' and Name = 'Hole-In-One'  Limit 1 ];
         
         
        List<Hole_In_One_Claim__c> lstCoopClaim = new List<Hole_In_One_Claim__c>();
        if(userAccountId != null)
        {
            lstCoopClaim =[SELECT Id, Name,Parent_Request_Name__c, Amount_of_Vehicle_on_Invoice__c, Club_Car_Invoice_Number__c,
                        (Select Comments From ProcessSteps Order by CreatedDate desc Limit 1), Date_of_Hole_In_One__c, HIO_Claim_amount__c, Status__c,
                          Vehicle_Serial_Number__c, Winner_Name__c from Hole_In_One_Claim__c where Coop_Request__r.Contract__r.AccountId=:userAccountId AND RecordTypeId =: rt[0].id];
        }
        for(Hole_In_One_Claim__c h:lstCoopClaim){
            String queryString = 'INT-'+h.Name;
            hoiName.add(queryString);
        }
        list<CC_Order_Shipment__c> ordershiplist = [SELECT id,Invoice_Number__c,Order__r.PO__c, Order__r.CC_Account__r.CC_Customer_Number__c, Invoice_Date__c, Order__c,  Order__r.CC_Status__c FROM CC_Order_Shipment__c WHERE Order__r.PO__c IN:hoiName]; 
        system.debug('@@@'+ordershiplist);
        if(ordershiplist.size()>0){
        for(CC_Order_Shipment__c ord:ordershiplist){
             String invoiceName = String.valueOf(ord.Invoice_Number__c)+'_'+String.valueOf(ord.Order__r.CC_Account__r.CC_Customer_Number__c)+'_'+String.valueOf(ord.Invoice_Date__c);
             hoiMap.put(ord.Order__r.PO__c,invoiceName);
             system.debug('@@@'+hoiMap);
            }
         }
         
        for(Hole_In_One_Claim__c hc:lstCoopClaim){
             String comm;
             if(hc.ProcessSteps.size() >0) {
                comm =  hc.ProcessSteps[0].Comments;
             }
             String poid = 'INT-'+hc.Name;
             orderwithIVllist.add(new orderWrapper(hoiMap.get(poid),hc.name,hc.Date_of_Hole_In_One__c,hc.Status__c,hc.Vehicle_Serial_Number__c,comm,hc.id));  
             
        }
        if(!orderwithIVllist.isEmpty())
        {
            String jsonData = (String)JSON.serialize(orderwithIVllist);
            system.debug('$$$$'+jsonData);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
    
    @RemoteAction
    global static String submitCoopClaimData(String claimId, String winnerName,String winnerPhNum,String firstMemName,String firstMemPhNum,String SecondMemName,String secondMemPhNum,
                                            String ThirdMemName,String thirdMemPhNum,String OfficialName,String officialPhNum,String vehSerialNum,String clubCarInvNum,String amtOnInv,
                                            String comments,String invoiceAttachmentName,String invoiceAttachmentBody)
    {
        try
        {
            Hole_In_One_Claim__c objCCCoopClaim = [Select id,Name from Hole_In_One_Claim__c where id =: claimId ];//Co_Op_Account_Summary__r.CC_Initial_Funds__c
            //String[] s = invoicedate.split('/');
            //Date newInvoiceDate = Date.newInstance(Integer.valueOf(s[2]),Integer.valueOf(s[1]),Integer.valueOf(s[0]));
                Decimal amntonVeh = Double.valueOf(amtOnInv); 
                Hole_In_One_Claim__c hinc = new Hole_In_One_Claim__c();
                hinc.id = id.valueOf(claimId);

                hinc.Winner_Name__c = winnerName;
                hinc.First_Member_Name__c = firstMemName;
                hinc.Second_Member_Name__c = SecondMemName;
                hinc.Third_Member_Name__c = ThirdMemName;
                hinc.Official_s_Name__c = OfficialName;

                hinc.Vehicle_Serial_Number__c = vehSerialNum;
                hinc.Club_Car_Invoice_Number__c  = clubCarInvNum;
                hinc.Amount_of_Vehicle_on_Invoice__c = Integer.valueOf(amtOnInv);
                hinc.Comments__c = comments;
                hinc.Winner_Phone_Number__c = winnerPhNum;
                hinc.First_Member_Phone_Number__c = firstMemPhNum;
                hinc.Second_Member_Phone_Number__c = secondMemPhNum;
                hinc.Third_Member_Phone_Number__c = thirdMemPhNum;
                hinc.Official_s_Phone_Number__c = officialPhNum;
                hinc.Status__c = 'Submitted';                                                                        

            if(invoiceAttachmentName != null && invoiceAttachmentBody != null){
               
            }
            update hinc;
            
            List<Attachment> lstAttachment = new List<Attachment>();
            
            if(String.isNotBlank(invoiceAttachmentName) && String.isNotBlank(invoiceAttachmentBody ) ) {
                Attachment objInvoiceAttachement = new Attachment();
                objInvoiceAttachement.body = EncodingUtil.base64Decode(invoiceAttachmentBody);
                objInvoiceAttachement.name = invoiceAttachmentName;
                objInvoiceAttachement.parentId = objCCCoopClaim.Id;
                lstAttachment.add(objInvoiceAttachement);
            }
            
            
            if(!lstAttachment.isEmpty()){
                insert lstAttachment;
            }
            return objCCCoopClaim.name +' is '+hinc.Status__c;
            //return 'Success';
        }
        catch(Exception e)
        {
            System.debug('Exception e'+e);
            return 'Failure';
        }
    }
     @RemoteAction
    global static String getClaimByIdJson(String claimId)
    {
        List<Hole_In_One_Claim__c> lstClaims = [SELECT Id, Winner_Name__c, Winner_Phone_Number__c,First_Member_Name__c,Second_Member_Name__c,Third_Member_Name__c,Official_s_Name__c,
                                                Date_of_Hole_In_One__c,Vehicle_Serial_Number__c,Club_Car_Invoice_Number__c,Amount_of_Vehicle_on_Invoice__c,Comments__c,
                                                First_Member_Phone_Number__c,Second_Member_Phone_Number__c,Third_Member_Phone_Number__c,Official_s_Phone_Number__c,Coop_Request__c,Status__c,
                                                (SELECT StepStatus, Comments FROM ProcessSteps Order by CreatedDate desc Limit 1), (SELECT name FROM Attachments)
                                                FROM Hole_In_One_Claim__c WHERE id =: claimId Limit 1];
        
        
        
        if(!lstClaims.isEmpty())
        {
            String jsonData = (String)JSON.serialize(lstClaims);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
    public class orderWrapper{
         //public Hole_In_One_Claim__c ccord {get;set;}
         public String invoiceName {get;set;}
         public String claimid {get;set;}
         public Date dateofHOI {get;set;}
         public String status {get;set;}
         public String VehSerNum {get;set;}
         public String ApprovalComm {get;set;}
         public String Hoiid{get;set;}
         public orderWrapper(String invname1, String claimid1,Date dateofHOI1, String status1,String VehSerNum1,String ApprovalComm1, String Hoiid1){
             
             invoiceName = invname1;
             claimid = claimid1;
             dateofHOI = dateofHOI1;
             status = status1;
             VehSerNum =VehSerNum1;
             ApprovalComm = ApprovalComm1;
             Hoiid = Hoiid1;
         }
      }
    
}