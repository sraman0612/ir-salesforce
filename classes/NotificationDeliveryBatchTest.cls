@isTest
public with sharing class NotificationDeliveryBatchTest {

    @testSetup
    public static void createData(){
        NotificationDeliveryServiceTest.createData();
    }

    @isTest
    private static void deliverNotificationMessagesTestScheduler(){
        
        Test.startTest();

        System.schedule('NotificationDeliveryBatch-Real-Time1-Testing123', '0 0 * * * ?', new NotificationDeliveryBatch('Real-Time'));

        Test.stopTest();

        system.assertEquals(1, [Select Count() From AsyncApexJob Where ApexClass.Name = 'NotificationDeliveryBatch' and JobType = 'BatchApex']);
    }

    @isTest
    private static void deliverNotificationMessagesTestRealTimeBatchDisabled(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        Notification_Settings__c settings = Notification_Settings__c.getOrgDefaults();
        settings.Enable_Notification_Batch_Job_Prod__c = false;
        settings.Enable_Notification_Batch_Job_Sandbox__c = false;    
        update settings;    

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend, true);
    }      

    @isTest
    private static void deliverNotificationMessagesTestRealTimeSuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }    

    @isTest
    private static void deliverNotificationMessagesTestRealTimeOutsideDeliveryWindow(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        Contact[] contacts = [Select Id, CTS_Notifications_Delivery_Window_Start__c, CTS_Notifications_Delivery_Window_End__c From Contact];

        for (Contact c : contacts){
            c.CTS_Notifications_Delivery_Window_Start__c = Time.newInstance(0, 0, 0, 0);
            c.CTS_Notifications_Delivery_Window_End__c = Time.newInstance(0, 0, 0, 0);            
        }

        update contacts;

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend, true);
    }    

    @isTest
    private static void deliverNotificationMessagesTestRealTimeFailure(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());
        NotificationTwilioServiceMock.forceError = true;

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend, true);
    }    

    @isTest
    private static void deliverNotificationMessagesTestDailySuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());
        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Daily';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(2, messagesToSend.size());

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }  
    
    @isTest
    private static void deliverNotificationMessagesTestDailyFailure(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Daily';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(2, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;
        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }     

    @isTest
    private static void deliverNotificationMessagesTestWeeklySuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Weekly';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(1, messagesToSend.size());

        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }   
    
    @isTest
    private static void deliverNotificationMessagesTestWeeklyFailure(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Weekly';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(1, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;
        Database.executeBatch(new NotificationDeliveryBatch(deliveryFrequency), 50);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }      
}