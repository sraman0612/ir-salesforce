public class CC_PavilionCreditCenterHomeController {
    public CC_Pavilion_Content__c HomePageText{get;set;}

    public CC_PavilionCreditCenterHomeController(CC_PavilionTemplateController controller) {
    HomePageText=[select CC_Body_1__c  from CC_Pavilion_Content__c where  Title__c='Credit Center Home Text' and RecordType.Name='Home Page Text'];
    }
  
}