/**************************************************************************
 * Description   : contains all common methods used by Deal car functionality
 * Created Date  : 14/06/2017
 * ***********************************************************************/

public class CC_Utility {
    
    public static Id clubcarRectypeId  = Schema.SObjectType.OBM_Notifier__c.getRecordTypeInfosByName().get(StringConstantsFinalClass.OBM_notifierClubcarRecordytype).getRecordTypeId();
    public static Id naairRectypeId  = Schema.SObjectType.OBM_Notifier__c.getRecordTypeInfosByName().get(StringConstantsFinalClass.OBM_notifierNAAirRecordytype).getRecordTypeId();
    
    public static OBM_Notifier__c createOBMNotifier(object record, string objType){
        OBM_Notifier__c notifier = new OBM_Notifier__c();

        Account acc = new Account();
        contact con    = new contact();
        opportunity opp = new opportunity();
        
        notifier.Operation__c   = 'Delete';
        notifier.Object_Type__c = objType;
        notifier.RecordTypeId   = clubcarRectypeId;
        
        if(objType == 'Account'){
           acc                     = (Account) record;
           notifier.Name           = acc.Name;
            system.debug('@@@@acc.Siebel_ID__c'+acc.Siebel_ID__c);
           notifier.External_Id__c = acc.CC_Siebel_ID__c; 
        }
        else if(objType == 'Contact'){
           con                     = (Contact) record;
           notifier.Name           = con.FirstName;
           notifier.External_Id__c = con.CC_Siebel_ID__c; 
        }
        else if(objType == 'Opportunity'){
            opp                     = (opportunity)record;
            notifier.Name           = opp.Name;
            notifier.External_Id__c = opp.Siebel_ID__c; 
        } 
        return notifier;
    }
    
    
    public static Set<String> getChildAccountIds(String accId){
        
        Set<String> childAccountIds = new Set<String>(new Map<String,Account>([select id from account where ParentId=:accId]).keyset());
        childAccountIds.add(accId);
        return childAccountIds;
        
    }
    
    
    
}