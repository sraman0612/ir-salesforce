@isTest
public class CC_PavilionPreApprovalRequestsCont_test {
    static testmethod void UnitTest_PavilionPreApprovalRequest()
    {
        // Creation of test data
        Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        System.assertEquals('Sample Account',sampleAccount.Name);
        System.assertNotEquals(null, sampleAccount.Id);
        Account newSampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert newSampleAccount;
        System.assertEquals('Sample Account',newSampleAccount.Name);
        System.assertNotEquals(null, newSampleAccount.Id);
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',sampleAccount.id);
        insert sampleContract;
        System.assertEquals(sampleAccount.Id, sampleContract.AccountId);
        System.assertNotEquals(null, sampleContract.Id);
         RecordType rt = [select id, Name from RecordType where SobjectType = 'CC_Hole_in_One__c' and Name = 'Pre-Approved Request'  Limit 1 ];
        CC_Hole_in_One__c sampleCoopRequest = TestUtilityClass.createCoopRequest(sampleContract.Id);
        sampleCoopRequest.RecordTypeId=rt.id;
        insert sampleCoopRequest;        
   
        CC_PavilionPreApprovalRequestsController coopHOIClaimRequestController =new CC_PavilionPreApprovalRequestsController();
        test.startTest();
        CC_PavilionPreApprovalRequestsController.getPreApprovalRequestsJson(sampleAccount.id);
        CC_PavilionPreApprovalRequestsController.getPreApprovalRequestsJson(newSampleAccount.id);
        test.stopTest();
    }
    
}