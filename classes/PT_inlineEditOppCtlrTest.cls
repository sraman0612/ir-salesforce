@isTest
public class PT_inlineEditOppCtlrTest {
    static testMethod void test1() {
        PTSolutionStatusVisibility__c cs = PTSolutionStatusVisibility__c.getOrgDefaults();
        cs.Solution_Center_User__c = [SELECT Id From User LIMIT 1].Id;
        upsert cs PTSolutionStatusVisibility__c.Id;
        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' and DeveloperName = 'PT_Powertools'].Id;
        Account a = new Account(
                RecordTypeId = ptAcctRTID,
                Name = 'TestAcc',
                Type = 'Customer',
                PT_Status__c = 'New',
                PT_IR_Territory__c = 'North',
                PT_IR_Region__c = 'EMEA'
        );
        insert a;
        PT_Parent_Opportunity__c p = TestUtilityClass.createParentOpp(a.Id);
        p.Lead_SourcePL__c = 'Other';
        p.Type__c = '2.Solution';
        insert p;
        List<Id> ids = new List<Id>();
        ids.add(p.Id);
        Test.startTest();
        Opportunity[] monthlyOppLst = [SELECT Id FROM Opportunity WHERE PT_Parent_Opportunity__c = :p.Id];
        PageReference myVfPage = Page.PT_inlineEditOpp;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('selectedOppId', monthlyOppLst[0].Id);
        ApexPages.StandardController stdCtlr = new ApexPages.StandardController(p);
        PT_inlineEditOppCtlr ctlr = new PT_inlineEditOppCtlr(stdCtlr);
        ctlr.monthlyOpps[0].PT_Solution_Center__c = '1.RFQ attached';
        Schema.DescribeFieldResult fieldResult = Opportunity.PT_Loss_Reason__c .getDescribe();
        for (Schema.PicklistEntry ple : fieldResult.getPicklistValues()) {
            ctlr.monthlyOpps[0].PT_Loss_Reason__c = ple.getValue();
            break;
        }
        ctlr.monthlyOpps[1].PT_Solution_Center__c = '3.RFQ incomplete';
        ctlr.monthlyOpps[2].PT_Solution_Center__c = '4.Quote complete';
        ctlr.monthlyOpps[3].PT_Solution_Center__c = '5.Quote incomplete';
        for (Integer index = 0; index < 5; index++) {
            ctlr.monthlyOpps[index].Amount = 20;
        }
        Opportunity [] oppLst = ctlr.monthlyOpps;
        system.assertEquals(monthlyOppLst.size(), oppLst.size());
        ctlr.saveMonthEdits();
        ctlr.openModal();
        ctlr.getDynamiclist();
        ctlr.closeModal();
        for (PT_inlineEditOppCtlr.emlWrapper ew : ctlr.emlLst) {
            ew.comments = 'somecomments';
        }
        ctlr.sendemails();
        Test.stopTest();
    }
}