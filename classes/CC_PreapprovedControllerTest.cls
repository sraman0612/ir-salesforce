//Test class created by @Priyanka Baviskar for Request 7364036.
@isTest
public class CC_PreapprovedControllerTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    
    static testMethod void UnitTest_CoopClaimDetailPageController()
    { 
        // Creation Of Test Data
        Account newAccount = TestUtilityClass.createAccount();
        insert newAccount;
        Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
        insert sampleContact;
        //String userNavigationProfile = newUser.CC_Pavilion_Navigation_Profile__c;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
        insert newPavilionNavigationProfile;
        
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',newAccount.id);
        Insert sampleContract;
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
        insert newUser;

        
        CC_Hole_in_One__c sampleCooprequest = TestUtilityClass.createCoopRequest(sampleContract.Id);
        sampleCooprequest.Type_of_pre_approval_Request__c='Promotional Assistance Funds';
        sampleCooprequest.Primary_Event_Contact__c='test';
        sampleCooprequest.Phone_Number__c='7896541230';
        sampleCooprequest.Email_Address__c='priyanka.baviskar@irco.com';
        sampleCooprequest.Sales_Rep__c='1123654';
        sampleCooprequest.Black_and_Gold_Partner__c= True;
        sampleCooprequest.Featured_Products__c='IR';
        sampleCooprequest.Any_Prior_Funds_Request__c= True;
        
        sampleCooprequest.Prior_Fund_Request__c= '123654';
        sampleCooprequest.Amount_Requesting__c= 12;
        sampleCooprequest.Event_Plan_Including_resources_to_commit__c = 'testing';
        
        sampleCooprequest.Type_of_pre_approval_Request__c='Trade Shows/ Events';
        sampleCooprequest.Type_of_pre_approval_Request__c='Promotional Assistance Funds';
        sampleCooprequest.Primary_Event_Contact__c='test';
        sampleCooprequest.Phone_Number__c='7896541230';
        sampleCooprequest.Event_Name__c='test_event';
        
        sampleCooprequest.Location__c= 'Augusta';
        //sampleCooprequest.Start_Date__c= 3112018;
       // sampleCooprequest.End_Date__c= 315209;
        sampleCooprequest.Target_Audience__c= 'me';
        sampleCooprequest.Estimated_Attendance__c= 15;
        sampleCooprequest.Featured_Products__c= 'IR';
        sampleCooprequest.Competitors__c= '18';
        sampleCooprequest.Digital_Ad_Opportunity__c= True;
        sampleCooprequest.Total_Show_Cost__c= 1000;
        sampleCooprequest.This_Event_has_been_Held_before__c= True;
        sampleCooprequest.participated_in_this_event_before__c= True;
        insert sampleCooprequest;
        system.debug('coop Request is '+sampleCooprequest );
        system.debug('current page is'+ApexPages.currentPage().getParameters().put('Id',sampleCooprequest.Id));
        Account a=TestUtilityClass.createAccount();
        insert a;
        System.assertNotEquals(null,a);
        Contact c=TestUtilityClass.createContact(a.id);
        insert c;
        System.assertNotEquals(null,c);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(c.Id,u.ContactId);
        test.startTest();
        System.runAs(newUser){
        ApexPages.currentPage().getParameters().put('Id',sampleCooprequest.Id);
        CC_PavilionTemplateController controller=new CC_PavilionTemplateController(); 
        cc_preapprovedcontroller reqObj =new cc_preapprovedcontroller(controller);
        test.stoptest();
        }     
    }
   }