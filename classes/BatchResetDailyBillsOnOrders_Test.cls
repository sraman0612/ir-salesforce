@isTest
private class BatchResetDailyBillsOnOrders_Test {

	// Account Name
	public static final String ACCOUNT_NAME = 'Some Name';
	// Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';

  /**
	 *	Tests that the Batch executes properly
	 */
	static testMethod void testBatchExecution() {
		// Insert a basic Account, all Orders will be related to it
		Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    Account acc = new Account(Name = ACCOUNT_NAME, RecordTypeId = rshvacRTID);
		insert acc;
		Integer newOrderTotal = 20;
		RSHVAC_Order__c firstOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = newOrderTotal, Ordered_Date__c = Date.today());
		RSHVAC_Order__c secondOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Billed_Yesterday__c = newOrderTotal, Close_Line_Subtotal__c = 0, Ordered_Date__c = Date.today());
		RSHVAC_Order__c thirdOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = 0, Ordered_Date__c = Date.today());
		insert new List<RSHVAC_Order__c>{firstOrder, secondOrder, thirdOrder};
		Set<Id> orderIds = new Set<Id>{firstOrder.Id, secondOrder.Id, thirdOrder.Id};
		// Verify that Billed today is not 0 for at least one of the orders
		System.assertEquals(true, [SELECT Id FROM RSHVAC_Order__c WHERE Id IN :orderIds AND Billed_Today__c > 0].size() > 0);
    Test.startTest();
      // Instantiate and execute the batch
      BatchResetDailyBillsOnOrders batch = new BatchResetDailyBillsOnOrders();
      Database.executeBatch(batch);
			// Dummy call to add coverage
      BatchResetDailyBillsOnOrders batch2 = new BatchResetDailyBillsOnOrders('SELECT Id FROM RSHVAC_Order__c');
    Test.stopTest();
		// Get the updated Orders
		for (RSHVAC_Order__c order : [SELECT Id, Billed_Yesterday__c, Billed_Today__c FROM RSHVAC_Order__c WHERE Id IN :orderIds]){
			// Billed Today should be 0 for all orders
			System.assertEquals(0, order.Billed_Today__c);
			if (order.Id == firstOrder.Id){
				// First Order should have newOrderTotal set as Billed yesterday
				System.assertEquals(newOrderTotal, order.Billed_Yesterday__c);
			} else {
				// For the other orders billed yesterday should be 0
				System.assertEquals(0, order.Billed_Yesterday__c);
			}
		}
  }
}