//
// (c) 2015, Appirio Inc.
//
// Handler Class for ServiceAgreement_Trigger
//
// June28,2015    Surabhi Sharma    T-412426
// July 21,2015   Surabhi Sharma    T-421031(Modified)
// July 30,2015   Surabhi Sharma    T-423339(Modified)
public class ServiceAgreementTriggerHandler{

public static void afterUpdate(Map<ID, Service_Agreement__c> oldMap, List<Service_Agreement__c> newList){    //method to define Updates in Service_Agreement__c records
            createNewOppRecord(oldMap, newList);
    }
    
    public static void createNewOppRecord(Map<ID, Service_Agreement__c> oldMap, List<Service_Agreement__c> newList){
        List <Opportunity> oppToInsert = new List <Opportunity>();
    Set<Id> setOfServiceAgree = new Set<Id>();    //Set of Ids for Service_Agreement__c
       
    for(Service_Agreement__c sa : newList){    //Assign Object in a list

        System.debug('==newList==' +newList);
        System.debug('==sa.X90DaysPriorEndDate__c==' +sa.X90DaysPriorEndDate__c);

        //checks the condition that End date is 90 days less than defined end date through Workflow.
        //  New: also checks that the X90DaysPriorEndDate__c flag was FALSE prior to this update
        if(sa.X90DaysPriorEndDate__c == true && oldMap.get(sa.Id).X90DaysPriorEndDate__c == false){
            setOfServiceAgree.add(sa.Id);    //add the record Id to set of Ids 
        }
    }
    
    //List of Service_Agreement__c whose X90DaysPriorEndDate__c  is true
    List<Service_Agreement__c> serAgreeList = new List<Service_Agreement__c>();
    
    //fetch record details through query where Id is record Id whose X90DaysPriorEndDate__c is true
    //fecth Account details associated with Service Agreement through Account__r parent Child Relationship 
        serAgreeList = [SELECT Id, Name, OwnerId, Account__r.Name, Account__r.ownerId, Account__r.ShippingStreet, Account__r.ShippingCity
                        FROM Service_Agreement__c WHERE Id IN : setOfServiceAgree];
        
        Id NaAirOppRecordTypeID = [SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        //Service  Agreement records in the List serAgreeList 
        for(Service_Agreement__c serAgreement : serAgreeList){
  
            //Assign field Values to opportunity record which need to be created.          
            Opportunity opp = new Opportunity();    //Create Instance of Opportunity
            
            //opp.OwnerId = serAgreement.OwnerId; - Owner Id for Svc Agr will not work, this is always an Integration user (SUrbanski - 102815)
            opp.OwnerId = serAgreement.Account__r.ownerId;
            opp.Name = serAgreement.Name;
            opp.AccountId = serAgreement.Account__c;
            System.debug('*accoutName=---------------------' + opp.AccountId);
            
            opp.CloseDate = System.today() + 90;
            opp.StageName = 'Stage 1. Qualify';
            opp.RecordTypeId=NaAirOppRecordTypeID;
            opp.Type = 'Renewal Contract';
            opp.New_Opp_through_S_Agreement__c = true;  //set true to notify User for the new Opportunity [workflow : New Opp through SA]
            System.debug('accountName ************************************' + opp.New_Opp_through_S_Agreement__c);
            
            
            oppToInsert.add(opp);
         
        }
   
        try {
            //insert oppToInsert list;    //insert the List of Records
            insert oppToInsert;
        }
        catch (system.Dmlexception e) {    //handles if error occurs
        system.debug (e);
        } 
    }
    
}