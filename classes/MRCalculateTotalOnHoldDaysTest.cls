@isTest
public class MRCalculateTotalOnHoldDaysTest {
     @TestSetup
    static void makeData(){
            
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name = 'MR Application Engineering'];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user = new User (FirstName='Test', LastName='Name ', Email='test@test.com', 
                             Alias='test', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='User_EmailPost@test.com', Business_Hours__c = bh.Id,Department__c = 'Milton Roy',Brand__c = 'Milton Roy',Default_Product_Category__c = 'Milton Roy',Assigned_From_Email_Address__c = 'om_general@irco.com');
        
        insert user;
        system.RunAs(user){
            
             Case caseRec = new Case(Subject='Test Case on hold calculate', Status='New', recordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management_Application').getRecordTypeId(),Assigned_From_Address_Picklist__c = 'om_general@irco.com');
            insert caseRec;
            
        }
       
    }
    @isTest
    public static void testcalculateOnHoldDays(){
        
        Case oCase = [SELECT Id,Status,Last_On_Hold_Date_Time__c,Total_On_Hold_Time_Days__c,Total_On_Hold_Time_Hours__c  FROM Case LIMIT 1];
        User oUser = [SELECT id,Name,isActive FROM User WHERE Name = 'Test Name' AND isActive = true LIMIT 1];
        System.debug('User Name '+ oUser.Name);
        System.debug('user Active : '+ oUser.IsActive);
        system.RunAs(oUser){
        Test.startTest();
        
        System.debug('status before update : ' + oCase.status);
        oCase.status = 'On Hold';
        update oCase;
        oCase.status='Open';
        update oCase;
        System.debug('Last on hold Date time : '+ oCase.Last_On_Hold_Date_Time__c + ' Total on hold days '+ oCase.Total_On_Hold_Time_Days__c + ' total on hold hours '+ oCase.Total_On_Hold_Time_Hours__c);
        Test.stopTest();
         }
       
    }


}