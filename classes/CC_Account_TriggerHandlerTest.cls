/*****************************************************
 * Description    : Test class for CC_Account_TriggerHandler
 * created Date   : 02/06/2017
 * ********************************************************/
@isTest
public class CC_Account_TriggerHandlerTest {
    
    @TestSetup
    private static void createData(){
        TestDataUtility.createStateCountries();
        TestDataUtility testData = new TestDataUtility();
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        Account acct1 = TestDataFactory.createAccount('Test Account1', 'Club Car: New Customer');
        acct1.CC_Price_List__c='PCOPI';
        acct1.BillingCountry = 'USA';
        acct1.BillingCity = 'Augusta';
        acct1.BillingState = 'GA';
        acct1.BillingStreet = '2044 Forward Augusta Dr';
        acct1.BillingPostalCode = '566';
        acct1.CC_Shipping_Billing_Address_Same__c = true;
        acct1.CC_Number_of_Gas_Cars__c = null;
        acct1.CC_Number_of_Electric_Cars__c = null;
        acct1.CC_Number_Of_Cars__c = null;
        acct1.CC_Sales_Rep__c = salesRepacc.Id;
        
        Account acct2 = acct1.clone(true, false);
        acct2.Name = 'Test Account2';
        
        insert new Account[]{acct1, acct2};
        
        
                
        Product2 prod1 = TestUtilityClass.createProduct2();
        prod1.Name = 'Gas Car1';
        prod1.P2__c = 'gas utility';
        prod1.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        
        Product2 prod2 = prod1.clone(true, false);
		prod2.Name = 'Gas Car2';
        prod2.P2__c = 'GAS_PRECEDENT';   
        
        Product2 prod3 = prod1.clone(true, false);
		prod3.Name = 'Electric Car1';
        prod3.P2__c = 'PRECEDENT_ELECTRIC'; 
        
        Product2 prod4 = prod1.clone(true, false);
		prod4.Name = 'Electric Car2';
        prod4.P2__c = 'PRECEDENT electric'; 
        
        Product2 prod5 = prod1.clone(true, false);
		prod5.Name = 'Utility Vehicle1';
        prod5.P2__c = 'UTILITY_ELECT';
        
        Product2 prod6 = prod1.clone(true, false);
		prod6.Name = 'Utility Vehicle2';
        prod6.P2__c = null;                                                        
        
        insert new Product2[]{prod1, prod2, prod3, prod4, prod5, prod6};         	
    }
    
    @isTest
    private static void testRollupAssetGroupsSettingsOff(){
    	
    	CC_Account_Settings__c settings = CC_Account_Settings__c.getOrgDefaults();
    	settings.Enable_Asset_Group_Rollup__c = false;
    	upsert settings;
    	
    	Account acct1 = [Select Id, Name, CC_Number_of_Gas_Cars__c, CC_Number_of_Electric_Cars__c, CC_Number_Of_Cars__c From Account Where Name = 'Test Account1'];
    	
        system.assertEquals(null, acct1.CC_Number_of_Gas_Cars__c);
        system.assertEquals(null, acct1.CC_Number_of_Electric_Cars__c);
        system.assertEquals(null, acct1.CC_Number_Of_Cars__c);
        
        Product2 gasCar1 = [Select Id From Product2 Where Name = 'Gas Car1'];   	
        Product2 electricCar1 = [Select Id From Product2 Where Name = 'Electric Car1'];
    
    	Test.startTest();
    	
        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(acct1.Id, gasCar1.Id);
    	CC_Asset_Group__c assetGroup2 = TestDataUtility.createAssetGroup(acct1.Id, electricCar1.Id);
    	
    	insert new CC_Asset_Group__c[]{assetGroup1, assetGroup2}; 
    	
    	Test.stopTest(); 
    	
    	acct1 = [Select Id, Name, CC_Number_of_Gas_Cars__c, CC_Number_of_Electric_Cars__c, CC_Number_Of_Cars__c From Account Where Name = 'Test Account1'];
    	
    	// Verify the fields were not updated with the settings turned off
        system.assertEquals(null, acct1.CC_Number_of_Gas_Cars__c);
        system.assertEquals(null, acct1.CC_Number_of_Electric_Cars__c);
        system.assertEquals(null, acct1.CC_Number_Of_Cars__c);    	
    }
    
    @isTest
    private static void testRollupAssetGroupsSettingsOn(){
    	
    	CC_Account_Settings__c settings = CC_Account_Settings__c.getOrgDefaults();
    	settings.Enable_Asset_Group_Rollup__c = true;
    	upsert settings;
    	
    	Account[] accounts = [Select Id, Name, CC_Number_of_Gas_Cars__c, CC_Number_of_Electric_Cars__c, CC_Number_Of_Cars__c From Account Where Name Like 'Test Account%' Order By Name ASC];
    	system.assertEquals(2, accounts.size());
    	
    	for (Account a : accounts){
	        system.assertEquals(null, a.CC_Number_of_Gas_Cars__c);
	        system.assertEquals(null, a.CC_Number_of_Electric_Cars__c);
	        system.assertEquals(null, a.CC_Number_Of_Cars__c);
    	}
        
        Product2[] gasCars = [Select Id, P2__c From Product2 Where Name Like 'Gas Car%' Order By Name ASC];   	
        Product2[] electricCars = [Select Id, P2__c From Product2 Where Name Like 'Electric Car%' Order By Name ASC];
        Product2[] otherCars = [Select Id, P2__c From Product2 Where Name Like 'Utility Vehicle%' Order By Name ASC];
        
        system.assertEquals(2, gasCars.size());
        system.assertEquals(2, electricCars.size());
        system.assertEquals(2, otherCars.size());
    
    	Test.startTest();
    	
    	// Account 1
        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(accounts[0].Id, gasCars[0].Id);
        assetGroup1.Quantity__c = 5;
        
    	CC_Asset_Group__c assetGroup2 = TestDataUtility.createAssetGroup(accounts[0].Id, gasCars[1].Id);
    	assetGroup2.Quantity__c = 5;
    	
    	CC_Asset_Group__c assetGroup3 = TestDataUtility.createAssetGroup(accounts[0].Id, electricCars[0].Id);
    	assetGroup3.Quantity__c = 5;
    	
    	CC_Asset_Group__c assetGroup4 = TestDataUtility.createAssetGroup(accounts[0].Id, electricCars[1].Id);
    	assetGroup4.Quantity__c = 5;
    	
    	CC_Asset_Group__c assetGroup5 = TestDataUtility.createAssetGroup(accounts[0].Id, otherCars[0].Id);
    	assetGroup5.Quantity__c = 5;
    	
    	CC_Asset_Group__c assetGroup6 = TestDataUtility.createAssetGroup(accounts[0].Id, otherCars[1].Id);
    	assetGroup6.Quantity__c = 5;
    	
    	// Account 2
    	CC_Asset_Group__c assetGroup7 = TestDataUtility.createAssetGroup(accounts[1].Id, gasCars[0].Id);
    	assetGroup7.Quantity__c = 5;
    	
    	system.debug('--inserting asset groups');
    	insert new CC_Asset_Group__c[]{assetGroup1, assetGroup2, assetGroup3, assetGroup4, assetGroup5, assetGroup6, assetGroup7}; 
    	
    	Test.stopTest(); 
    	
    	system.debug('asset Groups: ' + [Select Account__c, Fleet_Status__c, Car_Type__c From CC_Asset_Group__c]);
    	
    	accounts = [Select Id, Name, CC_Number_of_Gas_Cars__c, CC_Number_of_Electric_Cars__c, CC_Number_Of_Cars__c, RecordType.DeveloperName From Account Where Name Like 'Test Account%' Order By Name ASC];
    	
    	// Verify the fields were updated with the settings turned on
      system.assertEquals(2*5, accounts[0].CC_Number_of_Gas_Cars__c);
      system.assertEquals(2*5, accounts[0].CC_Number_of_Electric_Cars__c);
      system.assertEquals(6*5, accounts[0].CC_Number_Of_Cars__c); 
      system.assertEquals(1*5, accounts[1].CC_Number_of_Gas_Cars__c);
      system.assertEquals(0, accounts[1].CC_Number_of_Electric_Cars__c);
      system.assertEquals(1*5, accounts[1].CC_Number_Of_Cars__c);          
    }        
    
    static testmethod void testbeforeInsertUpdate(){
        
       /*  Club_Car_Payment_Terms__c terms = new Club_Car_Payment_Terms__c(); 
        terms.Name                      = '10';
        terms.Display_Value__c          = 'Testing';
        insert terms;*/
        
        //creating test Data
        TestDataUtility testData = new TestDataUtility();
        
         
        
        //creating partner community user
        user u = testData.createPartnerUser();
        
          
        
        List<account> acclist = new List<account>();
        system.runAs(u){
           
            
            acclist.add(testData.createAccountName('test1' ,'Club Car: New Customer'));
            acclist.add(testData.createAccountName('test2','Club Car: New Customer'));
            insert acclist;
            
            //To cover after delete scenario
            merge acclist[0] acclist[1];
        }
        
        //creating user with Clubcar Integration Name
         Id p = [select id from profile where name='System Administrator'].id;
        User SampleUser = new User(alias = 'Sample', email='SampleUser@gmail.com',
                                   emailencodingkey='UTF-8',FirstName ='Clubcar', lastname='Integration', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                   timezonesidkey='America/Los_Angeles', username='SampleUser123213@gmail.com',CC_View_Parts_Bulletins__c=true,
                                   CC_Pavilion_Navigation_Profile__c='test pavilion navigation profile',PS_Business_Unit_Description__c = 'CLUB CAR',CC_Department__c='Marketing');
        //insert SampleUser;
        
        //creating custom setting club car payment Terms
              
        system.runAs(SampleUser){
            account acc = testData.createAccountName('test3','Club Car: New Customer');
            acc.CC_Car_Terms__c = '10';
            acc.CC_Part_Terms__c = '10';
            insert acc;
            
        }
        
        
        
    }
    
     static testmethod void clubCarsalesRepupdates_test(){ 
       TestDataUtility testData = new TestDataUtility(); 
          //creating sales rep here
          CC_Sales_Rep__c salesRepacc = testData.createSalesRep('9999');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        
       User u = testData.createIntegrationUser();
       insert u;
       user pu = testData.createPartnerUser();
       system.runAs(u){
        insert salesRepacc; 
        Account acc = testData.createAccountName('testAccount','Club Car: New Customer');
        acc.CC_MAPICS_Sales_Rep__c = salesRepacc.id;   
        insert acc;
        AccountTeamMember atm = CC_Account_TriggerHandler.createATM(acc.id,u.id);
         
       }
       system.runAs(pu){
        Account acc = testData.createAccountName('testAccount123','Club Car: New Customer');
            acc.CC_MAPICS_Sales_Rep__c = salesRepacc.id;   
        insert acc;
        AccountTeamMember atm = CC_Account_TriggerHandler.createATM(acc.id,u.id);
       }
     }
}