global with sharing class CC_PavilionCustomerVIEWVdoCont {

     public CC_PavilionCustomerVIEWVdoCont(CC_PavilionTemplateController controller) {}
     global static List < ContentVersion > getContentList() {
       Id  usrId  = userinfo.getUserId();
       Id accId = [SELECT AccountId FROM User WHERE id =:usrId].AccountId;
       //code contributed by @Priyanka Baviskar for issue no 7627734.
       if(accId ==null)
       {
       //Account acc = [SELECT Type,CC_Global_Region__c FROM Account WHERE id='001j000000p5e4b'];
       List < ContentVersion > contentList = new List < ContentVersion > ();
       contentList = [SELECT id, Title, Content_Title__c, Content_Sub_title__c, Content_Body__c, ContentUrl, Description, FileType, CC_Date__c,
       RecordType.Name FROM ContentVersion WHERE RecordType.Name = 'CustomerVIEW' AND IsLatest = TRUE AND IsDeleted  = FALSE];
       
       return contentList;
       }
       else
       {
       Account acc = [SELECT Type,CC_Global_Region__c FROM Account WHERE id=:accId];
       List < ContentVersion > contentList = new List < ContentVersion > ();
       contentList = [SELECT id, Title, Content_Title__c, Content_Sub_title__c, Content_Body__c, ContentUrl, Description, FileType, CC_Date__c,
       RecordType.Name FROM ContentVersion WHERE RecordType.Name = 'CustomerVIEW' AND CC_Type__c includes (:acc.Type)
                                AND  CC_Region__c INCLUDES(:acc.CC_Global_Region__c) AND IsLatest = TRUE AND IsDeleted  = FALSE];
      return contentList;
      }
      //return contentList;
    }
}