@isTest
public with sharing class CC_ContractTriggerHandlerTest {

    @TestSetup
    private static void createData(){

    	User svcLeader = TestUtilityClass.createUser('CC_Pavilion Admin', null);
    	svcLeader.Alias = 'svcLeadr';       
        
        TestDataUtility dataUtility = new TestDataUtility();

    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'adminUse';        
        
        User financeUser = dataUtility.createIntegrationUser();
        financeUser.LastName = 'FinanceTestUser';
        
        User financeDefaultUser = dataUtility.createIntegrationUser();
        financeDefaultUser.LastName = 'FinanceTestDefaultUser';
        financeDefaultUser.Username = 'financeDefaultUser@abc123.com.sandbox';
        
        insert new User[]{svcLeader, adminUser, financeUser, financeDefaultUser};

        system.runAs(adminUser){

            // Assign a role to the service leader
            UserRole accTeamRole = [Select Id From UserRole Where DeveloperName = 'CC_Accounting_Team'];
            svcLeader.UserRoleId = accTeamRole.Id;
            update svcLeader;
        }
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;

        CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
        contractSettings.CC_Contract_Trigger_Handler_Enabled__c = true;
        contractSettings.CC_Default_Finance_Approver_ID__c = financeDefaultUser.Id;
        contractSettings.Allowed_Svc_Contract_Deletion_Statuses__c = 'Draft';
        upsert contractSettings;	

        Account a1 = dataUtility.createAccount('Club Car');
        a1.Name = 'Account1';
        a1.OwnerId = svcLeader.Id;
        Account a2 = a1.clone();
        a2.Name = 'Account2';
        a2.OwnerId = svcLeader.Id;
        insert new Account[]{a1, a2};
            
		// Add a finance rep to account 1 but not account 2
		insert TestDataUtility.createMyTeam(a1.Id, financeUser.Id, 'Customer Financing Representative');  
    }

    @isTest
    private static void testValidateDeletionsNoPermSet(){

        User adminUser = [Select Id From User Where Alias = 'adminUse'];
        Account a = [Select Id From Account Where Name = 'Account1'];

        Test.startTest();

            System.runAs(adminUser){

                Contract c1 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c1.CC_Contract_Status__c = 'Draft';

                Contract c2 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c2.CC_Contract_Status__c = 'Pending';     
                
                insert new Contract[]{c1, c2};

                // Try to delete the service contracts
                delete c1;
                delete c2;
            }
        
        Test.stopTest();
    }

    @isTest
    private static void testValidateDeletionsNoCustomSettingStatus(){

        // Remove allowed statuses
        CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
        contractSettings.Allowed_Svc_Contract_Deletion_Statuses__c = null;
        update contractSettings;        

        User svcLeader = [Select Id From User Where Alias = 'svcLeadr'];
        User adminUser = [Select Id From User Where Alias = 'adminUse'];
        Account a = [Select Id From Account Where Name = 'Account1'];

        // Assign the permission set to the user
        system.runAs(adminUser){
            PermissionSet svcEditPermset = [Select Id From PermissionSet Where Name = 'CC_Service_Contract_Status_Edit'];
            insert new PermissionSetAssignment(AssigneeId = svcLeader.Id, PermissionSetId = svcEditPermset.Id);
        }
        
        Test.startTest();

            System.runAs(svcLeader){

                Contract c1 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c1.CC_Contract_Status__c = 'Draft';

                Contract c2 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c2.CC_Contract_Status__c = 'Pending';     
                
                insert new Contract[]{c1, c2};

                // Try to delete the service contracts
                Boolean c1Success = true;
                String expectedErrorMsg = 'Service contracts are not allowed to be deleted.';

                try{
                    delete c1;
                }
                catch(Exception e){
                    c1Success = false;
                    system.assert(e.getMessage().contains(expectedErrorMsg));
                }

                system.assertEquals(false, c1Success);  

                Boolean c2Success = true;

                try{
                    delete c2;
                }
                catch(Exception e){
                    c2Success = false;
                    system.assert(e.getMessage().contains(expectedErrorMsg));
                }

                system.assertEquals(false, c2Success);                
            }
        
        Test.stopTest();
    }       

    @isTest
    private static void testValidateDeletionsPermSet(){

        User svcLeader = [Select Id From User Where Alias = 'svcLeadr'];
        Account a = [Select Id From Account Where Name = 'Account1'];

        // Assign the permission set to the user
        PermissionSet svcEditPermset = [Select Id From PermissionSet Where Name = 'CC_Service_Contract_Status_Edit'];
        insert new PermissionSetAssignment(AssigneeId = svcLeader.Id, PermissionSetId = svcEditPermset.Id);

        Test.startTest();

            System.runAs(svcLeader){

                Contract c1 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c1.CC_Contract_Status__c = 'Draft';

                Contract c2 = TestDataUtility.createCCServiceContract(a.Id, 'Monthly');
                c2.CC_Contract_Status__c = 'Pending';     
                
                insert new Contract[]{c1, c2};

                // Try to delete the service contracts
                delete c1;

                Boolean c2Success = true;
                String expectedErrorMsg = 'Only service contracts with the following statuses are allowed to be deleted: ';

                try{
                    delete c2;
                }
                catch(Exception e){
                    c2Success = false;
                    system.assert(e.getMessage().contains(expectedErrorMsg));
                }

                system.assertEquals(false, c2Success);                
            }
        
        Test.stopTest();
    }    
    
    @isTest
    private static void testTriggerSettings(){
        
        CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
        contractSettings.CC_Contract_Trigger_Handler_Enabled__c = true;
        update contractSettings;        
        
        system.assertEquals(true, CC_ContractTriggerHandler.getIsTriggerEnabled());
        
        contractSettings.CC_Contract_Trigger_Handler_Enabled__c = false;
        update contractSettings;        
        
        system.assertEquals(false, CC_ContractTriggerHandler.getIsTriggerEnabled());        
    }  
    
    @isTest
    private static void testLookupCustomerFinanceRep(){
        
        User financeUser = [Select Id From User Where LastName = 'FinanceTestUser'];
        User financeDefaultUser = [Select Id From User Where LastName = 'FinanceTestDefaultUser'];
        Account a1 = [Select Id From Account Where Name = 'Account1'];
        Account a2 = [Select Id From Account Where Name = 'Account2'];
        
        system.assertEquals(1, [Select Count() From CC_My_Team__c Where Account__c = :a1.Id and My_Team_Role__c = 'Customer Financing Representative']);
        system.assertEquals(0, [Select Count() From CC_My_Team__c Where Account__c = :a2.Id and My_Team_Role__c = 'Customer Financing Representative']);
        
        Contract c1 = TestDataUtility.createCCServiceContract(a1.Id, 'Monthly');
        Contract c2 = TestDataUtility.createCCServiceContract(a2.Id, 'Monthly');
        
        Test.startTest();
        
        insert new Contract[]{c1, c2};
            
        Test.stopTest();
        
        // Verify account 1 was assigned a finance rep and account 2 was assigned to the default finance user
        c1 = [Select Id, CC_Customer_Finance_Rep__c From Contract Where Id = :c1.Id];
        c2 = [Select Id, CC_Customer_Finance_Rep__c From Contract Where Id = :c2.Id];
        
        system.assertEquals(financeUser.Id, c1.CC_Customer_Finance_Rep__c);
        system.assertEquals(financeDefaultUser.Id, c2.CC_Customer_Finance_Rep__c);        
    }
}