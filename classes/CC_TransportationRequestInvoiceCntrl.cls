public with sharing class CC_TransportationRequestInvoiceCntrl {

    public class CC_TransportationRequestInvoiceCntrlException extends Exception{}

    @AuraEnabled
    public static LightningResponseBase createOrderInvoice(Id caseId){

        LightningResponseBase response = new LightningResponseBase();
        Savepoint sp = Database.setSavepoint();

        try{

            Case trRequest = [Select Id, CaseNumber, AccountId, Status, IsClosed, CC_Quoted_Price__c From Case Where Id = :caseId];

            // Validations
            if (trRequest.Status == 'Invoiced' || trRequest.IsClosed){
                throw new CC_TransportationRequestInvoiceCntrlException('This case has already been invoiced.');
            }

            Boolean pass = true;
            List<String> missingFields = new List<String>();

            if (trRequest.AccountId == null){
                missingFields.add('Account');   
                pass = false;   
            }

            if (trRequest.CC_Quoted_Price__c == null){
                missingFields.add('Quoted Price'); 
                pass = false;
            }

            if (!pass){
                throw new CC_TransportationRequestInvoiceCntrlException('The following empty fields are required to invoice partners: ' + String.join(missingFields, ', '));
            }

            Account a = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry 
                         FROM Account 
                         WHERE Id = :trRequest.AccountId LIMIT 1];

            Product2 p = [SELECT Id, Description FROM Product2 WHERE ProductCode = 'BACKHAUL UC' LIMIT 1];

            // insert order and order line
            CC_Order__c o = new CC_Order__c();
            o.isSystemCreated__c = TRUE;
            o.CC_Account__c = a.Id;
            o.CC_Status__c = 'Submitted';
            o.PO__c = 'BACKHAUL ORDER-*';
            o.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CC_Order__c' AND DeveloperName = 'CC_Parts_Ordering'].Id;
            o.CC_Address_line_1__c = a.ShippingStreet;
            o.CC_Addressee_name__c = a.Name;
            o.CC_City__c = a.ShippingCity;
            o.CC_States__c = a.ShippingState;
            o.CC_Postal_code__c = a.ShippingPostalCode;
            o.CC_Country__c = a.ShippingCountry;
            o.CC_Request_Date__c = system.today();
            o.CC_Warehouse__c = '1';
            insert o;

            CC_Order_Item__c oli = new CC_Order_Item__c();
            oli.Order__c = o.Id;
            oli.CC_Quantity__c = 1;
            oli.CC_Product_Code__c = 'BACKHAUL UC';            
            oli.Product__c = p.Id; 
            oli.CC_Description__c = p.Description;
            oli.CC_UnitPrice__c = trRequest.CC_Quoted_Price__c;
            oli.CC_TotalPrice__c = oli.CC_UnitPrice__c;
            oli.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CC_Order_Item__c' AND DeveloperName = 'CC_Parts_Ordering'].Id;
            insert oli; 

            // Set the case status to invoiced and assign the order to the case
            trRequest.Status = 'Invoiced';
            trRequest.CC_Order__c = o.Id;
            update trRequest;
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        if (!response.success){
            Database.rollback(sp);
        }

        return response;
    }  
}