public class C_Action_Item_Extension {

    public C_Action_Item_Extension(ApexPages.StandardController controller) {

    }

    @TestVisible
    public String fileID {get; set;}
    public List<addressWrapper> ccWrappers {get; set;}
    public String freeFormCC {get; set;}
    public String subTabId {get; private set;}
    
    public PageReference cancel() {      
        PageReference parentPR;
        if(isFollowUpFlag){
            parentPR = new PageReference('/' + originatingId);  
            parentPR.setRedirect(true);
        }
        else{
            parentPR = new PageReference('/' + parentId);  
            parentPR.setRedirect(true);
        }
        return parentPR;
    }

    private Set<Id> startingFiles;
    public Map<Id,FileWrapper> wrapMapF;
    public Boolean hasParentFiles {get{
        List<EmailMessage> theEmails = [SELECT Id FROM EmailMessage WHERE HasAttachment = TRUE AND ParentId = :parentId];
        Set<String> linkedEntityIds = new Set<String>();
        for (EmailMessage em : theEmails) {
            linkedEntityIds.add(em.Id);
        }
        //Add Id for the Case to the list as well
        linkedEntityIds.add(parentId);
        List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
        Set<String> currFileIds = new Set<String>();
        for (ContentDocumentLink cdl : CDLs) {
            currFileIds.add(cdl.ContentDocumentId);
        }
        List<ContentDocument> CDs = [SELECT Id FROM ContentDocument WHERE Id IN :currFileIds];
        return CDs.size() > 0;}
        set;}
    public Case theCase {get; set;}
    public String editorContent {get; set;}
    public String AIeditorContent {get; set;}
    public String FUeditorContent {get; set;}
    public Boolean isMRRecordType {get; set;} //Milton Roy change as part of SMR 1024, SMR 1027

    
    @TestVisible
    public List<FileWrapper> filesWrapper {
        get{
            List<EmailMessage> theEmails = [SELECT Id FROM EmailMessage WHERE HasAttachment = TRUE AND ParentId = :parentId];
            Set<String> linkedEntityIds = new Set<String>();
            for (EmailMessage em : theEmails) {
                linkedEntityIds.add(em.Id);
            }
            //Add Id for the Case to the list as well
            linkedEntityIds.add(parentId);
            List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
            Set<String> currFileIds = new Set<String>();
            for (ContentDocumentLink cdl : CDLs) {
                currFileIds.add(cdl.ContentDocumentId);
            }
            List<ContentDocument> files = [SELECT Id, Title, Description, FileExtension, CreatedDate, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :currFileIds ORDER BY CreatedDate ASC];
            List<FileWrapper> result = new List<FileWrapper>();
            if(files.size() > 0){
                for(ContentDocument cd : files){
                    if(!wrapMapF.containsKey(cd.Id)){
                        FileWrapper fw = new FileWrapper(cd, !startingFiles.contains(cd.Id), this);
                        wrapMapF.put(cd.Id,fw);
                        //System.debug('wrapMapFNew: ' + wrapMapF.get(cd.Id).theFile.Title + ' - ' + wrapMapF.get(cd.Id).include);
                        result.add(fw);
                    }
                    else{
                        
                        result.add(wrapMapF.get(cd.Id));
                        //System.debug('wrapMapF: ' + wrapMapF.get(cd.Id).theFile.Title + ' - ' + wrapMapF.get(cd.Id).include);
                    }
                }
                this.hasParentFiles = true;
            }
            else{
                this.hasParentFiles = false;
            }
            return result;
        }
        private set;}
    
    private Case parentCase;
    private Case orgCase;
    public String parentId {get; private set;}
    private String recordTypeId;
    private String incDesc;
    private String subject;
    private String originatingId;
    private String isFollowUp;
    private Boolean isFollowUpFlag;
    private String emailAIId;
    private EmailMessage theEm;
    private Boolean isMc;
    private Boolean parentIsAI = false;
    

    public C_Action_Item_Extension() {
        if(String.isEmpty(AIeditorContent)){
            AIeditorContent = '';
        }
        this.theCase = new Case();
        parentId = ApexPages.currentPage().getParameters().get('ParentId');
        this.parentCase = [SELECT Id, ParentId, Customer_Care_Division__c, Brand__c,GDI_Department__c,team__c,Business__c,
                           Product_Category__c, Assigned_From_Address__c, Assigned_From_Address_Picklist__c, BusinessHoursId,
                           Contact.Name, Org_Wide_Email_Address_ID__c, Subject, Description, RecordTypeId, Email_Branding__c,RecordType.DeveloperName,
                           (SELECT Id, ToAddress, FromAddress, FromName, MessageDate, Subject, CcAddress FROM EmailMessages ORDER BY CreatedDate ASC LIMIT 1)
                           FROM Case WHERE Id =: parentId];
        
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordTypeId');
        //incDesc = ApexPages.currentPage().getParameters().get('OrgBody');
        //subject = ApexPages.currentPage().getParameters().get('Subject');
        isFollowUp = ApexPages.currentPage().getParameters().get('isFollowUp');
        originatingId = ApexPages.currentPage().getParameters().get('OriginatingId');
        emailAIId = ApexPages.currentPage().getParameters().get('EmailId');
        subTabId = ApexPages.currentPage().getParameters().get('tabId');
        System.debug('emailAIId '+emailAIId);
        //Milton Roy Change
        Id aiId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Action Item').getRecordTypeId();
        Id mrAppRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Milton Roy Case Management - Application').getRecordTypeId();
        Id mrCustRecId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Milton Roy Case Management - Customer').getRecordTypeId();
        Id mrYZId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Milton Roy Case Management - YZ').getRecordTypeId();
        this.isMRRecordType = false;
        
        if(this.recordTypeId == aiId){
            this.parentIsAI = true;
        }
        //Milton Roy changes
         if(this.parentCase.RecordTypeId == mrAppRecId || this.parentCase.RecordTypeId == mrCustRecId || this.parentCase.RecordTypeId == mrYZId){
            this.isMRRecordType = true;
        }
        
        //check if parent case is Managed Care
      //  Id mcId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Managed Care').getRecordTypeId();
       // Id mclId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Managed Care - Locked').getRecordTypeId();
        //System.debug(mcId);
        //System.debug(mclId);
        //System.debug(this.parentCase.RecordTypeId);
               
        if(String.isEmpty(emailAIId)){
			  System.debug('is empty');         
            if(isFollowUp == 'TRUE'){
                isFollowUpFlag = true;
                this.orgCase = [SELECT Id, Status, AI_Email_Subject__c, AI_Original_Email_Body__c FROM Case WHERE Id = :originatingId];
                subject = this.orgCase.AI_Email_Subject__c;
                incDesc = this.orgCase.AI_Original_Email_Body__c;
            }
            else{
                
                isFollowUpFlag = false;
                subject = this.parentCase.Subject;
                incDesc = this.parentCase.Description;
            }
        }
        else{
             System.debug(' emailAIId --> '+emailAIId);
            isFollowUpFlag = false;
            theEM = [SELECT Id, Subject, HTMLBody, TextBody FROM EmailMessage WHERE Id = :emailAIId];
            subject = theEM.Subject;
            System.debug(' the EM --> '+theEM);
            if(String.isNotEmpty(theEM.TextBody)){
                incDesc = theEM.TextBody;
            }
            else{
                incDesc = theEM.HTMLBody.escapeHTML4().replace('\n','<br>');   
            }
        }
        
      /*  if(this.parentCase.RecordTypeId == mcId || this.parentCase.RecordTypeId == mclId){
            isMc = true;
            subject = '';
            theCase.AI_Add_Body_of_Original_Email__c = false;
            //System.debug('Setting false');
        }  */
        
            isMc = false;
            theCase.AI_Add_Body_of_Original_Email__c = true;
            //System.debug('Setting true');
        
        
        theCase.ParentId = parentId;
        theCase.RecordTypeId = recordTypeId;
        theCase.AI_Original_Email_Body__c = incDesc;
        //theCase.AI_Email_Subject__c = subject;
        String[] subjectwithreferenceremoved = subject.split('\\[ ref:');
        theCase.AI_Email_Subject__c = subjectwithreferenceremoved[0];
        theCase.Org_Wide_Email_Address_ID__c = this.parentCase.Org_Wide_Email_Address_ID__c;
        theCase.Assigned_From_Address_Picklist__c = this.parentCase.Assigned_From_Address_Picklist__c;
        theCase.Brand__c = this.parentCase.Brand__c;
        theCase.GDI_Department__c = this.parentCase.GDI_Department__c;
        theCase.Product_Category__c = this.parentCase.Product_Category__c;
        theCase.Email_Branding__c = this.parentCase.Email_Branding__c;
        theCase.Parent_Case_Record_Type__c = this.parentCase.RecordType.DeveloperName;
        theCase.BusinessHoursId = this.parentCase.BusinessHoursId;
        theCase.Team__c = this.parentcase.Team__c;
        theCase.Business__c = this.parentcase.Business__c;
        System.debug('Original Body '+  theCase.AI_Original_Email_Body__c );
        //System.debug(UserInfo.getName());
        //System.debug(UserInfo.getUserId());
        //theCase.OwnerId = UserInfo.getUserId();

        filesWrapper = new List<FileWrapper>();
       // System.debug('files wrapper inside constructor : ' + filesWrapper);
        
        ccWrappers = new List<addressWrapper>();
        for(Integer i = 0; i < 1; ++i){
            ccWrappers.add(new addressWrapper());
        }

        //System.debug(theCase.AI_Add_Body_of_Original_Email__c);

        wrapMapF = new Map<Id,FileWrapper>();
        List<EmailMessage> theEmails = [SELECT Id FROM EmailMessage WHERE HasAttachment = TRUE AND ParentId = :parentId];
        Set<String> linkedEntityIds = new Set<String>();
        for (EmailMessage em : theEmails) {
            linkedEntityIds.add(em.Id);
        }
        //Add Id for the Case to the list as well
        linkedEntityIds.add(parentId);
        List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
        Set<String> currFileIds = new Set<String>();
        for (ContentDocumentLink cdl : CDLs) {
            currFileIds.add(cdl.ContentDocumentId);
        }
        List<ContentDocument> parCDs = [SELECT Id FROM ContentDocument WHERE Id IN :currFileIds];
        startingFiles = new Set<Id>();
        if(parCDs.size() > 0){
            for(ContentDocument f : parCDs){
                this.startingFiles.add(f.Id);
            }
            this.hasParentFiles = true;
        }
    }
     

    public PageReference sendContent () {
        System.debug(AIeditorContent);
        //System.debug(FUeditorContent);
        this.theCase.AI_Action_Item_Notes_to_Include__c = AIeditorContent;
        this.theCase.FU_Notes_to_Include__c = FUeditorContent;
        return null;
    }
    
    public PageReference save() {
        System.Savepoint sp = Database.setSavepoint();
        //System.debug('Owner before insert: ' + this.theCase.OwnerId);
        if(theEm != null){
            if(String.isNotEmpty(theEM.HTMLBody)){
                this.incDesc = theEM.HTMLBody;
            }
        }
        
        Boolean isInternal = this.theCase.OwnerId != null;
        //RecordType internalRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Internal_Case' LIMIT 1];
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Internal_Case').getRecordTypeId();
        
        this.theCase.Subject = theCase.AI_Email_Subject__c;
        
        if(isInternal){
            
            this.theCase.RecordTypeId = rtId;
            //this.theCase.GDI_Department__c = this.parentCase.Customer_Care_Division__c;
              this.theCase.GDI_Department__c = this.parentCase.GDI_Department__c; //Harish added for case 02645742 to set Department on child Item to Parent Case  
              this.theCase.Product_Category__c = this.parentCase.Product_Category__c;
            //this.theCase.IC_Acton_Item_Owner__c = this.theCase.Send_To_Queue__c != null ? this.theCase.Send_To_Queue__c : this.theCase.Send_To_User__c;
            
            this.theCase.IC_Acton_Item_Owner__c = UserInfo.getUserId();
            String caseSubject = this.theCase.Subject;
            
            if(caseSubject != null){
                if(!caseSubject.startsWith('IC -')){
                    this.theCase.Subject = 'IC - ' + theCase.AI_Email_Subject__c;
                }
            }
            this.theCase.Origin = 'Action Item';
            this.theCase.IC_Action_Item_Notes__c = AIeditorContent;
            this.theCase.Status ='New';
        }
        else{
            if(this.parentIsAI){
                this.parentId = this.parentCase.ParentId;
                this.theCase.Team__c = this.parentcase.OwnerId;
            }
        }
        
        this.theCase.AI_Action_Item_Notes_to_Include__c = AIeditorContent;
        this.theCase.FU_Notes_to_Include__c = FUeditorContent;
        this.theCase.AI_Send_Initial_Email__c = false;
        this.theCase.AI_Action_Item_Notes_to_Include_Plain__c = AIeditorContent.replaceAll('<[^>]+>',' ');

        this.theCase.AI_Original_Email_Body__c = getHeader() + this.theCase.AI_Original_Email_Body__c;
        
        //System.debug(isFollowUpFlag);
        if(isFollowUpFlag){
            this.orgCase.Status = 'Closed';
            if(!String.isEmpty(this.theCase.AI_Original_Email_Body__c)){
                this.theCase.AI_Original_Email_Body__c = this.theCase.AI_Original_Email_Body__c.replaceAll('(ref:(\\w+).(\\w+):ref)','');
            }
            try{
                update this.orgCase;
            }
            catch(DmlException e){
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0));
                ApexPages.addMessage(errorMsg);
                System.debug(e);
                return null;
            }
            catch(Exception ee){
                System.debug(ee);
                return null;
            }
        }

        if(isMC && this.theCase.AI_Add_Body_of_Original_Email__c == false){
            this.theCase.AI_Original_Email_Body__c = null;
        }

        //insert new case
        try{
            if(Test.isRunningTest()){
                this.theCase.OwnerId = UserInfo.getUserId();
            }
            //System.debug(this.theCase.OwnerId);
            System.debug('Creating Action Item');
            System.debug('this.theCase Business hours :'+this.theCase.BusinessHoursId);
            insert this.theCase;
            System.debug('Action Item Successfully Created');
            //System.debug(this.theCase.Id);
        }
        catch(DmlException e){
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0));
            ApexPages.addMessage(errorMsg);
            System.debug(e);
            return null;
        }
        catch(Exception ee){
            System.debug(ee);
            return null;
        }
        
        // Associate the files from the parent case and related email messages with the new Action Item case

            List<ContentDocumentLink> toCloneCDL = new List<ContentDocumentLink>();
            Set<String> linkedEntityIds = new Set<String>();
            Set<Id> contentDocIds = new Set<Id>();
            for(FileWrapper fw : filesWrapper){
                if(fw.include){
                    contentDocIds.add(fw.theFile.Id);
                    System.debug('Content Document Added to list: ' + fw.theFile.Id);
                }
            }
            // Get the IDs of all email messages that have a file associated with them as well as the parent case
            List<EmailMessage> theEmails = [SELECT Id FROM EmailMessage WHERE HasAttachment = TRUE AND ParentId = :parentId];
            for (EmailMessage em : theEmails) {
                linkedEntityIds.add(em.Id);
            }
            linkedEntityIds.add(parentId);

        if(linkedEntityIds.size() > 0){
        
            for(ContentDocumentLink cdl : [SELECT ContentDocumentId,Id,IsDeleted,LinkedEntityId,ShareType,SystemModstamp,Visibility
                FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds])
            {
                if(this.theCase.Id!=null && contentDocIds.contains(cdl.ContentDocumentId))
                {
                    ContentDocumentLink newCDL = new ContentDocumentLink();
                    newCDL.ContentDocumentId = cdl.ContentDocumentId;
                    newCDL.LinkedEntityId = this.theCase.Id;
                    newCDL.ShareType = 'V';
                    newCDL.Visibility = 'AllUsers';
                    toCloneCDL.add(newCDL);
                }
            }
            if (toCloneCDL.size() > 0) Database.insert(toCloneCDL,false);
            
        }
        //System.debug('Updating Action Item');
        //update theCase;
        

        if(this.theCase.ContactId != null){
            Contact theContact = [SELECT Id, Name, Email FROM Contact WHERE Id =: this.theCase.ContactId];
            Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage();
            theEmail.setWhatId(this.theCase.Id);
            theEmail.setTargetObjectId(this.theCase.ContactId);
            Email_Template_Admin__c ET = Email_Template_Admin__c.getOrgDefaults();

            List<Id> ccIds = new List<Id>();
            for(addressWrapper aw : ccWrappers){
                ccIds.add(aw.theWrap.ContactId);
            }
            
            List<Contact> theCC_Contacts = [SELECT Email FROM Contact WHERE Id IN :ccIds];
            List<String> ccAddresses = new List<String>();
            for(Contact c: theCC_Contacts){
                if(String.isNotEmpty(c.Email)){
                    ccAddresses.add(c.Email);
                }
            }
            
            if(!String.isEmpty(freeFormCC)){
            
                if(!Pattern.matches('',freeFormCC)){
                    //throw formatting error
                }
            
                List<String> ffCC = freeFormCC.split(';');
                ccAddresses.addAll(ffCC);
            }
            theEmail.setCcAddresses(ccAddresses);
            
            if(this.theCase.AI_Add_Body_of_Original_Email__c == true){
                System.debug('inside email template with Email Body');
                theEmail.setTemplateId(String.valueOf(ET.get('AI_With_Email_Body__c')));
            }
            else{
                theEmail.setTemplateId(String.valueOf(ET.get('AI_Without_Email_Body__c')));
            }         

            theEmail.setOrgWideEmailAddressId(parentCase.Org_Wide_Email_Address_ID__c);
            List<Messaging.EmailFileAttachment> theAttachments = new List<Messaging.EmailFileAttachment>();
           
            List<ContentVersion> documents = new List<ContentVersion>();
            Set<Id> conDocIds = new Set<Id>();
            
            for(FileWrapper fw : filesWrapper){
                if(fw.include){
                    conDocIds.add(fw.theFile.Id);
                }

            }

            if(conDocIds.size() > 0){
                documents.addAll([
                        SELECT Id, Title, FileType, FileExtension, VersionData, IsLatest, ContentDocumentId
                        FROM ContentVersion
                        WHERE IsLatest = TRUE AND ContentDocumentId IN :conDocIds
                ]);
            }

            for (ContentVersion document: documents) {
                String newTitle = document.Title +'.'+ document.FileExtension;
                String titleEnd = document.Title.substringAfterLast('.');
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setBody(document.VersionData);
                if (titleEnd != null && document.FileExtension == titleEnd ) efa.setFileName(document.Title);
                if (titleEnd == null || titleEnd == '') efa.setFileName(newTitle);
                theAttachments.add(efa);
            }
            
            if(theAttachments.size() > 0){
                theEmail.setFileAttachments(theAttachments);
            }
           
            theEmail.setUseSignature(false);
            
            //System.debug(theEmail);
            try{
                System.debug('Action Item Sending Email');
                Messaging.SendEmailResult[] theResult = Messaging.sendEmail(new Messaging.Email[]{theEmail}, true);
            }
            catch(Exception e){
                Database.rollback(sp);
                this.theCase.Id = null;
                System.debug(e);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
                ApexPages.addMessage(myMsg);
                //System.debug(e);
                return ApexPages.currentPage();
            }
        }   
        
        //code added for Milton Roy delete contentdocuments not included and not in starting files.
        System.debug('checking contentdocs only includes selected files : ');
        System.debug(contentDocIds);
        Set<Id> contentDocsToDelete = new Set<id>();
        List<ContentDocumentLink> allParentDocLinks = [SELECT Id,ContentDocumentId fROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
        for(ContentDocumentLink cdl : allParentDocLinks){
            if(!contentDocIds.contains(cdl.ContentDocumentId) && !this.startingFiles.contains(cdl.ContentDocumentId)){
                contentDocsToDelete.add(cdl.ContentDocumentId);
            }
        }
        List<ContentDocument> delContents = [SELECT Id FROM ContentDocument WHERE Id IN : contentDocsToDelete];
        try{
            Database.delete(delContents,false);
        }
        catch(Exception e){
            System.debug(e);
        }
        System.debug('files needs to be delete from parent : ' + contentDocsToDelete);
    



        Case cTemp = [SELECT CaseNumber FROM Case WHERE Id =: theCase.Id];
        PageReference newPR = new PageReference('/apex/C_MiddleMan?origin='+subTabId+'&dest='+theCase.Id+'&prefix=AI&name='+cTemp.CaseNumber);
        newPR.setRedirect(true);
        return newPR;
    }
    
    @testVisible
    private String getHeader(){  
        System.debug(' getHeader ');
        try{
        String result = '';
        result += 'To: ' + (String.isEmpty(this.parentCase.EmailMessages[0].ToAddress) ? '' : this.parentCase.EmailMessages[0].ToAddress) + '\n';
        if(!String.isEmpty(this.parentCase.EmailMessages[0].CcAddress)){
            result += 'Cc: ' + this.parentCase.EmailMessages[0].CcAddress + '\n';
        }
        result += 'From: ' + (String.isEmpty(this.parentCase.EmailMessages[0].FromName) ? '' : this.parentCase.EmailMessages[0].FromName) + ' [' + this.parentCase.EmailMessages[0].FromAddress+ ']\n';
        result += 'Sent: ' + this.parentCase.EmailMessages[0].MessageDate + '\n';       
        result += 'Subject: ' + this.parentCase.EmailMessages[0].Subject + '\n\n';
            System.debug('result '+result);
        return result;
        }
        catch(Exception e){
            return '';
        }
    }

    @RemoteAction 
    public static RemoteSaveResult saveImage (String filename, String imageBody) {
        
        // Get the ID of the folder we wish to keep these pasted images in. The folder should be public if images are to be viewed in external emails.
        Id attachmentsFolderId = [SELECT Id FROM Folder WHERE Name = 'Attachments' LIMIT 1][0].Id;
        
        // Create the document that keeps the image.
        Document doc = new Document();
        doc.FolderId = attachmentsFolderId;
        doc.Name = filename+'_'+Datetime.now();
        doc.Description = filename;
        doc.ContentType = 'image/png';
        doc.Type = 'png';
        doc.Body = EncodingUtil.base64Decode(imageBody);
        doc.IsPublic = true;
        
        // Save the document.
        Database.SaveResult result = Database.insert(doc, false);
        
        // Create the URL to the image being saved. This should be org-agnostic.
        String baseOrgURL = System.Url.getSalesforceBaseUrl().toExternalForm();
        Integer firstIndex = baseOrgURL.indexOf('.')+1;
        Integer secondIndex = baseOrgURL.indexOf('.', firstIndex);
        String orgBase = baseOrgURL.substring(firstIndex, secondIndex);
        String contentBaseURL = 'https://c.'+ orgBase +'.content.force.com/servlet/servlet.ImageServer?id=';
        String docID = ((String)doc.Id).substring(0, 15);
        String orgID = ((String)[SELECT Id, Name FROM Organization LIMIT 1][0].Id).substring(0, 15);
        String imageURL = contentBaseURL+docID+'&oid='+orgID;
        
        
        // Put the results of this operation in a RemoteSaveResult object.
        RemoteSaveResult newResult = new RemoteSaveResult();
        newResult.success = result.isSuccess();
        newResult.successMessage = result.isSuccess() ? imageURL : '';
        newResult.errorMessage = result.isSuccess() ? '' : result.getErrors()[0].getMessage();
        
        return newResult;
    } 

    public class RemoteSaveResult {
        public Boolean success;
        public String errorMessage;
        public String successMessage;
    }
    
    public class addressWrapper{
        public Case theWrap {get; set;}
        
        public addressWrapper(){
            this.theWrap = new Case();
        }
        
        public addressWrapper(Contact c){
            this.theWrap = new Case();
            this.theWrap.ContactId = c.Id;
        }
    }

    public PageReference addCC() {
        //System.debug('addCC() action');
        ccWrappers.add(new addressWrapper());
        return null;
    }

    public class FileWrapper{
        private Boolean first = true;
        protected C_Action_Item_Extension controller;
        public ContentDocument theFile {get; private set;}
        public Boolean include {get;
            set{
                if(value == false && first && controller.startingFiles.contains(this.theFile.Id)){
                    System.debug('inside file wrapper class first changed to false' + this.first);
                    this.first = false;
                }
                else{
                    this.include = value;
                    this.controller.wrapMapF.put(this.theFile.Id,this);
                    System.debug('check include value inside FileWrapper ' + this.theFile.Id + ' is : ' + this.include);
                    system.debug('inside else block include function '+  this.controller.wrapMapF.keySet());

                }
            }
        }

        public FileWrapper(ContentDocument f, Boolean inc, C_Action_Item_Extension con){
            this.controller = con;
            this.theFile = f;
            this.include = inc;
        }
    }

    public void createNewFiles_Stage () {
        createNewFiles(fileID);
    }

    public void createNewFiles(String fileId) {
        //Get file
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :fileId].ContentDocumentId;
        //System.debug('conDocId ====> '+conDocId);

        if (conDocId != null) {
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocId; //Add ContentDocumentId
            cDocLink.LinkedEntityId = parentId; //Add file parentId
            cDocLink.ShareType = 'V'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers'; //AllUsers, InternalUsers, SharedUsers
            insert cDocLink;
        }
    }
}