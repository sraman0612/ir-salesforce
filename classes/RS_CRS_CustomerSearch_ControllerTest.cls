/******************************************************************************
Name:       RS_CRS_CustomerSearch_ControllerTest
Purpose:    Test class for RS_CRS_CustomerSearch_Controller. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Ashish Takke    12/26/2017  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_CustomerSearch_ControllerTest {
    
    /******************************************************************************
	Method Name : searchCustomer_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing searchCustomer Method
	******************************************************************************/
    static testmethod void searchCustomer_Test(){
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
            
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting Account Record Type Id through Schema.Describe Class
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type);
        
        //Getting Asset Record Type Id through Schema.Describe Class
        Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
        
        System.runAs(userRec){
            
            Account acc1 = new Account();
            acc1.recordTypeId = accRecId;
            acc1.LastName = 'Test Account 1';
            acc1.Phone = '123456789';
            acc1.Email_ID__c = 'first.last@email.com';
            acc1.PersonMailingState = 'IL';
            acc1.PersonMailingCountry = 'US';
            acc1.PersonMailingPostalCode = '60089';
            insert acc1;
            
            Account acc2 = new Account();
            acc2.recordTypeId = accRecId;
            acc2.LastName = 'Test account2';
            acc2.Phone = '987654321';
            acc2.Email_ID__c = 'first2.last2@email.com';
            acc2.PersonMailingState = 'IL';
            acc2.PersonMailingCountry = 'US';
            acc2.PersonMailingPostalCode = '60015';
            insert acc2;
            
            Asset assetRec = new Asset();
            assetRec.recordTypeId = assetRecId;
            assetRec.Name = '12345678';
            assetRec.SerialNumber = '12345678';
            assetRec.accountId = acc1.Id;
            assetRec.Manufacturer__c = 'Trane';
            insert assetRec;
            
            PageReference pageRef = Page.RS_CRS_CustomerSearch;
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(acc1);
            RS_CRS_CustomerSearch_Controller custSearch = new RS_CRS_CustomerSearch_Controller(stdController);
            custSearch.firstName = 'Test';
            custSearch.lastName = 'Test';
            custSearch.phone = '12345';
            custSearch.email = 'first.last@email.com';
            custSearch.serialNumber = '12345678';
            custSearch.street = 'street';
            custSearch.city = 'city';
            custSearch.state = 'IL';
            custSearch.country = 'US';
            custSearch.ZipCode = '60089';
            
            custSearch.searchCustomer();
            
            List<Account> accList = custSearch.accList;
            
            System.assertEquals(2, accList.size());
            
        }
    }
}