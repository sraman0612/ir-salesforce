@isTest
public class Case_TriggerHandler_Test {

	private static Id ccProdRecTypeId = sObjectService.getRecordTypeId(Product2.getSObjectType(), 'Club Car');
	private static Id ccgpsiProdRecTypeId = sObjectService.getRecordTypeId(Product2.getSObjectType(), 'CC GPSi');
    private static Id ccCaseRecordTypeId = SObjectService.getRecordTypeId(Case.sObjectType, 'Club Car');
    private static Id transportRequestRecordTypeId = sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car - Transportation Request');

    @TestSetup 
    private static void setupData() {
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
        
        Case_Settings__c caseSettings = Case_Settings__c.getOrgDefaults();
        caseSettings.CC_Case_Trigger_Handler_Enabled__c = true;
        upsert caseSettings;

        Account a = [Select Id From Account LIMIT 1];
        
        CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
        ml1.Customer__c = a.Id;
        insert ml1;
       
        CC_Short_Term_Lease__c stl1 = TestDataUtility.createShortTermLease();
        stl1.Beginning_Date__c = Date.today().addDays(-2);
        stl1.Master_Lease__c = ml1.Id;
        stl1.Delivery_Quoted_Rate__c = null;
        
        CC_Short_Term_Lease__c stl2 = TestDataUtility.createShortTermLease();
        stl2.Beginning_Date__c = Date.today();
        stl2.Master_Lease__c = ml1.Id;
        stl2.Delivery_Quoted_Rate__c = null;
        
        insert new CC_Short_Term_Lease__c[]{stl1, stl2};
        
        // Valid nature code, valid record type
        Product2 prod1 = TestDataUtility.createProduct('Prod1');
        prod1.RecordTypeId = ccProdRecTypeId;
        prod1.ERP_Item_Number__c = 'FRx12345';
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';        
        
        // Valid nature code, invalid record type
        Product2 prod2 = TestDataUtility.createProduct('Prod2');
        prod2.RecordTypeId = ccgpsiProdRecTypeId;
        prod2.ERP_Item_Number__c = 'FRx12346';
        prod2.CC_Item_Class__c = 'LCAR';
        prod2.CC_Market_Type__c = 'Consumer';         
        
        // Invalid nature code, valid record type
        Product2 prod3 = TestDataUtility.createProduct('Prod3');
        prod3.RecordTypeId = ccProdRecTypeId;
        prod3.ERP_Item_Number__c = 'ABc12347';     
        prod3.CC_Item_Class__c = 'LCAR';
        prod3.CC_Market_Type__c = 'Consumer';                          
              
        // Invalid nature code, invalid record type
        Product2 prod4 = TestDataUtility.createProduct('Prod4');
        prod4.RecordTypeId = ccgpsiProdRecTypeId;
        prod4.ERP_Item_Number__c = 'ABc12348';           
        prod4.CC_Item_Class__c = 'LCAR';
        prod4.CC_Market_Type__c = 'Consumer';         
        
        insert new Product2[]{prod1, prod2, prod3, prod4};        
        
        CC_Order__c ord1 = TestUtilityClass.createOrder(a.id);
        ord1.CC_Order_Number_Reference__c = 'Order1';
        
        CC_Order__c ord2 = TestUtilityClass.createOrder(a.id);
        ord2.CC_Order_Number_Reference__c = 'Order2';
        
        insert new CC_Order__c[]{ord1, ord2};         
        
        CC_Order_Item__c orderItem1 = TestUtilityClass.createOrderItemWithProduct(ord1.Id, prod1.Id, 500);
        CC_Order_Item__c orderItem2 = TestUtilityClass.createOrderItemWithProduct(ord1.Id, prod2.Id, 500);
        CC_Order_Item__c orderItem3 = TestUtilityClass.createOrderItemWithProduct(ord1.Id, prod2.Id, 500);
        CC_Order_Item__c orderItem4 = TestUtilityClass.createOrderItemWithProduct(ord2.Id, prod2.Id, 500);
        
        insert new CC_Order_Item__c[]{orderItem1, orderItem2, orderItem3, orderItem4};
        
        Case rgaCase = TestDataUtility.createCase(false, ccCaseRecordTypeId, null, a.Id, 'Test1', null);
        rgaCase.Reason = 'Parts';
        rgaCase.Sub_Reason__c = 'RGA';
        rgaCase.CC_Order__c = ord1.Id;
        insert rgaCase;
        
        CC_Case_Order_Item__c caseOrderItem1 = TestUtilityClass.createCaseOrderItem(rgaCase.Id, orderItem1.Id);  
        caseOrderItem1.Serial_Numbers__c = 'ABC123; ABC456;';
        
        CC_Case_Order_Item__c caseOrderItem2 = TestUtilityClass.createCaseOrderItem(rgaCase.Id, orderItem2.Id);  
        caseOrderItem2.Serial_Numbers__c = 'XYZ123; XYZ456;';

        CC_Case_Order_Item__c caseOrderItem3 = TestUtilityClass.createCaseOrderItem(rgaCase.Id, orderItem3.Id);  
        caseOrderItem3.Serial_Numbers__c = null;        

        insert new CC_Case_Order_Item__c[]{caseOrderItem1, caseOrderItem2, caseOrderItem3};        
    }
    
    @isTest
    private static void testTriggerSettings(){
        
        Case_Settings__c caseSettings = Case_Settings__c.getOrgDefaults();
        caseSettings.CC_Case_Trigger_Handler_Enabled__c = true;
        update caseSettings;        
        
        system.assertEquals(true, CC_Case_Trigger_Handler.getIsTriggerEnabled());
        
        caseSettings.CC_Case_Trigger_Handler_Enabled__c = false;
        update caseSettings;        
        
        system.assertEquals(false, CC_Case_Trigger_Handler.getIsTriggerEnabled());        
    }
    
    @isTest
    private static void testCopyRGAInfoToTRcase(){
        
        Case rgaCase = [Select Id, CaseNumber, CC_Order__c, AccountId, CC_Order__r.Total_Invoice_Amount__c From Case Where RecordTypeId = :ccCaseRecordTypeId and Sub_Reason__c = 'RGA'];
        Case trCase;

        system.debug('test rga case: ' + rgaCase);

        Test.startTest();
        
        trCase = TestDataUtility.createCase(false, transportRequestRecordTypeId, null, rgaCase.AccountId, 'Test2', null);
        trCase.ParentId = rgaCase.Id;
        insert trCase;

        Test.stopTest();

        // Verify the data copied to the new TR case
        trCase = [Select Description, CC_Bill_Amount__c From Case Where Id = :trCase.Id];

        system.assertEquals(1500, trCase.CC_Bill_Amount__c);
        system.assert(trCase.Description.startsWith('Serial Numbers: '));
        system.assert(trCase.Description.contains('ABC123; ABC456;'));
        system.assert(trCase.Description.contains('XYZ123; XYZ456;'));   
    }
    
    @isTest
    private static void testCreateTRPickupCasesInsert(){
    	
        CC_Short_Term_Lease__c[] stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];    	
    	
    	Case[] cases = new Case[]{};
    	
    	Test.startTest();
    	
    	Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c1.Create_Pickup_Case__c = true;
    	
    	Case c2 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c2.Create_Pickup_Case__c = true; 
    	
    	Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c3.Create_Pickup_Case__c = false;    	
    	
    	cases.add(c1);
    	cases.add(c2);
    	cases.add(c3);
    	
    	insert cases;   	
    	
    	Test.stopTest();
    	
    	Case[] newCases = [Select Id From Case Where Id not in :cases and Sub_Reason__c = 'Short Term Lease Pick-up'];
    	system.assertEquals(2, newCases.size());
    }   
    
    @isTest
    private static void testCreateTRPickupCasesUpdate(){
    	
        CC_Short_Term_Lease__c[] stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];    	
    	
    	Case[] cases = new Case[]{};
    	
    	Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c1.Create_Pickup_Case__c = false;
    	
    	Case c2 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c2.Create_Pickup_Case__c = false; 
    	
    	Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
    	c3.Create_Pickup_Case__c = false;    	
    	
    	cases.add(c1);
    	cases.add(c2);
    	cases.add(c3);
    	
    	insert cases;      	
    	
    	Test.startTest();
    	
 		c1.Create_Pickup_Case__c = true;
 		c2.Create_Pickup_Case__c = true;
 		
 		update cases;
    	
    	Test.stopTest();
    	
    	Case[] newCases = [Select Id From Case Where Id not in :cases and Sub_Reason__c = 'Short Term Lease Pick-up'];
    	system.assertEquals(2, newCases.size());
    }     
    
    @isTest
    private static void testProcessTRPriceChanges1(){
        
        CC_Short_Term_Lease__c[] stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];
    
        system.assertEquals(2, stls.size());
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[0].Pickup_Internal_Cost__c);        
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[1].Pickup_Internal_Cost__c);          
        
        Test.startTest();
        
        // Create a few new transportation cases
        Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 1000);
        c1.CC_Bill_Amount__c = 1200;
        
        Case c2 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 3000);
        
        Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
        c3.CC_Quote_Date__c = Date.today();
        
        Case c4 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, 2000);
        c4.CC_Bill_Amount__c = 2000;
        
        Case c5 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[1].Id, 3000);
        c5.CC_Bill_Amount__c = 3300;
        c5.CC_Quote_Date__c = Date.today();
        
        Case c6 = TestDataUtility.createTransportationCase(null, stls[0].Id, 1000);
        c6.CC_Bill_Amount__c = 1000;

        // Create a few pickup cases with no quoted price/date
        Case c7 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, null);
        Case c8 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, null);
        
        // Insert the pickup cases first and then delivery cases separately   
        system.debug('--inserting pickup cases');     
        insert new Case[]{c1, c2, c4, c6, c7, c8};

        system.debug('--inserting delivery cases');
        insert new Case[]{c3, c5};
            
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        // Quoted rates should not be updated per Shirely (12/9/2020)
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c); 
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c );
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(1200, stls[0].Pickup_Internal_Cost__c);           
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c ); 
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);    
        
        // Get updated pickup cases
        Case[] updatedPickupCases = [Select CC_Quoted_Price__c, CC_Quote_Date__c From Case Where Id in :new Case[]{c1, c2, c4, c6, c7, c8}];

        system.assertEquals(6, updatedPickupCases.size());

        for (Case c : updatedPickupCases){

            if (c.Id == c1.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }
            else if (c.Id == c2.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }   
            else if (c.Id == c4.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c6.Id){
                system.assertEquals(1000, c.CC_Quoted_Price__c);
                system.assertEquals(null, c.CC_Quote_Date__c);
            }  
            else if (c.Id == c7.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c8.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }                                                           
        }
        
        // Update the cases by changing reason, record type, short term lease, quoted price and bill amount
        
        // Reason change
        c1.Sub_Reason__c = 'Short Term Lease Delivery';
        
        update c1;
        
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(1200, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(0, stls[0].Pickup_Internal_Cost__c);           
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);    
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);                  
        
        Test.stopTest();
        
        // Verify no errors were logged
        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }

    @isTest
    private static void testProcessTRPriceChanges2(){
        
        CC_Short_Term_Lease__c[] stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];
    
        system.assertEquals(2, stls.size());
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[0].Pickup_Internal_Cost__c);        
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[1].Pickup_Internal_Cost__c);          
        
        Test.startTest();
        
        // Create a few new transportation cases
        Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 1000);
        c1.CC_Bill_Amount__c = 1200;
        
        Case c2 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 3000);
        
        Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
        c3.CC_Quote_Date__c = Date.today();
        
        Case c4 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, 2000);
        c4.CC_Bill_Amount__c = 2000;
        
        Case c5 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[1].Id, 3000);
        c5.CC_Bill_Amount__c = 3300;
        c5.CC_Quote_Date__c = Date.today();
        
        Case c6 = TestDataUtility.createTransportationCase(null, stls[0].Id, 1000);
        c6.CC_Bill_Amount__c = 1000;

        // Create a few pickup cases with no quoted price/date
        Case c7 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, null);
        Case c8 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, null);
        
        // Insert the pickup cases first and then delivery cases separately   
        system.debug('--inserting pickup cases');     
        insert new Case[]{c1, c2, c4, c6, c7, c8};

        system.debug('--inserting delivery cases');
        insert new Case[]{c3, c5};
            
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        // Quoted rates should not be updated per Shirely (12/9/2020)
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c); 
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c );
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(1200, stls[0].Pickup_Internal_Cost__c);           
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c ); 
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);    
        
        // Get updated pickup cases
        Case[] updatedPickupCases = [Select CC_Quoted_Price__c, CC_Quote_Date__c From Case Where Id in :new Case[]{c1, c2, c4, c6, c7, c8}];

        system.assertEquals(6, updatedPickupCases.size());

        for (Case c : updatedPickupCases){

            if (c.Id == c1.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }
            else if (c.Id == c2.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }   
            else if (c.Id == c4.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c6.Id){
                system.assertEquals(1000, c.CC_Quoted_Price__c);
                system.assertEquals(null, c.CC_Quote_Date__c);
            }  
            else if (c.Id == c7.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c8.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }                                                           
        }
        
        // Update the cases by changing reason, record type, short term lease, quoted price and bill amount
        
        // Quoted price change
        c2.CC_Quoted_Price__c = 7000;
        
        // Bill amount change
        c2.CC_Bill_Amount__c = 3100;        
        
        update c2;
        
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(4300, stls[0].Pickup_Internal_Cost__c);      
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);   
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);                          
        
        Test.stopTest();
        
        // Verify no errors were logged
        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }    

    @isTest
    private static void testProcessTRPriceChanges3(){
        
        CC_Short_Term_Lease__c[] stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];
    
        system.assertEquals(2, stls.size());
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[0].Pickup_Internal_Cost__c);        
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(null, stls[1].Pickup_Internal_Cost__c);          
        
        Test.startTest();
        
        // Create a few new transportation cases
        Case c1 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 1000);
        c1.CC_Bill_Amount__c = 1200;
        
        Case c2 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, 3000);
        
        Case c3 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[0].Id, 5000);
        c3.CC_Quote_Date__c = Date.today();
        
        Case c4 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, 2000);
        c4.CC_Bill_Amount__c = 2000;
        
        Case c5 = TestDataUtility.createTransportationCase('Short Term Lease Delivery', stls[1].Id, 3000);
        c5.CC_Bill_Amount__c = 3300;
        c5.CC_Quote_Date__c = Date.today();
        
        Case c6 = TestDataUtility.createTransportationCase(null, stls[0].Id, 1000);
        c6.CC_Bill_Amount__c = 1000;

        // Create a few pickup cases with no quoted price/date
        Case c7 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[0].Id, null);
        Case c8 = TestDataUtility.createTransportationCase('Short Term Lease Pick-up', stls[1].Id, null);
        
        // Insert the pickup cases first and then delivery cases separately   
        system.debug('--inserting pickup cases');     
        insert new Case[]{c1, c2, c4, c6, c7, c8};

        system.debug('--inserting delivery cases');
        insert new Case[]{c3, c5};
            
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        // Quoted rates should not be updated per Shirely (12/9/2020)
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c); 
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c );
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(1200, stls[0].Pickup_Internal_Cost__c);           
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c ); 
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);    
        
        // Get updated pickup cases
        Case[] updatedPickupCases = [Select CC_Quoted_Price__c, CC_Quote_Date__c From Case Where Id in :new Case[]{c1, c2, c4, c6, c7, c8}];

        system.assertEquals(6, updatedPickupCases.size());

        for (Case c : updatedPickupCases){

            if (c.Id == c1.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }
            else if (c.Id == c2.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }   
            else if (c.Id == c4.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c6.Id){
                system.assertEquals(1000, c.CC_Quoted_Price__c);
                system.assertEquals(null, c.CC_Quote_Date__c);
            }  
            else if (c.Id == c7.Id){
                system.assertEquals(5000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }  
            else if (c.Id == c8.Id){
                system.assertEquals(3000, c.CC_Quoted_Price__c);
                system.assertEquals(Date.today(), c.CC_Quote_Date__c);
            }                                                           
        }
        
        // Update the cases by changing reason, record type, short term lease, quoted price and bill amount             
        
        // Record Type change
        c3.RecordTypeId = sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car');

        update c3;
               
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(1200, stls[0].Pickup_Internal_Cost__c);           
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);  
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(2000, stls[1].Pickup_Internal_Cost__c);            
        
        // Short term lease change      
        c4.CC_Short_Term_Lease__c = stls[0].Id;

        update c4;
               
        // Verify the transportation price fields on the short term lease records were updated correctly
        stls = [Select Id, Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Order By Beginning_Date__c ASC];        
    
        system.assertEquals(null, stls[0].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[0].Pickup_Quoted_Rate__c);
        system.assertEquals(0, stls[0].Delivery_Internal_Cost__c);
        system.assertEquals(3200, stls[0].Pickup_Internal_Cost__c);         
        system.assertEquals(null, stls[1].Delivery_Quoted_Rate__c);
        system.assertEquals(null, stls[1].Pickup_Quoted_Rate__c);  
        system.assertEquals(3300, stls[1].Delivery_Internal_Cost__c);
        system.assertEquals(0, stls[1].Pickup_Internal_Cost__c);                 
        
        Test.stopTest();
        
        // Verify no errors were logged
        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }    
    
    static testMethod void UnitTest_Case_TriggerHandler()
    {    
        Test.startTest();
        List<Case> newList =new List<Case>();
        List<Case> OldList =new List<Case>();
        Map<Id,Case> newMap =new Map<Id,Case>();
        Map<Id,Case> OldMap =new Map<Id,Case>();
        Map<Id,String> UserMap =new Map<Id,String>();
             
        Account acc = [Select Id, Name From Account LIMIT 1];
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        contact con = TestUtilityClass.createContact(acc.Id);
        con.Email ='salesforce@clubcar.com.apextest';
        con.title='Apex Test Runner';
        con.phone='800 258 2227';
        insert con; 
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
                
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID); 
        user u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert u;
        System.assertEquals(u.ContactId,con.Id);
        System.assertNotEquals(false, u.IsActive);

        Case cas = new Case();
        cas.ContactId=con.Id;
        cas.CC_CorpId__c='123';
        newList.add(cas);
        insert newList;
        System.assertNotEquals(null, cas.Id);
        newMap.put(cas.Id, cas);
        Case ncas = new Case();
        ncas.ContactId=con.Id;
        ncas.CC_CorpId__c='456';
        OldList.add(ncas);
        insert OldList;        
        System.assertNotEquals(null, ncas.Id);
        OldMap.put(cas.Id, ncas);
        UserMap.put(u.Id,'123');
        Case_TriggerHandler.updateUserFedId(UserMap);
        Case_TriggerHandler.afterUpdate(newList, oldList, newMap, oldMap);
        delete OldList;
        Test.stopTest();
    }
}