@isTest
public with sharing class CC_PavilionSpeedCodeOrderCntMock implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
        res.setStatus('OK');
        res.setBody('<xml><hello>HI</hello></xml>');        
        return res;
    }
}