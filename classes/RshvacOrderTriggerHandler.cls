/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Trigger Handler for the "RSHVAC Order" SObject.
              This class should only be called by the trigger "rshvacOrderTrigger"
              And should be without sharing since triggers execute in System mode
------------------------------------------------------------*/

public without sharing class RshvacOrderTriggerHandler{
  // Instance of the helper class OpportunityUtils
  OpportunityUtils oppUtils;
  // This map will contain the Opportunity Id corresponding to each Order
  Map<Id, Id> opportunityIdByOrder = new Map<Id, Id>();
  // Map that will contain all the Opportunities related to the Orders being processed
  public Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();
  // Map that will contain all the Opportunities that need to be updated
  // The key of this map will be the Id of the Opportunity
  public Map<Id, Opportunity> opportunitiesToUpdate = new Map<Id, Opportunity>();
  // Map that will contain all the Revenue Schedules corresponding to each Opportunity
  // The key of this map will be the Id of the Opportunity
  public Map<Id, Opportunity_Revenue_Schedule__c> revenuesMap= new Map<Id, Opportunity_Revenue_Schedule__c>();
  // Map that will contain all the Revenue Schedules that need to be updated
  // The key of this map will be the Id of the Opportunity
  public Map<Id, Opportunity_Revenue_Schedule__c> revenuesToUpdate= new Map<Id, Opportunity_Revenue_Schedule__c>();
  // Account Sales Channel
  public static final String DSO = 'DSO';
  public static final String IWD = 'IWD';
    
  public List<Id> rshvacAccountList = new List<Id>();
  public List<Id> iwdAccountList = new List<Id>();
  public List<Id> costCenterList = new List<Id>();
  
  private static final Integer MAX_UPDATE_ATTEMPTS = 3;

  public RshvacOrderTriggerHandler(){
    // Get an instance of Oppo
    oppUtils = new OpportunityUtils();
  }

  public void bulkBefore(){

  }

  public void bulkAfter(){
    // On insert and updates, we'll need to update the Opportunities of the current year
    // Plus the appropiate Revenue Schedule
    if (Trigger.isUpdate || Trigger.isInsert){
      Set<Id> relatedAccounts = new Set<Id>();
      Map<Id, Set<Id>> ordersByAccount = new Map<Id, Set<Id>>();
      Map<Id, Set<Id>> ordersByCostCenter = new Map<Id, Set<Id>>();
      // Iterate over the Orders to collect the Account Ids
      for (RSHVAC_Order__c ord : [select id, Account__r.Channel__c, Account__r.Cost_Center_Name__c from RSHVAC_Order__c where Id in :Trigger.new]){
        //RSHVAC_Order__c ord = (RSHVAC_Order__c) sObj;
        relatedAccounts.add(ord.Account__c);
        // Also keep track of all the orders that are associated with a particular Account
        if (ord.Account__r.Channel__c != DSO){
          // Case 27331 / ticket 7532561 - Ronnie Stegall 1/8/2018 - Checking for null to prevent error 
          // "Non-selective query against large object type (more than 200000 rows)."   
          if (ord.Account__r.Cost_Center_Name__c != null)
          {
            if (ordersByCostCenter.get(ord.Account__r.Cost_Center_Name__c) == null){
              ordersByCostCenter.put(ord.Account__r.Cost_Center_Name__c, new Set<Id>());
            }
            ordersByCostCenter.get(ord.Account__r.Cost_Center_Name__c).add(ord.Id);
            costCenterList.add(ord.Account__r.Cost_Center_Name__c); 
          } 
        }
        else if(ord.Account__r.Channel__c == DSO) {
          if (ordersByAccount.get(ord.Account__c) == null){
            ordersByAccount.put(ord.Account__c, new Set<Id>());
          }
         ordersByAccount.get(ord.Account__c).add(ord.Id);
         rshvacAccountList.add(ord.Account__c);
        } 
      }
      // Get the Id of the Opportunity Existing BUsiness Record Type
      Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;
      if(!rshvacAccountList.isEmpty()){
          for (Opportunity opp : [SELECT Id, AccountId, Amount FROM Opportunity WHERE AccountId IN :rshvacAccountList AND CloseDate = THIS_YEAR AND Account.Channel__c = :DSO AND RecordTypeId = :existingBusinessRecTypeId] ){
            opportunitiesMap.put(opp.Id, opp); 
            for (Id orderId : ordersByAccount.get(opp.AccountId)){
              // There should one and only one Opportunity for each of the orders
              opportunityIdByOrder.put(orderId, opp.Id);
            }    
          }
                
        }
        if(!costCenterList.isEmpty()){
          for(Opportunity opp: [SELECT Id, Amount,AccountId,Cost_Center_Name__c FROM Opportunity WHERE Cost_Center_Name__c IN :costCenterList AND CloseDate = THIS_YEAR AND RecordTypeId = :existingBusinessRecTypeId]){          
            opportunitiesMap.put(opp.Id, opp);
            for (Id orderId : ordersByCostCenter.get(opp.Cost_Center_Name__c)){
              // There should one and only one Opportunity for each of the orders
              opportunityIdByOrder.put(orderId, opp.Id);
            }
          }
        }
      
      
       // Now query the opportunities to get the ones that we should update for each Order
       for (Opportunity_Revenue_Schedule__c rev : [SELECT Id, Opportunity__c, Actual__c, Opportunity__r.Account.Channel__c, Opportunity__r.Cost_Center_Name__r.Channel__c, Forecast_Month__c FROM Opportunity_Revenue_Schedule__c 
                                                      WHERE Opportunity__c IN :opportunitiesMap.keySet() AND Forecast_Month__c = THIS_MONTH]){
        // Preparing map with key as existing business opportunity id and and yesterday month's revenue schedule for DSO opportunity. 
        revenuesMap.put(rev.Opportunity__c, rev);
      }
    }
  }

  public void beforeInsert(RSHVAC_Order__c newOrder){
    // On insert only close line subtotal should be populated
    // so we don't consider any previous value nor the Billed today value
    newOrder.Billed_Today__c = newOrder.Close_Line_Subtotal__c;
    newOrder.Billed_Updated_Date__c = Date.today();
  }

  public void afterInsert(RSHVAC_Order__c newOrder){
    // Get the opportunity related to this particular Order
    Id oppId = opportunityIdByOrder.get(newOrder.Id);
    // There should always be one, but just in case
    if (oppId != null && opportunitiesMap.get(oppId) != null){
      // Update the Opportunity Amount value
      // If it was already on the ToUpdate Map get the Opportunity from it
      // That way we can make sure that we're updating the proper value
      Opportunity opp = opportunitiesToUpdate.get(oppId) != null ? opportunitiesToUpdate.get(oppId) : opportunitiesMap.get(oppId);
      oppUtils.updateExistingOpportunityAmount(opp, newOrder, null);
      opportunitiesToUpdate.put(oppId, opp);

      // Update the Revenue Schedule Actual value
      // If it was already on the ToUpdate Map get the Revenue from it
      // That way we can make sure that we're updating the proper value
      Opportunity_Revenue_Schedule__c revenue = revenuesToUpdate.get(oppId) != null ? revenuesToUpdate.get(oppId) : revenuesMap.get(oppId);
      if (revenue != null){
        oppUtils.updateExistingOpportunityRevenueScheduleForOrder(revenue, newOrder, null);
        revenuesToUpdate.put(oppId, revenue);

      }
    }
  }

  public void beforeUpdate(RSHVAC_Order__c oldOrder, RSHVAC_Order__c newOrder){
    // On update we set Billed today as the difference between the new and old sub totals
    // Plus the amount that has been alredy billed today
    // Only do this if the subtotals have changed and the new sub total is not null (to avoid any integration errors)
    if (oldOrder.Close_Line_Subtotal__c != newOrder.Close_Line_Subtotal__c && newOrder.Close_Line_Subtotal__c != null){
      newOrder.Billed_Today__c = newOrder.Close_Line_Subtotal__c;
      newOrder.Billed_Today__c -= oldOrder.Close_Line_Subtotal__c != null ? oldOrder.Close_Line_Subtotal__c : 0;
      newOrder.Billed_Today__c += oldOrder.Billed_Today__c != null ? oldOrder.Billed_Today__c : 0;
      newOrder.Billed_Updated_Date__c = Date.today();
    }
  }

  public void afterUpdate(RSHVAC_Order__c oldOrder, RSHVAC_Order__c newOrder){
    // Only do this if the totals have Changed and the new sub total is not null (to avoid any integration errors)
    if (oldOrder.Close_Line_Subtotal__c != newOrder.Close_Line_Subtotal__c && newOrder.Close_Line_Subtotal__c != null){
      // Get the opportunity related to this particular Order
      Id oppId = opportunityIdByOrder.get(newOrder.Id);
      // There should always be one, but just in case
      if (oppId != null && opportunitiesMap.get(oppId) != null){
        // If it was already on the ToUpdate Map get the Opportunity from it
        // That way we can make sure that we're updating the proper value
        Opportunity opp = opportunitiesToUpdate.get(oppId) != null ? opportunitiesToUpdate.get(oppId) : opportunitiesMap.get(oppId);
        // Update the Opportunity Amount value
        oppUtils.updateExistingOpportunityAmount(opp, newOrder, oldOrder);
        opportunitiesToUpdate.put(oppId, opp);

        // Update the Revenue Schedule Actual value
        // If it was already on the ToUpdate Map get the Revenue from it
        // That way we can make sure that we're updating the proper value
        Opportunity_Revenue_Schedule__c revenue = revenuesToUpdate.get(oppId) != null ? revenuesToUpdate.get(oppId) : revenuesMap.get(oppId);
        if (revenue != null){
          oppUtils.updateExistingOpportunityRevenueScheduleForOrder(revenue, newOrder, oldOrder);
          revenuesToUpdate.put(oppId, revenue);
        }
      }
    }
  }

  public void andFinally(){
    if (!opportunitiesToUpdate.isEmpty()){
      update opportunitiesToUpdate.values();
    }
    if (!revenuesToUpdate.isEmpty()){
      update revenuesToUpdate.values();
    }
  }
}