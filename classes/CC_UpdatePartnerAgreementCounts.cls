global class CC_UpdatePartnerAgreementCounts implements Schedulable, Database.Stateful,Database.Batchable<sObject>{
  
  global final string queryStr{get;set;}
  
  Map<String, String> errMap;
    
  global CC_UpdatePartnerAgreementCounts(string qry){queryStr = qry;}
    
  global Database.QueryLocator start(Database.BatchableContext bc) {return Database.getQueryLocator(queryStr);}

  global void execute(Database.BatchableContext bc, List<Account> acctLst){

    /*
      !!!VERY IMPORTANT!!! - this batch class is written to process only 1 Account at a time to be respectful of inetgration loads
    */
    if (null==errMap) {errMap = new Map<String,String>();}
    Account a = acctLst[0];
    Integer golfcnt=0;
    Integer commercialcnt=0;
    Integer consumercnt=0;
    boolean doUpdate = false;
    for(Contract c : [SELECT Id, CC_Market__c, CC_Sub_Type__c FROM Contract WHERE AccountId = :a.Id and CC_Contract_Status__c = 'Current']){
      if (c.CC_Market__c == 'Golf'){
        golfcnt+=1;
      } else if(c.CC_Market__c== 'Commercial'){
        commercialcnt+=1;
      } else if (c.CC_Market__c=='Consumer'){
        consumercnt+=1;
      }
    }
    if((null == a.Current_Commercial_Agreements__c || a.Current_Commercial_Agreements__c != commercialcnt) ||
       (null == a.Current_Consumer_Agreements__c || a.Current_Consumer_Agreements__c != consumercnt) ||
       (null == a.Current_Golf_Agreements__c || a.Current_Golf_Agreements__c != golfcnt)){
      a.Current_Commercial_Agreements__c = commercialcnt;
      a.Current_Consumer_Agreements__c = consumercnt;
      a.Current_Golf_Agreements__c = golfcnt;
      doUpdate = true;
    }
    if(doUpdate){
      Database.SaveResult sr = database.update(a,false);
      for(Database.Error err : sr.getErrors()) {
        errMap.put(a.Id, err.getMessage() + ' ' + err.getFields());
        System.debug('### unable to update agreement counts for this Account because ' + err.getMessage());
        System.debug('### error fields are ' + err.getFields());
      }
    }
  }
        
    
  global void finish(Database.BatchableContext bc){
    if(test.isRunningTest()){
        if (null==errMap) {errMap = new Map<String,String>();}
        errMap.put('testrecordid','testerrmsg');
    }  
    if(null != errMap && !errMap.isEmpty()){
      messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();     
      String[] sToAddress = new String[]{'lizy_frady@clubcar.com'};
      eml.setToAddresses(sToAddress);
      eml.setSenderDisplayName('Salesforce.com');
      eml.setSubject('Error Summary for Update Partner Current Agreeemnt Counts Batch Job');
      string msg = 'The below Accounts could not be updated' + '\n';
      for(String s : errMap.keyset()){
        msg += s + ' ' + errMap.get(s) + '\n';
      }
      eml.setPlainTextBody(msg);
      Messaging.sendEmail( new Messaging.SingleEmailMessage[]{eml});
    }
  }  
    
  global void execute(SchedulableContext sc) {

    /*
      !!!VERY IMPORTANT!!! - the batch class is written to process only 1 Account at a time
    */
    String qryStr = 'SELECT Id, Current_Commercial_Agreements__c, Current_Consumer_Agreements__c, Current_Golf_Agreements__c ' +
                    'FROM Account ' +
                    'WHERE RecordType.Name LIKE \'Club' + '%' + '\' and (Type = \'Dealer\' or Type = \'Distributor\')';

    CC_UpdatePartnerAgreementCounts upac = new CC_UpdatePartnerAgreementCounts(qryStr);
    Database.executebatch(upac,1);    
  }
}