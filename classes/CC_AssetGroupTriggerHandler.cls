public class CC_AssetGroupTriggerHandler {
    
	public static void beforeUpdate(List<CC_Asset_Group__c> assetGroups, Map<Id, CC_Asset_Group__c> oldMap){
    	setActiveLookups(assetGroups, oldMap);
  	}     
    
	public static void beforeInsert(List<CC_Asset_Group__c> assetGroups){
    	setFieldDefaults(assetGroups);
    	setActiveLookups(assetGroups, null);
  	}    
    
    public static void afterUpdate(List<CC_Asset_Group__c> assetGroups, Map<Id, CC_Asset_Group__c> oldMap){
    	checkAllRetired(assetGroups, oldMap);
    }
    
	public static void afterInsert(List<CC_Asset_Group__c> assetGroups){
    	CC_AssetGroupTriggerHelper.updatenewoldvendorfields(assetGroups);
  	}

	private static void setFieldDefaults(List<CC_Asset_Group__c> assetGroups){
		
		for (CC_Asset_Group__c ag : assetGroups){
			ag.Original_Fleet_Status__c = ag.Fleet_Status__c;
		}
	}
	
	private static void setActiveLookups(List<CC_Asset_Group__c> assetGroups, Map<Id, CC_Asset_Group__c> oldMap){
		
		for (CC_Asset_Group__c ag : assetGroups){
			
			CC_Asset_Group__c old = oldMap != null ? oldMap.get(ag.Id) : null;
			
			if (ag.Fleet_Status__c != 'Retirement' && (old == null || (ag.Fleet_Status__c != old.Fleet_Status__c))){
				
				ag.CC_Active_Asset_Lookup__c = ag.Account__c;
				ag.CC_Inactive_Asset_Lookup__c = null;
			}
		}
	}	

  	private static void checkAllRetired(List<CC_Asset_Group__c> assetGroups, Map<Id, CC_Asset_Group__c> oldMap){
  	
	  	Set<Id> golfAccIds = new Set<Id>();
	  	Set<Id> utilAccIds = new Set<Id>();
	  	CC_Asset_Group__c[] retiredAssetGroupsWithActiveCars = new CC_Asset_Group__c[]{};
	    
	    for (CC_Asset_Group__c ag : assetGroups){
	    
	    	CC_Asset_Group__c old = oldMap != null ? oldMap.get(ag.id) : null;
	    
	    	if (ag.Account__c != NULL && ag.Fleet_Status__c == 'Retirement'){
	    
	    		if(ag.Fleet_Type_Formula__c=='Utility'){
	    			utilAccIds.add(ag.Account__c);
	    		} 
	    		else if (ag.Fleet_Type_Formula__c=='Golf'){
	    			golfAccIds.add(ag.Account__c);
	    		}
	    		
	    		// Check to see if the asset group being retired has remaining assets
	    		if (old != null && ag.Fleet_Status__c != old.Fleet_Status__c && ag.Quantity__c > ag.Number_Retired__c){
	    			retiredAssetGroupsWithActiveCars.add(ag);
	    		}
	    	}
	    }
	    
	    system.debug('retiredAssetGroupsWithActiveCars: ' + retiredAssetGroupsWithActiveCars.size());
	    system.debug('golfAccIds: ' + golfAccIds.size());
	    system.debug('utilAccIds: ' + utilAccIds.size());
	    
	    if (retiredAssetGroupsWithActiveCars.size() > 0){
	    	
	    	CC_Asset_Group__c[] assetGroupsForRemainingCars = new CC_Asset_Group__c[]{};
	    	
	    	for (CC_Asset_Group__c ag : retiredAssetGroupsWithActiveCars){
	    		
	    		CC_Asset_Group__c newAg = new CC_Asset_Group__c(
	    			Account__c = ag.Account__c,
	    			Acquired_Date__c = ag.Acquired_Date__c,
	    			Asset_Group_Key__c = ag.Asset_Group_Key__c + '_2',
	    			Color__c = ag.Color__c,
	    			Fleet_Status__c = ag.Original_Fleet_Status__c,
	    			Fleet_Type__c = ag.Fleet_Type__c,
	    			Model_Year__c = ag.Model_Year__c,
	    			Order__c = ag.Order__c,
	    			Product__c = ag.Product__c,
	    			Quantity__c = ag.Quantity__c - ag.Number_Retired__c 
    			);
    			
    			assetGroupsForRemainingCars.add(newAg); 
	    	}
	    	
	    	Database.SaveResult[] saveResults = Database.insert(assetGroupsForRemainingCars, false);
	    	
	        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
	        
	        for (Database.SaveResult saveResult : saveResults){
	         
	         	system.debug('saveResult: ' + saveResult);
	            
	            if (!saveResult.isSuccess() && saveResult.getErrors().size() > 0){
	                apexLogHandler logHandler = new apexLogHandler('CC_AssetGroupTriggerHandler', 'checkAllRetired', saveResult.getErrors()[0].getMessage());
	                apexLogs.add(logHandler.logObj);
	            }
	        }
	        
	        system.debug('errors to log: ' + apexLogs);
	        
	        if (apexLogs.size() > 0){
	            insert apexLogs;
	        }  	    	
	    }
	    
    	if (!golfAccIds.isEmpty() || !utilAccIds.isEmpty()){
    	
	    	for (CC_Asset_Group__c ag : [SELECT Account__c, Fleet_Type_Formula__c
	    		                         FROM CC_Asset_Group__c 
	    		                         WHERE Fleet_Status__c != 'Retirement' and (Account__c IN :utilAccIds OR Account__c IN :golfAccIds)]){
	    		
	    		if(ag.Fleet_Type_Formula__c=='Utility'){
	    			utilAccIds.remove(ag.Account__c);
	    		} 
	    		else if (ag.Fleet_Type_Formula__c=='Golf'){
	    			golfAccIds.remove(ag.Account__c);
	    		}
	    	}
	    	
	    	if(!golfAccIds.isEmpty() || !utilAccIds.isEmpty()){
	    	
	    		Account [] aLst2Update = [SELECT Id, CC_Golf_Old_vendor__c, CC_Golf_New_Vendor__c, CC_Golf_Old_Vendor_Acquire_Date__c,
	                                             CC_Golf_New_Vendor_Acquire_Date__c, CC_Golf_Old_Vendor_Fleets__c, CC_Golf_New_Vendor_Fleets__c,
	                                             CC_Utility_Old_vendor__c, CC_Utility_New_Vendor__c, CC_Utility_Old_Vendor_Acquire_Date__c,
	                                             CC_Utility_New_Vendor_Acquire_Date__c,CC_Utility_Old_Vendor_Fleets__c, CC_Utility_New_Vendor_Fleets__c
	    								  FROM Account
	    								  WHERE Id IN :utilAccIds OR Id IN :golfAccIds];
	    								  
	    		for(Account a : aLst2Update){
	    			
	    			if(golfAccIds.contains(a.Id)){
	    				a.CC_Golf_Old_vendor__c = a.CC_Golf_New_Vendor__c;
	        	        a.CC_Golf_Old_Vendor_Acquire_Date__c = a.CC_Golf_New_Vendor_Acquire_Date__c;
	        	        a.CC_Golf_Old_Vendor_Fleets__c = a.CC_Golf_New_Vendor_Fleets__c;
	        	        a.CC_Golf_New_Vendor__c = 'NO CARS';
	        	        a.CC_Golf_New_Vendor_Acquire_Date__c = date.today();
	        	        a.CC_Golf_New_Vendor_Fleets__c = 0;
	    			}
	    			
	    			if(utilAccIds.contains(a.Id)){
	    				a.CC_Utility_Old_vendor__c = a.CC_Utility_New_Vendor__c;
	    	            a.CC_Utility_Old_Vendor_Acquire_Date__c = a.CC_Utility_New_Vendor_Acquire_Date__c;
	    	            a.CC_Utility_Old_Vendor_Fleets__c = a.CC_Utility_New_Vendor_Fleets__c;
	    	            a.CC_Utility_New_Vendor__c = 'NO CARS';
	    	            a.CC_Utility_New_Vendor_Acquire_Date__c = date.today();
	    	            a.CC_Utility_New_Vendor_Fleets__c = 0;
	    			}
	        	}
	        	
	        	if(!aLst2Update.isEmpty()){
	        		update aLst2Update;
	    		}    		
    		}
    	}
  	}
}