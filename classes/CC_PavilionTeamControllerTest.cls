@isTest
public class CC_PavilionTeamControllerTest
{
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
    }
    
    static testmethod void UnitTest_TeamController()
    {
        // Creation of Test Data
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con;
        system.debug('the contact created is'+con.AccountId);
        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        System.debug('User u ='+u);
        insert u; 
        System.assertEquals(u.ContactId,con.Id);
        System.assertNotEquals(false, u.IsActive);
        CC_My_Team__c myTeam =new CC_My_Team__c();
        myTeam.Account__c=acc.Id;
        myTeam.Custom_Department__c='Sales';
        insert myTeam;
        System.runAs(u)
        {
           
            CC_PavilionTeamController.getMyTeam('Sales');
            try
            {
                CC_PavilionTemplateController controller;
                CC_PavilionTeamController TeamCont = new CC_PavilionTeamController(controller);
                TeamCont.con=con;
            }
            Catch(Exception e)
            {
                System.debug(e.getMessage());
            }
            
        }
    }
    
    
  /*  @isTest
    static  void UnitTest_TeamController2()
    {
        User u = TestUtilityClass.getTestUser('');
        System.runas(u){
            CC_PavilionTeamController ctrl = new CC_PavilionTeamController(new CC_PavilionTemplateController() );
            CC_PavilionTeamController.getMyTeam('Custom Solutions'); 
        }
    } */
    
}