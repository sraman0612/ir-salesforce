@isTest
public with sharing class CC_EmailMessage_TriggerHandler_Test {
    
    private static Id ccRTid = Case.getSObjectType().getDescribe().getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
    
    @TestSetup
    private static void createData(){
        EmailMessage_TriggerTestHelper.createData();
    }
    
    private static testMethod void createNewCaseAndReparentEmail(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmail(ccRTid, null);
    }
    
    private static testMethod void createNewCaseAndReparentEmailPreexistingOpenChildCase(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailPreexistingOpenChildCase(ccRTid, null);
    }    
    
    private static testMethod void createNewCaseAndReparentEmailInactiveOwner(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailInactiveOwner(ccRTid, null, settings.CC_Default_Case_Owner_ID__c);        
    }    
    
    private static testMethod void createNewCaseAndReparentEmailOwnedByQueue(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailOwnedByQueue(ccRTid, null);        
    }        
    
    private static testMethod void testPreventDuplicateEmailsDisabled(){
    	
        Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        settings.CC_Email_Trigger_Enabled__c = false;
        upsert settings;       	
    	
        EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(ccRTid, false);         
    }    
    
    private static testMethod void testPreventDuplicateEmailsEnabled(){
        EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(ccRTid, true);         
    }
    
    private static testMethod void testPreventDuplicateEmailsEnabledClosedOriginalCase(){
        EmailMessage_TriggerTestHelper.testPreventDuplicateEmailsClosedOriginalCase(ccRTid, true);         
    }     
}