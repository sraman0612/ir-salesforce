public without sharing class CC_Order_LoadDetailsController{

    // Constants
    private static Id PARTNER_ACCOUNT_ID = [Select Id, AccountId From User Where Id = :UserInfo.getUserId()].AccountId;    

    // Need to decouple the load details from Koch if this is not a Koch tendered order
    public class LoadInfoResponse{
        public CC_Order__c order = new CC_Order__c();
        @AuraEnabled public Id orderId; 
        @AuraEnabled public Boolean isPortalUser = false;
        @AuraEnabled public LoadInfo[] loads = new LoadInfo[]{};
        @AuraEnabled public Boolean kochLoadDataFound = false;
        @AuraEnabled public String OrderNumber = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String OrdRefnum = CC_KochUtilities.EMPTY_VALUE;   
        @AuraEnabled public String PONumber = CC_KochUtilities.EMPTY_VALUE;     
        @AuraEnabled public String ShippingInstructions = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public Boolean success = true;
        @AuraEnabled public String errorMessage = '';
    }

    @TestVisible
    private class LoadInfo{        
        @AuraEnabled public Boolean isPortalUser = false;
        @AuraEnabled public Boolean kochLoadDataMatch = false;
        @AuraEnabled public Id carItemId;          
        @AuraEnabled public String Carrier = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String LoadCode = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String LastUpdate = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StpRefnum = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StpStatus = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StpDepartureDate = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StpArrivalDate = CC_KochUtilities.EMPTY_VALUE;                    
        @AuraEnabled public String CmpName = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StpAddress = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String CtyName = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String Distance = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StartCmpName = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StartAddress = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String StartCityName = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String LinkURL = CC_KochUtilities.EMPTY_VALUE;        
        @AuraEnabled public String OrderNumber = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String Quantity = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String BackOrderDetails = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String ScheduledPickupDateTime = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String EstimatedShipWeek = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String ActualPickupDateTime = CC_KochUtilities.EMPTY_VALUE;        
        @AuraEnabled public String ScheduledDeliveryDateTime = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String ActualDeliveryDateTime = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String LatestCalculatedDeliveryDate = CC_KochUtilities.EMPTY_VALUE;  
        @AuraEnabled public String NearestGPSLocation = CC_KochUtilities.EMPTY_VALUE;   
        @AuraEnabled public String TimeOffset;   
        @AuraEnabled public String KochLoadCode;
        @AuraEnabled public String Latitude = '';
        @AuraEnabled public String Longitude = '';
        @AuraEnabled public String LoadID = CC_KochUtilities.EMPTY_VALUE;    
        @AuraEnabled public String CarName = CC_KochUtilities.EMPTY_VALUE;
        @AuraEnabled public String SerialNumbers = CC_KochUtilities.EMPTY_VALUE;  

        // Original Koch values that will be used to update the database
        public String OriginalLastUpdate = CC_KochUtilities.EMPTY_VALUE;
        public String OriginalScheduledPickupDateTime = CC_KochUtilities.EMPTY_VALUE;
        public String OriginalActualPickupDateTime = CC_KochUtilities.EMPTY_VALUE;
        public String OriginalScheduledDeliveryDateTime = CC_KochUtilities.EMPTY_VALUE;
        public String OriginalLatestCalculatedDeliveryDate = CC_KochUtilities.EMPTY_VALUE;
        public String OriginalActualDeliveryDateTime = CC_KochUtilities.EMPTY_VALUE;
    } 
    
    @AuraEnabled(cacheable=false)
    public static LoadInfoResponse getLoadInfo(Id recordId){
        
        LoadInfoResponse response = new LoadInfoResponse();

        sObjectType objectType = recordId.getSObjectType();

        User currentUser = [Select Id, Name, AccountId, IsPortalEnabled, CC_Pavilion_Navigation_Profile__c From User Where Id = :UserInfo.getUserId()];
        response.isPortalUser = currentUser.IsPortalEnabled;              

        CC_Order__c order;
        Id orderId;
        CC_Car_Item__c[] carItems;

        String orderQuery = 
            'Select Id, Order_Number__c, RecordType.DeveloperName, CC_Shipping_Instructions__c , CC_Carrier_ID__c, PO__c, ' +
                'CC_Addressee_name__c, CC_Address_line_1__c, CC_Address_line_2__c, CC_Address_line_3__c, CC_City__c ' +
            'From CC_Order__c ' +
            'Where Id = :orderId';

        String carItemQueryBase = 
            'Select Id, Car_Item_Key__c, Shipment_Header_Number__c, Order_Item__r.Order__r.Order_Number__c, Scheduled_Ship_Date__c, Estimated_Ship_Date__c, Ship_Date__c, Delivery_Date_c__c, Delivery_Date_Status__c, ' +
                'Load_Shipped__c, Serial_No_s__c, Load_Code__c, Truck_Quantity__c, Line_Sequence__c, Truck_Sequence__c, LastModifiedDate, Order_Item__r.Product__r.Name, Accessory_Backorder_Details__c, ' +
                'Koch_Last_Update_Date__c, Scheduled_Ship_Date_Display__c, Delivery_Date_Display__c, Koch_Load_Code_Out_Of_Sync__c ' +
            'From CC_Car_Item__c ';

        String carItemOrderBy = 'Order By Delivery_Date_c__c DESC, Load_Date__c DESC, Scheduled_Ship_Date__c DESC';

        if (objectType == CC_Order__c.getSObjectType()){   

            orderId = recordId;   
            order = Database.query(orderQuery);

            String carItemQuery = carItemQueryBase + 'Where Order_Item__r.Order__c = :orderId ' + carItemOrderBy;
            carItems = Database.query(carItemQuery);
        }
        else if (objectType == CC_Car_Item__c.getSObjectType()){

            String carItemQuery = carItemQueryBase + 'Where Id = :recordId ' + carItemOrderBy;
            carItems = Database.query(carItemQuery);

            if (carItems.size() == 1){

                orderId = carItems[0].Order_Item__r.Order__c;   
                order = Database.query(orderQuery);
            }
        }

        // Build shipment header map to match with car items to pull shipment info
        Map<Double, CC_Order_Shipment__c> shipmentHeaderMap = new Map<Double, CC_Order_Shipment__c>();

        for (CC_Order_Shipment_Item__c shipmentItem : [Select Shipment_Header_Number__c, Order_Shipment__r.Carrier__c From CC_Order_Shipment_Item__c Where Order_Item__r.Order__c = :order.Id]){
            shipmentHeaderMap.put(shipmentItem.Shipment_Header_Number__c, shipmentItem.Order_Shipment__r);
        }

        response.order = order;
        response.orderId = orderId;
        response.OrderNumber = getAttributeValue(order.Order_Number__c, false, null);
        response.OrdRefnum = getAttributeValue(order.Order_Number__c, false, null);  
        response.PONumber = getAttributeValue(order.PO__c, false, null);        
        response.ShippingInstructions = getAttributeValue(order.CC_Shipping_Instructions__c, false, null);

        Map<String, CC_Car_Item__c> carItemKeyMap = new Map<String, CC_Car_Item__c>();

        for (CC_Car_Item__c carItem : carItems){
            carItemKeyMap.put(carItem.Car_Item_Key__c, carItem);
        }               

        // Call Koch API first to see if they tendered the load
        CC_KochSoapAPI.KochSOAPWebServiceSoap kochWS = new CC_KochSoapAPI.KochSOAPWebServiceSoap();

        kochWS.SecuredTokenHeader.AuthenticationToken = kochWS.AuthenticationUser();

        String loadCode = carItems.size() == 1 ? carItems[0].Load_Code__c : null;

        CC_KochSoapAPI.ArrayOfLoadInfo kochLoadInfoResponse = kochWS.LoadInfoRequest(loadCode, order.Order_Number__c);
        system.debug('kochLoadInfoResponse: ' + JSON.serialize(kochLoadInfoResponse));

        if (kochLoadInfoResponse.LoadInfo.size() > 0){

            for (CC_KochSoapAPI.LoadInfo load : kochLoadInfoResponse.LoadInfo){

                if (String.isBlank(load.Error)){

                    response.OrdRefnum = getAttributeValue(load.OrdRefnum, false, null);   

                    LoadInfo loadInfo = new LoadInfo();
                    loadInfo.isPortalUser = response.isPortalUser;
                    loadInfo.StpRefnum = load.StpRefnum;
                    loadInfo.Carrier = 'Koch Transportation';
                    loadInfo.KochLoadCode = load.LoadCode;

                    Decimal timeOffset = String.isNotBlank(load.TimeOffset) ? Decimal.valueOf(load.TimeOffset) : null;

                    String stopStatus = load.StpStatus;

                    if (stopStatus == 'Open'){

                        Datetime latestCalculatedDeliveryDate = CC_KochUtilities.getDateTimeFromKochData(load.LatestCalculatedDeliveryDate);
                        Datetime scheduledDeliveryDateTime = CC_KochUtilities.getDateTimeFromKochData(load.ScheduledDeliveryDateTime);

                        if (latestCalculatedDeliveryDate != null && scheduledDeliveryDateTime != null && latestCalculatedDeliveryDate > scheduledDeliveryDateTime){
                            stopStatus = 'Delayed';
                        }
                        else if (String.isNotBlank(load.ActualPickupDateTime)){
                            stopStatus = 'In Transit';
                        }                        
                        else if (scheduledDeliveryDateTime != null){
                            stopStatus = 'Scheduled';
                        }
                    }

                    loadInfo.StpStatus = stopStatus;                    
                    loadInfo.StpDepartureDate = getAttributeValue(load.StpDepartureDate, true, timeOffset);
                    loadInfo.StpArrivalDate = getAttributeValue(load.StpArrivalDate, true, timeOffset);                    
                    loadInfo.CmpName = getAttributeValue(load.CmpName, false, timeOffset);
                    loadInfo.StpAddress = getAttributeValue(load.StpAddress, false, timeOffset);
                    loadInfo.CtyName = getAttributeValue(load.CtyName, false, timeOffset);
                    loadInfo.Distance = getAttributeValue(load.Distance, false, timeOffset);
                    loadInfo.StartCmpName = getAttributeValue(load.StartCmpName, false, timeOffset);
                    loadInfo.StartAddress = getAttributeValue(load.StartAddress, false, timeOffset);
                    loadInfo.StartCityName = getAttributeValue(load.StartCityName, false, timeOffset);
                    loadInfo.LinkURL = getAttributeValue(load.LinkURL, false, timeOffset);                    
                    loadInfo.OrderNumber = getAttributeValue(load.OrderNumber, false, timeOffset);
                    loadInfo.Quantity = getAttributeValue(load.Quantity, false, timeOffset);
                    loadInfo.ScheduledPickupDateTime = getAttributeValue(load.ScheduledPickupDateTime, true, timeOffset); 
                    loadInfo.OriginalScheduledPickupDateTime = getAttributeValue(load.ScheduledPickupDateTime, false, null);                   
                    loadInfo.ActualPickupDateTime = getAttributeValue(load.ActualPickupDateTime, true, timeOffset);
                    loadInfo.OriginalActualPickupDateTime = getAttributeValue(load.ActualPickupDateTime, false, null);
                    loadInfo.NearestGPSLocation = CC_KochUtilities.cleanNearestGPSLocation(getAttributeValue(load.NearestGPSLocation, false, timeOffset));
                    loadInfo.Latitude = load.Latitude;
                    loadInfo.Longitude = load.Longitude;
                    loadInfo.ScheduledDeliveryDateTime = getAttributeValue(load.ScheduledDeliveryDateTime, true, timeOffset);
                    loadInfo.OriginalScheduledDeliveryDateTime = getAttributeValue(load.ScheduledDeliveryDateTime, false, null);
                    loadInfo.ActualDeliveryDateTime = getAttributeValue(load.ActualDeliveryDateTime, true, timeOffset);
                    loadInfo.OriginalActualDeliveryDateTime = getAttributeValue(load.ActualDeliveryDateTime, false, null);
                    loadInfo.LatestCalculatedDeliveryDate = getAttributeValue(load.LatestCalculatedDeliveryDate, true, timeOffset);
                    loadInfo.OriginalLatestCalculatedDeliveryDate = getAttributeValue(load.LatestCalculatedDeliveryDate, false, null);
                    loadInfo.TimeOffset = load.TimeOffset;
                    loadInfo.LastUpdate = getAttributeValue(load.UTCLastUpdate, true, timeOffset);
                    loadInfo.OriginalLastUpdate = getAttributeValue(load.UTCLastUpdate, false, null);
                    loadInfo.LoadID = getAttributeValue(load.LoadID, false, timeOffset);   

                    DateTime scheduledPickupDate = CC_KochUtilities.getDateTimeFromKochData(load.ScheduledPickupDateTime);

                    CC_Car_Item__c carItem = carItemKeyMap.get(CC_KochUtilities.getKochCarItemUniqueKey(load.OrdRefnum));                    
                    system.debug('carItem: ' + carItem);

                    Date estimatedShipWeek;

                    if (carItem != null){

                        loadInfo.kochLoadDataMatch = true;
                        loadInfo.carItemId = carItem.Id;
                        loadInfo.LoadCode = carItem.Load_Code__c;
                        loadInfo.SerialNumbers = getAttributeValue(carItem.Serial_No_s__c, false, null);
                        loadInfo.CarName = getAttributeValue(carItem.Order_Item__r.Product__r.Name, false, null);
                        loadInfo.BackOrderDetails = getAttributeValue(carItem.Accessory_Backorder_Details__c, false, null);    
                        estimatedShipWeek = carItem.Estimated_Ship_Date__c;                                                            
                    }   
                    
                    loadInfo.EstimatedShipWeek = getAttributeValue((estimatedShipWeek != null ? estimatedShipWeek.format() : null), false, null);
                    
                    response.loads.add(loadInfo);               
                }
                else if (load.Error != CC_LoadTrackingUtilities.KOCH_LOAD_NOT_FOUND_MSG){

                    response.success = false;
                    response.errorMessage = load.Error;
                }   
            }
            
            if (response.loads.size() > 0){

                response.kochLoadDataFound = true;
                updateCarItemsFromKoch(response.loads);
            }
        }

        // Return MAPICS info since no Koch load info was found
        if (!response.kochLoadDataFound){

            for (CC_Car_Item__c carItem : carItems){

                LoadInfo loadInfo = new LoadInfo(); 

                CC_Order_Shipment__c shipment = shipmentHeaderMap.get(carItem.Shipment_Header_Number__c);
                loadInfo.Carrier = getAttributeValue(CC_LoadTrackingUtilities.getCarrier(carItem, order, shipment), false, null);                
                loadInfo.isPortalUser = response.isPortalUser;
                loadInfo.StpStatus = carItem.Delivery_Date_Status__c.toLowerCase().capitalize();                                                                      
                loadInfo.kochLoadDataMatch = false;
                loadInfo.carItemId = carItem.Id;   
                loadInfo.LoadCode = carItem.Load_Code__c;              
                loadInfo.OrderNumber = order.Order_Number__c;            
                loadInfo.CmpName = getAttributeValue(order.CC_Addressee_name__c, false, null);
                loadInfo.StpAddress = getAttributeValue(order.CC_Address_line_1__c, false, null);

                if (String.isNotBlank(order.CC_Address_line_2__c)){
                    loadInfo.StpAddress += '/n' + getAttributeValue(order.CC_Address_line_2__c, false, null);
                }

                if (String.isNotBlank(order.CC_Address_line_3__c)){
                    loadInfo.StpAddress += '/n' + getAttributeValue(order.CC_Address_line_3__c, false, null);
                }                

                loadInfo.CtyName = getAttributeValue(order.CC_City__c, false, null);                
                loadInfo.Quantity = getAttributeValue(String.valueOf(carItem.Truck_Quantity__c), false, null);
                loadInfo.ScheduledPickupDateTime = String.isNotBlank(carItem.Scheduled_Ship_Date_Display__c) ? carItem.Scheduled_Ship_Date_Display__c : CC_KochUtilities.EMPTY_VALUE;
                loadInfo.OriginalScheduledPickupDateTime = loadInfo.ScheduledPickupDateTime;
                loadInfo.ActualPickupDateTime = getAttributeValue((carItem.Ship_Date__c != null ? carItem.Ship_Date__c.format() : ''), false, null);
                loadInfo.OriginalActualPickupDateTime = loadInfo.ActualPickupDateTime;
                loadInfo.ScheduledDeliveryDateTime = String.isNotBlank(carItem.Delivery_Date_Display__c) ? carItem.Delivery_Date_Display__c : CC_KochUtilities.EMPTY_VALUE;
                loadInfo.OriginalScheduledDeliveryDateTime = loadInfo.ScheduledDeliveryDateTime;
                loadInfo.ActualDeliveryDateTime = CC_KochUtilities.EMPTY_VALUE;
                loadInfo.OriginalActualDeliveryDateTime = loadInfo.ActualDeliveryDateTime;
                loadInfo.LoadID = getAttributeValue(carItem.Load_Code__c, false, null); 
                loadInfo.LastUpdate = getAttributeValue(carItem.LastModifiedDate.format('MM/dd/yyyy hh:mm') + ' GMT', false, null);
                loadInfo.OriginalLastUpdate = carItem.LastModifiedDate.format('MM/dd/yyyy hh:mm');
                loadInfo.SerialNumbers = getAttributeValue(carItem.Serial_No_s__c, false, null);
                loadInfo.CarName = getAttributeValue(carItem.Order_Item__r.Product__r.Name, false, null);
                loadInfo.BackOrderDetails = getAttributeValue(carItem.Accessory_Backorder_Details__c, false, null);                
                loadInfo.EstimatedShipWeek = getAttributeValue((carItem.Estimated_Ship_Date__c != null ? carItem.Estimated_Ship_Date__c.format() : null), false, null);                

                response.loads.add(loadInfo);  
            }
        }

        return response;
    }

    public static String getAttributeValue(String val, Boolean isDateTime, Decimal timeOffset){

        if (String.isNotBlank(val) && val != CC_KochUtilities.EMPTY_VALUE){

            // Expecting a GMT date/time string
            if (isDateTime){

                DateTime dt = CC_KochUtilities.getDateTimeFromKochData(val);
                system.debug('parsed gmt dt: ' + dt);

                dt = dt.addHours(timeOffset != null ? Integer.valueOf(timeOffset) : 0);
                system.debug('local dt: ' + dt);

                String dtDisplay = dt.formatGMT('MM/dd/yyyy HH:mm') + (timeOffset == null ? ' GMT' : '');
                system.debug('dt to display: ' + dtDisplay);

                return dtDisplay;
            }
            else{
                return val;
            }
        }
        else{
            return CC_KochUtilities.EMPTY_VALUE;
        }
    }

    private static Set<Id> processedCarItemIds = new Set<Id>();

    private static void updateCarItemsFromKoch(LoadInfo[] loadInfoRecords){
        
        CC_Car_Item__c[] carItemsToUpdate = new CC_Car_Item__c[]{};

        for (LoadInfo li : loadInfoRecords){

            // Prevent duplicate car items in the update if multiple stops were returned for each car item, only use the first stop info which is the most recent
            if (li.kochLoadDataMatch && li.carItemId != null && !processedCarItemIds.contains(li.carItemId)){

                CC_Car_Item__c carItem = new CC_Car_Item__c(Id = li.carItemId);

                // Update Koch fields
                CC_KochUtilities.updateCarItemKochFields(
                    carItem, 
                    li.OriginalLastUpdate, 
                    li.OriginalScheduledPickupDateTime,
                    li.OriginalActualPickupDateTime,                     
                    li.OriginalScheduledDeliveryDateTime, 
                    li.OriginalLatestCalculatedDeliveryDate, 
                    li.Distance,
                    li.OriginalActualDeliveryDateTime,
                    li.TimeOffset,
                    li.KochLoadCode
                );              

                carItemsToUpdate.add(carItem);
                processedCarItemIds.add(carItem.Id);
            }
        }

        system.debug('carItemsToUpdate: ' + carItemsToUpdate);

        if (carItemsToUpdate.size() > 0){
            sObjectService.updateRecordsAndLogErrors(carItemsToUpdate, 'CC_Order_LoadDetailsController', 'updateCarItemsFromKoch');
        }
    } 
}