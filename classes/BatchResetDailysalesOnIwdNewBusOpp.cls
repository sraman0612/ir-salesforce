/*------------------------------------------------------------
Author:       Randhir Kumar
Description:  Batch that will reset the field Today's Claim Build and Yesterday's Claim Build on the IWD New Business Opportunity
------------------------------------------------------------*/
global class BatchResetDailysalesOnIwdNewBusOpp implements Database.Batchable<sObject> { 

    global String query;

    public BatchResetDailysalesOnIwdNewBusOpp(){
        string RECORD_TYPE_IWD_NEW_BUSINESS = 'IWD_New_Business';
        string RECORD_TYPE_NEW_BUSINESS = 'RS_Business';
        Id rshvacDsoRTID = OpportunityUtils.OPP_RECORD_TYPES.get(RECORD_TYPE_NEW_BUSINESS).Id;
        Id rshvacIwdRTID = OpportunityUtils.OPP_RECORD_TYPES.get(RECORD_TYPE_IWD_NEW_BUSINESS).Id;
        // SELECT fields
        query = 'SELECT Id, Todays_Claim_Billed__c, Yesterdays_Claim_Billed__c';
        // FROM object
        query += ' FROM Opportunity';
        // Put condition for fecthing the specific records.
        query += ' WHERE (Todays_Claim_Billed__c != 0 OR Yesterdays_Claim_Billed__c != 0) AND (RecordTypeId = \''+rshvacIwdRTID+'\' OR RecordTypeId = \''+rshvacDsoRTID+'\')';
    } 

    /**
     * Use this constructor just to ran the batch manually to test it (or other purpose)
    */
    public BatchResetDailysalesOnIwdNewBusOpp(String customQuery){
        query = customQuery;
    }
       
    /* start method */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('START BEGIN >>>');
        System.debug('Query >>> ' + query);
        return Database.getQueryLocator(query);
    } 

    /* execute method */
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('EXECUTE BEGIN >>>');
        List<Opportunity> opptyToUpdate = new List<Opportunity>();
        for (SObject sObj : scope){
          Opportunity opp = (Opportunity) sObj;
          // If there was any bill today set it to Yesterday
          // Set 0 otherwise
          opp.Yesterdays_Claim_Billed__c= opp.Todays_Claim_Billed__c!= 0 ? opp.Todays_Claim_Billed__c: 0;
          // Billed today will always be reset to 0
          opp.Todays_Claim_Billed__c= 0;
          opptyToUpdate.add(opp);
        }
        update opptyToUpdate;
    } 

    /* finish method */
    public void finish(Database.BatchableContext BC) {
        System.debug('FINISH BEGIN >>>');
    } 
}