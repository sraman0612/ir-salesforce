@isTest
public class CC_PavilionContentHistoryControllerTest{
    static testMethod void cc_PavilionContentHistory(){
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='Content_Title__c');
            insert cvh;   
         
            CC_PavilionContentHistoryController conHistory = new CC_PavilionContentHistoryController(new ApexPages.StandardController(contentVer));                        
            conHistory.selectedPage='CC_Pavilion';           
            conHistory.pageNumber=-1;           
            conHistory.noOfPages=20;
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           
            exportContentVersionHistoryController exp = new exportContentVersionHistoryController();
    }
     
    static testMethod void cc_PavilionContentHistory1(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;           
            CC_PavilionContentHistoryController conHistory = new CC_PavilionContentHistoryController(new ApexPages.StandardController(contentVer));                       
            conHistory.startDate=System.today();
            conHistory.endDate=System.today()+15;           
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           

    }
     static testMethod void cc_PavilionContentHistory2(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;           
            CC_PavilionContentHistoryController conHistory = new CC_PavilionContentHistoryController(new ApexPages.StandardController(contentVer));                       
                  
            conHistory.startDate=System.today();
            conHistory.endDate=Null;         
           
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           

    }
    static testMethod void cc_PavilionContentHistory3(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;            
            CC_PavilionContentHistoryController conHistory = new CC_PavilionContentHistoryController(new ApexPages.StandardController(contentVer));
            conHistory.startDate=Null;
            conHistory.endDate=Null;
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           

    }
    static testMethod void cc_PavilionContentHistory4(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;            
            CC_PavilionContentHistoryController conHistory = new CC_PavilionContentHistoryController(new ApexPages.StandardController(contentVer));
           conHistory.startDate=Null;
            conHistory.endDate=System.today()+15;        
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           
    
 
    }
    static testMethod void cc_PavilionContentHistoryV2(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersionType('bulletin','Parts','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;   
         
            CC_PavilionContentHistoryControllerV2 conHistory = new CC_PavilionContentHistoryControllerV2(new ApexPages.StandardController(contentVer));                        
            conHistory.selectedPage='CC_Pavilion';           
            conHistory.pageNumber=-1;           
            conHistory.noOfPages=20;
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();   
            
            exportContentDownloadHistoryController exp = new exportContentDownloadHistoryController();        
    }
    static testMethod void cc_PavilionContentHistoryV21(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersionType('bulletin','Parts','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;           
            CC_PavilionContentHistoryControllerV2 conHistory = new CC_PavilionContentHistoryControllerV2(new ApexPages.StandardController(contentVer));                       
            conHistory.startDate=System.today();
            conHistory.endDate=System.today()+15;           
               
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           

    }
    static testMethod void cc_PavilionContentHistoryV23(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;            
            CC_PavilionContentHistoryControllerV2 conHistory = new CC_PavilionContentHistoryControllerV2(new ApexPages.StandardController(contentVer));
            conHistory.startDate=Null;
            conHistory.endDate=Null;
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           

    }
    static testMethod void cc_PavilionContentHistoryV24(){
        
            ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Tech Pubs','Japan','USD','Industrial Utility');
            insert contentVer; 
            ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
            insert cvh;            
            CC_PavilionContentHistoryControllerV2 conHistory = new CC_PavilionContentHistoryControllerV2(new ApexPages.StandardController(contentVer));
           conHistory.startDate=Null;
            conHistory.endDate=System.today()+15;        
            conHistory.getInitialContentVersionHistorySet();
            conHistory.dateRangeFilter();
            conHistory.next();    
            conHistory.previous();           
    
 
    }
}