public without sharing class CC_Manage_Pavilion_Notification_SubCtrl {

    @TestVisible
    private class InitResponse{
        @AuraEnabled public Boolean isPortalUser = false;
        @AuraEnabled public Id currentUserId = UserInfo.getUserId(); 
        @AuraEnabled public String mobilePhone = '';
        @AuraEnabled public User[] users = new User[]{};
        @AuraEnabled public String[] zones = new String[]{}; // from CC_Sales_Rep__c
        @AuraEnabled public String[] profitCenters = new String[]{}; // from CC_Sales_Rep__c
        @AuraEnabled public CC_Order__c order = new CC_Order__c();
        @AuraEnabled public CC_Car_Item__c carItem = new CC_Car_Item__c();
        @AuraEnabled public SubscriptionOption[] subscriptionOptions = new SubscriptionOption[]{};
    }

    @TestVisible
    public class SubscriptionOption{
        @AuraEnabled public Boolean subscribed = false;
        @AuraEnabled public CC_Pavilion_Notification_Subscription__c subscription = new CC_Pavilion_Notification_Subscription__c();
        @AuraEnabled public CC_Pavilion_Notification_Type__c notificationType = new CC_Pavilion_Notification_Type__c();
    }

    @AuraEnabled
    public static InitResponse getInit(String recordId){

        InitResponse response = new InitResponse();

        Id orderId;
        sObjectType objectType;

        if (String.isNotBlank(recordId)){

            objectType = Id.valueOf(recordId).getSObjectType();

            if (objectType == CC_Order__c.getSObjectType()){

                orderId = Id.valueOf(recordId);
                response.order = [Select Id, Order_Number__c From CC_Order__c Where Id = :orderId];
            }
            else if (objectType == CC_Car_Item__c.getSObjectType()){

                response.carItem = [Select Id, Order_Item__r.Order__c, Load_Code__c From CC_Car_Item__c Where Id = :recordId];
                orderId = response.carItem.Order_Item__r.Order__c;
                response.order = [Select Id, Order_Number__c From CC_Order__c Where Id = :orderId];
            }
        }  

        User currentUser = 
            [Select Id, Name, AccountId, IsPortalEnabled, CC_Pavilion_Navigation_Profile__c, MobilePhone, Contact.MobilePhone
            From User 
            Where Id = :UserInfo.getUserId()];

        Id currentUserId = currentUser.Id;
        String navigationProfile = currentUser.CC_Pavilion_Navigation_Profile__c;
        response.isPortalUser = currentUser.IsPortalEnabled;
        response.mobilePhone = CC_TwilioService.getRecipientMobilePhone(currentUser); 

        if (!response.isPortalUser && String.isBlank(recordId)){

            AggregateResult[] profitCenterAgg = [Select CC_Profit_Center__c From CC_Sales_Rep__c Where CC_Profit_Center__c != null Group By CC_Profit_Center__c Order By CC_Profit_Center__c ASC];

            for (AggregateResult agg : profitCenterAgg)  {
                response.profitCenters.add((String)agg.get('CC_Profit_Center__c'));
            }    
            
            AggregateResult[] zoneAgg = [Select CC_Zone__c From CC_Sales_Rep__c Where CC_Zone__c != null Group By CC_Zone__c Order By CC_Zone__c ASC];

            for (AggregateResult agg : zoneAgg)  {
                response.zones.add((String)agg.get('CC_Zone__c'));
            }               
            
            response.users = [Select Id, Name From User Where IsActive = true and Profile.Name like 'CC_%' Order By Name ASC];
        }

        // Get any current subscriptions
        Map<Id, CC_Pavilion_Notification_Subscription__c> subscriptionTypeSubscriptionsMap = new Map<Id, CC_Pavilion_Notification_Subscription__c>();

        String subscriptionQuery = 
            'Select Id, Name, Delivery_Frequencies__c, Delivery_Methods__c, Pavilion_Notification_Type__c, Subscription_Scope_Type__c, Subscription_Scope_Value__c ' +                
            'From CC_Pavilion_Notification_Subscription__c ' + 
            'Where OwnerId = :currentUserId and Pavilion_Notification_Type__r.Active__c = true ';

        if (String.isNotBlank(recordId)){
            
            if (objectType == CC_Order__c.getSObjectType()){
                subscriptionQuery += ' and CC_Order__c = :recordId ';   
            }
            else if (objectType == CC_Car_Item__c.getSObjectType()){
                subscriptionQuery += ' and CC_Car_Item__c = :recordId ';
            }
        }
        else{
            subscriptionQuery += ' and CC_Order__c = null and CC_Car_Item__c = null ' ;
        }

        for (CC_Pavilion_Notification_Subscription__c subscription : Database.query(subscriptionQuery)){
            subscriptionTypeSubscriptionsMap.put(subscription.Pavilion_Notification_Type__c, subscription);
        }

        // Partner user
        if (response.isPortalUser){

            // Only allow new subscriptions based on active notification type assignments to the Pavilion navigation profile
            for (Pavilion_Nav_Profile_Notification_Type__c profileNotificationType : CC_LoadTrackingUtilities.getPortalUserSubscriptionsAvailable()){

                    SubscriptionOption subscriptionOption = new SubscriptionOption();
                    subscriptionOption.notificationType = profileNotificationType.Pavilion_Notification_Type__r;
                    subscriptionOption.subscription = subscriptionTypeSubscriptionsMap.get(profileNotificationType.Pavilion_Notification_Type__c); 
                    subscriptionOption.subscribed = subscriptionOption.subscription != null ? true : false;
                    response.subscriptionOptions.add(subscriptionOption);               
            }
        }
        // Internal user
        else{

            // All active notification types available to internal users
            Map<String, CC_Pavilion_Notification_Type__c[]> notificationsByObject = CC_LoadTrackingUtilities.getInternalUserSubscriptionsAvailable(); 
            CC_Pavilion_Notification_Type__c[] notificationTypesAvailable = new CC_Pavilion_Notification_Type__c[]{};

            // Show all notification types unless it is a car item subscription then filter out any order specific types (i.e. order hold/release) 
            if (objectType != null && objectType == CC_Car_Item__c.getSObjectType()){
                notificationTypesAvailable = notificationsByObject.get(String.valueOf(objectType));
            }
            else{
                
                for (CC_Pavilion_Notification_Type__c[] notificationTypes : notificationsByObject.values()){
                    notificationTypesAvailable.addAll(notificationTypes);
                }
            }

            for (CC_Pavilion_Notification_Type__c notificationType : notificationTypesAvailable){

                    SubscriptionOption subscriptionOption = new SubscriptionOption();
                    subscriptionOption.notificationType = notificationType;
                    subscriptionOption.subscription = subscriptionTypeSubscriptionsMap.get(notificationType.Id); 
                    subscriptionOption.subscribed = subscriptionOption.subscription != null ? true : false;
                    response.subscriptionOptions.add(subscriptionOption);               
            }            
        }

        return response;
    }
}