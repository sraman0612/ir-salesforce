global class CC_PavilionCustomSolContactsController{
  
  public CC_PavilionCustomSolContactsController(CC_PavilionTemplateController controller){}
  
  global static List<User> getUserCustomSolContacts(){
    List<User> marContacts = new List<User>();
    marContacts = [SELECT id,CC_Department__c,Name,Email,Title,Extension,Phone FROM User WHERE CC_Department__c='Custom Solutions'];  
    return marContacts; 
  }
}