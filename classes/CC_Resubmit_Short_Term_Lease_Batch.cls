public without sharing class CC_Resubmit_Short_Term_Lease_Batch implements Database.Batchable<sObject>{
	
	@TestVisible private static String defaultLeaseApprovalStatus = 'Not Yet Submitted';
    
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        // Find all leases that are flagged for approval override
        return Database.getQueryLocator('Select Id From CC_Short_Term_Lease__c LIMIT 1');
   	}    
   	
   	public void execute(Database.BatchableContext BC, List<CC_Short_Term_Lease__c> leases){
   		/*
   		try{
   			
   			Map<Id, CC_Short_Term_Lease__c> leaseMap = new Map<Id, CC_Short_Term_Lease__c>();
   			Map<Id, Id> leaseIdRequestorMap = new Map<Id, Id>();
   			
   			for (CC_Short_Term_Lease__c l : leases){
   				leaseMap.put(l.Id, l);
   				leaseIdRequestorMap.put(l.Id, l.Unlock_Requestor__c);
   			}
   			
   			system.debug('leaseIdRequestorMap: ' + leaseIdRequestorMap);
   			
   			// Unlock the leases
   			List<Approval.UnlockResult> unlockResults = Approval.unlock(leases, false);
   			
   			CC_Short_Term_Lease__c[] successes = new CC_Short_Term_Lease__c[]{};
   			Map<Id, String> failures = new Map<Id, String>();
   			
   			for (Approval.UnlockResult unlockResult : unlockResults){
   				
   				if (unlockResult.isSuccess()){
   					successes.add(new CC_Short_Term_Lease__c(Id = unlockResult.getId()));
   				}
   				else{
   					failures.put(unlockResult.getId(), unlockResult.getErrors()[0].getMessage());
   				}
   			}
   			
   			system.debug('successes: ' + successes);
   			system.debug('failures: ' + failures);

   			if (successes.size() > 0){
   				
   				// Set the status back to Draft and reset the unlock request fields
   				for (CC_Short_Term_Lease__c l : successes){

   					l.Approval_Status__c = defaultLeaseApprovalStatus;
   					l.Unlock_Requested__c = false;
   					l.Unlock_Requestor__c = null;
   				}
   				
   				sObjectService.updateRecordsAndLogErrors(successes, 'CC_Resubmit_Short_Term_Lease_Batch', 'execute');
   			}
   			
			// Build email notifications for successes and failures
			Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
			
			for (CC_Short_Term_Lease__c success : successes){
				
				CC_Short_Term_Lease__c l = leaseMap.get(success.Id);
				system.debug('lease: ' + l);
				
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(l);
				String viewURL = URL.getSalesforceBaseUrl().toExternalForm() + stdCtrl.view().getURL();	
				String emailSubject = 'Lease ' + l.Name + ' successfully unlocked.';
				String emailTextBody = 'The lease has been unlocked and can now be modified and resubmitted for approval.<br/><br/>Here is a link to to view the lease: <a href="' + viewURL + '">' + l.Name + '</a>';		                
                Id requestor = leaseIdRequestorMap.get(l.Id);
                
                system.debug('requestor: ' + requestor);
                system.debug('viewURL: ' + viewURL);
                
                messages.add(EmailService.buildSingleEmailMessage(
                    requestor, 
                    null, 
                    null, 
                    null,
                    emailSubject,
                    null, 
                    emailTextBody, 
                    null,
                    null)
                );		
			}
			
			for (Id leaseId : failures.keySet()){
				
				CC_Short_Term_Lease__c l = leaseMap.get(leaseId);
				system.debug('lease: ' + l);
				
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(l);
				String viewURL = URL.getSalesforceBaseUrl().toExternalForm() + stdCtrl.view().getURL();					
				String emailSubject = 'Lease ' + l.Name + ' failed to unlock.';
				String emailTextBody = 'The lease could not be unlocked due to the following error below.  Please contact your administrator for more details.<br/><br/>' + 'Error: ' + failures.get(leaseId) + '<br/><br/>Here is a link to to view the lease: <a href="' + viewURL + '">' + l.Name + '</a>';		                
                Id requestor = leaseIdRequestorMap.get(l.Id);
                
                system.debug('requestor: ' + requestor);
                system.debug('viewURL: ' + viewURL);
                
                messages.add(EmailService.buildSingleEmailMessage(
                    requestor, 
                    null, 
                    null, 
                    null,
                    emailSubject,
                    null, 
                    emailTextBody, 
                    null,
                    null)
                );		
			}					
			
			EmailService.sendSingleEmailMessages(messages);  			
   		}
   		catch(Exception e){
   			system.debug('error: ' + e.getMessage());
   			ApexLogHandler logHandler = new ApexLogHandler('CC_Resubmit_Short_Term_Lease_Batch', 'execute', e.getMessage());
   			insert logHandler.logObj;
   		}*/
   	}
   	
	public void finish(Database.BatchableContext BC){
		
   	}
}