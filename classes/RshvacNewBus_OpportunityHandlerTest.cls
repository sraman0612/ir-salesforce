@isTest
public class RshvacNewBus_OpportunityHandlerTest{
  
  public static String DSO = 'DSO';
  public static String IWD = 'IWD';
  public static String DSO_ACCOUNT = 'DSO Account';
  public static String IWD_ACCOUNT = 'IWD Account';
  public static String DSO_COST_CENTER = 'DSO COST CENTER';
  public static String IWD_COST_CENTER = 'IWD COST CENTER';
  public static final String POTENTIAL = 'Potential';
  public static final String DEALER = 'Dealer';
  public static final String IWD_OPPORTUNITY_NAME = 'IWD New Business Opportunity';
  public static final String DSO_OPPORTUNITY_NAME = 'DSO New Business Opportunity';
  public static final String IWD_OPPORTUNITY_NAME_SETUP = 'IWD New Business Opportunity Setup';
  public static final String DSO_OPPORTUNITY_NAME_SETUP = 'DSO New Business Opportunity Setup';
  
  // Getting Project Type values for BDM, MDM and RM
  public static final string SINGLE_FAMILY_NC = 'Single Family - NC';
  public static final string MULTI_FAMILY_NC = 'Multi-Family - NC';
  public static final string MULTI_FAMILY_REPLACEMENT = 'Multi-Family - Replacement';
  public static final string SINGLE_FAMILY_NOO = 'Single Family Non Owner Occupied';
  public static final string COMMERCIAL = 'Commercial Replacement';
  
  public Static final Integer maxGrossSales = 2000;
  public static final Integer minGrossSales = 1000;

  // IWD New Business record type developer name
  public static final String IWD_NEW_BUSINESS_RT_NAME = 'IWD_New_Business';
  // DSO New Business record type developer name
  public static final String DSO_NEW_BUSINESS_RT_NAME = 'RS_Business';
  // IWD New Business record type Id
  public static Id DSO_NEW_BUSINESS_RT_ID = OpportunityUtils.OPP_RECORD_TYPES.get(DSO_NEW_BUSINESS_RT_NAME).Id;
  // DSO New Business record type Id
  public static Id IWD_NEW_BUSINESS_RT_ID = OpportunityUtils.OPP_RECORD_TYPES.get(IWD_NEW_BUSINESS_RT_NAME).Id;
  // Get the Id of the Opportunity Existing BUsiness Record Type
  public static Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;
  // Name of API User
  public static String API_User = 'RHVAC Integration';
  // Name of BDM_MDM User
  public static String BDM_MDM_USER = 'BDM MDM User';
  // Developer Name of BDMMDM_PROFILE
  public static String BDMMDM_PROFILE_NAME = 'RSHVAC IWD RM/BDM/MDM'; 
  //Name of one of the BDM User Roles
  public static String RSHVAC_NOO_BDM_ROLE_NAME = 'RSHVAC NOO BDM';
  // Id of API Profile
  public static String BDMMDM_PROFILE_ID;
  // Developer Name of RSHVAC Profile
  public static String API_PROFILE_NAME = 'API_Profile';
  //Id of RSHVAC Profile
  public static String API_PROFILE_ID;
  //Id of RSHVAC NOO BDM role
  public static String RSHVAC_NOO_BDM_ROLE_ID;
  // Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
  // Id of the RS_HVAC Record type
  public static Id RS_HVAC_RT_ID;
  
  static{
    RS_HVAC_RT_ID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    BDMMDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = :BDMMDM_PROFILE_NAME].Id;
    API_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = :API_PROFILE_NAME].Id;
    RSHVAC_NOO_BDM_ROLE_ID = [select Id from userrole where name = :RSHVAC_NOO_BDM_ROLE_NAME].Id;
  }
  
  @testSetup 
  static void mySetup() {
    List<Residential_Quota__c> residentialQuotaList = new List<Residential_Quota__c>();
    List<Cost_Center__c> costCenterList = new List<Cost_Center__c>();
    List<Account> accountList = new List<Account>();
    Cost_Center__c dsoCostCenter;
    Cost_Center__c iwdCostCenter;
    List<User> resQuotaUsers = new List<User>();
    
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    LocaleSidKey='en_US', ProfileId = p.Id, 
    TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
    insert u;
        
    System.runAs(u){
        
      for(Integer i=0;i<6;i++){
        User testUser = new User(LastName = 'testingUser'+i, profileId=BDMMDM_PROFILE_ID, Username='aaatesting'+i+'@example.com', Email='aaa@example.com',
                                  Alias = 'abc'+i, CommunityNickname='abc'+i, TimeZoneSidKey = 'GMT', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US', UserRoleId = RSHVAC_NOO_BDM_ROLE_ID);
        resQuotaUsers.add(testUser); 
      }
      insert resQuotaUsers;
      for(Integer i=0;i<6;i++){
        residentialQuotaList.add(new Residential_Quota__c(User__c = resQuotaUsers.get(i).Id, Forecast_Month__c = Date.Today() ));
        residentialQuotaList.add(new Residential_Quota__c(User__c = resQuotaUsers.get(i).Id, Forecast_Month__c = Date.newInstance(Date.Today().year(),Date.Today().month()-1,1) ));
       }
      insert residentialQuotaList;
      costCenterList.add(new Cost_Center__c(Name = DSO_COST_CENTER, Default_Branch__c = DSO_COST_CENTER, Channel__c = DSO, BDM__c = resQuotaUsers.get(0).Id, MDM__c = resQuotaUsers.get(1).Id, OwnerId = resQuotaUsers.get(2).Id, Create_Existing_Business_Opportunity__c=False));
      costCenterList.add(new Cost_Center__c(Name = IWD_COST_CENTER, Default_Branch__c = IWD_COST_CENTER, Channel__c = IWD, BDM__c = resQuotaUsers.get(3).Id, MDM__c = resQuotaUsers.get(4).Id, OwnerId = resQuotaUsers.get(5).Id, Create_Existing_Business_Opportunity__c=True));
      insert costCenterList;
    
      accountList.add(new Account(Name = DSO_ACCOUNT, Default_Branch__c = DSO_COST_CENTER , Status__c = POTENTIAL, Type = DEALER, RecordTypeId = RS_HVAC_RT_ID ));
      accountList.add(new Account(Name = IWD_ACCOUNT, Default_Branch__c = IWD_COST_CENTER , Status__c = POTENTIAL, Type = DEALER, RecordTypeId = RS_HVAC_RT_ID));
      insert accountList;
    
      RSHVAC_Properties__c IntegrationUser = new RSHVAC_Properties__c(Name='Integration User', Value__c='RHVAC Integration');
      insert IntegrationUser;
    
      Account iwdAccount = new Account();
      iwdAccount = [select id, Cost_Center_Name__c from Account where Name = :IWD_ACCOUNT];
      Account dsoAccount = new Account();
      dsoAccount = [select id, Cost_Center_Name__c from Account where Name = :DSO_ACCOUNT];
    
   // User apiUser = new User();
   // apiUser = [select id,name from User where name like 'testingUser1'];
   // System.runAs(apiUser){
      List<Opportunity> newOppties = new List<Opportunity>();
      newOppties.add(new Opportunity(Name = IWD_OPPORTUNITY_NAME_SETUP, AccountId = iwdAccount.Id, RecordTypeId = IWD_NEW_BUSINESS_RT_ID, Type = SINGLE_FAMILY_NC, StageName = 'Request', CloseDate = System.Today()));
      newOppties.add(new Opportunity(Name = DSO_OPPORTUNITY_NAME_SETUP, AccountId = dsoAccount.Id, RecordTypeId = DSO_NEW_BUSINESS_RT_ID, Type = SINGLE_FAMILY_NC, StageName = 'Request', CloseDate = System.Today()));  
      insert newOppties;  
    }  
  }

  public testMethod static void iwdInsertTest(){
    List<Residential_Quota__c> quotaList = new List<Residential_Quota__c>();
    List<Opportunity_Revenue_Schedule__c> revenueList = new List<Opportunity_Revenue_Schedule__c>();
    Account iwdAccount = new Account();
    iwdAccount = [select id, Cost_Center_Name__c from Account where Name = :IWD_ACCOUNT];
    Cost_Center__c costCenter = new Cost_Center__c();
    costCenter = [select id, Bdm__c, MDM__c, OwnerId from Cost_Center__c where id = :iwdAccount.Cost_Center_Name__c];
    Opportunity newIwdBusinessOppty = new Opportunity(Name = IWD_OPPORTUNITY_NAME, AccountId = iwdAccount.Id, RecordTypeId = IWD_NEW_BUSINESS_RT_ID, Type = SINGLE_FAMILY_NC, Gross_Sales__c = minGrossSales, StageName = 'Request', CloseDate = System.Today());
    User apiUser = new User();
    apiUser = [select id from User where name like :API_User];
    Test.startTest();
    System.runAs(apiUser){
      insert newIwdBusinessOppty;
      newIwdBusinessOppty = [select id, OwnerId, Todays_Claim_Billed__c,Gross_Sales__c from Opportunity where Id = :newIwdBusinessOppty.Id];
      System.assertEquals(newIwdBusinessOppty.ownerId, costCenter.BDM__c);
      // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
      // being displayed on a Tableau report now.  RNC and NOO segment sales are also being tracked on Tableau report.
      //System.assertEquals(newIwdBusinessOppty.Gross_Sales__c, newIwdBusinessOppty.Todays_Claim_Billed__c);
      //revenueList = [Select id, Single_Family_NC_Actual__c from Opportunity_Revenue_Schedule__c where Opportunity__r.Cost_Center_Name__r.Default_Branch__c = :IWD_COST_CENTER AND Forecast_Month__c = THIS_MONTH];
      //System.assertEquals(revenueList[0].Single_Family_NC_Actual__c, newIwdBusinessOppty.Todays_Claim_Billed__c);
      //quotaList = [Select Single_Family_NC_Actual_IWD__c from Residential_Quota__c where user__c = :costCenter.BDM__c AND Forecast_Month__c = THIS_MONTH];
      //System.assertEquals(quotaList[0].Single_Family_NC_Actual_IWD__c, newIwdBusinessOppty.Todays_Claim_Billed__c);
      }
    Test stopTest;    
  } 
  
   public testMethod static void iwdUpdateTest(){
    List<Residential_Quota__c> quotaList = new List<Residential_Quota__c>();
    List<Opportunity_Revenue_Schedule__c> revenueList = new List<Opportunity_Revenue_Schedule__c>();
    Account iwdAccount = new Account();
    iwdAccount = [select id, Cost_Center_Name__c from Account where Name = :IWD_ACCOUNT];
    Cost_Center__c costCenter = new Cost_Center__c();
    costCenter = [select id, Bdm__c, MDM__c, OwnerId from Cost_Center__c where id = :iwdAccount.Cost_Center_Name__c];
    User apiUser = new User();
    apiUser = [select id from User where name like :API_User];
    Opportunity newIwdBusinessOppty = [select id, OwnerId, Todays_Claim_Billed__c,Gross_Sales__c from Opportunity where Name = :IWD_OPPORTUNITY_NAME_SETUP];
    Test.startTest();
    System.runAs(apiUser){
      newIwdBusinessOppty.Type = MULTI_FAMILY_REPLACEMENT;
      newIwdBusinessOppty.Gross_Sales__c = maxGrossSales;
      update newIwdBusinessOppty;
      newIwdBusinessOppty = [select id, Todays_Claim_Billed__c, ownerId,Gross_Sales__c from Opportunity where Id = :newIwdBusinessOppty.Id];
      System.assertEquals(newIwdBusinessOppty.ownerId, costCenter.MDM__c);
      // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
      // being displayed on a Tableau report now. RNC and NOO segment sales are also being tracked on Tableau report.
      //System.assertEquals(newIwdBusinessOppty.Gross_Sales__c, newIwdBusinessOppty.Todays_Claim_Billed__c);
      //revenueList = [Select id, Multi_Family_Replacement_Actual__c from Opportunity_Revenue_Schedule__c where Opportunity__r.Cost_Center_Name__r.Default_Branch__c = :IWD_COST_CENTER AND Forecast_Month__c = THIS_MONTH];
      //System.assertEquals(revenueList[0].Multi_Family_Replacement_Actual__c, maxGrossSales);
      //quotaList = [Select Multi_Family_Replacement_Actual_IWD__c from Residential_Quota__c where user__c = :costCenter.MDM__c AND Forecast_Month__c = THIS_MONTH];
      //System.assertEquals(quotaList[0].Multi_Family_Replacement_Actual_IWD__c, maxGrossSales);
    }
    Test stopTest; 
  } 
  
  public testMethod static void dsoInsertTest(){
    List<Residential_Quota__c> quotaList = new List<Residential_Quota__c>();
    List<Opportunity_Revenue_Schedule__c> revenueList = new List<Opportunity_Revenue_Schedule__c>();
    Integer monthOfYesterday = Date.Today().Month()-1;
    Integer monthOfRevenueScheduleUpdate;
    if(Date.Today().day() == 1){
      monthOfRevenueScheduleUpdate = monthOfYesterday;
    }
    else
    {
      monthOfRevenueScheduleUpdate = Date.today().month();
    }
    Account dsoAccount = new Account();
    dsoAccount = [select id, Cost_Center_Name__c from Account where Name = :DSO_ACCOUNT];
    Cost_Center__c costCenter = new Cost_Center__c();
    costCenter = [select id, Bdm__c, MDM__c, OwnerId from Cost_Center__c where id = :dsoAccount.Cost_Center_Name__c];
    Opportunity newDsoBusinessOppty = new Opportunity(Name = IWD_OPPORTUNITY_NAME, AccountId = dsoAccount.Id, RecordTypeId = DSO_NEW_BUSINESS_RT_ID, Type = SINGLE_FAMILY_NC, Gross_Sales__c = minGrossSales, StageName = 'Request', CloseDate = System.Today());
    User apiUser = new User();
    apiUser = [select id from User where name like :API_User];
    Test.startTest();
    System.runAs(apiUser){
      insert newDsoBusinessOppty;
      newDsoBusinessOppty = [select id, OwnerId, Todays_Claim_Billed__c,Gross_Sales__c from Opportunity where Id = :newDsoBusinessOppty.Id];
      System.assertEquals(newDsoBusinessOppty.ownerId, costCenter.BDM__c);
      // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
      // being displayed on a Tableau report now.  RNC and NOO segment sales are also being tracked on Tableau report.
      //System.assertEquals(newDsoBusinessOppty.Gross_Sales__c, newDsoBusinessOppty.Todays_Claim_Billed__c);
      //revenueList = [Select id, Single_Family_NC_Actual__c from Opportunity_Revenue_Schedule__c where Opportunity__r.Account.Name = :DSO_ACCOUNT AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate];
      //System.assertEquals(revenueList[0].Single_Family_NC_Actual__c, newDsoBusinessOppty.Todays_Claim_Billed__c);
      //quotaList = [Select Single_Family_NC_Actual_DSO__c from Residential_Quota__c where user__c = :costCenter.BDM__c AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate];
      //System.assertEquals(quotaList[0].Single_Family_NC_Actual_DSO__c, newDsoBusinessOppty.Todays_Claim_Billed__c);
    }
  }
  
  public testMethod static void dsoUpdateTest(){
    List<Residential_Quota__c> quotaList = new List<Residential_Quota__c>();
    List<Opportunity_Revenue_Schedule__c> revenueList = new List<Opportunity_Revenue_Schedule__c>();
    Integer monthOfYesterday = Date.Today().Month()-1;
    Integer monthOfRevenueScheduleUpdate;
    if(Date.Today().day() == 1){
      monthOfRevenueScheduleUpdate = monthOfYesterday;
    }
    else
    {
      monthOfRevenueScheduleUpdate = Date.today().month();
    }
    Account dsoAccount = new Account();
    dsoAccount = [select id, Cost_Center_Name__c from Account where Name = :DSO_ACCOUNT];
    Cost_Center__c costCenter = new Cost_Center__c();
    costCenter = [select id, Bdm__c, MDM__c, OwnerId from Cost_Center__c where id = :dsoAccount.Cost_Center_Name__c];
    User apiUser = new User();
    apiUser = [select id from User where name like :API_User];
    Opportunity newDsoBusinessOppty = [select id, Todays_Claim_Billed__c, ownerId,Gross_Sales__c from Opportunity where Name = :DSO_OPPORTUNITY_NAME_SETUP];
    Test.startTest();
    System.runAs(apiUser){
      newDsoBusinessOppty.Type = MULTI_FAMILY_REPLACEMENT;
      newDsoBusinessOppty.Gross_Sales__c = maxGrossSales;
      update newDsoBusinessOppty;
      newDsoBusinessOppty = [select id, Todays_Claim_Billed__c, ownerId,Gross_Sales__c from Opportunity where Id = :newDsoBusinessOppty.Id];
      System.assertEquals(newDsoBusinessOppty.ownerId, costCenter.MDM__c);
      // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
      // being displayed on a Tableau report now.  RNC and NOO segment sales are also being tracked on Tableau report.
      //System.assertEquals(newDsoBusinessOppty.Gross_Sales__c, newDsoBusinessOppty.Todays_Claim_Billed__c);
      //revenueList = [Select id, Multi_Family_Replacement_Actual__c from Opportunity_Revenue_Schedule__c where Opportunity__r.Account.Name = :DSO_ACCOUNT AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate];
      //System.assertEquals(revenueList[0].Multi_Family_Replacement_Actual__c, maxGrossSales);
      //quotaList = [Select Multi_Family_Replacement_Actual_DSO__c from Residential_Quota__c where user__c = :costCenter.MDM__c AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate];
      //System.assertEquals(quotaList[0].Multi_Family_Replacement_Actual_DSO__c, maxGrossSales);
      Test stopTest;    
    }
  }
  public testMethod static void commercialOwnerUpdateTest(){
    List<Residential_Quota__c> quotaList = new List<Residential_Quota__c>();
    List<Opportunity_Revenue_Schedule__c> revenueList = new List<Opportunity_Revenue_Schedule__c>();
    Account iwdAccount = new Account();
    iwdAccount = [select id, Cost_Center_Name__c from Account where Name = :IWD_ACCOUNT];
    Cost_Center__c costCenter = new Cost_Center__c();
    costCenter = [select id, Bdm__c, MDM__c, OwnerId from Cost_Center__c where id = :iwdAccount.Cost_Center_Name__c];
    Opportunity newIwdBusinessOppty = new Opportunity(Name = IWD_OPPORTUNITY_NAME, AccountId = iwdAccount.Id, RecordTypeId = IWD_NEW_BUSINESS_RT_ID, Type = COMMERCIAL, Gross_Sales__c = minGrossSales, StageName = 'Request', CloseDate = System.Today());
    User apiUser = new User();
    apiUser = [select id from User where name like :API_User];
    Test.startTest();
    System.runAs(apiUser){
      insert newIwdBusinessOppty;
      newIwdBusinessOppty = [select id, OwnerId from Opportunity where id = :newIwdBusinessOppty.id];
      System.assertEquals(newIwdBusinessOppty.OwnerId, costCenter.OwnerId);
    }
    Test.stopTest();
  } 
  
}