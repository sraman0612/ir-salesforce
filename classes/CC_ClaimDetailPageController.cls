public class CC_ClaimDetailPageController {
    
    public CC_Coop_Claims__c claim {get; set;}
    public string accid;
    
    public CC_ClaimDetailPageController(CC_PavilionTemplateController controller) {
      
      accid = Controller.acctid;
      
      claim = [SELECT Id, Name, RecordTypeId, CC_Status__c, CC_Submit_Date__c, Advertising_Type__c, Vendor_Adv_Name__c, Invoice_date__c, 
                Number_of_competitive_brands__c, Invoice_amount__c, New_Claim_Amount__c, Global_Region__c, Co_Op_Account_Summary__c, 
                CC_Customer_Number__c, CC_Agreement_Type__c, CC_Customer_Name__c, Comment__c, TearSheet_of_Advertisment__c,
                Copy_Of_Invoice__c, Club_Car_Logo_in_Add__c FROM CC_Coop_Claims__c 
                      WHERE Co_Op_Account_Summary__r.Contract__r.Account.id =: accid AND 
                      Id = : ApexPages.currentPage().getParameters().get('Id')];
    }
    
}