global with sharing class CC_FleetAnalysisCntrl {
    
    public CC_Fleet_Analysis__c fleetData{get;set;}
    public Id fleet_Id{get;set;}
    public List<CC_Asset_Analysis__c> listofAnalysis{get;set;}
    public Map<id,CC_Asset_Analysis__c> mapofAnalysis{get;set;}
    public Boolean flagForAssetAnalysis{get;set;}
    public String selectedAssetId{get;set;}
    public CC_Asset_Analysis__c AssetAnalysisDetail{get;set;}
    public boolean toShowSaveAnalysis{get;set;}
    public boolean toShowMinimumRequirement{get;set;}
    public ContentVersion cont{get;set;}
    public String attachName{get;set;}
    public String attachType{get;set;}
    public String attachBody{get;set;}
    public String assetCustomerComment{get;set;}
    public String assetInternalComment{get;set;}
    public Map<String,String> mapOfAssetNameVsValue{get;set;}
    public static String bodyOfAttachment;
    
    public CC_FleetAnalysisCntrl(ApexPages.StandardController controller){
         fleet_Id = (Id)controller.getRecord().id;
         flagForAssetAnalysis = false;
         selectedAssetId = '';
         attachName = '';
         attachType ='';
         attachBody = '';
         assetCustomerComment = '';
         assetInternalComment = '';
         listofAnalysis = new List<CC_Asset_Analysis__c>();
         mapofAnalysis = new  Map<id,CC_Asset_Analysis__c>();
         toShowMinimumRequirement = false;
         toShowSaveAnalysis = false;
         cont = new ContentVersion();
         bodyOfAttachment = '';
         mapOfAssetNameVsValue = new Map<String,String>();
         
         System.debug('@@@@ the Fleet Ids : '+fleet_Id);
         fleetData = [select id,Name,Status__c,Account__c,Account__r.Name,Quantity__c,Asset_Group_Quantity__c,Asset__c,Asset__r.Name,
                             Number_of_Assets_Analyzed__c,Fleet_Type__c,Required_Number_of_Asset_to_Analyze__c,Owner.Name,Minimum_Requirement_Reason__c,
                             Analysis_Date__c,Minimum_Requirement_Reason_Other__c, Comments__c, 
                             Service_Maintenance_records_available__c,Asset__r.Product__r.M2__c,Asset__r.Product__r.P3__c
                        from CC_Fleet_Analysis__c 
                       where Id =:fleet_Id 
                       Limit 1];
         if(fleetData.Number_of_Assets_Analyzed__c < fleetData.Required_Number_of_Asset_to_Analyze__c){
            toShowMinimumRequirement = true;
         }
         
         for(CC_Asset_Analysis__c asset_Ana : [select id,Fleet_Analysis__r.Name,Name,Decal_Number__c,Serial_Number__c,Model__r.Name, Status_formula__c, 
                                                      Status__c
                                                 from CC_Asset_Analysis__c 
                                                Where Fleet_Analysis__r.id = :fleet_Id 
                                             Order by Decal_Number__c Asc 
                                                Limit 1000]){
            mapofAnalysis.put(asset_Ana.id,asset_Ana);
         }
         listofAnalysis.addAll(mapofAnalysis.values());
    }
    
    public void AssetDetails(){
        flagForAssetAnalysis = true;
        System.debug('@@@@@ the Selected Asset id :'+selectedAssetId);
        AssetAnalysisDetail = [select id,Name,Model__r.Name,Decal_Number__c,Serial_Number__c,Status__c,Body_Cosmetics__c,Seats_Cosmetics__c,
                                      X360_Bumper_Cosmetics__c,Canopy_Cosmetics__c,SAM_Cosmetics__c,Tires_Tread_Condition__c,Steering_Basic_Operation__c,
                                      Accelerator_Basic_Operation__c,Brakes_Basic_Operation__c,Batteries_Water_Levels__c,Oil_Level_Gas__c,
                                      Batteries_Engine_Cleanliness__c,Energy_Units__c,Internal_Comments__c,Customer_Comments__c, Front_End_Alignment__c, 
                                      Tread_Condition__c  
                                 from CC_Asset_Analysis__c 
                                where id=:selectedAssetId 
                                Limit 1 ];
        mapOfAssetNameVsValue.put('Body_Cosmetics__c',String.valueOf(AssetAnalysisDetail.Body_Cosmetics__c));
        mapOfAssetNameVsValue.put('Seats_Cosmetics__c',String.valueOf(AssetAnalysisDetail.Seats_Cosmetics__c));
        mapOfAssetNameVsValue.put('X360_Bumper_Cosmetics__c',String.valueOf(AssetAnalysisDetail.X360_Bumper_Cosmetics__c));
        mapOfAssetNameVsValue.put('Canopy_Cosmetics__c',String.valueOf(AssetAnalysisDetail.Canopy_Cosmetics__c));
        mapOfAssetNameVsValue.put('SAM_Cosmetics__c',String.valueOf(AssetAnalysisDetail.SAM_Cosmetics__c));
        mapOfAssetNameVsValue.put('Front_End_Alignment__c',String.valueOf(AssetAnalysisDetail.Front_End_Alignment__c));
        mapOfAssetNameVsValue.put('Tires_Tread_Condition__c',String.valueOf(AssetAnalysisDetail.Tires_Tread_Condition__c));
        mapOfAssetNameVsValue.put('Tread_Condition__c',String.valueOf(AssetAnalysisDetail.Tread_Condition__c));
        mapOfAssetNameVsValue.put('Steering_Basic_Operation__c',String.valueOf(AssetAnalysisDetail.Steering_Basic_Operation__c));
        mapOfAssetNameVsValue.put('Accelerator_Basic_Operation__c',String.valueOf(AssetAnalysisDetail.Accelerator_Basic_Operation__c));
        mapOfAssetNameVsValue.put('Brakes_Basic_Operation__c',String.valueOf(AssetAnalysisDetail.Brakes_Basic_Operation__c));
        mapOfAssetNameVsValue.put('Batteries_Water_Levels__c',String.valueOf(AssetAnalysisDetail.Batteries_Water_Levels__c));
        mapOfAssetNameVsValue.put('Oil_Level_Gas__c',String.valueOf(AssetAnalysisDetail.Oil_Level_Gas__c));
        mapOfAssetNameVsValue.put('Batteries_Engine_Cleanliness__c',String.valueOf(AssetAnalysisDetail.Batteries_Engine_Cleanliness__c));
        if(AssetAnalysisDetail.Energy_Units__c!=null && AssetAnalysisDetail.Energy_Units__c > 0){
            mapOfAssetNameVsValue.put('Energy_Units__c',String.valueOf(true));
        }else{
            mapOfAssetNameVsValue.put('Energy_Units__c',String.valueOf(false));
        }
        tocheckSaveAnalysis();
    }
    public void tocheckSaveAnalysis(){
        System.debug('@@@@@ the AssetAnalysisDetail.Energy_Units__c  :'+AssetAnalysisDetail );
        if(AssetAnalysisDetail.Body_Cosmetics__c != null && 
           AssetAnalysisDetail.Seats_Cosmetics__c != null && 
           AssetAnalysisDetail.X360_Bumper_Cosmetics__c != null && 
           AssetAnalysisDetail.Canopy_Cosmetics__c != null && 
           AssetAnalysisDetail.SAM_Cosmetics__c != null && 
           AssetAnalysisDetail.Front_End_Alignment__c != null && 
           AssetAnalysisDetail.Tires_Tread_Condition__c != null && 
           AssetAnalysisDetail.Tread_Condition__c != null && 
           AssetAnalysisDetail.Steering_Basic_Operation__c != null && 
           AssetAnalysisDetail.Accelerator_Basic_Operation__c != null && 
           AssetAnalysisDetail.Brakes_Basic_Operation__c != null && 
           AssetAnalysisDetail.Batteries_Water_Levels__c != null && 
           AssetAnalysisDetail.Oil_Level_Gas__c != null && 
           AssetAnalysisDetail.Batteries_Engine_Cleanliness__c != null  && 
           AssetAnalysisDetail.Energy_Units__c != null  && 
           AssetAnalysisDetail.Energy_Units__c >0 ){
            toShowSaveAnalysis = true;
        }else{
            toShowSaveAnalysis = false;
        }
        System.debug('@@@@@ the toShowSaveAnalysis :'+toShowSaveAnalysis );
    }
    public void hideAssetAnalysis(){
        flagForAssetAnalysis = false;
        toShowSaveAnalysis = false;
    }
    public void SaveAnalysis(){
        System.debug('@@@@@ the AssetAnalysisDetail to save  :'+AssetAnalysisDetail );
        flagForAssetAnalysis = false;
        AssetAnalysisDetail.Status__c ='Analysis Completed';
        AssetAnalysisDetail.Customer_Comments__c = assetCustomerComment;
        AssetAnalysisDetail.Internal_Comments__c = assetInternalComment;
        try{
            update AssetAnalysisDetail;

        }catch(Exception e){
            System.debug('The reason for asset update failure :'+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Reason for the failure of the Record is :.'+e));
        }
        fleetData = [select id,Name,Status__c,Account__c,Account__r.Name,Quantity__c,Asset_Group_Quantity__c,Asset__c,
                            Asset__r.Name,Number_of_Assets_Analyzed__c,Fleet_Type__c,Required_Number_of_Asset_to_Analyze__c,Owner.Name,
                            Minimum_Requirement_Reason__c,Analysis_Date__c,Minimum_Requirement_Reason_Other__c, Comments__c, 
                            Service_Maintenance_records_available__c 
                       from CC_Fleet_Analysis__c 
                      where Id =:fleet_Id 
                      Limit 1];
        if(fleetData.Number_of_Assets_Analyzed__c < fleetData.Required_Number_of_Asset_to_Analyze__c){
            toShowMinimumRequirement = true;
         }
         CC_Asset_Analysis__c tempAssets = [select id,Fleet_Analysis__r.Name,Name,Decal_Number__c,Serial_Number__c,Model__r.Name, 
                                                   Status_formula__c, Status__c 
                                              from CC_Asset_Analysis__c 
                                             Where id=:AssetAnalysisDetail.id 
                                             Limit 1 ];
         listofAnalysis.clear();
         mapofAnalysis.put(tempAssets.id,tempAssets);
         listofAnalysis.addAll(mapofAnalysis.values());
         //return null;
    }
    @RemoteAction
    public static String SaveAnalysis(String attachmentName,String attachmentType, String attachmentBody,String attachmentId,String assetsIds){
        if(attachmentBody != null) {
            Attachment att = getAttachment(attachmentId);
            String newBody = '';
            if(att.Body != null) {
                newBody = EncodingUtil.base64Encode(att.Body);
            }
            newBody += attachmentBody;
            att.Body = EncodingUtil.base64Decode(newBody);
            if(attachmentId == null) {
                att.Name = attachmentName;
                att.ContentType = attachmentType;
                att.parentId = assetsIds;
            }
            upsert att;
            return att.Id;  
        }
        else {
            return 'Attachment Body was null';
        }
    }
    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body
                                        FROM Attachment 
                                        WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
    
    public PageReference CompleteAnalysis(){
        System.debug('@@@@@ the AssetAnalysisDetail to compelete analysis  :'+AssetAnalysisDetail );
        System.debug('@@@@@ the fleetData to compelete analysis  :'+fleetData );
        flagForAssetAnalysis = false;
        if(fleetData.Number_of_Assets_Analyzed__c >= fleetData.Required_Number_of_Asset_to_Analyze__c || (fleetData.Minimum_Requirement_Reason__c =='Other' && fleetData.Minimum_Requirement_Reason_Other__c != null) ||(fleetData.Minimum_Requirement_Reason__c !='Other' || fleetData.Minimum_Requirement_Reason__c != null )){
            if(fleetData.Minimum_Requirement_Reason__c !='Other'){
                fleetData.Minimum_Requirement_Reason_Other__c = '';
            }
            try{
                update fleetData;
                generatePDF(fleetData.Name,fleetData.id);
                PageReference pg = new PageReference('/apex/CC_ScheduleNextFleetAnalysisFlow?fleetId='+fleetData.id);
                pg.setRedirect(true);
                return pg;
            }catch(Exception e){
                System.debug('@@@ The reason for fleet update failure :'+e);
                return null;
            }
            
        }
        else if(fleetData.Number_of_Assets_Analyzed__c < fleetData.Required_Number_of_Asset_to_Analyze__c){
            if(fleetData.Minimum_Requirement_Reason__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select the minimum requirement reason value.'));
            }
            else if(fleetData.Minimum_Requirement_Reason__c =='Other' && fleetData.Minimum_Requirement_Reason_Other__c == null  ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill the reason for the minimum requirement reason value.'));
            }
        }
        return null;
    }
    
    @Future(callout=true)
    private static void generatePDF(String fleetdataname, String fleetanalysisid){
      PageReference pdf = new PageReference('/apex/CC_FleetAnalysisPdf?fleetId='+fleetanalysisid);
      Attachment attach = new Attachment();
      Blob body;
      try{
          body = pdf.getContent();
      }
      catch(VisualforceException e){
          body = Blob.valueOf('Exception is thrown while creating Pdf Page');
      }
      ContentVersion contVer = new ContentVersion();
      contVer.Title=fleetdataname+' '+System.now();
      contVer.pathOnClient =fleetdataname+' '+System.now()+'.pdf';
      contVer.versionData = body;
      try{
          insert contVer;
      }
      catch(Exception e){
          System.debug('@@@ Exception due to :'+e);
      }
      contVer = [select id,ContentDocumentId from ContentVersion where Id=:contVer.id limit 1];
      ContentDocumentLink contDocLink = new ContentDocumentLink();
      if(contVer!=null){
          contDocLink.ContentDocumentId = contVer.ContentDocumentId;
          contDocLink.LinkedEntityId = fleetanalysisid;
          contDocLink.ShareType = 'V';
          contDocLink.Visibility = 'AllUsers';
          try{
          insert contDocLink;
          }
          catch(Exception e){
              System.debug('Exception due to :'+e);
          }
      }
      System.debug('@@@@@ the Content Version Id :'+contVer);
      System.debug('@@@@@ the ContentDocumentLink :'+contDocLink);
    }
}