public class CC_ProductItemTransactionHandler {
  
  public static void assignWarehouses(ProductItemTransaction[] pitLst){
    
    Map<Id,ProductConsumed> productConsumedMap = New Map<Id, ProductConsumed>();
    Map<Id,ProductTransfer> productTransferMap = New Map<Id, ProductTransfer>();
    Map<Id,ProductItem> productItemMap = new Map<Id, ProductItem>();
    
    for (ProductItemTransaction pit : pitLst){
      if(null != pit.RelatedRecordId){
        if(pit.TransactionType=='Consumed'){
          productConsumedMap.put(pit.RelatedRecordId,null);
        } else if (pit.TransactionType=='Transferred' && pit.Quantity > 0){
          productTransferMap.put(pit.RelatedRecordId,null);
        }
      } else if(pit.TransactionType=='Adjusted' && pit.Quantity > 0 && pit.CreatedByid!='005j000000Dze64AAB'){ 
        productItemMap.put(pit.ProductItemId,null);
        
      }
    }

    for(ProductConsumed pc : [SELECT Id,ProductItem.Location.Name,ProductItem.Location.ParentLocation.Name,WorkOrderId 
                                FROM ProductConsumed 
                               WHERE Id IN :productConsumedMap.keyset()]){
      productConsumedMap.put(pc.Id,pc);
    }

    for(ProductTransfer pt : [SELECT Id,DestinationLocation.Name,DestinationLocation.ParentLocation.Name,SourceLocation.Name,SourceLocation.ParentLocation.Name,Work_Order_lookup__c
                                FROM ProductTransfer 
                               WHERE Id IN :productTransferMap.keyset()]){
      productTransferMap.put(pt.Id,pt);
    }
    
    for(ProductItem pi : [SELECT Id, Location.Name, Location.ParentLocation.Name FROM ProductItem WHERE Id IN :productItemMap.Keyset()]){
      productItemMap.put(pi.Id,pi);
    }
    
    
    for (ProductItemTransaction pit : pitLst){
      if(null != pit.RelatedRecordId){
        if(pit.TransactionType=='Consumed'){
          ProductConsumed pc = productConsumedMap.get(pit.RelatedRecordId);
          pit.Source_Location__c=pc.ProductItem.Location.Name;
          pit.Source_Warehouse__c=pc.ProductItem.Location.ParentLocation.Name;
          pit.Work_Order__c=pc.WorkOrderId;
        } else if (pit.TransactionType=='Transferred' && pit.Quantity > 0){
          ProductTransfer pt = productTransferMap.get(pit.RelatedRecordId);
          pit.Source_Location__c=pt.SourceLocation.Name;
          pit.Source_Warehouse__c=pt.SourceLocation.ParentLocation.Name;
          pit.Destination_Location__c=pt.DestinationLocation.Name;
          pit.Destination_Warehouse__c=pt.DestinationLocation.ParentLocation.Name;
          pit.Work_Order__c=pt.Work_Order_lookup__c;
        }
      } else if(pit.TransactionType=='Adjusted' && pit.Quantity > 0 && pit.CreatedByid!='005j000000Dze64AAB'){
        ProductItem pi = productItemMap.get(pit.ProductItemId);
        pit.Source_Location__c=pi.Location.Name;
        pit.Source_Warehouse__c=pi.Location.ParentLocation.Name;
      }
    }
  }
}