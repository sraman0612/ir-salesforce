public class CC_PavilionContentHistoryController {
    public Integer pageSize{get;set;}
    public List<ContentVersionHistory> cvhList{get;set;}
    public Integer noOfPages{get;set;}
    public Integer pageNumber{get;set;}
    public String recordTypeN='Tech Pubs';
    public Integer recordLimit=5;
    public String baseQueryNew;
    public String baseQuery = 'select ContentVersionId, CreatedById, CreatedDate, Field, Id, IsDeleted, NewValue, OldValue from ContentVersionHistory Where (Field=\'CC_Date__c\' OR Field=\'CC_Deliverable_Type__c\' OR Field=\'CC_Language__c\' OR Field=\'Owner\' OR Field=\'CC_Publication_Number__c\' OR Field=\'Content_Title__c\' OR Field=\'CC_Revision_Code__c\' OR Field=\'CC_TechPubs_Status__c\' OR Field=\'CC_TechPubs_Comments__c\' OR Field=\'Description\' OR Field=\'CC_Kit_Part_Number__c\' OR Field=\'CC_SN_Prefix__c\' OR Field=\'CC_Year__c\') AND (ContentVersion.RecordType.Name =\'Tech Pubs\') ';
    //baseQuery+ = ' limit '+recordLimit;
    public Integer totalNoOfRecs;
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    
         
public string selectedPage{get;set{selectedPage=value;}
    }

    
    public CC_PavilionContentHistoryController(ApexPages.StandardController controller) {
        pageSize = 10;
        totalNoOfRecs = [select count() from ContentVersionHistory where ContentVersion.RecordType.Name ='Tech Pubs'];
        getInitialContentVersionHistorySet1();  
    }
    
    //code contributed by @Priyanka Baviskar for issue no 7500029.

    public PageReference getInitialContentVersionHistorySet()
    {        
        pageNumber = 0;
        noOfPages = totalNoOfRecs/pageSize;
        
        if (Math.mod(totalNoOfRecs, pageSize) > 0)
            noOfPages++;
        
        try{
           
            //cvhList= Database.query(baseQuery); 
           cvhList= Database.query(baseQuery + ' limit ' +pageSize );
            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;  
    }
    
     public PageReference getInitialContentVersionHistorySet1()
    {        
        pageNumber = 0;
        noOfPages = totalNoOfRecs/pageSize;
        
        if (Math.mod(totalNoOfRecs, pageSize) > 0)
            noOfPages++;
        
        try{
           
            //cvhList= Database.query(baseQuery); 
           cvhList= Database.query(baseQuery + ' limit 50000');
               cvhList= Database.query(baseQuery + ' limit '+pageSize);

            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;  
    }
   //code contributed by @Priyanka Baviskar for issue no 7500029.
   
    public PageReference dateRangeFilter(){
        cvhRec();
        return null;
    }
    public PageReference next(){
        pageNumber++; 
        
        contentVersionHistoryRecords();
        return null;
    }

    public PageReference previous(){
        pageNumber--;
        if (pageNumber < 0)
            return null;
        
        contentVersionHistoryRecords();
        return null;
    }
    
    private void contentVersionHistoryRecords()
    {
        Integer offset = pageNumber * pageSize;    
        String query = baseQuery + ' limit '+pageSize +' offset '+ offset;
        System.debug('Query is'+query);
        try{
            cvhList= Database.query(query);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }       
    }
    private void cvhRec(){
       string stDate=string.valueof(startDate);
       system.debug('startDate:'+startDate);
       string eDate=String.valueof(endDate);
       system.debug('EndDate'+endDate);
       Integer offset = pageNumber * pageSize;   
       String query = baseQuery +' and ((ContentVersion.createdDate>=:startDate OR ContentVersion.ContentModifiedDate>=:startDate) AND (ContentVersion.createdDate<=:endDate OR ContentVersion.ContentModifiedDate<=:endDate)) limit '+pageSize;
       system.debug('query is'+query);
      try{
          if(startDate !=null && endDate!=null)
          cvhList= Database.query(query);
          else if(startDate !=null && endDate==null){
          ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter the End Date');
          ApexPages.addMessage(Msg);
          System.debug('message'+msg);
          }
          else if(startDate ==null && endDate!=null){
          ApexPages.Message Msg2 = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter the Start Date');
          ApexPages.addMessage(Msg2);
          System.debug('message'+msg2);
          }
          else if(startDate ==null && endDate==null){
          ApexPages.Message Msg3 = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter the Start Date and End Date');
          ApexPages.addMessage(Msg3);
          System.debug('message'+msg3);
          }
      }
      catch(Exception e){
      System.debug('exception:'+e);
      }      
            }
}