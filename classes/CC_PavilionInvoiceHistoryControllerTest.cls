@isTest
private class CC_PavilionInvoiceHistoryControllerTest {
    static testMethod void testInvoiceController() 
    {
        
        //Account acc;
        //Contact con;
        //User PortalUser;
        CC_My_Team__c CustFinRep,FinRep;
        CC_Invoice2__c SampleInv1,SampleInv2;
        CC_Statements__c SampleStmt1,SampleStmt2,SampleStmt3,SampleStmt4,SampleStmt5,SampleStmt6;
        List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();
        List<CC_Statements__c> stmtLists=new List<CC_Statements__c>(); 
        
        //creating test data starts here//
        Account acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        System.assertEquals('Sample account', acc.Name);
        System.assertNotEquals(null, acc.Id); 
        Contact con= TestUtilityClass.createContact(acc.id);
        insert con;
        System.assertNotEquals(null, con.Id);        
        // PavilionSettings__c records are created to handle User Events trigger
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        System.assertEquals('ClubCarCommunityLoginProfileId',newPavnSettings.Name);
        System.assertNotEquals(null,newPavnSettings.Id);
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        System.assertEquals('PavillionCommunityProfileId',firstPavnSettings.Name);
        System.assertNotEquals(null,firstPavnSettings.Id);
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        System.assertEquals('Sample Group',grp.Name);
        System.assertNotEquals(null,grp.Id);
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        System.assertEquals('PavilionPublicGroupID',secondPavnSettings.Name);
        System.assertNotEquals(null,secondPavnSettings.Id);
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert u;
        System.assertEquals(u.ContactId,con.Id);
        System.assertNotEquals(false, u.IsActive);
        CustFinRep =TestUtilityClass.createMyTeam('Customer Financing Representative',acc.id,'Customer Finance');
        insert CustFinRep;
        System.assertEquals('Customer Financing Representative', CustFinRep.My_Team_Role__c);
        System.assertNotEquals(null, CustFinRep.Id);
        CC_Invoice2__c inv=new CC_Invoice2__c();
        inv.CC_Invoice_Number__c='123';
        inv.CC_Net_Due_Date__c=System.today();
        inv.CC_sequence__c=1;
        inv.CC_Customer_Number__c=acc.id;
        inv.CC_Invoice_Date__c=System.today() - 365;
        insert inv;
                //creating test data ends here//    
        
        System.runAs(u)
        {
            test.startTest();
            CC_PavilionTemplateController controller;
            CC_PavilionInvoiceHistoryController invController= new CC_PavilionInvoiceHistoryController(controller);
            CC_PavilionInvoiceHistoryController.getInvHistJson(acc.id,'');
            /*CC_PavilionInvoiceHistoryController.invoiceWrapper invWrapper = new CC_PavilionInvoiceHistoryController.invoiceWrapper('new invoice','123',System.today(),'LosAngeles','123', 
                          '123',10000,'2016','invdesc'); */
            test.stopTest();
        }
        
    }
}