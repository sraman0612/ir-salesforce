Public Class CC_PavilionConfirmationcnt{
  Public String orderId {get;set;}
  Public String hazMatFee {get;set;}
  
  Public CC_PavilionConfirmationcnt(CC_PavilionTemplateController controller){
    orderId=ApexPages.currentPage().getParameters().get('id');
    hazMatFee=PavilionSettings__c.getInstance('HAZMATPRICE').Value__c;
  }

  @remoteAction
  public static CC_Order__c getOrderHeader(String oId){
    CC_Order__c o=[SELECT Id,Name,CC_Address_line_1__c,CC_Address_line_2__c,CC_Address_line_3__c,CC_Addressee_name__c,CC_Contact_name__c,CC_Fax_number__c,  
                          CC_Telephone_number__c,CC_City__c,CC_States__c,CC_Postal_code__c,CC_Country__c,CC_Account__c,CC_Shipping_Methods__c,CC_Request_Date__c,
                          Total_Invoice_Amount__c,CC_Carrier_ID__c,CC_Priority_ID__c,CC_Account__r.CC_P_O_Required__c,CC_Account__r.Type,CC_Account__r.CC_Account_Type__c,CC_Account__r.CC_Shipping_Classification__c
                     FROM CC_Order__c 
                    WHERE id =: oId
                    LIMIT 1];
    return o;
  }
  
  @remoteAction
  public static orderItemWrapper[] getOrderItems(String oId){
    //@TODO HAZMAT FEE ONLY APPLIES TO INT
    orderItemWrapper[] oiWrapperLst = new List<orderItemWrapper>();
    Map<String, Product2> itemMasterMap = new Map<String,Product2>();
    Set<String> itemSet = new Set<String>();
    CC_Order_Item__c[] lst = [SELECT CC_Product_Code__c,CC_Description__c,CC_UnitPrice__c,CC_Quantity__c,CC_StockAvailability__c,CC_TotalPrice__c
                                FROM CC_Order_Item__c
                               WHERE Order__c =:oId
                            ORDER BY CreatedDate DESC];
    for (CC_Order_Item__c oi : lst) {itemSet.add(oi.CC_Product_Code__c);}
    for(Product2 p : [SELECT ProductCode,CC_Battery__c,CC_Hazard__c,CC_Inventory_Count__c,CC_Value_Class__c,CC_Domestic_Free_Freight__c,CC_International_Free_Freight__c
                        FROM Product2 
                       WHERE ProductCode in :itemSet and RecordType.DeveloperName='Club_Car']){
      itemMasterMap.put(p.ProductCode,p);
    }
    for (CC_Order_Item__c i : lst){
      orderItemWrapper oiw = new orderItemWrapper(i);
      oiw.availability=calculateAvailability(i.CC_Quantity__c,itemMasterMap.get(i.CC_Product_Code__c).CC_Inventory_Count__c,itemMasterMap.get(i.CC_Product_Code__c).CC_Value_Class__c);
      oiw.battery=itemMasterMap.get(i.CC_Product_Code__c).CC_Battery__c;
      oiw.hazMat=itemMasterMap.get(i.CC_Product_Code__c).CC_Hazard__c;
      //added to determine the eligibility of free freight
      oiw.hasDomFrFreight=itemMasterMap.get(i.CC_Product_Code__c).CC_Domestic_Free_Freight__c;
      oiw.hasIntFrFreight=itemMasterMap.get(i.CC_Product_Code__c).CC_International_Free_Freight__c;
      if(itemMasterMap.containsKey(i.CC_Product_Code__c)){oiw.partNumber = i.CC_Product_Code__c;}
      oiWrapperLst.add(oiw);
    }
    
    return oiWrapperLst;
  }
  
  @remoteAction
  public static String[] determineFreeFreight(String acctType, String type,String carrier, String priority, String country, Decimal orderTotal, 
                                              String shipDate, String shipClass, Boolean hasHazMat, Boolean hasBattery, 
                                              Boolean hasDomFrFrieght, Boolean hasIntFrFrieght,String partNumberArr){

    Set<String> ineligibleCarriers = new Set<String>();
    for (CC_Shipping_Methods__c sm : CC_Shipping_Methods__c.getAll().values()) {
      if (!sm.Eligible_for_Free_Freight__c){
        ineligibleCarriers.add(sm.CC_Carrier_ID__c);
      }
    }
    
    String [] responseLst = new List<String>();
    String [] partNumberLst = new List<String>();
    Map<String,String> partNumberMap = new Map<String,String>();
    Boolean isDealer=acctType=='Dealer'?true:false;
    Boolean isDistributor=acctType=='Distributor'?true:false;
    Boolean isNonAuthorized=acctType=='Non-Authorized Dealer'?true:false;//Added by @Priyanka Baviskar for ticket #8075444 :FREE FREIGHT NOT Working PROPERLY FOR USED CAR BUYER ACCOUNTS on 3/21/2019.
    Boolean isChannelPartner=type=='Channel Partner'?true:false; //Added by @Priyanka Baviskar for ticket #8075444 :FREE FREIGHT NOT Working PROPERLY FOR USED CAR BUYER ACCOUNTS on 3/21/2019.
    Date today = system.today();
    Date orderDate = Date.parse(shipDate);
    partNumberLst = partNumberArr.split(',');
    for(String s : partNumberLst){
      if(s.contains('[')){s=s.replace('[','');}
      if(s.contains(']')){s=s.replace(']','');}
      s=s.replaceAll( '\\s+','');//REMOVE IF THERE ARE ANY BLANK SPACES
      partNumberMap.put(s,s);
    }
    if(ineligibleCarriers.contains(carrier) || (!isDistributor && !isDealer && !isNonAuthorized)){
      responseLst.add('DNA');
    } else if ((shipClass=='US'||shipClass=='CA')) {
        if(orderTotal<100000){
          if(!hasDomFrFrieght){
            for(String s : partNumberMap.keyset()){
              responseLst.add(s+' is not eligible for free freight.');
            }
          }
          if(carrier=='UPSR' || carrier=='UPSB'){responseLst.add('Only ground transportation is eligible.');} 
          if(isDistributor && orderTotal<1750){responseLst.add('Order total must be > 1,750 to be eligible.');}
          else if(isDealer && orderTotal<1500){responseLst.add('Order total must be > 1,500 to be eligible.');}
          if(isNonAuthorized && isChannelPartner && orderTotal<2000){responseLst.add('Order total must be > 2,000 to be eligible.');} //Added by @Priyanka Baviskar for ticket #8075444 :FREE FREIGHT NOT Working PROPERLY FOR USED CAR BUYER ACCOUNTS on 3/21/2019.
          else if(isNonAuthorized && !isChannelPartner){responseLst.add('');}
          System.debug('###responselist::-'+responseLst);
        }
    } else {//is INTNL
      if(carrier=='OCEAN'){
        if(orderTotal<30000){responseLst.add('Order total must be > 30,000 to be eligible.');}
      } else { // its either UPSFF or MTF based on all other intl carriers in the ineligible set 
        if(!hasIntFrFrieght){
          for(String s : partNumberMap.keyset()){responseLst.add(s+' is not eligible for free freight.');}        
        }
        if(orderTotal<2500){responseLst.add('Order total must be > 2,500 to be eligible.');}
      }
    }
    if(responseLst.isEmpty())
      responseLst.add('ELIGIBLE');
      System.debug('@@@responseLst::-'+responseLst);
    return responseLst;
  }
  
  @remoteAction
  public static String submitOrder(String oId,Boolean shipComplete,String PONum,Boolean suspended,Boolean freeFreight,boolean isHazMat){
    if (suspended){return 'HOLD';}
    String returnMsg='OK';
    CC_Order__c o = new CC_Order__c();
    o.Id=oId;
    o.CC_Status__c = 'Submitted';
    o.CC_Quote_Order_Date__c = system.today();
    o.PO__c=PONum;
    o.Ship_Complete__c=shipComplete;
    try {
      if(freeFreight){
        note n = new note();
        n.parentId=oId;
        n.body='FREE FREIGHT';
        n.title='FREE FREIGHT';
        insert n;
      }
      if(isHazMat){
        CC_Order_Item__c oli = new CC_Order_Item__c();
        oli.Order__c=oId;
        oli.CC_Quantity__c=1;
        oli.CC_Product_Code__c='HAZMAT';
        Product2 p = [SELECT Id, Description FROM Product2 WHERE ProductCode='HAZMAT'];
        oli.Product__c=p.Id; 
        oli.CC_Description__c=p.Description;
        oli.CC_UnitPrice__c=Decimal.valueOf(PavilionSettings__c.getInstance('HAZMATPRICE').Value__c);
        oli.CC_TotalPrice__c=oli.CC_UnitPrice__c;
        oli.RecordTypeId = [SELECT Id FROM recordType WHERE sobjecttype='CC_Order_Item__c' AND DeveloperName='CC_Parts_Ordering'].Id;
        insert oli;
      }
      update o;
    } catch (Exception e) {
      returnMsg = e.getMessage() + ' ' + e.getStackTraceString();
    }
    return returnMsg;
  }
  
  class orderItemWrapper{
    public string item {get;set;}
    public string description {get;set;}
    public string priceEa {get;set;}
    public string qty {get;set;}
    public string priceTotal {get;set;}
    public string availability {get;set;}
    public string partNumber{get;set;}
    public boolean battery {get;set;}
    public boolean hazMat {get;set;}
    public boolean hasDomFrFreight {get;set;}
    public boolean hasIntFrFreight {get;set;}
    
    public orderItemWrapper(){}
  
    public orderItemWrapper(CC_Order_Item__c oi){
        item=oi.CC_Product_Code__c;
        description=oi.CC_Description__c;
        priceEa=oi.CC_UnitPrice__c.toPlainString();
        qty=oi.CC_Quantity__c.format();
        priceTotal=oi.CC_TotalPrice__c.toPlainString();
    }
  }
  
  public static String calculateAvailability(Decimal Quantity,Decimal InventoryCount,String ValueClass){
    String availability='';
    if(InventoryCount==0){
      if(ValueClass=='A' || ValueClass=='B' || ValueClass=='C') {availability='Out Of Stock';} 
      else {availability='Subject To Lead Time';}} 
    else {
      if(Quantity> InventoryCount) {availability='Quantity Unavailable';} 
      else {availability='In Stock';}}
    return availability;
  }
}