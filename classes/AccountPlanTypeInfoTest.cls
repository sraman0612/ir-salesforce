@isTest
public class AccountPlanTypeInfoTest {
 // Account Record Types
 //Commented as part of EMEIA Cleanup
    //public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
    public static final String RS_HVAC_RT_DEV_NAME = 'Customer';
    public static final String AirNAAcct_RT_DEV_NAME = 'CTS_EU';
    
    //Account Plan Record Types
    public static final String RS_Account_Plan_Standard_RT_DEV_NAME = 'CTS_Account_Plan';
    public static final String CTS_Account_Plan_RT_DEV_NAME = 'CTS_Account_Plan';
    
    @testSetup static void setup(){
        AccountPlan__c accountPlan;
        String currentYear = '2020';
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        System.runAs(u){
            
            Account airdTestAccount = IRSPXTestDataFactory.createAccount('TestAcc1',AirNAAcct_RT_DEV_NAME);
            airdTestAccount.ownerid=u.id;
            insert airdTestAccount;
            
            accountPlan = IRSPXTestDataFactory.CreateAccountPlan(airdTestAccount.id, CTS_Account_Plan_RT_DEV_NAME, currentYear);
            //accountPlan.OwnerId = u.id;
            insert accountPlan;
			
            //Commented as part of EMEIA Cleanup
           /* Account rhvacTestAccount = IRSPXTestDataFactory.createAccount('TestAcc2',RS_HVAC_RT_DEV_NAME);
            rhvacTestAccount.ownerid=u.id;
            insert rhvacTestAccount;
            
            accountPlan = IRSPXTestDataFactory.CreateAccountPlan(rhvacTestAccount.id, RS_Account_Plan_Standard_RT_DEV_NAME, currentYear);
            //accountPlan.OwnerId = u.id;
            insert accountPlan;
            */
        }
    }
    
    @isTest static void accountPlanTypeTest(){
        AccountPlanTypeInfo accountPlanType;
        Map<Id,Schema.RecordTypeInfo> recordTypeInfo;
        String developerName;
        AccountPlan__c ap;
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt.1'];
        
        System.runAs(u){
            ap = [select Id, Name, RecordTypeId from AccountPlan__c where accountName__r.name = 'TestAcc1'];    
            accountPlanType = AccountPlanTypeInfo.getInstance();  
            recordTypeInfo = accountPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)ap.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            system.assertEquals(CTS_Account_Plan_RT_DEV_NAME, developerName);
            
            /*ap = [select Id, Name, RecordTypeId from AccountPlan__c where accountName__r.name = 'TestAcc2'];
            accountPlanType = AccountPlanTypeInfo.getInstance();  
            recordTypeInfo = accountPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)ap.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            system.assertEquals(RS_Account_Plan_Standard_RT_DEV_NAME, developerName);*/
        }
        Test.Stoptest();
        
    }
}