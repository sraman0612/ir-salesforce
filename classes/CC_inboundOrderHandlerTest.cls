@isTest
global class CC_inboundOrderHandlerTest{
   static testMethod void orderHeaderTest(){
       CC_inboundOrderHandler.orderHeader callOrdHeader = new CC_inboundOrderHandler.orderHeader();
            callOrdHeader.warehouse = 'A';
            callOrdHeader.status = 'CLOSED';
            callOrdHeader.reference = '1-FO50VV';
            callOrdHeader.orderDate = date.newinstance(2015, 12, 3);
            callOrdHeader.recordType = 'BB';
            callOrdHeader.company = '01';
            callOrdHeader.origPromiseDate = date.newinstance(2016, 1, 18);
            callOrdHeader.origRequestDate = date.newinstance(2016, 1, 04);
            callOrdHeader.customer = '488420';
            callOrdHeader.PO = 'B2690';
            callOrdHeader.shipInfo = 'UPS COLLECT #74X105';
            callOrdHeader.salesRep = '44300';
            callOrdHeader.requestDate = date.newinstance(2016, 1, 04);
            callOrdHeader.type = '1';
            callOrdHeader.creditMemoCode = '';
            callOrdHeader.carrierID = 'CCT';
            callOrdHeader.reasonCode = '';
            callOrdHeader.priorityID = '';
            callOrdHeader.servicingDealer = '0';
            callOrdHeader.s2RecordType = 'BF';
            callOrdHeader.s2Zone = 'B5';
            callOrdHeader.s2StateCode = 'TX';
            callOrdHeader.s2Addressee = 'Metro Golf Cars';
            callOrdHeader.s2AddressLine1 = 'METRO GOLF CARS INC';
            callOrdHeader.s2AddressLine2 = '4063 SOUTH FWY';
            callOrdHeader.s2CountryCode = 'US';
            callOrdHeader.s2AddressLine3 = '';
            callOrdHeader.s2City = 'FORT WORTH';
            callOrdHeader.s2Contact = '';
            callOrdHeader.s2Phone = '8179215491';
            callOrdHeader.s2Fax = '8179240037';
            callOrdHeader.s2PostalCode = '76110-6353';
            callOrdHeader.s2AddressLine4 = '';
            callOrdHeader.s2AddressLine5 = '';
            callOrdHeader.billToZone = 'B5';
            callOrdHeader.billToStateCode = 'TX';
            callOrdHeader.billToAddressee = 'METRO GOLF CARS, INC.';
            callOrdHeader.billToAddressLine1 = '4063 SOUTH FWY';
            callOrdHeader.billToAddressLine2 = '';
            callOrdHeader.billToCountryCode = 'US';
            callOrdHeader.billToAddressLine3 = '';
            callOrdHeader.billToCity = 'FORT WORTH';
            callOrdHeader.billToContact = 'NELSON KING';
            callOrdHeader.billToPhone = '817-921-5491';
            callOrdHeader.billToFax = '';
            callOrdHeader.billToPostalCode = '76110-6353';
            callOrdHeader.billToAddressLine4 = '';
            callOrdHeader.billToAddressLine5 = '';
            callOrdHeader.soldToZone = 'B5';
            callOrdHeader.soldToStateCode = 'TX';
            callOrdHeader.soldToAddressee = 'METRO GOLF CARS, INC.';
            callOrdHeader.soldToAddressLine1 = '4063 SOUTH FWY';
            callOrdHeader.soldToAddressLine2 = '';
            callOrdHeader.soldToCountryCode = 'US';
            callOrdHeader.soldToAddressLine3 = '';
            callOrdHeader.soldToCity = 'FORT WORTH';
            callOrdHeader.soldToContact = 'NELSON KING';
            callOrdHeader.soldToPhone = '817-921-5491';
            callOrdHeader.soldToFax = '';
            callOrdHeader.soldToPostalCode = '76110-6353';
            callOrdHeader.soldToAddressLine4 = '';
            callOrdHeader.soldToAddressLine5 = '';
            callOrdHeader.taxSuffix = 'TXNTX';
            callOrdHeader.termsCode = '60';
            callOrdHeader.termsDesc = 'NET 60 DAYS';
            callOrdHeader.priceBook = 'PCOPI';
            callOrdHeader.custPriceCode = 'D3';
            callOrdHeader.totSpclChrgsNum = '0';
            callOrdHeader.totSpclChrgsAmt = '0';
            callOrdHeader.totTaxAmt = '0';
            callOrdHeader.loadError = 'ORDER UPDATE';
            callOrdHeader.MAPICSOrderNumber = '1437435';
            callOrdHeader.fusionTrxId = '20160725153817449906';
            callOrdHeader.orderType = 'CAR';
       
        List<CC_inboundOrderHandler.orderItem> callOrderItemsList = new List<CC_inboundOrderHandler.orderItem>();
        
        CC_inboundOrderHandler.orderItem callOrderItem = new CC_inboundOrderHandler.orderItem();
        
        callOrderItem.warehouse = 'A';
        callOrderItem.pricingUM = 'EA';
        callOrderItem.sourceSequence = '0000000';
        callOrderItem.recordType = 'BL';
        callOrderItem.qty = '10';
        callOrderItem.company = '01';
        callOrderItem.item = '105032401';
        callOrderItem.taxIndicator = 'T15';
        callOrderItem.basePrice = '0';
        callOrderItem.customer = '488420';
        callOrderItem.dimUM = '';
        callOrderItem.contract = '00000';
        callOrderItem.creditMemoCode = '';
        callOrderItem.reasonCode = '';
        callOrderItem.sequence = '0000100';
        callOrderItem.indicator = '';
        callOrderItem.sellPrice = '6380.7000000';
        callOrderItem.netSales = '63807.000';
        callOrderItem.nForeignDesc = '';
        callOrderItem.nTaxIndicator = '';
        callOrderItem.nItemWeight = '';
        callOrderItem.nUM = '';
        callOrderItem.nCommodityCode = '';
        callOrderItem.nItem = '';
        callOrderItem.nClassCode = '';
        callOrderItem.nPriceClass = '';
        callOrderItem.nDesc = '';
        callOrderItem.nCost = '';
        callOrderItem.nVolumeUM = '';
        callOrderItem.nVolume = '';
        callOrderItem.nTrxType = '';
        callOrderItem.nTaxClass = '';

        callOrderItemsList.add(callOrderItem);
       
       List<CC_inboundOrderHandler.carItem> callCarItemsList = new List<CC_inboundOrderHandler.carItem>();
        
        CC_inboundOrderHandler.carItem callCarItem = new CC_inboundOrderHandler.carItem();
        
        callCarItem.carXARel = '1';
        callCarItem.carTruckSeq = '1';
        callCarItem.carLineSeq = '1';
        callCarItem.carsequence = '0000100';
        callCarItem.carModel = 'PGF2';
        callCarItem.carEstShipDate = date.newinstance(2016, 1, 18);
        callCarItem.carMfgDueDate = date.newinstance(2016, 1, 18);
        callCarItem.carOrigMfgDue = date.newinstance(2016, 1, 18);
        callCarItem.carSchShipDate = date.newinstance(2016, 1, 19);
        callCarItem.carDeliveryDate = date.newinstance(2016, 1, 25);
        callCarItem.carLoadDate = date.newinstance(9999, 12, 31);
        callCarItem.carMfgDate = date.newinstance(2016, 1, 22);
        callCarItem.carAddOnDate = date.newinstance(2016, 1, 18);
        callCarItem.carCustomDate = date.newinstance(9999, 12, 31);
        callCarItem.carCriticalDate = date.newinstance(9999, 12, 31);
        callCarItem.carDropShipSeq = '1600';
        callCarItem.carLoadCode = '952';
        callCarItem.carComments = 'CCT       UPS COLLECT #74X105';
        callCarItem.carCritASAPCmts = '';
        callCarItem.carASAP = '';
        callCarItem.carLoadTime = '99';
        callCarItem.carDockDoor = '';
        callCarItem.carSerialNos = 'SL1629659044 SL1629659045 SL1629659046 SL1629659047 SL1629659048 SL1629659051 SL1629659065 SL1629659069 SL1629659073 SL1629659074';
        callCarItem.carsNumber = '08082471112100000000';
       
        callCarItemsList.add(callCarItem);
       
       List<CC_inboundOrderHandler.carFeature> callCarFeaturesList = new List<CC_inboundOrderHandler.carFeature>();
        
           CC_inboundOrderHandler.carFeature callCarFeature = new CC_inboundOrderHandler.carFeature();
        
            callCarFeature.caritem = '105032401';
            callCarFeature.linesequence = '0000100';
            callCarFeature.carFeatureNo = '01';
            callCarFeature.carOptItemNo = '103950908';
            callCarFeature.featShpHdrNum = '1';
            callCarFeature.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature);
         
            CC_inboundOrderHandler.carFeature callCarFeature1 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature1.caritem = '105032401';
            callCarFeature1.linesequence = '0000100';
            callCarFeature1.carFeatureNo = '02';
            callCarFeature1.carOptItemNo = '103950928';
            callCarFeature1.featShpHdrNum = '1';
            callCarFeature1.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature1);
            
            
            CC_inboundOrderHandler.carFeature callCarFeature2 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature2.caritem = '105032401';
            callCarFeature2.linesequence = '0000100';
            callCarFeature2.carFeatureNo = '03';
            callCarFeature2.carOptItemNo = '102535001';
            callCarFeature2.featShpHdrNum = '1';
            callCarFeature2.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature2);
            
            CC_inboundOrderHandler.carFeature callCarFeature3 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature3.caritem = '105032401';
            callCarFeature3.linesequence = '0000100';
            callCarFeature3.carFeatureNo = '04';
            callCarFeature3.carOptItemNo = '102539604';
            callCarFeature3.featShpHdrNum = '1';
            callCarFeature3.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature3);
            
            CC_inboundOrderHandler.carFeature callCarFeature4 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature4.caritem = '105032401';
            callCarFeature4.linesequence = '0000100';
            callCarFeature4.carFeatureNo = '05';
            callCarFeature4.carOptItemNo = '105061707';
            callCarFeature4.featShpHdrNum = '1';
            callCarFeature4.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature4);
            
            CC_inboundOrderHandler.carFeature callCarFeature5 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature5.caritem = '105032401';
            callCarFeature5.linesequence = '0000100';
            callCarFeature5.carFeatureNo = '06';
            callCarFeature5.carOptItemNo = '102554101';
            callCarFeature5.featShpHdrNum = '1';
            callCarFeature5.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature5);
            
            CC_inboundOrderHandler.carFeature callCarFeature6 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature6.caritem = '105032401';
            callCarFeature6.linesequence = '0000100';
            callCarFeature6.carFeatureNo = '07';
            callCarFeature6.carOptItemNo = '102568401';
            callCarFeature6.featShpHdrNum = '1';
            callCarFeature6.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature6);
            
            CC_inboundOrderHandler.carFeature callCarFeature7 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature7.caritem = '105032401';
            callCarFeature7.linesequence = '0000100';
            callCarFeature7.carFeatureNo = '08';
            callCarFeature7.carOptItemNo = '105061501';
            callCarFeature7.featShpHdrNum = '1';
            callCarFeature7.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature7);
            
            CC_inboundOrderHandler.carFeature callCarFeature8 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature8.caritem = '105032401';
            callCarFeature8.linesequence = '0000100';
            callCarFeature8.carFeatureNo = '09';
            callCarFeature8.carOptItemNo = '103951102';
            callCarFeature8.featShpHdrNum = '1';
            callCarFeature8.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature8);
            
            CC_inboundOrderHandler.carFeature callCarFeature9 = new CC_inboundOrderHandler.carFeature();
            
            callCarFeature9.caritem = '105032401';
            callCarFeature9.linesequence = '0000100';
            callCarFeature9.carFeatureNo = '10';
            callCarFeature9.carOptItemNo = '105085801';
            callCarFeature9.featShpHdrNum = '1';
            callCarFeature9.featShpRelSeq = '1';
            
            callCarFeaturesList.add(callCarFeature9);
       
       List<CC_inboundOrderHandler.orderShipment> callOrderShipmentsList = new List<CC_inboundOrderHandler.orderShipment>();
       
            CC_inboundOrderHandler.orderShipment callOrderShipment = new CC_inboundOrderHandler.orderShipment();
            
            callOrderShipment.shpShpHdrNum = '1';
            callOrderShipment.shpShpDate = date.newinstance(2016, 1, 22);
            callOrderShipment.shpProBill = '';
            callOrderShipment.shpShpNumber = '2654395';
            
            callOrderShipmentsList.add(callOrderShipment);
       
       List<CC_inboundOrderHandler.shipmentItem> callShipmentItemsList = new List<CC_inboundOrderHandler.shipmentItem>();
       
            CC_inboundOrderHandler.shipmentItem callShipmentItem = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem.shpShpHdrNum = '1';
            callShipmentItem.shpShpRelSeq = '1';
            callShipmentItem.shpKitRelSeq = '0';
            callShipmentItem.shplineSeq = '0000100';
            callShipmentItem.shipQuantity = '10.000';
            callShipmentItem.backQuantity = '.000';
            callShipmentItem.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem);
            
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem1 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem1.shpShpHdrNum = '1';
            callShipmentItem1.shpShpRelSeq = '2';
            callShipmentItem1.shpKitRelSeq = '0';
            callShipmentItem1.shplineSeq = '0000150';
            callShipmentItem1.shipQuantity = '10.000';
            callShipmentItem1.backQuantity = '.000';
            callShipmentItem1.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem1);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem2 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem2.shpShpHdrNum = '1';
            callShipmentItem2.shpShpRelSeq = '3';
            callShipmentItem2.shpKitRelSeq = '0';
            callShipmentItem2.shplineSeq = '0000200';
            callShipmentItem2.shipQuantity = '10.000';
            callShipmentItem2.backQuantity = '.000';
            callShipmentItem2.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem2);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem3 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem3.shpShpHdrNum = '1';
            callShipmentItem3.shpShpRelSeq = '4';
            callShipmentItem3.shpKitRelSeq = '0';
            callShipmentItem3.shplineSeq = '0000300';
            callShipmentItem3.shipQuantity = '10.000';
            callShipmentItem3.backQuantity = '.000';
            callShipmentItem3.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem3);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem4 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem4.shpShpHdrNum = '1';
            callShipmentItem4.shpShpRelSeq = '5';
            callShipmentItem4.shpKitRelSeq = '0';
            callShipmentItem4.shplineSeq = '0000400';
            callShipmentItem4.shipQuantity = '10.000';
            callShipmentItem4.backQuantity = '.000';
            callShipmentItem4.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem4);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem5 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem5.shpShpHdrNum = '1';
            callShipmentItem5.shpShpRelSeq = '6';
            callShipmentItem5.shpKitRelSeq = '0';
            callShipmentItem5.shplineSeq = '0000500';
            callShipmentItem5.shipQuantity = '10.000';
            callShipmentItem5.backQuantity = '.000';
            callShipmentItem5.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem5);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem6 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem6.shpShpHdrNum = '1';
            callShipmentItem6.shpShpRelSeq = '7';
            callShipmentItem6.shpKitRelSeq = '0';
            callShipmentItem6.shplineSeq = '0000700';
            callShipmentItem6.shipQuantity = '10.000';
            callShipmentItem6.backQuantity = '.000';
            callShipmentItem6.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem6);
            
            CC_inboundOrderHandler.shipmentItem callShipmentItem7 = new CC_inboundOrderHandler.shipmentItem();
            
            callShipmentItem7.shpShpHdrNum = '1';
            callShipmentItem7.shpShpRelSeq = '8';
            callShipmentItem7.shpKitRelSeq = '0';
            callShipmentItem7.shplineSeq = '0000600';
            callShipmentItem7.shipQuantity = '-10.000';
            callShipmentItem7.backQuantity = '20.000';       
            callShipmentItem7.removedfromopenord = true;
            
            callShipmentItemsList.add(callShipmentItem7);
       
       
       List<CC_inboundOrderHandler.orderComment> callOrderCommentsList = new List<CC_inboundOrderHandler.orderComment>();
       
            CC_inboundOrderHandler.orderComment callOrderComment = new CC_inboundOrderHandler.orderComment();
            
            callOrderComment.cmtShpHdrNum = 1;
            callOrderComment.recordType = 'BJ';
            callOrderComment.sequence = '00001';
            callOrderComment.orderLineSequence = '0';
            callOrderComment.comment = 'C/N NELSON KING';
            
            callOrderCommentsList.add(callOrderComment);
            
            
            CC_inboundOrderHandler.orderComment callOrderComment1 = new CC_inboundOrderHandler.orderComment();
            
            callOrderComment1.cmtShpHdrNum = 1;
            callOrderComment1.recordType = 'BJ';
            callOrderComment1.sequence = '00002';
            callOrderComment1.orderLineSequence = '0';
            callOrderComment1.comment = 'C/P 817-921-5491';
            
            callOrderCommentsList.add(callOrderComment1);
            
            CC_inboundOrderHandler.orderComment callOrderComment2 = new CC_inboundOrderHandler.orderComment();
            
            callOrderComment2.cmtShpHdrNum = 1;
            callOrderComment2.recordType = 'BJ';
            callOrderComment2.sequence = '00003';
            callOrderComment2.orderLineSequence = '0';
            callOrderComment2.comment = 'C/N NELSON KING';
            
            callOrderCommentsList.add(callOrderComment2);
            
            CC_inboundOrderHandler.orderComment callOrderComment3 = new CC_inboundOrderHandler.orderComment();
            
            callOrderComment3.cmtShpHdrNum = 1;
            callOrderComment3.recordType = 'BJ';
            callOrderComment3.sequence = '00004';
            callOrderComment3.orderLineSequence = '0';
            callOrderComment3.comment = 'C/P 817-921-5491';
            
            callOrderCommentsList.add(callOrderComment3);
            
            CC_inboundOrderHandler.orderComment callOrderComment4 = new CC_inboundOrderHandler.orderComment();
            
            callOrderComment4.cmtShpHdrNum = 1;
            callOrderComment4.recordType = 'BJ';
            callOrderComment4.sequence = '00005';
            callOrderComment4.orderLineSequence = '0';
            callOrderComment4.comment = 'ADD SUSPENSION PN PER';
             
            callOrderCommentsList.add(callOrderComment4); 
            
            CC_inboundOrderHandler.orderComment callOrderComment5 = new CC_inboundOrderHandler.orderComment();
             
            callOrderComment5.cmtShpHdrNum = 1;
            callOrderComment5.recordType = 'BJ';
            callOrderComment5.sequence = '00006';
            callOrderComment5.orderLineSequence = '0';
            callOrderComment5.comment = 'D HARRISON 12/30/15';
            
            callOrderCommentsList.add(callOrderComment5);
            
           List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
            
           List<StatesCountries__c> stateCountriesList = new List<StatesCountries__c>();
           stateCountriesList = TestUtilityClass.createStateandCountries();
           
           insert stateCountriesList ;
           
            
           CC_Sales_Rep__c ccSalesRep = new CC_Sales_Rep__c();
            
           Id p = [select id from profile where name= 'CC_Sales Rep'].id;
            
           User u = new User();
           u = TestUtilityClass.createNonPortalUser(p);
            
           insert u;
           
           
            
            
            ccSalesRep = TestUtilityClass.createSalesRep(callOrdHeader.salesRep,u.id);
            
            insert ccSalesRep; 
            
            List<Account> accList = new List<Account>();
            
            Account acc = new Account();
            
            acc = TestUtilityClass.createStatementCustomAccount('Test1',30.0,'3025 Washington Rd,Evans,GA,30907',callOrdHeader.customer,'USA','GA','20907',50.0,40.0,30.0,20.0);
            acc.CC_Customer_Number__c = callOrdHeader.customer;
            
            Account acc1 = new Account();
            
            acc1 = TestUtilityClass.createStatementCustomAccount('Test2',20.0,'6025 poshington Rd,Evans,GA,20907',callOrdHeader.servicingDealer,'USA','GA','30907',50.0,40.0,30.0,20.0);
            acc1.CC_Customer_Number__c = callOrdHeader.servicingDealer;
            
            accList.add(acc);
            accList.add(acc1);
            
            insert accList;
            
            Product2 prod = new Product2();
            prod = TestUtilityClass.createInboundProduct2();
            prod.ERP_Item_Number__c = callOrderItemsList[0].item ;
            
            insert prod;
            
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id pricebookId = Test.getStandardPricebookId();
            
            List<Product2> prodList = new List<Product2>();
            
            for(CC_inboundOrderHandler.carFeature cFeature : callCarFeaturesList) {
                Product2 prodTemp = new Product2();
                prodTemp = TestUtilityClass.createInboundProduct2();
                prodTemp.ERP_Item_Number__c = cFeature.carOptItemNo;
                
                prodList.add(prodTemp);
            }
            
           insert prodList;           
            
           CC_inboundOrderHandler.order(callOrdHeader,callOrderItemsList,callCarItemsList,callCarFeaturesList,callOrderShipmentsList,
                                        callShipmentItemsList,callOrderCommentsList); 
            
           CC_Order__c ccOrder = new CC_Order__c();
            
           ccOrder = TestUtilityClass.createOrder(accList[0].Id);
           ccOrder.CC_Status__c = 'Submitted' ;
            
           insert ccOrder ;
           
           List<Note> notesList = new List<Note>();
           notesList = TestUtilityClass.createNotes(ccOrder.Id,5);
           
           insert notesList ;
           
           
            
           List<CC_Order_Item__c> ccOrderItemList =  new List<CC_Order_Item__c>();
            
            for(CC_inboundOrderHandler.carItem carItem : callCarItemsList) {
                CC_Order_Item__c ccOrderItem = new CC_Order_Item__c();
                ccOrderItem = TestUtilityClass.createOrderItem(ccOrder.Id);  
                ccOrderItem.Order_Item_Key__c = callOrdHeader.company + '_' + callOrdHeader.type + '_' + callOrdHeader.MAPICSOrderNumber+'_'+carItem.carsequence ;
                ccOrderItemList.add(ccOrderItem);
            }
            
         //   insert ccOrderItemList ; 
            
            CC_inboundOrderHandler.order(callOrdHeader,callOrderItemsList,callCarItemsList,callCarFeaturesList,callOrderShipmentsList,
                                        callShipmentItemsList,callOrderCommentsList);
            
       
   }
}