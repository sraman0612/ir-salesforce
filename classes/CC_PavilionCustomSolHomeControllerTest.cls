@isTest
public class CC_PavilionCustomSolHomeControllerTest 
{

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testMethod void UnitTest_CC_PavilionCustomSolHomeController()
    {
        // Creation of Test Data
        Account acc = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con;
        system.debug('the contact created is'+con.Id);
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert u;
        System.assertEquals(u.ContactId,con.Id);
        System.assertNotEquals(false, u.IsActive);
       /* Id recordtypid = [select id from RecordType where RecordType.Name= 'Custom Solutions' and RecordType.DeveloperName='Custom_Solutions'].id;
        CC_Pavilion_Content__c pavContent = new CC_Pavilion_Content__c();
        pavContent.Title__c='Important Message';
        pavContent.RecordTypeId =recordtypid;
        pavContent.CC_Body_1__c='data';
        pavContent.CC_Body_2__c='new data';
        insert pavContent;
        System.assertEquals(recordtypid,pavContent.RecordTypeId);
        System.assertNotEquals('old data', pavContent.CC_Body_2__c);*/
         ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Custom Solutions','Japan','USD','Industrial Utility');
        insert contentVer;  
        ContentVersionHistory cvh=new ContentVersionHistory (ContentVersionId=contentVer.id,Field='contentVersionDownloaded');
        insert cvh;
        // instantiating CC_PavilionTemplateController
        CC_PavilionTemplateController controller;
        // instantiating CC_PavilionCustomSolHomeController
        CC_PavilionCustomSolHomeController customSolHomeCntrl = new CC_PavilionCustomSolHomeController(controller);
        //customSolHomeCntrl.ContentHistory cntHis=new customSolHomeCntrl.ContentHistory(cvh.ContentVersionId,1);
        customSolHomeCntrl.getContentHist();
        
    }
}