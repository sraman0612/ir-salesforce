/**
    @author Marc Powell (Providity)
    @date 09May-2019
    @description Controller class for CTS_OM_CaseSatSurveyVF page. Records user feedback
*/
public without sharing class CTS_OM_CaseSatisfactionSurveyCtrl {    
    
    public List<String> levels{get;set;}
    public Case cse {get;set;}
    public String cNumber {get;set;}
    public String msgs {get;set;}
    public Boolean showIRLogo {get;set;}
    public Boolean showZEKSLogo {get;set;}
    public String cOwnerName{get;set;}
    
    public CTS_OM_CaseSatisfactionSurveyCtrl() {        
        init();                
    }
    
    private void init() {
        showIRLogo = True;
        showZEKSLogo= True;
        //Check if it's a valid case Id
        Map<String,String> params = ApexPages.currentPage().getParameters();
        if(params.containskey('cId')) {
            try {
                cse = [SELECT Id, CTS_Case_Survey_Satisfaction_Level__c, CaseNumber, Owner.FirstName,Owner.LastName,RecordType.DeveloperName FROM Case WHERE Id =: params.get('cId')];
               
                if( cse.RecordType.DeveloperName.startsWith('CTS_OM_ZEKS') || cse.RecordType.DeveloperName.startsWith('Club_Car')){
                    showIRLogo = false;
                }                
                showZEKSLogo = (cse.RecordType.DeveloperName.startsWith('Club_Car') || cse.RecordType.DeveloperName.startsWith('CTS_OM_Americas')) ? false : true;
                cNumber = cse.CaseNumber;
                cOwnerName = cse.Owner.FirstName +' '+ cse.Owner.LastName;
                
            } catch(Exception e) {
                System.debug('Most probably, invalid ID: ' + e.getMessage());
                msgs = System.Label.CTS_OM_Survey_CaseClosed_Error_IncorrectId;
                return;
            }
        } else {
            msgs = System.Label.CTS_OM_Survey_CaseClosed_Error_IncorrectId;
            return;
        } 
        //and hasn't been surveyed already
        if(!String.isBlank(cse.CTS_Case_Survey_Satisfaction_Level__c)) {
            msgs = System.Label.CTS_OM_Survey_CaseClosed_Error_SurveyCompleted;
        }
        
        //Satisfaction levels. Read from the field (CTS_Case_Survey_Satisfaction_Level__c) definition
        levels = new List<String>();       
                
        Schema.DescribeFieldResult fieldResult = Case.CTS_Case_Survey_Satisfaction_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
        for( Schema.PicklistEntry f : ple) {
             levels.add(f.getValue());
        }
    }
    
    @AuraEnabled
    @RemoteAction
    public static String submitFeedback(String recordId, String selectedLevel, String comments) {
        Case c;
        try {
            c = [SELECT Id, CTS_Case_Survey_Satisfaction_Level__c,ContactId,RecordType.DeveloperName, CaseNumber FROM Case WHERE Id =: recordId];
        } catch(Exception e) {
            System.debug('Most probably, invalid ID: ' + e.getMessage());
            return System.Label.CTS_OM_Survey_CaseClosed_Error_IncorrectId;
        }
        if(!String.isBlank(c.CTS_Case_Survey_Satisfaction_Level__c)) {
            return System.Label.CTS_OM_Survey_CaseClosed_Error_SurveyCompleted;
        }
        
        c.CTS_Case_Survey_Satisfaction_Level__c = selectedLevel;
        //Commented as part of EMEIA Cleanup
        //c.CC_Case_Survey_Comments__c = comments;
        try {
            update c;
             /*if (c.RecordType.DeveloperName.startsWith('CTS')) { 
                Case newFeedbackCase = new Case();
                newFeedbackCase.Priority = 'Low';
                newFeedbackCase.ParentId = c.Id;
                newFeedbackCase.CTS_TechDirect_Category__c = 'Feedback';
                newFeedbackCase.CTS_TechDirect_Sub_Category__c = 'Feedback';
                List<RecordType> rts = [SELECT Id FROM RecordType WHERE sobjectType = 'Case' and DeveloperName ='CTS_Tech_Direct_Case_Feedback'];
                newFeedbackCase.RecordTypeId = rts.size() > 0 ? rts[0].Id : null;
                newFeedbackCase.Description = comments;
                newFeedbackCase.ContactId = c.ContactId;
                newFeedbackCase.Subject  = 'Feedback for Case ' + c.CaseNumber;
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.useDefaultRule= true;
                newFeedbackCase.setOptions(dmlOpts);
                if(rts.size() > 0) insert newFeedbackCase;
             }*/
        } catch(Exception e) {
            return e.getMessage() + '\n\n' + e.getCause() + '\n\n' + e.getLineNumber() + '\n\n' + e.getStackTraceString();
        }
        
        return System.Label.CTS_OM_Survey_CaseClosed_FeedbackRecorded;
    }
}