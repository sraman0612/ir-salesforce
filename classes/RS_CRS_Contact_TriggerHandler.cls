/*****************************************************************
Name:      RS_CRS_Contact_TriggerHandler
Purpose:   Class used for Preventing Contact Deletion 
           If Contact is related to Case.
History                                                            
-------                                                            
VERSION    AUTHOR      DATE      DETAIL
1.0      Neela Prasad  02/02/2018    INITIAL DEVELOPMENT
*****************************************************************/
public class RS_CRS_Contact_TriggerHandler {
    /******************************************************************************
    Purpose            :  Method used to Check Contact has Case                                              
    Parameters         :  Contact object
    Returns            :  void
    Throws [Exceptions]:                                                          
    ******************************************************************************/ 
     public static void CheckingcaseOnContact(List<Contact> Con){
        //To make sure this Class is triggering for RS_CRS Record Types only.
        Id caseRecordTypeId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
         List<case> listCase = New List<case>();
         
         //getting the Set of Contact id's which user is trying to Delete.
        Set<Id> caseIds = New Set<Id>();
         try{
         for(Contact c: Con){
            if(c.RS_CRS_Case__c != null){  
               caseIds.add(c.RS_CRS_Case__c);
            }
         }
         
         listCase = [select id,CaseNumber from case where id =:caseIds AND RecordTypeId =: caseRecordTypeId];
         //Looping Over Contacts to check whether we found any records matching our Criteria
         for(Contact c: con){
           if(listCase.size() > 0){
            c.addError('Cannot delete Contact with related Cases.');
           }
         } 
         }catch (Exception ex){
             System.debug('Error while Preventing Contact Deletion In class \'RS_CRS_Contact_TriggerHandler\': \n'+ ex.getMessage());
         }
     }
     /******************************************************************************
    Purpose            :  Method used to Check Contact related List has Case                                              
    Parameters         :  Contact object
    Returns            :  void
    Throws [Exceptions]:                                                          
    ******************************************************************************/ 
     public static void CaseOnContactRelatedList(List<Contact> Cont){
        //To make sure this Class is triggering for RS_CRS Record Types only.
        Id caseRecordTypeId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
         Id conRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Contact', Label.RS_CRS_Contact_Record_Type);
         
         //getting the Set of Contact id's which user is trying to Delete.
        Set<ID> vacant = New Set<ID>();
         //List<Contact> conList = [select id,RS_CRS_Case__c from Contact WHERE Id IN (SELECT RS_CRS_Case_Contact_Name__c FROM Case)
         //                           AND Id =: Cont AND RecordTypeId =: conRecId];
         try{
           for(Contact c: [select id,RS_CRS_Case__c from Contact WHERE Id IN (SELECT RS_CRS_Case_Contact_Name__c FROM Case)
                                    AND Id =: Cont AND RecordTypeId =: conRecId])
                    {
                       Trigger.oldMap.get(c.Id).addError('Cannot delete Contact with related Cases.');
                    }  
             }catch (Exception ex){
               System.debug('Error while Preventing Contact Deletion In class \'RS_CRS_Contact_TriggerHandler\': \n'+ ex.getMessage());
                                  }
       }
}