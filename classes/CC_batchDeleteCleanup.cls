global class CC_batchDeleteCleanup implements Database.Batchable<sObject>, Database.Stateful{
  global final string queryStr;
  
  public CC_batchDeleteCleanup(string qry){queryStr = qry;}
  
  public Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(queryStr);}
  
  public void execute(Database.BatchableContext BC, List<sObject> scope){delete scope;}
  
  public void finish(Database.BatchableContext BC){}
}