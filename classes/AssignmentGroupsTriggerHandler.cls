public class AssignmentGroupsTriggerHandler {
    public static void queueNameHandler(List<Assignment_Groups__c> assignmentRecs){
        Set<String> queSet = new Set<String>();
    	List<Group> fetchQueues = [Select Id, DeveloperName FROM Group WHERE Type = 'Queue'];
        for(Group queueName : fetchQueues){
            queSet.add(queueName.DeveloperName);
        }
        
        Map<Id, Assignment_Group_Name__c> assignmentTerritoryMap = new Map<Id, Assignment_Group_Name__c>();
        Set<Id> assignmentTerritorySet = new Set<Id>();
        for(Assignment_Groups__c newRecord : assignmentRecs){
            if(String.isNotBlank(newRecord.Group_Name__c)){
                 assignmentTerritorySet.add(newRecord.Group_Name__c);
            }
        }
        List<Assignment_Group_Name__c> assignmentTerritoryList = [SELECT Id, Name FROM Assignment_Group_Name__c 
                                                                  WHERE Id IN :assignmentTerritorySet];
        for(Assignment_Group_Name__c territory:assignmentTerritoryList){
            assignmentTerritoryMap.put(territory.id, territory);
        }
        
        for(Assignment_Groups__c newRecord : assignmentRecs){
            if(String.isNotBlank(newRecord.Queue_Name__c)){
                if(assignmentTerritoryMap.containsKey(newRecord.Group_name__c) && (assignmentTerritoryMap.get(newRecord.Group_Name__c).Name == 'Special First Assignment' 
                                                                        || assignmentTerritoryMap.get(newRecord.Group_Name__c).Name == 'MP/OB/IRP RSM' 
                                                                        || assignmentTerritoryMap.get(newRecord.Group_Name__c).Name == 'MP/OB/IRP Distributor'
                                                                        || assignmentTerritoryMap.get(newRecord.Group_Name__c).Name == 'MP/OB/IRP Special First Assignment'
                                                                        || assignmentTerritoryMap.get(newRecord.Group_Name__c).Name == 'ARO Special First Assignment')){
                    if(!queSet.contains(newRecord.Queue_Name__c)){
                        newRecord.addError('Please enter valid queue name, the name entered does not match the queue name configured \n'+
                                         'e.g. if Queue Name is NA PST Inside Sales Team then enter NA_PST_Inside_Sales_Team');
                    }
                }
                else{
                        newRecord.addError('"Current Territory :'+assignmentTerritoryMap.get(newRecord.Group_Name__c).Name+
                                       '"\n Queue Name cannot be inserted in other territories except Special First Assignment,MP/OB/IRP RSM,MP/OB/IRP Distributor & MP/OB/IRP Special First Assignment');
                } 
            }
        }
    }
}