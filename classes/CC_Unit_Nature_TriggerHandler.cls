public with sharing class CC_Unit_Nature_TriggerHandler {
    
    public static void onAfterInsert(CC_Unit_Nature__c[] unitNatures){
    	natureSync(unitNatures, null, false);
    }
    
    public static void onAfterUpdate(CC_Unit_Nature__c[] unitNatures, Map<Id, CC_Unit_Nature__c> oldMap){
    	natureSync(unitNatures, oldMap, false);
    }
    
    public static void onAfterDelete(CC_Unit_Nature__c[] unitNatures){
    	natureSync(unitNatures, null, true);
    }        
    
    // Make sure the CC_Nature__c table has a unique list of all nature codes derived from CC_Unit_Nature__c
    private static void natureSync(CC_Unit_Nature__c[] unitNatures, Map<Id, CC_Unit_Nature__c> oldMap, Boolean isDelete){
    	
    	system.debug('--natureSync');
    	
    	CC_Nature__c[] natures = new CC_Nature__c[]{};
    	Map<String, String> natureCodeDescriptionMap = new Map<String, String>();
    	Set<String> oldNatureCodes = new Set<String>();
    	
    	for (CC_Unit_Nature__c unitNature : unitNatures){
    		
    		CC_Unit_Nature__c old = oldMap != null ? oldMap.get(unitNature.Id) : null;
    		
			if (isDelete){
    			oldNatureCodes.add(unitNature.Nature__c);
    		}    		    		    		    		
    		else if ((old == null && String.isNotBlank(unitNature.Nature__c)) || (old != null && (unitNature.isActive__c != old.isActive__c || unitNature.Nature__c != old.Nature__c || unitNature.Nature_Description__c != old.Nature_Description__c))){
    				
    			// Need to capture unit code changes to ensure data integrity in CC_Nature__c
    			if (old != null && unitNature.Nature__c != old.Nature__c){
    				natureCodeDescriptionMap.put(unitNature.Nature__c, unitNature.Nature_Description__c);
    				oldNatureCodes.add(old.Nature__c);
    			}
    			// Capture deactivations
    			else if (old != null && unitNature.isActive__c != old.isActive__c && !unitNature.isActive__c){
    				oldNatureCodes.add(unitNature.Nature__c);
    			}
    			else if (unitNature.isActive__c){
    				natureCodeDescriptionMap.put(unitNature.Nature__c, unitNature.Nature_Description__c);
    			}
    		} 
    	}
    	
    	system.debug('nature codes to process: ' + natureCodeDescriptionMap.keySet());
    	system.debug('oldNatureCodes: ' + oldNatureCodes);
    	
    	if (oldNatureCodes.size() > 0){
    		
    		// Check to see if there are any remaining active unit/nature combinations for the old nature codes and if not they need deactivated in CC_Nature__c
    		Set<String> natureCodesToKeep = new Set<String>();
    		
    		for (CC_Unit_Nature__c unitNature : [Select Id, Nature__c From CC_Unit_Nature__c Where Nature__c in :oldNatureCodes and isActive__c = true]){
    			natureCodesToKeep.add(unitNature.Nature__c);
    		}
    		
    		system.debug('natureCodesToKeep: ' + natureCodesToKeep);
    		    		    		
    		for (CC_Nature__c nature : [Select Id, Nature_Code__c From CC_Nature__c Where Nature_Code__c in :oldNatureCodes and Nature_Code__c not in :natureCodesToKeep]){
    			nature.Active__c = false;
    			natures.add(nature);
    		}    		
    	}
    	
    	if (natureCodeDescriptionMap.size() > 0){
    		
    		for (String natureCode : natureCodeDescriptionMap.keySet()){
    			
    			String natureDescription = natureCodeDescriptionMap.get(natureCode);
    			natures.add(new CC_Nature__c(Active__c = true, Nature_Code__c = natureCode, Nature_Description__c = natureDescription));
    		}		
    	}
    	
    	if (natures.size() > 0){
    		
    		system.debug('natures: ' + natures);
    		
        	Database.UpsertResult[] saveResults = Database.upsert(natures, CC_Nature__c.Nature_Code__c, false);
        
	        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
	        
	        for (Database.UpsertResult saveResult : saveResults){
	            
	            if (!saveResult.isSuccess() && saveResult.getErrors().size() > 0){
	                apexLogHandler logHandler = new apexLogHandler('CC_Unit_Nature_TriggerHandler', 'natureSync', saveResult.getErrors()[0].getMessage());
	                apexLogs.add(logHandler.logObj);
	            }
	        }
	        
	        system.debug('errors to log: ' + apexLogs);
	        
	        if (apexLogs.size() > 0){
	            insert apexLogs;
	        }     		
    		
        	sObjectService.upsertRecordsAndLogErrors(natures, 'CC_Unit_Nature_TriggerHandler', 'natureSync');
    	}
    }
}