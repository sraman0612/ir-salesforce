/*****************************************************************
Name:      RS_CRS_CustAssetsListController
Purpose:   Fetch Assets associated with Account on Case
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Ashish Takke	01/02/2018		INITIAL DEVELOPMENT
*****************************************************************/
public with sharing class RS_CRS_CustAssetsListController {
    
    private static Integer pagesize = Integer.valueOf(Label.RS_CRS_Account_Assets_Page_Size); 
    
    /******************************************************************************
	Purpose            :  Method used to fetch Assets associated with Account on Case
	Parameters         :  Case Id, next, prev, offset
	Returns            :  Wrapper class "CustAssetsInnerClass" having Assets List
	Throws [Exceptions]:                                                          
	******************************************************************************/
    @AuraEnabled
    public static CustAssetsInnerClass fetchCustAssetsList(Id caseId, Boolean next, Boolean prev, Decimal off){
        try{
            //To make sure this Class is triggering for RS_CRS Case Record Types only.
            List<Id> caseRecTypeIdList= new List<Id>();
            caseRecTypeIdList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type));
            caseRecTypeIdList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_FSR_Case_Record_Type));
            caseRecTypeIdList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_PSI_Case_Record_Type));
            caseRecTypeIdList.add(RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Close_Case_Record_Type));
            
            //Getting RS CRS Asset Record Type Id 
        	Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
            
            Integer offset = (Integer) off;

            //get Account ID based on case record ID
            Case caseRec = [Select Id, AccountId 
                            From case 
                            Where Id =: caseId AND 
                            	RecordTypeId IN : caseRecTypeIdList Limit 1];
            
            //list of assets variable
            List<Asset> assetLst = new List<Asset>();
            
            Integer listLength = 0;
            if(caseRec.AccountId != null){
                //total assets associated with account on case
                listLength = [Select count() 
                              From Asset 
                              Where RecordTypeId =: assetRecId AND 
                              		AccountId =: caseRec.AccountId ];
            } else{
                //return empty list with no account flag to display error message        
                CustAssetsInnerClass custAssetVar = 
                    new CustAssetsInnerClass(false, assetLst, listLength, offset, false, false);
                
                return custAssetVar;
            }
            
            if(listLength == 0){
                //return empty list to display error message        
                CustAssetsInnerClass custAssetVar = 
                    new CustAssetsInnerClass(true, assetLst, listLength, offset, false, false);
                
                return custAssetVar;
            } else {
                //on load of page
                if(next == false && prev == false){
                    assetLst = [Select Id, Name, SerialNumber, Model_Name__c, Manufacturer__c 
                                From Asset 
                                Where RecordTypeId =: assetRecId AND
                                	AccountId =: caseRec.AccountId 
                                	LIMIT :pagesize OFFSET : offset];
                    
                } //on click of Next button
                else if(next == true && (offset + pagesize) <= listLength) {
                    offset = offset + pagesize;
                    assetLst = [Select Id, Name, SerialNumber, Model_Name__c, Manufacturer__c 
                                From Asset 
                                Where RecordTypeId =: assetRecId AND
                                	AccountId =: caseRec.AccountId 
                                	LIMIT :pagesize OFFSET :offset];
                    
                } //on click of Previous button
                else if(prev==true && offset > 0) {
                    offset = offset - pagesize;
                    assetLst = [Select Id, Name, SerialNumber, Model_Name__c, Manufacturer__c 
                                From Asset 
                                Where RecordTypeId =: assetRecId AND
                                	AccountId =: caseRec.AccountId 
                                	LIMIT :pagesize OFFSET :offset];
                }        
                
                //set variables for use in page        
                CustAssetsInnerClass custAssetVar = 
                    new CustAssetsInnerClass(true, assetLst, listLength, offset, hasPrev(offset), hasNext(offset, listlength, pagesize));
                
                return custAssetVar;                
            }
            
        } catch(Exception ex){
            System.debug('Error while fetching Assets in class \'RS_CRS_CustAssetsListController\': \n'+ ex.getMessage() + '\' at Line# ' + ex.getLineNumber());
            return null;
        }
    }
    
    /******************************************************************************
	Purpose            :  Method used to check Has Previous
	Parameters         :  offset
	Returns            :  Boolean
	Throws [Exceptions]:                                                          
	******************************************************************************/
    private static boolean hasPrev(integer off){
        if(off == 0){
            return false;
        }
        return true; 
    }
    
    /******************************************************************************
	Purpose            :  Method used to check Has Next
	Parameters         :  offset, List Length, Page Size
	Returns            :  Boolean
	Throws [Exceptions]:                                                          
	******************************************************************************/
    private static boolean hasNext(integer off, integer lstLength, integer pgSise){
        if(off + pgSise > lstLength){
            return false;
        }
        return true;
    } 
    
    /******************************************************************************
	Purpose            :  Inner class to set Asset List and other parameters
	******************************************************************************/
    public class CustAssetsInnerClass{
        @AuraEnabled
        public Boolean isAccountLinked;
        
        @AuraEnabled
        public List<Asset> assetList;
        
        @AuraEnabled
        public Integer totalAssets;
        
        @AuraEnabled
        public Integer offset;
        
        @AuraEnabled
        public Boolean hasPrev;
        
        @AuraEnabled
        public Boolean hasNext;
        
        //constructor
        public custAssetsInnerClass(Boolean isAccLinked, List<Asset> assetLst, Integer total, Integer off, Boolean hasPrv, Boolean hasNxt){
            this.isAccountLinked = isAccLinked;
            this.assetList = assetLst;
            this.totalAssets = total;
            this.offset = off;
            this.hasPrev = hasPrv;
            this.hasNext = hasNxt;            
        }
    }
}