// 11/30/2016
// Cal Steele / Ben Lorenz
@isTest
Public class CC_OpportunityEventHandler_Test {
   
    static testmethod void testOpportunityEventHandlerTest(){
        TestDataUtility testData = new TestDataUtility();
        TestDataUtility.createStateCountries();
            
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1101');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        insert acc;    
        opportunity opp = TestDataFactory.createOpportunity('testopty', 'CC Dealer Contract', acc.id, 'No','Budgetary','Qualify','Pipeline');
        opp.CC_Shipment_Month__c = date.today() + 50;
        opp.RecordTypeId = [SELECT Id FROM RecordType where SobjectType='Opportunity' and DeveloperName='Club_Car'].Id;
        opp.StageName  = NULL;
        insert opp;
    }
}