/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioApiClient {
    global TwilioApiClient() {

    }
    global TwilioApiClient(String username, String password) {

    }
    global void addParam(String name, String value) {

    }
    global void addParam(String name, String value, String operator) {

    }
    global void addUrlPart(String urlPart) {

    }
    global String buildFullUrl() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doDelete() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doDelete(String url) {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doGet() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doGet(String url) {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doGetNext() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doGetPrevious() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doPost() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doPost(String url) {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doPut() {
        return null;
    }
    global TwilioSF.TwilioApiClientResponse doPut(String url) {
        return null;
    }
    global String getApiVersion() {
        return null;
    }
    global String getBaseUrl() {
        return null;
    }
    global Boolean hasNextPage() {
        return null;
    }
    global Boolean hasPreviousPage() {
        return null;
    }
    global void setApiVersion(String version) {

    }
    global void setBaseUrl(String url) {

    }
    global void setNextUrl(String url) {

    }
    global void setPreviousUrl(String url) {

    }
    global void setThrowExceptionsOnFailedResponses() {

    }
}
