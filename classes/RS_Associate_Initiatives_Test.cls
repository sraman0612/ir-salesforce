@isTest
private class RS_Associate_Initiatives_Test {

  // Account Name
  private static final String ACCOUNT_NAME = 'Some Name';
  // Default Document Values
  private static final String DOC_TITLE = 'myTitle';
  private static final String DOC_PATH = 'myTitle.pdf';
  private static final Blob DOC_DATA = Blob.valueOf('This is test data');
  // Key initiatives to create on Setup
  private static final Integer KI_SIZE = 5;
  // Days offset for the key initiatives
  private static final Integer DAYS_OFFSET = 3;
  // Developer Name of the RS_HVAC Record type
  private static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';

  /**
     *  Sets the data need for the tests
     */
    @testSetup static void mySetup() {
    // Insert three Key Initiatives
    List<Key_Initiatives__c> initiatives = new List<Key_Initiatives__c>();
    for (Integer i=0;i < KI_SIZE;i++){
      initiatives.add(new Key_Initiatives__c(Start_Date__c = Date.today().addDays((-1) * DAYS_OFFSET), End_Date__c = Date.today().addDays(DAYS_OFFSET)));
    }
    insert initiatives;
    // Insert a Document for each Initiative
    List<ContentVersion> contentVersions = new List<ContentVersion>();
    for (Integer i=0;i < KI_SIZE;i++){
      contentVersions.add(new ContentVersion(Title = DOC_TITLE + i, Account_Initiative__c = initiatives.get(i).Id, PathOnClient = DOC_PATH, VersionData = DOC_DATA));
    }
    insert contentVersions;
    }

  /**
     *
     */
    static testMethod void testController() {
    // Get the RS HVAC Record Type Id
    Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    // Insert an Account
    Account testAcc = new Account(Name = ACCOUNT_NAME, RecordTypeId = rshvacRTID);
    insert testAcc;
    // Count of Initiatives to Associate
    Integer associateCount = KI_SIZE/2;
    // Ids of the Initiatives to associate
    Set<Id> ki_ids = new Set<Id>();
    Test.startTest();
      // Create a reference to the Associate Initiatives page and set it as the test page
      PageReference testPage = Page.RS_Associate_Initiatives;
      Test.setCurrentPage(testPage);
      ApexPages.currentPage().getParameters().put('id', testAcc.Id);
      // Create an instance of the controller
      RS_Associate_Initiatives controller = new RS_Associate_Initiatives();
      // Get the Account Name
      String accountNameFromController = controller.getAccountName();
      Integer initialCountOfKI = controller.getInitiatives().size();
      Integer otherInitialCountOfKI = controller.getKeyInitiativesCount();
      System.debug('@@@@@@@otherInitialCountOfKI'+otherInitialCountOfKI);
      // Iterate over the wrapper class and select some of the initiatives
      for (Integer i=0;i < associateCount;i++){
        if(controller.initList !=null && controller.initList.size() > i){
         controller.initList.get(i).selected = true;
         System.debug('@@@@@@@controller.initList.get(i).selected'+controller.initList.get(i).selected);
         ki_ids.add(controller.initList.get(i).init.Id);
         }
      }
      // Process the selections
      controller.processSelected();
      // Junctions between the account and the key initiatives should've been created
      // The count of available options shoul've decreased
      Integer laterCountOfKI = controller.getInitiatives().size();
      //
      String returnUrl = controller.REDIRECT_URL;
        Test.stopTest();
    // Check the Account Name is correct
    System.assertEquals(ACCOUNT_NAME, accountNameFromController);
    // Check the count of initiatives is equal to the total
    //System.assertEquals(KI_SIZE, initialCountOfKI);
    //System.assertEquals(KI_SIZE, otherInitialCountOfKI);
    // Check that the return url contains the Id of the Account
    System.assertEquals(true, returnUrl.contains(testAcc.Id));
    // After the junctions were created, there should be less initiatives to associate
   // System.assertEquals(KI_SIZE - associateCount, laterCountOfKI);
    // Check that the junctions were actually inserted
    List<Account_Initiatives_Junction__c> junctions = [SELECT Id, Key_Initiative__c FROM Account_Initiatives_Junction__c WHERE Account__c = :testAcc.Id];
    //System.assertEquals(associateCount, junctions.size());
    // Verify that there's one for each of the Ids in ki_ids
    for (Account_Initiatives_Junction__c aij : junctions){
      System.assertEquals(true, ki_ids.contains(aij.Key_Initiative__c));
      ki_ids.remove(aij.Key_Initiative__c);
    }
    //System.assertEquals(true, ki_ids.isEmpty());
    }
}