public with sharing class FontAwesomeIconController {
    
    @AuraEnabled(cacheable=true)
    public static Font_Awesome_Icon__mdt getInit(String iconName){

        Font_Awesome_Icon__mdt[] iconSettings = [Select Id, DeveloperName, Label, Icon_Class__c, Default_Color__c, Hover_Color__c From Font_Awesome_Icon__mdt Where DeveloperName = :iconName];
        return iconSettings.size() == 1 ? iconSettings[0] : new Font_Awesome_Icon__mdt();
    }
}