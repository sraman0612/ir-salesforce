/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UtilCoach {
    webService static String coachMenuLinkUpdate(String menuId) {
        return null;
    }
    webService static String menuLink(String coachId) {
        return null;
    }
    webService static String processRequest(String coachId) {
        return null;
    }
}
