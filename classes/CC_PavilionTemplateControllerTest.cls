@isTest
public class CC_PavilionTemplateControllerTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
        
         CC_Pavilion_Content__c objcont=TestUtilityClass.createCustomPavilionContent('Home Page Tile');
        insert objcont;
    }
    
    static testMethod void testTemplateEMEA(){
    	
        user u = createPartnerUser(10);
        
        u.CC_Pavilion_Navigation_Profile__c = 'EMEA_GENERAL_ADMIN';
        update u;
        
        System.runAs(u)           {
            
            User userDetails= [SELECT id,name,CC_Pavilion_Navigation_Profile__c from User WHERE id=:userinfo.getuserid()];
            String userNavigationProfile =userDetails.CC_Pavilion_Navigation_Profile__c;
            System.debug('the userNavigation profile'+userNavigationProfile);
            
            try{
                
                test.startTest();  
                
                Pavilion_Navigation_Profile__c p = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
                p.Region__c = 'EMEA';
                insert p;
                
                System.debug('the p.name is'+p.Name);
                System.assertEquals(p.Name,userDetails.CC_Pavilion_Navigation_Profile__c);
                CC_PavilionTemplateController objTemp = new CC_PavilionTemplateController();
                
                CC_PavilionTemplateController.getData('test');
                
                system.assertEquals(false, objTemp.accessPricing);    
            }
            catch (Exception e){
            	
            }	
        }
    }
    
    static testMethod void testTemplate()
    {
        
        user u = createPartnerUser(10);
        System.runAs(u)
            
        {
           
            User userDetails= [SELECT id,name,CC_Pavilion_Navigation_Profile__c from User WHERE id=:userinfo.getuserid()];
            String userNavigationProfile =userDetails.CC_Pavilion_Navigation_Profile__c;
            System.debug('the userNavigation profile'+userNavigationProfile);
            
            try{
                
                test.startTest();  
                
                Pavilion_Navigation_Profile__c p = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
                insert p;
                
                System.debug('the p.name is'+p.Name);
                System.assertEquals(p.Name,userDetails.CC_Pavilion_Navigation_Profile__c);
                CC_PavilionTemplateController objTemp = new CC_PavilionTemplateController();
                
                CC_PavilionTemplateController.getData('test');
                
                system.assertEquals(true, objTemp.accessPricing);
                
                objTemp.redirectCoOpHome();
                System.assertEquals(objTemp.redirectCoOpHome(),null);
                
                objTemp.redirectAccountInformation();
                System.assertEquals(objTemp.redirectAccountInformation(),null);
                
                objTemp.redirectAccountInvoiceHistory();
                System.assertEquals(objTemp.redirectAccountInvoiceHistory(),null);
                
                objTemp.redirectAfterMarketCatalog();
                System.assertEquals(objTemp.redirectAfterMarketCatalog(),null);
                
                objTemp.redirectBulletins();
                System.assertEquals(objTemp.redirectBulletins(),null);
                
                objTemp.redirectBoothRental();
                System.assertEquals(objTemp.redirectBoothRental(),null);
                
                objTemp.redirectBlueBook();
                System.assertEquals(objTemp.redirectBlueBook(),null);
                
                objTemp.redirectCarsHome();
                System.assertEquals(objTemp.redirectCarsHome(),null);
                
                objTemp.redirectClubCarConnect();
                System.assertEquals(objTemp.redirectClubCarConnect(),null);
                
                objTemp.redirectClubCarContacts();
                System.assertEquals(objTemp.redirectClubCarContacts(),null);
                
                objTemp.redirectCreditApplication();
                System.assertEquals(objTemp.redirectCreditApplication(),null);
                
                objTemp.redirectCreditCenterHome();
                System.assertEquals(objTemp.redirectCreditCenterHome(),null);
                
                objTemp.redirectCustomerComplaint();
                System.assertEquals(objTemp.redirectCustomerComplaint(),null);
                
                objTemp.redirectCustomSolutions();
                System.assertEquals(objTemp.redirectCustomSolutions(),null);
                
                objTemp.redirectCustomerVIEW();
                System.assertEquals(objTemp.redirectCustomerVIEW(),null);
                
                objTemp.redirectDealerInvoicing();
                System.assertEquals(objTemp.redirectDealerInvoicing(),null);
                
                objTemp.redirectDealerManual();
                //System.assertNotEquals(objTemp.redirectDealerManual(),null);
                
                objTemp.redirectDealerSignage();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectEvents();
                System.assertEquals(objTemp.redirectEvents(),null);
                
                objTemp.redirectFinanceContactList();
                System.assertEquals(objTemp.redirectFinanceContactList(),null);
                
                objTemp.redirectFinancePrograms();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectHelp();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectHoleInOne();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectInventory();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectiStore();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectLiterature();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMarketingContacts();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMarketingHome();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMerchandising();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMarketingIdeas();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMSDSDocuments();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMyCoOpAccount();
                System.assertEquals(objTemp.redirectDealerSignage(),null);

                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMyProfile();
                System.assertEquals(objTemp.redirectDealerSignage(),null);
                
                objTemp.redirectMyTeam();
                System.assertEquals(objTemp.redirectMyTeam(),null);
                
                objTemp.redirectNationalOrderHistory();
                System.assertEquals(objTemp.redirectNationalOrderHistory(),null);
                
                objTemp.redirectNewPartsOrder();
                System.assertEquals(objTemp.redirectNewPartsOrder(),null);
                
                objTemp.redirectOrderHistory();
                System.assertEquals(objTemp.redirectOrderHistory(),null);
                
                objTemp.redirectPartCart();
                System.assertEquals(objTemp.redirectPartCart(),null);
                
                objTemp.redirectPartsOrderHistory();
                System.assertEquals(objTemp.redirectPartsOrderHistory(),null);
                
                objTemp.redirectPartHome();
                System.assertEquals(objTemp.redirectPartHome(),null);
                
                objTemp.redirectPreApprovalRequest();
                System.assertEquals( objTemp.redirectPreApprovalRequest(),null);
                
                objTemp.redirectProgramDocuments();
                System.assertEquals(objTemp.redirectProgramDocuments(),null);
                
                objTemp.redirectPromotionsForms();
                System.assertEquals(objTemp.redirectPromotionsForms(),null);
                
                objTemp.redirectReporting();
                System.assertEquals(objTemp.redirectReporting(),null);
                
                objTemp.redirectResourceLibrary();
                System.assertEquals(objTemp.redirectResourceLibrary(),null);
                
                objTemp.redirectSalesTraining();
                System.assertEquals(objTemp.redirectSalesTraining(),null);
                
                objTemp.redirectSmartLead();
                System.assertEquals(objTemp.redirectSmartLead(),null);
                
                objTemp.redirectSpeedCodes();
                System.assertEquals(objTemp.redirectSpeedCodes(),null);
                
                objTemp.redirectSysAdmin();
                System.assertEquals(objTemp.redirectSysAdmin(),null);
                
                objTemp.redirectTechnicalPublications();
                System.assertEquals(objTemp.redirectTechnicalPublications(),null);
                
                objTemp.redirectTechWarrantyHome();
                System.assertEquals( objTemp.redirectTechWarrantyHome(),null);
                
                objTemp.redirectTavant();
                System.assertEquals( objTemp.redirectTavant(),null);
                
                objTemp.redirectTermsAndConditions();
                System.assertEquals(objTemp.redirectTermsAndConditions(),null);
                
                objTemp.redirectTheMarketPlace();
                System.assertEquals(objTemp.redirectTheMarketPlace(),null);
                
                objTemp.redirectTrainingVideos();
                System.assertEquals(objTemp.redirectNationalOrderHistory(),null);
                
                objTemp.redirectTrainingCourseCatalog();
                System.assertEquals(objTemp.redirectTrainingCourseCatalog(),null);
                
                objTemp.redirectTrainingSuppHome();
                System.assertEquals(objTemp.redirectTrainingSuppHome(),null);
                
                objTemp.redirectTransportationRequest();
                //System.assertNotEquals(objTemp.redirectTransportationRequest(),null);
                
                objTemp.redirectUniformOrdering();
                System.assertEquals(objTemp.redirectUniformOrdering(),null);
                
                objTemp.redirectVideos();
                System.assertEquals(objTemp.redirectVideos(),null);
                
                //objTemp.sendNotificationEmail();
                //System.assertEquals(objTemp.sendNotificationEmail(),null);
                
                test.stopTest();
            }
            Catch(Exception e){}
            
        }
    }
    
      public static  user createPartnerUser(integer i){
        
     
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           

            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
            
             //creating sales rep here
              TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;
            
           
            //Create account
         Account   account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,
                                                BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566',
                                  CC_Billing_County__c = county.id,BillingState = 'GA',CC_Shipping_Billing_Address_Same__c=true, CC_Sales_Rep__c = salesRepacc.Id,
                                  Type = 'Distributor',
             CC_Global_Region__c = 'United States');
            Database.insert(account);

            //Create contact
            contact contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = account.Id,
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            Database.insert(contact);

            //Create user
            Profile portalProfile = [SELECT Id FROM Profile where name ='CC_PavilionCommunityUser' LIMIT 1];
            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+i,
                                 ContactId = contact.Id,ProfileId = portalProfile.Id,
                                 Alias = 'test123',Email = 'test12345@test.ingersollrand.com',
                                 EmailEncodingKey = 'UTF-8',LastName = 'McTesty',
                                 CommunityNickname = 'test12345',TimeZoneSidKey = 'America/New_York',country='USA',
                                 LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',
                            CC_Pavilion_Navigation_Profile__c='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN',PS_Business_Unit_Description__c = 'CLUB CAR',
                             CC_Department__c='Marketing',CC_View_Service_Bulletins__c=true,CC_View_Parts_Pricing__c=true,
                             CC_View_Sales_Pricing__c=true,CC_View_Sales_Bulletins__c=true);
     
            Database.insert(user1);
        }
       return user1; 
    }
}