global class CC_PavilionPreApprovalRequestsController{

    /* member variable */

    public String userAccountId{get; set;}
    
        
    /* constructor */    

    public CC_PavilionPreApprovalRequestsController()
    {    
        List<User> lstUser = [SELECT AccountId, ContactId FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        if(lstUser.isEmpty()) return;
         UserAccountId = lstUser[0].AccountId;
        
    }
    
    /*Remote method to be called from CC_PavilionCoopHOIClaimRequests page via JS Remoting*/
    
    @RemoteAction
    global static String getPreApprovalRequestsJson(String userAccountId)
    {
         List<RecordType> rt = [select id, Name from RecordType where SobjectType = 'CC_Hole_in_One__c' and Name = 'Pre-Approved Request'  Limit 1 ];
           list<orderWrapper> orderwithIVllist = new list<orderWrapper>();
         
        List<CC_Hole_in_One__c> lstCoopClaim = new List<CC_Hole_in_One__c>();
        if(userAccountId != null)
        {
            lstCoopClaim =[SELECT Id, Name, Type_of_pre_approval_Request__c, Event_Name__c, CC_Status__c, Status__c, Pre_Approved_Request__c,
                            (Select Comments From ProcessSteps Order by CreatedDate desc Limit 1)
                           from CC_Hole_in_One__c where Contract__r.AccountId=:userAccountId AND RecordTypeId =: rt[0].id];
                         system.debug('$$$'+lstCoopClaim); 
                            
        }
        
        for(CC_Hole_in_One__c cc:lstCoopClaim){
             String appcomm;
             if(cc.ProcessSteps.size() >0)
             appcomm =  cc.ProcessSteps[0].Comments;
             orderwithIVllist.add(new orderWrapper(cc.Pre_Approved_Request__c,cc.Type_of_pre_approval_Request__c, cc.Event_Name__c,appcomm,cc.CC_Status__c,cc.Id));
        }
        
        if(!orderwithIVllist.isEmpty())
        {
            String jsonData = (String)JSON.serialize(orderwithIVllist);
            system.debug('$$$'+jsonData);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
    
    public class orderWrapper{
         //public Hole_In_One_Claim__c ccord {get;set;}
         //code contributed by @Priyanka Baviskar for Request 7364036.
         public String preAppName {get;set;}
         public String typeofPre {get;set;}
         public String EventName {get;set;}
         public String PreStatus {get;set;}
         public String ApprovalComm {get;set;}
         public String id{get;set;}
     
         public orderWrapper(String invname1, String claimid1,String dateofHOI1, String ApprovalComm1,String PreStatus1,String id1){
             
             preAppName = invname1;
             typeofPre = claimid1;
             EventName = dateofHOI1;
             PreStatus = PreStatus1;
             ApprovalComm = ApprovalComm1;
             id =id1;
         }
      }
}