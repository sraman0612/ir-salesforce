@isTest
public class CC_PavilionOrderHistoryControllerTest {
    @isTest
    static  void UnitTest_OrderHistory()
    {
        List<CC_Car_Item__c> caritemList =new List <CC_Car_Item__c>();
        List<CC_Order__c> orderList =new List <CC_Order__c>();
        List<Contact> conList=new List<Contact>();
        Account a = TestUtilityClass.createAccount();
        insert a;
        Contact con= TestUtilityClass.createContact(a.id);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        insert con;
        conList.add(con);
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        CC_Order__c ord = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
        ord.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Cars_Ordering').getRecordTypeId();
        insert ord;
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';
        insert prod1;        
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod1.Id,1);
        orditem.CC_Quantity__c = 2;
        insert orditem;
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(orditem.Id);
        carItem.Drop_Ship_Sequence__c = 900;
        carItem.Estimated_Ship_Date__c=System.today()+7;
        insert carItem;
        CC_Car_Item__c carItem2=[select Order_Item__r.Order__r.CC_Request_Date__c from CC_Car_Item__c  where id=: carItem.Id limit 1];
        System.assertEquals(carItem.Order_Item__c, orditem.Id);
        System.assertNotEquals(null, carItem.Id);   
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        CC_Car_Feature__c cf =  TestUtilityClass.createCarFeature(carItem.Id,prod.Id,ord.Id);
        insert cf;
        CC_Invoice2__C inv =TestUtilityClass.createInvoice2(a.Id,'123');
        insert inv;
        CC_Order_Shipment__c ordship = TestUtilityClass.createOrderShipment(ord.id);
        insert ordship;
        CC_Order_Shipment_Item__c ordshipitem =TestUtilityClass.createOrderShipmentItem(orditem.Id,ordship.Id);
        insert ordshipitem;
        
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',a.Id);
        insert ps;
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('profileID'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        
        insert u;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
        insert newPavilionNavigationProfile;
        
        system.runAs(u){
            Test.startTest();
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            controller.acctId=a.id;
            CC_PavilionOrderHistoryController ordhis =new CC_PavilionOrderHistoryController();
            ApexPages.currentPage().getParameters().put('p','');
            ApexPages.currentPage().getParameters().put('id','');
            CC_PavilionOrderHistoryController ordhist =new CC_PavilionOrderHistoryController(controller);
            caritemList.add(caritem);
            orderList.add(ord);
            ApexPages.currentPage().getParameters().put('p',null);
            CC_PavilionOrderHistoryController.getOrders(a.id);
            Test.stopTest();
        }
    }
    
    @isTest
    static  void UnitTest_OrderHistory1()
    {
        List<CC_Car_Item__c> caritemList =new List <CC_Car_Item__c>();
        List<CC_Order__c> orderList =new List <CC_Order__c>();
        List<Contact> conList=new List<Contact>();
        Account a = TestUtilityClass.createAccount();
        insert a;
        Contact con= TestUtilityClass.createContact(a.id);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        insert con;
        conList.add(con);
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        
        CC_Order__c ord = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
        ord.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Cars_Ordering').getRecordTypeId();
        insert ord;
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';
        insert prod1;        
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod1.Id,1);
        orditem.CC_Quantity__c = 2;
        insert orditem;
        CC_Invoice2__C inv =TestUtilityClass.createInvoice2(a.Id,'123');
        insert inv;
        CC_Order_Shipment__c ordship = TestUtilityClass.createOrderShipment(ord.id);
        insert ordship;
        CC_Order_Shipment_Item__c ordshipitem =TestUtilityClass.createOrderShipmentItem(orditem.Id,ordship.Id);
        insert ordshipitem;
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',a.Id);
        insert ps;

        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('profileID'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        
        insert u;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
        insert newPavilionNavigationProfile;
        
        system.runAs(u){        
        
            Test.startTest();
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            controller.acctId=a.id;
            CC_PavilionOrderHistoryController ordhis =new CC_PavilionOrderHistoryController();
            ApexPages.currentPage().getParameters().put('p','');
            ApexPages.currentPage().getParameters().put('id','');
            CC_PavilionOrderHistoryController ordhist =new CC_PavilionOrderHistoryController(controller);
            
            orderList.add(ord);
            ApexPages.currentPage().getParameters().put('p',null);
            CC_PavilionOrderHistoryController.getOrders(a.id);
            Test.stopTest();
        }
    }
    @isTest
    static  void UnitTest_OrderHistory3()
    {
        List<CC_Car_Item__c> caritemList =new List <CC_Car_Item__c>();
        List<CC_Order__c> orderList =new List <CC_Order__c>();
        List<Contact> conList=new List<Contact>();
        Account a = TestUtilityClass.createAccount();
        insert a;
        Contact con= TestUtilityClass.createContact(a.id);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        insert con;
        conList.add(con);
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        CC_Order__c ord = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
        ord.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Cars_Ordering').getRecordTypeId();
        insert ord;
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';
        insert prod1;        
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod1.Id,1);
        orditem.CC_Quantity__c = 2;
        insert orditem;
        CC_Invoice2__C inv =TestUtilityClass.createInvoice2(a.Id,'123');
        insert inv;
        CC_Order_Shipment__c ordship = TestUtilityClass.createOrderShipment(ord.id);
        insert ordship;
        CC_Order_Shipment_Item__c ordshipitem =TestUtilityClass.createOrderShipmentItem(orditem.Id,ordship.Id);
        insert ordshipitem;
        CC_Order_Shipment_Item__c ordshipitem1 =TestUtilityClass.createOrderShipmentItem(orditem.Id,ordship.Id);
        insert ordshipitem1;
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',a.Id);
        insert ps;
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('profileID'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        
        insert u;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
        insert newPavilionNavigationProfile;
        
        system.runAs(u){        
        
            Test.startTest();
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            controller.acctId=a.id;
            CC_PavilionOrderHistoryController ordhis =new CC_PavilionOrderHistoryController();
            ApexPages.currentPage().getParameters().put('p','');
            ApexPages.currentPage().getParameters().put('id','');
            CC_PavilionOrderHistoryController ordhist =new CC_PavilionOrderHistoryController(controller);
            
            orderList.add(ord);
            ApexPages.currentPage().getParameters().put('p',null);
            CC_PavilionOrderHistoryController.getOrders(a.id);
            Test.stopTest();
        }
    }
    @isTest
    static  void UnitTest_OrderHistory2()
    {
        List<CC_Car_Item__c> caritemList =new List <CC_Car_Item__c>();
        List<CC_Order__c> orderList =new List <CC_Order__c>();
        List<Contact> conList=new List<Contact>();
        Account a = TestUtilityClass.createAccount();
        insert a;
        Contact con= TestUtilityClass.createContact(a.id);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        insert con;
        conList.add(con);
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        CC_Order__c ord = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
        ord.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByDeveloperName().get('CC_Cars_Ordering').getRecordTypeId();
        insert ord;
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.CC_Market_Type__c = 'Utility';
        insert prod1;        
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod1.Id,1);
        orditem.CC_Quantity__c = 2;
        insert orditem;
        CC_Invoice2__C inv =TestUtilityClass.createInvoice2(a.Id,'123');
        insert inv;
        
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',a.Id);
        insert ps;
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('profileID'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        
        insert u;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
        insert newPavilionNavigationProfile;
        
        system.runAs(u){
            Test.startTest();
            CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
            controller.acctId=a.id;
            CC_PavilionOrderHistoryController ordhis =new CC_PavilionOrderHistoryController();
            ApexPages.currentPage().getParameters().put('p','');
            ApexPages.currentPage().getParameters().put('id','');
            CC_PavilionOrderHistoryController ordhist =new CC_PavilionOrderHistoryController(controller);
            
            orderList.add(ord);
            ApexPages.currentPage().getParameters().put('p',null);
            CC_PavilionOrderHistoryController.getOrders(a.id);
            Test.stopTest();
        }
    }
}