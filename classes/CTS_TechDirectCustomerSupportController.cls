public without sharing class CTS_TechDirectCustomerSupportController {
    @AuraEnabled
    public static List< PicklistEntryWrapper > getPicklistValues(String object_name, String field_name, String default_val) {
        List<PicklistEntryWrapper> options = new List<PicklistEntryWrapper>(); //new list for holding all of the picklist options       
        options.add(new PicklistEntryWrapper('--None--', '', true)); // Set None value by default
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get(object_name); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list       
            if(a.isActive()) {
                if(a.getValue() == default_val) {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), true)); //add the value and label to our final list    
                } else {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), false)); //add the value and label to our final list    
                }    
            }                        
        }
        return options; //return the List
    }
    
    public class PicklistEntryWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean selected { get; set; }      
        
        // Default Constructor
        public PicklistEntryWrapper() { }
        
        // Parameterized Constructor
        public PicklistEntryWrapper(String label, String value, Boolean selected) {
            this.label = label;
            this.value = value;
            this.selected = selected;
        }
    }
    
    @AuraEnabled
    public static Asset getAssetInfo(String assetNumber) {
        Asset assetObj = null;
        List < Asset > assetList = [SELECT Id, Name FROM Asset WHERE Name = :assetNumber];
        if(assetList != null && assetList.isEmpty() == false) {
            assetObj = assetList.get(0);
        }
        System.debug('assetObj ## ' + assetObj);
        return assetObj;
    }
    
    @AuraEnabled
    public static Account getAccountInfo(String regionName) {
        Account accountObj = null;
        String accountName = 'End Customer Account%' + regionName;
        // RecordType.Name = 'CTS TechDirect Account' AND -- Removed
        List < Account > accountList = [SELECT Id, Name FROM Account WHERE Name like :accountName];
        if(accountList != null && accountList.isEmpty() == false) {
            accountObj = accountList.get(0);
        }
        System.debug('accountObj ## ' + accountObj);
        return accountObj;
    }
    
    @AuraEnabled
    public static Contact getContactInfo(String contactEmail) {
        Contact contactObj = null;
        List < Contact > contactList = [SELECT Id, Name FROM Contact WHERE Email = :contactEmail];
        if(contactList != null && contactList.isEmpty() == false) {
            contactObj = contactList.get(0);
        }
        System.debug('contactObj ## ' + contactObj);
        return contactObj;
    }
    
    @AuraEnabled
    public static Product2 getProductInfo(String productName) {
        Product2 productObj = null;
        List < Product2 > productList = [SELECT Id, Name FROM Product2 WHERE Name = :productName];
        if(productList != null && productList.isEmpty() == false) {
            productObj = productList.get(0);
        }
        return productObj;
    }
    
    @AuraEnabled 
    public static Case createCase(String assetNumber, String productName, Case caseObj, Contact contactObj, String region) {     
        if(contactObj != null) {
            if(String.isNotBlank(contactObj.Email)) {
                // Check for existing contact
                Contact exisingContact = getContactInfo(contactObj.Email);
                system.debug('exisingContact'+exisingContact);
                if(exisingContact != null) {
                    caseObj.contactId = exisingContact.Id;
                } else {
                    try {
                        // If existing contact not available then create new contact
                        Account existingAccount = getAccountInfo(region);
                        if(existingAccount != null) {
                            contactObj.AccountId = existingAccount.Id;
                            contactObj.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IR Comp TechDirect Contact').getRecordTypeId();
                            insert contactObj;
                        }
                        caseObj.contactId = contactObj.id;
                    } catch (Exception ex) {
                        System.debug('## Exception Raised while creating Contact ##' + ex.getMessage());
                    }
                }
            } 
        }
        if(String.isNotBlank(caseObj.ContactId) && String.isNotBlank(assetNumber)) {
            // Check for exising asset
            Asset exisingAsset = getAssetInfo(assetNumber);
            if(exisingAsset != null) {
                caseObj.AssetId = exisingAsset.Id;
            } else {
                try {                    
                    // Fetch Product Information based on Product Name
                    Product2 existingProduct = getProductInfo(productName);
                    if(existingProduct != null) {
                        // If existing asset not available then create new asset
                        Asset assetObj = new Asset();
                        assetObj.Name = assetNumber;
                        assetObj.ContactId = caseObj.ContactId;
                        assetObj.Product2Id = existingProduct.Id;
                        assetObj.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp TechDirect Asset').getRecordTypeId();
                        insert assetObj;
                        System.debug('assetObj $$ ' + assetObj);
                        // Assign Asset Id to Case
                        caseObj.AssetId = assetObj.Id;    
                    }                    
                } catch (Exception ex) {
                    System.debug('## Exception Raised while creating Asset ##' + ex.getMessage());
                }
            }
        }System.debug(' caseObj ## ' + caseObj);
        try {
            caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IR Comp TechDirect Ask a Question').getRecordTypeId();
            caseObj.Origin = 'Community';  
            caseObj.CTS_TechDirect_End_User_Region__c = region;
            //Fetching the assignment rules on case
            List < AssignmentRule > arList = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true];
            if(arList != null && !arList.isEmpty()) {
                //Creating the DMLOptions for "Assign using active assignment rules" checkbox
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.assignmentRuleId= (arList.get(0)).Id;                  
                //Setting the DMLOption on Case instance
                caseObj.setOptions(dmlOpts);
            }            
            insert caseObj;    
        } catch(Exception ex) {
            System.debug('## Exception Raised while creating Case ##' + ex.getMessage());
        }                        
        if(caseObj != null && String.isNotBlank(caseObj.Id)) {
            System.debug('caseObj ID ## ' + caseObj.Id + ' -- ' + caseObj);    
            List < Case > caseList = [SELECT Id, CaseNumber FROM Case WHERE Id = :caseObj.Id];
            if(caseList != null && !caseList.isEmpty()) {
                caseObj = caseList.get(0);
            }
        } else {
            caseObj = null;
        }    
        return caseObj;
    }
    
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        // which is save the check data and return the attachemnt Id after insert, 
        // next time (in else) we are call the appentTOFile() method for update the attachment with reamins chunks   
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
        return Id.valueOf(fileId);
    }
 
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;
        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = fileName;
        oAttachment.ContentType = contentType;
        try {
            insert oAttachment;    
        } catch(Exception ex) {
            System.debug('Exception has occurred while uploading file ## ' + ex.getMessage());
            return null;
        }        
        return oAttachment.Id;
    }
 
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8'); 
        Attachment a = [SELECT Id, Body FROM Attachment WHERE Id =: fileId];
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
        update a;
    }
}