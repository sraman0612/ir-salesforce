/*
* Revisions:
18-Nov-2024 : Update code against Case #03273773 - AMS Team
*/
@IsTest
private class LGD_ApprovalProcessTest {
    @IsTest
    public static void unitTest1(){
        User u = CTS_TestUtility.createUser(true);
        u.Email='xavier.duboc@irco.com';
        update u;
        Account a1 = new Account(Name = 'Test Account');
        a1.Currency__c = 'USD';
        a1.ShippingCountry = 'USA';
        a1.ShippingStreet = '123';
        a1.ShippingPostalCode = '123';
        a1.ShippingCity = 'test';
        a1.ShippingState = 'CA';
        a1.CTS_Global_Shipping_Address_1__c = '13';
        a1.CTS_Global_Shipping_Address_2__c = 'street2';          
        a1.County__c = '123';
        a1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert a1;
        List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 1000000;
        op1.closedate = system.today();
        op1.LGD_Channel__c='Direct';
        op1.Region__c='France';
        op1.Process__c='Special price request';
        op1.Product__c='Standard';
        op1.Business__c='Compressor';
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        op1.AccountId = a1.Id;
        op1.List_Price__c=100;
        op1.Discounted_Price__c=96;
        oppt1.add(op1);
        insert oppt1;
        List<ID> oppId = new List<ID>();
        oppId.add(oppt1[0].id);
        System.runAs(u){
            test.startTest();
            LGD_ApprovalProcess.updateApprover(oppId);
            test.stopTest();
        }
    }
    @IsTest
    public static void unitTest2(){
        User u = CTS_TestUtility.createUser(true);
        u.Email='frederic_roux@irco.com';
        update u;
        Account a1 = new Account(Name = 'Test Account');
        a1.Currency__c = 'USD';
        a1.ShippingCountry = 'USA';
        a1.ShippingStreet = '123';
        a1.ShippingPostalCode = '123';
        a1.ShippingCity = 'test';
        a1.ShippingState = 'CA';
        a1.CTS_Global_Shipping_Address_1__c = '13';
        a1.CTS_Global_Shipping_Address_2__c = 'street2';          
        a1.County__c = '123';
        a1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert a1;
        List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 1000000;
        op1.closedate = system.today();
        op1.LGD_Channel__c='Direct';
        op1.Region__c='France';
        op1.Process__c='Special price request';
        op1.Product__c='Standard';
        op1.Business__c='Compressor';
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        op1.AccountId = a1.Id;
        op1.List_Price__c=100;
        op1.Discounted_Price__c=88;
        oppt1.add(op1);
        insert oppt1;
        List<ID> oppId = new List<ID>();
        oppId.add(oppt1[0].id);
        System.runAs(u){
            test.startTest();
            LGD_ApprovalProcess.updateApprover(oppId);
            test.stopTest();
        }
    }
    @IsTest
    public static void unitTest3(){
        User u = CTS_TestUtility.createUser(true);
        u.Email='rajesh_ganjoo@irco.com';
        update u;
        Account a1 = new Account(Name = 'Test Account');
        a1.Currency__c = 'USD';
        a1.ShippingCountry = 'USA';
        a1.ShippingStreet = '123';
        a1.ShippingPostalCode = '123';
        a1.ShippingCity = 'test';
        a1.ShippingState = 'CA';
        a1.CTS_Global_Shipping_Address_1__c = '13';
        a1.CTS_Global_Shipping_Address_2__c = 'street2';          
        a1.County__c = '123';
        a1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert a1;
        List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 1000000;
        op1.closedate = system.today();
        op1.LGD_Channel__c='Direct';
        op1.Region__c='France';
        op1.Process__c='Special price request';
        op1.Product__c='Standard';
        op1.Business__c='Compressor';
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        op1.AccountId = a1.Id;
        op1.List_Price__c=100;
        op1.Discounted_Price__c=35;
        oppt1.add(op1);
        insert oppt1;
        List<ID> oppId = new List<ID>();
        oppId.add(oppt1[0].id);
        System.runAs(u){
            test.startTest();
            LGD_ApprovalProcess.updateApprover(oppId);
            test.stopTest();
        }
    }
    /* Commented the Emco code for Case #03273773 18 Nov 20204
      @IsTest
    public static void unitTest4(){
        User u = CTS_TestUtility.createUser(true);
        u.Email='test@gmail.com';
        update u;
        Account a1 = new Account(Name = 'Test Account');
        a1.Currency__c = 'USD';
        a1.ShippingCountry = 'USA';
        a1.ShippingStreet = '123';
        a1.ShippingPostalCode = '123';
        a1.ShippingCity = 'test';
        a1.ShippingState = 'CA';
        a1.CTS_Global_Shipping_Address_1__c = '13';
        a1.CTS_Global_Shipping_Address_2__c = 'street2';          
        a1.County__c = '123';
        a1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
        insert a1;
        Business_Relationship__c BR= New Business_Relationship__c();
        BR.Account__c=a1.Id;
        BR.Business__c='Emco Loading Arms';
        BR.Type__c='Prospect';
        BR.CTS_Channel__c='Direct';
        insert BR;
        //List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 100000;
        op1.ForecastCategoryName='Omitted';
        op1.Confidence__c='0';
        op1.Was_this_the_result_of_an_assessment__c='No';
        op1.closedate = system.today();
        op1.LGD_Channel__c='Direct';
        //op1.EW_Manufacturing_Location__c='Germany';
        op1.Region__c='Germany';
        op1.CTS_Product__c='LLS';
        op1.Process__c='Approval';
        op1.Product__c='Standard';
        op1.Business__c='Emco Loading Arms';
        op1.Comment__c='test';
        op1.EW_Manufacturing_Location__c='Germany';
        op1.EW_Product_Group__c='LLS';
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
        op1.AccountId = a1.Id;
        //oppt1.add(op1);
        insert op1;
        List<ID> oppId = new List<ID>();
        oppId.add(op1.id);
        System.runAs(u){
            test.startTest();
            LGD_ApprovalProcess.updateApprover(oppId);
            test.stopTest();
        }
    }*/
}