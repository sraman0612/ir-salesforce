global class CC_PavilionEventsCalendarController {
  public Static Id accId {get;set;} 
    public CC_PavilionEventsCalendarController(CC_PavilionTemplateController controller) {
        accId =controller.acctId;
}

        
public CC_PavilionEventsCalendarController() {
}

    /* remote action to display bulletins */
@RemoteAction
global static List<CC_Pavilion_Content__c> getPavilionEventsContent() {
        
        Set<String> SubType = new Set<String>();
        CC_Pavilion_Content__c [] Eventlist = new List<CC_Pavilion_Content__c>();
    
        User u=[Select AccountId from User where id=:userinfo.getUserId()];
        Account acc=[Select Id,CC_Global_Region__c from Account where id=:u.AccountId];
        string gRegion = acc.CC_Global_Region__c;
          
          
          for(Contract Con : [SELECT Id, AccountId, CC_Sub_Type__c, CC_Type__c 
                                from Contract 
                               where AccountId = : accid AND CC_Type__c = 'Dealer/Distributor Agreement' 
                               and CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired']){
              SubType.add(con.CC_Sub_Type__c);
          }
          
          
        
         for(CC_Pavilion_Content__c PC : [SELECT Id,Title__c,Agreement_Type__c,CC_Region__c,
                                         Summary__c,CC_Marketing_Event_Date__c, CC_Marketing_Event_Type__c, 
                                         CC_Marketing_Event_End_Date__c,Link__c, Resource_Link__c,Type__c 
                                         FROM CC_Pavilion_Content__c 
                                         WHERE Type__c ='Events Calendar' and RecordType.Name = 'Events'])
                                           
             {
                  if(PC.Agreement_Type__c != null && PC.Agreement_Type__c != '' && PC.CC_Region__c != '' && PC.CC_Region__c != NULL){
                      
                      List<String> agreementTypeList = new List<String>();
                      agreementTypeList = PC.Agreement_Type__c.split(';');
                      
                      for(String agType : agreementTypeList) {
                          if(SubType.contains(agType) && PC.CC_Region__c.contains(gRegion)){
                              Eventlist.add(PC);
                              break;
                          }
                      } 
                  }
              }              
        
        return Eventlist;
    }
}