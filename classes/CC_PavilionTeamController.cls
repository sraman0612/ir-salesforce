/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */

/* This Class User for  CC_PavilionTeam page*/
global with sharing class CC_PavilionTeamController {

 /* member variables */
 public Boolean showMyTeam {
  get;
  set;
 }
 public Account Accname {
  get;
  set;
 } //holds the account name to display that on the vf page.
 public User Usr {
  get;
  set;
 } // holds the account id of the loggedin user and used to populate the value to the vf as well.
 Public Contact con {
  get;
  set;
 } //holds contact details
 /* constructor */
 public CC_PavilionTeamController(CC_PavilionTemplateController controller) {
   showMyTeam = null == ApexPages.currentPage().getParameters().get('dpt') ? TRUE : FALSE;
   Usr = [Select AccountId, Phone, ContactId from User where Id = : userinfo.getUserId()]; //getting all user credentials
   Accname = [Select name, CC_Customer_Number__c from Account where id = : Usr.AccountId]; //getting all account credentials
   con = [Select Id, Phone from Contact where id = : Usr.ContactId]; //getting all contact credentials

  }
  /* Remote Action method Get the team members list based account id and department */
 @RemoteAction
 global static List < CC_My_Team__c > getMyTeam(String d) {
  Id acctId = [Select AccountId from User where Id = : userinfo.getUserId()].AccountId;
  String dept = String.escapeSingleQuotes(d);
  String queryStr = 'SELECT Id,Account__c,Contact__c,Custom_Department__c,Custom_Display_Name__c,Custom_Email__c,Custom_Fax__c, ' +
   'Custom_My_Team_Role__c,Custom_Phone__c,Displayed_Department__c,Displayed_Email__c,Displayed_Fax__c,' +
   'Displayed_My_Team_Role__c,Displayed_Name__c,Displayed_Phone__c,Hide_Department__c,Hide_Fax__c,My_Team_Role__c ' +
   'FROM CC_My_Team__c  WHERE Account__c=:acctId';
  queryStr += dept == '' ? ' ' : ' AND Displayed_Department__c=:dept ';
  queryStr += 'ORDER BY Id';
  return Database.query(queryStr);
 }


}