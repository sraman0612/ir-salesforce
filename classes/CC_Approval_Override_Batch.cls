public without sharing class CC_Approval_Override_Batch implements Database.Batchable<sObject>{
    
    private class CC_Approval_Override_BatchException extends Exception{}
    private String objectName;
    @TestVisible private static final String approvalOverrideComment = ' via manager approval override by ';

    public CC_Approval_Override_Batch(String objectName){

        this.objectName = objectName;

        if (String.isBlank(objectName)){
            throw new CC_Approval_Override_BatchException('Object name is required.');
        }
    }
    
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        // Find all records that are flagged for approval override
        return Database.getQueryLocator(
            'Select Id, ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName) + 
            ' From ' + String.escapeSingleQuotes(objectName) + 
            ' Where ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideFieldName) + ' = true'
        );
   	}

   	public void execute(Database.BatchableContext BC, List<sObject> records){   		   		
   		
        Map<Id, sObject> recordMap = new Map<Id, SObject>();

        for (sObject record : records){
            recordMap.put(record.Id, record);
        }

        // Build a map of records to override requests and collect each override user Id
        Map<Id, CC_Manage_Approvals_Controller.ApprovalOverrideRequest> recordOverrideRequestMap = new Map<Id, CC_Manage_Approvals_Controller.ApprovalOverrideRequest>();
        Set<Id> overrideUsers = new Set<Id>();

        for (sObject record : records){
            
            system.debug('processing record: ' + record);

            CC_Manage_Approvals_Controller.ApprovalOverrideRequest overrideRequest = (CC_Manage_Approvals_Controller.ApprovalOverrideRequest)JSON.deserialize((String)record.get(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName), CC_Manage_Approvals_Controller.ApprovalOverrideRequest.class);
            recordOverrideRequestMap.put(record.Id, overrideRequest);
            overrideUsers.add(overrideRequest.overrideUserId);
        }

        // Get the name from the override users to display in the comments
        Map<Id, User> userMap = new Map<Id, User>([Select Id, Name From User Where Id in :overrideUsers]);

        // Build the approval requests
        Approval.ProcessWorkitemRequest[] approvalOverrideRequests = new Approval.ProcessWorkitemRequest[]{};

   		for (sObject record : records){

            CC_Manage_Approvals_Controller.ApprovalOverrideRequest overrideRequest = recordOverrideRequestMap.get(record.Id);            

            Approval.ProcessWorkitemRequest req = overrideRequest.pwr;
            User u = userMap.get(overrideRequest.overrideUserId);
            req.setComments((req.getAction() == 'Approve' ? 'Approved' : 'Rejected') + approvalOverrideComment + ' ' + u.Name + ' with comments: \n' + req.getComments());
            approvalOverrideRequests.add(req);   	
   		}

		Approval.ProcessResult[] approvalResults = sObjectService.processApprovalRequestsAndLogErrors(approvalOverrideRequests, 'CC_Approval_Override_Batch', 'excecute: approvals');
			
        system.debug('approvalResults: ' + approvalResults);

		// Clear the approval override flag and request fields for successful approval records
        sObject[] recordsToUpdate = new sObject[]{};

		for(Approval.ProcessResult approvalResult : approvalResults){

            if (approvalResult.isSuccess()){               

                sObject record = recordMap.get(approvalResult.getEntityId());
                record.put(CC_Manage_Approvals_Controller.approvalOverrideFieldName, false);
                record.put(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName, null);
                recordsToUpdate.add(record);
            }
        }
        
        system.debug('recordsToUpdate: ' + recordsToUpdate);

        sObjectService.updateRecordsAndLogErrors(recordsToUpdate, 'CC_STL_Approval_Override_Batch', 'execute: record update'); 		
   	}

   	public void finish(Database.BatchableContext BC){
		
   	}    
}