@isTest
public with sharing class Font_Awesome_Icons_Preview_ControllerTst {

    @isTest
    private static void getIconsTest(){

        Font_Awesome_Icons_Preview_Controller ctrl = new Font_Awesome_Icons_Preview_Controller();
        system.assert(ctrl.getIcons().size() > 0);
    }
}