/****************************************************************
Name:      RS_CRS_OpenConcessionGuide_Controller
Purpose:   Open concession guide
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Ashish Takke	01/02/2018		INITIAL DEVELOPMENT
*****************************************************************/
public with sharing class RS_CRS_OpenConcessionGuide_Controller {
	
    /******************************************************************************
	Purpose            :  Method used to fetch concession guide
	Parameters         :  NA
	Returns            :  nothing
	Throws [Exceptions]:                                                          
	******************************************************************************/
    @AuraEnabled
    public static String fetchConcessionGuideId(){
        //get ID of concession guide file
        ContentDocument contDoc = [SELECT Id, Title 
                                   FROM ContentDocument 
                                   WHERE Title =: Label.RS_CRS_Concession_Guideline];
        //return concession guide id
        return contDoc.Id;
    }
}