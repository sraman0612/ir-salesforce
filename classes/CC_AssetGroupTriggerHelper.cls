public class CC_AssetGroupTriggerHelper {

  public static void updatenewoldvendorfields(List<CC_Asset_Group__c>assetGroupList){        
    Set<Id> accIdSet = new Set<Id>();
    Map<Id,CC_Asset_Group__c> accGolfMap  = new Map<Id,CC_Asset_Group__c>();
    Map<Id,CC_Asset_Group__c> accUtilMap  = new Map<Id,CC_Asset_Group__c>();
    List<account> accountstoUpdate        = new List<account> ();
    for(CC_Asset_Group__c  asg : assetGroupList){
      if(asg.Account__c != NULL ){
        accIdSet.add(asg.Account__c);
        if(asg.Fleet_Type_Formula__c == 'Golf'){
          accGolfMap.put(asg.Account__c,asg);
        } else if (asg.Fleet_Type_Formula__c == 'Utility') {
          accUtilMap.put(asg.Account__c,asg);
        }
      }
    }    
    for(Account acc : [SELECT Id,Name ,CC_Golf_New_Vendor__c,CC_Golf_Old_vendor__c,CC_Golf_Old_Vendor_Acquire_Date__c,
                              CC_Golf_New_Vendor_Acquire_Date__c, CC_Utility_New_Vendor__c,CC_Utility_Old_vendor__c,
                              CC_Utility_Old_Vendor_Acquire_Date__c, CC_Utility_New_Vendor_Acquire_Date__c,
                              CC_Golf_New_Vendor_Fleets__c,CC_Golf_Old_Vendor_Fleets__c,CC_Utility_New_Vendor_Fleets__c,
                              CC_Utility_Old_Vendor_Fleets__c 
                         FROM Account
                        WHERE ispersonaccount = false and Id IN:accIdSet]){
      CC_Asset_Group__c  asg;
      Boolean updateAccount = false;
      if(accGolfMap.containsKey(acc.id)){
        asg =  accGolfMap.get(acc.id);
        If(asg.Vendor_Formula__c != acc.CC_Golf_New_Vendor__c) {
          acc.CC_Golf_Old_vendor__c              = acc.CC_Golf_New_Vendor__c;
          acc.CC_Golf_Old_Vendor_Acquire_Date__c = acc.CC_Golf_New_Vendor_Acquire_Date__c;
          acc.CC_Golf_Old_Vendor_Fleets__c = acc.CC_Golf_New_Vendor_Fleets__c;
          acc.CC_Golf_New_Vendor__c              = asg.Vendor_Formula__c;
          acc.CC_Golf_New_Vendor_Acquire_Date__c = asg.Acquired_date__c;
          acc.CC_Golf_New_Vendor_Fleets__c = 1;
        } else {
          if(null!=acc.CC_Golf_New_Vendor_Fleets__c){
            acc.CC_Golf_New_Vendor_Fleets__c += 1;
          } else {
            acc.CC_Golf_New_Vendor_Fleets__c = 1;
          }
        }
        updateAccount = true;
      }
      if(accUtilMap.containsKey(acc.id)){
        asg =  accUtilMap.get(acc.id);
        if(asg.Vendor_Formula__c != acc.CC_Utility_New_Vendor__c){
          acc.CC_Utility_Old_vendor__c              = acc.CC_Utility_New_Vendor__c;
          acc.CC_Utility_Old_Vendor_Acquire_Date__c = acc.CC_Utility_New_Vendor_Acquire_Date__c;
          acc.CC_Utility_Old_Vendor_Fleets__c = acc.CC_Utility_New_Vendor_Fleets__c;
          acc.CC_Utility_New_Vendor__c              = asg.Vendor_Formula__c;
          acc.CC_Utility_New_Vendor_Acquire_Date__c = asg.Acquired_date__c;
          acc.CC_Utility_New_Vendor_Fleets__c = 1;
        } else {
          if(null != acc.CC_Utility_New_Vendor_Fleets__c){
            acc.CC_Utility_New_Vendor_Fleets__c += 1;
          } else {
            acc.CC_Utility_New_Vendor_Fleets__c = 1;
          }
        }
        updateAccount = true;
      }
      if(updateAccount){accountstoUpdate.add(acc);}
    } 
    if(!accountstoUpdate.isEmpty()){update accountstoUpdate;}
  }
}