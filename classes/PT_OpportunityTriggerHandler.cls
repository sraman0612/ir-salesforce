/*

Purpose: Migrated from PowerTools Org 
Date: Feb 28, 2018 By CloudGofer.com

Mar 13, 2018: CloudGofer: Added code for summing up open amount from Opportunity to Account Total Open Amount

*/

public class PT_OpportunityTriggerHandler {
   
    public static void setOpportunityCampaign(Opportunity[] opportunities)
    {
        if(!opportunities.isEmpty()){
            //before update
            Opportunity[] opportunitiesToUpdate = new Opportunity[]{};
            Set<Id> parentOpportunityIds = new Set<Id>();
            
            for(Opportunity thisOpportunity: opportunities)
            {
                if(thisOpportunity.amount > 0)
                {
                    opportunitiesToUpdate.add(thisOpportunity);
                    parentOpportunityIds.add(thisOpportunity.PT_Parent_Opportunity__c);
                }
            }
            
            Map<Id,PT_Parent_Opportunity__c> opportunityParents = new Map<Id,PT_Parent_Opportunity__c>(
                [Select Id, PT_Campaign__c from PT_Parent_Opportunity__c where Id IN :parentOpportunityIds AND PT_Campaign__c != null]
            );
            
            for(Opportunity thisOpportunity: opportunitiesToUpdate)
            {
                if(opportunityParents.containsKey(thisOpportunity.PT_Parent_Opportunity__c))
                {
                    thisOpportunity.CampaignId = opportunityParents.get(thisOpportunity.PT_Parent_Opportunity__c).PT_Campaign__c;
                }
            }
        }
    }
    
  	 // OppAftTrig on Opportunity (after update)
    public static void afterUpdate(List<Opportunity> newOppties, Map<Id,Opportunity> oldOpptiesMap) {
        List<id> parentIds = new List<id>();
        Set<Id> accountIds = new Set<Id>();
        Map<Id,Decimal> accountIdOpenAmountMap = new Map<Id,Decimal>(); // added for summing up opportunity amount to Account
         
        for(Opportunity op : newOppties){
            
            System.debug('Providity:oldOpptiesMap.get(op.id).amount:'+oldOpptiesMap.get(op.id).amount);
            System.debug('Providity:op.Amount:'+op.Amount);
            if(oldOpptiesMap.get(op.id).amount!=op.Amount || oldOpptiesMap.get(op.id).isclosed!=op.isclosed ){            
                parentIds.add(op.PT_parent_opportunity__c);
                accountIds.add(op.accountId);
            }
        }
        if(!parentIds.isEmpty()){
            List<PT_Parent_Opportunity__c> paOpps = [select id,Total_Open_Amount__c,Total_Amount__c from PT_Parent_Opportunity__c where id in :parentIds];
            Map<id,PT_Parent_Opportunity__c> paUp = new Map<id,PT_Parent_Opportunity__c>();
            List<Opportunity> opps = [select id, AccountId, PT_parent_opportunity__c,amount,stagename, isclosed from opportunity where PT_parent_opportunity__c in :parentIds];
            system.debug('paOpps '+paOpps);
            List<PT_OppStages__c> stages = PT_OppStages__c.getall().values();
            set<string> setStages = new set<string>(); 
            for(PT_OppStages__c st : stages){
                setStages.add(st.name);
            }
            
            for(PT_Parent_Opportunity__c pa : paOpps){
                decimal total = 0;
                decimal totalOpen = 0;
                for(opportunity op : opps){
                    if(pa.id==op.PT_Parent_Opportunity__c && op.Amount!=null){
                        if(setStages.contains(op.StageName)){
                            total += op.Amount;
                        }
                        
                        if(op.IsClosed == false){
                            totalOpen += op.Amount;
                        }
                    }
                }
                pa.Total_Amount__c = total;
                pa.Total_Open_Amount__c = totalOpen;
                paUp.put(pa.id,pa);
            }
            
            if(!paUp.isEmpty()){
                update paUp.values();
            }

        } // end of !parentIds.isEmpty())
         
    }
    
    
    
    // OppPusher on Opportunity (before update)
    public static void OppPusher(List<Opportunity> newOppties, Map<Id,Opportunity> oldOpptiesMap) {
        Date dNewCloseDate;
        Date dOldCloseDate;
        Boolean bPushed=false;
        
        for (Opportunity oIterator : newOppties) { //Bulk trigger handler so that you can mass update opportunities and this fires for all'
                                                    // gets new values for updated rows
            dNewCloseDate = oIterator.CloseDate; // get the new closedate 
            dOldCloseDate = oldOpptiesMap.get(oIterator.Id).CloseDate; //get the old closedate for this opportunity
            if (dOldCloseDate<dNewCloseDate) { //if the new date is after the old one, look if the month numbers are different
                if (dOldCloseDate.month()<dNewCloseDate.month()) { // the month number is higher, it's been pushed out
                    bPushed=true;
                }
                else {
                    if (dOldCloseDate.year()<dNewCloseDate.year()) { // the month wasn't higher, but the year was, pushed!
                        bPushed=true;
                        }
                    }
                
            }
            if (bPushed==true) { // let's go make them sorry
                if (oIterator.PT_PushCount__c==null) {
                    oIterator.PT_PushCount__c=1;
                }
                else {
                oIterator.PT_PushCount__c++;          
                }
            } 
        }
    }
    
    // Prevent deletion of opportunities by PT Standard user profile
    public static void preventDeletion(List<Opportunity> newOppties, Map<Id,Opportunity> oldOpptiesMap) {
      Id ptStandardUserProfileId = [SELECT Id From Profile WHERE Name='PT Standard User'].Id;
      if(userINfo.getProfileId()==ptStandardUserProfileId){
        for (Opportunity o : oldOpptiesMap.values()) {
            o.addError('Users in PT Standard User Profile cannot delete Opportunities');
        }
      }
    }        
}