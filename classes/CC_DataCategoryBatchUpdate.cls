global class CC_DataCategoryBatchUpdate implements Schedulable, Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        Id recType = CC_KnowledgeTriggerHandler.getCCRecordTypeId();
        return Database.getQueryLocator([SELECT Id, Title, RecordTypeId 
                                         FROM Knowledge__kav
                                         WHERE RecordTypeId = :rectype]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        CC_KnowledgeTriggerHandler.SetCCDataCategory(scope);
    }

    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new CC_DataCategoryBatchUpdate(), 100);    
    }

}