/**
    Helper/Utility class for CC_KBContentSearchControllerTest
*/

public class CC_Test_Utility {

    public static knowledge__kav createKnowledge(String title, String urlName, String description, Boolean isInsert){
        knowledge__kav knowledge= new knowledge__kav(title= title, urlName = urlName , RS_CRS_Description__c = description, CTS_TechDirect_Article_Type__c = 'Test',
                                   language='en_US');
        knowledge.recordTypeId = Schema.SObjectType.knowledge__kav.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        if(isInsert){
            insert knowledge;
        }
        return knowledge;
    }
    
     public static ContentVersion createContentVersion(String title, String description, String contentUrl,  Id recordid,Boolean isInsert){
        ContentVersion contentVersionObj = new ContentVersion(title= title, contenturl = contenturl, RecordTypeId=recordid);
        if(isInsert){
            insert contentVersionObj;
        }
        return contentVersionObj;
    }
    
    public static CC_kb_content_search_config__c createSearchConfig(){
    
        CC_kb_content_search_config__c searchConfig = new CC_kb_content_search_config__c(available_options_in_the_page_size__c = '5,10,20,30', Default_page_size__c=5,show_search_filter_options__c = false,
                            Image_files__c = 'AI,EPS,GIF,JPEG,JPG,PNG', Video_files__c = 'M4V,MOV,MP4,MPG,MOV', Music_files__c = 'MP3',Link_files__c = 'LINK', Record_Type__c = 'Club Car');
        insert searchConfig;
        return searchConfig;
    }
}