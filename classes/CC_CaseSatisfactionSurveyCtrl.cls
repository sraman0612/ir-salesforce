/**
    @author Ben Lorenz
    @date 19DEC18
    @description Controller class for CC_CaseSatSurveyVF page. Records user feedback
*/
public without sharing class CC_CaseSatisfactionSurveyCtrl {    
    
    public List<String> levels{get;set;}
    public Case c {get;set;}
    public String msgs {get;set;}
    
    public CC_CaseSatisfactionSurveyCtrl() {        
        init();                
    }
    
    private void init() {
        //Check if it's a valid case Id
        Map<String,String> params = ApexPages.currentPage().getParameters();
        if(params.containskey('cId')) {
            try {
                c = [SELECT Id, CC_Case_Survey_Satisfaction_Level__c, CaseNumber FROM Case WHERE Id =: params.get('cId')];
            } catch(Exception e) {
                System.debug('Most probably, invalid ID: ' + e.getMessage());
                msgs = System.Label.CC_Survey_CaseClosed_Error_IncorrectId;
                return;
            }
        } else {
            msgs = System.Label.CC_Survey_CaseClosed_Error_IncorrectId;
            return;
        } 
        //and hasn't been surveyed already
        if(!String.isBlank(c.CC_Case_Survey_Satisfaction_Level__c)) {
            msgs = System.Label.CC_Survey_CaseClosed_Error_SurveyCompleted;
        }
        
        //Satisfaction levels. Read from the field (CC_Case_Survey_Satisfaction_Level__c) definition
        levels = new List<String>();       
                
        Schema.DescribeFieldResult fieldResult = Case.CC_Case_Survey_Satisfaction_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
        for( Schema.PicklistEntry f : ple) {
             levels.add(f.getValue());
        }
    }
    
    @AuraEnabled
    @RemoteAction
    public static String submitFeedback(String recordId, String selectedLevel, String comments) {
        Case c;
        try {
            c = [SELECT Id, CC_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: recordId];
        } catch(Exception e) {
            System.debug('Most probably, invalid ID: ' + e.getMessage());
            return System.Label.CC_Survey_CaseClosed_Error_IncorrectId;
        }
        if(!String.isBlank(c.CC_Case_Survey_Satisfaction_Level__c)) {
            return System.Label.CC_Survey_CaseClosed_Error_SurveyCompleted;
        }
        
        c.CC_Case_Survey_Satisfaction_Level__c = selectedLevel;
        c.CC_Case_Survey_Comments__c = comments;
        try {
            update c;
        } catch(Exception e) {
            return e.getMessage();
        }
        return System.Label.CC_Survey_CaseClosed_FeedbackRecorded;
    }
}