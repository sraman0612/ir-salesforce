/**
 * Test class of CTS_IOT_Data_Service_WithoutSharing
 **/
@isTest
public class CTS_IOT_Data_Service_WithoutSharingTest {

    @testSetup
    static void setup(){
UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        List<Account> accounts = new List<Account>();
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        accounts.add(acc);
        accounts.add(CTS_TestUtility.createAccount('Test Account 1', false));
        insert accounts;
        
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(false);
        setting.Account_Matching_Record_Types__c = accounts.get(0).recordTypeId;
        setting.Asset_Matching_Record_Types__c = assetRecTypeId;
        setting.Asset_Matching_Record_Types2__c = assetRecTypeId;
        insert setting;

        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', accounts.get(0).Id, assetRecTypeId, false);
        asset.IRIT_RMS_Flag__c = true;
        insert asset;

        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', accounts.get(0).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', accounts.get(0).Id, false));

        for(Contact con : contacts){
            con.CTS_IOT_Community_Status__c = 'Approved';
        }
        insert contacts;

        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(contacts.get(0).Id, profileId, true);     
    }
}
    @isTest
    static void testUserMehtods(){

        List<Contact> contacts = [select id, Name from Contact where lastname = 'Contact123'];

        if(!contacts.isEmpty()){

            Test.startTest();

            User usr = CTS_IOT_Data_Service_WithoutSharing.lookupUserbyContact(contacts.get(0).Id);
            System.assert(usr != null);

            usr = CTS_IOT_Data_Service_WithoutSharing.lookupUser(usr.Id);
            System.assert(usr != null);

            Boolean isSuperUser = CTS_IOT_Data_Service_WithoutSharing.getIsSuperUser(usr.Id);
            CTS_IOT_Data_Service_WithoutSharing.updateUser(usr);

            System.runAs(new User(Id = UserInfo.getUserId())){
                CTS_IOT_Data_Service_WithoutSharing.updateContact(contacts.get(0));
                CTS_IOT_Data_Service_WithoutSharing.deactivateUser(contacts.get(0).Id);
                CTS_IOT_Data_Service_WithoutSharing.activateUser(usr.Id);
            }

            Test.stopTest();
        }
    }

    @isTest
    static void testGetIsSandbox(){
        Test.StartTest();
        CTS_IOT_Data_Service_WithoutSharing.getIsSandbox();
        Test.StopTest();
    }

    @isTest
    static void testGetContactsByAccount(){

        List<Contact> contacts = [SELECT id, AccountId FROM Contact WHERE lastName = 'Contact123' LIMIT 1];

        if(!contacts.isEmpty()){

           Test.startTest();

           CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse contactResponse =  CTS_IOT_Data_Service_WithoutSharing.getContacts(contacts.get(0).Id);
           System.assert(contactResponse != null);
           contactResponse =  CTS_IOT_Data_Service_WithoutSharing.getContacts(contacts.get(0).accountId);

           Integer siteContactsCount = CTS_IOT_Data_Service_WithoutSharing.getContactsCount(null, contacts[0].AccountId);
           System.assert(siteContactsCount > 0);

           Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = contacts[0].Id;
           Test.setFixedSearchResults(fixedSearchResults);           

           Integer siteContactsCount2 = CTS_IOT_Data_Service_WithoutSharing.getContactsCount('Test', contacts[0].AccountId);
           System.assert(siteContactsCount2 > 0);

           Test.stopTest();
        }
    }

    @isTest
    static void testGetContactsByContact(){

        List<Contact> contacts = [SELECT id, AccountId FROM Contact WHERE lastName = 'Contact123' LIMIT 1];

        if(!contacts.isEmpty()){

           Test.startTest();
           CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse contactResponse =  CTS_IOT_Data_Service_WithoutSharing.getContacts(contacts.get(0).Id);
           System.assert(contactResponse != null);
           contactResponse =  CTS_IOT_Data_Service_WithoutSharing.getContacts(contacts.get(0).Id);
           Test.stopTest();
        }
    }   

    @isTest
    static void testGetSuperUsersInHierarchy(){

        List<Account> accounts = [SELECT id  FROM Account WHERE Name = 'Test Account' LIMIT 1];

        if(!accounts.isEmpty()){

           Test.startTest();
           User[] usrList =  CTS_IOT_Data_Service_WithoutSharing.getSuperUsersInHierarchy(accounts.get(0));
           System.assert(usrList != null);
           Test.stopTest();
        }
    }
    
    @isTest
    static void testSiteUserMethods(){

        List<User> users = [SELECT id, ContactId, AccountId FROM User WHERE contact.lastName = 'Contact123' LIMIT 1];

        if(!users.isEmpty()){

           Test.startTest();

           List<Account> accountList = [select id from Account where Name ='Test Account 1'];

            if(!accountList.isEmpty()){

               CTS_IOT_Data_Service_WithoutSharing.removeSitesFromContact(users.get(0).ContactId, new Set<Id>{accountList.get(0).Id});
               CTS_IOT_Data_Service_WithoutSharing.addSitesToUser(users.get(0).Id, new List<Id>{accountList.get(0).Id});
               Integer count = CTS_IOT_Data_Service_WithoutSharing.checkContactAccountRelation(users.get(0).ContactId, accountList.get(0).Id);
               System.assert(count == 1);
            }
            Test.stopTest();
        }
    }
   
    @isTest
    static void testIsContactInAssetHierarhcy(){

        List<User> users = [SELECT id, ContactId, AccountId FROM User WHERE contact.lastName = 'Contact123' LIMIT 1];

        if(!users.isEmpty()){

            Test.startTest();

            List<Account> accountList = [select id,Bill_To_Account__c from Account where Name ='Test Account 1'];

            if(!accountList.isEmpty()){
               CTS_IOT_Data_Service_WithoutSharing.isContactInAssetHierarhcy(users.get(0).ContactId,accountList.get(0));
            }

            Test.stopTest();
        }
    }
    
    @isTest
    static void testRegisterUser(){

        CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest userRequest = new CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest();

        List<Account> accountList = [select id from Account where Name ='Test Account 1'];

        if(!accountList.isEmpty()){

            userRequest.siteId = accountList.get(0).Id;
            userRequest.firstName = 'test';
            userRequest.lastName = 'user1';
            userRequest.username = 'testuser1@cts.com1';
            userRequest.email = 'testuser1@cts.com';
            userRequest.title = 'testUser';        
         }

         CTS_IOT_Data_Service_WithoutSharing.RegisterUserResponse userResponse = CTS_IOT_Data_Service_WithoutSharing.registerUser(userRequest);
         System.assert(userResponse != null);
    }
}