/**
    Class to hold constant value for CC_KBContentSearchController and it's related classes
*/

public class CC_KBContent_Constants {
    public static final String KNOWLEDGE_OBJECT = 'Knowledge__kav';
    public static final String KNOWLEDGE = 'Knowledge';
    public static final String BULLETINS = 'Bulletins';
    public static final String CONTENT = 'Content';
    public static final String CONTENT_VERSION = 'ContentVersion';
    public static final String AUTO = 'Auto';
    public static final String USER_SELECTED = 'User-Selected';
    public static final String EQUALS  = 'Equals';
    public static final String NOT_EQUALS = 'Not Equals';
    public static final String CONTANS = 'Contains';
    public static final String NOT_CONTAINS = 'Not Contains';    
}