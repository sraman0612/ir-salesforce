/*
*
* $Revision: 1.0 $
* $Date: $
* $Author: $
*/
@isTest
public class CC_PavilionCreditCenterHomeContTest {
static testMethod void UnitTest_CreditCenterHome()
    {    
		// Creation of test data
        CC_Pavilion_Content__c pc = TestUtilityClass.createPavilionContentWithRecordType('Credit Center Home Text','Home Page Text');
        insert pc;
        Id recordtypid = [select id from RecordType where RecordType.Name='Home Page Text'].Id;
        System.assertEquals(recordtypid, pc.RecordTypeId);
        System.assertNotEquals(null, pc.Id);
        Test.startTest();
        // instantiating CC_PavilionTemplateController
        CC_PavilionTemplateController controller;
        // instantiating CC_PavilionCreditCenterHomeController
        CC_PavilionCreditCenterHomeController CredCentHome = new CC_PavilionCreditCenterHomeController(controller);
        Test.stopTest();
        
        
}
}