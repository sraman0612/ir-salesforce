/**
	@author Ben Lorenz
	@date 24JAN19
	@description Controller for Ltng Cmp CC_GenericList.cmp
*/
public class CC_GenericListController {
	
    @AuraEnabled
    public static Object getData(String recordId) {
		try {            
        	String q = 'SELECT Id, ContentDocument.LatestPublishedVersion.ContentDocumentId, ContentDocument.LatestPublishedVersion.ContentSize, ContentDocument.LatestPublishedVersion.FileType, ContentDocument.LatestPublishedVersion.ContentModifiedDate, ContentDocument.LatestPublishedVersion.Title FROM ContentDocumentLink where LinkedEntityId =: recordId';
            List<Doc> docs = new List<Doc>();
            Doc d;
            for(ContentDocumentLink cdl: Database.query(q)) {
                d = new Doc(cdl.ContentDocument.LatestPublishedVersion.ContentDocumentId, String.valueOf(Math.round(cdl.ContentDocument.LatestPublishedVersion.ContentSize/1024)),
                           cdl.ContentDocument.LatestPublishedVersion.FileType, cdl.ContentDocument.LatestPublishedVersion.ContentModifiedDate.format(),
                           cdl.ContentDocument.LatestPublishedVersion.Title);
                docs.add(d);
            }
            
            return JSON.serialize(docs);
        } catch(Exception e) {
        	throw new AuraException(e);
        }        
    }

    public class Doc {
        @AuraEnabled public String cdId;
        @AuraEnabled public String size;
        @AuraEnabled public String type;
        @AuraEnabled public String modDate;
        @AuraEnabled public String title;
        public Doc(String cdId, String size, String type, String modDate, String title) {
            this.cdId = cdId;
            this.size = size;
            this.type = type;
            this.modDate = modDate;
            this.title = title;
        }
    }    
}