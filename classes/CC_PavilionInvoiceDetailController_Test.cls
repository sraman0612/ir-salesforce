@isTest
public class CC_PavilionInvoiceDetailController_Test {
    static testmethod void UnitTest_PavilionInvoiceDetailController(){
        List<CC_Invoice2__c> INVLists=new List<CC_Invoice2__c>();
        Account acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;
        Contact con= TestUtilityClass.createContact(acc.id);
        insert con;
        Id p = [select id from profile where name='CC_PavilionCommunityUser'].id;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',p);
        insert newPavnSettings;
        PavilionSettings__c firstPavnSettings = TestUtilityClass.createPavilionSettings('PavillionCommunityProfileId',p);
        insert firstPavnSettings;
        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        PavilionSettings__c secondPavnSettings = TestUtilityClass.createPavilionSettings('PavilionPublicGroupID',grp.id);
        insert secondPavnSettings;
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, con.Id);
        insert u;
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',acc.Id);
        insert ps;
        CC_Invoice2__c SampleInv1= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,1,'12345','14593',2000,'14593',System.today(),'Sample');
        CC_Invoice2__c SampleInv2= TestUtilityClass.getTestInvoiceDataForInvoices(acc.Id,1,'12345','74593',2000,'74593',System.today(),'Sample');
        INVLists.add(SampleInv1);
        INVLists.add(SampleInv2);
        insert INVLists;
        System.runAs(u){
          test.startTest();
          Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(u.CC_Pavilion_Navigation_Profile__c);
          insert newPavilionNavigationProfile;
          CC_PavilionTemplateController controller=new CC_PavilionTemplateController();
          controller.acctId=acc.id;
          controller.CustomerNumber='12321';
          ApexPages.currentPage().getParameters().put('invid',null);
          CC_PavilionInvoiceDetailController invoiceDetailController =new CC_PavilionInvoiceDetailController(controller);
          invoiceDetailController.SalesRep='';
          invoiceDetailController.custNum='12321';
          invoiceDetailController.lstinvoice=INVLists;    
          ApexPages.currentPage().getParameters().put('invid','14593_123');
          CC_PavilionInvoiceDetailController newInvoiceDetailController =new CC_PavilionInvoiceDetailController(controller);
          test.stopTest();
        }
    }
}