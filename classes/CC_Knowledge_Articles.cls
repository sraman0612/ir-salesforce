public class CC_Knowledge_Articles{
    @AuraEnabled
    public static List<knowledgeObject> getArticlesOnLoad(String parameter){
        Set<Id> parentSet = new Set<Id>();
        Set<Id> parentVoteSet = new Set<Id>();
        String query = 'SELECT Id,Title,CreatedDate,ArticleTotalViewCount,LastModifiedDate,UrlName'
            		 + ' FROM Knowledge__kav WHERE RecordType.Name = \''
                	 + String.escapeSingleQuotes('Club Car')+'\'';
        if(!String.isBlank(parameter)){
            if(!parameter.equals('EntitySubscription') 
               && !parameter.equals('mostHelpfulArticles')){
            	query=query+' '+parameter;
            }else if(parameter.equals('mostHelpfulArticles')){
                List<knowledge__VoteStat > votStatlst=[SELECT Id,ParentId 
                                                 FROM knowledge__VoteStat];
                for(knowledge__VoteStat enSub: votStatlst){
                	parentVoteSet.add(enSub.ParentId);
                }
                query = 'SELECT Id,Title,CreatedDate,LastModifiedDate,UrlName'
            		  + ' FROM Knowledge__kav WHERE knowledgeArticleId IN :parentVoteSet AND PublishStatus=\''
                	  + String.escapeSingleQuotes('Online')+'\' AND RecordType.Name = \''
                	  + String.escapeSingleQuotes('Club Car')+'\'';
            }else{
                List<EntitySubscription> eSublst=[SELECT ParentId 
                                                 FROM EntitySubscription 
                                                 WHERE SubscriberId =: UserInfo.getUserId()
                                                 AND Parent.Type = 'knowledge__ka'];
                for(EntitySubscription enSub: eSublst){
                	parentSet.add(enSub.ParentId);
                }
                query = 'SELECT Id,Title,CreatedDate,LastModifiedDate,UrlName'
            		  + ' FROM Knowledge__kav WHERE knowledgeArticleId IN :parentSet AND PublishStatus=\''
                	  + String.escapeSingleQuotes('Online')+'\' AND RecordType.Name = \''
                	  + String.escapeSingleQuotes('Club Car')+'\'';
            }
        }
        List<Knowledge__kav> knowledgeLst = Database.query(query);
        List<knowledgeObject> kObjectlst = new List<knowledgeObject>();
        for(Knowledge__kav kObj : knowledgeLst){
            String kURL = '/ClubCarLinks/s/article/'+kObj.UrlName;
            knowledgeObject kObject = new knowledgeObject(kObj.Title,kURL,kObj.LastModifiedDate);
            kObjectlst.add(kObject);
        }
        return kObjectlst;
    }
    
    public class KnowledgeObject{
        @AuraEnabled
        public String kName{get;set;}
        @AuraEnabled
        public String kLink{get;set;}
        @AuraEnabled
        public DateTime kLastUpdated{get;set;}
        
        public knowledgeObject(String kName,String kLink, DateTime kLastUpdated){
            this.kName = kName;
            this.kLink = kLink;
            this.kLastUpdated = kLastUpdated;
        }
    }
}