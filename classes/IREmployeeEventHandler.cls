public class IREmployeeEventHandler { 
  
  @TestVisible
  private static Set<String> federationIdsToFreeze = new Set<String>();
  
  public static void  copyDataToEmployeeFromUser(List<IR_Employee__c> empLst){
    //only process records where we have a matching user to update
    Set<String> corpIDs  = new Set<String>();
    Set<String> corpIDs2Update  = new Set<String>();
    for (IR_Employee__c ire : empLst){if(!(null==ire.Active_Directory_ID__c) && !(''==ire.Active_Directory_ID__c)){corpIDs.add(ire.Active_Directory_ID__c.toUpperCase());}}
    Map<Id, Profile> ccProfileMap = new Map<Id, Profile>([select Name, Id from Profile where name like 'CC_%']);
    for (User u : [SELECT FederationIdentifier FROM User WHERE FederationIdentifier in :corpIDs and ProfileId in :ccProfileMap.Keyset()]){
      corpIDs2Update.add(u.FederationIdentifier.toUpperCase());
    }
    if (!corpIDs2Update.isEmpty()){updateUserFromEmployee(corpIDs2Update);}
    
    // sync Employees with contacts via House Account
    //upsertContactFromEmployee(empLst);
  }
  

  @future
  public static void updateUserFromEmployee(Set<String> corpIDs2Update){
    try{
      //get IR employee records matching to the CORP Ids being updated
      Map<String, IR_Employee__c> IREmployeeMap = new Map<String, IR_Employee__c>();
      for (IR_Employee__c ire : [SELECT Active_Directory_ID__c,Address_Line_1__c,Address_Line_2__c,Business_Cell_Phone__c,Business_Title__c,
                                        Business_Unit__c,Business_Unit_Description__c,City__c,Country__c,Date_of_Termination__c,Department_Code__c,
                                        Department_Description__c,Empl_Status__c,Employee_ID__c,Employee_Type__c,First_Name__c,Job_Code__c,
                                        Job_Description__c,Last_Name__c,Location_Code__c,Location_Description__c,Middle_Name__c,Office_Phone_Number__c,
                                        Purchasing_LOA__c,State__c,Supervisor_ID__c,Supervisor_Name__c,Work_Email_Address__c,Zip_Code__c,Phone_Extension__c
                                   FROM IR_Employee__c
                                  WHERE Active_Directory_ID__c in :corpIDs2Update]){
        IREmployeeMap.put(ire.Active_Directory_ID__c.toUpperCase(),ire);
      }
      //get users matching CORP Ids and their supervisors
      Map<String,Id> supervisorMap = new Map<String, Id>();
      List<User> users2Update = new List<User>();
      for (User usr : [SELECT Id,Legacy_Id__c,Title,PS_Business_Unit__c,PS_Business_Unit_Description__c,Address,PS_Date_of_Termination__c,
                              PS_Department_Code__c,Department,PS_Employee_Status__c,PS_Employee_ID__c,PS_Employee_Type__c,FirstName,
                              PS_Job_Code__c,PS_Job_Description__c,LastName,PS_Location_Code__c,PS_Location_Description__c,PS_Middle_Name__c,
                              Phone,PS_Purchasing_LOA__c,ManagerId,Email,MobilePhone,FederationIdentifier,Extension
                         FROM User]){
        supervisorMap.put(usr.PS_Employee_ID__c,usr.Id);
        if (null!=usr.FederationIdentifier && IREmployeeMap.containsKey(usr.FederationIdentifier.toUpperCase())){users2Update.add(usr);}
      }
      //update the user records
      for (User u : users2Update){
        IR_Employee__c e = IREmployeeMap.get(u.FederationIdentifier.toUpperCase());
        u.street = e.Address_Line_1__c + e.Address_Line_2__c;
        u.MobilePhone = e.Business_Cell_Phone__c ;
        u.Title = e.Business_Title__c ;
        u.PS_Business_Unit__c = e.Business_Unit__c ;
        u.PS_Business_Unit_Description__c = e.Business_Unit_Description__c ;
        u.city = e.City__c;
        u.country = e.Country__c;
        u.PS_Date_of_Termination__c = e.Date_of_Termination__c ;
        u.PS_Department_Code__c = e.Department_Code__c ;
        u.Department = e.Department_Description__c;
        u.PS_Employee_ID__c = e.Employee_ID__c ;
        u.PS_Employee_Type__c = e.Employee_Type__c ;
        u.PS_Employee_Status__c = e.Empl_Status__c ;
        u.FirstName = e.First_Name__c ;
        u.PS_Job_Code__c = e.Job_Code__c ;
        u.PS_Job_Description__c = e.Job_Description__c ;
        u.LastName = e.Last_Name__c ;
        u.PS_Location_Code__c = e.Location_Code__c ;
        u.PS_Location_Description__c = e.Location_Description__c ;
        u.PS_Middle_Name__c = e.Middle_Name__c ;
        u.Phone = e.Office_Phone_Number__c ;
        u.Extension = e.Phone_Extension__c;
        u.PS_Purchasing_LOA__c = e.Purchasing_LOA__c;
        u.state = e.State__c;
        u.ManagerId = supervisorMap.get(e.Supervisor_ID__c);
        u.Email = e.Work_Email_Address__c ;
        u.postalcode = e.Zip_Code__c;
      }
      update users2Update;
    } catch (Exception e) {
      String cids='';
      for(String s : corpIDs2Update){cids+=s + ';';}
      apexLogHandler log = new apexLogHandler('IREmployeeEventHandler','updateUserFromEmployee',cids);
      log.logStatusCode(400);
      log.logException(e);
      log.saveLog();
    }
 
  }

  /*Commented by CG on 23-09-2022 as part of descoped record type
   	public static void upsertContactFromEmployee(List<IR_Employee__c> employees) {
      List<Contact> contactSync = new List<Contact>();
      Id houseAcctId = null;
      Id ccContactRTId = [SELECT Id FROM RecordType WHERE sobjecttype='Contact' and DeveloperName='Club_Car'].Id;
      PavilionSettings__c ps = PavilionSettings__c.getAll().get('INTERNALCLUBCARACCT');
      if (null != ps) {houseAcctId = ps.Value__c;}
      for (IR_Employee__c e :employees) {
        if(e.Business_Unit_Description__c == 'Club Car'){
          Contact c = new Contact();
          c.RecordTypeId = ccContactRTId;
          c.AccountId = houseAcctId;          
          c.CORP_ID__c = e.Active_Directory_ID__c;
          c.FirstName = e.First_Name__c ;
          c.LastName = e.Last_Name__c ;        
          c.Title = e.Business_Title__c ;
          c.MobilePhone = e.Business_Cell_Phone__c ;
          c.Phone  = (null == e.Office_Phone_Number__c ? '' : e.Office_Phone_Number__c) + ' ' +
                     (null == e.Phone_Extension__c ? '' : e.Phone_Extension__c);
          c.Email = e.Work_Email_Address__c ;
          c.Mailingstreet = (null == e.Address_Line_1__c ? '' : e.Address_Line_1__c) + ' ' +
                            (null == e.Address_Line_2__c ? '' : e.Address_Line_2__c);
          c.Mailingcity = e.City__c;
          c.Mailingstate = e.State__c;
          c.Mailingcountry = e.Country__c;
          c.MailingPostalCode = e.Zip_Code__c;
          c.IR_Employee__c = e.Id; 
          contactSync.add(c);
        }
      }
      if (!contactSync.isEmpty()) {
          upsert contactSync CORP_ID__c;
      }
  }*/
  
  	// Deactivate users based on certain criteria, freeze them if the update fails as a backup
  	public static void deactivateUsers(IR_Employee__c[] newEmployees, Map<Id, IR_Employee__c> oldMap){
  
  		User_Automation__c settings = User_Automation__c.getOrgDefaults();
  		
  		system.debug('settings: ' + settings);
  		
  		if (settings.Deactivation_Trigger_Active__c && String.isNotBlank(settings.Deactivation_Statuses__c)){
  
	  		Set<String> federatedIds = new Set<String>();
	  		
	  		for (IR_Employee__c employee : newEmployees){
	  		
	  			IR_Employee__c old = oldMap != null ? oldMap.get(employee.Id) : null;
	  			
	  			// Flag the employee for deactivation of there is a termination date assigned and the status qualifies
				if (String.isNotBlank(employee.Active_Directory_ID__c) && employee.Date_of_Termination__c != null && String.isNotBlank(employee.Empl_Status__c) && settings.Deactivation_Statuses__c.contains(employee.Empl_Status__c)){
					
					if (old == null || (employee.Date_of_Termination__c != old.Date_of_Termination__c || employee.Empl_Status__c != old.Empl_Status__c)){
						federatedIds.add(employee.Active_Directory_ID__c);
					}					
				}
	  		}
	  		
	  		system.debug('federatedIds: ' + federatedIds.size());
	  		
	  		if (!federatedIds.isEmpty()){
	  			
	  			User[] usersToDeactivate = new User[]{};
	  		
	  			// Try to find active standard users with matching federated Ids
	  			usersToDeactivate = [Select Id, Name, Email, FederationIdentifier From User Where isActive = true and UserType = 'Standard' and FederationIdentifier in :federatedIds];
	  			
	  			system.debug('usersToDeactivate: ' + usersToDeactivate.size());
	  			
	  			if (!usersToDeactivate.isEmpty()){
	  			
	  				Set<String> failedFederatedIds = new Set<String>();
	  			
	  				for (User userToDeactivate : usersToDeactivate){
	  					
	  					userToDeactivate.isActive = false;
	  					
	  					if (Test.isRunningTest() && federationIdsToFreeze.contains(userToDeactivate.FederationIdentifier)){
	  						
	  						system.debug('forcing failure to test freeze');
	  						userToDeactivate.UserName = null;
	  					}
	  				}
	  			
		  			try{
		  			
		  				Database.SaveResult[] results = Database.update(usersToDeactivate, false);
		  				
		  				Map<Id, User> userIdsToFreeze = new Map<Id, User>();
		  				
						for (Integer i = 0; i < results.size(); i++){
							
							Database.SaveResult result = results[i];
							
							User userToDeactivate = usersToDeactivate[i];
							
							system.debug('userToDeactivate: ' + userToDeactivate);
							system.debug('federationIdsToFreeze: ' + federationIdsToFreeze);
							
							if (!result.isSuccess() || (Test.isRunningTest() && federationIdsToFreeze.contains(userToDeactivate.FederationIdentifier))){
								userIdsToFreeze.put(userToDeactivate.Id, userToDeactivate);	
								system.debug('deactivation failed: ' + result.getErrors()[0].Message);		
							}
						}
						
						system.debug('userIdsToFreeze: ' + userIdsToFreeze);
						
						if (!userIdsToFreeze.isEmpty()){
							
							UserLogin[] usersToFreeze = [Select Id, UserId From UserLogin Where isFrozen = false and UserId in :userIdsToFreeze.keySet()];
							
							for (UserLogin userToFreeze : usersToFreeze){
								userToFreeze.isFrozen = true;
							}
							
			  				Database.SaveResult[] freezeResults = Database.update(usersToFreeze, false);
			  					  				
							for (Integer i = 0; i < freezeResults.size(); i++){
								
								Database.SaveResult result = freezeResults[i];
								
								if (!result.isSuccess()){
									
									failedFederatedIds.add(userIdsToFreeze.get(usersToFreeze[i].UserId).FederationIdentifier);
									system.debug('freeze failed: ' + result.getErrors()[0].Message);		
								}
							}					
						}
		  			}
		  			catch (Exception e){
		  				
		  				system.debug('Exception thrown: ' + e.getMessage());
		  				
		  				for (User userToDeactivate : usersToDeactivate){
		  					failedFederatedIds.add(userToDeactivate.FederationIdentifier);
		  				}
		  			}
		  			
		  			if (!failedFederatedIds.isEmpty()){
		  				
		  				system.debug('failedFederatedIds: ' + failedFederatedIds);
		  				//TODO: Additional error handling or alerts?
		  			}
		  		}
	  		}
  		}
  	}
}