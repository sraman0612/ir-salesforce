@istest 
Public class CC_GPSi_addMultiplePartsUsedCtlrTest{
  
  static testmethod void test1(){
    String [] strLst = new List<String>();
    Product2 newP = TestUtilityClass.createProduct2WithProductCode('8675309');
    newP.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Product2' and DeveloperName='CC_GPSi'].Id;
    insert newP;
    for(Product2 p : CC_GPSi_addMultiplePartsUsedController.getParts()){
      strLst.add(p.Id);
      break;
    }
    system.assertEquals(1, strLst.size());
    String resultStr = CC_GPSi_addMultiplePartsUsedController.createPartUsed('abc',strLst);
  }
}