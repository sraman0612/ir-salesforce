@isTest
public class CC_ContractControllerTest
{    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    static testMethod void ContractControllerTest(){        
        // Creation of Test Data
        Account sampleAccount=TestUtilityClass.createAccount();
        insert sampleAccount;
        System.assertEquals('Sample Account',sampleAccount.Name);
        System.assertNotEquals(null, sampleAccount.Id);
        Contact newContact = TestUtilityClass.createContact(sampleAccount.id);
        insert newContact;
        System.assertEquals(sampleAccount.Id,newContact.AccountId);
        System.assertNotEquals(null,newContact.Id);
        // PavilionSettings__c records are created to handle User Events trigger
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User newUser = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, newContact.Id);
        insert newUser;
        //AccountTeamMember for Account share error        
        AccountTeamMember atm = TestUtilityClass.createATM(sampleAccount.Id,newUser.Id);
        insert atm;       
        AccountShare accShare = TestUtilityClass.createAccShare(sampleAccount.Id,newUser.Id);
        insert accShare;
        List<Integer> yearlist =new List<Integer>();
        yearlist.add(2015);
        yearlist.add(2016);
        List<string> options = new List<String>();
        options.add('test');
        System.runAs(newUser)
        {            
            Id accid = [select AccountId from user where id=:UserInfo.getUserId()].AccountId;
            System.debug('accid is'+accid);
            Contract sampleContact = new Contract(AccountId=accid,CC_Sub_Type__c='Golf Car Dealer',CC_Type__c ='Dealer/Distributor Agreement',CC_Contract_End_Date__c=System.today()+1);
            insert sampleContact;          
            system.debug('*****Contract***'+sampleContact);
            CC_Order__c newOrder = TestUtilityClass.createOrder(accid);
            insert newOrder;
            CC_Co_Op_Account_Summary__c ccas = TestUtilityClass.createCoopAccountSummary(sampleContact.id);
            insert ccas;
            system.debug('*****Account summary***'+ccas);
            CC_Coop_Claims__c cclaims = TestUtilityClass.createCoopClaimWithAccountSummary(ccas.Id);
            insert cclaims;
            cclaims = [select id,name from CC_Coop_Claims__c where id=:cclaims.Id limit 1];
            System.debug('the cclaims name is'+cclaims.Name);
            CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
            insert ordShip;            
            CC_PavilionTemplateController controller ;
            CC_ContractController contractCnt = new CC_ContractController(controller);
            contractCnt.year=2015;
            contractCnt.selectedYear=2015;
            contractCnt.agreementType='Contract';
            contractCnt.fundStartingAmount=100; 
            contractCnt.availableAmount= 100;
            contractCnt.selectedMAT='All Agreements';
            contractCnt.invoiceNum='568415';
            contractCnt.setYear=yearlist;
            contractCnt.setAgreementType= options;
            CC_Invoice2__c newInvoice = TestUtilityClass.createInvoice2(sampleAccount.Id,contractCnt.invoiceNum);
            insert newInvoice;
            System.assertNotEquals(null, newInvoice.Id);
            Test.startTest();  
            contractCnt.getYears();
            CC_ContractController.refreshCOOPTable(ccas.id,'2016','All Agreements');
            CC_ContractController.refreshCOOPTable('','2016','tests');
            CC_ContractController.refreshTableBasedOnYear('2016','All Agreements');
            contractCnt.invoiceNum=newInvoice.CC_PO_Number__c;
            CC_ContractController.refreshTableBasedOnYear('2016','test');
            Test.stopTest();
        }  
    }
}