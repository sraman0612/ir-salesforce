//
// (c) 2015, Appirio Inc.
//
// Handler Class for Opportunity_Line_Item Trigger
//
// April 16,2015    Surabhi Sharma    T-377640(modified)
//
public class Opportunity_Line_Item_TriggerHandler
{
    public static void afterInsert(Opportunity_Line_Item__c[] newList)
    {
        Set<Id> optys = new Set<Id>();
        for (Opportunity_Line_Item__c oli : newList)
            optys.add(oli.Opportunity__c);
            recalcOptys(optys);
    }
    
    public static void afterUpdate(Opportunity_Line_Item__c[] newList, Map<Id,Opportunity_Line_Item__c> oldMap)
    {
        Set<Id> optys = new Set<Id>();
        for (Opportunity_Line_Item__c oli : newList)
        {
            if (oli.amount__c != oldMap.get(oli.Id).amount__c || oli.quantity__c != oldMap.get(oli.Id).quantity__c)
                optys.add(oli.Opportunity__c);
        }
        recalcOptys(optys);
        UtilityClass.deleteApprovedRecords(newList,'Opportunity_Line_Item__c',UtilityClass.GENERIC_APPROVAL_API_NAME);
    }
    
    public static void afterDelete(Opportunity_Line_Item__c[] oldList)
    {
        Set<Id> optys = new Set<Id>();
        for (Opportunity_Line_Item__c oli : oldList)
            optys.add(oli.Opportunity__c);
            recalcOptys(optys);
    }
    
    public static void recalcOptys(Set<Id> optysToCalc)
    {
        
       List<Opportunity> optys = new List<Opportunity>();
       Opportunity_Line_Item__c[] optyLines = [SELECT opportunity__c,CTS_Sell_Price_Each__c,amount__c,quantity__c FROM Opportunity_Line_Item__c WHERE Opportunity__c IN :optysToCalc];
        
        Map<Id,Decimal> optyAmounts = new Map<Id,Decimal>();
        Map<Id,Decimal> optySaleAmounts = new Map<Id,Decimal>();
        for (Opportunity_Line_Item__c oli :optyLines)
        {
            Decimal amt = oli.amount__c * oli.quantity__c;
            /*start of changes 01416071*/
            Decimal saleAmt;
            if(oli.CTS_Sell_Price_Each__c!=null){
            	saleAmt = oli.CTS_Sell_Price_Each__c * oli.quantity__c;   
            }else{
                saleAmt= 0;
            }
            /*end of changes 01416071*/
                
            if (optyAmounts.containsKey(oli.opportunity__c))
                amt += optyAmounts.get(oli.opportunity__c);
                optyAmounts.put(oli.opportunity__c,amt);
            /*start of changes 01416071*/
            if(optySaleAmounts.containsKey(oli.opportunity__c)){
                saleAmt += optySaleAmounts.get(oli.opportunity__c);
            }
            system.debug('saleAmt------>'+saleAmt);
            optySaleAmounts.put(oli.opportunity__c,saleAmt);
            
            /*end of changes 01416071*/
        }
        
        for (Id opptyID : optysToCalc)
        {
            Opportunity oppty = new Opportunity(Id=opptyID);
            if (optyAmounts.containsKey(oppty.Id))
                oppty.Opportunity_Line_Item_Amount__c = optyAmounts.get(oppty.Id);
            else
                oppty.Opportunity_Line_Item_Amount__c = 0;
            /*start of changes 01416071*/
            /*if (optySaleAmounts.containsKey(oppty.Id))
                oppty.CTS_Sell_Price__c = optySaleAmounts.get(oppty.Id);
            else
                oppty.CTS_Sell_Price__c = 0;
            /*end of changes 01416071*/
            optys.add(oppty);
        }
        
        update optys;
    }
}