public class CC_TrainingRequest {
  Public String CC_Access_Code {get;set;}
  Public String CC_Attendee_Email {get;set;}
  Public String CC_Attendee_Name {get;set;}
  Public String CC_Attendee_Telephone {get;set;}
  Public String Attendee_Type {get;set;}
  Public Date CC_Date_Of_Course {get;set;}
  Public String CC_Time_Of_Course {get;set;}
  Public String CC_Company_Address {get;set;}
  Public String CC_Company_Email {get;set;}
  Public String CC_Company_Name {get;set;}
  Public String CC_Company_Telephone {get;set;}
  Public String CC_Courses {get;set;}
  Public String CC_Mode_of_Payment {get;set;}
  Public String CC_Clubcar_Customer {get;set;}
  Public String CC_Sales_Rep_Name {get;set;}
  Public String ContactId {get;set;}
  Public String AccountId {get;set;}
  Public String RecordTypeId {get;set;}
  Public String OwnerId {get;set;}
  Public String SalesRepId {get;set;}
  Public String CaseSubreason {get;set;}
  
  Public CC_TrainingRequest(){}

}