public without sharing class TaskMethods {
    
    public static Map<string, string> recTypeMap (string obj){
        Map<string, string> recTypeMap = new Map<string, string>();
        string recTypeQuery = 'SELECT Id, SObjectType, Name, DeveloperName FROM RecordType WHERE SObjectType =\'' + obj + '\'';
        RecordType[] recTypeList = database.Query(recTypeQuery);
        if ( recTypeList.size() > 0 ){
            for ( RecordType recType : recTypeList ){
                recTypeMap.put(recType.id, recType.DeveloperName);
            }
        }
        return recTypeMap;
    }
    
    public static void UpdateFaceToFaceEvent ( List<Event> EventList, Map<id, Event> oldEventMap, Map<id, Event> newEventMap ){
        Map<id, Date> MaxActDateMap = new Map<id, Date>();
        Map<id, Date> MaxActPhoneDateMap = new Map<id, Date>();
        for ( Event evt : EventList ){
            // in the update context, we are only interested in Account Ids for Events where certain values have changed
            if ( trigger.isUpdate ){
                if ( oldEventMap.get(evt.id).AccountId != newEventMap.get(evt.id).AccountId ){
                    if ( evt.AccountId != null ){
                        MaxActDateMap.put(evt.AccountId,null);
                        MaxActPhoneDateMap.put(evt.AccountId,null);
                    }
                    if ( oldEventMap.get(evt.id).AccountId != null ){
                        MaxActDateMap.put(oldEventMap.get(evt.id).AccountId,null);
                        MaxActPhoneDateMap.put(oldEventMap.get(evt.id).AccountId,null);
                    }
                }
                if ( evt.AccountId != null ){
                    if ( oldEventMap.get(evt.id).ActivityDateFF__c != newEventMap.get(evt.id).ActivityDateFF__c ){
                        MaxActDateMap.put(evt.AccountId,null);
                        MaxActPhoneDateMap.put(evt.AccountId,null);
                    }
                    if ( oldEventMap.get(evt.id).Activity_Type__c != newEventMap.get(evt.id).Activity_Type__c ){
                        MaxActDateMap.put(evt.AccountId,null);
                        MaxActPhoneDateMap.put(evt.AccountId,null);
                    }
                }
            } else { // if the Event has been deleted or inserted, we will always evaluate, so those Account Ids are always added
                if ( evt.AccountId != null ){
                    MaxActDateMap.put(evt.AccountId,null);
                    MaxActPhoneDateMap.put(evt.AccountId,null);
                }
            }
        }
        if ( MaxActDateMap.keyset().size() > 0 ){
            MaxActDateMap = TaskMethods.setFace2FaceDatefromEvent(MaxActDateMap, 'Face To Face Call');
            MaxActDateMap = TaskMethods.setFace2FaceDatefromTask(MaxActDateMap, 'Face To Face Call');
            MaxActPhoneDateMap = TaskMethods.setFace2FaceDatefromEvent(MaxActPhoneDateMap, 'Phone Call');
            MaxActPhoneDateMap = TaskMethods.setFace2FaceDatefromTask(MaxActPhoneDateMap, 'Phone Call');
            TaskMethods.updateAccountFace2FaceDate(MaxActDateMap,MaxActPhoneDateMap);
        }
    }    
    
    public static void UpdateFaceToFaceTask ( List<Task> TaskList, Map<id, Task> oldTaskMap, Map<id, Task> newTaskMap ){
        Map<id, Date> MaxActDateMap = new Map<id, Date>();
        Map<id, Date> MaxActPhoneDateMap = new Map<id, Date>();
        for ( Task tsk : TaskList ){
            // in the update context, we are only interested in Account Ids for Tasks where certain values have changed
            if ( trigger.isUpdate ){
                if ( oldTaskMap.get(tsk.id).AccountId != newTaskMap.get(tsk.id).AccountId ){
                    if ( tsk.AccountId != null ){
                        MaxActDateMap.put(tsk.AccountId,null);
                        MaxActPhoneDateMap.put(tsk.AccountId,null);
                    }
                    if ( oldTaskMap.get(tsk.id).AccountId != null ){
                        MaxActDateMap.put(oldTaskMap.get(tsk.id).AccountId,null);
                        MaxActPhoneDateMap.put(oldTaskMap.get(tsk.id).AccountId,null);
                    }
                }
                if ( tsk.AccountId != null ){
                    if ( oldTaskMap.get(tsk.id).Status != newTaskMap.get(tsk.id).Status ){
                        MaxActDateMap.put(tsk.AccountId,null);
                        MaxActPhoneDateMap.put(tsk.AccountId,null);
                    }
                    if ( oldTaskMap.get(tsk.id).ActivityDateFF__c != newTaskMap.get(tsk.id).ActivityDateFF__c ){
                        MaxActDateMap.put(tsk.AccountId,null);
                        MaxActPhoneDateMap.put(tsk.AccountId,null);
                    }
                    if ( oldTaskMap.get(tsk.id).Activity_Type__c != newTaskMap.get(tsk.id).Activity_Type__c ){
                        MaxActDateMap.put(tsk.AccountId,null);
                        MaxActPhoneDateMap.put(tsk.AccountId,null);
                    }
                }
            } else {	     // if the Task has been deleted or inserted, we will always evaluate, so those Account Ids are always added
                if ( tsk.AccountId != null ){
                    MaxActDateMap.put(tsk.AccountId,null);
                	MaxActPhoneDateMap.put(tsk.AccountId,null);
                }
            }
        }
        if ( MaxActDateMap.keyset().size() > 0 ){
            MaxActDateMap = TaskMethods.setFace2FaceDatefromEvent(MaxActDateMap,'Face To Face Call');
            MaxActDateMap = TaskMethods.setFace2FaceDatefromTask(MaxActDateMap,'Face To Face Call');
            MaxActPhoneDateMap = TaskMethods.setFace2FaceDatefromEvent(MaxActPhoneDateMap, 'Phone Call');
            MaxActPhoneDateMap = TaskMethods.setFace2FaceDatefromTask(MaxActPhoneDateMap, 'Phone Call');
            TaskMethods.updateAccountFace2FaceDate(MaxActDateMap,MaxActPhoneDateMap);
        }
    }
    
    public static Map<Id,Date> setFace2FaceDatefromEvent(Map<Id,Date> MaxActDateMap,String type){
        for ( AggregateResult ar : [SELECT AccountId accId, MAX(ActivityDateFF__c) actDate
                                    FROM Event
                                    WHERE AccountId in :MaxActDateMap.keyset() AND Activity_Type__c =: type AND ActivityDateFF__c <= today
                                    GROUP BY AccountId] ){
                                        Date d = (Date)ar.get('actDate');
                                        Id acctId = (Id)ar.get('accId');
                                        if(null==MaxActDateMap.get(acctId) || d > MaxActDateMap.get(acctId)){
                                            MaxActDateMap.put(acctId,d);
                                        }
                                    }
        return MaxActDateMap;
    }
    
    public static Map<Id,Date> setFace2FaceDatefromTask(Map<Id,Date> MaxActDateMap,String type){
        
        for ( AggregateResult ar : [SELECT AccountId accId, MAX(ActivityDateFF__c) actDate
                                    FROM Task
                                    WHERE AccountId in :MaxActDateMap.keyset() AND Activity_Type__c =:type AND Status = 'Completed'AND ActivityDateFF__c <= today
                                    GROUP BY AccountId] ){
                                        Date d = (Date)ar.get('actDate');
                                        Id acctId = (Id)ar.get('accId');
                                        if(null==MaxActDateMap.get(acctId) || d > MaxActDateMap.get(acctId) ){
                                            MaxActDateMap.put(acctId,d);
                                        }
                                    }
        return MaxActDateMap;
    }
    
    public static void updateAccountFace2FaceDate(Map<Id,Date> MaxActDateMap,Map<Id,Date>MaxActPhoneDateMap ){
        List<Account> AccUpdList = new List<Account>();  
        Map<string, string> recTypeMap = TaskMethods.recTypeMap('Account');
        system.debug('***MaxActDateMap.keySet() ****'+ MaxActDateMap);
        for ( Account acc : [ SELECT Id, RecordTypeId FROM Account WHERE Id in :MaxActDateMap.keySet() ]){
            system.debug('***acc record type ****'+ acc.RecordTypeId);
            string recTypeName = recTypeMap.get(acc.RecordTypeId).toLowerCase() ;
            boolean rtExcluded = recTypeName.contains('club') || recTypeName.contains('thermo') || recTypeName.contains('hibon') ;
            //Commented as part of EMEIA Cleanup
            /*|| recTypeName.contains('oem') || recTypeName.contains('chvac') || recTypeName.contains('HVAC')*/
            // do not include any excluded record types in the pending dml operation 
            if (!rtExcluded ){
                acc.Face_to_Face_Date__c = MaxActDateMap.get(acc.id) != null ? MaxActDateMap.get(acc.id) : null ;
                acc.CTS_Phone_Call_Date__c = MaxActPhoneDateMap.get(acc.id) != null ? MaxActPhoneDateMap.get(acc.id) : null ;
                AccUpdList.add(acc);
            }
        }
        if ( AccUpdList.size() > 0 ){ 
            try{ 
                update AccUpdList;
            } catch (Exception e){
                System.debug('Caught in taskMethods account updates ' + e.getMessage());
            }
        }
    }
    
     public static void SetMaxEventDateField (List<Event>EventList ){
        
        Set<Id> accountIdSet = new Set<Id>();
        List<Account> accUpdList = new List<Account>(); 
        for (Event e : EventList)
        {
            If(e.AccountId <> null)
            {
                accountIdSet.add(e.AccountId);
                System.debug('Account Id id' +accountIdSet);
            }
        }
         //Query Max Event Date
           for ( AggregateResult ar : [SELECT AccountId accId, MAX(ActivityDateFF__c) actDate
                                    FROM Event
                                    WHERE AccountId in :accountIdSet AND Activity_Type__c in ('Face To Face Call' , 'Phone Call') AND ActivityDateFF__c > today
                                    GROUP BY AccountId] ){
                                        Date d = (Date)ar.get('actDate');
                                        Id acctId = (Id)ar.get('accId');
                                        
               Account acc = New Account (Id = acctId) ;
               acc.Max_Event_Date_Field__c = d;
               accUpdList.add(acc);
                                       }
        
        If(!accUpdList.isEmpty())
        {
            update accUpdList;
        }
   
        
    }
}