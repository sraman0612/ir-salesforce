public with sharing class CC_ProductConsumedTriggerHandler {

    public static Boolean updateAll = false;

    public static void afterInsert(ProductConsumed[] pcs){
        updateWorkOrderLineItems(pcs, null);
    }

    public static void afterUpdate(ProductConsumed[] pcs, Map<Id, ProductConsumed> oldMap){
        updateWorkOrderLineItems(pcs, oldMap);
    }    

    private static void updateWorkOrderLineItems(ProductConsumed[] pcs, Map<Id, ProductConsumed> oldMap){

        system.debug('--updateWorkOrderLineItems');

        Set<Id> workOrderLineItemsToRecalc = new Set<Id>();

        for (ProductConsumed pc : pcs){

            ProductConsumed old = oldMap != null ? oldMap.get(pc.Id) : null;

            if (updateAll || (pc.WorkOrderId != null && (
                 (old == null && pc.QuantityConsumed != null) || 
                 (old != null && 
                    (pc.QuantityConsumed != old.QuantityConsumed || 
                    pc.WorkOrderLineItemId != old.WorkOrderLineItemId)
                )
            ))){             
                
                if (pc.WorkOrderLineItemId != null){
                    workOrderLineItemsToRecalc.add(pc.WorkOrderLineItemId);
                }

                if (old != null && old.WorkOrderLineItemId != null){
                    workOrderLineItemsToRecalc.add(old.WorkOrderLineItemId);
                }                
            }
        }

        system.debug('workOrderLineItemsToRecalc: ' + workOrderLineItemsToRecalc);

        if (workOrderLineItemsToRecalc.size() > 0){

            // Recalc quantity consumed all on work order line items
            AggregateResult[] lineItemAggResults = 
                [Select WorkOrderLineItemId, SUM(QuantityConsumed) QuantityConsumed 
                From ProductConsumed 
                Where WorkOrderLineItemId in :workOrderLineItemsToRecalc
                Group By WorkOrderLineItemId];

            WorkOrderLineItem[] lineItemsToUpdate = new WorkOrderLineItem[]{};

            for (AggregateResult aggResult : lineItemAggResults){

                lineItemsToUpdate.add(
                    new WorkOrderLineItem(
                        Id = (Id)aggResult.get('WorkOrderLineItemId'),
                        Quantity_Consumed__c = (Double)aggResult.get('QuantityConsumed')
                    )
                );
            }

            system.debug('lineItemsToUpdate: ' + lineItemsToUpdate);

            sObjectService.updateRecordsAndLogErrors(lineItemsToUpdate, 'CC_ProductConsumedTriggerHandler', 'updateWorkOrderLineItems');
        }
    }
}