/**
 * Test class of CTS_IOT_Site_ListController
 **/
@isTest
private class CTS_IOT_Site_ListControllerTest {
    
    @testSetup
    static void setup(){

        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', acc.Id, assetRecTypeId, true);
    }
    
    @isTest
    static void testGetInit(){

        Test.startTest();

        CTS_IOT_Site_ListController.InitResponse initRes = CTS_IOT_Site_ListController.getInit(null, 10, 0, 10);
        System.assert(initRes != null);
        System.assert(initRes.siteDetails != null);

        CTS_IOT_Site_ListController.InitResponse initRes2 = CTS_IOT_Site_ListController.getInit('Test', 10, 0, 10);
        System.assert(initRes2 != null);
        System.assert(initRes2.siteDetails != null);

        System.assertEquals(10, CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId()).User_Selected_List_Page_Size__c);

        Test.stopTest();
    }
}