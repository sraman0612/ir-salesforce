public with sharing class CC_STLCarInfoEventsHandler {

    public static void onBeforeInsert(CC_STL_Car_Info__c[] cars){
        validateCarItems(cars);
        populateMapicsInfo(cars);
    }

    // Items on a lease must be unique
    private static void validateCarItems(CC_STL_Car_Info__c[] cars){

        Set<Id> leaseIds = new Set<Id>();

        for (CC_STL_Car_Info__c car : cars){
            leaseIds.add(car.Short_Term_Lease__c);
        }

        Map<Id, Set<Id>> leaseItemMap = new Map<Id, Set<Id>>();

        for (CC_STL_Car_Info__c car : [Select Id, Item__c, Short_Term_Lease__c From CC_STL_Car_Info__c Where Short_Term_Lease__c in :leaseIds]){

            if (leaseItemMap.containsKey(car.Short_Term_Lease__c)){
                leaseItemMap.get(car.Short_Term_Lease__c).add(car.Item__c);
            }
            else{
                leaseItemMap.put(car.Short_Term_Lease__c, new Set<Id>{car.Item__c});
            }
        }

        List<Id> allItems = new List<Id>();

        for (Id leaseId : leaseItemMap.keySet()){
            allItems.addAll(leaseItemMap.get(leaseId));
        }

        Map<Id, Product2> itemMap = new Map<Id, Product2>([Select Id, Name From Product2 Where Id in :allItems]);

        for (CC_STL_Car_Info__c car : cars){

            Set<Id> leaseItems = leaseItemMap.get(car.Short_Term_Lease__c);
            Product2 item = itemMap.get(car.Item__c);

            if (leaseItems != null && leaseItems.contains(car.Item__c)){
                car.addError('Duplicate car item found on lease: ' + item.Name + '.  Each car item can only be added one time to a lease.');
            }
        }        
    }

    private static void populateMapicsInfo(CC_STL_Car_Info__c[] cars){

    	Map<Id,Decimal> STLCarCountMap = new Map<Id,Decimal>();
    	Map<Id,String> STLId2LeaseNumMap = new Map<Id,String>();
      
      	for(CC_STL_Car_Info__c ci : cars){
      		STLCarCountMap.put(ci.Short_Term_Lease__c,0);
		}
      
      	for(CC_Short_Term_Lease__c stl : [SELECT Id, LEASEKEY__c, carInfoCount__c FROM CC_Short_Term_Lease__c WHERE Id IN :STLCarCountMap.keyset()]){
        	STLCarCountMap.put(stl.Id,stl.carInfoCount__c+1);
        	STLId2LeaseNumMap.put(stl.Id,stl.LEASEKEY__c);
      	}
      
      	for(CC_STL_Car_Info__c newci : cars){
	        
	        String stlId = newci.Short_Term_Lease__c;
	        newci.lineSequence__c = STLCarCountMap.get(stlId);
	        newci.carInfoKey__c = Test.isRunningTest() ? newci.Unique_Key_Test_Helper__c : STLId2LeaseNumMap.get(stlId) + '_' + newci.lineSequence__c;
	        
	        system.debug('carInfoKey: ' + newci.carInfoKey__c);
	        
	        STLCarCountMap.put(stlId,newci.lineSequence__c+1);
        }        
    }
}