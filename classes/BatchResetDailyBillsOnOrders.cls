/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Batch that will reset the field Billed Yesterday and Billed Today on the RSHVAC Orders
------------------------------------------------------------*/
global class BatchResetDailyBillsOnOrders implements Database.Batchable<sObject>{

    global String query;

    public BatchResetDailyBillsOnOrders(){
        // SELECT fields
        query = 'SELECT Id, Billed_Yesterday__c, Billed_Today__c ';
        // FROM object
        query += 'FROM RSHVAC_Order__c ';
        // WHERE conditions
        // Ronnie Stegall - Modified query because return orders (billed_yesterday__c and bill_today__c) will be negative
        // query += 'WHERE Billed_Yesterday__c > 0 OR Billed_Today__c > 0';
        query += 'WHERE Billed_Yesterday__c != 0 OR Billed_Today__c != 0';
    }

    /**
     * Use this constructor just to ran the batch manually to test it (or other purpose)
     */
    public BatchResetDailyBillsOnOrders(String customQuery){
        query = customQuery;
    }

    /* start method */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('START BEGIN >>>');
        System.debug('Query >>> ' + query);
        return Database.getQueryLocator(query);
    }

    /* execute method */
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('EXECUTE BEGIN >>>');
        System.debug('Received >>> ' + scope.size() + ' inbound objects to process...');
        List<RSHVAC_Order__c> ordersToUpdate = new List<RSHVAC_Order__c>();
        for (SObject sObj : scope){
          RSHVAC_Order__c order = (RSHVAC_Order__c) sObj;
          // If there was any bill today set it to Yesterday
          // Set 0 otherwise
          // Ronnie Stegall - billed today can be negative 
          // order.Billed_Yesterday__c = order.Billed_Today__c > 0 ? order.Billed_Today__c : 0;
          order.Billed_Yesterday__c = order.Billed_Today__c != 0 ? order.Billed_Today__c : 0;
          // Billed today will always be reset to 0
          order.Billed_Today__c = 0;
          ordersToUpdate.add(order);
        }
        update ordersToUpdate;
    }

    /* finish method */
    public void finish(Database.BatchableContext BC) {
        System.debug('FINISH BEGIN >>>');
    }
}