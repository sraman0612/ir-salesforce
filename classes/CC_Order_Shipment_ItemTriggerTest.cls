@isTest
public without sharing class CC_Order_Shipment_ItemTriggerTest {

    public static testMethod void testPushUpdatesToCarItems(){

        CC_Load_Tracking_Settings__c settings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        settings.Order_Shipment_Item_Trigger_Enabled__c=true;
        insert settings;
         
        CC_Order_Shipment_ItemTriggerHandler osit = new CC_Order_Shipment_ItemTriggerHandler();

        Map<Id, CC_Order_Shipment_Item__c> shipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Account sampleAccount = TestUtilityClass.createAccount();
        sampleAccount.CC_Customer_Number__c='123';
        insert sampleAccount;
        
        CC_Order__c newOrder = TestUtilityClass.createCustomOrder(sampleAccount.id,'OPEN','N');
        insert newOrder;
        
        Product2 accessory = TestUtilityClass.createProduct2();
        accessory.CC_Item_Class__c = 'Test';
        
        Product2 car = TestUtilityClass.createProduct2();
        car.CC_Item_Class__c = 'NEW';  
        
        insert new Product2[]{accessory, car};
        
        CC_Order_Item__c carOrderItem = TestUtilityClass.createOrderItem(newOrder.Id);
        carOrderItem.Product__c = car.Id;
        carOrderItem.CC_Quantity__c = 5;
        
        CC_Order_Item__c accessoryOrderItem = TestUtilityClass.createOrderItem(newOrder.Id);
        accessoryOrderItem.Product__c = accessory.Id;
        accessoryOrderItem.CC_Quantity__c = 10;

        insert new CC_Order_Item__c[]{carOrderItem, accessoryOrderItem};
        
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(carOrderItem.Id);
        carItem.Truck_Sequence__c = 1;
        carItem.Shipment_Header_Number__c = 1;
        carItem.Truck_Quantity__c = 5;
        insert carItem;
        
        CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
        ordShip.Shipment_Header_Number__c = 1;
        ordShip.Invoice_Number__c = '123';
        ordShip.Ship_Date__c = Date.today();
        ordShip.Invoice_Date__c = Date.today();
        insert ordShip;
        
        CC_Order_Shipment_Item__c carShipmentItem = TestUtilityClass.createOrderShipmentItem(carOrderItem.Id, ordShip.Id);
        carShipmentItem.Shipment_Header_Number__c = 1;  
        carShipmentItem.Ship_Quantity__c = 5;      

        CC_Order_Shipment_Item__c accessoryShipmentItem = TestUtilityClass.createOrderShipmentItem(accessoryOrderItem.Id, ordShip.Id);
        accessoryShipmentItem.Shipment_Header_Number__c = 1; 
        accessoryShipmentItem.Backorder_Quantity__c = 1;             
        accessoryShipmentItem.Ship_Quantity__c = 9;      

        Test.startTest();

        insert new CC_Order_Shipment_Item__c[]{carShipmentItem, accessoryShipmentItem};

        Test.stopTest(); 
    }

    public static testMethod void TestpushUpdatesToCarItems1(){

        CC_Load_Tracking_Settings__c settings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        settings.Order_Shipment_Item_Trigger_Enabled__c=true;
        insert settings;
        
        Test.startTest();

        CC_Order_Shipment_ItemTriggerHandler osit = new CC_Order_Shipment_ItemTriggerHandler();

        Map<Id, CC_Order_Shipment_Item__c> shipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        
        List<CC_Order_Shipment_Item__c> osi = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi1 = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi2 = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi3 = new List<CC_Order_Shipment_Item__c>();

        Account sampleAccount = TestUtilityClass.createAccount();
        sampleAccount.CC_Customer_Number__c='123';
        insert sampleAccount;
        
        CC_Order__c newOrder = TestUtilityClass.createCustomOrder(sampleAccount.id,'OPEN','N');
        insert newOrder;
        
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        
        CC_Order_Item__c ordItem = TestUtilityClass.createOrderItem(newOrder.Id);
        ordItem.Product__c = prod.Id;
        insert ordItem;
         
        CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
        ordShip.Shipment_Header_Number__c=1111;
        ordShip.Invoice_Number__c='123';
        ordShip.Invoice_Date__c=System.today();
        insert ordShip;
        
        CC_Order_Shipment_Item__c ordShipItem =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        ordShipItem.Backorder_Quantity__c =1;
        ordShipItem.Shipment_Header_Number__c =1111;
        insert ordShipItem;
        osi.add(ordShipItem);
        
        for(CC_Order_Shipment_Item__c oi :osi ){
            shipmentItems.put(oi.Id, oi);
        }

        System.debug('shipmentItems'+shipmentItems);
        
        CC_Order_Shipment_Item__c ordShipItem1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        ordShipItem1.Backorder_Quantity__c =1;
        ordShipItem1.Shipment_Header_Number__c =1111;
        insert ordShipItem1;
        osi1.add(ordShipItem1);
        
        for(CC_Order_Shipment_Item__c oi :osi1 ){
            oldShipmentItems.put(oi.Id, oi);
        }

        System.debug('oldShipmentItems'+oldShipmentItems);
        
        CC_Order_Shipment_Item__c shipmentit =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        
        insert shipmentit;
        osi2.add(shipmentit);
        
        for(CC_Order_Shipment_Item__c oi :osi2 ){
            shipmentItems1.put(oi.Id, oi);
        }
        System.debug('shipmentItems1'+shipmentItems1);
        
        
        CC_Order_Shipment_Item__c shipmentit1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert shipmentit1;
        osi3.add(shipmentit1);
        
        for(CC_Order_Shipment_Item__c oi :osi3 ){
            oldShipmentItems1.put(oi.Id, oi);
        }

        System.debug('oldShipmentItems1'+oldShipmentItems1);
        CC_Order_Shipment_ItemTriggerHandler.onAfterUpdate(null,null);
        CC_Order_Shipment_ItemTriggerHandler.onAfterUpdate(shipmentItems,oldShipmentItems);

        Test.stopTest();
    }

    public static testMethod void TestpushUpdatesToCarItems2(){

        CC_Load_Tracking_Settings__c settings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        settings.Order_Shipment_Item_Trigger_Enabled__c=true;
        insert settings;
        
        Test.startTest();
        CC_Order_Shipment_ItemTriggerHandler osit = new CC_Order_Shipment_ItemTriggerHandler(); 
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        
        List<CC_Order_Shipment_Item__c> osi = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi1 = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi2 = new List<CC_Order_Shipment_Item__c>();
        List<CC_Order_Shipment_Item__c> osi3 = new List<CC_Order_Shipment_Item__c>();
        Account sampleAccount = TestUtilityClass.createAccount();
        sampleAccount.CC_Customer_Number__c='123';
        insert sampleAccount;
        
        CC_Order__c newOrder = TestUtilityClass.createCustomOrder(sampleAccount.id,'OPEN','N');
        insert newOrder;
        
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        
        CC_Order_Item__c ordItem = TestUtilityClass.createOrderItem(newOrder.Id);
        ordItem.Product__c = prod.Id;
        insert ordItem;  
        
        CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
        ordShip.Shipment_Header_Number__c=1111;
        ordShip.Invoice_Number__c='123';
        ordShip.Invoice_Date__c=System.today();
        insert ordShip;
        
        CC_Order_Shipment_Item__c ordShipItem =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        ordShipItem.Backorder_Quantity__c =1;
        ordShipItem.Shipment_Header_Number__c =1111;
        insert ordShipItem;
        osi.add(ordShipItem);
        
        for(CC_Order_Shipment_Item__c oi :osi ){
            shipmentItems.put(oi.Id, oi);
        }

        System.debug('shipmentItems'+shipmentItems);
        
        CC_Order_Shipment_Item__c ordShipItem1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        ordShipItem1.Backorder_Quantity__c =1;
        ordShipItem1.Shipment_Header_Number__c =1111;
        insert ordShipItem1;
        osi1.add(ordShipItem1);
        
        for(CC_Order_Shipment_Item__c oi :osi1 ){
            oldShipmentItems.put(oi.Id, oi);
        }

        System.debug('oldShipmentItems'+oldShipmentItems);
        
        CC_Order_Shipment_Item__c shipmentit =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert shipmentit;
        osi2.add(shipmentit);
        
        for(CC_Order_Shipment_Item__c oi :osi2 ){
            shipmentItems1.put(oi.Id, oi);
        }
        System.debug('shipmentItems1'+shipmentItems1);
        
        
        CC_Order_Shipment_Item__c shipmentit1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert shipmentit1;
        osi3.add(shipmentit1);
        
        for(CC_Order_Shipment_Item__c oi :osi3 ){
            oldShipmentItems1.put(oi.Id, oi);
        }
        System.debug('oldShipmentItems1'+oldShipmentItems1);
        CC_Order_Shipment_ItemTriggerHandler.pushUpdatesToCarItems(shipmentItems,oldShipmentItems);

        Test.stopTest();
    }
    
    @isTest
    private static void testError(){
        
        CC_Order_Shipment_ItemTriggerHandler.forceError = true;
        
        CC_Load_Tracking_Settings__c settings = CC_Load_Tracking_Settings__c.getOrgDefaults();
        settings.Order_Shipment_Item_Trigger_Enabled__c=true;
        insert settings;
         
        CC_Order_Shipment_ItemTriggerHandler osit = new CC_Order_Shipment_ItemTriggerHandler();
        
        Apex_Log__c log = new Apex_Log__c();
        log.Class_Name__c='CC_Order_Shipment_ItemTriggerHandler';
        log.Method_Name__c='pushUpdatesToCarItems';
        log.Message__c='Test';
        Insert log;
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Map<Id, CC_Order_Shipment_Item__c> shipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        Map<Id, CC_Order_Shipment_Item__c> oldShipmentItems1 = new Map<Id, CC_Order_Shipment_Item__c>();
        
        Account sampleAccount = TestUtilityClass.createAccount();
        sampleAccount.CC_Customer_Number__c='123';
        insert sampleAccount;
        
        CC_Order__c newOrder = TestUtilityClass.createCustomOrder(sampleAccount.id,'OPEN','N');
        insert newOrder;
        
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        
        CC_Order_Item__c ordItem = TestUtilityClass.createOrderItem(newOrder.Id);
        ordItem.Product__c = prod.Id;
        insert ordItem;
        
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(ordItem.Id);
        carItem.Truck_Sequence__c=111;
        carItem.Order_Item__c=ordItem.id;
        carItem.Shipment_Header_Number__c=1111;
        insert carItem;
        
        CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
        ordShip.Shipment_Header_Number__c=1111;
        ordShip.Invoice_Number__c='123';
        ordShip.Invoice_Date__c=System.today();
        insert ordShip;
        
        CC_Order_Shipment_Item__c ordShipItem =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        ordShipItem.Backorder_Quantity__c =1;
        ordShipItem.Shipment_Header_Number__c =1111;
        insert ordShipItem;
        shipmentItems.put(ordShipItem.Id, ordShipItem);
        
        CC_Order_Shipment_Item__c ordShipItem1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert ordShipItem1;
        oldShipmentItems.put(ordShipItem1.Id, ordShipItem1);
        CC_Order_Shipment_Item__c shipmentit =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        
        insert shipmentit;
        shipmentItems1.put(shipmentit.Id, shipmentit);
        
        CC_Order_Shipment_Item__c shipmentit1 =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert shipmentit1;

        Test.startTest();
        CC_Order_Shipment_ItemTriggerHandler.onAfterUpdate(shipmentItems1,oldShipmentItems1);
        update shipmentit1;
        CC_Order_Shipment_ItemTriggerHandler.pushUpdatesToCarItems(shipmentItems1,null);
        CC_Order_Shipment_ItemTriggerHandler.onAfterInsert(shipmentItems);
        Test.stopTest();      
    }
}