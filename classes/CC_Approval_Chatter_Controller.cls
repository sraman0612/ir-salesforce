public with sharing class CC_Approval_Chatter_Controller {

    public class GetRecordInApprovalResponse extends LightningResponseBase{
        @AuraEnabled public Id recordInApprovalId;
    }

    @AuraEnabled
    public static GetRecordInApprovalResponse getRecordInApproval(Id processInstanceWorkitemId){

        GetRecordInApprovalResponse response = new GetRecordInApprovalResponse();

        try{
            
            ProcessInstanceWorkitem piw = [Select ProcessInstance.TargetObjectId From ProcessInstanceWorkitem Where Id = :processInstanceWorkitemId];
            response.recordInApprovalId = piw.ProcessInstance.TargetObjectId;
        }
        catch (Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}