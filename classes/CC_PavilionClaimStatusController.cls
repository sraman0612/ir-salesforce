global class CC_PavilionClaimStatusController {
    
   public string claimID {get;set;}
   public string venderName {get;set;}
   public string invoicedate {get;set;}
   public string numCompBrsnds {get;set;}
   public string invoiceAmmount {get;set;}
   public string copyOfInvoice {get;set;}
   public string tearsheetOfAdd {get;set;}
   public string ccLogoInAd {get;set;}
   public string newClaimAmount {get;set;}
   public string comments {get;set;}
   public string claimStatusMessage{get;set;}
   public string returnStatus{get;set;}   
   public Static String userAccountId{get; set;} //@TODO THIS SHOULD GO AWAY AND USE FROM TEMPLATE CONTROLLER
   public Attachment invoiceattachment {
    get {
      if (invoiceattachment == null)
        invoiceattachment = new Attachment();
      return invoiceattachment;
    }
     set;
   }
   public static Attachment tearsheetattachment {
     get {
          if (tearsheetattachment == null)
            tearsheetattachment = new Attachment();
          return tearsheetattachment;
       }
     set;
    }
    
    public CC_PavilionClaimStatusController(CC_PavilionTemplateController controller) {
    
        claimStatusMessage = ApexPages.currentPage().getParameters().get('claimMsg');
    }
    
    public CC_PavilionClaimStatusController(){/* @TODO WHY DO WE HAVE THIS????? */}
    
    @RemoteAction
    global static String getClaimJson() {
      String contactId = [SELECT ContactId from User  where id =:  UserInfo.getUserId()].ContactId;
      userAccountId = contactId != null ? [Select AccountId from Contact where id =: contactId].AccountId : null;
      List<CC_Coop_Claims__c> lstClaims = [SELECT Id, Name, createdDate,Start_Date__c, CC_Status__c,Rejection_Code__c, (Select Comments From ProcessSteps Order by CreatedDate desc Limit 1) 
                                             FROM CC_Coop_Claims__c
                                            WHERE Co_Op_Account_Summary__r.Contract__r.AccountId =: userAccountId 
                                         ORDER BY createdDate desc];//PriyaP7NOv 2018 RT 8043665.Added rejection code
                                         
            system.debug('@@lstClaims=' + lstClaims );                              
      if(!lstClaims.isEmpty()){
        String jsonData = (String)JSON.serialize(lstClaims);
        return jsonData;
      } else {
        return 'No data found';
      }
      
        
    }
    
 
    
    @RemoteAction
    global static String getClaimByIdJson(String claimId){
      List<CC_Coop_Claims__c> lstClaims = [SELECT Id, Vendor_Adv_Name__c, Invoice_date__c, Number_of_competitive_brands__c,  Invoice_amount__c, New_Claim_Amount__c, 
                                                  Copy_Of_Invoice__c, TearSheet_of_Advertisment__c, Club_Car_Logo_in_Add__c, Comment__c,
                                                  (Select StepStatus, Comments From ProcessSteps Order by CreatedDate desc Limit 1) ,(SELECT name FROM Attachments  )
                                             FROM CC_Coop_Claims__c
                                            WHERE id =: claimId Limit 1];
      if(!lstClaims.isEmpty()){
        String jsonData = (String)JSON.serialize(lstClaims);
        return jsonData;
      } else {
        return 'No data found';
      }
    }
    
    

    public pagereference submitPavCoopClaimData() {
      system.debug('### ' + invoiceattachment);
      system.debug('### ' + tearsheetattachment);
      try{
        CC_Coop_Claims__c objCCCoopClaim = [Select id,Name , Co_Op_Account_Summary__r.CC_Initial_Funds__c from CC_Coop_Claims__c where id =: claimId ];
        String[] s = invoicedate.split('/');
        Date newInvoiceDate = Date.newInstance(Integer.valueOf(s[2]),Integer.valueOf(s[1]),Integer.valueOf(s[0]));
        Decimal claimAmount = (objCCCoopClaim.Co_Op_Account_Summary__r.CC_Initial_Funds__c >= Double.valueOf(newClaimAmount)) ?
                                                                    Double.valueOf(newClaimAmount) :  objCCCoopClaim.Co_Op_Account_Summary__r.CC_Initial_Funds__c;
        objCCCoopClaim.Vendor_Adv_Name__c = venderName;
        objCCCoopClaim.Invoice_date__c = newInvoiceDate;
        objCCCoopClaim.Number_of_competitive_brands__c = numCompBrsnds;
        objCCCoopClaim.Invoice_amount__c = Decimal.valueOf(invoiceAmmount);
        objCCCoopClaim.Copy_Of_Invoice__c = copyOfInvoice;
        objCCCoopClaim.New_Claim_Amount__c = Double.valueOf(claimAmount);
        objCCCoopClaim.TearSheet_of_Advertisment__c= tearsheetOfAdd;
        objCCCoopClaim.Club_Car_Logo_in_Add__c= ccLogoInAd;
        objCCCoopClaim.Comment__c= comments;
        if(invoiceattachment.name != null && invoiceattachment.name != ''){
          objCCCoopClaim.CC_Status__c = 'Submitted';
        }
        update objCCCoopClaim;
        List<Attachment> lstAttachment = new List<Attachment>();
        if(String.isNotBlank(invoiceattachment.name)) {
          Attachment objInvoiceAttachement = new Attachment();
          invoiceattachment.parentId = objCCCoopClaim.Id;
          lstAttachment.add(invoiceattachment);
        }
        if(String.isNotBlank(tearsheetattachment.name)) {
          Attachment objtearsAttachement = new Attachment();
          tearsheetattachment.parentId = objCCCoopClaim.Id;
          lstAttachment.add(tearsheetattachment);
        }
        if(!lstAttachment.isEmpty()){insert lstAttachment;}
         returnStatus   =   'Your Co-Op claim : '+objCCCoopClaim.Name+' has been submitted for approval.';
      } catch(Exception e) {
        System.debug('Exception e'+e);
        returnStatus = 'Claim Submission Failure '+e.getMessage();
        //@TOOD EITHER GET RID OF THIS TRY/CATCH OF PUT EXCEPTION OUT TO SCREEN FOR USER
      }
      PageReference pr = Page.CC_PavilionClaimStatus;
      pr.getParameters().put('claimMsg',returnStatus);
      pr.setRedirect(true);
      return pr;
    }
}