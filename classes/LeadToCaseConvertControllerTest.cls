/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 07-27-2023
 * @last modified by  : RISHAB GOYAL
**/
@isTest
public class LeadToCaseConvertControllerTest {
    
    @testSetup 
    private static void setup(){

        Lead_To_Case__c objcstolead = new Lead_To_Case__c();
        
        objcstolead.Case_Fields__c ='Brand__c';
        objcstolead.Name ='VPTECH_Brand__c';
        insert objcstolead;
        
        Lead_To_Case__c objcstolead1 = new Lead_To_Case__c();
        objcstolead1.Case_Fields__c ='SuppliedCompany';
        objcstolead1.Name ='Company';
        insert objcstolead1;
        
        Lead_To_Case__c objcstolead2 = new Lead_To_Case__c();
        objcstolead2.Case_Fields__c ='LFS_First_Name__c';
        objcstolead2.Name ='lastName';
        insert objcstolead2;
        
        
        Lead_To_Case__c objcstolead5 = new Lead_To_Case__c();
        objcstolead5.Case_Fields__c ='CTS_Region__c';
        objcstolead5.Name ='CTS_Lead_Region_Mappin__c ';
        insert objcstolead5;
        

        
        Lead objLead = new Lead();
        objLead.Company = 'Company';
        objlead.LastName = 'Last';
        objlead.VPTECH_Brand__c ='Thomas';
        objlead.CTS_Lead_Region_Mappin__c  ='MEIA';
        objLead.LeadSource = 'Web';
        objLead.Email = 'test@lastcompany.com';
        insert objlead; 
    }
        
    @isTest
    private static void testCaseMappingThomas(){

        Lead l = [Select id from lead limit 1];

        Test.startTest();
        validateConversionResults(l.Id, Label.Thomas);
        Test.stopTest();
    }

    @isTest
    private static void testCaseMappingTricontinentNA(){

        Lead l = [Select id from lead limit 1];
        l.VPTECH_Brand__c = 'TRICONTINENT';
        update l;

        Test.startTest();
        validateConversionResults(l.Id, Label.TriContinent);
        Test.stopTest();
    }

    @isTest
    private static void testCaseMappingTricontinentEU(){

        Lead l = [Select id from lead limit 1];
        l.CTS_Lead_Region_Mappin__c  = 'EU';
        l.VPTECH_Brand__c = 'TRICONTINENT';
        update l;

        Test.startTest();
        validateConversionResults(l.Id, Label.TriContinent_EMEIA);
        Test.stopTest();
    }

    @isTest
    private static void testCaseMappingOINA(){

        Lead l = [Select id from lead limit 1];
        l.VPTECH_Brand__c  = 'OINA';
        update l;

        Test.startTest();
        validateConversionResults(l.Id, Label.Thomas);
        Test.stopTest();
    }

    @isTest
    private static void testCaseMappingILS(){

        Lead l = [Select id from lead limit 1];
        l.VPTECH_Brand__c = 'ILS';
        update l;

        Test.startTest();
        validateConversionResults(l.Id, Label.TriContinent);
        Test.stopTest();
    }

    @isTest
    private static void testCaseMappingZINSSERANALYTIC(){

        Lead l = [Select id from lead limit 1];
        l.VPTECH_Brand__c = 'ZINSSER ANALYTIC';
        update l;

        Test.startTest();
        validateConversionResults(l.Id, Label.TriContinent);
        Test.stopTest();
    }    

    private static void validateConversionResults(Id leadId, Id expectedCaseOwnerId){

        system.assertEquals('Success', LeadToCaseConvertController.caseMapping(leadId));

        Lead l = [Select Id, Status From Lead Where Id = :leadId];
        Case c = [Select Id, OwnerId From Case Order By CreatedDate DESC LIMIT 1];
        
        // Lead check
//        system.assertEquals('Archived', l.Status);
    
        // Case check
        system.assertEquals(expectedCaseOwnerId, c.OwnerId);
    }
}