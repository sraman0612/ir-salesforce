/**
* @createdBy   Priya Patil
* @date         03/11/2019
* @description  Test Class for CC_CE_Cert_Test 
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest 
public class CC_CE_Cert_Test 
{

   
    static testmethod void UnitTestCC_CE_Cert()
    {
        TestDataUtility testData = new TestDataUtility();
        TestDataUtility.createStateCountries();
        
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        Account acc = testData.createAccount('Club Car: Channel Partner');
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        acc.CC_Siebel_ID__c = '1234567890';
        insert acc;
        
        List<CC_Order__c> orderList = new List<CC_Order__c>();
        CC_Order__c ord = TestUtilityClass.createCustomOrder(acc.id,'OPEN','N');
        ord.Siebel_Acct_Row_ID__c = '1234567890';
        orderList.add(ord);
        CC_Order__c ord2 = TestUtilityClass.createCustomOrder(acc.id,'OPEN','N');
        ord.Siebel_Acct_Row_ID__c = '1234567890';
        orderList.add(ord2);
        insert orderList;
        
        List<Product2> productsList = new List<Product2>();
        Id ccProdRTId = [SELECT Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Club_Car'].Id;
        Product2 prod =  TestUtilityClass.createProductwstructtype('Customizable');
        prod.RecordTypeId = ccProdRTId;
        productsList.add(prod);
        Product2 prod2 =  TestUtilityClass.createProduct2();
        prod2.RecordTypeId = ccProdRTId;
        productsList.add(prod2);
        Product2 prod3 =  TestUtilityClass.createProduct2();
        prod3.RecordTypeId = ccProdRTId;
        productsList.add(prod3);
        Product2 prod4 =  TestUtilityClass.createProduct2();
        prod4.RecordTypeId = ccProdRTId;
        productsList.add(prod4);
        Product2 prod5 =  TestUtilityClass.createProduct2();
        prod5.RecordTypeId = ccProdRTId;
        productsList.add(prod5);
        Product2 prod6 =  TestUtilityClass.createProduct2();
        prod6.RecordTypeId = ccProdRTId;
        productsList.add(prod6);
        Product2 prod7 =  TestUtilityClass.createProductwstructtype('Customizable');
        prod7.RecordTypeId = ccProdRTId;
        productsList.add(prod7);
        Product2 prod8 =  TestUtilityClass.createProduct2();
        prod8.RecordTypeId = ccProdRTId;
        productsList.add(prod8);
        Product2 prod9svp =  TestUtilityClass.createProduct2WithProductCode('SVP1234');
        prod9svp.RecordTypeId = ccProdRTId;
        productsList.add(prod9svp);
        insert productsList;
        
        List<CC_Order_Item__c> orderItemsList = new List<CC_Order_Item__c>();
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod.id,12);
        orderItemsList.add(orditem);
        CC_Order_Item__c orditem2 = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod2.id,13);
        orderItemsList.add(orditem2);
        CC_Order_Item__c orditem3 = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod3.id,14);
        orderItemsList.add(orditem3);
        CC_Order_Item__c orditem4 = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod4.id,14);
        orderItemsList.add(orditem4);
        CC_Order_Item__c orditem5 = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod5.id,14);
        orderItemsList.add(orditem5);
        CC_Order_Item__c orditem6 = TestUtilityClass.createOrderItemWithProduct(ord.Id,prod6.id,14);
        orderItemsList.add(orditem6);
        CC_Order_Item__c orditem10svp = TestUtilityClass.createOrderItemWithProduct(ord2.Id,prod9svp.id,14);
        orderItemsList.add(orditem10svp);
        CC_Order_Item__c orditem7 = TestUtilityClass.createOrderItemWithProduct(ord2.Id,prod7.id,12);
        orderItemsList.add(orditem7);
        CC_Order_Item__c orditem8 = TestUtilityClass.createOrderItemWithProduct(ord2.Id,prod8.id,13);
        orderItemsList.add(orditem8);
        CC_Order_Item__c orditem9 = TestUtilityClass.createOrderItemWithProduct(ord2.Id,prod.id,13);
        orderItemsList.add(orditem9);
        insert orderItemsList;
        
        List<CC_CE_Parts__c> CEPartList = new List<CC_CE_Parts__c>();
        CC_CE_Parts__c CEPart =  TestUtilityClass.CreateCEPart(prod.Id,prod2.Id,prod3.Id,prod4.Id,prod5.Id,prod6.Id);
        CEPartList.add(CEPart);
        CC_CE_Parts__c CEPart2 =  TestUtilityClass.CreateCEPart(prod7.Id,prod8.Id,null,null,null,null);
        CEPartList.add(CEPart2);
        insert CEPartList;
        
        List<CC_Asset_Group__c> AssetGroupList= new List<CC_Asset_Group__c>();
        CC_Asset_Group__c AssetGroup=TestDataUtility.createAssetGroup(acc.Id,prod.Id);
        AssetGroupList.add(AssetGroup);
        insert AssetGroupList;
        
        List<CC_Asset__c> AssetList= new List<CC_Asset__c>();
        CC_Asset__c asset=TestDataUtility.createAsset(acc.Id,prod.Id, AssetGroup.Id, ord.Id);
        AssetList.add(asset);
        CC_Asset__c asset2=TestDataUtility.createAsset(acc.Id,prod7.Id, AssetGroup.Id, ord2.Id);
        AssetList.add(asset2);
        insert AssetList;
        
        Test.startTest();
        List<Id> AssetId= new List<Id>();
        AssetId.add(asset.Id);
        AssetId.add(asset2.Id);
        CC_CE_Cert.getAssetId(AssetId);
        CC_CE_Cert ce = new CC_CE_Cert();
        ce.CEPartId = CEPart.Id;
        ce.GetProductDetails();        
        Test.stopTest();
    }
    
    


   
    
    
}