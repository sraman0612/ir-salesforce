/*** Util Class for all common actions***/
public class CGCommonUtility {
   public static boolean skipTrigger(string sObjName) {
        /* Created By Kannan Capgemini on 5-Oct-21 to ByPass Trigger Execution Logic */
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        //CG_Trigger_Setting__mdt TriggerSkip = [SELECT MasterLabel, Trigger_Disabled__c FROM CG_Trigger_Setting__mdt where MasterLabel =: sObjName];
        string removeunderscore = sObjName.replaceAll('__c', '');
        CG_Trigger_Setting__mdt TriggerSkip = CG_Trigger_Setting__mdt.getInstance(removeunderscore);
        if(TriggerSkip.Trigger_Disabled__c)
            return true;
        if(isDataMigrationUser)
            return true;
        
        return false;
    }

    public static string getRecordTypeNamefromMetadata(string sObjName) {
        /* Created By Lakshmi Capgemini on 12-Oct-21 to get Record type names from metadata to replace hardcoded values */
        CG_Trigger_Branching_and_Channel_Mapping__mdt obj_RTname_record = [SELECT Object_Name__c, Record_Type_Name__c FROM CG_Trigger_Branching_and_Channel_Mapping__mdt
            where Object_Name__c =: sObjName limit 1
        ];
        return obj_RTname_record.Record_Type_Name__c;
    }
    
    /*public static List<Contact> accountContactRelationGetRecords(List<sObject> lstObj){
        DescribeSObjectResult sObjDescribeResult = lstObj[0].getSobjectType().getDescribe();
        String sObj = sObjDescribeResult.getName();
        List<Contact> conRecs = new List<Contact>();
        List<Contact> contactRecords;
        if(sObj=='AccountContactRelation'){
            for(sObject accCon : lstObj){
                String recordId=(string)accCon.get('Id');
                Contact conrecord=[SELECT  Name,RecordType.DeveloperName FROM Contact 
                               WHERE Id IN (SELECT ContactId FROM AccountContactRelation where Id=:recordId)]; 
                conRecs.add(conrecord);
                //sObj='Contact';
                //sTrrtname=conrecord.RecordType.DeveloperName;
            }
            contactRecords=branchingAndChannelPopulation(conRecs);           
        }
        return contactRecords;
    }*/
    Public static List<sObject> branchingAndChannelPopulation(List<sObject> lstObj) {
        //System.debug('lstObj.5 : '+lstObj);
        DescribeSObjectResult sObjDescribeResult = lstObj[0].getSobjectType().getDescribe();
        String sObj = sObjDescribeResult.getName();
        String sTrchannel;
        Id defaultRecordTypeId;
        List<Default_Record_Type__mdt> defaultRecordTypes;
        List<sObject> returnList = new List<sObject>();
        Map<String,List<CG_Trigger_Branching_and_Channel_Mapping__mdt>> metadataMap= new Map<String,List<CG_Trigger_Branching_and_Channel_Mapping__mdt>>();
        List<CG_Trigger_Branching_and_Channel_Mapping__mdt> metadataList = new List<CG_Trigger_Branching_and_Channel_Mapping__mdt>();
        
        metadataList=[SELECT MasterLabel, Record_Type_Name__c, Channel__c,BRNO__c,EMEIA__c, Object_Name__c
                      FROM CG_Trigger_Branching_and_Channel_Mapping__mdt
                      where Object_Name__c =: sObj];
        System.debug('metadataList : '+metadataList);
        /* Changes START
        Update By Aman Kumar on 20-jan-22 
        To assign Record types as per assigned Permission set for contact, Opportunity, Account & Task Objects, 
        when it is null for current User while converting Lead.
        */
        List<PermissionSetAssignment> currentUserPermissionSets = [SELECT Id, PermissionSet.Name,AssigneeId
                                                                   FROM PermissionSetAssignment
                                                                   WHERE AssigneeId = :Userinfo.getUserId()];
        List<String> assignedPermissionSets = new List<String>();
        for (PermissionSetAssignment psa: currentUserPermissionSets){
            assignedPermissionSets.add(psa.PermissionSet.Name); 
        }
        assignedPermissionSets.sort();
       defaultRecordTypes=[SELECT MasterLabel, 
                            RecordType_Developer_Name__c,
                            Permission_Set_Name__c,
                            Object_Name__c
                            FROM Default_Record_Type__mdt];
        Map<String, String> psToRecordType = new Map<String, String>();
        for(Default_Record_Type__mdt drt : defaultRecordTypes){
            psToRecordType.put(drt.Permission_Set_Name__c + '_' + drt.Object_Name__c, drt.RecordType_Developer_Name__c);
        }
        System.debug('lstObj1 : '+lstObj);
        for(sObject obj:lstObj){
            String channel;
            String recordTypeID = (string)obj.get('RecordTypeId');
            system.debug('record type ID ======='+ recordTypeID);
            if(String.isBlank(recordTypeID) && !defaultRecordTypes.isEmpty()){
                //System.debug('Inside blank RecordType1>>> ');
				//System.debug('Current USer >>> '+UserInfo.getName()+UserInfo.getUserId());
			system.debug('assignedPermissionSets++++++++++++++++'+assignedPermissionSets);
                for(String userPermissionset : assignedPermissionSets){
                    String temp = userPermissionset + '_' + obj.getSobjectType().getDescribe().getName();
                    system.debug('temp==='+ temp + 'userPermissionset=='+ userPermissionset);
                   
                    if(psToRecordType.get(temp) != null){
                        defaultRecordTypeId = Schema.getGlobalDescribe().get(obj.getSobjectType().getDescribe().getName()).getDescribe()
                            .getRecordTypeInfosByDeveloperName().get(psToRecordType.get(temp)).getRecordTypeId();
                       
                       /* if(obj.getSobjectType().getDescribe().getName() == 'Account'){
                            
                            Account acc = (Account)obj;
                            acc.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                            
                        } else*/ if(obj.getSobjectType().getDescribe().getName() == 'Contact'){
                            Contact con = (Contact)obj;
                            con.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                            
                        }
                        
                            /*else if(obj.getSobjectType().getDescribe().getName() == 'Opportunity'){
                            
                            Opportunity opp = (Opportunity)obj;
                            opp.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                          
                        }else if(obj.getSobjectType().getDescribe().getName() == 'Case' && 
                                Schema.SObjectType.Case.getRecordTypeInfosById().get(recordTypeID).getDeveloperName() != 'Internal_Case'
                                && Schema.SObjectType.Case.getRecordTypeInfosById().get(recordTypeID).getDeveloperName() != 'Action_Item'){
                           if(!test.isRunningTest()){
                                    Case cse = (Case)obj;
                            cse.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                           }
                        }*//*else if(obj.getSobjectType().getDescribe().getName() == 'ServiceAppointment'){
                            ServiceAppointment servApp = (ServiceAppointment)obj;
                            servApp.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                        }*/
                        else{
                            Task tsk = (Task)obj;
                            tsk.RecordTypeId = defaultRecordTypeId;
                            recordTypeID = defaultRecordTypeId;
                            Break;
                        }
                    }
                    
                }
            }
            /* Changes END */
            //System.debug('Outside blank RecordType and recordTypeID>>> '+recordTypeID);
           /* if(obj.getSobjectType().getDescribe().getName() == 'Case'){
               recordTypeID = '012j0000000L9vjAAC'; 
            }
*/
            if(recordTypeID != null){
                String sTrrtname = sObjDescribeResult.getRecordTypeInfosById().get(recordTypeID).getDeveloperName();
                for(CG_Trigger_Branching_and_Channel_Mapping__mdt mtd :metadataList){                  
                    if(mtd.Record_Type_Name__c == sTrrtname && sTrrtname != 'Internal_Case' && sTrrtname != 'Action_Item' ){
                        channel=mtd.EMEIA__c?System.Label.CG_EMEIA_Trigger:System.Label.CG_BRNO_Trigger;//obj.CG_Org_Channel__c
                        String channelVal=(String)obj.put('CG_Org_Channel__c', channel);       
                    }
                }
            }
            //System.debug('Account Channel >>> '+channel);
            returnList.add(obj);
        }
        //System.debug('returnList >>> '+returnList);
        return returnList;
      /*  CG_Trigger_Branching_and_Channel_Mapping__mdt ChannelMapping = [SELECT MasterLabel, Record_Type_Name__c, Channel__c, GD__c, IR__c, Object_Name__c
            FROM CG_Trigger_Branching_and_Channel_Mapping__mdt
            where Object_Name__c =: sObj];
        //metadataMap.put(sObj,ChannelMapping);
        
        if(sObj == 'AccountContactRelation'){
            String recordId=(string)lstObj.get('Id');
            Contact conrecord=[SELECT  Name,RecordType.DeveloperName FROM Contact 
                               WHERE Id IN (SELECT ContactId FROM AccountContactRelation where Id=:recordId)];
            
            sObj='Contact';
            sTrrtname=conrecord.RecordType.DeveloperName;
        }
        CG_Trigger_Branching_and_Channel_Mapping__mdt ChannelMapping = [SELECT MasterLabel, Record_Type_Name__c, Channel__c, GD__c, IR__c, Object_Name__c
            FROM CG_Trigger_Branching_and_Channel_Mapping__mdt
            where Object_Name__c =: sObj and Record_Type_Name__c =: sTrrtName
        ];

        if (ChannelMapping.GD__c && !ChannelMapping.IR__c) {
            sTrchannel = 'GD';
        }
        Else
        if (!ChannelMapping.GD__c && ChannelMapping.IR__c) {
            sTrchannel = 'IR';
        }

        System.debug('RT Name :' + sTrrtname);
        System.debug('Object Name :' + sObj);
        System.debug('Channel :' + sTrchannel);

        return sTrchannel;*/
    }

    
}