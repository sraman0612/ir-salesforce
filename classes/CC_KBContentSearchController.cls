/*************************************
 * CC_KBContentSearchController
 * Controller class for Knowledge Article and Content Version
 **************************************/
public with sharing class CC_KBContentSearchController {
    /**
     * Knowledge Article and ContentVersion search 
     * @param keyword, searchTypes
     * @return List
     */
    @AuraEnabled
    public static List<ObjectWrapper> search(String keyword, List<String> searchTypes) {        
        
        Set<String> types = new Set<String>();
        
        List<ObjectWrapper> queryResults = new List<ObjectWrapper>(); 
        types.addAll(searchTypes);
        List<String> objectNames = new List<String>();
        
        //if(types.contains(CC_KBContent_Constants.KNOWLEDGE)){
            objectNames.add(CC_KBContent_Constants.KNOWLEDGE_OBJECT);
        //}
        //if(types.contains(CC_KBContent_Constants.BULLETINS) || types.contains(CC_KBContent_Constants.CONTENT)){
            objectNames.add(CC_KBContent_Constants.CONTENT_VERSION);
        //}

        Map<String, List<String>> fieldMap = CC_KBContentSearchConfig.getFields(objectNames);
        
        Map<String, List<CC_KB_Content_Search_Filter_Config__mdt>> filterMap = CC_KBContentSearchConfig.getFilterConfig(objectNames, CC_KBContent_Constants.AUTO);
        CC_kb_content_search_config__c searchConfigSetting = CC_KBContentSearchConfig.getSearchConfig();
        
        Set<String> imageFiles = new Set<String>();
        if(searchConfigSetting != null && searchConfigSetting.image_files__c != null){
            imageFiles.addAll(searchConfigSetting.Image_files__c.split(','));
        }
        Set<String> videoFiles = new Set<String>();
        if(searchConfigSetting != null && searchConfigSetting.Video_files__c != null){
            videoFiles.addAll(searchConfigSetting.Video_files__c.split(','));
        }
        Set<String> musicFiles = new Set<String>();
        if(searchConfigSetting != null && searchConfigSetting.Music_files__c != null){
            musicFiles.addAll(searchConfigSetting.Music_files__c.split(','));
        }
        Set<String> linkFiles = new Set<String>();
        if(searchConfigSetting != null && searchConfigSetting.Link_files__c != null){
            linkFiles.addAll(searchConfigSetting.Link_files__c.split(','));
        }
        
        String kbQuery =  (types.contains(CC_KBContent_Constants.KNOWLEDGE) ? CC_KBContentSearchHelper.buildKnowledgArticleQuery(fieldMap.get(CC_KBContent_Constants.KNOWLEDGE_OBJECT), filterMap.get(CC_KBContent_Constants.KNOWLEDGE_OBJECT)): '');
        String contentQuery = '';
        if(types.contains(CC_KBContent_Constants.BULLETINS) || types.contains(CC_KBContent_Constants.CONTENT)){
            contentQuery = CC_KBContentSearchHelper.buildContentQuery(fieldMap.get(CC_KBContent_Constants.CONTENT_VERSION),types, filterMap.get(CC_KBContent_Constants.CONTENT_VERSION));
        }
        String q = 'FIND {' + keyword + '} RETURNING ' + kbQuery + ' ' + contentQuery;
        q = q.trim().removeEnd(',');
        System.debug('q****'+q);
                
        try {
            List<List<Sobject>> result =  Search.query(q);
            
            if(result.size() >= 1 && !result[0].isEmpty()){
                String objectType;
                for(Sobject sobj : result[0]) {
                    ObjectWrapper objWrapper = new ObjectWrapper();
                    objWrapper.sobj = sobj;
                    objWrapper.searchType = sobj.getSObjectType().getDescribe().getName();
                    if(objWrapper.searchType == 'Knowledge__kav') {
                        objWrapper.iconName = 'standard:knowledge';
                        if(objectType == null){
                            objectType = CC_KBContent_Constants.KNOWLEDGE;
                        }
                    } else {
                      /* NOT CALLED WHEN SEARCH TYPES ARE FIXED TO KB AND CONTENT
                        String ext = (String)sobj.get('FileExtension');
                        if(imageFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'standard:photo';
                        else if(videoFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:video';
                        else if(musicFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:audio';
                        else if(linkFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:link';
                        else objWrapper.iconName = 'standard:document';
                        if(objectType == null) {
                            objectType = CC_KBContent_Constants.CONTENT_VERSION;
                        }*/
                    }                    
                    queryResults.add(objWrapper);
                }
            }
            if(result.size() == 2 && !result[1].isEmpty()){
                String objectType;
                for(Sobject sobj : result[1]){
                    ObjectWrapper objWrapper = new ObjectWrapper();
                    objWrapper.sobj = sobj;
                    objWrapper.searchType = sobj.getSObjectType().getDescribe().getName();
                    if(objectType == null){
                        objectType = objWrapper.searchType;
                    }
                    String ext = (String)sobj.get('FileExtension');
                    if(ext != null) {
                        if(imageFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'standard:photo';
                        else if(videoFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:video';
                        else if(musicFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:audio';
                        else if(linkFiles.contains(ext.toUpperCase())) objWrapper.iconName = 'doctype:link';
                        else objWrapper.iconName = 'standard:document';
                    } else {
                       objWrapper.iconName = 'standard:document';
                    }
                    queryResults.add(objWrapper);
                }
            }            
            return queryResults;
        } catch(Exception e) {
            System.debug('::exception' +e.getMessage());
            return null;
        }
    }
    
    /**
     * Load Search Config Details
     * @return Object
     */
    @AuraEnabled
    public static SearchConfigWrapper getSearchConfigDetails() {
        CC_kb_content_search_config__c searchConfigSetting = CC_KBContentSearchConfig.getSearchConfig();
        return new SearchConfigWrapper(Integer.valueOf(searchConfigSetting.Default_page_size__c), searchConfigSetting.show_search_filter_options__c, searchConfigSetting.available_options_in_the_page_size__c, searchConfigSetting.Record_Type__c);
    }
    
    /**
     * get Case Number
     * @param caseId
     * @return String
     */
    @AuraEnabled
    public static String getCaseNumber(String caseId) {
        String caseNumber = '';
        for(Case cs : [select id, caseNumber from Case where id =: caseId]){
            caseNumber = cs.CaseNumber;
        }
        return caseNumber;
    }
    
    /**
     * update ViewStat of opened article
     * @param String recordId
     * @return Boolean
     */
    @AuraEnabled
    public static Boolean updateViewStat(String recordId) {
        try {
            Knowledge__kav kb = [SELECT Title FROM Knowledge__kav WHERE Id =: recordId UPDATE VIEWSTAT];
            return true;
        } catch(Exception e) {
            return false;            
        }
    }
    
    /**
     * Get a list of recommended articles based on topic(s) assigned. Used in CC_KB_RecommendedArticles.cmp
     * @param String topicNames
     * @return List<ObjectWrapper>
     */
    @AuraEnabled
    public static List<ObjectWrapper> getRecArticles(String topicNames, Integer numOfArticles) {
        List<String> topics = topicNames.split(',');
        Id [] entityIdLst = topicAssignmentAccessor.getEntityIdsByTopic(topics);
        String q = 'SELECT Id,Title,ArticleNumber,RS_CRS_Description__c,Summary,KnowledgeArticleId,CreatedDate, ' +
                    'ArticleTotalViewCount,LastModifiedDate,UrlName,LastPublishedDate,VersionNumber ' +
                    'FROM Knowledge__kav WHERE Id IN :entityIdLst AND PublishStatus=\'' + 
                    String.escapeSingleQuotes('Online') +
                    '\' AND RecordType.Name = \'' + 
                    String.escapeSingleQuotes('Club Car') +
                    '\' ORDER BY LastModifiedDate DESC LIMIT :numOfArticles';
        List<ObjectWrapper> kObjectlst = new List<ObjectWrapper>();
        try {
            for(Knowledge__kav kObj : Database.query(q)){
                ObjectWrapper kObject = new ObjectWrapper();
                kObject.sobj = kObj;
                kObject.searchType = 'Knowledge__kav';
                kObject.iconName = 'standard:knowledge';
                kObjectlst.add(kObject);
            }
            return kObjectlst;
        } catch(Exception e) {
            System.debug('e***' + e.getMessage());
            throw new AuraException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<ObjectWrapper> getArticlesOnLoad(String parameter){
        Set<Id> parentSet = new Set<Id>();
        Set<Id> parentVoteSet = new Set<Id>();
        String query = 'SELECT Id,Title,ArticleNumber,RS_CRS_Description__c,Summary,KnowledgeArticleId,CreatedDate,'
                     + 'ArticleTotalViewCount,LastModifiedDate,UrlName,LastPublishedDate,VersionNumber'
                     + ' FROM Knowledge__kav WHERE RecordType.Name = \''
                     + String.escapeSingleQuotes('Club Car')+'\'';
        if(!String.isBlank(parameter)){
            if(!parameter.equals('EntitySubscription') 
               && !parameter.equals('mostHelpfulArticles')){
                query=query+' '+parameter;
            } else if(parameter.equals('mostHelpfulArticles')){
                List<knowledge__VoteStat > votStatlst=[SELECT Id,ParentId 
                                                 FROM knowledge__VoteStat];
                for(knowledge__VoteStat enSub: votStatlst){
                    parentVoteSet.add(enSub.ParentId);
                }
                query = 'SELECT Id,Title,ArticleNumber,RS_CRS_Description__c,Summary,KnowledgeArticleId,CreatedDate,'
                      + 'ArticleTotalViewCount,LastModifiedDate,UrlName,LastPublishedDate,VersionNumber'
                      + ' FROM Knowledge__kav WHERE knowledgeArticleId IN :parentVoteSet AND PublishStatus=\''
                      + String.escapeSingleQuotes('Online')+'\' AND RecordType.Name = \''
                      + String.escapeSingleQuotes('Club Car')+'\'';
            }else{
                List<EntitySubscription> eSublst=[SELECT ParentId 
                                                 FROM EntitySubscription 
                                                 WHERE SubscriberId =: UserInfo.getUserId()
                                                 AND Parent.Type = 'knowledge__ka'];
                for(EntitySubscription enSub: eSublst){
                    parentSet.add(enSub.ParentId);
                }
                query = 'SELECT Id,Title,ArticleNumber,RS_CRS_Description__c,Summary,KnowledgeArticleId,CreatedDate,'
                      + 'ArticleTotalViewCount,LastModifiedDate,UrlName,LastPublishedDate,VersionNumber'
                      + ' FROM Knowledge__kav WHERE knowledgeArticleId IN :parentSet AND PublishStatus=\''
                      + String.escapeSingleQuotes('Online')+'\' AND RecordType.Name = \''
                      + String.escapeSingleQuotes('Club Car')+'\'';
            }
        }
        List<Knowledge__kav> knowledgeLst = Database.query(query);
        List<ObjectWrapper> kObjectlst = new List<ObjectWrapper>();
        for(Knowledge__kav kObj : knowledgeLst){
            ObjectWrapper kObject = new ObjectWrapper();
            kObject.sobj = kObj;
            kObject.searchType = 'Knowledge__kav';
            kObject.iconName = 'standard:knowledge';
            kObjectlst.add(kObject);
        }
        return kObjectlst;
    }
         
   // Create a wrapper class with @AuraEnabled Properties    
   public class ObjectWrapper {
        @AuraEnabled public String searchType {get;set;}
        @AuraEnabled public Sobject sobj {get;set;}
        @AuraEnabled public String iconName {get;set;}    
   }
   
   public class SearchConfigWrapper {
      @AuraEnabled public Integer pageSize{get;set;}
      @AuraEnabled public Boolean showSearchOption {get;set;}
      @AuraEnabled public String pageSizeOptions {get;set;}
      @AuraEnabled public String recordTypeId{get;set;}
      
      public SearchConfigWrapper(){}
      
      public SearchConfigWrapper(Integer pageSize, Boolean showSearchOption, String pageSizeOptions, String recordTypeName){
          this.pageSize = pageSize;
          this.showSearchOption = showSearchOption;
          this.pageSizeOptions = pageSizeOptions;
          this.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().containsKey(recordTypeName) ? Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId() : null;
      }
   }
}