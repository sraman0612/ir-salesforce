@isTest
public with sharing class NotificationDeliveryServiceTest {

    @testSetup
    public static void createData(){

		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Notification_Settings__c notificationSettings = NotificationsTestDataService.createNotificationSettings();
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);
        Organization org = [SELECT Id, Name, TimeZoneSidKey FROM Organization LIMIT 1];

        Account billTo1 = CTS_TestUtility.createAccount('Test Bill To1', false);
        billTo1.RecordTypeId = NotificationsTestDataService.billToRecordTypeId;

        Account billTo2 = CTS_TestUtility.createAccount('Test Bill To2', false);
        billTo2.RecordTypeId = NotificationsTestDataService.billToRecordTypeId;

        insert new Account[]{billTo1, billTo2};

        Account shipTo1 = CTS_TestUtility.createAccount('Test Ship To1', false);
        shipTo1.Bill_To_Account__c = billTo1.Id;

        Account shipTo2 = CTS_TestUtility.createAccount('Test Ship To2', false);
        shipTo2.Bill_To_Account__c = billTo1.Id;

        Account shipTo3 = CTS_TestUtility.createAccount('Test Ship To3', false);
        shipTo3.Bill_To_Account__c = billTo2.Id;

        Account distributor1 = CTS_TestUtility.createAccount('Test Distributor1', false);
        distributor1.RecordTypeId = NotificationsTestDataService.distributorRecordTypeId;
        
        insert new Account[]{shipTo1, shipTo2, shipTo3, distributor1};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', shipTo1.Id, assetRecTypeId, false); 
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', shipTo1.Id, assetRecTypeId, false); 
        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '25810', shipTo2.Id, assetRecTypeId, false);  
        Asset asset4 = CTS_TestUtility.createAsset('Test Asset4', '25874', shipTo3.Id, assetRecTypeId, false);
        Asset asset5 = CTS_TestUtility.createAsset('Test Asset5', '84555', distributor1.Id, assetRecTypeId, false);
        insert new Asset[]{asset1, asset2, asset3, asset4, asset5};        

        Contact standCommContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts1@gmail.com', shipTo1.Id, false);
        standCommContact.CTS_IOT_Community_Status__c = 'Approved';
        standCommContact.MobilePhone = '555-555-5555';

        Contact superCommContact = CTS_TestUtility.createContact('Contact345','Test', 'testcts2@gmail.com', shipTo2.Id, false);
        superCommContact.CTS_IOT_Community_Status__c = 'Approved';
        superCommContact.MobilePhone = '555-555-5555';        

        Contact partnerCommContact = CTS_TestUtility.createContact('Contact678', 'Test', 'testcts3@gmail.com', distributor1.Id, false);
        partnerCommContact.MobilePhone = '555-555-5555'; 

        insert new Contact[]{standCommContact, superCommContact, partnerCommContact}; 

        Id airdDistributorPartnerProfileId = [Select Id From Profile Where Name = 'CTS EU Partner User'].Id;

        User internalUser = CTS_TestUtility.createUser(false);    
        internalUser.isActive = true;
        internalUser.LastName = 'Internal_User_Test1';        
        internalUser.MobilePhone = '555-555-5555';

        User standCommUser = CTS_TestUtility.createUser(standCommContact.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id, false);    
        standCommUser.LastName = 'Comm_Stand_User_Test1';

        User superCommUser = CTS_TestUtility.createUser(superCommContact.Id, CTS_IOT_Data_Service_WithoutSharing.defaultSuperUserCommunityProfile.Id, false);    
        superCommUser.LastName = 'Comm_Super_User_Test1';   
        
        User partnerCommUser = CTS_TestUtility.createUser(partnerCommContact.Id, airdDistributorPartnerProfileId, false);    
        partnerCommUser.LastName = 'Partner_Comm_User_Test1';
        
        insert new User[]{internalUser, standCommUser, superCommUser, partnerCommUser};           

        Notification_Type__c newWarningType = NotificationsTestDataService.createNotificationType(false, 'New Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'The following warning was detected: {!CTS_RMS_Event__c.Event_Message__c}');

        Notification_Type__c newTripType = NotificationsTestDataService.createNotificationType(false, 'New Trip Test', false, 'Trip',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'The following trip was detected: {!CTS_RMS_Event__c.Event_Message__c}'); 

        Notification_Type__c clearedWarningType = NotificationsTestDataService.createNotificationType(false, 'Warning Clear Test', false, 'True',
            'CTS_RMS_Event__c', 'Cleared_Warning__c', 'Asset', 'Asset__c', 'Cleared_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'The following warning was cleared: {!CTS_RMS_Event__c.Event_Message__c}');

        // No one will be subscribed to this type for testing purposes
        Notification_Type__c clearedTripType = NotificationsTestDataService.createNotificationType(false, 'Trip Clear Test', false, 'True',
            'CTS_RMS_Event__c', 'Cleared_Trip__c', 'Asset', 'Asset__c', 'Cleared_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'The following trip was cleared: {!CTS_RMS_Event__c.Event_Message__c}');            

        insert new Notification_Type__c[]{newWarningType, newTripType, clearedWarningType, clearedTripType};

        /*** Standard community user subscriptions ***/
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, standCommUser.Id, null, newWarningType.Id,
            'Real-Time', 'Email;SMS');   
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, standCommUser.Id, null, newTripType.Id,
            'Daily;Real-Time', 'Email;SMS;Community');  
        
        // 3 subscriptions to cleared warnings type (global, site, asset)
        Notification_Subscription__c subscription3 = NotificationsTestDataService.createSubscription(false, standCommUser.Id, null, clearedWarningType.Id,
            'Weekly;Real-Time', 'Email');
            
        Notification_Subscription__c subscription4 = NotificationsTestDataService.createSubscription(false, standCommUser.Id, asset1.Id, clearedWarningType.Id,
            'Weekly', 'Email');
            
        Notification_Subscription__c subscription5 = NotificationsTestDataService.createSubscription(false, standCommUser.Id, shipTo1.Id, clearedWarningType.Id,
            'Real-Time', 'Community');            
            
        /*** Super community user subscriptions ***/
        Notification_Subscription__c subscription6 = NotificationsTestDataService.createSubscription(false, superCommUser.Id, null, newWarningType.Id,
            'Real-Time', 'Email;SMS');   
            
        Notification_Subscription__c subscription7 = NotificationsTestDataService.createSubscription(false, superCommUser.Id, null, newTripType.Id,
            'Weekly;Daily', 'Email');    
            
        Notification_Subscription__c subscription8 = NotificationsTestDataService.createSubscription(false, superCommUser.Id, null, clearedWarningType.Id,
            'Real-Time', 'SMS');         
            
        /*** Partner community user subscriptions ***/           
        Notification_Subscription__c subscription9 = NotificationsTestDataService.createSubscription(false, partnerCommUser.Id, null, newTripType.Id,
            'Weekly;Daily', 'Email');    
        

        insert new Notification_Subscription__c[]{
            subscription1, subscription2, subscription3, subscription4, subscription5, 
            subscription6, subscription7, subscription8, subscription9
        };

        notificationSettings.RMS_Alerts_Trigger_Enabled__c = false;
        update notificationSettings;
        
        CTS_RMS_Event__c event1 = NotificationsTestDataService.createRmsEvent(false, asset1.Id, 'Warning', 'System temperature warning1');
        event1.Event_Cleared__c = true;
        event1.Cleared_Timestamp__c = Datetime.now();

        CTS_RMS_Event__c event2 = NotificationsTestDataService.createRmsEvent(false, asset2.Id, 'Warning', 'System temperature warning2');
        event2.Event_Cleared__c = true;
        event2.Cleared_Timestamp__c = Datetime.now();

        CTS_RMS_Event__c event3 = NotificationsTestDataService.createRmsEvent(false, asset3.Id, 'Warning', 'System temperature warning3');
        CTS_RMS_Event__c event4 = NotificationsTestDataService.createRmsEvent(false, asset4.Id, 'Warning', 'System temperature warning4');
        CTS_RMS_Event__c event5 = NotificationsTestDataService.createRmsEvent(false, asset1.Id, 'Trip', 'System failure 1');
        CTS_RMS_Event__c event6 = NotificationsTestDataService.createRmsEvent(false, asset5.Id, 'Trip', 'System failure 2');
        
        CTS_RMS_Event__c[] rmsEvents = new CTS_RMS_Event__c[]{event1, event2, event3, event4, event5, event6};

        insert rmsEvents;

        system.assertEquals(0, [Select Count() From Notification_Message__c]);    
        
        // Standard user messages
        Notification_Message__c message1 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription1.Id, subscription1.Notification_Type__c, event1.Id, DateTime.now(), 'Real-Time', subscription1.Delivery_Methods__c);

        Notification_Message__c message2 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription2.Id, subscription2.Notification_Type__c, event1.Id, DateTime.now(), 'Real-Time', subscription2.Delivery_Methods__c);            

        Notification_Message__c message3 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription2.Id, subscription2.Notification_Type__c, event5.Id, DateTime.now(), 'Daily', 'Email');  
            
        Notification_Message__c message4 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription3.Id, subscription3.Notification_Type__c, event2.Id, DateTime.now(), 'Real-Time', subscription3.Delivery_Methods__c);            

        // Don't send yet
        Notification_Message__c message5 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription3.Id, subscription3.Notification_Type__c, event2.Id, DateTime.now().addDays(3), 'Weekly', 'Email');     
            
        Notification_Message__c message6 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription4.Id, subscription4.Notification_Type__c, event1.Id, DateTime.now(), 'Weekly', 'Email'); 
        
        // Don't send yet
        Notification_Message__c message7 = NotificationsTestDataService.createNotificationMessage(false, 
            standCommUser.Id, subscription5.Id, subscription5.Notification_Type__c, event2.Id, DateTime.now().addHours(1), 'Real-Time', subscription5.Delivery_Methods__c);            

        // Super user messages
        Notification_Message__c message8 = NotificationsTestDataService.createNotificationMessage(false, 
            superCommUser.Id, subscription6.Id, subscription6.Notification_Type__c, event3.Id, DateTime.now(), 'Real-Time', subscription6.Delivery_Methods__c);            

        Notification_Message__c message9 = NotificationsTestDataService.createNotificationMessage(false, 
            superCommUser.Id, subscription7.Id, subscription7.Notification_Type__c, event5.Id, DateTime.now(), 'Daily', 'Email');  

        // Don't send yet
        Notification_Message__c message10 = NotificationsTestDataService.createNotificationMessage(false, 
            superCommUser.Id, subscription7.Id, subscription7.Notification_Type__c, event5.Id, DateTime.now().addDays(2), 'Weekly', 'Email');              

        Notification_Message__c message11 = NotificationsTestDataService.createNotificationMessage(false, 
            superCommUser.Id, subscription8.Id, subscription8.Notification_Type__c, event2.Id, DateTime.now(), 'Real-Time', subscription8.Delivery_Methods__c);              

        // Partner user messages
        Notification_Message__c message12 = NotificationsTestDataService.createNotificationMessage(false, 
            partnerCommUser.Id, subscription6.Id, subscription9.Notification_Type__c, event6.Id, DateTime.now(), 'Real-Time', subscription9.Delivery_Methods__c);            

        insert new Notification_Message__c[]{
            message1, message2, message3, message4, message5, message6, message7, message8, message9, message10, 
            message11, message12
        };
    }    
}
    @isTest
    private static void deliverNotificationMessagesTestInternalUserJustMessageShell(){

        User internalUser = [Select Id, Name From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        CTS_RMS_Event__c rmsEvent = [Select Id, Name From CTS_RMS_Event__c LIMIT 1];
        Notification_Type__c notificationType = [Select Id, Name From Notification_Type__c LIMIT 1];

        Notification_Message__c notification1 = NotificationsTestDataService.createNotificationMessage(true, 
            internalUser.Id, null, null, null, DateTime.now(), 'Real-Time', 'Email;SMS;Community');

        List<Notification_Message__c> messagesInScope = new List<Notification_Message__c>{notification1};

        Test.startTest();

        // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        system.debug('messagesInScope before: ' + messagesInScope);
        messagesInScope = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesInScope));
        system.debug('messagesInScope after: ' + messagesInScope);

        system.assertEquals(1, messagesInScope.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesInScope);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesInScope);
    }    

    @isTest
    private static void deliverNotificationMessagesTestInternalUserNoSubscription(){

        User internalUser = [Select Id, Name From User Where LastName = 'Internal_User_Test1' LIMIT 1];
        CTS_RMS_Event__c rmsEvent = [Select Id, Name From CTS_RMS_Event__c LIMIT 1];
        Notification_Type__c notificationType = [Select Id, Name From Notification_Type__c LIMIT 1];

        Notification_Message__c notification1 = NotificationsTestDataService.createNotificationMessage(true, 
            internalUser.Id, null, notificationType.Id, rmsEvent.Id, DateTime.now(), 'Real-Time', 'Email;SMS;Community');

        List<Notification_Message__c> messagesInScope = new List<Notification_Message__c>{notification1};

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());--SGOTHAND -3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        system.debug('messagesInScope before: ' + messagesInScope);
        messagesInScope = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesInScope));
        system.debug('messagesInScope after: ' + messagesInScope);

        system.assertEquals(1, messagesInScope.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesInScope);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesInScope);
    }     

    @isTest
    private static void deliverNotificationMessagesTestRealTimeSuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }

    @isTest
    private static void deliverNotificationMessagesTestRealTimeOutsideDeliveryWindow(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        Contact[] contacts = [Select Id, CTS_Notifications_Delivery_Window_Start__c, CTS_Notifications_Delivery_Window_End__c From Contact];

        for (Contact c : contacts){
            c.CTS_Notifications_Delivery_Window_Start__c = Time.newInstance(0, 0, 0, 0);
            c.CTS_Notifications_Delivery_Window_End__c = Time.newInstance(0, 0, 0, 0);            
        }

        update contacts;

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(6, messagesToSend.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend, true);
    }    

    @isTest
    private static void deliverNotificationMessagesTestRealTimeFailure(){

       //system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

     //   TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = NotificationService.REAL_TIME;
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        //system.assertEquals(6, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;
        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();
        try{
            // NotificationsTestDataService.validateMessagesToSend(messagesToSend);
            
        }
        catch (Exception e){
            System.debug(e);
        }
    }    

    @isTest
    private static void deliverNotificationMessagesTestDailySuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

        // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Daily';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(2, messagesToSend.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }  
    
    @isTest
    private static void deliverNotificationMessagesTestDailyFailure(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());--SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Daily';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(2, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;
        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }     

    @isTest
    private static void deliverNotificationMessagesTestWeeklySuccess(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());- --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Weekly';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(1, messagesToSend.size());

        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }   
    
    @isTest
    private static void deliverNotificationMessagesTestWeeklyFailure(){

        system.assertEquals(12, [Select Count() From Notification_Message__c]);

        List<Notification_Message__c> messagesToSend;

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock()); --SGOTHAND 3/30/2022 SGOM-44

        DateTime runTime = DateTime.now();
        String deliveryFrequency = 'Weekly';
        messagesToSend = Database.query(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, messagesToSend));

        system.assertEquals(1, messagesToSend.size());

        NotificationDeliveryService.forceFail = true;
        NotificationDeliveryService.deliverNotificationMessages(messagesToSend);
        
        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend(messagesToSend);
    }     
}