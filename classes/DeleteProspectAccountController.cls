//Created by Capgemini Aman Kumar Date 8/1/2023 for US SFAVPTECH-105
public class DeleteProspectAccountController {
    
    @AuraEnabled
    public static Boolean checkOwner(String accountId) {
        Boolean isOwner = false;
        Business_Relationship__c b =[SELECT Id,Type__c,Account__r.Type,BR_Owner__c FROM Business_Relationship__c WHERE Id = :accountId];
        
        if(((b.Type__c !='Prospect' && b.Type__c !='Distributor End User') && b.Account__r.Type != 'Prospect') || UserInfo.getUserId() != b.BR_Owner__c){
            isOwner = true ;
        }        
        return isOwner;
    }
    
    @AuraEnabled
    public static Boolean markAccountForDeletion(String accountId) {
        // Query the business relationship record and its parent account
        Business_Relationship__c br = [SELECT Id,Type__c,Business__c,SAP_BR_External_Id__c,CTS_Channel__c, Account__c, 
                                       Account__r.Type,Account__r.RecordTypeId,BR_Owner__c
                                       FROM Business_Relationship__c WHERE Id = :accountId];//BR Record
        Account parentAccount = br.Account__r;
        
        // Check if the business relationship record is a "prospect" and the parent account is also a "prospect"
        if ((br.Type__c == 'Prospect' || br.Type__c =='Distributor End User') && parentAccount.Type == 'Prospect' ) {
            
            List<Business_Relationship__c> brList = [SELECT Id FROM Business_Relationship__c WHERE Account__c = :parentAccount.id];
            Account acc = [Select id,Name, (Select id FROM Cases), (Select ID,Business__c FROM Opportunities), (Select id,Business__c FROM Contacts) FROM Account WHERE ID = :parentAccount.id];
            
            if (brList.size() == 1){
               
                if( acc.Cases.Size() == 0 &&
                   acc.Opportunities.Size() == 0 &&
                   acc.Contacts.Size() == 0) {
                       
                       QueuableDeleteProspectAccounts qDeletePAccount = new QueuableDeleteProspectAccounts(br,acc);
                       ID jobID = System.enqueueJob(qDeletePAccount);
                       System.debug('jobID : '+jobID );
                       
                       
                   } else{
                       if(checkSameBusiness(acc, br)){
                           return true;
                       }else{
                           QueuableDeleteProspectAccounts qDeletePAccount = new QueuableDeleteProspectAccounts(br,null);
                           ID jobID = System.enqueueJob(qDeletePAccount);
                           System.debug('jobID : '+jobID );
                       } 
                       
                   }
                
            }else {
                if( acc.Cases.Size() == 0 &&
                   acc.Opportunities.Size() == 0 &&
                   acc.Contacts.Size() == 0) {

                       QueuableDeleteProspectAccounts qDeletePAccount = new QueuableDeleteProspectAccounts(br,null);
                       ID jobID = System.enqueueJob(qDeletePAccount);
                       System.debug('jobID : '+jobID );
                       
                       
                   } 
                if( acc.Cases.Size() > 0 ||
                   acc.Opportunities.Size() > 0 ||
                   acc.Contacts.Size() > 0) {
                       if(checkSameBusiness(acc, br)){
                           return true;
                       }else{
                           QueuableDeleteProspectAccounts qDeletePAccount = new QueuableDeleteProspectAccounts(br,null);
                           ID jobID = System.enqueueJob(qDeletePAccount);
                           System.debug('jobID : '+jobID );
                       }
                       
                       
                   }
            }
        }
        return false;
    }
    
    private static Boolean checkSameBusiness (Account acc, Business_Relationship__c br){
        Boolean hasSameBuisness = false;
        for(Opportunity opp : acc.Opportunities){
            if(opp.Business__c !=null && opp.Business__c.equalsIgnoreCase(br.Business__c)){
                hasSameBuisness = true;
            }
        }
        
        for(Contact con : acc.Contacts){
            if(con.Business__c !=null && con.Business__c.containsIgnoreCase(br.Business__c)){
                hasSameBuisness = true;
            }
        }
        
        
        return hasSameBuisness;
    }
}