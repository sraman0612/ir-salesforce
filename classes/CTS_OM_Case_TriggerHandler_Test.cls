@isTest
public with sharing class CTS_OM_Case_TriggerHandler_Test {
    static String CTS_OM_Email_Case_Record_Type_IDs;
    
    @TestSetup
    private static void createData(){
        Case_Trigger_ContactHandlerTest.createData();
    }    
    
    private static testMethod void testContactSingleMatch1(){
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c, CTS_OM_Email_Case_Record_Type_IDs_2__c, CTS_OM_Email_Case_Record_Type_IDs_3__c, CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
        System.debug('caseRecordTYpe Id passed>>'+CTS_OM_Email_Case_Record_Type_IDs); 
        Id caseRecIdToPass = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Tech_Direct_Case_Feedback').getRecordTypeId();
        Id contactRecIdToPass = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_Global_Distributor_Contact').getRecordTypeId();
        try{
        Case_Trigger_ContactHandlerTest.testContactSingleMatch1(caseRecIdToPass, contactRecIdToPass, 'CTS_OM_Contact_Match_Status__c');
        }
            catch(exception e){
          system.debug('Exception'+e);  
        }
    }
    
    
    /*private static testMethod void testContactSingleMatch2(){
        Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        Case_Trigger_ContactHandlerTest.testContactSingleMatch2(CTS_OM_Email_Case_Record_Type_IDs, settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c, 'CTS_OM_Contact_Match_Status__c');    
    } */   
    
    private static testMethod void testContactSingleDuplicateMatch(){
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c, CTS_OM_Email_Case_Record_Type_IDs_2__c, CTS_OM_Email_Case_Record_Type_IDs_3__c, CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
       Id caseRecIdToPass = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Tech_Direct_Case_Feedback').getRecordTypeId();
        Id contactRecIdToPass = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_Global_Distributor_Contact').getRecordTypeId();    	
        try{
        Case_Trigger_ContactHandlerTest.testContactSingleDuplicateMatch(caseRecIdToPass,contactRecIdToPass, 'CTS_OM_Contact_Match_Status__c');          
    }
        catch(exception e){
          system.debug('Exception'+e);  
        }
    }
    
    private static testMethod void testContactNoMatch1(){
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c, CTS_OM_Email_Case_Record_Type_IDs_2__c, CTS_OM_Email_Case_Record_Type_IDs_3__c, CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
        Id caseRecIdToPass = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Tech_Direct_Case_Feedback').getRecordTypeId();
        Id contactRecIdToPass = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_Global_Distributor_Contact').getRecordTypeId();
        system.debug('testContactNoMatch1 rec Ids >>'+caseRecIdToPass+''+contactRecIdToPass);
        try{
            Case_Trigger_ContactHandlerTest.testContactNoMatch1(caseRecIdToPass,contactRecIdToPass, 'CTS_OM_Contact_Match_Status__c');          
    }
      catch(exception e){
          system.debug('Exception'+e);  
        }   
    }
    
    private static testMethod void testContactNoMatch2(){
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,Email_Message_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c, CTS_OM_Email_Case_Record_Type_IDs_2__c, CTS_OM_Email_Case_Record_Type_IDs_3__c, CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
       Id caseRecIdToPass = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Tech_Direct_Case_Feedback').getRecordTypeId();
        Id contactRecIdToPass = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_Global_Distributor_Contact').getRecordTypeId();   
        try{
        Case_Trigger_ContactHandlerTest.testContactNoMatch2(caseRecIdToPass, contactRecIdToPass, 'CTS_OM_Contact_Match_Status__c');          
    }  
    catch(exception e){
          system.debug('Exception'+e);  
        }   
    }
}