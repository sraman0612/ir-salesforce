/***********************************************************************
 Class          : SmartAssetSearchExtension
 Author         : Appirio
 Created Date   : 4 march 2014
 Modified Date  : 29 July 2015
 Modified By    : Surabhi Sharma
 Descritption   : Provide functionality to asset search.
 ************************************************************************/
//
// Changed to without sharing so that users can see duplicates across the world
//
public without sharing class SmartAssetSearchExtensions {

    //Search criteria fields
    //public String accountNameToSearch {set;get;}
    public String assetNameToSearch {get;set;}
    public String assetSerialNumberToSearch{get;set;}
    //public String accountAddressToSearch {set;get;}
    //public String accountPhoneToSearch {set;get;}
    //public String accountFieldCsv {get;set;}
    public String assetFieldCsv {get;set;}
    //public String leadFieldCsv {get;set;}
    public string filterCondition {get;set;}
    public string soslSearchString {get;set;}
    public boolean disableaccButton{get;set;}
    
    

    //Constructor
    public SmartAssetSearchExtensions(ApexPages.StandardController controller) {
        init();
        disableaccButton = false;
        //checkRecordType();
         
     
    }

    //Constructor
    public SmartAssetSearchExtensions(){
        init();
        
       
        
    }

     
    private void init(){
        filterCondition = '';
        //createAccountFieldCsv();
        createAssetFieldCsv();
        //createLeadFieldCsv();
        soslSearchString = '';
    }
    
  /*  public void checkRecordType(){
          String recordType = ApexPages.currentPage().getParameters().get('RecordType');
          Id siteAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Account - NA Air').getRecordTypeId();          
          
          if(recordType == siteAccRecTypeId) {
              disableaccButton = true;
              
          }
     
      }  */

    // Method for create csv of field from fieldset of Account
    /* private void createAccountFieldCsv(){
        accountFieldCsv = SmartSearchUtility.createFieldCsv('Account','account_search_result');
    } */
    
    private void createAssetFieldCsv(){
        assetFieldCsv = SmartSearchUtility.createFieldCsv('Asset','asset_search_result');
    }

    // Method for create csv of field from fieldset of Lead
   /* private void createLeadFieldCsv(){
        leadFieldCsv = SmartSearchUtility.createFieldCsv('Lead','lead_search_result');
    } */

    //set to default status of page
    public void resetSearchStatus(){
        assetNameToSearch = '';
        assetSerialNumberToSearch = '';
        //accountNameToSearch = '';
       // accountAddressToSearch = '';
        //accountPhoneToSearch = '';
        filterCondition = '';
    }

    // This method do nothing, just used in action function to render the search list
    public void performSearch() {
        System.debug('assetNameToSearch::::::::::::' + assetNameToSearch);
        System.debug('assetSerialNumberToSearch::::::::::' + assetSerialNumberToSearch);
        //System.debug('accountNameToSearch:::::::::::' + accountNameToSearch);
        //System.debug('accountAddressToSearch ::::::::::::' + accountAddressToSearch);
       // System.debug('accountPhoneToSearch ::::::::::::' + accountPhoneToSearch);
        filterCondition = findSearchCondition();
    }

    public pagereference createNewAsset(){
        String recordType = ApexPages.currentPage().getParameters().get('RecordType');
        Pagereference pg = new Pagereference('/' + SmartSearchUtility.getPrefix('Asset') + '/e?retURL=/apex/SmartAssetSearch&nooverride=1&ass2=' + assetNameToSearch);
        pg.setRedirect(true);
        return pg;
    }
    
     
    
   
  /*  public pagereference createNewLead(){
        Pagereference pg = new Pagereference('/' + SmartSearchUtility.getPrefix('Lead') + '/e?retURL=/apex/SmartAccountSearch&nooverride=1&lea3=' + assetNameToSearch);
        pg.setRedirect(true);
        return pg;
    }    */

    public String findSearchCondition(){

        string query = '';
     

        if(String.isNotBlank(assetNameToSearch)){
            assetNameToSearch = assetNameToSearch.replace('*','');
            assetNameToSearch = String.escapeSingleQuotes(assetNameToSearch);
            query += ' Name like \'%' + assetNameToSearch.Trim() + '%\' AND';
        }
        
        if(String.isNotBlank(assetSerialNumberToSearch)){
            assetSerialNumberToSearch= assetSerialNumberToSearch.replace('*','');
            assetSerialNumberToSearch= String.escapeSingleQuotes(assetSerialNumberToSearch);
            query += ' SerialNumber like \'%' + assetSerialNumberToSearch.Trim() + '%\' AND';
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Asset Serial Number is required for search.'));
        }

        /*if(String.isNotBlank(accountAddressToSearch)){
            accountAddressToSearch = accountAddressToSearch.replace('*','');
            accountAddressToSearch = String.escapeSingleQuotes(accountAddressToSearch);
            query += ' (ShippingStreet like \'%' + accountAddressToSearch.Trim() + '%\' OR';
            query += ' ShippingCity like \'%' + accountAddressToSearch.Trim() + '%\' OR';
            query += ' ShippingState like \'%' + accountAddressToSearch.Trim() + '%\' OR';
            query += ' ShippingPostalCode like \'%' + accountAddressToSearch.Trim() + '%\' OR';
            query += ' ShippingCountry like \'%' + accountAddressToSearch.Trim() + '%\') AND';

        } */
     /*   if(String.isNotBlank(accountPhoneToSearch)){
            accountPhoneToSearch = accountPhoneToSearch.replace('*','');
            accountPhoneToSearch = String.escapeSingleQuotes(accountPhoneToSearch);
            query += ' Phone like \'%' + accountPhoneToSearch.Trim() + '%\' AND';
        } */

        if(String.isNotBlank(query)){
            query = query.substring(0,query.lastindexof('AND'));
        }
        system.debug(query);
        return query;
    }

}