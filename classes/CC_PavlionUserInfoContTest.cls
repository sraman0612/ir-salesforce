@isTest
public class CC_PavlionUserInfoContTest {
    
  @testSetup 
  static void setupData() {
      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
      Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
      User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@CC_PavlionUserInfoContTest.irco.com',Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
      Database.insert(portalAccountOwner1);
      System.runAs ( portalAccountOwner1 ) {
        Pavilion_Navigation_Profile__c pnp =new Pavilion_Navigation_Profile__c();
        pnp.Name='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN';
        pnp.Region__c='Asia Pacific; Canada; Latin America; United States';
        pnp.Display_Name__c='Sample admin';
        insert pnp;
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
        CC_Pavilion_Content__c pc=new CC_Pavilion_Content__c();
        pc.Type__c='Home Page Tile';
        pc.Title__c='Sample';
        pc.CC_isTile__c=true;
        insert pc;
        Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
        //creating county
        CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
        insert county;
        //creating sales rep here
        TestDataUtility testData = new TestDataUtility();
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        //Create account
        Account a = TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        a.CC_Global_Region__c = 'Test';
        a.CC_Primary_Address_NOT_Shipping_Billing__c = True;
        a.CC_Primary_Country__c = 'USA';
        a.CC_Sales_Rep__c = salesRepacc.Id;    
        insert a;
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',a.id);
        Insert sampleContract;
        //Create contacts
        Contact [] contactLst2Insert = new List<Contact>();
        Contact c = new Contact(
          FirstName = 'Test',
          Lastname = 'McTesty',
          AccountId = a.Id,
          Email = System.now().millisecond() + 'test@test.ingersollrand.com',  
          MobilePhone ='123123123',
          Title = 'Sales',
          Department = 'Sales'
        );
        contactLst2Insert.add(c);
        Contact c1 = new Contact(
          FirstName = 'Test',
          Lastname = 'McTesty',
          AccountId = a.Id,
          Email = System.now().millisecond() + 'test@test.ingersollrand.com',  
          MobilePhone ='123123123',
          Title = 'Sales',
          Department = 'Sales'
        );
        contactLst2Insert.add(c1);
        c=contactLst2Insert[0];
        c1=contactLst2Insert[1];
        Database.insert(contactLst2Insert);            
        //Create users
        User [] uLst2Insert = new List<User>();
        User user1 = TestUtilityClass.createUserWithTiles('CC_PavilionCommunityUser',c.id);
        user1.Warranty_Tavant__c = True;
        user1.Warranty_Tavant__c = True;
        user1.CC_View_Parts_Order_Invoice_Pricing__c = True;
        user1.CC_Part_Cart__c = True;
        user1.isActive = True;
        user1.MobilePhone ='123123123';
        user1.Title = 'Sales';
        user1.Department = 'Sales';
        user1.CC_Pavilion_Navigation_Profile__c='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN';
        user1.FirstName = 'test';
        uLst2Insert.add(user1);
        User uu1 =TestUtilityClass.createUserWithTiles('CC_PavilionCommunityUser',c1.id);
        uu1.Warranty_Tavant__c = False;
        uu1.CC_Part_Cart__c = False;
        uu1.CC_View_Parts_Order_Invoice_Pricing__c = True;
        uu1.MobilePhone ='123123123';
        uu1.Title = 'Sales';
        uu1.Department = 'Sales';
        uu1.IsActive = false;    
        uLst2Insert.add(uu1);
        Database.insert(uLst2Insert);
        
      }
  }

  public static testmethod void UnitTest_UserInfoContTest1(){
    User uu = [SELECT ID, Name, Email FROM User WHERE isActive=TRUE ORDER BY CREATEDDATE DESC LIMIT 1];
    User uu1 = [SELECT ID, Name, Email FROM User WHERE Id != :uu.Id ORDER BY CREATEDDATE DESC LIMIT 1];
    Contact c = [SELECT Id FROM Contact ORDER BY CREATEDDATE DESC LIMIT 1];
    Account a = [SELECT Id FROM Account ORDER BY CREATEDDATE DESC LIMIT 1];
    String caseSubject = 'Warranty Access';
    String caseDescription = 'Warranty Access has been Provided';
    CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',uu.Email,uu.name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, true, false, false, 'Manager', 'comments', 'manager', true,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');
    CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',uu.Email ,uu.Name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, false, true, false, 'SalesRep', 'comments', 'SalesRep', false,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');
    //CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',u.Email ,u.Name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, true, true, true, 'Support', 'comments', 'SalesRep', false,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');      
    test.starttest();
    System.runAs(uu){
      CC_PavilionTemplateController controller=new CC_PavilionTemplateController();
      controller.acctId=a.id;
      CC_PavlionUserInfoCont UserInfoCont = new CC_PavlionUserInfoCont(controller);
      UserInfoCont.useremail=uu.email;
      CC_PavlionUserInfoCont.UserListAndPavilionDisplayWrapper usrWrap=new CC_PavlionUserInfoCont.UserListAndPavilionDisplayWrapper(uu,[SELECT Display_Name__c FROM Pavilion_Navigation_Profile__c LIMIT 1].Display_Name__c);
      UserInfoCont.selectedSubOrdinates = 'testselectedSubOrdinates';
      UserInfoCont.SelectedSingleUser=uu1;
      UserInfoCont.email1 = uu1.id;
      UserInfoCont.getuserperms();
      UserInfoCont.selectedUserActive = 'Yes';
      UserInfoCont.upsertdata();
      UserInfoCont.SelectedSingleUser = uu;
      UserInfoCont.email1=uu.Id;
      UserInfoCont.getJSStart(); 
      UserInfoCont.upsertdata();
      String useremail = uu.Email;
      CC_PavlionUserInfoCont.getSelectedUserInfo(useremail,uu.Name);
      CC_PavlionUserInfoCont.getSubordinates(useremail);  
      CC_PavlionUserInfoCont.getSelectedUserAccountInfo(useremail);
      UserInfoCont.getpavilionUsers();
      UserInfoCont.getAllContacts1();
      UserInfoCont.getNavProfiles();  
      UserInfoCont.getroles();
      UserInfoCont.closeUser();
      UserInfoCont.email1=uu.Id;
      UserInfoCont.SelectedSingleUser=uu;
      UserInfoCont.getJSEnd();    
      UserInfoCont.allCurrentmanagers(uu.Id);
    }
    test.stoptest();
  }
  
  public static testmethod void UnitTest_UserInfoContTest2(){
    User uu = [SELECT ID, Name, Email FROM User WHERE isActive=TRUE ORDER BY CREATEDDATE DESC LIMIT 1];
    Contact c = [SELECT Id FROM Contact ORDER BY CREATEDDATE DESC LIMIT 1];
    Account a = [SELECT Id FROM Account ORDER BY CREATEDDATE DESC LIMIT 1];
    String caseSubject = 'Warranty Access';
    String caseDescription = 'Warranty Access has been Provided';
    CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',uu.Email,uu.name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, true, false, false, 'Manager', 'comments', 'manager', true,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');
    CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',uu.Email ,uu.Name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, false, true, false, 'SalesRep', 'comments', 'SalesRep', false,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');
    CC_PavlionUserInfoCont.saveCVF('selectedPricing', 'selectedSubOrdinates',uu.Email ,uu.Name, 'CC_Country', 'CC_State', 'CC_County', '500028', true, true, true, true, 'Support', 'comments', 'SalesRep', false,true , true, true, 'comments1', 'UserToReplace1',c.Id,'8179947559','9031212121');      
    test.starttest();
    System.runAs(uu){
      CC_PavilionTemplateController controller=new CC_PavilionTemplateController();
      controller.acctId=a.id;
      CC_PavlionUserInfoCont UserInfoCont = new CC_PavlionUserInfoCont(controller);
      UserInfoCont.useremail=uu.email;
      CC_PavlionUserInfoCont.UserListAndPavilionDisplayWrapper usrWrap=new CC_PavlionUserInfoCont.UserListAndPavilionDisplayWrapper(uu,[SELECT Display_Name__c FROM Pavilion_Navigation_Profile__c LIMIT 1].Display_Name__c);
      Id caseRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName= 'Club_Car_Case' AND SobjectType ='Case' LIMIT 1].Id;
      Id conId = [SELECT ContactId from User where id = :UserInfo.getUserId()].ContactId;
      string corpID = [SELECT Id, FederationIdentifier FROM User WHERE Id = :uu.Id].FederationIdentifier;
      Case [] caseLst4Insert = new List<Case>();
      Case newc = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc,'Warranty/Tavant Access Granted');
      newc.Description = 'Modify User' + '\n' + 'Part Cart Access has been Provided';
      caseLst4Insert.add(newc);
      Case newc1 = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc1,'Warranty/Tavat Access Revoked');
      newc1.Description = 'Modify User' + '\n' + 'Warranty/Tavat Access Revoked';
      caseLst4Insert.add(newc1);
      Case newc2 = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc2,'Part Cart Access Granted');
      newc2.Description = 'Modify User' + '\n' + 'Part Cart Access Granted';
      caseLst4Insert.add(newc2);
      Case newc3 = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc3,'Part Cart Access Revoked');
      newc3.Description = 'Modify User' + '\n' + 'Part Cart Access Revoked';
      caseLst4Insert.add(newc3);
      Case newc4 = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc4,'User Deactivated');
      newc4.Description = 'Modify User' + '\n' + 'User Deactivated';
      caseLst4Insert.add(newc4);
      Case newc5 = CC_PavlionUserInfoCont.createBaseCase(caseRecordTypeId,conId,corpID,uu.Id);
      CC_PavlionUserInfoCont.setCaseOwnerandSubReason(newc5,'User Re-Activated');
      newc5.Description = 'Modify User' + '\n' + 'User Re-Activated';
      caseLst4Insert.add(newc5);
      String jsonString = json.serialize(caseLst4Insert);
      CC_PavlionUserInfoCont.insertCasesFuture(jsonString);
      Case c2 = TestUtilityClass.createCaseWithRecordType(c.id,'testCase','Club Car');
      Set<id> conIds = new Set<id>();
      conIds.add(c.id);
      CC_PavlionUserInfoCont.updateContact(conIds);
      try{
        UserInfoCont.allcurrentSubrodinates(uu.Id);
        UserInfoCont.sendMailOnCVchange(uu,c);
      } Catch(Exception e){
      }
    }
    test.stoptest();
  }
}