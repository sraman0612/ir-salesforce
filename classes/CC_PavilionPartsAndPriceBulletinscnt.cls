global class CC_PavilionPartsAndPriceBulletinscnt{ 
  
  public static String partsBulletinBody{get;set;}
  
  public CC_PavilionPartsAndPriceBulletinscnt(CC_PavilionTemplateController controller){
    for (CC_Pavilion_Content__c pc : [SELECT CC_Body_1__c, Title__c  
                                        FROM CC_Pavilion_Content__c 
                                       WHERE Title__c = 'partsBulletinContent' and RecordType.Name = 'Home Page Text' LIMIT 1]) {
      partsBulletinBody=pc.CC_Body_1__c;
    }
  }
  
  @Remoteaction
  public static set<String> getMarkets(string aid){
    return getcontractSubTypes(aid);
  }
  
  @RemoteAction
  public static list<selectOption> getYears(){
    list<selectOption> catOpt= new list<selectOption>();
    list<Schema.PicklistEntry> values =ContentVersion.CC_Year__c.getDescribe().getPickListValues();
    for (Schema.PicklistEntry a : values){ catOpt.add(new SelectOption(a.getLabel(), a.getValue())); }
    return catOpt;
  }
  
  @Remoteaction
  Public static set<ContentVersion> getBulletins(boolean viewpartsbull, boolean viewpartprice, string region, string currncy,string aid){
    Id  usrId  = userinfo.getUserId();
    Boolean isInternal=aid==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
    set<string> contractSubTypes  = getcontractSubTypes(aid);
    List<ContentVersion> acctFilteredBulletinsLst = new List<ContentVersion>();
    set<ContentVersion> finalBulletinSet = new set<ContentVersion>();
    String qryStr = 'SELECT id,CC_Date__c,CC_To_Year__c,Bulletin_Title__c,Bulletin_Number__c,CC_Bulletin_Type__c,Agreement_Type__c,Currency__c,RecordType.Name ' +
                      'FROM ContentVersion ' + 
                     'WHERE RecordType.Name=\'bulletin\' and CC_Bulletin_Type__c!= null and (CC_Bulletin_Type__c=\'Parts\' or CC_Bulletin_Type__c=\'Pricing\') and Bulletin_Title__c != null';
    if (!isInternal){qryStr += ' and Currency__c INCLUDES(:currncy) and CC_Region__c INCLUDES(:region)';}
    acctFilteredBulletinsLst = Database.query(qryStr);
    if(isInternal){
      finalBulletinSet.addAll(acctFilteredBulletinsLst);
    } else {
      finalBulletinSet.addAll(filterBulletinsByUserandAgreement(acctFilteredBulletinsLst,contractSubTypes,viewpartsbull,viewpartprice));
    }
    return finalBulletinSet;
  }
  
  /* get bulletins from full text search */
  @RemoteAction
  global static List<ContentVersion> searchBulletins(boolean viewpartsbull, boolean viewpartprice, string region, string currncy,string aid,String SearchType) {
    Id  usrId  = userinfo.getUserId();
    Boolean isInternal=aid==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
    List<Contentversion> Templist=new List<ContentVersion>();
    set<string> contractSubTypes  = getcontractSubTypes(aid);
    List<ContentVersion> contentListToFilter = new List<ContentVersion>();
    String searchString = String.escapeSingleQuotes(SearchType); 
    String RecordTypeName='bulletin';
    String searchQuery = 'FIND {%' + searchString + '} IN ALL FIELDS ' +
                         'RETURNING  ContentVersion (Bulletin_Title__c, Bulletin_Number__c, CC_Bulletin_Type__c,CC_Region__c,CC_Date__c, CC_To_Year__c, Currency__c ,Agreement_Type__c, RecordType.Name ' +
                         'WHERE RecordType.Name= \''+RecordTypeName+ '\' and CC_Bulletin_Type__c != null and (CC_Bulletin_Type__c=\'Parts\' or CC_Bulletin_Type__c=\'Pricing\'))'; 
    if (!isInternal){
      searchQuery=searchQuery.removeEnd(')');
      searchQuery += ' and Currency__c INCLUDES ( \''+currncy+ '\') and  CC_Region__c INCLUDES (\''+region+ '\') and Bulletin_Title__c != null)';
    }
    system.debug('### searchquery is ' + searchQuery);
    for(List<ContentVersion> cList : search.query(searchQuery)) {for(ContentVersion c : cList) {contentListToFilter.add(c);}} 
    Templist = filterBulletinsByUserandAgreement(contentListToFilter,contractSubTypes,viewpartsbull,viewpartprice);
    return Templist;
  }
  
  private static set<String> getcontractSubTypes(String acctId){
    set<string> subtypeSet = new set<string>();
    Contract []  cLst = [SELECT id,name,CC_Sub_Type__c,CC_Type__c 
                           FROM Contract 
                           WHERE AccountId=:acctId and CC_Type__c='Dealer/Distributor Agreement' and 
                                 CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and 
                                 CC_Contract_End_Date__c >=:system.today()];
    for(Contract c: cLst){if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != ''){subtypeSet.add(c.CC_Sub_Type__c); }}                             
    return subtypeSet;
  }
  
  private static ContentVersion [] filterBulletinsByUserandAgreement(List<ContentVersion> cvLst, set<String> subTypes, Boolean vpb, Boolean vpp){
    List<ContentVersion> contentListToReturn = new List<ContentVersion>();
    List<ContentVersion> contentListUserFiltered = new List<ContentVersion>();
    for(ContentVersion cv : cvLst){
      if(vpb && cv.CC_Bulletin_Type__c=='Parts'){contentListUserFiltered.add(cv);continue;}
      if(vpp && cv.CC_Bulletin_Type__c=='Parts Pricing'){contentListUserFiltered.add(cv);continue;}  
    }
    for(ContentVersion cv2 : contentListUserFiltered) {
      for(String s : cv2.Agreement_Type__c.split(';')){if(subTypes.contains(s)){contentListToReturn.add(cv2);break;}}
    }
    return contentListToReturn;
  }
}