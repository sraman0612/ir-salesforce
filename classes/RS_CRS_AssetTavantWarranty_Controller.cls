/****************************************************************
Name:		RS_CRS_AssetTavantWarranty_Controller
Purpose:	Used for redirecting to lightining tab "RS_CRS_Tavant_Warranty_Info"
having lightning component "RS_CRS_TavantAssetWarrantyInfo"
History                                                            
-------                                                            
VERSION		AUTHOR			DATE			DETAIL
1.0			Ashish Takke	03/26/2018		INITIAL DEVELOPMENT
*****************************************************************/
public with sharing class RS_CRS_AssetTavantWarranty_Controller {
    //Asset Record ID
    public ID assetId;
    
    /*****************************************************************************
	Controller
	******************************************************************************/
    public RS_CRS_AssetTavantWarranty_Controller(ApexPages.StandardController controller) {
        assetId = ApexPages.currentPage().getParameters().get('assetId');
    }
    
    /*****************************************************************************
	Purpose            :  Navigate to lightining tab "RS_CRS_Tavant_Warranty_Info"
	Parameters         :  NA
	Returns            :  PageReference - to lightining tab
	Throws [Exceptions]:                                                          
	******************************************************************************/
    public PageReference navToTavantWarrantyInfo(){ 
        try{
            //Getting Asset Record Type Id through Schema.Describe Class
            Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);            
            
            //get Asset Serial# based on asset record id
            Asset assetRec = [Select Id, Name From Asset Where Id =: assetId and RecordTypeId =: assetRecId];
                
            //Save Asset Serial# on logged in user record to send it as SAML attribute to Tavant canvas app
            User userRec = [Select Id, RS_CRS_Asset_Serial_Number__c 
                            From User Where Id =: UserInfo.getUserId() limit 1];
            userRec.RS_CRS_Asset_Serial_Number__c = assetRec.Name;
            update userRec;
            
            //redirect to "RS_CRS_Tavant_Warranty_Info" lightning component tab
            PageReference pageRef = new PageReference ('/one/one.app?source=alohaHeader#/n/RS_CRS_Tavant_Warranty_Info');
            pageRef.setRedirect(true);
            return pageRef; 
            
        } catch(Exception ex){
            System.debug('Error occurred while getting Model# in \'RS_CRS_AssetTavantWarranty_Controller\' \''+ex.getMessage() + '\' at Line# ' + ex.getLineNumber());
            return null;
        }
        
    }
    
}