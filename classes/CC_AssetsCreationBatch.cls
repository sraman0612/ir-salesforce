global class CC_AssetsCreationBatch implements Schedulable, Database.Stateful,Database.Batchable<sObject>{
  
	global string queryStr{get;set;}
	global Set<String> productstoAssets{get;set;}
	global Map<String, String> errMap {get;set;}
	
    global CC_AssetsCreationBatch(string qry){
	    queryStr = qry;
	    productstoAssets = new Set<String>();
        errMap = new Map<String,String>();
      
	    for(Club_Car_Product_to_Asset_Map__c r : Club_Car_Product_to_Asset_Map__c.getall().values()){
	        productstoAssets.add(r.Name);
	    }  	
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(queryStr);
    }

    /*
      !!!VERY IMPORTANT!!! - this batch class is written to process only 1 order shipment at a time
    */    
	global void execute(Database.BatchableContext bc, List<CC_Order_Shipment__c> shipmentLst){
    
        if(shipmentLst.size()==1){

            string orderShipmentId = shipmentLst[0].Id;
            Schema.SObjectField key = CC_Asset_Group__c.Asset_Group_Key__c;
            Boolean successfullyProcessed = TRUE;
            Savepoint sp = Database.setSavepoint();
      
            try{
         
                List<CC_Order_Shipment_Item__c> nonCarShipmentItems = new List<CC_Order_Shipment_Item__c>();
                String acctSiebelIdforAssets = shipmentLst[0].Order__r.CC_Account__r.CC_Siebel_ID__c;
                Map<String,CC_Asset_Group__c> existingAssetGroups = new Map<String,CC_Asset_Group__c>();
                Map<String,CC_Asset_Group__c> carAssetGroups2Upsert = new Map<String,CC_Asset_Group__c>();
                Map<String,CC_Asset_Group__c> productAssetGroups2Upsert = new Map<String,CC_Asset_Group__c>();
                //List<CC_Asset_Group__c> productassetgroups = new List<CC_Asset_Group__c>();
                List<CC_Asset__c> assetsList = new List<CC_Asset__c>();
                
                /* we prefer the end customer siebel id from the order over the siebel id of bill to for the order */
    	        if(null != shipmentLst[0].Order__r.Siebel_Acct_Row_ID__c){acctSiebelIdforAssets = shipmentLst[0].Order__r.Siebel_Acct_Row_ID__c;}
                      
                /* existing asset group records for the account */
                for(CC_Asset_Group__c ag : [SELECT Id, Asset_Group_Key__c, Quantity__c FROM CC_Asset_Group__c WHERE Account__r.CC_Siebel_ID__c = :acctSiebelIdforAssets]){
                    existingAssetGroups.put(ag.Asset_Group_Key__c,ag);
                }

                /* car item for this shipment which holds s/no of individual car assets to create */
                for(CC_Car_Item__c carItem : [SELECT id,Name, Order_Item__r.Order__c,Shipment_Header_Number__c,Asset_Status__c,Serial_No_s__c,
                                            Order_Item__r.Product__r.ProductCode,Order_Item__r.Order__r.Order_Number__c,
                                            Order_Item__r.Order__r.CC_Quote_Order_Date__c,order_item__r.Product__c,Truck_Quantity__c,
                                            Order_Item__r.Order__r.CC_Account__r.CC_Enterprise_Code__c,
                                            (SELECT id, Name,Color__c FROM Car_Features__r)
                                    FROM CC_Car_Item__c 
                                    WHERE Order_Item__r.Order__c = :shipmentLst[0].Order__c AND
                                            Shipment_Header_Number__c = :shipmentLst[0].Shipment_Header_Number__c]){
        
                    String AGKey = 	carItem.Order_Item__r.Order__r.Order_Number__c + '_' + carItem.Order_Item__r.Order__r.CC_Quote_Order_Date__c + '_' + carItem.Order_Item__r.Product__r.ProductCode;
                    CC_Asset_Group__c fleet  = new CC_Asset_Group__c();  
        
                    if(carAssetGroups2Upsert.containsKey(AGKey)){

                        fleet = carAssetGroups2Upsert.get(AGKey);
                        fleet.Quantity__c += carItem.Truck_Quantity__c;        
                        carAssetGroups2Upsert.put(AGKey,fleet);
                    } 
                    else if (existingAssetGroups.containsKey(AGKey)){

                        fleet = existingAssetGroups.get(AGKey);
                        fleet.Quantity__c += carItem.Truck_Quantity__c;        
                        carAssetGroups2Upsert.put(AGKey,fleet);
                    } 
                    else {

                        fleet.putSObject('Account__r', new Account(CC_Siebel_Id__c = acctSiebelIdforAssets));
                        fleet.Acquired_Date__c = shipmentLst[0].Ship_Date__c;
                        fleet.Asset_Group_Key__c = AGKey;
            
                        for(CC_Car_Feature__c cf : carItem.Car_Features__r){
                
                            if(null != cf.Color__c){
                                fleet.Color__c = cf.Color__c;
                                break;
                            }
                        }
                            
                        if(carItem.Serial_No_s__c != NULL){fleet.Model_Year__c = '20' + carItem.Serial_No_s__c.subString(2,3) + carItem.Serial_No_s__c.substring(3,4);}
                        
                        if (carItem.Order_Item__r.Order__r.CC_Account__r.CC_Enterprise_Code__c == 'F' ){
                            fleet.Fleet_Status__c = 'Lease-New';
                        }
                        else{
                            fleet.Fleet_Status__c = 'Purchase-New';
                        }                            

                        fleet.Order__c = carItem.Order_Item__r.Order__c;
                        fleet.Product__c = carItem.order_item__r.Product__c;
                        fleet.Quantity__c = carItem.Truck_Quantity__c;
                        carAssetGroups2Upsert.put(AGKey,fleet);        
                    }

                    assetsList.addAll(createAssets(carItem,AGKey,acctSiebelIdforAssets,shipmentLst[0].Ship_Date__c));
                }
                    
                /* create asset group records for eligible non car items */
                for(CC_Order_Shipment_Item__c osi : [SELECT Order_Item__r.Product__r.ProductCode,
                                                Order_Shipment__r.Ship_Date__c,Order_Item__r.Order__r.Order_Number__c,
                                                Order_Item__r.Order__r.CC_Quote_Order_Date__c,Order_Shipment__r.Order__c,
                                                Order_Item__r.Product__c,Ship_Quantity__c
                                            FROM CC_Order_Shipment_Item__c
                                            WHERE Order_Shipment__c = :orderShipmentId]){
                    
                    if(productstoAssets.contains(osi.Order_Item__r.Product__r.ProductCode)){
                
                        String  AGKey = osi.Order_Item__r.Order__r.Order_Number__c + '_' + osi.Order_Item__r.Order__r.CC_Quote_Order_Date__c+'_'+ osi.Order_Item__r.Product__r.ProductCode;
        
                        if(productAssetGroups2Upsert.containsKey(AGKey)){
                        
                            CC_Asset_Group__c ag2Update = productAssetGroups2Upsert.get(AGKey);
                            ag2Update.Quantity__c += osi.Ship_Quantity__c;
                            productAssetGroups2Upsert.put(AGKey,ag2Update);
                        } 
                        else if (existingAssetGroups.containsKey(AGKey)){
                            
                            CC_Asset_Group__c ag2Update = existingAssetGroups.get(AGKey);
                            ag2Update.Quantity__c += osi.Ship_Quantity__c;
                            productAssetGroups2Upsert.put(AGKey,ag2Update);
                        } 
                        else {
            
                            CC_Asset_Group__c assetGroup  = new CC_Asset_Group__c();
                            assetGroup.putSObject('Account__r', new Account(CC_Siebel_Id__c = acctSiebelIdforAssets));
                            assetGroup.Acquired_Date__c      = 	osi.Order_Shipment__r.Ship_Date__c;
                            assetGroup.Asset_Group_Key__c    = 	osi.Order_Item__r.Order__r.Order_Number__c + '_' + osi.Order_Item__r.Order__r.CC_Quote_Order_Date__c+'_'+ osi.Order_Item__r.Product__r.ProductCode;
                            assetGroup.Fleet_Status__c        = 'Purchase-New';
                            assetGroup.Order__c               = osi.Order_Shipment__r.Order__c;
                            assetGroup.Product__c             = osi.Order_Item__r.Product__c;
                            assetGroup.Quantity__c            = osi.Ship_Quantity__c;
                            productAssetGroups2Upsert.put(AGKey,assetGroup);
                        }
                    }
                }

                /* upsert car asset group records */   
                if(!carAssetGroups2Upsert.isEmpty()){

                    Database.UpsertResult[] srLst = Database.Upsert(carAssetGroups2Upsert.values(),key,false);
        
                    for (Database.UpsertResult sr : srLst) {
        
                        for(Database.Error err : sr.getErrors()) {
                            errMap.put(orderShipmentId,'Failed to upsert asset group ' + sr.getId() + ' from ' + carAssetGroups2Upsert.values() + ' because ' + err.getMessage() + ' :: ' + err.getFields());
                            successfullyProcessed = FALSE;
                        }
                    }
                } 

                /* insert car assset detail records */
                if(successfullyProcessed && !assetsList.isEmpty()){

                    Database.SaveResult[] srLst = Database.insert(assetsList,false);
        
                    for (Database.SaveResult sr : srLst) {
        
                        for(Database.Error err : sr.getErrors()) {
                            errMap.put(orderShipmentId, 'Failed to upsert asset ' + sr.getId() + ' from ' + assetsList + ' because ' + err.getMessage() + ' :: ' + err.getFields());
                            successfullyProcessed = FALSE;
                        }
                    }
                }
    
                /* upsert non car asset group records */
                if(successfullyProcessed && !productAssetGroups2Upsert.isEmpty()){
        
                    Database.UpsertResult [] srLst = Database.upsert(productAssetGroups2Upsert.values(),key,false);  
        
                    for (Database.UpsertResult sr : srLst) {
        
                        for(Database.Error err : sr.getErrors()) {
                            errMap.put(orderShipmentId, 'Failed to upsert non car asset group ' +  sr.getId() + ' from ' + productAssetGroups2Upsert.values() + ' because ' + err.getMessage() + ' :: ' + err.getFields());
                            successfullyProcessed = FALSE;
                        }
                    }
                }

                /* update opp actual ship date */
                if(null != shipmentLst[0].Order__r.Siebel_Opp_Row_ID__c && null != shipmentLst[0].Ship_Date__c){ 
        
                    Opportunity opp2Update;
        
                    for(Opportunity o : [SELECT id, CC_Actual_Ship_Date__c FROM Opportunity WHERE Siebel_ID__c = :shipmentLst[0].Order__r.Siebel_Opp_Row_ID__c]){
                        o.CC_Actual_Ship_Date__c =  shipmentLst[0].Ship_Date__c;  
                        opp2Update = o;
                        }
                        
                    if(null!=opp2Update){
                    
                        Database.SaveResult oppsr = database.update(opp2Update,false);
            
                        for(Database.Error err : oppsr.getErrors()) {
                            errMap.put(orderShipmentId, ' Failed to update opp ' + opp2Update + ' ' + oppsr.getId() + ' becasuse ' +  err.getMessage() + ' :: ' + err.getFields());
                        }
                    }
                }
                    
                /* update the car assets created flag to true for this shipment */
                if(successfullyProcessed){

                    shipmentLst[0].Car_Assets_Created__c = TRUE;
                    Database.SaveResult shpmtsr = database.update(shipmentLst[0],false);
        
                    for(Database.Error err : shpmtsr.getErrors()) {
                        errMap.put(orderShipmentId, 'Assets created but failed to update shipment processed flag because ' + err.getMessage() + ' :: ' + err.getFields());        
                        Database.rollback(sp);
                    }
                } 
                else {
                    Database.rollback(sp);
                }
            } 
            catch (exception e){
                errMap.put(orderShipmentId, 'Shipment processing failed because: ' + e.getMessage() + ' :: ' + e.getStackTraceString()); 
                Database.rollback(sp);
            }
        }
    }
    
    global static List<CC_Asset__c> createAssets(CC_Car_Item__c car, String agkey,String acctsiebelid,Date shipDate){
      
        List<CC_Asset__c> assets   = new  List<CC_Asset__c>();
      
        if(car.Serial_No_s__c != NULL){
        
            List<string> serialNumbers = car.Serial_No_s__c.split('; ');
        
            for(String sn : serialNumbers){
          
                CC_Asset__c  ast = new CC_Asset__c ();
                ast.putSObject('Account__r', new Account(CC_Siebel_Id__c = acctsiebelid));
                ast.Acquired_Date__c  = shipDate;
                ast.putSObject('Asset_Group__r', new CC_Asset_Group__c(Asset_Group_Key__c = agkey));
                ast.Order__c = car.Order_Item__r.Order__c;
                ast.Product__c = car.order_item__r.Product__c;
                ast.Status__c = 'Active';
                ast.Model_Year__c = '20'+ sn.subString(2,3) + sn.substring(3,4);

                if(car.Car_Features__r != NULL && car.Car_Features__r.size() > 0){ast.Color__c = car.Car_Features__r[0].Color__c;}
        
                if(sn.contains('~')){
                    string [] snArr = sn.split('~');
                    ast.Serial__c = snArr[0];
          
                    if (snArr.size() > 1){
                        ast.Decal_Number__c = snArr[1].substringBefore(';');  
                    }
                } 
                else {
                    ast.Serial__c = sn.substringBefore(';');
                }

                assets.add(ast);
            }
        } 
          
  	    system.debug('### list of assets to be created is ' + assets);       
  	    return assets;
    }
    
	global void finish(Database.BatchableContext bc){
      
        if(test.isRunningTest()){
        
            if (null==errMap) {errMap = new Map<String,String>();}
  	        errMap.put('testrecordid','testerrmsg');
        }

  	    if(null != errMap && !errMap.isEmpty()){
        
            messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();    
            String[] sToAddress = new String[]{};
            CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
      
            if (null != batchControls && null != batchControls.Asset_Create_Failure_Email__c){
                sToAddress.add(batchControls.Asset_Create_Failure_Email__c);
            } 
            else {
                stoAddress.add('lizy_frady@clubcar.com');  
  		    }
          
            eml.setToAddresses(sToAddress);
    	    eml.setSenderDisplayName('Salesforce.com');
    	    eml.setSubject('Error Summary for Asset Create Batch Job');
    	    string msg = 'The below could not be created/updated' + '\n';
          
            for(String s : errMap.keyset()){msg += s + ' ' + errMap.get(s) + '\n';}
  		    eml.setPlainTextBody(msg);
  		    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{eml});
  	    }  
	}  
    
    /*
      !!!VERY IMPORTANT!!! - the batch class is written to process only 1 order shipment at a time
    */    
	global void execute(SchedulableContext sc) {
    
        String createdDateStr = '2020-02-01T00:00:00.000Z';
        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
    
        if (null != batchControls && null != batchControls.Asset_Min_CreatedDate__c){
            createdDateStr = batchControls.Asset_Min_CreatedDate__c;
	    }
    
        String qryStr = 'SELECT Id, Order__r.Siebel_Acct_Row_ID__c, Ship_Date__c, Order__r.Order_Number__c,' +
                    'Order__r.CC_Quote_Order_Date__c, Shipment_Header_Number__c, Car_Assets_Created__c,' +
                    'Order__r.Siebel_Opp_Row_ID__c, Order__r.CC_Account__r.CC_Siebel_ID__c '+
                    'FROM CC_Order_Shipment__c ' +
                    'WHERE Car_Assets_Created__c = FALSE AND ' +
                    'Invoice_Date__c != NULL AND ' +
                    'Invoice_Number__c != NULL AND ' +
                    'CreatedDate > ' + createdDateStr;
      
        CC_AssetsCreationBatch acb = new CC_AssetsCreationBatch(qryStr);      
        Database.executebatch(acb,1);    
	}
}