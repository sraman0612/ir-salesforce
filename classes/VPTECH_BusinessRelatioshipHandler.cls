//Created By Capgemini Aman kumar 12/15/2022 
public class VPTECH_BusinessRelatioshipHandler {

    public static void beforeInsert(List<Business_Relationship__c> newBussinessRelationship){
     userBusinessValidation(newBussinessRelationship);
    }
    
    public static void beforeUpdate(List<Business_Relationship__c> newBussinessRelationship, List<Business_Relationship__c> oldBussinessRelationship, Map<Id,Business_Relationship__c> oldMap){
     userBusinessValidation(newBussinessRelationship);
    }
    
    public static void afterInsert(List<Business_Relationship__c> newBussinessRelationship){
       
       
        /*QueuableDistributorAccount qDisAcc = new QueuableDistributorAccount(newBussinessRelationship);
        ID jobID = System.enqueueJob(qDisAcc);
        System.debug('jobID : '+jobID );*/
    }
    
    public static void afterUpdate(List<Business_Relationship__c> newBussinessRelationship,Map<id,Business_Relationship__c> oldMapBussinessRelationship){
      
        
        
        /*List<Business_Relationship__c> listBusinessRelationShip = new List<Business_Relationship__c>();        
        for(Business_Relationship__c br : newBussinessRelationship){
            if( br != null && oldMapBussinessRelationship != null && br.id == oldMapBussinessRelationship.get(br.id).id && br.CTS_Channel__c != oldMapBussinessRelationship.get(br.id).CTS_Channel__c && br.CTS_Channel__c == 'Distributor'){
                listBusinessRelationShip.add(br);
            }
        }
        
        QueuableDistributorAccount qDisAcc = new QueuableDistributorAccount(listBusinessRelationShip);
        ID jobID = System.enqueueJob(qDisAcc);
        System.debug('jobID : '+jobID );*/
    }
    
    public static void afterDelete(List<Business_Relationship__c> oldBussinessRelationship){
        
    }
    
    public static void userBusinessValidation(List<Business_Relationship__c> newBussinessRelationship){
        User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
        
        for(Business_Relationship__c newBR:newBussinessRelationship){
            System.debug('user.Business__c: '+ user.Business__c );
            System.debug('acc.Business__c : '+ newBR.Business__c );
            System.debug('user.Channel__c : '+ user.Channel__c);
            System.debug('acc.Channel__c : '+ newBR.CTS_Channel__c );
            System.debug('***user role name****'+ user.UserRole.name);
            System.debug('***user role name****'+ user.profile.name);
           // if(user.UserRole.name!= null  && !(string.valueOf(user.UserRole.name).contains('India'))){
            if(user.Name != 'CPI Integration' && user.profile.name != 'System Administrator' && (newBR.Type__c == 'Prospect' || newBR.Type__c == 'Distributor End User')){
                if(user.Business__c!=null && !user.Business__c.containsIgnoreCase(newBR.Business__c ) ){//|| !user.Channel__c.containsIgnoreCase(newBR.CTS_Channel__c )) {
                	System.debug('Error Condition!!!');
                	newBR.addError(System.Label.VPTECH_Account_Error);
            	}
            }
           // }
        }
    }
    
}