/*****************************************************************************
Name:       RS_CRS_AssetTavantWarranty_CtrlTest
Purpose:    Test class for RS_CRS_AssetTavantWarranty_Controller. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Ashish Takke    03/26/2017  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_AssetTavantWarranty_CtrlTest {
    
    Static testmethod void navToTavantWarrantyInfo_Test(){
        //Getting Case Record Type Id through Schema.Describe Class
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type);
        
        //Getting Asset Record Type Id through Schema.Describe Class
        Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        Test.startTest();
        
        //Create test user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        insert userRec;
                
        System.runAs(userRec){
            //insert test Account
            Account acc = new Account();
            acc.recordTypeId = accRecId;
            acc.LastName = 'Test Account 1';
            acc.Phone = '123456789';
            acc.Email_ID__c = 'first.last@email.com';
            insert acc;   
            
            Asset assetRec = new Asset();
            assetRec.recordTypeId = assetRecId;
            assetRec.Name = 'Test_Asset';
            assetRec.SerialNumber = '12345';
            assetRec.accountId = acc.Id;
            assetRec.Manufacturer__c = 'Trane';
            insert assetRec;
                        
            PageReference pageRef = new PageReference('/apex/RS_CRS_AssetTavantWarrantyInfo?assetId='+assetRec.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(assetRec);
            RS_CRS_AssetTavantWarranty_Controller ctrl = new RS_CRS_AssetTavantWarranty_Controller(stdController);
            ctrl.navToTavantWarrantyInfo();            
        }
        
        Test.stopTest();
        
        User user = [Select Id, RS_CRS_Asset_Serial_Number__c From User Where Id =: userRec.Id Limit 1];
        System.assertEquals('Test_Asset', user.RS_CRS_Asset_Serial_Number__c);
    }
}