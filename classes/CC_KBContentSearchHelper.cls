/**********************************************
 * CC_KBContentSearchHelper
 * Helper class of Knowledge Article and ContentVersion search 
 **********************************************/
public class CC_KBContentSearchHelper {

    /**
     * build Knowledge Article Query
     * @param fieldsForQuery, filters
     * @return String 
     */
    public static String buildKnowledgArticleQuery(List < String > fieldsForQuery, List < CC_KB_Content_Search_Filter_Config__mdt > filters) {

        String objectName = 'Knowledge__kav';
        String kbFields = 'Id';
        for (String fd: fieldsForQuery) {
            kbFields += ',' + fd;
        }
        String orderByField = 'Title';
        String kbFilter = buildFilters(filters);
        String kbQuery = objectName + ' (' + kbFields + ' WHERE ' + kbFilter +
            ' ORDER BY ' + orderByField + '),';
        System.debug('kbQuery**' + kbQuery);
        return kbQuery;
    }
    /**
     * build content Query
     * @param fieldsForQuery, types
     * @return String 
     */
    public static String buildContentQuery(List < String > fieldsForQuery, Set < String > types, List < CC_KB_Content_Search_Filter_Config__mdt > filters) { // TBD for filters
        String contentQuery = '';
        String objectName = 'ContentVersion';
        String contentFields = 'Id'; //Added in metadata Title, ContentDocumentId, ContentDocument.Description, FileExtension, FileType';

        for (String fd: fieldsForQuery) {
            contentFields += ',' + fd;
        }

        contentQuery = objectName + ' (' + contentFields + ' WHERE (RecordType.Name=\'Tech Pubs\' AND IsLatest = TRUE) OR ';
        contentQuery += '(RecordType.Name=\'bulletin\''; 

        /* currenctly including all types
        if (types.contains('Bulletins') && !types.contains('Content')) {
            contentQuery += 'CC_Bulletin_Type__c != null AND ';
        } else if (!types.contains('Bulletins') && types.contains('Content')) {
            contentQuery += 'CC_Bulletin_Type__c == null AND ';
        }
        */

        //Additional Content filters
        userFields = getUserFields();
        Id accId = null == userFields.AccountId ? PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c : userFields.AccountId;
        Account acc = new Account();
        acc = [SELECT Id, Name, CC_Global_Region__c, Currency__c
            FROM Account
            WHERE Id =: accId LIMIT 1
        ];
        if (null != acc) contentQuery += ' AND Currency__c INCLUDES (\''+acc.Currency__c+'\') AND CC_Region__c INCLUDES (\'' + acc.CC_Global_Region__c+ '\')';

        List < String > subtypeSet = getContractSubTypes(accId);
        String subTypes = String.join(subtypeSet, ',');
        if (!String.isBlank(subTypes)) {
          contentQuery += ' AND (';
          Boolean firstTime = TRUE;
          for(String subtype : subtypeSet){
            if(!firstTime){
              contentQuery += ' OR';
            } else {
              firstTime=FALSE;
            }
            contentQuery += ' Agreement_Type__c INCLUDES (\'' + subType + '\')';
          }
          contentQuery += ')';
        }
        List<String> bulletinTypes = new List<String>();
        if (userFields.CC_View_Parts_Bulletins__c) bulletinTypes.add('\'Parts\'');
        if (userFields.CC_View_Sales_Bulletins__c) bulletinTypes.add('\'Sales\'');
        if (userFields.CC_View_Service_Bulletins__c) bulletinTypes.add('\'Service & Warranty\'');
        if (userFields.CC_View_Parts_Pricing__c) bulletinTypes.add('\'Parts Pricing\'');
        if (userFields.CC_View_Sales_Pricing__c) bulletinTypes.add('\'Sales Pricing\'');
        system.debug('### bulletin types are ' + bulletinTypes);
        if(!bulletinTypes.isEmpty())
          contentQuery += ' AND CC_Bulletin_Type__c IN ' + bulletinTypes +'';
        contentQuery += ') ORDER BY FileType) ';
        return contentQuery;
    }

    public static User userFields;
    public static User getUserFields() {
        if (null == userFields) {
            userFields = [SELECT id, name, CC_Pavilion_Navigation_Profile__c, CC_View_Parts_Bulletins__c, CC_View_Sales_Bulletins__c, CC_View_Service_Bulletins__c,
                CC_View_Sales_Pricing__c, CC_View_Parts_Order_Invoice_Pricing__c, CC_View_Parts_Pricing__c, AccountId, CC_Sales__c,
                FirstName, LastName, FederationIdentifier, Email
                FROM User
                WHERE id =: userinfo.getUserId() LIMIT 1
            ];
        }
        return userFields;
    }
    private static List < String > getContractSubTypes(String acctId) {
        List < string > subtypeSet = new List < string > ();
        Contract[] cLst = [SELECT id, name, CC_Sub_Type__c, CC_Type__c
            FROM Contract
            WHERE AccountId =: acctId and CC_Type__c = 'Dealer/Distributor Agreement'
            and
            CC_Contract_Status__c != 'Suspended'
            and CC_Contract_Status__c != 'Terminated'
            and CC_Contract_Status__c != 'Expired'
            and
            CC_Contract_End_Date__c >=: system.today()
        ];
        for (Contract c: cLst) {
            if (c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '') {
                subtypeSet.add(c.CC_Sub_Type__c);
            }
        }
        return subtypeSet;
    }

    /**
     * build filter for query
     * @param filters
     * @return String 
     */
    public static String buildFilters(List < CC_KB_Content_Search_Filter_Config__mdt > filters) {

        Boolean isAnd = false;
        String filter = '';
        for (CC_KB_Content_Search_Filter_Config__mdt sfilter: filters) {
            if (sfilter.filter_Type__c == 'Auto') {
                if (isAnd) {
                    filter += ' AND ';
                } else {
                    isAND = true;
                }
                filter += ' ' + sfilter.Matching_Field__c + getOperator(sfilter.Operator__c) + getValue(sfilter.data_Type__c, sfilter.filter_Values__c, sfilter.Filter_Customer_Profile_Field__c);
            }
        }
        return filter;
    }
    /**
     * get operator for filter 
     * @param optStr
     * @return String 
     */
    private static String getOperator(String optStr) { // TBD - will add more later
        if (optStr == CC_KBContent_Constants.EQUALS) {
            return '=';
        } else if (optStr == CC_KBContent_Constants.NOT_EQUALS) {
            return '!=';
        } else if (optStr == CC_KBContent_Constants.CONTANS) {
            return ' like ';
        } else if (optStr == CC_KBContent_Constants.NOT_CONTAINS) {
            return 'not like';
        }
        return '=';
    }
    /**
     * get Value for filter
     * @param dataType, value
     * @return String 
     */
    private static String getValue(String dataType, String value, String profileField) { // TBD - will add more later
        if (profileField != null) {
            if (profileField == 'LanguageLocaleKey') value = UserInfo.getLanguage();
        }
        if (dataType == 'String') {
            return '\'' + value + '\'';
        } else {
            return value;
        }

    }
    /**
     * String to Set
     * @param value, defaultValue
     * @return String 
     */
    public static Set < String > StringToSet(String value, String defaultValue) {
        if (value == null) value = defaultValue;
        Set < String > valueSet = new Set < String > ();
        valueSet.addAll(value.split(','));
        return valueSet;
    }
}