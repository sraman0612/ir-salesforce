@isTest
public class coopClaimTriggerHandlerTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }

    static testmethod void UnitTest_coopClaimTriggerHandler()
    {
       setupData();
        Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        Map<String,id> profMap = new Map<String,id>();
        list<Profile> profid = [SELECT id, Name FROM Profile];
        for(Profile p:profid){
            profMap.put(p.Name,p.id);
        }
        
     
        User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profMap.get('CC_System Administrator'),Alias = 'test1236',
                            Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                            TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst');
        insert testUser;
        test.startTest();
        System.runAs (testUser) {
         // creating test data
        Contract cnt =new Contract();
        cnt.AccountId=sampleAccount.Id;
        cnt.Name='Sample Contract';
        cnt.CC_type__c='Dealer/Distributor Agreement';
        cnt.CC_Sub_Type__c='Commercial Utility';
        insert cnt;
        CC_Co_Op_Account_Summary__c ccas =new CC_Co_Op_Account_Summary__c();
        ccas.Contract__c=cnt.Id;
        insert ccas;
        CC_Pavilion_Co_Op_Product_Mapping__c ccpm =new CC_Pavilion_Co_Op_Product_Mapping__c();
        ccpm.CC_Account_Region__c='United States';
        ccpm.Marketing_Agreement__c='Golf Utility';
        ccpm.Coop_Type__c='Advertising';
        ccpm.Charge_Account__c='UTILITY COOP';
        ccpm.Name='Pavilion Co-Op Product mapping';
        insert ccpm;
        Id coopAdRecordTypeId = [select id from RecordType where Name='Advertising' limit 1].id;
        CC_Coop_Claims__c cc =new CC_Coop_Claims__c();
        cc.RecordTypeId=coopAdRecordTypeId;
        cc.CC_Status__c='Approved';
        cc.Co_Op_Account_Summary__c=ccas.Id;
        insert cc;
        CC_Coop_Claims__c newcc =new CC_Coop_Claims__c();
        newcc.CC_Status__c='Approved';
        newcc.Co_Op_Account_Summary__c=ccas.Id;
        newcc.RecordTypeId=coopAdRecordTypeId;
        insert newcc;
        cc.CC_Submit_Date__c=System.today();
        newcc.CC_Submit_Date__c=System.today();
        update cc;
        update newcc;
        Product2 p1 =new Product2();
        p1.Name='first product';
        p1.ProductCode='UTILITY COOP';
        insert p1;
        Product2 p2 =new Product2();
        p2.Name='second product';
        p2.ProductCode='COOP SE ASIA';
        insert p2;
        List<CC_Coop_Claims__c> CoopClaimsList =new List<CC_Coop_Claims__c>();
        List<CC_Coop_Claims__c> newCoopClaimsList =new List<CC_Coop_Claims__c>();
        CoopClaimsList.add(cc);
        newCoopClaimsList.add(newcc);
        delete newcc;
        undelete newcc;
        
        }
    }
    
    
    static testmethod void UnitTest_coopClaimTriggerHandler_US(){
       setupData();
        Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        Map<String,id> profMap = new Map<String,id>();
        list<Profile> profid = [SELECT id, Name FROM Profile];
        for(Profile p:profid){
            profMap.put(p.Name,p.id);
        }
        
     
        User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profMap.get('CC_System Administrator'),Alias = 'test1236',
                            Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                            TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst');
        insert testUser;
        test.startTest();
        System.runAs (testUser) {
         // creating test data
        Contract cnt =new Contract();
        Contract cnt1 =new Contract();
        //Isheman's Team
        cnt.AccountId=sampleAccount.Id;
        cnt.Name='Sample Contract';
        cnt.CC_type__c='Dealer/Distributor Agreement';
        cnt.CC_Sub_Type__c='Commercial Utility';
        insert cnt;
        //Alison's Team
        cnt1.AccountId=sampleAccount.Id;
        cnt1.Name='Sample Contract';
        cnt1.CC_type__c='Dealer/Distributor Agreement';
        cnt1.CC_Sub_Type__c='LSV Transportation';
        insert cnt1;
        CC_Co_Op_Account_Summary__c ccas =new CC_Co_Op_Account_Summary__c();
        ccas.Contract__c=cnt.Id;
        insert ccas;
        CC_Co_Op_Account_Summary__c ccas1 =new CC_Co_Op_Account_Summary__c();
        ccas1.Contract__c=cnt1.Id;
        insert ccas1;
        Id coopAdRecordTypeId = [select id from RecordType where Name='Advertising' limit 1].id;
        CC_Coop_Claims__c cc =new CC_Coop_Claims__c();
        cc.RecordTypeId=coopAdRecordTypeId;
        cc.CC_Status__c='Submitted';
        cc.Co_Op_Account_Summary__c=ccas.Id;
        insert cc;
        CC_Coop_Claims__c cc1 =new CC_Coop_Claims__c();
        cc1.RecordTypeId=coopAdRecordTypeId;
        cc1.CC_Status__c='Submitted';
        cc1.Co_Op_Account_Summary__c=ccas1.Id;
        insert cc1;
        }
    }
}