/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 */
public class CC_PavilionSysProcessTrainingController {

 public String dates {
  get;
  set;
 }
 public String times {
  get;
  set;
 }
 public String contents {
  get;
  set;
 }
 public List<CC_Pavilion_Content__c> ListEveryWednesday{get;set;}
 public Set<String> TrainingEventTypeList{get;set;}
 public static CC_Pavilion_Content__c customerViewHeadingContent{get;set;}
 public static CC_Pavilion_Content__c customerViewBodyContent{get;set;}
  
 public CC_PavilionSysProcessTrainingController(CC_PavilionTemplateController controller) {
  customerViewHeadingContent =[select CC_Body_1__c  from CC_Pavilion_Content__c where  Training_Event_Type__c='Every Wednesday' and RecordType.DeveloperName='CC_Process_System_Training'];
  customerViewBodyContent = [select CC_Body_1__c  from CC_Pavilion_Content__c where  Training_Event_Type__c='Every Third Thursday' and RecordType.DeveloperName='CC_Process_System_Training'];   
  }
 public PageReference redirectToDiffPage() {
  Integer currentYear = System.Today().year();
  String calculatedDate = String.valueOf(currentyear) + '-' + dates;
  PageReference newPage = new PageReference('/apex/CC_PavillionCvTrainingReq?date=' + calculatedDate + '&time=' + times + '&contents=' + contents);
  return newPage;
 }
 @RemoteAction
 public Static Set<String> getEventType(){
     Set<String> getEvent=new Set<String>();
     for(CC_Pavilion_Content__c pc:[select id,Training_Event_Type__c,Training_Month_Value_In_Number__c,Training_Month__c,Training_Time__c from CC_Pavilion_Content__c where  RecordType.DeveloperName='CC_Process_System_Training']){
         getEvent.add(pc.Training_Event_Type__c);
     }
     return getEvent;
 }
 
 /*public Class WrapperTrainingContentList{
     Public String TraningContent{get;set;}
     Public String TraningTime{get;set;}
     Public String TraningMonthDatesJan{get;set;}
     Public String TraningMonthDatesFeb{get;set;}
     Public String TraningMonthDatesMar{get;set;}
     Public String TraningMonthDatesApr{get;set;}
     Public String TraningMonthDatesMay{get;set;}
     Public String TraningMonthDatesJun{get;set;}
     Public String TraningMonthDatesJul{get;set;}
     Public String TraningMonthDatesAug{get;set;}
     Public String TraningMonthDatesSep{get;set;}
     Public String TraningMonthDatesOct{get;set;}
     Public String TraningMonthDatesNov{get;set;}
     Public String TraningMonthDatesDec{get;set;}
     
     
     public WrapperTrainingContentList(String Content, String TimeTraining,String Jan,String Feb,String Mar,String Apr){
         TraningContent =
     }
 }
 public */
 @RemoteAction
 public static List<String> getEveryWednesdayData(String EventType){
     Integer Month = Date.Today().Month();
     CC_Pavilion_Content__c pc=[select id,Training_Event_Type__c,Training_Month_Value_In_Number__c,Training_Month__c,Training_Time__c from CC_Pavilion_Content__c where  RecordType.DeveloperName='CC_Process_System_Training' and Training_Event_Type__c=:EventType];
     Attachment att=[select id,name,body from Attachment where parentID=:pc.id];
     Blob bodyBlob = att.Body;

     String bodyStr = bodyBlob.toString();

     List<String> parts= new List<String>();

     parts = bodyStr.split('\n');
     //String bodyContent=att.body.toString();
     system.debug(parts.size());
     
    return parts;
     
 }
     public String getEveryThursdayData(){
       return null;  
     }
    public static string getCustomerVIEWHeading()
    {
      
      String HeadingText = customerViewHeadingContent.CC_Body_1__c ;
     return HeadingText.escapeEcmaScript();
    }
    
    
    public static string getCustomerVIEWBody()
    {
    
    String BodyText = customerViewBodyContent.CC_Body_1__c;
    return BodyText.escapeEcmaScript();
    }
    
 /* public static List < CC_Pavilion_Content__c> getContentList() {
   Id  usrId  = userinfo.getUserId();
   Id accId = [SELECT AccountId from User  where id =:usrId].AccountId;  
   Account acc = [select Type,CC_Global_Region__c from Account where id=:accId];
   List < CC_Pavilion_Content__c> contentList = new List < CC_Pavilion_Content__c> ();
   contentList = [select id, Title__c, CC_Region__c, Link__c, RecordType.Name from CC_Pavilion_Content__c where RecordType.Name = 'Home Page Text' and Title__c = 'CCInsight'
                            and CC_Type__c INCLUDES(:acc.Type) and CC_Region__c INCLUDES(:acc.CC_Global_Region__c)];
  return contentList;
} */
}