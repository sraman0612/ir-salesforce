/* Description: Controller used to for hole in one claim page. The page dynamically builds updon the no of days vehicle will be offered field.
                The page will be accessed from HIOClaimSummary pge.
 * Created Date: 31-08-2-16
 * Author: Srini (srinivas.yarramsetti@tcs.com/irco.com)
 * Modified History:
 *
 *
 */


global class CC_PavilionHOIClaimController{
    
    Global id holeClaimId{get;set;}
    
    public CC_Hole_in_One__c holeInOne {get;set;}
    public String UserAccountId {get;set;}
    public Date lstDateOfFirstTrnmt{get;set;}
    public String trntStartDate{get;set;}
    public String lstNameOfTrnmt{get;set;}
    public String lstCourseName{get;set;}
    public String lstVehicle{get;set;}
    public String lstCourseAdd{get;set;}
    public List<CC_Hole_in_One__c> listHIO{get;set;}
    public String alreadyClaim{get;set;}
    
    public static String holeinClaim{get;set;}
    public static String claimJSON{get;set;}
    public static Attachment billAttachment {
    get {
          if (billAttachment == null)
            billAttachment = new Attachment();
          return billAttachment;
       }
     set;
    }
    public static string PageMessage{get;set;}
      
    //Contorller method
    public CC_PavilionHOIClaimController(CC_PavilionTemplateController controller){
        
        holeClaimId = ApexPages.currentPage().getParameters().get('id');
                             
        lstDateOfFirstTrnmt =   [SELECT Date_of_the_first_day_of_Tournament__c from CC_Hole_in_One__c
                                 WHERE id =:holeClaimId].Date_of_the_first_day_of_Tournament__c;  
        trntStartDate = lstDateOfFirstTrnmt.format();
        //For Future Use if the requirement is to submit only one claim
        /*list<Hole_In_One_Claim__c> existingClaim = new list<Hole_In_One_Claim__c>();
        existingClaim = [SELECT id, Name, Status__c, Coop_Request__c FROM Hole_In_One_Claim__c WHERE Coop_Request__c =:holeClaimId ];
        if(existingClaim.size()>0){
            alreadyClaim = 'You have claim #';
            for(Hole_In_One_Claim__c hoi:existingClaim){
                alreadyClaim += hoi.name+'with Status:'+hoi.Status__c;
            }
                alreadyClaim += 'You can\'\t submit anymore claims';
        }*/
     }
     
     
     @RemoteAction
     public static String listofHOIRecords(String hoiidString){
        id hoiid = id.valueOf(hoiidString);
        List<CC_Hole_in_One__c> lstCoopClaim = new List<CC_Hole_in_One__c>();
        lstCoopClaim =[SELECT Id, Name, Date_of_the_first_day_of_Tournament__c, CC_Status__c, Complete_Name_of_Tournament__c, 
                           Course_Name__c,Course_Address__c,City__c, State_Province__c,Country__c, Vehicle__c,Players__c,Pro_AM__c,round__c,
                           Day__c,Par__c,Yards__c, Hole__c,Player1__c,Player2__c,Player3__c,Pro_AM2__c,Pro_AM1__c,Pro_AM3__c,round1__c,round2__c,round3__c,
                           Day1__c,Day2__c,Day3__c, Par1__c, Par2__c,Par3__c, Yards1__c, Yards2__c,Yards3__c, Hole1__c,Hole2__c,Hole3__c 
                           from CC_Hole_in_One__c where id=:hoiid];
        if(!lstCoopClaim.isEmpty())
        {
            String jsonData = (String)JSON.serialize(lstCoopClaim);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
     }
     
   
    /* Remote Method to save the record to Hole_In_One_Claim__c Object.
     * Method return Tyoe: String
     *
     */
    public static PageReference submitHIOClaim(){
        list<Hole_In_One_Claim__c> listHoleInClaim = new list<Hole_In_One_Claim__c>();
        String parentName = [SELECT Name FROM CC_Hole_in_One__c WHERE id=:holeinClaim].Name;
        system.debug(parentName);
        try{
        if (String.isNotEmpty(claimJSON) || Test.isRunningTest()){
            CoopClaimParser parserData = new CoopClaimParser();
            if(Test.isRunningTest()){
                CoopClaimItems ccItem = new CoopClaimItems();
                ccItem.winnerName = 'test'; 
                ccItem.firstMemName = 'test';
                ccItem.SecondMemName = 'test';
                ccItem.ThirdMemName = 'test' ;
                ccItem.OfficialName = 'test'; 
                ccItem.tourndate = '05/05/1998';
                ccItem.vehSerialNum = 'test'; 
                ccItem.clubCarInvNum = 'test';
                ccItem.amtOnInv = '34'; 
                ccItem.comments = 'test'; 
                ccItem.winnerPhNum = 'test'; 
                ccItem.firstMemPhNum = 'test';
                ccItem.secondMemPhNum = 'test'; 
                ccItem.thirdMemPhNum = 'test'; 
                ccItem.officialPhNum = 'test';
                ccItem.conditionsApply = 'test';
                
                List<CoopClaimItems> ccItems = new List<CoopClaimItems>();
                ccItems.add(ccItem);
                
                parserData.CoopClaimItem = ccItems;
            }
            else{
             parserData = (CoopClaimParser) JSON.deserializeStrict(claimJSON, CoopClaimParser.class);
            }
                for(CoopClaimItems ccItems: parserData.CoopClaimItem) {
                Hole_In_One_Claim__c hinc = new Hole_In_One_Claim__c();
                hinc.Name = parentName;
                hinc.Winner_Name__c = ccItems.winnerName;
                hinc.First_Member_Name__c = ccItems.firstMemName;
                hinc.Second_Member_Name__c = ccItems.SecondMemName;
                hinc.Third_Member_Name__c = ccItems.ThirdMemName;
                hinc.Official_s_Name__c = ccItems.OfficialName;
                String[] myDateOnly = ccItems.tourndate.split(' ');
                String[] strDate = myDateOnly[0].split('/');
                Integer myIntDate = integer.valueOf(strDate[1]);
                Integer myIntMonth = integer.valueOf(strDate[0]);
                Integer myIntYear = integer.valueOf(strDate[2]);
                hinc.Date_of_Hole_In_One__c = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                hinc.Vehicle_Serial_Number__c = ccItems.vehSerialNum;
                hinc.Club_Car_Invoice_Number__c  = ccItems.clubCarInvNum;
                hinc.Amount_of_Vehicle_on_Invoice__c = Integer.valueOf(ccItems.amtOnInv);
                hinc.Comments__c = ccItems.comments;
                hinc.Winner_Phone_Number__c = ccItems.winnerPhNum;
                hinc.First_Member_Phone_Number__c = ccItems.firstMemPhNum;
                hinc.Second_Member_Phone_Number__c = ccItems.secondMemPhNum;
                hinc.Third_Member_Phone_Number__c = ccItems.thirdMemPhNum;
                hinc.Official_s_Phone_Number__c = ccItems.officialPhNum;
                hinc.Coop_Request__c = holeinClaim;
                hinc.Status__c = 'Submitted';
               // System.debug('@@@holeClaimId@@@'+holeClaimId);
                listHoleInClaim.add(hinc);
            }
        }
        
        if(listHoleInClaim.size()>0){
           insert listHoleInClaim;
           
          List <Attachment> lstAttachment = new List <Attachment>();
           if(String.isNotBlank(billAttachment.Name)) {
           Attachment objBillAttachement = new Attachment();
           objBillAttachement = billAttachment;
          // objBillAttachement.body = EncodingUtil.base64Decode(billAttachmentBody);
           //objBillAttachement.name = billAttachmentName;
           objBillAttachement.parentId = listHoleInClaim[0].Id;
           lstAttachment.add(objBillAttachement);
           }
           
           if (!lstAttachment.isEmpty()) {
            insert lstAttachment;
                }
            
            }
           
            PageMessage = 'Your Hole in One claim #'+parentName+' has been submitted for review and approval.';
            system.debug('%%%'+listHoleInClaim[0].Name);
            //return PageMessage;
        }
        catch(Exception e){
            system.debug(e.getStackTraceString());
            PageMessage = 'Failure';
            //return 'Failure';
        }
        
        PageReference pr = Page.CC_PavilionCoopHOIClaimRequests;
        pr.setRedirect(true);
        return pr;
    }
    

    //Wapper class for JSON String.
     public class CoopClaimParser {
      public List <CoopClaimItems> CoopClaimItem;
     }
     public class CoopClaimItems {
        String winnerName;
        String firstMemName;
        String SecondMemName;
        String ThirdMemName;
        String OfficialName;
        String tourndate;
        String vehSerialNum;
        String clubCarInvNum;
        String amtOnInv;
        String comments;
        String winnerPhNum;
        String firstMemPhNum;
        String secondMemPhNum;
        String thirdMemPhNum;
        String officialPhNum;
        String conditionsApply;
     }
        
}