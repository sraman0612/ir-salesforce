@isTest
private class CC_FieldHistoryLogger_test {
    
    static testMethod void testLogger() {
        Map<Id, Account> before = new Map<Id, Account>();
        before.put('001j0000019a4ya', new Account(Id = '001j0000019a4ya', 
                                                  Name = 'SameName', 
                                                  CC_GPSI_Bev_VDUs__c = 1, 
                                                  CC_GPSI_Advertisement_Start_Date__c = Date.today()));
        Map<id, Account> after = new Map<Id, Account>();
        after.put('001j0000019a4ya', new Account(Id = '001j0000019a4ya', 
                                                 Name = 'sameName', 
                                                 CC_GPSI_Bev_VDUs__c = null));           
        CC_FieldHistoryLogger logger = new CC_FieldHistoryLogger(SObjectType.Account.FieldSets.CC_AccountFieldHistory);
        logger.LogHistory(before, after);     
    }

}