@isTest
public with sharing class CC_Approval_Chatter_ControllerTest {

    @TestSetup
    private static void createData(){

    	TestDataUtility dataUtility = new TestDataUtility();
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;
    	
    	// Create users to use in the sales hiearchy
    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'xyz123';

    	User salesRepUser = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser.Alias = 'alias122';
    	
    	User salesRepMgr = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr.Alias = 'alias123';
    	salesRepMgr.PS_Purchasing_LOA__c = 50000;
    	
    	User salesRepDir = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir.Alias = 'alias124';
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    

    	insert new User[]{salesRepUser, salesRepMgr, salesRepDir, salesRepVP, adminUser};
    	
    	salesRepUser.ManagerId = salesRepMgr.Id;
    	salesRepMgr.ManagerId = salesRepDir.Id;
    	salesRepDir.ManagerId = salesRepVP.Id;
    	
    	update new User[]{salesRepUser, salesRepMgr, salesRepDir};    	
    	
    	User financeUser;
    	
    	// Avoid mixed DML since role will be assigned to the finance user
    	system.runAs(adminUser){	
    	
	        financeUser = TestUtilityClass.createUser('CC_General Admin', null);
	        financeUser.LastName = 'FinanceTestUser';    	
	        financeUser.UserRoleId = [Select Id From UserRole Where DeveloperName = 'CC_Finance_Team' LIMIT 1].Id;

	    	insert financeUser;    	
    	}
    	
    	// Create a sales rep that has a manager, director, and VP in the hierarchy
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser.Id);
    	insert salesRep;

        // Create Customer Accounts 
        Account acct1 = TestUtilityClass.createAccount2();
        acct1.BillingCountry = 'USA';
        acct1.ShippingCountry = 'USA';
        insert acct1;
        
        // Create Master Lease 
        CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
        
        // Assign new Account to Master Lease
        ml1.Customer__c = acct1.Id;
        insert ml1;
        
        CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
        
        for (Integer i = 0; i < 5; i++){
            
            CC_Short_Term_Lease__c lease = TestDataUtility.createShortTermLease();
            lease.Master_Lease__c = ml1.Id;
            lease.Sales_Rep__c = salesRep.Id;
            lease.Sales_Rep_User__c = salesRepUser.Id;                
            leases.add(lease);
        } 	
        
        insert leases;   

        // Submit the leases for approval
        Approval.ProcessSubmitRequest[] approvalRequests = new Approval.ProcessSubmitRequest[]{};

        for (CC_Short_Term_Lease__c lease : leases){

            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
            approvalRequest.setSkipEntryCriteria(true);
            approvalRequest.setObjectId(lease.Id);
            approvalRequest.setProcessDefinitionNameOrId('Revenue_Lease_Approval');
            approvalRequests.add(approvalRequest);                           
        }

        Approval.ProcessResult[] approvalResults = sObjectService.processApprovalRequestsAndLogErrors(approvalRequests, 'CC_Manage_Approvals_ControllerTest', 'createData');
        system.debug('approvalResults: ' + approvalResults);

        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }    

    @isTest
    private static void testGetRecordInApprovalSuccess(){

        User financeUser = [Select Id From User Where LastName = 'FinanceTestUser' and isActive = true Order By CreatedDate DESC LIMIT 1];

        Test.startTest();

            System.runAs(financeUser){

                ProcessInstanceWorkitem piw = [Select Id, ProcessInstance.TargetObjectId From ProcessInstanceWorkitem LIMIT 1];
                CC_Approval_Chatter_Controller.GetRecordInApprovalResponse response = CC_Approval_Chatter_Controller.getRecordInApproval(piw.Id);

                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(piw.ProcessInstance.TargetObjectId, response.recordInApprovalId);
            }

        Test.stopTest();
    }

    @isTest
    private static void testGetRecordInApprovalFail(){

        User financeUser = [Select Id From User Where LastName = 'FinanceTestUser' and isActive = true Order By CreatedDate DESC LIMIT 1];

        Test.startTest();

            System.runAs(financeUser){

                CC_Approval_Chatter_Controller.GetRecordInApprovalResponse response = CC_Approval_Chatter_Controller.getRecordInApproval(null);

                system.assertEquals(false, response.success);
                system.assert(String.isNotBlank(response.errorMessage));
                system.assertEquals(null, response.recordInApprovalId);
            }

        Test.stopTest();
    }    
}