/**
	@author Providity
	@date 15FEB19
	@description controller for PicklistSelect aura component
*/

public class PicklistSelectController {

    /**
     * Given an API object and field name, returns the configured field label to display to users.
     */    
    @AuraEnabled
    public static String getFieldLabel( String objectName, String fieldName ) {        
        String label = Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap().get( fieldName ).getDescribe().getLabel();        
        return label;
    }
    
    /**
     * Given an API object and field name, returns list of the picklist values for use in select input.
     */
    @AuraEnabled
    public static List<Object> getPicklistOptions(String objectName, String fieldName, String recordTypes, String pickValsToReturn) {
        
        CTS_Community_Search_Settings__c settings = CTS_Community_Search_Settings__c.getInstance();
        if(recordTypes == null && objectName == 'Case') {
            recordTypes = settings.CTS_TD_Case_Record_Types__c;
        }
        System.debug('recordTypes**'+recordTypes);
        List<PicklistOption> options = new List<PicklistOption>();
        List<Object> result = new List<Object>();
        Set<String> pickVals = new Set<String>();
        Set<String> pickValsSet = String.isBlank(pickValsToReturn) ? new Set<String>() : new Set<String>(pickValsToReturn.split(','));
        if(recordTypes == null) {
        	for( PicklistEntry entry : Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap().get( fieldName ).getDescribe().getPicklistValues() ) {
                if( pickValsSet.isEmpty() || ( !pickValsSet.isEmpty() && pickValsSet.contains(entry.getValue()) ) ) {
                    options.add( new PicklistOption( entry.getLabel(), entry.getValue() ) );
                }
            }
            result = options;
        } else {            
            String sessionId = Test.isRunningTest() ? UserInfo.getSessionId() : PicklistSelectController.getSessionIdFromVFPage();
            
            Map<String,Schema.RecordTypeInfo> rtByName = Schema.getGlobalDescribe().get( objectName ).getDescribe().getRecordTypeInfosByName();            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
			String ep = CTS_Article_Search_Config__c.getInstance().Salesforce_Endpoint__c;
            ep += '/services/data/v44.0/ui-api/object-info/' + objectName + '/picklist-values/{rtId}/' + fieldName;
            for(String rtName: recordTypes.trim().split(',')) {
                if(rtByName.containsKey(rtName.trim())) {
                    ep = ep.replace('{rtId}', rtByName.get(rtName.trim()).getRecordTypeId());
                    req.setEndpoint(ep);                    
                }
                req.setHeader('Authorization', 'OAuth ' + sessionId);
                req.setHeader('Authorization', 'Bearer ' + sessionId);
                req.setMethod('GET');                
                try {
                    if(Test.isRunningTest()) result.add(JSON.serialize('{"label":"test", "value":"test"}'));
                    else {
                    	res = h.send(req);
                        Map<String,Object> meta = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
                        List<Object> values = (List<Object>) meta.get('values');
                        result.addAll(values); 
                        System.debug('values**' + values);
                    }                                       
                } catch(Exception e) {
                    System.debug('PicklistSelectController Exception**' + e.getMessage());
                }          
            }            
        }
        System.debug('result**' + result);
        
        return result;
    }        
    
    public static String getSessionIdFromVFPage(){
        String content = Page.CTS_UIAPI_SessionId.getContent().toString();
        Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
        return content.substring(s, e);
    }
    
    /**
     * The system class PicklistEntry is not aura enabled so cannot be returned from @AuraEnabled method.
     * Workaround is to define our own class with aura enabled properties.
     */
    public class PicklistOption {
        @AuraEnabled public String label { get; set; }        
        @AuraEnabled public String value { get; set; }
        
        public PicklistOption( String label, String value ) {
            this.label = label;
            this.value = value;
        }
    }
}