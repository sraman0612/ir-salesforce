@isTest
public class OppySalesCallPlan_TriggerHandler_Test {
    // Account Record Types
    public static final String RS_HVAC_RT_DEV_NAME = 'CTS_MEIA';
    public static final String AirNAAcct_RT_DEV_NAME = 'CTS_MEIA';

    // Opportunity Record Types
    public static final String RS_New_Business_RT_DEV_NAVE = 'CTS_MEIA';
    public static final String CTS_Global_New_RT_DEV_NAME = 'CTS_MEIA';
    
    // Opportunity Sales Call Plan Record Types
    public static final String CTS_Sales_Call_Plan = 'CTS_Sales_Call_Plan';
    
    @testSetup static void setup(){
        Opportunity_Sales_Call_Plan__c salesCallPlan;
        List<Opportunity_Sales_Call_Plan__c> salesCallPlans = new List<Opportunity_Sales_Call_Plan__c>();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
    }
    
    @isTest static void salesCallTest(){
        Opportunity_Sales_Call_Plan__c salesCallPlan;
        List<Opportunity_Sales_Call_Plan__c> salesCallPlans = new List<Opportunity_Sales_Call_Plan__c>();
        Object objectMessage;

        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt.1'];
        
        System.runAs(u){
            
            Account airdTestAccount =IRSPXTestDataFactory.createAccount('TestAcc1',AirNAAcct_RT_DEV_NAME);
            airdTestAccount.ownerid=u.id;
            insert airdTestAccount;
            
            for(RecordType rt: [Select Id, DeveloperName from RecordType where SobjectType='Opportunity_Sales_Call_Plan__c' and DeveloperName = :CTS_Sales_Call_Plan ]){
                for (Integer i = 0; i < 10; i++){
                    salesCallPlan = IRSPXTestDataFactory.createSalesCallPlan(airdTestAccount.id, rt.DeveloperName);
                    salesCallPlans.add(salesCallPlan);
                }
            }  
            
            Database.SaveResult[] srList = Database.insert(salesCallPlans,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Sales call plan fields that affected this error: ' + err.getFields());
                    }
                }
            }              

            
            
            salesCallPlans = [select id, name, Account__r.name, Percentage_of_Fields_Populated__c, RecordTypeId, Notes_From_Last_Meeting__c from Opportunity_Sales_Call_Plan__c];
            
            for(Opportunity_Sales_Call_Plan__c scp: salesCallPlans){
                system.debug(scp);
                system.assert(scp.Percentage_of_Fields_Populated__c > 0,objectMessage);  
                scp.Notes_From_Last_Meeting__c = 'test note';
            }  
            
            srList = Database.update(salesCallPlans,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Sales call plan fields that affected this error: ' + err.getFields());
                    }
                }
            }              
            
        }
        
         for(Opportunity_Sales_Call_Plan__c scp: salesCallPlans){
                system.debug(scp);
                system.assert(scp.Percentage_of_Fields_Populated__c > 0,objectMessage);  
            }
        
        Test.Stoptest();
    }
}