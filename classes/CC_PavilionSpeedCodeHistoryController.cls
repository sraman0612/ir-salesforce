Global class CC_PavilionSpeedCodeHistoryController {
  
  public CC_PavilionSpeedCodeHistoryController(CC_PavilionTemplateController controller) {}
  
  @RemoteAction
  public static schWrapper[] getspeedcodehistory(String aId){
    schWrapper[] lst = new schWrapper[]{};
    Map <String, String> invMap = new Map<String,String>();
    for(CC_Invoice2__c i : [SELECT Id,Name,CC_Order_Number__c FROM CC_Invoice2__c WHERE CC_Customer_Number__c = :aId AND CC_Item_Number__c='PASSCODE']){
      invMap.put(i.CC_Order_Number__c,i.Name);
    }
    
    //code contributed by @Priyanka Baviskar for issue no 7300015.
    for (CC_SpeedCodeHistory__c sch : [SELECT CC_Order_Number__r.Name,CC_Order_Number__r.CC_MAPICS_Order_Key__c,CC_Order_Number__r.Order_Number__c,CC_Order_Date__c,CC_Customer_Name__c,
                                              CC_Purchase_Order_Number__c,Name,CC_Controller_Serial_Numbr__c,CC_Account__r.CC_Customer_Number__c,CC_Order_Number__r.CC_Order_Number_Reference__c
                                         FROM CC_SpeedCodeHistory__c
                                        WHERE CC_Account__c =:aId  ORDER BY CC_Order_Date__c desc]){
      schWrapper schw = new schWrapper(sch);
      if(invMap.containsKey(sch.CC_Order_Number__r.Order_Number__c)){
        schw.ordNum='<a href="' + Site.getPathPrefix() + '/CC_PavilionInvoicedetail?invId=' + invMap.get(sch.CC_Order_Number__r.Order_Number__c) + 
                    '&pagetype=2" Class="btn" target="_blank" style="text-decoration:none" ><u>' + sch.CC_Order_Number__r.Order_Number__c + '</u></a>';
      }
      lst.add(schw);
    }                
    return lst;
  }
  
  public class schWrapper{
    public String ordNum {get;set;}
    public String orddate {get;set;}
    public String customer {get;set;}
    public String poNum {get;set;}
    public String vehicleNum {get;set;}
    public String controllerSerialNum {get;set;}
    public String code {get;set;}
    public schWrapper(){}
    public schWrapper(CC_SpeedCodeHistory__c sch){
      ordNum=null==sch.CC_Order_Number__r.CC_MAPICS_Order_Key__c?sch.CC_Order_Number__r.Name:sch.CC_Order_Number__r.Order_Number__c;
      ordDate=sch.CC_Order_Date__c.month() + '/' + sch.CC_Order_Date__c.day() + '/' + sch.CC_Order_Date__c.year();
      customer=sch.CC_Customer_Name__c;
      poNum=sch.CC_Purchase_Order_Number__c;
      vehicleNum=sch.Name;
      controllerSerialNum=sch.CC_Controller_Serial_Numbr__c;
      code='<a href="' + Site.getPathPrefix() + '/CC_PavilionSpeedCodeDetail?spcid=' + sch.id + '" Class="btn" target="_blank" style="text-decoration:none" ><u>View Speed Codes</u></a>';
    }
  }
}