public without sharing class NotificationQueuingService {

    private static CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
    private static Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
    private static Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
    private static Map<String, Map<String, DescribeFieldResult>> objectFieldDescribeMap = new Map<String, Map<String, DescribeFieldResult>>();
    private static Map<Id, Notification_Type__c[]> allTriggeredEventsMap;
    private static Map<Id, Set<Id>> subscriberEventsProcessedMap;
    private static Map<Id, Notification_Type__c> notificationTypeMap = new Map<Id, Notification_Type__c>();

    // Static constructor
    static{

        // Add all triggered objects defined on notification types to this map
        for (Notification_Type__c notificationType : NotificationService.getNotificationTypes()){

            notificationTypeMap.put(notificationType.Id, notificationType);

            Schema.SObjectType triggeringObjType = globalDescribe.get(notificationType.Triggering_Object__c);

            if (triggeringObjType != null){

                if (!objectFieldDescribeMap.containsKey(notificationType.Triggering_Object__c)){
                    objectFieldDescribeMap.put(notificationType.Triggering_Object__c, sObjectService.getObjectFieldDescribeMap(triggeringObjType));
                }
            }
        }
    }    

    public static void processNotificationSubscriptions(Map<Id, sObject> events, Map<Id, sObject> oldEvents){

        system.debug('processNotificationSubscriptions');

        try{

            allTriggeredEventsMap = new Map<Id, Notification_Type__c[]>();

            Set<Id> bypassUserIds = new Set<Id>();

            if (notificationSettings != null && String.isNotBlank(notificationSettings.Notification_Queuing_Bypass_Users__c)){

                for (String bypassUserId : notificationSettings.Notification_Queuing_Bypass_Users__c.split(',')){
                    bypassUserIds.add(bypassUserId.trim());
                }
            }

            Set<String> eventObjects = new Set<String>();
            Map<String, Set<String>> eventObjectGrandParentFieldsToQuery = new Map<String, Set<String>>();

            // Process each event and detect any qualifying notification events 
            for (sObject event : events.values()){

                system.debug('processing event: ' + event);

                if (bypassUserIds.contains((Id)event.get('LastModifiedById'))){
                    system.debug('skipping record, last modified by a bypass user');
                    continue;
                }

                String eventObject = String.valueOf(event.getSObjectType());
                system.debug('eventObject: ' + eventObject);                

                sObject oldEvent = oldEvents != null ? oldEvents.get(event.Id) : null;                

                // Go through each notification type and find the ones for the correct triggering object and triggering field value
                for (Notification_Type__c notificationType : NotificationService.getNotificationTypes()){

                    system.debug('checking for triggering match on notification type: ' + notificationType.Name);

                    if (notificationType.Active__c){

                        if (eventObject == notificationType.Triggering_Object__c){

                            Object eventFieldValueObj = event.get(notificationType.Triggering_Field__c);
                            String eventFieldValue = eventFieldValueObj != null ? String.valueOf(eventFieldValueObj).toLowerCase() : null;
                            Object oldEventFieldValueObj = oldEvent != null ? oldEvent.get(notificationType.Triggering_Field__c) : null;
                            String oldEventFieldValue = oldEventFieldValueObj != null ? String.valueOf(oldEventFieldValueObj).toLowerCase() : null;
                            String triggeringFieldValue = notificationType.Triggering_Field_Value__c.toLowerCase();

                            system.debug('oldEventFieldValue: ' + oldEventFieldValue);
                            system.debug('eventFieldValue: ' + eventFieldValue);
                            system.debug('triggering field to compare: ' + notificationType.Triggering_Field__c);
                            system.debug('notification type triggering field filter value: ' + triggeringFieldValue);
                
                            // Check for a field value match on new records, on existing records look for a match only when the field is changing
                            if (eventFieldValue == triggeringFieldValue && (oldEvent == null || (oldEventFieldValue != eventFieldValue))){

                                if (String.isNotBlank(notificationType.Triggering_Object_Parent_Field__c) && String.isNotBlank(notificationType.Parent_Object_Grandparent_Field__c)){

                                    String granparentFieldToQuery = notificationType.Triggering_Object_Parent_Field__c.replace('__c', '__r') + '.' + notificationType.Parent_Object_Grandparent_Field__c;
                                    system.debug('adding granparentFieldToQuery: ' + granparentFieldToQuery);

                                    if (eventObjectGrandParentFieldsToQuery.containsKey(eventObject)){
                                        eventObjectGrandParentFieldsToQuery.get(eventObject).add(granparentFieldToQuery);
                                    }
                                    else{
                                        eventObjectGrandParentFieldsToQuery.put(eventObject, new Set<String>{granparentFieldToQuery});
                                    }                                
                                }                                

                                system.debug('--event triggered on triggering field value match..');
                                updateTriggeredEventsMap(notificationType, event);
                            }
                            else{
                                system.debug('skipping event, mismatch on notification type triggering field value');
                            }
                        }
                        else{
                            system.debug('skipping notification type, mismatch on triggering object');
                        }       
                    }    
                    else{
                        system.debug('skipping notification type, notification type inactive');
                    }                                
                }                                                                                 
            }         

            // Retrieve the grandparent lookup value on the parent record which isnt provided from a trigger on the event record
            Map<Id, sObject> eventWithGrandParentIdMap = new Map<Id, sObject>();

            if (eventObjectGrandParentFieldsToQuery.size() > 0){

                for (String eventObject : eventObjectGrandParentFieldsToQuery.keySet()){

                    List<String> grandParentFieldsToQuery = new List<String>(eventObjectGrandParentFieldsToQuery.get(eventObject));
                    Set<Id> eventIds = events.keySet();
                    
                    for (sObject eventWithGrandParentId : Database.query('Select Id, ' + String.join(grandParentFieldsToQuery, ', ') + ' From ' + eventObject + ' Where Id in :eventIds')){
                        eventWithGrandParentIdMap.put(eventWithGrandParentId.Id, eventWithGrandParentId);
                    }
                }
            }

            queueNotifications(events, eventWithGrandParentIdMap, oldEvents);
        }
        catch(Exception e){

            system.debug('exception: ' + e.getMessage());
            NotificationService.logError(e, 'NotificationQueuingService', 'processNotificationSubscriptions');
        }        
    }  

    private static void queueNotifications(Map<Id, sObject> records, Map<Id, sObject> eventWithGrandParentIdMap, Map<Id, sObject> oldRecords){

        system.debug('queueNotifications - records triggered: ' + allTriggeredEventsMap.keySet().size());
     
        // Get all subscriptions to the triggered events
        Notification_Type__c[] allTriggeredEvents = new Notification_Type__c[]{};

        for (Id recordId : allTriggeredEventsMap.keySet()){

            Notification_Type__c[] notificationEvents = allTriggeredEventsMap.get(recordId);
            allTriggeredEvents.addAll(notificationEvents);
        }      

        if (allTriggeredEventsMap.size() > 0){

            Map<Id, Notification_Subscription__c[]> triggeredEventSubscriptionsMap = NotificationService.getSubscriptionsByNotificationTypes(allTriggeredEvents);        
            system.debug('triggeredEventSubscriptionsMap: ' + triggeredEventSubscriptionsMap.size());
            
            // Get subscriber information
            Set<Id> subscriberIds = new Set<Id>();

            for (Notification_Subscription__c[] subscriptions : triggeredEventSubscriptionsMap.values()){

                for (Notification_Subscription__c subscription : subscriptions){

                    system.debug('adding subscriber from subscription: ' + subscription);
                    subscriberIds.add(subscription.OwnerId);
                }
            }

            system.debug('subscriberIds: ' + subscriberIds);
            
            Map<Id, User> subscriberMap = new Map<Id, User>([Select Id, Name, UserType, Profile.Name, ContactId, AccountId, Account.Bill_To_Account__c, TimezoneSidKey, Profile.PermissionsPortalSuperUser From User Where Id in :subscriberIds]);

            Map<Id, Set<Id>> subscriberSiteAccessMap = getSubscriberSiteAccessMap(subscriberMap);

            // Queue notifications     
            Notification_Message__c[] notifications = new Notification_Message__c[]{};
                
            for (Id recordId : allTriggeredEventsMap.keySet()){                

                sObject record = records.get(recordId);
                sObject recordWithGrandparentId = eventWithGrandParentIdMap.get(recordId);
                sObject oldRecord = oldRecords != null ? oldRecords.get(recordId) : null;

                system.debug('processing triggered event record: ' + record);

                Notification_Type__c[] recordTriggeredEvents = allTriggeredEventsMap.get(recordId);

                for (Notification_Type__c notificationType : recordTriggeredEvents){

                    system.debug('notificationType: ' + notificationType.Name);

                    Notification_Subscription__c[] notificationTypeSubscriptions = triggeredEventSubscriptionsMap.get(notificationType.Id);

                    system.debug('notification type subscriptions found: ' + (notificationTypeSubscriptions != null ? notificationTypeSubscriptions.size() : 0));

                    if (notificationTypeSubscriptions != null){

                        /* Find the lowest level subscription to use for each subscriber for this notification type
                           Priority of subscription level is as follows: specific record first, then specific parent, then specific grandparent, then global/all sites */

                        Notification_Subscription__c[] recordSubscriptions = new Notification_Subscription__c[]{};   
                        Notification_Subscription__c[] parentSubscriptions = new Notification_Subscription__c[]{};
                        Notification_Subscription__c[] grandparentSubscriptions = new Notification_Subscription__c[]{};
                        Notification_Subscription__c[] globalSubscriptions = new Notification_Subscription__c[]{};

                        for (Notification_Subscription__c subscription : notificationTypeSubscriptions){
                            
                            String subscriptionLevel = NotificationService.getSubscriptionLevel(subscription, notificationType);

                            if (subscriptionLevel == 'Record'){
                                recordSubscriptions.add(subscription);
                            }
                            else if (subscriptionLevel == 'Parent'){
                                parentSubscriptions.add(subscription);
                            }
                            else if (subscriptionLevel == 'Grandparent'){
                                grandparentSubscriptions.add(subscription);
                            } 
                            else if (subscriptionLevel == 'Global'){
                                globalSubscriptions.add(subscription);
                            }                                                        
                        }

                        system.debug('recordSubscriptions: ' + recordSubscriptions.size());
                        system.debug('parentSubscriptions: ' + parentSubscriptions.size());
                        system.debug('grandparentSubscriptions: ' + grandparentSubscriptions.size());
                        system.debug('globalSubscriptions: ' + globalSubscriptions.size());
                  
                        // Qualify each subscription, triggering records first, then parent, then grandparents, then global subscriptions
                        // Only queue 1 notification for each user/event record combo
                        subscriberEventsProcessedMap = new Map<Id, Set<Id>>();

                        for (Notification_Subscription__c subscription : recordSubscriptions){
                            system.debug('evaluating record subscription: ' + subscription);
                            notifications.addAll(createNotificationMessagesFromSubscription(subscription, notificationType, record, recordWithGrandparentId, oldRecords, subscriberMap, subscriberSiteAccessMap)); 
                        }

                        for (Notification_Subscription__c subscription : parentSubscriptions){
                            system.debug('evaluating parent subscription: ' + subscription);
                            notifications.addAll(createNotificationMessagesFromSubscription(subscription, notificationType, record, recordWithGrandparentId, oldRecords, subscriberMap, subscriberSiteAccessMap)); 
                        }

                        for (Notification_Subscription__c subscription : grandparentSubscriptions){
                            system.debug('evaluating grandparent subscription: ' + subscription);
                            notifications.addAll(createNotificationMessagesFromSubscription(subscription, notificationType, record, recordWithGrandparentId, oldRecords, subscriberMap, subscriberSiteAccessMap));                     
                        }   
                        
                        for (Notification_Subscription__c subscription : globalSubscriptions){
                            system.debug('evaluating global subscription: ' + subscription);
                            notifications.addAll(createNotificationMessagesFromSubscription(subscription, notificationType, record, recordWithGrandparentId, oldRecords, subscriberMap, subscriberSiteAccessMap));                             
                        }                            
                    }
                }
            }

            system.debug('notifications built: ' + notifications.size());

            if (notifications.size() > 0){
                sObjectService.insertRecordsAndLogErrors(notifications, 'NotificationQueuingService', 'queueNotifications');
            }
        }
    }    

    private static void updateTriggeredEventsMap(Notification_Type__c notificationType, sObject eventRecord){

        if (notificationType != null){            

            if (allTriggeredEventsMap.containsKey(eventRecord.Id)){
                allTriggeredEventsMap.get(eventRecord.Id).add(notificationType);
            }
            else{
                allTriggeredEventsMap.put(eventRecord.Id, new Notification_Type__c[]{notificationType});
            }

            notificationTypeMap.put(notificationType.Id, notificationType);
        }            
    }   
    
    private static Map<Id, Set<Id>> getSubscriberSiteAccessMap(Map<Id, User> subscriberMap){

        system.debug('getSubscriberSiteAccessMap');

        Map<Id, Set<Id>> subscriberSiteAccessMap = new Map<Id, Set<Id>>();

        Set<Id> superUserBillToAccountIds = new Set<Id>();
        Set<Id> standardUserContactIds = new Set<Id>();
        Set<Id> partnerPortalUserIds = new Set<Id>();
        Map<Id, Set<Id>> billToShipToSiteIds = new Map<Id, Set<Id>>();

        for (User u : subscriberMap.values()){

            system.debug('processing user: ' + u.Name + ' (' + u.Id + ')');

            if (NotificationService.getIsPartner(u)){
                partnerPortalUserIds.add(u.Id);
                subscriberSiteAccessMap.put(u.Id, new Set<Id>{u.AccountId});
            }
            else if (u.Profile.PermissionsPortalSuperUser || u.Profile.Name == communitySettings.Default_Standard_User_All_Profile_Name__c){

                if (u.Account.Bill_To_Account__c != null){

                    superUserBillToAccountIds.add(u.Account.Bill_To_Account__c);
                    billToShipToSiteIds.put(u.Account.Bill_To_Account__c, new Set<Id>{u.AccountId});
                }
            }
            else{
                standardUserContactIds.add(u.ContactId);
            }
        }

        system.debug('partnerPortalUserIds: ' + partnerPortalUserIds);

        if (partnerPortalUserIds.size() > 0){

            for (AccountTeamMember atm : [Select UserId, AccountId From AccountTeamMember Where UserId in :partnerPortalUserIds]){
                subscriberSiteAccessMap.get(atm.UserId).add(atm.AccountId);
            }
        }

        if (superUserBillToAccountIds.size() > 0){

            for (Account site : [Select Id, Bill_To_Account__c From Account Where Bill_To_Account__c in :superUserBillToAccountIds]){

                if (billToShipToSiteIds.containsKey(site.Bill_To_Account__c)){
                    billToShipToSiteIds.get(site.Bill_To_Account__c).add(site.Id);
                }
                else{
                    billToShipToSiteIds.put(site.Bill_To_Account__c, new Set<Id>{site.Id});
                }
            }
        }

        Map<Id, Set<Id>> standardUserSiteIds = new Map<Id, Set<Id>>();

        if (standardUserContactIds.size() > 0){
            
            for (AccountContactRelation acr : [Select Id, ContactId, AccountId From AccountContactRelation Where ContactId in :standardUserContactIds]){

                if (standardUserSiteIds.containsKey(acr.ContactId)){
                    standardUserSiteIds.get(acr.ContactId).add(acr.AccountId);
                }
                else{
                    standardUserSiteIds.put(acr.ContactId, new Set<Id>{acr.AccountId});
                }
            }
        }

        system.debug('standardUserSiteIds: ' + standardUserSiteIds);
        system.debug('billToShipToSiteIds: ' + billToShipToSiteIds);

        for (User u : subscriberMap.values()){

            if (!NotificationService.getIsPartner(u)){

                if (u.Profile.PermissionsPortalSuperUser || u.Profile.Name == communitySettings.Default_Standard_User_All_Profile_Name__c){
                    subscriberSiteAccessMap.put(u.Id, billToShipToSiteIds.get(u.Account.Bill_To_Account__c));
                }
                else{
                    subscriberSiteAccessMap.put(u.Id, standardUserSiteIds.get(u.ContactId));
                }
            }
        }        

        system.debug('subscriberSiteAccessMap: ' + JSON.serialize(subscriberSiteAccessMap));

        return subscriberSiteAccessMap;
    }   

    private static Notification_Message__c evaluateSubscription(
        Notification_Subscription__c subscription, 
        String deliveryFrequency,
        Notification_Type__c notificationType, 
        sObject record,
        sObject recordWithGrandparentId,
        sObject oldRecord,
        User subscriber,
        Set<Id> grandParentIds){

        system.debug('------evaluateSubscription-------: ' + subscription);
        system.debug('subscriber: ' + subscriber);
        system.debug('notificationType: ' + notificationType);
        system.debug('event record: ' + record);
        
        Id recordId = record.Id;
        Id parentId;
        Id grandParentId;

        if (String.isNotBlank(notificationType.Triggering_Object_Parent_Field__c)){
         
            parentId = (Id)record.get(notificationType.Triggering_Object_Parent_Field__c); 

            if (String.isNotBlank(notificationType.Parent_Object_Grandparent_Field__c) && recordWithGrandparentId != null){            
                grandParentId = (Id)recordWithGrandparentId.getSObject(notificationType.Triggering_Object_Parent_Field__c.replace('__c', '__r')).get(notificationType.Parent_Object_Grandparent_Field__c);      
            }            
        }

        system.debug('recordId: ' + recordId);
        system.debug('parentId: ' + parentId);
        system.debug('grandParentId: ' + grandParentId);
        system.debug('grandParentIds: ' + grandParentIds);
        system.debug('subscription record ID: ' + subscription.Record_ID__c);

        Notification_Message__c notification;
        
        if (subscription.Scope__c == 'All Sites'){

            system.debug('all grandparents scope');

            if (grandParentIds != null && grandParentIds.contains(grandParentId)){

                system.debug('global subscription grandparent match, proceeding to build notification');
                notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
            }
            else{
                system.debug('skipping global subscription, grandparents mismatch');
            }
        }
        else if (String.isNotBlank(subscription.Record_ID__c)){

            // If a grandparent (i.e. site) is found on the event record and the user site access are also found then we need to enforce site access here to ensure proper notifications are queued
            if (grandParentId != null && grandParentIds != null){

                system.debug('grandparent found on triggering record and user grandparents were found..');

                if (grandParentIds.contains(grandParentId)){

                    if (subscription.Record_ID__c == grandParentId){
                        system.debug('subscription grandparent record match, proceeding to build notification');
                        notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
                    } 
                    else if (parentId != null && subscription.Record_ID__c == parentId){
                        system.debug('subscription parent record match, proceeding to build notification');
                        notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
                    }  
                    else if (recordId != null && subscription.Record_ID__c == recordId){
                        system.debug('subscription record match, proceeding to build notification');
                        notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
                    }
                    else{
                        system.debug('skipping, no record, parent or grandparent record match');
                    } 
                }
                else{
                    system.debug('grandparent was not found in users hierarchy, skipping record');
                }
            }
            else{

                system.debug('no grandparent found on triggering record or no grandparent access (i.e. site) found for user');

                if (parentId != null && subscription.Record_ID__c == parentId){
                    system.debug('subscription parent record match, proceeding to build notification');
                    notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
                }  
                else if (recordId != null && subscription.Record_ID__c == recordId){
                    system.debug('subscription record match, proceeding to build notification');
                    notification = buildNotification(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, oldRecord, subscriber);
                }
                else{
                    system.debug('skipping, no record, parent or grandparent record match');
                }                 
            }
        }                          
        else{
            system.debug('skipping, unexpected scope found');
        } 

        system.debug('---subscription qualified? ' + (notification != null));

        return notification;
    }

    private static DateTime calcNotificationScheduledDeliveryDate(User subscriber, Notification_Subscription__c subscription, String deliveryFrequency){

        system.debug('calcNotificationScheduledDeliveryDate for ' + deliveryFrequency);
        
        DateTime scheduledDeliveryDate;
        DateTime now = DateTime.now();
        DateTime nowTomorrow = now.addDays(1);
        Date today = now.dateGMT();
        Date tomorrow = today.addDays(1);

        system.debug('now: ' + now);
        system.debug('subscribers timezone key: ' + subscriber.TimezoneSidKey);

        Timezone tz = Timezone.getTimeZone(subscriber.TimezoneSidKey);
        Integer hourOffset = tz.getOffset(DateTime.now()) / 1000 / 60 / 60;
        system.debug('hourOffset: ' + hourOffset);        

        switch on deliveryFrequency {
            when 'Daily' {

                // Schedule for hour configured in custom settings either today or tomorrow
                Integer deliveryHour = Integer.valueOf(notificationSettings.Daily_Digest_Delivery_Hour__c);
                DateTime todaysCutOffTimeGMT = Datetime.newInstanceGMT(now.yearGMT(), now.monthGMT(), now.dayGMT(), deliveryHour - hourOffset, 0, 0);
                system.debug('todaysCutOffTimeGMT: ' + todaysCutOffTimeGMT);
                
                if (now < todaysCutOffTimeGMT){
                    scheduledDeliveryDate = Datetime.newInstanceGMT(now.yearGMT(), now.monthGMT(), now.dayGMT(), deliveryHour - hourOffset - 1, 59, 0);
                }
                else{
                    scheduledDeliveryDate = Datetime.newInstanceGMT(nowTomorrow.yearGMT(), nowTomorrow.monthGMT(), nowTomorrow.dayGMT(), deliveryHour - hourOffset - 1, 59, 0);
                }       
                
                system.debug('scheduledDeliveryDate: ' + scheduledDeliveryDate);
            }
            when 'Weekly' {

                // Schedule for custom setting defined day and hour either this week or next week
                Integer deliveryDay = Integer.valueOf(notificationSettings.Weekly_Digest_Day_of_Week__c);
                Integer deliveryHour = Integer.valueOf(notificationSettings.Weekly_Digest_Delivery_Hour__c);
                
                // deliveryDay = start of week
                Integer daysFromStartOfWeek = today.toStartOfWeek().addDays(deliveryDay).daysBetween(today);
                system.debug('daysFromStartOfWeek: ' + daysFromStartOfWeek);
                
                if (daysFromStartOfWeek <= 1){
                    system.debug('today is after the start of week, subtracting days: ' + (daysFromStartOfWeek + 1));
                    today = today.addDays(-(daysFromStartOfWeek+1));
                }
                
                Date startOfWeek = today.toStartofWeek().addDays(deliveryDay); // This should represent the start of week of the current week
                DateTime startOfWeekTime = Datetime.newInstanceGMT(startOfWeek.year(), startOfWeek.month(), startOfWeek.day(), deliveryHour - hourOffset, 0, 0);
                DateTime startOfNextWeek = startOfWeekTime.addDays(7); // This should represent the start of week of the following week

                system.debug('startOfWeekTime: ' + startOfWeekTime);
                system.debug('startOfNextWeek: ' + startOfNextWeek);
                
                if (now < startOfNextWeek){
                    scheduledDeliveryDate = Datetime.newInstanceGMT(startOfNextWeek.yearGMT(), startOfNextWeek.monthGMT(), startOfNextWeek.dayGMT(), deliveryHour - hourOffset - 1, 59, 0);
                }
                else{

                    startOfNextWeek = startOfNextWeek.addDays(7);
                    scheduledDeliveryDate = Datetime.newInstanceGMT(startOfNextWeek.year(), startOfNextWeek.month(), startOfNextWeek.day(), deliveryHour - hourOffset - 1, 59, 0);
                }                  
            }
            when else {
                scheduledDeliveryDate = now;
            }
        }

        system.debug(subscription.Delivery_Frequencies__c + ' - scheduledDeliveryDate: ' + scheduledDeliveryDate);   

        return scheduledDeliveryDate;
    }    

    private static Notification_Message__c buildNotification(
        Notification_Subscription__c subscription, 
        String deliveryFrequency, 
        Notification_Type__c notificationType, 
        sObject record,
        sObject recordWithGrandparentId,
        sObject oldRecord, 
        User subscriber){

        String message = notificationType.Message_Template__c;

        system.debug('message template: ' + message);

        // Merge in fields
        if (record != null && record.Id != null){
            message = buildMergedNotificationMessage(message, record, oldRecord);
        }

        Notification_Message__c newMessage = new Notification_Message__c(
            OwnerId = subscription.OwnerId,
            Message__c = message,
            Notification_Subscription__c = subscription.Id,
            Notification_Subscription__r = subscription,
            Notification_Type__c = subscription.Notification_Type__c,    
            Delivery_Methods__c = (deliveryFrequency == 'Daily' || deliveryFrequency == 'Weekly') ? 'Email' : subscription.Delivery_Methods__c,
            Delivery_Frequency__c = deliveryFrequency,
            Scheduled_Delivery_Date__c = calcNotificationScheduledDeliveryDate(subscriber, subscription, deliveryFrequency)                                           
        );  

        if (record != null && notificationType != null){

            newMessage.Triggering_Record_ID__c = record.Id;

            String triggeringObjectFieldName = notificationType.Notification_Triggering_Object_Relation__c.replace('__r', '__c');
            newMessage.put(triggeringObjectFieldName, record.Id);
        }

        return newMessage;
    }      
    
    // Instead of using Messaging.renderEmailTemplate to handle the merging which counts a SOQL query for each merge, we will do this manually
    private static String buildMergedNotificationMessage(String messageTemplate, sObject record, sObject oldRecord){

        // Merge field format expected (same as email template): {!<Object_API_Name>.<Field_API_Name>}
        // Prior field value function format expected around a merge field: PRIORVALUE()
        String message = messageTemplate;   

        system.debug('message to merge: ' + message);

        // Replace all merge fields with field values
        Boolean hasMergeFields = true;

        while (hasMergeFields) {
            
            String objectName = String.valueOf(record.getSObjectType());
            String field = message.substringBetween('{!' + objectName + '.', '}');

            if (String.isNotBlank(field)){
               
                system.debug('merge field found for field: ' + objectName + '.' + field);
                
                Integer mergeFieldIndexStart = message.indexOf('{!' + objectName + '.');
                Integer mergeFieldIndexEnd = message.indexOf('}') + 1;
                String mergeField = message.subString(mergeFieldIndexStart, mergeFieldIndexEnd);

                if (mergeField != null){

                    DescribeFieldResult fieldDescribe = objectFieldDescribeMap.get(objectName).get(field);

                    Boolean useOldValue = false;

                    if (mergeFieldIndexStart > 10){
    
                        Integer priorValueIndexStart = message.subString(mergeFieldIndexStart-11, mergeFieldIndexEnd).indexOf('PRIORVALUE(');
                        system.debug('priorValueIndexStart: ' + priorValueIndexStart);
                        
                        if (priorValueIndexStart > -1){                         
                            system.debug('PRIORVALUE function found for this merge field..');
                            useOldValue = true;
                        }
                    }

                    system.debug('useOldValue: ' + useOldValue);

                    sObject recordToUse = (useOldValue && oldRecord != null ? oldRecord : record);

                    String fieldValue;

                    if (fieldDescribe != null){

                        system.debug('field change type: ' + fieldDescribe.getType());

                        switch on fieldDescribe.getType(){
                            when Date{
                                Date dtValue = (Date)recordToUse.get(field);                                
                                fieldValue = dtValue != null ? dtValue.format() : null;                                
                            }
                            when DateTime{
                                DateTime dtValue = (DateTime)recordToUse.get(field);                                
                                fieldValue = dtValue != null ? dtValue.format('MM/dd/yyyy hh:mm a', 'America/New_York') : null;
                            }   
                            when Integer{
                                fieldValue = String.valueOf((Integer)recordToUse.get(field));
                            }  
                            when Boolean{
                                fieldValue = String.valueOf((Boolean)recordToUse.get(field));
                            }                                
                            when Double{
                                fieldValue = String.valueOf((Double)recordToUse.get(field));
                            } 
                            when Long{
                                fieldValue = String.valueOf((Long)recordToUse.get(field));
                            }                                                                                                                                                                        
                            when else{
                                fieldValue = (String)recordToUse.get(field);
                            }
                        }
                    }

                    system.debug('field value to merge: ' + fieldValue);

                    // Escape braces for regex
                    mergeField = mergeField.replace('{!', '\\{!');
                    mergeField = mergeField.replace('}', '\\}');

                    system.debug('final merge field to use: ' + mergeField);

                    if (String.isNotBlank(fieldValue)){                 
                        message = message.replaceFirst(mergeField, fieldValue);
                    }
                    else{
                        message = message.replaceFirst(mergeField, 'blank');
                    }

                    system.debug('updated message after field merge: ' + message);
                }
            }
            else{
                system.debug('done merging fields');
                hasMergeFields = false;
            }
        }

        // Remove any supported functions
        message = message.replace('PRIORVALUE(', '');
        message = message.replace(')', '');        

        system.debug('merged message: ' + message);

        return message;
    }     

    private static Notification_Message__c[] createNotificationMessagesFromSubscription(
        Notification_Subscription__c subscription, 
        Notification_Type__c notificationType,
        sObject record,
        sObject recordWithGrandparentId,
        Map<Id, sObject> oldRecords,
        Map<Id, User> subscriberMap,
        Map<Id, Set<Id>> subscriberSiteAccessMap){

        Notification_Message__c[] notifications = new Notification_Message__c[]{};

        User subscriber = subscriberMap.get(subscription.OwnerId);
        system.debug('createNotificationMessagesFromSubscription for subscriber: ' + subscriber);          
        
        Set<Id> alreadyProcessedEvents = subscriberEventsProcessedMap.get(subscriber.Id);

        if (alreadyProcessedEvents != null && alreadyProcessedEvents.size() > 0){
            system.debug('skipping subscription, already processed this event from another subscription for this subscriber...');
        }
        else{

            // Opt out subscriptions will not have delivery frequencies assigned
            String[] deliveryFrequencies = String.isNotBlank(subscription.Delivery_Frequencies__c) ? subscription.Delivery_Frequencies__c.split(';') : new String[]{NotificationService.REAL_TIME};

            for (String deliveryFrequency : deliveryFrequencies){

                Set<Id> siteIds = subscriberSiteAccessMap != null ? subscriberSiteAccessMap.get(subscriber.Id) : null;

                Notification_Message__c notification = evaluateSubscription(subscription, deliveryFrequency, notificationType, record, recordWithGrandparentId, (oldRecords != null ? oldRecords.get(record.Id) : null), subscriber, siteIds);
                        
                if (notification != null){

                    // If an opt-out subscription is qualified, skip adding the notification 
                    // but still add it to the processed events map so this event is not qualified with a higher level subscription for the subscriber
                    if (!subscription.Opt_Out__c){
                        notifications.add(notification);
                    }
                    else{
                        system.debug('opt out found, skipping notification for: ' + subscriber.Name + '(' + notificationType.Name + ')');
                    }

                    if (subscriberEventsProcessedMap.get(subscriber.Id) == null){
                        subscriberEventsProcessedMap.put(subscriber.Id, new Set<Id>{record.Id});
                    }
                    else{
                        subscriberEventsProcessedMap.get(subscriber.Id).add(record.Id);
                    }                    
                }
            }   
        }    
        
        return notifications;
    }    
}