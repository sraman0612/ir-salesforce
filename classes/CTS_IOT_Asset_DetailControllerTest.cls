/**
 * Test class of CTS_IOT_Asset_DetailController
 **/
@isTest
private class CTS_IOT_Asset_DetailControllerTest {
	
    @testSetup
    static void setup(){
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', acc.Id, assetRecTypeId, true);
    }

    @isTest
    static void testGetInit(){

        List<Asset> assets = [select id from Asset limit 1];

        if(!assets.isEmpty()){

            Test.startTest();

            CTS_IOT_Asset_DetailController.InitResponse initRes = CTS_IOT_Asset_DetailController.getInit(assets.get(0).Id);
            System.assert(initRes != null);
            System.assert(initRes.assetDetail != null);
            System.assert(initRes.site != null);

            Test.stopTest();
        }
    }

    @isTest
    static void testGetInitException(){

        List<Asset> assets = [select id from Asset limit 1];

        if(!assets.isEmpty()){

            Test.startTest();

            CTS_IOT_Asset_DetailController.forceError = true;
            CTS_IOT_Asset_DetailController.InitResponse initRes = CTS_IOT_Asset_DetailController.getInit(assets.get(0).Id);
            System.assert(initRes != null);

            Test.stopTest();
        }
    }    
}