public class PT_ParentOpportunityTriggerHandler 
{
	public static void setLinkedInCampaign(PT_Parent_Opportunity__c[] parentOpportunities)
    {
        PT_General_Setting__mdt[] ptSettings = [
            SELECT Default_LinkedIn_Campaign_Name__c
            FROM PT_General_Setting__mdt
            LIMIT 1
        ];
    	if(ptSettings.isEmpty()) return;
        
		Campaign[] campaigns = [SELECT Id, PT_Campaign_Id__c FROM Campaign WHERE Name = :ptSettings[0].Default_LinkedIn_Campaign_Name__c];
        
        for(PT_Parent_Opportunity__c thisParentOpportunity: parentOpportunities)
        {
            if(thisParentOpportunity.Lead_SourcePL__c == 'LinkedIn Sales Navigator')
            {
				thisParentOpportunity.PT_Campaign__c = campaigns[0].Id;                
            }
            
        }
        
    }
}