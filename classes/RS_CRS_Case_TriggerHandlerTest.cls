/******************************************************************************
Name:       RS_CRS_Case_TriggerHandleTest
Purpose:    Test class for RS_CRS_Contact_TriggerHandler. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Neela Prasad    02/02/2018  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_Case_TriggerHandlerTest {
    /******************************************************************************
	Method Name : getPreviousOwner_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing getPreviousOwner Method
	******************************************************************************/
    Static testmethod void getPreviousOwner_Test(){
        
        //get Esc Spec profile
        Profile escSpecProf = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get Manager profile
        Profile managerProf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        //get esc spec user role
        UserRole userRole1 =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole1; 
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecordTypeId			= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        Id caseFSRRecordTypeId		= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_FSR_Case_Record_Type);
        Id closeCaseRecordTypeId	= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Close_Case_Record_Type);
        
        Test.startTest();
        //prepare username
        String uniqueUserName1 = 'standarduser1' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec1 = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                 EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                 LocaleSidKey='en_US', ProfileId = escSpecProf.Id, UserRoleId = userRole1.Id,
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName=uniqueUserName1);
        insert userRec1;
        
        //prepare username
        String uniqueUserName2 = 'standarduser2' + DateTime.now().getTime() + '@testorg.com';
        
        User userRec2 = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                 EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                 LocaleSidKey='en_US', ProfileId = managerProf.Id, //UserRoleId = userRole2.Id,
                                 TimeZoneSidKey='America/Los_Angeles',
                                 UserName=uniqueUserName2);
        
        insert userRec2;
        
        
        Case caseObj = new Case();
        Case caseFSRObj = new Case();
        System.runAs(userRec1){
            //insert test case
            caseObj = new Case();
            caseObj.RecordTypeId = caseRecordTypeId;
            caseObj.Status = 'Customer Action';
            caseObj.OwnerId =  userRec1.id;
            insert caseObj;
            
            //insert test case
            caseFSRObj = new Case();
            caseFSRObj.RecordTypeId = caseFSRRecordTypeId;
            caseFSRObj.Status = 'Customer Action';
            caseFSRObj.OwnerId =  userRec1.id;
            insert caseFSRObj;
            
            caseFSRObj.status = 'Closed';
            update caseFSRObj;
        }
        
        //check case close record type
        Case caseFSR = [Select Id, RecordTypeId, RS_CRS_Previous_Record_Type_Id__c From case Where Id =: caseFSRObj.Id Limit 1];
        //System.assertEquals(closeCaseRecordTypeId, caseFSR.RecordTypeId);
        
        System.runAs(userRec2){
            caseObj.OwnerId =  userRec2.id;
            update caseObj;
            
            caseFSRObj.status = 'Reopen';
            update caseFSRObj;
        }
        
        Test.stopTest();
        
        Case caseRec = [Select Id, RS_CRS_Previous_Owner__c From case Where Id =: caseObj.Id Limit 1];
        //System.assertEquals(userRec1.id, caseRec.RS_CRS_Previous_Owner__c );
        
        //check case reverted back to FSR record type        
        Case caseFSRRec = [Select Id, RS_CRS_Previous_Record_Type_Id__c From case Where Id =: caseFSRObj.Id Limit 1];
        //System.assertEquals(caseFSRRec.RS_CRS_Previous_Record_Type_Id__c, caseFSRRecordTypeId);        
        
    } 
}