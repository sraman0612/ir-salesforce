public with sharing class Font_Awesome_Icons_Preview_Controller {

    public Font_Awesome_Icon__mdt[] getIcons(){
        return [Select Id, DeveloperName, Label, Icon_Class__c, Default_Color__c, Hover_Color__c From Font_Awesome_Icon__mdt Order By Label];
    }
}