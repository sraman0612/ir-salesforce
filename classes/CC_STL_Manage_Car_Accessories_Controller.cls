public with sharing class CC_STL_Manage_Car_Accessories_Controller {

    @TestVisible
    private class InitResponse extends LightningResponseBase{
        public STL_Car_Accessory_Line_Item_Option[] lineItemOptions = new STL_Car_Accessory_Line_Item_Option[]{};
    }
    
    @TestVisible private static Boolean forceError = false;
    
    private class CC_STL_Manage_Car_Accessories_ControllerException extends Exception{}
    
    // Class to display on the component to allow selections of all line items available to the car
    @TestVisible
    private class STL_Car_Accessory_Line_Item_Option{
        
        public Boolean isSelected;
        public Id lineItemId;
        public Id carInfoId;
        public Id carAccessoryId;
        public String accessoryName;
        public Decimal quantity;
        public Decimal unitLaborCost;
        
        public STL_Car_Accessory_Line_Item_Option(Boolean isSelected, Id lineItemId, Id carInfoId, Id carAccessoryId, String accessoryName, Decimal quantity, Decimal unitLaborCost){
        	this.isSelected = isSelected;
            this.lineItemId = lineItemId;
            this.carInfoId = carInfoId;
            this.carAccessoryId = carAccessoryId;
            this.accessoryName = accessoryName;
            this.quantity = quantity;
            this.unitLaborCost = unitLaborCost;
        }
        
        public CC_STL_Car_Accessory_Line_Item__c convertToLineItem(){
            
            CC_STL_Car_Accessory_Line_Item__c lineItem = new CC_STL_Car_Accessory_Line_Item__c();
            lineItem.Id = null;
            lineItem.STL_Car__c = carInfoId;
            lineItem.STL_Car_Accessory__c = carAccessoryId;
            lineItem.Quantity__c = quantity;
            lineItem.Unit_Labor_Cost__c = unitLaborCost;
            
            return lineItem;
        }
    }
    
    @AuraEnabled
    // Get everything needed to initialize the component
    public static String getInit(Id stlCarInfoId){
        
        InitResponse response = new InitResponse();
        
        try{
        
            if (forceError){
                throw new CC_STL_Manage_Car_Accessories_ControllerException('Test Exception');
            }
            
            // Retrieve the product assigned to the car record including the currently assigned accessories
            CC_STL_Car_Info__c stlCarInfo = [Select Id, Item__c, (Select Id, Quantity__c, Unit_Labor_Cost__c, STL_Car_Accessory__r.Accessory_Type__c From CC_STL_Car_Accessory_Line_Items__r Order By Accessory_Type__c ASC) From CC_STL_Car_Info__c Where Id = :stlCarInfoId];
            
            Set<Id> assignedAccessoryIds = new Set<Id>();
            
            if (stlCarInfo.CC_STL_Car_Accessory_Line_Items__r != null && stlCarInfo.CC_STL_Car_Accessory_Line_Items__r.size() > 0){
                
                for (CC_STL_Car_Accessory_Line_Item__c lineItem : stlCarInfo.CC_STL_Car_Accessory_Line_Items__r){
                    
                    assignedAccessoryIds.add(lineItem.STL_Car_Accessory__c);
                    
                    // Build out the accessory line item options to display in the component based on the already assigned accessories
                	response.lineItemOptions.add(new STL_Car_Accessory_Line_Item_Option(
                		true, 
                        lineItem.Id, 
                        stlCarInfoId, 
                        lineItem.STL_Car_Accessory__c, 
                        lineItem.STL_Car_Accessory__r.Accessory_Type__c,
                        lineItem.Quantity__c, 
                        lineItem.Unit_Labor_Cost__c
                	));           
                }
            }
            
            // Find all available accessories for the same product as the STL car info record that are not already assigned to the car
            for (CC_STL_Car_Accessory__c accessory : [Select Id, Name, Accessory_Type__c, Labor_Cost__c 
                                                      From CC_STL_Car_Accessory__c 
                                                      Where Product__c = :stlCarInfo.Item__c and Id not in :assignedAccessoryIds
                                                      Order By Accessory_Type__c ASC]){
                                                          
               	// Build out the accessory line item options to display in the component based on the available accessories
              	response.lineItemOptions.add(new STL_Car_Accessory_Line_Item_Option(
                	false, 
                    null, 
                    stlCarInfoId, 
                    accessory.Id, 
                    accessory.Accessory_Type__c,
                    1, 
                    accessory.Labor_Cost__c
                ));                     
            }
        }
        catch (Exception e){
            system.debug('exception caught: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }
        
        return JSON.serialize(response);
    }
    
    @AuraEnabled
    // Save all selected accessories
    public static String saveAccessories(Id stlCarInfoId, String lineItemOptionsStr){
        
        system.debug('stlCarInfoId: ' + stlCarInfoId);
        system.debug('lineOptionsStr: ' + lineItemOptionsStr);
        
        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();
        
        try{
        	
            if (forceError){
                throw new CC_STL_Manage_Car_Accessories_ControllerException('Test Exception');
            }
            
            STL_Car_Accessory_Line_Item_Option[] lineItemOptions = (STL_Car_Accessory_Line_Item_Option[])JSON.deserialize(lineItemOptionsStr, STL_Car_Accessory_Line_Item_Option[].class);
                
            system.debug('lineItemOptions: ' + lineItemOptions); 
            
            CC_STL_Car_Accessory_Line_Item__c[] lineItemsToAdd = new CC_STL_Car_Accessory_Line_Item__c[]{};
                
            for (STL_Car_Accessory_Line_Item_Option lineItemOption : lineItemOptions){
                
                if (lineItemOption.isSelected){
                	lineItemsToAdd.add(lineItemOption.convertToLineItem());
                }
            }
            
            // Remove all items from the car first
            delete [Select Id From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c = :stlCarInfoId];
            
            // Insert all selected accessories
            insert lineItemsToAdd; 
        }
        catch (Exception e){
            system.debug('exception caught, rolling back changes: ' + e.getMessage());
            Database.rollback(sp);
            response.success = false;
            response.errorMessage = e.getMessage();            
        }
        
        return JSON.serialize(response);
    }
}