@isTest
public with sharing class CC_STL_Send_To_MAPICS_ControllerTest {
    
    @TestSetup
    private static void createData(){
    	
    	TestDataUtility dataUtil = new TestDataUtility();
    	
        User testUser = dataUtil.createIntegrationUser();
        testUser.LastName = 'Test_User_123';
        insert testUser;    	
        
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
    	insert salesRep;        
        
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;           
        
    	CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
    	insert masterLease;
    	
        CC_Short_Term_Lease__c stl = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
        insert stl;   	
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
		insert prod1;      
        
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl.Id;
        car1.Quantity__c = 10;
        car1.Per_Car_Cost__c = 25;      
        insert car1; 
        
        delete [Select Id From CC_ERPMQ__c]; // Clean out anything generated from workflow to control the data in the table   
        
        CC_ERPMQ__c msgSTLNew = new CC_ERPMQ__c(Key__c = stl.Id, Type__c = 'STL_New', Processed__c = false);
        CC_ERPMQ__c msgSTLUpdate = new CC_ERPMQ__c(Key__c = stl.Id, Type__c = 'STL_Update', Processed__c = false); 
        CC_ERPMQ__c msgSTLUpdateProcessed = new CC_ERPMQ__c(Key__c = stl.Id, Type__c = 'STL_Update', Processed__c = true);        
        insert new CC_ERPMQ__c[]{msgSTLNew, msgSTLUpdate, msgSTLUpdateProcessed};               
    }
    
    @isTest
    private static void testSendToMAPICSAlreadySent(){
    	
    	User testUser = [Select Id From User Where LastName = 'Test_User_123'];
    	CC_Short_Term_Lease__c stl = [Select Id, Synced_to_MAPICS__c From CC_Short_Term_Lease__c];
    	
    	CC_ERPMQ__c[] messages = [Select Type__c From CC_ERPMQ__c Where Key__c = :stl.Id];
    	system.assertEquals(3, messages.size());

    	Test.startTest();
    	
    	system.runAs(testUser){
    		
	    	// Flag the lease to be sent to MAPICS
	    	stl.Synced_to_MAPICS__c = true;
	    	update stl;    		
    		
            // Send the lease to MAPICS using the component
    		LightningResponseBase response = CC_STL_Send_To_MAPICS_Controller.sendLeaseToMAPICS(stl.Id);
    		
    		system.assertEquals(true, response.success);
    		system.assertEquals('', response.errorMessage);
    	}
    	
    	Test.stopTest();
    	
    	stl = [Select Id, Synced_to_MAPICS__c From CC_Short_Term_Lease__c];
    	
        // Verify the lease still shows as synced
    	system.assertEquals(true, stl.Synced_to_MAPICS__c); 
    	
    	// Verify the unprocessed STL_NEW message is still there and a new STL_UPDATE message has been queued
    	Integer newMessageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :stl.Id and Processed__c = false and Type__c = 'STL_NEW'];
    	system.assertEquals(1, newMessageCount);   
    	
    	// Verify there is a new unprocessed STL_UPDATE message in the queue
    	Integer unprocessedUpdateMessageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :stl.Id and Processed__c = false and Type__c = 'STL_UPDATE' and Id not in :messages];
    	system.assertEquals(1, unprocessedUpdateMessageCount);         

    	// Verify the processed STL_UPDATE message is still in the queue
    	Integer processedUpdateMessageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :stl.Id and Processed__c = true and Type__c = 'STL_UPDATE' and Id in :messages];
    	system.assertEquals(1, processedUpdateMessageCount); 
    }
    
    @isTest
    private static void testSendToMAPICSSuccess(){
    	
    	User testUser = [Select Id, Name From User Where LastName = 'Test_User_123'];
    	CC_Short_Term_Lease__c stl = [Select Id, Synced_to_MAPICS__c From CC_Short_Term_Lease__c];
    	
        // Clear out all messages for the lease in the queue
    	delete [Select Type__c From CC_ERPMQ__c Where Key__c = :stl.Id];

        // Verify the lease is not synced yet
		system.assertEquals(false, stl.Synced_to_MAPICS__c);

    	Test.startTest();
    	
    	system.runAs(testUser){
    		    		
    		LightningResponseBase response = CC_STL_Send_To_MAPICS_Controller.sendLeaseToMAPICS(stl.Id);
    		
    		system.assertEquals(true, response.success);
    		system.assertEquals('', response.errorMessage);
    	}
    	
    	Test.stopTest();
    	    	
    	stl = [Select Id, Synced_to_MAPICS__c From CC_Short_Term_Lease__c];
    	
        // Verify the lease is now synced
    	system.assertEquals(true, stl.Synced_to_MAPICS__c); 
    	
    	// Verify there is an unprocessed STL_NEW message in the queue now
    	Integer newMessageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :stl.Id and Processed__c = false and Type__c = 'STL_NEW'];
    	system.assertEquals(1, newMessageCount);   
    	
    	// Verify there are no other messages in the queue
    	Integer otherMessageCount = [Select Count() From CC_ERPMQ__c Where Key__c = :stl.Id and Type__c != 'STL_NEW'];
    	system.assertEquals(0, otherMessageCount);
    }         
}