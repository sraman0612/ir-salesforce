public class PT_LeadTriggerHandler {
    
    public class PT_LeadTriggerHandlerException extends Exception{}
    
    public static void run(Map<String, List<Lead>> newLeadMap, Map<String, List<Lead>> oldLeadMap)
    {
        System.debug('PT_LeadTriggerHandler run');
        Lead[] newPowertoolLeads = newLeadMap.get('PT Lead');
        Lead[] oldPowertoolLeads = oldLeadMap.get('PT Lead');
        if(newPowertoolLeads == null) return;
        
        Map<Id, Lead> triggerOldMap = (Map<Id, Lead>)Trigger.oldMap;
        
        switch on Trigger.operationType {
        
            when BEFORE_INSERT 
            {
                setDefaultCampiagn(newPowertoolLeads);
                syncLeadStatus(newPowertoolLeads); 
                setCampaignId(newPowertoolLeads);
            }
            when AFTER_INSERT 
            {
                createLeadCampaignMember(newPowertoolLeads);
            }
            when BEFORE_UPDATE {
                assignLeads(newPowertoolLeads);
                syncLeadStatus(newPowertoolLeads); 
                checkLeadOwner(newPowertoolLeads, triggerOldMap);
                setCampaignId(newPowertoolLeads);
            }
            when AFTER_UPDATE {}
            when AFTER_DELETE {}
            when else {}
        }
    }
    
    public static void setCampaignId(Lead[] leads)
    {
        System.debug('PT_LeadTriggerHandler.setCampaignId');
        Set<String> utmCampaignIds = new Set<String>();
        for(Lead thisLead: leads)
        {
            if(!String.isEmpty(thisLead.UTM_Campaign__c))
            {
                utmCampaignIds.add(thisLead.UTM_Campaign__c); 
            }
        }
        System.debug('PT_LeadTriggerHandler.setCampaignId utmCampaignIds found:'+utmCampaignIds);
        
        
        Campaign[] campaigns = [SELECT Id, PT_Campaign_Id__c FROM Campaign WHERE PT_Campaign_Id__c IN :utmCampaignIds];
        System.debug('PT_LeadTriggerHandler.setCampaignId Campaigns found:'+campaigns);
        
        Map<String, Campaign> campaignsByPtCampaignId = new Map<String,Campaign>();

        for(Campaign thisCampaign: campaigns)
        {
            campaignsByPtCampaignId.put(thisCampaign.PT_Campaign_Id__c, thisCampaign);    
        }

        for(Lead thisLead: leads)
        {
            if(campaignsByPtCampaignId.containsKey(thisLead.UTM_Campaign__c))
            {
                thisLead.PT_Campaign__c = campaignsByPtCampaignId.get(thisLead.UTM_Campaign__c).Id;
            }
        }
    }
    

    public static void setDefaultCampiagn(Lead[] newPowertoolLeads)
    {
        for(Lead thisLead: newPowertoolLeads) 
        {
            if(thisLead.LeadSource == 'Ingersollrandproducts.com' && thisLead.UTM_Campaign__c == null) thisLead.UTM_Campaign__c = 'CA_000001';            
        }        
    }
    
    public static void createLeadCampaignMember(Lead[] newPowertoolLeads)
    {   
        Set<String> campaignAutoNumbers = new Set<String>();
        for(Lead thisLead: newPowertoolLeads) 
        {
            campaignAutoNumbers.add(thisLead.UTM_Campaign__c);
        }
        
        String campaignsQuery = 'SELECT Id, PT_Campaign_Id__c FROM Campaign WHERE PT_Campaign_Id__c IN :campaignAutoNumbers';
        Campaign[] campaigns = database.query(campaignsQuery);
        Map<String, Campaign> campaignsByAutonumber = new Map<String,Campaign>();

        for(Campaign thisCampaign: campaigns)
        {
            campaignsByAutonumber.put((String)thisCampaign.get('PT_Campaign_Id__c'), thisCampaign);    
        }
        
        CampaignMember[] campaignMembers = new CampaignMember[]{};
        for(Lead thisLead: newPowertoolLeads) 
        {
            if(campaignsByAutonumber.containsKey(thisLead.UTM_Campaign__c))
            {
                CampaignMember newCampaignMember = new CampaignMember();
                newCampaignMember.CampaignId = campaignsByAutonumber.get(thisLead.UTM_Campaign__c).Id;
                newCampaignMember.LeadId = thisLead.Id;
                campaignMembers.add(newCampaignMember);
            }
        }

        insert campaignMembers;
    }
    
    /*
        Set PT_Status__c. This field is used allow path to be used in the UI
    */
    public static void syncLeadStatus(Lead[] newPowertoolLeads)
    {   
        if(newPowertoolLeads == null) return;
        
        for(Lead thisLead: newPowertoolLeads) 
        { 
            //For web to lead leads set the correct status
            if(thisLead.Status == 'Open') thisLead.Status = '1. Open'; 
            system.debug('thisLead.Status------->'+thisLead.Status);
            
            //Set the PT_Status__c to the Lead.Status.
            //This is needed for when users edit the status outside of the lightning:picklistPath on the lead page
            if(thisLead.Status.contains('4')) thisLead.PT_Status__c = '4. Close';
            else thisLead.PT_Status__c = thisLead.Status;
        }
    }
    
    public static void assignLeads(List<Lead> leadLst){
        if(!leadLst.isEmpty()){
            Id PTLeadQueueId = [SELECT id,Name FROM Group WHERE type='Queue' AND Name='PT Leads' LIMIT 1].id;
            Map<String, Id> ptLeadMap = new Map<String, Id>();
            for(PT_Lead_Sales_Mapping__c lsm : [SELECT Country__c,User__c FROM PT_Lead_Sales_Mapping__c WHERE User__c != null]){
                ptLeadMap.put(lsm.Country__c.toLowerCase(),lsm.User__c);
            }
            for(Lead l : leadLst){
                if(l.ownerId==PTLeadQueueId && null!=l.Country && null!=ptLeadMap.get(l.Country.toLowerCase())){
                    l.OwnerId = ptLeadMap.get(l.Country.toLowerCase());
                    l.PT_Send_Lead_Owner_Notice__c=TRUE;
                }
            }
        }
    }

    public static void checkLeadOwner(List<Lead> leads, Map<Id, Lead> oldLeadsMap) {
        try {
            System.debug('PT_LeadTriggerHandler checkLeadOwner');
            //Declare Collections
            Map<String, List<Lead>> ownerIdToLeadsMap = new Map<String, List<Lead>>();
            Set<String> leadIdSet = new Set<String>();
            List<PT_General_Setting__mdt> ptSettings = new List<PT_General_Setting__mdt>();

            for (Lead thisLead : leads) {
                //Entry Criteria Owner Changed
                if(thisLead.OwnerId != oldLeadsMap.get(thisLead.Id).OwnerId) {
                    if(!ownerIdToLeadsMap.containsKey(thisLead.OwnerId)) ownerIdToLeadsMap.put(thisLead.OwnerId, new List<Lead>());
                    ownerIdToLeadsMap.get(thisLead.OwnerId).add(thisLead);
                }
            }

            if (!ownerIdToLeadsMap.isEmpty()) {
                //Fetch Metadata Record
                ptSettings = [
                        SELECT Default_Lead_Record_Type_Name__c,PT_Profile_Names__c
                        FROM PT_General_Setting__mdt
                        LIMIT 1
                ];

                if(ptSettings.isEmpty()) return;

                //Update Leads
                leadIdSet = getProfileNameToLeadsMap(ownerIdToLeadsMap, ptSettings[0]);

                //Delete Campign Members
                if (!leadIdSet.isEmpty()) deleteCampaignMembers(leadIdSet);
            }
        } catch (Exception er) {
            System.debug('Error in checkLeadOwner ' + er.getStackTraceString());
        }
    }

    private static Set<String> getProfileNameToLeadsMap(Map<String, List<Lead>> ownerIdToLeadsMap, PT_General_Setting__mdt ptProfileToRecordTypeMapping) {
        List<User> users = new List<User>();
        Set<String> leadIdSet = new Set<String>();

        //Get Users
        users = [
                SELECT Id,Profile.Name
                FROM User
                WHERE Id IN :ownerIdToLeadsMap.keySet()
        ];

        if (!users.isEmpty()) {
            for (User userRec : users) {

                //Check If the Profile is Not in the Metadata Record
                if (!ptProfileToRecordTypeMapping.PT_Profile_Names__c.split(',').contains(userRec.Profile.Name)) {

                    for (Lead thisLead : ownerIdToLeadsMap.get(userRec.Id)) 
                    {
                        String defaultLeadRecordTypeName = ptProfileToRecordTypeMapping.Default_Lead_Record_Type_Name__c;
                        System.debug('PT_LeadTriggerHandler defaultLeadRecordTypeName:'+defaultLeadRecordTypeName);
                        thisLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(defaultLeadRecordTypeName).getRecordTypeId();
                        System.debug('PT_LeadTriggerHandler thisLead.RecordTypeId:'+thisLead.RecordTypeId);
                        leadIdSet.add(thisLead.Id);
                    }
                }
            }
        }
        return leadIdSet;
    }

    private static void deleteCampaignMembers(Set<String> leadIdSet) {
        //Delete Campign Members
        delete [
                SELECT Id
                FROM CampaignMember
                WHERE LeadId IN :leadIdSet
        ];
    }
}