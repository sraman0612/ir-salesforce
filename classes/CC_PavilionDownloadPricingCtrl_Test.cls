@isTest
public class CC_PavilionDownloadPricingCtrl_Test {

    @TestSetup
    private static void createData(){
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%CC_PavilionCommunityUser%' Limit 1];
        Id ccAccountRTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();

        // CAD Pricing 
        Account account1 = TestUtilityClass.createAccount();
        account1.RecordTypeId = ccAccountRTypeId;
        account1.name = 'Test1';
        account1.CC_Price_List__c = 'PCOPP';
        account1.CC_Customer_Number__c = '5621';
        
        // Non-CAD Pricing 
        Account account2 = TestUtilityClass.createAccount();
        account2.RecordTypeId = ccAccountRTypeId;
        account2.Name = 'Test2';
        account2.CC_Price_List__c = 'PCOPI';
        account2.CC_Customer_Number__c = '5612';
        
        insert new Account[]{account1, account2};

        Id ccContactRTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        
        // CAD
        Contact contact1 = new Contact(
            RecordTypeId = ccContactRTypeId,
            FirstName = 'CAD',
            LastName ='Test',
            AccountId = account1.Id,
            Email = 'test@test.ingersollrand.com'
        );

        Contact contact2 = new Contact(
            RecordTypeId = ccContactRTypeId,
            FirstName = 'NonCAD',
            LastName ='Test',
            AccountId = account2.Id,
            Email = 'test@test.ingersollrand.com'
        );

        insert new Contact[]{contact1, contact2};  
        
        User cadUser = new User(
            Username = System.now().millisecond() + 'test12345Clubcar1@test.ingersollrand.com',
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );

        User nonCADUser = new User(
            Username = System.now().millisecond() + 'test12345Clubcar2@test.ingersollrand.com',
            ContactId = contact2.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12346',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );        

        insert new User[]{cadUser, nonCADUser};    
		
       	Id prodRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        
        Product2 prod1 = new product2( 
            name='Test1',
            ProductCode = 'AM10003',
            isactive=true,
            CC_Inventory_Count__c = 10,
            RecordTypeId = prodRecordTypeId
        );

        Product2 prod2 = new product2( 
            name='Test2',
            ProductCode = 'AM10004',
            isactive=true, 
            CC_Inventory_Count__c = 10,
            RecordTypeId = prodRecordTypeId
        );        

        insert new Product2[]{prod1, prod2};

        CC_Product_Rule__c rule1 = new CC_Product_Rule__c(
            CC_Obsolete_Item__c = prod1.Id,
            CC_Type__c = 'Message',             
            CC_Message__c = 'This part may require additional shipping charges.'
        );

        CC_Product_Rule__c rule2 = new CC_Product_Rule__c(
            CC_Obsolete_Item__c = prod2.Id,
            CC_Type__c = 'Replace', 
            CC_Replacement_Item__c = prod1.Id,
            CC_Substitute_Effective_Date__c = Date.today().addDays(-10)
        );

        insert new CC_Product_Rule__c[]{rule1, rule2};
            
        Pricebook2 plist = new pricebook2(name='PLIST',isactive=true);        
        Pricebook2 customerPL1 = new pricebook2(name=account1.CC_Price_List__c,isactive=true);
        Pricebook2 customerPL2 = new pricebook2(name=account2.CC_Price_List__c,isactive=true);

        insert new Pricebook2[]{plist, customerPL1, customerPL2};  

        Pricebookentry plistPBE1 = new Pricebookentry(
            isActive = true,
            product2id = prod1.id, 
            pricebook2id = plist.id, 
            UnitPrice = 10,
            CC_New_UnitPrice__c = 10,
            CC_New_Quantity_Price__c = 10,
            CC_New_CAD_Unit_Price__c = 10,
            CC_CAD_Unit_Price__c = 10,
            CC_Quantity_Discount__c = 10,
            CC_CAD_Quantity_Price__c = 10,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );

        Pricebookentry customerPBE1 = new Pricebookentry(
            isActive = true,
            product2id = prod1.id, 
            pricebook2id = customerPL1.id, 
            UnitPrice = 15,
            CC_New_UnitPrice__c = 15,
            CC_New_Quantity_Price__c = 15,
            CC_New_CAD_Unit_Price__c = 15,
            CC_CAD_Unit_Price__c = 15,
            CC_Quantity_Discount__c = 15,
            CC_CAD_Quantity_Price__c = 15,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );

        Pricebookentry customerPBE2 = new Pricebookentry(
            isActive = true,
            product2id = prod1.id, 
            pricebook2id = customerPL2.id, 
            UnitPrice = 15,
            CC_New_UnitPrice__c = 15,
            CC_New_Quantity_Price__c = 15,
            CC_New_CAD_Unit_Price__c = 15,
            CC_CAD_Unit_Price__c = 15,
            CC_Quantity_Discount__c = 15,
            CC_CAD_Quantity_Price__c = 15,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );        

        Pricebookentry plistPBE2 = new Pricebookentry(
            isActive = true,
            product2id = prod2.id, 
            pricebook2id = plist.id, 
            UnitPrice = 20,
            CC_New_UnitPrice__c = 20,
            CC_New_Quantity_Price__c = 20,
            CC_New_CAD_Unit_Price__c = 20,
            CC_CAD_Unit_Price__c = 20,
            CC_Quantity_Discount__c = 20,
            CC_CAD_Quantity_Price__c = 20,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );

        Pricebookentry customerPBE3 = new Pricebookentry(
            isActive = true,
            product2id = prod2.id, 
            pricebook2id = customerPL1.id, 
            UnitPrice = 25,
            CC_New_UnitPrice__c = 25,
            CC_New_Quantity_Price__c = 25,
            CC_New_CAD_Unit_Price__c = 25,
            CC_CAD_Unit_Price__c = 25,
            CC_Quantity_Discount__c = 25,
            CC_CAD_Quantity_Price__c = 25,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );

        Pricebookentry customerPBE4 = new Pricebookentry(
            isActive = true,
            product2id = prod2.id, 
            pricebook2id = customerPL2.id, 
            UnitPrice = 25,
            CC_New_UnitPrice__c = 25,
            CC_New_Quantity_Price__c = 25,
            CC_New_CAD_Unit_Price__c = 25,
            CC_CAD_Unit_Price__c = 25,
            CC_Quantity_Discount__c = 25,
            CC_CAD_Quantity_Price__c = 25,
            CC_Price_Change_Date__c = Date.newInstance(1960, 2, 17)
        );                

        insert new Pricebookentry[]{plistPBE1, customerPBE1, customerPBE2, plistPBE2, customerPBE3, customerPBE4};

        // Populate custom settings before user creation
        PavilionSettings__c pavillionCommunityProfileId1 = new PavilionSettings__c(
            Name = 'PavillionCommunityProfileId',
            Value__c = portalProfile.Id
        );

        PavilionSettings__c pavillionCommunityProfileId2 = new PavilionSettings__c(
            Name = 'PavilionPublicGroupID',
            Value__c = portalProfile.Id
        );      

        insert new PavilionSettings__c[]{pavillionCommunityProfileId1, pavillionCommunityProfileId2};           
    }

    @isTest
    private static void testCheckItemPricingMatchCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];
        testCheckItemPricingCADPriceChange(testUser, true, true, 'AM10003', true, 1, null);
    }  

    @isTest
    private static void testCheckItemPricingMatchNonCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];
        testCheckItemPricingCADPriceChange(testUser, false, true, 'AM10003', true, 1, null);
    }      

    @isTest
    private static void testCheckItemPricingNoMatchNonCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];
        testCheckItemPricingCADPriceChange(testUser, false, true, 'AM10009', false, 0, null);
    }  

    @isTest
    private static void testCheckItemPricingNoMatchCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];
        testCheckItemPricingCADPriceChange(testUser, true, true, 'AM10009', false, 0, null);
    }      

    @isTest
    private static void testCheckItemPricingMatchWithReplacementCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];
        CC_Product_Rule__c rule = [Select CC_Replacement_Item__c From CC_Product_Rule__c Where CC_Type__c = 'Replace' and CC_Obsolete_Item__r.ProductCode = 'AM10004'];
        testCheckItemPricingCADPriceChange(testUser, true, true, 'AM10004', true, 1, rule.CC_Replacement_Item__c);
    }         

    private static void testCheckItemPricingCADPriceChange(User testUser, Boolean cadUser, Boolean priceChangeInPast, String partNumber, Boolean matchExpected, Integer rulesExpected, Id replacementProdId){

        Account acct = [Select Id, Name, CC_Price_List__c From Account Where id = :testUser.AccountId];
        PriceBookEntry[] customerPbes = [Select Id, Product2.ProductCode, Product2.Name, CC_UM__c, UnitPrice, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c,CC_Quantity_Discount__c,
            				                CC_New_UnitPrice__c, CC_New_CAD_Unit_Price__c, CC_New_Quantity_Price__c, CC_New_CAD_Quantity_Price__c,CC_Price_Change_Date__c, CC_Code__c,
            					            Product2.CC_Inventory_Count__c
                                         From PriceBookEntry Where PriceBook2.Name = :acct.CC_Price_List__c
                                         Order By Product2.Name ASC];

        PriceBookEntry[] plistPbes = [Select Id, Product2.ProductCode, Product2.Name, CC_UM__c, UnitPrice, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c,CC_Quantity_Discount__c,
            				            CC_New_UnitPrice__c, CC_New_CAD_Unit_Price__c, CC_New_Quantity_Price__c, CC_New_CAD_Quantity_Price__c,CC_Price_Change_Date__c, CC_Code__c,
            					        Product2.CC_Inventory_Count__c
                                      From PriceBookEntry Where PriceBook2.Name = 'PLIST'
                                      Order By Product2.Name ASC];                                 
    
        System.runAs (testUser) {

            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            CC_PavilionDownloadPricingCtrl.ProductSearchResult result = CC_PavilionDownloadPricingCtrl.checkItemPricing(partNumber);
        
            system.assertEquals(matchExpected ? 1 : 0, result.pbes.size());
            system.assertEquals(rulesExpected, result.rules.size());

            if (matchExpected){

                CC_PavilionDownloadPricingCtrl.PricebookData pbd = result.pbes[0];
                PriceBookEntry customerPbe, plistPbe;
                
                if (replacementProdId == null){
                    customerPbe = pbd.productName == 'Test1' ? customerPbes[0] : customerPbes[1];
                    plistPbe = pbd.productName == 'Test1' ? plistPbes[0] : plistPbes[1];
                }
                else{
                    customerPbe = customerPbes[0];
                    plistPbe = plistPbes[0];
                }
                             
                system.assertEquals(replacementProdId == null ? customerPbe.Product2Id : replacementProdId, pbd.productId);
                system.assertEquals(customerPbe.Product2.ProductCode, pbd.productCode);

                if (priceChangeInPast){

                    system.assertEquals(cadUser ? plistPbe.CC_CAD_Unit_Price__c : plistPbe.UnitPrice, pbd.listPrice);
                    system.assertEquals(cadUser ? customerPbe.CC_CAD_Unit_Price__c : customerPbe.UnitPrice, pbd.discountPrice);
                    system.assertEquals(customerPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                    system.assertEquals(cadUser ? customerPbe.CC_CAD_Quantity_Price__c : customerPbe.CC_Quantity_Price__c, pbd.qtyPrice);
                    system.assertEquals(customerPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                }
                else {

                    system.assertEquals(cadUser ? plistPbe.CC_New_CAD_Unit_Price__c : plistPbe.CC_New_UnitPrice__c, pbd.listPrice);
                    system.assertEquals(cadUser ? customerPbe.CC_New_CAD_Unit_Price__c : customerPbe.CC_New_UnitPrice__c, pbd.discountPrice);
                    system.assertEquals(customerPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                    system.assertEquals(cadUser ? customerPbe.CC_New_CAD_Quantity_Price__c : customerPbe.CC_New_Quantity_Price__c, pbd.qtyPrice);
                    system.assertEquals(customerPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                }                
            }   
        }
    }    

    @isTest
    private static void testGetOrderItemsFromPriceBookEntryCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];
        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, true, true, false);
    }

    @isTest
    private static void testGetOrderItemsFromPriceBookEntryNonCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];
        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, false, true, false);
    }    

    @isTest
    private static void testGetOrderItemsFromPriceBookEntryCADPriceChangeInFuture() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];

        PriceBookEntry[] pbes = [Select Id From PriceBookEntry];

        for (PriceBookEntry pbe : pbes){
            pbe.CC_Price_Change_Date__c = Date.today().addDays(10);
        }

        update pbes;

        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, true, false, false);
    }

    @isTest
    private static void testGetOrderItemsFromPriceBookEntryNonCADPriceChangeInFuture() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];

        PriceBookEntry[] pbes = [Select Id From PriceBookEntry];

        for (PriceBookEntry pbe : pbes){
            pbe.CC_Price_Change_Date__c = Date.today().addDays(10);
        }

        update pbes;

        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, false, false, false);
    }
    
    @isTest
    private static void testGetOrderItemsFromPLISTCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];
        deactivateCustomerPriceBookEntries(testUser.AccountId);       
        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, true, true, true);
    }

    @isTest
    private static void testGetOrderItemsFromPLISTNonCADPriceChangeInPast() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];
        deactivateCustomerPriceBookEntries(testUser.AccountId);
        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, false, true, true);
    }    

    @isTest
    private static void testGetOrderItemsFromPLISTCADPriceChangeInFuture() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar1@test.ingersollrand.com'];

        PriceBookEntry[] pbes = [Select Id From PriceBookEntry];

        for (PriceBookEntry pbe : pbes){
            pbe.CC_Price_Change_Date__c = Date.today().addDays(10);
        }

        update pbes;
        
        deactivateCustomerPriceBookEntries(testUser.AccountId);

        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, true, false, true);
    }

    @isTest
    private static void testGetOrderItemsFromPLISTNonCADPriceChangeInFuture() {

        User testUser = [Select Id, AccountId From User Where Username Like '%test12345Clubcar2@test.ingersollrand.com'];

        PriceBookEntry[] pbes = [Select Id From PriceBookEntry];

        for (PriceBookEntry pbe : pbes){
            pbe.CC_Price_Change_Date__c = Date.today().addDays(10);            
        }

        update pbes;
        
        deactivateCustomerPriceBookEntries(testUser.AccountId);

        testGetOrderItemsFromPriceBookEntryPriceChange(testUser, false, false, true);
    }
    
    //deactivate customer pricebook entries to for use of PLIST entries for pricing
    private static void deactivateCustomerPriceBookEntries(string accId){
        Account a = [SELECT id, CC_Price_List__c FROM Account WHERE Id = :accId];
        PriceBook2 pb = [SELECT id, Name FROM PriceBook2 WHERE Name = :a.CC_Price_List__c];
        List<PriceBookEntry> pbes = [SELECT id, IsActive FROM PriceBookEntry WHERE PriceBook2Id = :pb.id];
        for (PriceBookEntry pbe : pbes){
            pbe.IsActive = false;            
        }
        
        update pbes;
    }

    private static void testGetOrderItemsFromPriceBookEntryPriceChange(User testUser, Boolean cadUser, Boolean priceChangeInPast, Boolean defaultEntries){

        Account acct = [Select Id, Name, CC_Price_List__c From Account Where id = :testUser.AccountId];
        PriceBookEntry[] customerPbes = [Select Id, Product2.ProductCode, Product2.Name, CC_UM__c, UnitPrice, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c,CC_Quantity_Discount__c,
            				                CC_New_UnitPrice__c, CC_New_CAD_Unit_Price__c, CC_New_Quantity_Price__c, CC_New_CAD_Quantity_Price__c,CC_Price_Change_Date__c, CC_Code__c,
            					            Product2.CC_Inventory_Count__c
                                         From PriceBookEntry Where PriceBook2.Name = :acct.CC_Price_List__c
                                         Order By Product2.Name ASC];

        PriceBookEntry[] plistPbes = [Select Id, Product2.ProductCode, Product2.Name, CC_UM__c, UnitPrice, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c,CC_Quantity_Discount__c,
            				            CC_New_UnitPrice__c, CC_New_CAD_Unit_Price__c, CC_New_Quantity_Price__c, CC_New_CAD_Quantity_Price__c,CC_Price_Change_Date__c, CC_Code__c,
            					        Product2.CC_Inventory_Count__c
                                      From PriceBookEntry Where PriceBook2.Name = 'PLIST'
                                      Order By Product2.Name ASC];                                 
    
        System.runAs (testUser) {

            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Map<String, List<CC_PavilionDownloadPricingCtrl.PricebookData>> results = CC_PavilionDownloadPricingCtrl.getOrderItemsFromPriceBookEntry('');
        
            system.assertEquals(1, results.size());
            system.assertEquals(2, results.values()[0].size());

            Integer productCheckCount = 0;
            
            for (CC_PavilionDownloadPricingCtrl.PricebookData pbd : results.values()[0]){
                
                if (pbd.productName == 'Test1' || pbd.productName == 'Test2'){
                    productCheckCount++;
                }
                
                PriceBookEntry customerPbe = pbd.productName == 'Test1' ? customerPbes[0] : customerPbes[1];
                PriceBookEntry plistPbe = pbd.productName == 'Test1' ? plistPbes[0] : plistPbes[1];
                
                system.assertEquals(customerPbe.Product2Id, pbd.productId);
                system.assertEquals(customerPbe.Product2.ProductCode, pbd.productCode);
				
                if(defaultEntries){//test using default PLIST pricebook entries
                    if (priceChangeInPast){
                        system.assertEquals(cadUser ? plistPbe.CC_CAD_Unit_Price__c : plistPbe.UnitPrice, pbd.listPrice);
                        system.assertEquals(cadUser ? plistPbe.CC_CAD_Unit_Price__c : plistPbe.UnitPrice, pbd.discountPrice);
                        system.assertEquals(plistPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                        system.assertEquals(cadUser ? plistPbe.CC_CAD_Quantity_Price__c : plistPbe.CC_Quantity_Price__c, pbd.qtyPrice);
                        system.assertEquals(plistPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                    }
                    else {
    
                        system.assertEquals(cadUser ? plistPbe.CC_New_CAD_Unit_Price__c : plistPbe.CC_New_UnitPrice__c, pbd.listPrice);
                        system.assertEquals(cadUser ? plistPbe.CC_New_CAD_Unit_Price__c : plistPbe.CC_New_UnitPrice__c, pbd.discountPrice);
                        system.assertEquals(plistPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                        system.assertEquals(cadUser ? plistPbe.CC_New_CAD_Quantity_Price__c : plistPbe.CC_New_Quantity_Price__c, pbd.qtyPrice);
                        system.assertEquals(plistPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                    }      
                }
                else{//tests using entries in customer pricebook
                   if (priceChangeInPast){
                        system.assertEquals(cadUser ? plistPbe.CC_CAD_Unit_Price__c : plistPbe.UnitPrice, pbd.listPrice);
                        system.assertEquals(cadUser ? customerPbe.CC_CAD_Unit_Price__c : customerPbe.UnitPrice, pbd.discountPrice);
                        system.assertEquals(customerPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                        system.assertEquals(cadUser ? customerPbe.CC_CAD_Quantity_Price__c : customerPbe.CC_Quantity_Price__c, pbd.qtyPrice);
                        system.assertEquals(customerPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                    }
                    else {
    
                        system.assertEquals(cadUser ? plistPbe.CC_New_CAD_Unit_Price__c : plistPbe.CC_New_UnitPrice__c, pbd.listPrice);
                        system.assertEquals(cadUser ? customerPbe.CC_New_CAD_Unit_Price__c : customerPbe.CC_New_UnitPrice__c, pbd.discountPrice);
                        system.assertEquals(customerPbe.CC_Quantity_Discount__c, pbd.qtyDiscount);
                        system.assertEquals(cadUser ? customerPbe.CC_New_CAD_Quantity_Price__c : customerPbe.CC_New_Quantity_Price__c, pbd.qtyPrice);
                        system.assertEquals(customerPbe.Product2.CC_Inventory_Count__c, pbd.inventory);
                    } 
                }                				             
            }

            system.assertEquals(2, productCheckCount);
        }    
    }
}