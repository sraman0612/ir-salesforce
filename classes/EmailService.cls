public with sharing class EmailService {
	
	@TestVisible
	private static Boolean emailDeliverabilityEnabled = true;
	
	@TestVisible
	private static Integer emailsSent = 0;

    // Should provide either a templateId or textBody/htmlBody and a targetObjectId and/or toAddresses
    public static Messaging.SingleEmailMessage buildSingleEmailMessage(
        Id targetObjectId,
        String[] toAddresses, // Can be either email addresses or record Ids (i.e. lead/contact/user Id)
        String[] ccAddresses, // Can be either email addresses or record Ids (i.e. lead/contact/user Id)
        Id templateId,
      	String subject,
      	String textBody, 
      	String htmlBody,
        Id whatId, // Optional, Id of target record used for templates and merge fields
        String oweaId //Optional, org wide email Id to be used as the from address
    ){
    
		Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();   
        
        if (targetObjectId != null){
        	
        	msg.setTargetObjectId(targetObjectId);
        	
        	if (targetObjectId.getSObjectType() == User.getSObjectType()){
        		msg.setSaveAsActivity(false);
        	}
        }        
        
        if (toAddresses != null && toAddresses.size() > 0){
        	msg.setToAddresses(toAddresses);
        }   
        
        if (ccAddresses != null && ccAddresses.size() > 0){
        	msg.setCCAddresses(ccAddresses);
        }        
        
        msg.setSubject(subject);
    
        if (templateId != null){
            msg.setTemplateId(templateId);
        }
        else{    
            
            if (String.isNotBlank(textBody)){
                msg.setPlainTextBody(textBody);
            }
                
            if (String.isNotBlank(htmlBody)){
                msg.setHtmlBody(htmlBody);
            }   
        }
        
        if (whatId != null){
        	
        	msg.setWhatId(whatId);
        	
        	if (whatId.getSObjectType() == Lead.getSObjectType()){
        		msg.setSaveAsActivity(false);
        	}        	
        }      
        
        if (oweaId != null) {
          msg.setOrgWideEmailAddressId(oweaId);
        }
        
        return msg;
    }

    public static void sendSingleEmailMessages(Messaging.SingleEmailMessage[] messages){
        sendSingleEmailMessagesWithResults(messages);
    }

    public class SendSingleEmailMessagesResult{
        public String systemicErrorMessage = '';
        public Messaging.SendEmailResult[] sendResults = new Messaging.SendEmailResult[]{};        
    }
    
    public static SendSingleEmailMessagesResult sendSingleEmailMessagesWithResults(Messaging.SingleEmailMessage[] messages){
        
        SendSingleEmailMessagesResult result = new SendSingleEmailMessagesResult();

        try{
        	
            system.debug('reserving (' + messages.size() + ') email messages');
        	Messaging.reserveSingleEmailCapacity(messages.size());	
            
            system.debug('--sending email messages--');
            result.sendResults = Messaging.sendEmail(messages, false);
            
            List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
            
            for (Messaging.SendEmailResult sendResult : result.sendResults){
            	
            	if (sendResult.isSuccess()){
            		emailsSent++;
            	}
            	else if (sendResult.getErrors().size() > 0){
            		
            		system.debug('send error: ' + sendResult.getErrors()[0].getMessage());
            		apexLogHandler logHandler = new apexLogHandler('EmailService', 'sendSingleEmailMessages', sendResult.getErrors()[0].getMessage());
                	apexLogs.add(logHandler.logObj);           		
            	}
            }
            
        	system.debug('errors to log: ' + apexLogs.size());
        
	        if (apexLogs.size() > 0){
	            insert apexLogs;
	        }             
        }
        catch (Exception e){
        	
            system.debug('exception sending emails: ' + e.getMessage());
            
            if (e.getMessage().contains('organization is not permitted to send email')){
            	emailDeliverabilityEnabled = false;
            }
            else{
	            apexLogHandler logHandler = new apexLogHandler('EmailService', 'sendSingleMessages', e.getMessage());
            	insert logHandler.logObj;
            }

            result.systemicErrorMessage = e.getMessage();
        }
        
        return result;
    }    
}