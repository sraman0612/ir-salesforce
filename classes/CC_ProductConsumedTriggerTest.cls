@isTest
public with sharing class CC_ProductConsumedTriggerTest {

    @TestSetup
    private static void createData(){

        Pricebook2 pb = TestUtilityClass.createPriceBook2();
        pb.Name = 'Test PB12345';
        insert pb;      

        Account a = TestUtilityClass.createAccount();
        a.ShippingCountry = 'USA';
        a.CC_Customer_Number__c = '111111111';
        a.Name = 'Test Customer1';
        a.CC_Price_List__c = 'Test PB12345';
        insert a;   

        CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
        hcs.FS_No_Charge_Acct_Id__c = a.Id;
        hcs.FS_No_Charge_Pricebook_ID__c = pb.Id;
        upsert hcs;        

        OperatingHours oh  = new OperatingHours(Name = 'Test');
        insert oh;        
        
        ServiceTerritory st = new ServiceTerritory(
            isActive = true,
            Name = 'TestSvcTerr',
            OperatingHoursId = oh.Id
        );
        
        insert st;           

        WorkOrder wo1 = TestUtilityClass.createWorkOrder(a.id, 'CC_Demo');
        wo1.Work_Order_status__c = 'Work Completed';
        wo1.SLAM_Report__c = 'Completed';
        wo1.Subject = 'Test1';
        wo1.ServiceTerritoryId = st.Id;
        wo1.Billing_Account__c = a.Id;
        wo1.Pricebook2Id = pb.Id;

        WorkOrder wo2 = wo1.clone(false, true);
        wo2.Subject = 'Test2';

        insert new WorkOrder[]{wo1, wo2};
        
        Schema.Location loc = new Schema.Location(
            Name = 'TestLoc1',    
            IsInventoryLocation = true,
            IsMobile = true,
            LocationType = 'Van',                        
            TimeZone = 'America/New_York'
        );
        
        insert loc;

        Product2 prod = new Product2(
            Name = 'Test FSL Product', 
            Family = 'Club Car Parts'
        );

        insert prod;

        PricebookEntry pbe = new PricebookEntry(
            Product2Id = prod.Id,
            Pricebook2Id = pb.Id,
            UnitPrice = 50,
            isActive = true
        );

        insert pbe;

        system.debug('***pbe: ' + [Select Product2Id, Pricebook2.Name, UnitPrice, isActive From PricebookEntry Where Id = :pbe.Id]);

        ProductItem pi = new ProductItem(
            LocationId = loc.Id, 
            Product2Id = prod.Id, 
            QuantityOnHand = 15
        );

        insert pi;   
        
        WorkOrderLineItem woli1 = new WorkOrderLineItem(
            WorkOrderId = wo1.Id, 
            Quantity = 5,
            UnitPrice = 1,
            Part__c = pi.Id,
            PricebookEntryid = pbe.Id
        );

        WorkOrderLineItem woli2 = woli1.clone(false, true);
        woli2.Quantity = 5;  

        WorkOrderLineItem woli3 = woli1.clone(false, true);
        woli3.WorkOrderId = wo2.Id;
        woli3.Quantity = 1;   
        
        WorkOrderLineItem[] wolis = new WorkOrderLineItem[]{woli1, woli2, woli3};

        insert wolis;
    }

    @isTest
    private static void testUpdateWorkOrderLineItems(){

        ProductItem pi = [Select Id From ProductItem Where Product2.Name = 'Test FSL Product'];
        WorkOrder wo1 = [Select Id From WorkOrder Where Subject = 'Test1'];
        WorkOrder wo2 = [Select Id From WorkOrder Where Subject = 'Test2'];
        WorkOrderLineItem[] wo1lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo1.Id];
        WorkOrderLineItem[] wo2lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo2.Id];
        system.assertEquals(2, wo1lis.size());
        system.assertEquals(1, wo2lis.size());

        Test.startTest();

        ProductConsumed pc1 = new ProductConsumed();
        pc1.WorkOrderId = wo1.Id;
        pc1.WorkOrderLineItemId = wo1lis[0].Id;
        pc1.QuantityConsumed = 1;
        pc1.ProductItemId = pi.Id;

        ProductConsumed pc2 = pc1.clone(false, true);
        pc2.QuantityConsumed = 2;

        ProductConsumed pc3 = pc1.clone(false, true);
        pc3.QuantityConsumed = 1;    
        pc3.WorkOrderLineItemId = wo1lis[1].Id;
        
        ProductConsumed pc4 = pc1.clone(false, true);
        pc4.QuantityConsumed = 1;    
        pc4.WorkOrderId = wo2.Id;
        pc4.WorkOrderLineItemId = wo2lis[0].Id;    
        
        insert new ProductConsumed[]{pc1, pc2, pc3, pc4};

        wo1lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo1.Id];
        wo2lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo2.Id];

        system.assertEquals(3, wo1lis[0].Quantity_Consumed__c);
        system.assertEquals(1, wo1lis[1].Quantity_Consumed__c);
        system.assertEquals(1, wo2lis[0].Quantity_Consumed__c);

        pc1.WorkOrderLineItemId = wo1lis[1].Id;
        update pc1;

        wo1lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo1.Id];
        wo2lis = [Select Id, Quantity, Quantity_Consumed__c From WorkOrderLineItem Where WorkOrderId = :wo2.Id];

        system.assertEquals(2, wo1lis[0].Quantity_Consumed__c);
        system.assertEquals(2, wo1lis[1].Quantity_Consumed__c);
        system.assertEquals(1, wo2lis[0].Quantity_Consumed__c);        

        Test.stopTest();
    }
}