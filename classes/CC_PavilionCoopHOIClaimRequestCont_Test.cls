@isTest
public class CC_PavilionCoopHOIClaimRequestCont_Test {
    static testmethod void UnitTest_CoopHOIClaimRequest()
    {
        // Creation of test data
        Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        Map<String,id> profMap = new Map<String,id>();
        list<Profile> profid = [SELECT id, Name FROM Profile];
        for(Profile p:profid){
            profMap.put(p.Name,p.id);
        }
        
        PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',sampleAccount.id);
        insert ps;
        
        PavilionSettings__c pva1 = new PavilionSettings__c(name='ClubCarCommunityLoginProfileId', Value__c = profMap.get('CC_CommunityLoginUser'));
        insert pva1;
        
        PavilionSettings__c pva2 = new PavilionSettings__c(name='PavillionCommunityProfileId', Value__c = profMap.get('CC_PavilionCommunityUser'));
        insert pva2;
        
        Pavilion_Navigation_Profile__c pnp = TestUtilityClass.createPavilionNavigationProfile('ccircotst');
        insert pnp;
        
        User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profMap.get('CC_System Administrator'),Alias = 'test1236',
                            Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                            TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst');
        insert testUser;
        test.startTest();
        System.runAs (testUser) {
        //Account newSampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        //insert newSampleAccount;
  
        Contract sampleContract = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer',sampleAccount.id);
        insert sampleContract;
        System.assertEquals(sampleAccount.Id, sampleContract.AccountId);
        System.assertNotEquals(null, sampleContract.Id);
        CC_Hole_in_One__c sampleCoopRequest = TestUtilityClass.createCoopRequest(sampleContract.Id);
        insert sampleCoopRequest;        
        Hole_In_One_Claim__c firstHoleInOneClaim = TestUtilityClass.createHoleInOneClaim(sampleCoopRequest.Id);
        insert firstHoleInOneClaim;
        CC_PavilionCoopHOIClaimRequestController coopHOIClaimRequestController =new CC_PavilionCoopHOIClaimRequestController();
        
        CC_PavilionCoopHOIClaimRequestController.getCoopHOIClaimRequestJson(sampleAccount.id);
        //CC_PavilionCoopHOIClaimRequestController.getCoopHOIClaimRequestJson(newSampleAccount.id);
        CC_PavilionCoopHOIClaimRequestController.submitCoopClaimData(firstHoleInOneClaim.id, 'john', '8785476612', 'Smith', '7894581234', 'Smith', '4567894321', 'Mark', '65789423122', 'New Coop', '8975346129', '123456', '123', '10000', 'none', 'attachment for new coop', 'a new coop is generated');
        //CC_PavilionCoopHOIClaimRequestController.submitCoopClaimData('', 'john', '8785476612', 'Smith', '7894581234', 'Smith', '4567894321', 'Mark', '65789423122', 'New Coop', '8975346129', '123456', '123', '10000', 'none', 'attachment for new coop', 'a new coop is generated');
        CC_PavilionCoopHOIClaimRequestController.getClaimByIdJson(firstHoleInOneClaim.id);
        //CC_PavilionCoopHOIClaimRequestController.getClaimByIdJson('');
        test.stopTest();
        }
    }
    
}