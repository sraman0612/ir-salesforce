@isTest
public class CC_PavilionInventoryCntTest {
  static testmethod void UnitTest_CC_PavilionInventoryCnt(){
    Account a = TestUtilityClass.createAccount();
    insert a;
    Contract c=TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','LSV Transportation',a.id);
    insert c;
    ProductLineToMarketType__c pLM=new ProductLineToMarketType__c(Name='TestPLM',Agreement_Type__c='XRT Utility');
    insert pLM;
    CC_Vehicle__c veh = TestUtilityClass.createVehicle('veh');
    insert veh;
    CC_Vehicle__c v2 = TestUtilityClass.createVehicle('veh');
    insert v2;
    CC_My_Team__c mteam = TestUtilityClass.createMyTeam('Utility Sales Representative',a.Id,'Sales');
    mteam.Custom_Email__c='clubc@r.com';
    insert mteam;
    PavilionSettings__c ps = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',a.id);
    insert ps;
    Pavilion_Navigation_Profile__c pnp = TestUtilityClass.createPavilionNavigationProfile('ccircotst');
    insert pnp;
    Id profileID = [SELECT Id FROM Profile WHERE Name ='CC_System Administrator' Limit 1].Id;
    User testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = profileID,Alias = 'test1236',
                        Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                        TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst');
    insert testUser;
    test.startTest();
    System.runAs (testUser) {
      CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
      CC_PavilionInventoryCnt InvCnt =new CC_PavilionInventoryCnt(controller);
      CC_PavilionInventoryCnt.getLibraryData('Veh');
      ApexPages.currentPage().getParameters().put('vehicleid',veh.Id);
    }
    test.stopTest();
  }
}