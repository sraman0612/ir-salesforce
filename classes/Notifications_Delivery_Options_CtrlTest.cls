@isTest
public with sharing class Notifications_Delivery_Options_CtrlTest {

    @testSetup
    private static void createData(){
        UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account acc = CTS_TestUtility.createAccount('Test Account', true);   

        Contact communityContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false);
        communityContact.CTS_IOT_Community_Status__c = 'Approved';
        communityContact.MobilePhone = '555-555-5555';
        insert communityContact;

        Id profileId = [SELECT Id FROM Profile WHERE Name = :settings.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(communityContact.Id, profileId, false);    
        communityUser.LastName = 'Comm_User_Test1';
        insert communityUser;    
    }
}
    @isTest
    private static void initTest(){

        Test.startTest();

        Notifications_Delivery_Options_Ctrl.InitResponse response = Notifications_Delivery_Options_Ctrl.getInit();
        system.assertEquals(Schema.SObjectType.User.fields.TimeZoneSidKey.picklistValues.size(), response.timeZoneOptions.size());

        Test.stopTest();
    }

    @isTest
    private static void updatePreferencesSuccess(){

        User u = [Select Id, ContactId, TimezoneSidKey, MobilePhone, Email From User Where LastName = 'Comm_User_Test1'];
        Contact contact = [Select Id, CTS_Notifications_Delivery_Window_Start__c, CTS_Notifications_Delivery_Window_End__c From Contact Where Id = :u.ContactId];

        system.assertEquals(null, contact.CTS_Notifications_Delivery_Window_Start__c);
        system.assertEquals(null, contact.CTS_Notifications_Delivery_Window_End__c);

        Test.startTest();

        System.runAs(u){

            LightningResponseBase response = Notifications_Delivery_Options_Ctrl.updatePreferences(
                contact.Id, 
                u.Id,
                null, 
                '01:15:00:00', 
                'Asia/Dubai', 
                '111-555-5555', 
                'abc123@gmail.com'
            );

            system.assertEquals(true, response.success);
        }

        u = [Select Id, ContactId, TimezoneSidKey, MobilePhone, Email From User Where LastName = 'Comm_User_Test1'];
        contact = [Select Id, CTS_Notifications_Delivery_Window_Start__c, CTS_Notifications_Delivery_Window_End__c From Contact Where Id = :u.ContactId];        

        system.assertEquals('Asia/Dubai', u.TimezoneSidKey);
        system.assertEquals('111-555-5555', u.MobilePhone);
        system.assertEquals('abc123@gmail.com', u.Email);

        system.assertEquals(null, contact.CTS_Notifications_Delivery_Window_Start__c);
        system.assertEquals(1, contact.CTS_Notifications_Delivery_Window_End__c.hour());
        system.assertEquals(15, contact.CTS_Notifications_Delivery_Window_End__c.minute());

        Test.stopTest();
    }    

    @isTest
    private static void updatePreferencesFail(){

        User u = [Select Id, ContactId, TimezoneSidKey, MobilePhone, Email From User Where LastName = 'Comm_User_Test1'];
        Contact contact = [Select Id, CTS_Notifications_Delivery_Window_Start__c, CTS_Notifications_Delivery_Window_End__c From Contact Where Id = :u.ContactId];

        system.assertEquals(null, contact.CTS_Notifications_Delivery_Window_Start__c);
        system.assertEquals(null, contact.CTS_Notifications_Delivery_Window_End__c);

        Test.startTest();

        System.runAs(u){

            LightningResponseBase response = Notifications_Delivery_Options_Ctrl.updatePreferences(
                contact.Id, 
                u.Id,
                null, 
                'BOGUS TIME FORMAT!', 
                'Asia/Dubai', 
                '111-555-5555', 
                'abc123@gmail.com'
            );

            system.assertEquals(false, response.success);
            system.assert(String.isNotBlank(response.errorMessage));
        }

        Test.stopTest();
    }     
}