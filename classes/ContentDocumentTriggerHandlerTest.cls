@isTest
public class ContentDocumentTriggerHandlerTest {
  
  @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
    }
    
    
    static testmethod void UnitTest_ContentDocumentTriggerHandler()
    {
        //Creation Of Test Data
        Account a = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        a.CC_Account_Status__c = 'Active' ;
        insert a;
        Id conrecId1 =[SELECT Id FROM RecordType where SobjectType='Contact' and DeveloperName='Compressed_Air' limit 1].id;
        Contact ct =new Contact(FirstName ='Sample',Phone ='88888888',LastName ='Contact',Email = 'testContact@test.ingersollrand.com',AccountId=a.id,CC_Type__c='All Agreements');
        insert ct;
        Contract cnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','LSV Commercial', a.id);
        cnt.CC_Contract_Status__c = 'Current';
        cnt.CC_Contract_End_Date__c = system.today()+10;
        insert cnt;
        // PavilionSettings__c records are created to handle User Events trigger

        Group grp = TestUtilityClass.createCustomGroup('Sample Group');
        insert grp;
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, ct.Id);
        u.CC_View_Parts_Bulletins__c =True;
        u.CC_View_Parts_Pricing__c =True;
        u.CC_View_Sales_Bulletins__c =True;
        u.CC_View_Sales_Pricing__c =True;
        u.CC_View_Service_Bulletins__c =True;
        insert u;
        Test.StartTest();
        ContentVersion contentVersion_first = TestUtilityClass.createCustomContentVersion('bulletin',';Japan;United States;Canada',';CAD;USD;YEN',';LSV Commercial;XRT Utility;Commercial Utility');
        contentVersion_first.CC_Bulletin_Type__c = 'Sales';
        insert contentVersion_first;
        Map<id,ContentDocument> newContentDocumentMap = new Map<id,ContentDocument>();
        Map<id,ContentDocument> newContentDocumentMap1 = new Map<id,ContentDocument>();
        List<ContentDocument> documents = [SELECT Id FROM ContentDocument];
        
        for(ContentDocument c:documents)
        {
            newContentDocumentMap.put(c.id, c);
        }
        ContentDocumentTriggerHandler.sendEmailForNewContDoc(documents);
        delete documents;
        undelete documents;
        
        ContentVersion contentVersion_third = TestUtilityClass.createCustomContentVersion('bulletin','United States','USD','LSV Commercial');
        contentVersion_third.CC_Bulletin_Type__c = 'Parts';
        insert contentVersion_third;
        
        ContentVersion contentVersion_fifth = TestUtilityClass.createCustomContentVersion('bulletin','EMEA','GBP With Duty',';LSV Commercial;XRT Utility;Commercial Utility');
        contentVersion_fifth.CC_Bulletin_Type__c = 'Parts Pricing';
        insert contentVersion_fifth;
        
        ContentVersion contentVersion_seventh = TestUtilityClass.createCustomContentVersion('bulletin','Canada','CAD','XRT Utility');
        contentVersion_seventh.CC_Bulletin_Type__c = 'Sales Pricing';
        insert contentVersion_seventh;
        
        ContentVersion contentVersion_nineth = TestUtilityClass.createCustomContentVersion('bulletin','United States','USD','Commercial Utility');
        contentVersion_nineth.CC_Bulletin_Type__c = 'Service & Warranty';
        insert contentVersion_nineth;
        
        List<ContentDocument> newdocuments = [SELECT Id FROM ContentDocument];
        for(ContentDocument c:newdocuments)
        {
            newContentDocumentMap1.put(c.id, c);
        }
        ContentDocumentTriggerHandler.sendEmailForNewContDoc(newdocuments);
        delete newdocuments;
        undelete newdocuments;
        Test.stopTest();
         
    }
}