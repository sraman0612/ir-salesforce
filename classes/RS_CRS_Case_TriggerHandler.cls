/*****************************************************************
Name:      RS_CRS_Case_TriggerHandler
Purpose:   Class used to store previous record owner 
			and used to revert record type on reopen case.
If Contact is related to Case.
History                                                            
-------                                                            
VERSION		AUTHOR			DATE		DETAIL
1.0      	Neela Prasad	02/27/2018	INITIAL DEVELOPMENT
*****************************************************************/
public class RS_CRS_Case_TriggerHandler {
    
    /******************************************************************************
	Purpose            :  Method used to Check Contact has Case                                              
	Parameters         :  Contact object
	Returns            :  void
	Throws [Exceptions]:                                                          
	******************************************************************************/ 
    public static void getPreviousOwner(List<case> caseList){
        //To make sure this Class is triggering for RS_CRS Record Types only.
        Id newCaseRecordTypeId		= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        Id fsrCaseRecordTypeId		= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_FSR_Case_Record_Type);
        Id psiCaseRecordTypeId		= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_PSI_Case_Record_Type);
        Id closeCaseRecordTypeId	= RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Close_Case_Record_Type);
        
        try{
            for( Case newCase : caseList) 
            {
                if(newCase.RecordTypeId == newCaseRecordTypeId  || newCase.RecordTypeId == psiCaseRecordTypeId 
                   || newCase.RecordTypeId == fsrCaseRecordTypeId  || newCase.RecordTypeId == closeCaseRecordTypeId )
                {
                    Case oldCase = (Case)Trigger.oldMap.get(newCase.Id);
                    
                    if(oldCase.OwnerId != newCase.OwnerId && 
                       (!string.ValueOf(oldCase.OwnerId).startsWith( '00G' )) && 
                       (string.ValueOf(newCase.OwnerId)).startsWith( '005' ) )
                    {  
                        newCase.RS_CRS_Previous_Owner__c = oldCase.OwnerId;
                    }
                    
                    //used to set previous record type on reopen
                    if(oldCase.status == 'Closed' && 
                       (newCase.status == 'Open' || newCase.status == 'Reopen'))
                    {
                        newCase.RecordTypeId = newCase.RS_CRS_Previous_Record_Type_Id__c;
                    }
                }
            }        
        }catch (Exception ex){
            System.debug('Error while Updating Previous owner \'RS_CRS_Case_TriggerHandler\': \n'+ ex.getMessage()+ ' at line# ' +ex.getLineNumber());
        }
    }
}