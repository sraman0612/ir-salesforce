public with sharing class MRAssignmentGroupExtract{
    
    @AuraEnabled(cacheable=true)
	public static List<Assignment_Groups__c> getChannelPartnerInformation(String GroupName,String searchKey){
        if(String.isNotBlank(searchKey) && String.isNotBlank(GroupName)){
            
           String DistributorLabel = System.Label.MR_Distributor_Trio;
           String RSMLabel = System.Label.RSM_Group_Name;
           String RSMLabelMPOB = System.Label.MP_OB_IRP_RSM_Group_Name;
           String DistributorLabelMPOB = System.Label.MP_OB_IRP_Distributor_Territory;
           String MRDistributorLabel = System.Label.MR_Distributor_Territory;
           String MRRSMLabel = System.Label.MR_RSM_Territory;
           String ARODistributorLabel = System.Label.ARO_Distributor_Territory;
           String ARORSMLabel = System.Label.ARO_RSM_Territory;

           String query = 'SELECT Id,Account_Name__c,Distributor_Contact_Name__c,RSM_Name__c,Distributor_Email__c,Distributor_Name__c,Group_Name__r.Name, Active__c, County__c, State__c, User__c,Industry__c, Country__c, Brand__c,DISTID__c,RSMID__c,GroupName__c,Queue_Name__c '+
                           'FROM Assignment_Groups__c WHERE Active__c = \'True\'';
            
            if(GroupName == MRDistributorLabel || GroupName == ARODistributorLabel){
                query +=' AND GroupName__c = \''+GroupName +'\' AND Account_Name__c LIKE \'%'+ searchKey+'%\'';
               
                
            }
            else if(GroupName == MRRSMLabel || GroupName == ARORSMLabel){
                query +=' AND GroupName__c = \''+GroupName +'\' AND RSM_Name__c LIKE \'%'+ searchKey+'%\'';
            }

            else if(GroupName == RSMLabelMPOB){
                query +=' AND GroupName__c = \''+GroupName +'\' AND (RSM_Name__c LIKE \'%'+ searchKey+'%\' OR Queue_Name__c LIKE \'%'+ searchKey+'%\')';
                
            }
            else if(GroupName == DistributorLabelMPOB){
                query +=' AND GroupName__c = \''+GroupName +'\' AND Account_Name__c LIKE \'%'+ searchKey+'%\'';
                
            }
      		
             
        
             List<Assignment_Groups__c> DistributorList = Database.query(query);
            
            if(DistributorList.size() > 0)
               return DistributorList; 
        }
        return null;
    }
}