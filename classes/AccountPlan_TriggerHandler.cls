// Handler Class for AccountPlan_Trigger
//
// Created on Nov 29th,2018 by Dhilip C

public class AccountPlan_TriggerHandler{
    
    public static void beforeUpdate(List<AccountPlan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
    }
    
    public static void beforeInsert(List<AccountPlan__c> newList)
    {
        IRSPX_PercentComplete.updatePercentageCompleted(newList);
    }
    
    public static void afterUpdate(List<AccountPlan__c> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    public static void afterInsert(List<AccountPlan__c> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    public static void afterDelete(List<AccountPlan__c> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    //method to update Account Attributes
    public static void UpdateAccountAttributes(List<AccountPlan__c> newList)
    {
        
        List<Id> AccountPlanAccountIds = new List<Id> ();
        for (AccountPlan__c ast : newList)
        {
            AccountPlanAccountIds.add(ast.AccountID__c);
        }
        
        List<Account> AccountsList = new List<Account> ();        
        AccountsList = [Select Id,AccountPlan_Count__c from Account where Id in :AccountPlanAccountIds];        
        //System.debug('AccountsList == ' + AccountsList + '\n');
        
        Map<Id,AggregateResult> results = new Map<Id,AggregateResult>([Select AccountName__r.Id Id,Count(Id) AccPlnCnt from AccountPlan__c where AccountName__r.Id != null and AccountName__r.Id in :AccountPlanAccountIds Group By AccountName__r.Id]);
        //System.debug('results == ' + results + '\n');       
        for(Account ac:AccountsList)
        {   
            if (results.get(ac.Id) == null)
                ac.AccountPlan_Count__c = 0; 
            else
                ac.AccountPlan_Count__c = Decimal.ValueOf(String.ValueOf(results.get(ac.Id).get('AccPlnCnt')));
                       
        }
            
        update(AccountsList);    
    }
}