global class CC_scheduledDeleteCleanup implements Schedulable {
  global void execute(SchedulableContext sc) {
    CC_batchDeleteCleanup statements = new CC_batchDeleteCleanup('Select Id From CC_Statements__c where Request_Delete__c=True');
    Database.executeBatch(statements);
    
    CC_batchDeleteCleanup vehicles = new CC_batchDeleteCleanup('Select Id From CC_Vehicle__c where Request_Delete__c=True');
    Database.executeBatch(vehicles);
    
    CC_batchDeleteCleanup productRules = new CC_batchDeleteCleanup('Select Id From CC_Product_Rule__c where Request_Delete__c=True');
    Database.executeBatch(productRules);
    
    CC_batchDeleteCleanup contentEmailPublish = new CC_batchDeleteCleanup('Select Id From CC_Content_Publish_Email__c where CreatedDate != LAST_N_DAYS:60');
    Database.executeBatch(contentEmailPublish);
   }  
}