//
// (c) 2015, Appirio Inc.
// Handler Class for Contact Trigger
// April 16,2015    Surabhi Sharma    T-377640
// July 13, 2016  Ben Lorenz refactored for SBU specific lists
//Compressed_Air Recordtype commented as per record type deletion.
     //Code commented by Yadav Vineet Capgemini for descoped RecordTypes                        

public class Contact_TriggerHandler{

  public static void afterDelete(Map<String, List<Contact>> trgOldMap){
    Contact[] AirLst = new Contact[]{};
    //AirLst.addAll(trgOldMap.get('Compressed_Air'));
    AirLst.addAll(trgOldMap.get('Customer_Contact'));
     AirLst.addAll(trgOldMap.get('CTS_EU_Indirect_Contact'));
     AirLst.addAll(trgOldMap.get('CTS_MEIA_Indirect_Contact')); 
     
    if(!AirLst.isEmpty()){Contact_TriggerHandler.createNewOBMNotifiers(AirLst);}
  }
  
  public static void afterUpdate(Map<String, List<Contact>> trgNewMap){    
    Contact[] AirLst = new Contact[]{};
    //AirLst.addAll(trgNewMap.get('Compressed_Air'));
    AirLst.addAll(trgNewMap.get('Customer_Contact'));
      AirLst.addAll(trgNewMap.get('CTS_EU_Indirect_Contact'));
      AirLst.addAll(trgNewMap.get('CTS_MEIA_Indirect_Contact')); 
     
    if(!AirLst.isEmpty()){UtilityClass.deleteApprovedRecords(AirLst,'Contact',UtilityClass.GENERIC_APPROVAL_API_NAME);}
  }
    
  // Method to create OBM_Notifier record on contact delete
  //Added by Surabhi Sharma on July 14, 2015 @T-418696
  private static void createNewOBMNotifiers(List<Contact> conList){ 
    list<OBM_Notifier__c> newRecord = new list<OBM_Notifier__c>();
    //  Set<Id> conRecSet = new Set<Id>();
    OBM_Notifier__c notifier;       
    for(Contact conObj : conList){   
      notifier = new OBM_Notifier__c(); 
      notifier.Name = conObj.FirstName ;        
      notifier.External_Id__c = conObj.Siebel_ID__c ;
      notifier.Operation__c = 'Delete';
      notifier.Object_Type__c = 'Contact';
      //notifier.RecordTypeId = CC_Utility.naairRectypeId;
      newRecord.add(notifier);
    }
    if(!newRecord.isEmpty()){insert newRecord;} 
  }
}