@isTest
private class RshvacOrderTriggerHandler_Test {

    //Cost Center Details
    public static final String DSO_COST_CENTER = 'DSO Cost Center';
    public static final String DSO_DEFAULT_BRANCH = 'DSO Default Branch';
    public static final String DSO = 'DSO';
    public static final String IWD_COST_CENTER = 'IWD Cost Center';
    public static final String IWD_DEFAULT_BRANCH = 'IWD Default Branch';
    public static final String IWD = 'IWD';
    // User Details
    private static final String FIRST_NAME = 'Thomas';
    private static final String LAST_NAME = 'Pynchon';
    private static final String USERNAME = 'tpynchon@nowhere.com';
    private static final String NICKNAME = 'TPynch';
    private static final String LOCALE = 'en_US';
    private static final String TIMEZONE = 'America/New_York';
    private static final String ENCODING = 'ISO-8859-1';
    private static final String PROFILE_NAME = 'RHVAC Sales';
    private static final String SALES_PERSON_ID = '4242424242';
    
    // Data used to create the User needed for testing IWD Cost Center's Revenue Schedules.
    private static final String IWD_USER_FIRST_NAME = 'David';
    private static final String IWD_USER_LAST_NAME = 'Garrett';
    private static final String IWD_USER_USERNAME = 'dgarrett@nowhere.com';
    private static final String IWD_USER_NICKNAME = 'dgarr';
    // Default Quota values
    private static final Integer QUOTA = 1000;
  
    // Account Name
    public static final String DSO_ACCOUNT_NAME = 'DSO Account';
    public static final String IWD_ACCOUNT_NAME = 'IWD Account';
    public static final String ACCOUNT_NAME = 'Some Account';
    // Defaul annual revenue
    public static final Integer ANNUAL_REVENUE = 12000;
    public static final Integer MONTHLY_REVENUE = 12000/12;
    // RS_HVAC Record Type Id
    private static Id RS_HVAC_RT_ID = RshvacAccountTriggerHandler.RS_HVAC_RT_ID;
    // Account Types
    private static final String DEALER = RshvacAccountTriggerHandler.DEALER;
    // Account Status
    private static final String POTENTIAL = RshvacAccountTriggerHandler.POTENTIAL;
    private static final String CUSTOMER = RshvacAccountTriggerHandler.CUSTOMER;

    // Opportunity Record Types Data
    private static final String RS_HVAC_Opp_RT_DEV_NAME = 'RS_Existing_Business';
    private static Id rshvacOppRTID;
  
    
    /**
     *  Sets the data need for the tests
     */
    @testSetup static void mySetup() {
        // Insert a single Account, the trigger will take care of inserting the Opportunity and the Revenue Schedules records
        Account baseAccount = new Account(Name = ACCOUNT_NAME, AnnualRevenue = ANNUAL_REVENUE,
                                    RecordTypeId = RS_HVAC_RT_ID, Type = DEALER, Status__c = CUSTOMER);
        insert baseAccount;
        
        // Get the Profile Id to assign to the test User
        Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME LIMIT 1].Id;
        // Insert a new User, it'll initially have the FIRST_SP_ID as Sales Person Id
        User dsoTestUser = new User(Sales_Person_Id__c = SALES_PERSON_ID, FirstName = FIRST_NAME, LastName = LAST_NAME, ProfileId = profileId,
                              UserName = USERNAME, Email = USERNAME, CommunityNickname = NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                              TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = NICKNAME);
        insert dsoTestUser;
        
        // Insert Cost Center record
        Cost_Center__c dsoCostCenter = New Cost_Center__c(Name = DSO_COST_CENTER, Ownerid = dsoTestUser.id, Default_Branch__c = DSO_DEFAULT_BRANCH, 
                                                        Channel__c =DSO);
        insert dsoCostCenter;  
        
        Account dsoAccount = new Account(Name = DSO_ACCOUNT_NAME, Default_Branch__c = DSO_DEFAULT_BRANCH,
                                          RecordTypeId = RS_HVAC_RT_ID, Type = DEALER, Status__c = CUSTOMER, OwnerId = dsoTestUser.Id);
        insert dsoAccount;
        
        
        // Insert Residential Quota records for all months in the Year
        List<Residential_Quota__c> quotas = new List<Residential_Quota__c>();
        Integer currentYear = Date.today().year();
        for (Integer i = 1;i <= 12;i++){
          Integer daysInMonth = Date.daysInMonth(currentYear, i);
          Date forecastMonth = Date.newInstance(currentYear, i, daysInMonth);
          quotas.add(new Residential_Quota__c(User__c = dsoTestUser.Id, Forecast_Month__c = forecastMonth, Plan__c = QUOTA));
          quotas.add(new Residential_Quota__c(User__c = dsoTestUser.Id, Forecast_Month__c = Date.newInstance(Date.Today().year(),Date.Today().month()-1,1), Plan__c = QUOTA ));
        }
        insert quotas;
        
        User iwdTestUser = new User(FirstName = IWD_USER_FIRST_NAME, LastName = IWD_USER_LAST_NAME, ProfileId = profileId,
                              UserName = IWD_USER_USERNAME, Email = IWD_USER_USERNAME, CommunityNickname = IWD_USER_NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                              TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = NICKNAME);
        insert iwdTestUser;
        
        // Insert Cost Center record
        Cost_Center__c iwdCostCenter = New Cost_Center__c(Name = IWD_COST_CENTER, Ownerid = iwdTestUser.id, Default_Branch__c = IWD_DEFAULT_BRANCH, 
                                                           Create_Existing_Business_Opportunity__c = True, Channel__c =IWD);
        insert iwdCostCenter;  
        System.debug('jaya line96 '+[select id from opportunity_revenue_Schedule__C where opportunity__r.Cost_Center_Name__c = :iwdCostCenter.id]);
        Account iwdAccount = new Account(Name = IWD_ACCOUNT_NAME, Default_Branch__c = IWD_DEFAULT_BRANCH,
                                          RecordTypeId = RS_HVAC_RT_ID, Type = DEALER, Status__c = CUSTOMER);
        insert iwdAccount;
        
        List<Residential_Quota__c> iwdQuotas = new List<Residential_Quota__c>();
        for (Integer i = 1;i <= 12;i++){
          Integer daysInMonth = Date.daysInMonth(currentYear, i);
          Date forecastMonth = Date.newInstance(currentYear, i, daysInMonth);
          iwdQuotas.add(new Residential_Quota__c(User__c = iwdTestUser.Id, Forecast_Month__c = forecastMonth, Plan__c = QUOTA));
        }
        insert iwdQuotas;
       
    }
    
    /**
     *  Tests that the logic on the insert trigger is correct
     */
    static testMethod void testInsertOrders() {
        // Get the default account
        Account acc = [SELECT Id FROM Account WHERE Name =:ACCOUNT_NAME LIMIT 1];
        Integer orderTotal = 100;
        RSHVAC_Order__c firstOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = orderTotal, Ordered_Date__c = Date.today());
        Test.startTest();
            insert firstOrder;
        Test.stopTest();
    }

    /**
     *  Tests that the logic on the update trigger is correct
     */
    static testMethod void testUpdateOrders() {
        // Get the default account
        Account acc = [SELECT Id FROM Account WHERE Name =:ACCOUNT_NAME LIMIT 1];
        Integer orderTotal = 100;
        RSHVAC_Order__c firstOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = orderTotal, Ordered_Date__c = Date.today());
        insert firstOrder;
        Integer newOrderTotal = 300;
        Test.startTest();
            firstOrder.Close_Line_Subtotal__c = newOrderTotal;
            update firstOrder;
        Test.stopTest();
    }
    
    static testMethod void testUpdateDSORevenueSchedules(){
      Integer monthOfRevenueScheduleUpdate = Date.today().month();
      Account acc = [SELECT Id, OwnerId FROM Account WHERE Name =:DSO_ACCOUNT_NAME LIMIT 1];
      Integer orderTotal = 100;
      System.debug('Accounts owner : '+acc.OwnerId);
      Test.startTest();  
      RSHVAC_Order__c firstOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = orderTotal, Ordered_Date__c = Date.today());
      insert firstOrder;
      rshvacOppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_Opp_RT_DEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;     
      Opportunity_Revenue_Schedule__c dsoRevSchedules = [Select id, Actual__c from Opportunity_Revenue_Schedule__c 
                                                               where Opportunity__r.Account.Name = :DSO_ACCOUNT_NAME AND Opportunity__r.RecordTypeId = :rshvacOppRTID
                                                                   AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate];
      System.assertEquals(dsoRevSchedules.Actual__c, orderTotal);
      Residential_Quota__c dsoQuota = [Select id, Actual__c from Residential_Quota__c where CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate 
                                        AND user__c = :acc.OwnerId LIMIT 1]; 
      System.assertEquals(dsoQuota.Actual__c, orderTotal);
    }
    
    static testMethod void testUpdateIWDRevenueSchedules(){
      Cost_Center__c costCenter = [SELECT Id, OwnerId FROM Cost_Center__c  WHERE Name =:IWD_COST_CENTER LIMIT 1];
      Account acc = [SELECT Id, OwnerId FROM Account WHERE Name =:IWD_ACCOUNT_NAME LIMIT 1];
      Integer orderTotal = 100;
      Test.startTest();  
      RSHVAC_Order__c firstOrder = new RSHVAC_Order__c(Account__c = acc.Id,  Close_Line_Subtotal__c = orderTotal, Ordered_Date__c = Date.today());
      insert firstOrder;
      rshvacOppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_Opp_RT_DEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;     
      Opportunity_Revenue_Schedule__c iwdRevSchedules = [Select id, Actual__c from Opportunity_Revenue_Schedule__c 
                                                               where Opportunity__r.Cost_Center_Name__r.Name = :IWD_COST_CENTER AND Opportunity__r.RecordTypeId = :rshvacOppRTID
                                                                   AND Forecast_Month__c = THIS_MONTH];
      System.assertEquals(iwdRevSchedules.Actual__c, orderTotal);
      Residential_Quota__c iwdQuota = [Select id, Actual__c from Residential_Quota__c where Forecast_Month__c = THIS_MONTH
                                        AND user__c = :acc.OwnerId LIMIT 1]; 
      System.assertEquals(iwdQuota.Actual__c, orderTotal);
      
      firstOrder.Close_Line_Subtotal__c = 2*orderTotal;
      update firstOrder;
      iwdRevSchedules = [Select id, Actual__c from Opportunity_Revenue_Schedule__c 
                                                               where Opportunity__r.Cost_Center_Name__r.Name = :IWD_COST_CENTER AND Opportunity__r.RecordTypeId = :rshvacOppRTID
                                                                   AND Forecast_Month__c = THIS_MONTH];
      System.assertEquals(iwdRevSchedules.Actual__c, 2*orderTotal);
      iwdQuota = [Select id, Actual__c from Residential_Quota__c where Forecast_Month__c = THIS_MONTH
                                        AND user__c = :acc.OwnerId LIMIT 1]; 
      System.assertEquals(iwdQuota.Actual__c, 2*orderTotal);
    } 
}