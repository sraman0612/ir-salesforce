/**
* @createdBy   Sangram Ray
* @date         08/01/2016
* @description  Test Class for CC_PavilionShippingcnt
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest
public class CC_PavilionShippingcnt_Test {
    static PavilionSettings__c pavillionCommunityProfileId;
    static PavilionSettings__c  pavilionPublicGroupID;
    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Account account;
    static CC_Order__c order;
    static CC_Order_Item__c orderItem;
    static PavilionSettings__c hazMatFee;
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    
    // test getAllCountriesInSet
    @isTest
    public static void getAllCountriesInSet_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;

            // insert CC_Parts_Calendar__c
            CC_Parts_Calendar__c partsCalender = new CC_Parts_Calendar__c(
                Date__c = Date.today()
            );
            insert partsCalender;

            // insert StatesCountries__c
            StatesCountries__c sAndC = new StatesCountries__c(
                Name = 'Alabama',
                CODE__c = 'AL',
                Country__c = 'USA',
                Type__c = 'S'
            );
            insert sAndC;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt PavilionShippingcnt = new CC_PavilionShippingcnt(cc_PavilionTemplateController);
            PavilionShippingcnt.getAllCountriesInSet();
            PavilionShippingcnt.getCountriesWithStates();
            CC_PavilionShippingcnt.deleteAddress(addr.id);
        }
    }

    // test newaddress
    @isTest
    public static void newaddress_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;

            // insert CC_Parts_Calendar__c
            CC_Parts_Calendar__c partsCalender = new CC_Parts_Calendar__c(
                Date__c = Date.today()
            );
            insert partsCalender;

            // insert StatesCountries__c
            StatesCountries__c sAndC = new StatesCountries__c(
                Name = 'Alabama',
                CODE__c = 'AL',
                Country__c = 'USA',
                Type__c = 'S'
            );
            insert sAndC;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.newaddress(
              '',
                account.Id,
                'Test Name',
                'address1',
                'address2',
                'address3',
                '123456',
                'city',
                'Alabama',
                'USA',
                'Test Contact',
                '1234567890',
                '1234567890'
            );
        }
    }

    // test getStatesList
    @isTest
    public static void getStatesList_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;

            // insert CC_Parts_Calendar__c
            CC_Parts_Calendar__c partsCalender = new CC_Parts_Calendar__c(
                Date__c = Date.today()
            );
            insert partsCalender;

            // insert StatesCountries__c
            StatesCountries__c sAndC = new StatesCountries__c(
                Name = 'Alabama',
                CODE__c = 'AL',
                Country__c = 'USA',
                Type__c = 'S'
            );
            insert sAndC;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.getStatesList('USA');
        }
    }

    // test getShipDates
    @isTest
    public static void getShipDates_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;

            // insert CC_Parts_Calendar__c
            CC_Parts_Calendar__c partsCalender = new CC_Parts_Calendar__c(
                Date__c = Date.today()
            );
            insert partsCalender;

            // insert StatesCountries__c
            StatesCountries__c sAndC = new StatesCountries__c(
                Name = 'Alabama',
                CODE__c = 'AL',
                Country__c = 'USA',
                Type__c = 'S'
            );
            insert sAndC;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.getShipDates();
        }
    }

    // test checkTaxStatus
    @isTest
    public static void checkTaxStatus_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.checkTaxStatus('CA', 'USA', account.Id);
        }
    }

    // test getShipAddress
    @isTest
    public static void getShipAddress_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.getShipAddress(order.Id);
           
        }
    }

    // test updateOrder
    @isTest
    public static void updateOrder_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.updateOrder(order.Id, addr.Id, 'DHL', 'INTNLEXMPT', 'Test Acct', shipDate);
        }
    }

    // test updateOrder -2
    @isTest
    public static void updateOrder2_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);

            String shipDate = '12/27/2009';
            CC_PavilionShippingcnt.updateOrder(order.Id, account.Id, 'DHL', 'INTNLEXMPT', 'Test Acct', shipDate);
        }
    }

    // test theShipMethods
    @isTest
    public static void theShipMethods_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionShippingcnt.theShipMethods(
                order.Id, 
                'US',
                'USA'
            );
        }
    }

    // test theShipMethods -2
    @isTest
    public static void theShipMethods2_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            // insert CC_Shipping_Methods__c
            CC_Shipping_Methods__c shipMethods = new CC_Shipping_Methods__c(
                Name = 'DHL',
                CC_Is_Domestic__c = true,
                CC_Eligible_for_Parcel__c = true,
                CC_Eligible_for_Motorfreight__c = true,
                CC_Canada_Only__c = true,
                CC_Carrier_ID__c = 'OCEAN'
            );
            insert shipMethods;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionShippingcnt.theShipMethods(
                order.Id, 
                'OR',
                'IN'
            );
            CC_PavilionShippingcnt.theShipMethods(
                order.Id, 
                'CA',
                'IN'
            );
        }
    }
    
    // test getExistingAddresses
    @isTest
    public static void getExistingAddresses_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert CC_Account_Address__c
            CC_Account_Address__c addr = new CC_Account_Address__c(
                CC_Account__c = account.Id
            );
            insert addr;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionShippingcnt.getExistingAddresses(account.Id);
        }
    }
    
    // test constructor
    @isTest
    public static void constructor_Test() {
        setup();
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionShipping'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionShippingcnt cc_PavilionShippingcnt = new CC_PavilionShippingcnt(cc_PavilionTemplateController);
        }
    }
    
    public static void setup() {
        // for CAD Pricing 
        account = TestUtilityClass.createAccount();
         account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
         account.name = 'Test';
         account.CC_Customer_Number__c = '5621';
        insert account;

        contact = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN',
            FirstName = 'FS',
            AccountId = account.Id,
            Email = 'test@test.ingersollrand.com'
        );
        insert contact;

        portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Partner Community User%' Limit 1];
        
        testUser = new User(
            Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',
            ContactId = contact.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test1236',
            Email = 'test123456@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test123456',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert testUser;
        
       //AccountTeamMember for Account share error
        
        AccountTeamMember atm = TestUtilityClass.createATM(account.Id,testUser.Id);
        insert atm;
                
        AccountShare accShare = TestUtilityClass.createAccShare(account.Id,testUser.Id);
        insert accShare; 
    }
}