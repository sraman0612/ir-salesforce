/*
*
* CREATED FEB 18 2016: POC SGL
*
* This class will update the Snapshop Report custom object by setting the record owner to the opportunity owner.
*
*/
global class SnapshotReportMaintenance implements Database.Batchable<sObject> {
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Opportunity_OwnerTest__c, OwnerId FROM Snapshot_Test__c WHERE Opportunity_OwnerTest__c != null and Oppty_Rec_Owner_Match__c = false';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Snapshot_Test__c> scope) {
         for(Snapshot_Test__c st : scope)
         {
             st.OwnerId = st.Opportunity_OwnerTest__c;            
         }
         update scope;
    }   
    
    global void finish(Database.BatchableContext BC) {
    
    }
}