public class CC_TechWarrantyTrainingReq {
  
  public CC_TrainingRequest attendee{get;set;}
  public String returnStr {get;set;}
  
  public List<SelectOption> getAttendeeTypes(){
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult =Case.CC_Attendee_Type__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption(f.getLabel(), f.getValue()));
    }       
    return options;
  }
  
  public List<SelectOption> getCourses(){
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult =Case.CC_Courses__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption(f.getLabel(), f.getValue()));
    }       
    return options;
  }
  
  public List<SelectOption> getPaymentModes(){
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult =Case.CC_Mode_of_Payment__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption(f.getLabel(), f.getValue()));
    }       
    return options;
  }  

  public CC_TechWarrantyTrainingReq(CC_PavilionTemplateController controller){
    attendee=new CC_TrainingRequest();
    User usr =[select Id, ContactId, Name, Phone, Extension, MobilePhone from User where Id = : UserInfo.getUserId() limit 1];
    Contact contct = [SELECT Id, AccountId, Account.CC_Sales_Rep__c, Account.CC_Sales_Rep__r.Name, Email, HomePhone, MobilePhone, 
                             OtherPhone, Phone, CC_Account_Number__c, Account.CC_Customer_Number__c, Account.Name,account.ShippingCity,
                             account.ShippingState, account.ShippingPostalCode, account.ShippingCountry
                        FROM Contact
                       WHERE ID = : usr.ContactId limit 1];
    attendee.ContactId = contct.Id;
    attendee.AccountId = contct.AccountId;
    attendee.CC_Company_Name=contct.account.Name!=null?contct.account.Name:'';
    attendee.CC_Clubcar_Customer=contct.account.CC_Customer_Number__c!= null?contct.account.CC_Customer_Number__c:'';
    attendee.CC_Company_Name = contct.account.Name != null?contct.account.Name:'';
    String addressStr = '';
    if (contct.account.ShippingCity != null)
      addressStr += contct.account.ShippingCity + ', ';
    if (contct.account.ShippingState != null)
      addressStr += contct.account.ShippingState + ' ';
    if (contct.account.ShippingPostalCode != null)
      addressStr += contct.account.ShippingPostalCode;
    addressStr +=  '\n';
    if (contct.account.ShippingCountry == null)
      addressStr += contct.account.ShippingCountry;
    attendee.CC_Company_Address = addressStr;
    attendee.CC_Company_Telephone = contct.MobilePhone != null ? contct.MobilePhone : '';
    attendee.CC_Company_Email = contct.Email != null ? contct.Email : '';
    attendee.CC_Sales_Rep_Name = contct.Account.CC_Sales_Rep__r.Name != null ? contct.Account.CC_Sales_Rep__r.Name : '';
  }   
  
  public void createTrainingRegistration(){
    Case newCase = new Case();
    newCase.Origin = 'Community';
    newCase.Reason = 'Training';
    newCase.Sub_Reason__c = 'Tech Training';
    newCase.OwnerId = '00Gj0000001wb43';
    newCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName ='Club_Car_Training_Request' and SobjectType='Case' LIMIT 1].Id;
    newCase.ContactId = attendee.ContactId;
    newCase.CC_Sales_Rep__c = attendee.SalesRepId;
    newCase.CC_Attendee_Name__c = attendee.CC_Attendee_Name;
    newCase.CC_Attendee_Phone__c = attendee.CC_Attendee_Telephone;
    newCase.CC_Attendee_Email__c = attendee.CC_Attendee_Email;
    newCase.CC_Courses__c = attendee.CC_Courses;
    newCase.CC_Attendee_Type__c = attendee.Attendee_Type;
    newCase.CC_Mode_of_Payment__c = attendee.CC_Mode_of_Payment;
    try{
      insert newCase;
      returnStr = 'Training Request Created. Thank You!';
    } catch(Exception e) {
      returnStr = 'We were unable to create your training request.' + e.getStackTraceString();
    }
  }
}