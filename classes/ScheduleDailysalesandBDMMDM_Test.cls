@isTest
private class ScheduleDailysalesandBDMMDM_Test {

  static testmethod void testSchedule() {
    // Set a job name
    String jobName1 = 'testSchedule1';
    String jobName2 = 'testSchedule2'; 
    Test.startTest();
      // Schedule the job
      String jobId1 = ScheduleResetDailysalesOnIwdNewBusOpp.scheduleMe(jobName1);
      String jobId2 = ScheduleBatchChangeBdmMdmNewBusOpp.scheduleMe(jobName2);
    Test.stopTest();
    // Verify that a job Id was returned
    System.assertEquals(true, jobId1 != null);
    System.assertEquals(true, jobId2 != null);

    // Get the information from the CronTrigger API object
    CronTrigger ct1 = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId1];
    CronTrigger ct2 = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId2];
    // Verify the expressions are the same
    System.assertEquals(ScheduleResetDailysalesOnIwdNewBusOpp.SCHEDULE_INTERVAL, ct1.CronExpression);
    System.assertEquals(ScheduleBatchChangeBdmMdmNewBusOpp.SCHEDULE_INTERVAL, ct2.CronExpression);
  }
}