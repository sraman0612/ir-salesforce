@isTest
public class CTSUpdateAccountStreetAddress_TEST {
 static testmethod void test1(){
    /*Division__c divn = new Division__c();
    divn.Name = 'Test Division';
    divn.Division_Type__c = 'Customer Center';
    divn.EBS_System__c = 'Oracle 11i';     
   	insert divn;*/     
    test.startTest();    
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for Oracle Quote';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
  //  a.ShippingStreet = '12, street1678';
    a.CTS_Global_Shipping_Address_1__c = '13 street 1';
    a.CTS_Global_Shipping_Address_2__c = 'street 2';
    a.ShippingPostalCode = '123';  
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
 // a.Account_Division__c= 'a2c4Q000006rW5NQAU'; //Ritesh:Added division to avoid division validation on Accounts.
 //   a.Account_Division__c = divn.Id;
    insert a;
    a.CTS_Global_Shipping_Address_2__c = 'street 3';
    update a;
    a.CTS_Global_Shipping_Address_1__c = '81 street address 1';
    update a;   
 //   system.debug(a.ShippingStreet);
    try{
      delete a;
    } catch (exception e){
    }
    test.stopTest();
  }
    
 static testmethod void test2(){
/*    Division__c divn = new Division__c();
    divn.Name = 'Test Division';
    divn.Division_Type__c = 'Customer Center';
    divn.EBS_System__c = 'Oracle 11i';     
   	insert divn;    */ 
    test.startTest();    
    Id AirNAAcctDistRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for Oracle Quote NA Dist R11';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
  //  a.ShippingStreet = '12, street1678';
    a.CTS_Global_Shipping_Address_2__c = '';
    a.CTS_Global_Shipping_Address_1__c = '81 street address 1';
    a.ShippingPostalCode = '123';  
    a.County__c = 'Mecklenburg';
    a.RecordTypeId = AirNAAcctDistRT_ID;
  //a.Division__c	= 'AIRDYNE INC'; //Ritesh:Added division to avoid division validation on Accounts.
 //   a.Account_Division__c = divn.Id;
    insert a;
    a.CTS_Global_Shipping_Address_2__c = 'street 4';
    a.CTS_Global_Shipping_Address_1__c = '';
    update a;
    a.CTS_Global_Shipping_Address_1__c = '80 street address 1';
    a.CTS_Global_Shipping_Address_2__c = '';
    update a;     
 //   system.debug(a.ShippingStreet);
    try{
      delete a;
    } catch (exception e){
    }
    test.stopTest();
  } 
    
 static testmethod void test3(){
/*    Division__c divn = new Division__c();
    divn.Name = 'Test Division';
    divn.Division_Type__c = 'Customer Center';
    divn.EBS_System__c = 'Oracle 11i';     
   	insert divn;    */ 
    test.startTest();    
    Id AirNAAcctDistRTR12_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for Oracle Quote NA Dist R12';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'Canada';
    a.ShippingState = 'CA';
  //  a.ShippingStreet = '12, street1678';
    a.CTS_Global_Shipping_Address_1__c = '';
    a.CTS_Global_Shipping_Address_2__c = 'street 3';
    a.ShippingPostalCode = '123';  
    a.County__c = 'Mecklenburg';
    a.RecordTypeId = AirNAAcctDistRTR12_ID;
  //a.Division__c	= 'IR ITS Canada'; //Ritesh:Added division to avoid division validation on Accounts.
 //   a.Account_Division__c = divn.Id;
    insert a;
    a.CTS_Global_Shipping_Address_2__c = 'street 5';
    a.CTS_Global_Shipping_Address_1__c = '';
    update a;
    a.CTS_Global_Shipping_Address_1__c = '79 street address 1';
    a.CTS_Global_Shipping_Address_2__c = '';
    update a;     
 //   system.debug(a.ShippingStreet);
    try{
      delete a;
    } catch (exception e){
    }
    test.stopTest();
  } 
}