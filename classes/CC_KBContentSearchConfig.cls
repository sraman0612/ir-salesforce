/*************************************
 * KBContentSearchConfig
 * Contains methods related to custom setting and custom metadata
 **************************************/
public class CC_KBContentSearchConfig {
    
    /**
     * get fields to add in SOSL query
     * @param objectNames
     * @return Map
     */
    public static Map<String, List<String>> getFields(List<String> objectNames) {
        Map<String, List<String>> fieldMap = new Map<String, List<String>>();
        for(String objName : objectNames){
            fieldMap.put(objName, new List<String>());
        }
        String q = 'select Field_API_Name__c,Object_Name__c from CC_KB_Content_Search_Field_Config__mdt where Object_Name__c IN: objectNames';
        
        for(CC_KB_Content_Search_Field_Config__mdt fieldConfig : Database.query(q) ){
            fieldMap.get(fieldConfig.Object_Name__c).add(fieldConfig.Field_API_Name__c);
        }
        return fieldMap;
    }
    
    /**
     * get filter congifuration
     * @param objectNames, filterType
     * @return Map
     */
    public static Map<String, List<CC_KB_Content_Search_Filter_Config__mdt>> getFilterConfig(List<String> objectNames, String filterType){
        Map<String, List<CC_KB_Content_Search_Filter_Config__mdt>> filterMap = new Map<String, List<CC_KB_Content_Search_Filter_Config__mdt>>();
        for(String objName : objectNames){
            filterMap.put(objName, new List<CC_KB_Content_Search_Filter_Config__mdt>());
        }
        for(CC_KB_Content_Search_Filter_Config__mdt filterConfig : [select Data_Type__c,Filter_Type__c,filter_Values__c,Matching_Field__c,Object_Name__c,Operator__c,Filter_Customer_Profile_Field__c from CC_KB_Content_Search_Filter_Config__mdt where Object_Name__c in :objectNames AND filter_Type__c = : filterType]){
            filterMap.get(filterConfig.Object_Name__c).add(filterConfig);
        }
        return filterMap;
    }
    
     /**
     * get Search config
     * @return Object
     */

    public static CC_kb_content_search_config__c getSearchConfig(){
        CC_kb_content_search_config__c searchConfig = CC_kb_content_search_config__c.getInstance();
        if(searchConfig == null){
            searchConfig = new CC_kb_content_search_config__c(available_options_in_the_page_size__c = '30,50,100', Default_page_size__c=30,show_search_filter_options__c = false,
                        Image_files__c = 'AI,EPS,GIF,JPEG,JPG,PNG', Video_files__c = 'M4V,MOV,MP4,MPG,MOV', Music_files__c = 'MP3',Link_files__c = 'LINK', Record_Type__c = 'Club Car');
        }
        return searchConfig;        
    }
}