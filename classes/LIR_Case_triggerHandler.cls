public class LIR_Case_triggerHandler {
    public static void beforeInsert(List<Case> newCase){
        Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Case');
        if(!softDisable.insertDisabled()){
            CTS_OM_Case_TriggerHandler.onBeforeInsert(newCase);
        }
        // for updating Parent Case Status - New Action Item button.
        Update_Parent_PendingResponse.updateParentCaseStatusMethod(newCase);

    }
    
    public static void beforeUpdate(List<Case> newCase, Map<Id,Case> oldMap){
        Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Case');
        if(!softDisable.updateDisabled()){
             for(Case caselist:newCase){
                System.debug('recordtypeName: '+ caselist.RecordTypeName__c);
                if(caselist.RecordTypeName__c.startsWith('CTS_OM') || caselist.RecordTypeName__c == 'CTS_LATAM_Brazil_Application_Engineering'){
                    CTS_OM_UpdateCaseAge.updateCaseAge(newCase, oldMap);
              	}
            }
        }
    }
    
 /*   public static void afterUpdate(List<Case> newCase, List<Case> oldCase,  Map<Id,Case> newMap, Map<Id,Case> oldMap){
        Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Case');
        if(!softDisable.updateDisabled()){
            Case_TriggerHandler.afterUpdate(newCase,oldCase,newMap,oldMap);
        }
    }*/
}