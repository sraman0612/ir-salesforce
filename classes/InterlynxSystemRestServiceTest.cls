@isTest
public class InterlynxSystemRestServiceTest {
    @testSetup
    public static void newTestleads(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(200));
        List<Lead> testLeads = new List<Lead>();
		List<Assignment_Groups__c> groupMembers = new List<Assignment_Groups__c>();
        for(Integer i = 0; i<=5; i++){
            Lead testLead = new Lead();
            testLead.FirstName = 'test';
            testLead.LastName = 'lead '+i;
            testLead.VPTECH_Brand__c = 'Milton Roy';
            testLead.Assigner_Comments__c = 'Test Assigner Comments '+i;
            testLead.Lead_Source_1__c = 'Marketing '+i;
            testLead.Lead_Source_2__c = 'Advertisement '+i;
            testLead.Lead_Source_3__c = 'Social media '+i;
            testLead.Lead_Source_4__c = 'Friend '+i;
            testLead.Email = 'test@gmail.com';
            testLead.Title = 'Mr';
            testLead.Website = 'www.google.com';
            testLead.LeadSource = 'Identified by Sales Rep';
            testLead.Description = 'Description '+i;
            testLead.Phone = '989898';
            testLead.Ready_to_share_with_Interlynx__c = true;
            testLead.Country = 'US';
            testLead.Industry = 'Industrial';
            testLead.Opportunity_Type__c = 'Budgetary';
            if(i == 1)
                testLead.Dist_ID__c = '100';
            else if(i == 2)
                testLead.Dist_ID__c = '180';
            else if(i == 3){
                testLead.Dist_ID__c = '180';
                testLead.Partner_Comments__c = 'Test Partner_Comments__c';
                testLead.VPTECH_Brand__c = 'MP/OB/IRP';
            }
            else if(i == 4){
                testLead.Dist_ID__c = '110';
                testLead.Interlynx_Integration_Error__c = 'random error';
            }
            else 
                testLead.Dist_ID__c = '123';
            
         	testLeads.add(testLead);
        }
        insert testLeads;
        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account').getRecordTypeId();
        acc.ShippingCity = 'Test City';
        acc.ShippingCountry = 'Test country';
        acc.ShippingPostalCode = '658776';
        acc.ShippingStreet = 'test street';
        acc.Name = 'Test Account';
        insert acc;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'RSMUser', Email='RSM@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='RSMuser1@IR.com');
        insert u;
        for(Integer i = 0; i<=7; i++){
            Assignment_Groups__c groupMember = new Assignment_Groups__c();
            if(i == 0){
                groupMember.Group_Name__c = System.Label.RSM_Group_Name;
                groupMember.RSMID__c = '192';
                groupMember.User__c = u.Id;
            }
            else{
                groupMember.Group_Name__c = System.Label.MR_Distributor_Trio;
                if(i == 1)
                    groupMember.DISTID__c = '110';
                else if(i == 2)
                    groupMember.DISTID__c = '151';
                else if(i == 3)
                    groupMember.DISTID__c = '206';
                else if(i == 4)
                    groupMember.DISTID__c = '180';
                else if(i == 5)
                    groupMember.DISTID__c = '100';
                
                groupMember.Distributor_Name__c = acc.Id;
                groupMember.Distributor_Email__c = 'TestgrpMem@gmail.com';
                groupMember.Distributor_Contact_Name__c = 'GrpMem Contact';
                groupMember.RSMID__c = '192';
                groupMember.User__c = u.Id;
                //groupMember.Queue_Name__c = 'Test Queue';
            }
            groupMembers.add(groupMember);
        }
        insert groupMembers;
    }
    @isTest
    static void insertLeadsTest(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(200));
        InterlynxSystemRestService.Response wrapper = new InterlynxSystemRestService.Response();
        List<InterlynxSystemRestService.InsertPayload> payloadList = new List<InterlynxSystemRestService.InsertPayload>();
        for(Integer i = 0; i<=5; i++){
            InterlynxSystemRestService.InsertPayload payload = new InterlynxSystemRestService.InsertPayload();
          	payload.RSMID = '192';
            payload.Business = 'Milton Roy';
            payload.LastName = 'Test last name';
            payload.FirstName = 'Test first name';
            payload.Email = 'Test@lead.com';
            payload.Phone = '0809821';
            payload.Title = 'Mr';
            payload.Street = 'Test Street';
            payload.City = 'Jawa Timur, Kab. Probolinggo, Paiton 67291';
            payload.County = 'Test County';
            payload.State = 'Test state';
            payload.PostalCode = '07099';
            payload.Country = 'Test Country';
            payload.Industry = 'Test industry';
            payload.Description = 'test description';
            payload.LeadSource = 'Friend';
            payload.Brand = 'Milton Roy';
            payload.ProductType = 'Other';
            payload.ValueStream = 'Parts';   
            payload.RepLocatorId = '101010'+i;
            payload.oppType = 'Budgetary';
            if(i == 0){
                payload.ChannelPartnerID = '100';
                payload.City = 'Test City';
            }
            if(i == 1){
                payload.ChannelPartnerID = '180';
                payload.Business = 'MP/OB/IRP';
                payload.Brand = 'Ingersoll Rand';
            }
            payloadList.add(payload);
        }
        RestRequest leadRequest = new RestRequest();
        leadRequest.requestUri = '/services/apexrest/InterlynxService/';
        leadRequest.httpMethod = 'POST';
        leadRequest.requestBody = Blob.valueOf(JSON.serialize(payloadList));
        RestContext.request = leadRequest;
        Test.startTest();   
        List<InterlynxSystemRestService.InsertResponse> result = InterlynxSystemRestService.insertLeads();
        Test.stopTest();
    }
	@isTest
    static void updateLeadsTest(){
        List<Lead> leadToUpdate = [SELECT Id, Dist_ID__c FROM Lead LIMIT 5];
        InterlynxSystemRestService.Response wrapper = new InterlynxSystemRestService.Response();
        List<InterlynxSystemRestService.Payload> payloadList = new List<InterlynxSystemRestService.Payload>();
        for(Lead oLead: leadToUpdate){
            InterlynxSystemRestService.Payload payload = new InterlynxSystemRestService.Payload();
            payload.SalesforceLeadID = oLead.Id;
            payload.Brand = 'Linc';
            payload.LeadStatus = 'Follow Up';
            if(oLead.Dist_ID__c == '110')
                payload.ErrorAtInterlynx = 'Test ErrorAtInterlynx';
            else if(oLead.Dist_ID__c == '151')
                payload.Brand = 'Ingersoll Rand';
           	else if(oLead.Dist_ID__c == '180')
                payload.LeadStatus = 'Not My Lead';
             else if(oLead.Dist_ID__c == '100')
                payload.LeadStatus = 'Wrong leadStatus';
            else
                payload.ChannelPartnerID = '123';
            payload.DistributorOverallResponseTime = '10';
            payload.FirstQuoteDate = '2024-06-06';
            payload.LeadValue = '10000';
            payload.LUID = '';
            payload.RSMID = '192';
            payload.NAICSDescription = 'Test NAICSDescription';
            payload.PartnerComments = 'test partner comments';
            payload.SICDescription = 'Test SICDescription';
            payload.Industry = 'Industrial';
            payloadList.add(payload);
        }
        RestRequest leadRequest = new RestRequest();
        leadRequest.requestUri = '/services/apexrest/InterlynxService/';
        leadRequest.httpMethod = 'PATCH';
        leadRequest.requestBody = Blob.valueOf(JSON.serialize(payloadList));
        RestContext.request = leadRequest;
        Test.startTest();   
        List<InterlynxSystemRestService.Response> result = InterlynxSystemRestService.updateLeads();
        Test.stopTest();
    }
}