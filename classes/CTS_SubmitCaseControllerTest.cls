/**
    @author Providity
    @date 16March2019
    @description Test class for CTS Submit Case
*/

@IsTest
public class CTS_SubmitCaseControllerTest {
	
    static User setupData() {        
        
        Id profileId = [Select Id From Profile Where Name = 'System Administrator' LIMIT 1].Id;
        Id roleId = [Select Id From UserRole Where Name = 'CTS TechDirect BU Global' LIMIT 1].Id; 
        User u = TestUtilityClass.createNonPortalUser(profileId);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {        	           
            u.Username += '1';
            u.FederationIdentifier = '1234560';
            u.DB_Region__c = 'EMEIA';
            u.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';
            u.UserRoleId = roleId;
            insert u;   
        }
        return u;
    }
    
    @isTest
    public static void testSubmitCase(){
        List<Object> caseSuccess = CTS_SubmitCaseController.submitArticleFeedback('Test Case' , null,'IR Comp Tech Direct Article Feedback', 'Test Case');
        System.assert(caseSuccess != NULL);
    }
    
    @isTest
    public static void testSubmitCaseWithArticle(){
        Test.startTest();
        Id artId = createKnowledge();
        List<Object> caseSuccess = CTS_SubmitCaseController.submitArticleFeedback('Test Case' , artId,'IR Comp Tech Direct Article Feedback', 'Test Case');
        System.assert(caseSuccess != NULL);
        Test.stopTest();
    }

    @isTest
    public static void testConstants(){
        CTS_Constants cons = new CTS_Constants();
    }
    private static Id createKnowledge(){
        User u = CTS_SubmitCaseControllerTest.setupData();
        
        Knowledge__kav akv=new Knowledge__kav();
        List<Knowledge__kav> kvlist=new List<Knowledge__kav>();
        akv.UrlName='testurl';
        akv.Title='Test kav';
        akv.CTS_TechDirect_Author_Reviewer__c = u.Id;
        
        kvlist.add(akv);
        System.runAs(new User(Id = u.Id)) {
        	insert kvlist;
        }
		Knowledge__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM Knowledge__kav WHERE id =: kvlist[0].id];
        return obj1.Id;
    }
}