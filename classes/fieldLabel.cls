public class fieldLabel{
    @AuraEnabled
    public String fieldName {get;set;}
    
    public fieldLabel(String fieldName){
        this.fieldName = fieldName;
    }
}