public without sharing class CTS_IOT_Self_RegisterController {

    public class CTS_IOT_Self_RegisterControllerException extends Exception{}

    // Public variables
    public static final String pendingReviewStatus = 'Pending Review';
    public static final String alreadyApprovedStatus = 'Already Approved';            

    // Private variables
    private static CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults(); 
    @TestVisible private static final String defaultLeadMatchType = '6 - Access Approved (New Lead Reconciled)';

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
    }

    @AuraEnabled
    public static InitResponse getInit(){
        return new InitResponse();
    }

    public class SelfRegisterRequest{        
        @AuraEnabled public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public String email;    
        @AuraEnabled public String phone;
        @AuraEnabled public String title;
        @AuraEnabled public String serialNumber;   
        @AuraEnabled public String company;
        @AuraEnabled public String address1;
        @AuraEnabled public String address2;
        @AuraEnabled public String city;
        @AuraEnabled public String state;
        @AuraEnabled public String zip;      
        @AuraEnabled public String country;        
    }    

    public class SelfRegisterResponse extends LightningResponseBase{
        @AuraEnabled public String message = '';
    }
    
    @AuraEnabled
    public static SelfRegisterResponse selfRegister(String requestStr) {
        
        SelfRegisterResponse response = new SelfRegisterResponse();
        
        system.debug('requestStr: ' + requestStr);

        Savepoint sp = null;
        
        try {
            
            sp = Database.setSavepoint();

            SelfRegisterRequest request = (SelfRegisterRequest)JSON.deserialize(requestStr, SelfRegisterRequest.class);

            String email = request.email;

            String contactQuery = 'Select Id, Name, AccountId, Account.OwnerId, Account.Owner.isActive From Contact Where Email = :email';

            Set<Id> accountRecordTypes = CTS_IOT_Data_Service.getAccountRecordTypeIds();

            // Filter on account record types if specified            
            if (accountRecordTypes.size() > 0){
                contactQuery += ' and Account.RecordTypeId in :accountRecordTypes';
            }

            contactQuery += ' Order By LastModifiedDate DESC';

            Contact[] contactMatches = Database.query(contactQuery);
            system.debug('contactMatches: ' + contactMatches);

            Contact contactMatch;

            if (contactMatches.size() > 0){
                contactMatch = contactMatches[0];
            }            

            String serialNumber = request.serialNumber;

            // Check to see if there is a serial number match and return the associated account if so
            String assetQuery = 'Select Id, AccountId, Account.OwnerId, Account.Owner.isActive, Account.Bill_To_Account__c, Account.Name, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode ' +
                           'From Asset ' +
                           'Where SerialNumber = :serialNumber';

            // Filter on asset record types if specified
            Set<Id> assetRecordTypes = CTS_IOT_Data_Service.getAssetRecordTypeIds();

            if (assetRecordTypes.size() > 0){
                assetQuery += ' and RecordTypeId in :assetRecordTypes';
            }            

            // Filter on account record types if specified            
            if (accountRecordTypes.size() > 0){
                assetQuery += ' and Account.RecordTypeId in :accountRecordTypes';
            }

            Asset[] assetSearch = Database.query(assetQuery);

            system.debug('assetSearch: ' + assetSearch);

            Asset asset;
            Account assetSite;

            if (assetSearch.size() == 1){
                
                asset = assetSearch[0];
                assetSite = ((Account)assetSearch[0].getSObject('Account'));                
            }            

            // Serial number match/asset found
            if (asset != null){

                system.debug('--asset match');

                // Check to see if there is an active super user for the bill to account in the hierarchy
                User[] superUsers = CTS_IOT_Data_Service_WithoutSharing.getSuperUsersInHierarchy(assetSite);
                User superUser = superUsers.size() > 0 ? superUsers[0] : null;

                system.debug('superUser: ' + superUser);

                // Contact matches found
                if (contactMatch != null){

                    system.debug('--contact match');

                    // Check to see if the contact is already an active user
                    User userFound = CTS_IOT_Data_Service_WithoutSharing.lookupUserbyContact(contactMatch.Id);                    
                    system.debug('userFound: ' + userFound);
                    
                    // Contact's account matches the asset's account
                    if (contactMatch.AccountId == asset.AccountId){

                        system.debug('--contact account matches asset account');

                        // Active user found for contact
                        if (userFound != null && userFound.isActive){

                            system.debug('--active user found, result: 1 - Access Already Available');
                            response.message = Label.CTS_IOT_Registration_User_Already_Exists_Message;         
                            flagContactForApproval(request, alreadyApprovedStatus, contactMatch, superUser, asset, '1 - Access Already Available', Label.CTS_IOT_Registration_User_Already_Exists_Message);                   
                        }      
                        // No active user found                  
                        else{
                            system.debug('--no active user found, result: 2 - Access Approved (Existing Asset Contact)');
                            flagContactForApproval(request, pendingReviewStatus, contactMatch, superUser, asset, '2 - Access Approved (Existing Asset Contact)', 'User activation required.');
                        }
                    }
                    // Contact's site is different than the asset's site
                    else{

                        system.debug('--contact site does not match assets site');

                        // Check to see if the contact's site is in the same hierarchy as the asset found
                        Boolean siteCheck = CTS_IOT_Data_Service_WithoutSharing.isContactInAssetHierarhcy(contactMatch.AccountId, assetSite);

                        system.debug('siteCheck: ' + siteCheck);

                        // Contact's site is in the asset's hierarchy
                        if (siteCheck){

                            system.debug('--contact is in the assets hierarchy');

                            // Active user found for contact
                            if (userFound != null && userFound.isActive){

                                system.debug('--active user found, result: 3 - Additional Site Access Approved (Existing User)');
                                flagContactForApproval(request, pendingReviewStatus, contactMatch, superUser, asset, '3 - Additional Site Access Approved (Existing User)', 'User has requested access to site "' + asset.Account.Name + '".');
                            }      
                            // No active user found                  
                            else{
                                system.debug('no active user found, result: 3 - Additional Site Access Approved (Existing User)');
                                flagContactForApproval(request, pendingReviewStatus, contactMatch, superUser, asset, '3 - Additional Site Access Approved (Existing User)', 'Contact needs access to site "' + asset.Account.Name + '"  User activation is also required.');
                            }
                        }
                        // Contact's site is not in the asset's hierarchy
                        else{                 

                            system.debug('--contact is NOT in the assets hierarchy');       

                            String message = 'Contact needs access to site "' + asset.Account.Name + '".';

                            if (userFound == null || !userFound.isActive){
                                message += '  User activation is also required.';
                            }
                            
                            system.debug('result: 4 - Access Approved (Existing Contact Different Account)');
                            flagContactForApproval(request, pendingReviewStatus, contactMatch, superUser, asset, '4 - Access Approved (Existing Contact Different Account)', message);
                        }
                    }
                }
                // No contact found by email address
                else{
                    
                    system.debug('--no contact found by email');                    

                    // Existing active super user already exists
                    if (superUser != null){

                        system.debug('--super user exists, result: 5 - Access Approved (New Contact on Asset Account)');

                        Contact newContact = createContact(request, assetSite);
                        
                        // Send approval request to super user
                        flagContactForApproval(request, pendingReviewStatus, newContact, superUser, asset, '5 - Access Approved (New Contact on Asset Account)', 'New user created that requires approval and activation.');
                    }
                    // No active super user
                    else{

                        system.debug('--no super user exists, result: create lead');

                        // Create lead for IR to approve
                        createLead(request, 'No contact found by email address.');
                    }
                }
            }
            // Asset not found by serial number
            else{

                system.debug('--asset not found by serial number, result: create lead');

                // Create lead for IR to approve
                createLead(request, 'Asset not found by serial number.');
            }     

            ApexPages.PageReference confirmRef = new PageReference(communitySettings.Registration_Confirmation_URL_Path__c);            
            
            if(!Test.isRunningTest()) {
                aura.redirect(confirmRef);       
            }                
        }
        catch (Exception e) {
            Database.rollback(sp);
            response.success = false;
            response.errorMessage = e.getMessage();            
        }

        return response;
    }

    private static void flagContactForApproval(SelfRegisterRequest request, String approvalStatus, Contact c, User superUser, Asset asset, String matchType, String matchDetails){

        system.debug('--flagContactForApproval');

        // Update the contact flag to indicate approval is needed, workflow should then send the approval request email to either the super user found or the designated IR user
        c.CTS_IOT_Community_Status__c = approvalStatus;
        c.CTS_IOT_Match_Type__c = matchType;
        c.CTS_IOT_Contact_Match_Detail__c = matchDetails + '  Serial number provided: ' + request.serialNumber;        
        c.CTS_IOT_Registration_Date__c = Datetime.now();

        if (asset != null){
            c.CTS_IOT_Registration_Asset__c = asset.Id;
        }

        // Determine the approver
        User approver;

        if (superUser != null){
            approver = superUser;
        }
        else if (asset != null && asset.AccountId != null && asset.Account.Owner.isActive){
            approver = asset.Account.Owner;
        }
        else if (communitySettings != null && String.isNotBlank(communitySettings.Default_Contact_Owner_NA__c)){      
            approver = new User(Id = communitySettings.Default_Contact_Owner_NA__c);
        }

        c.CTS_IOT_Community_Status_Approver__c = approver != null ? approver.Id : null;

        update c;
    }

    private static Lead createLead(SelfRegisterRequest request, String matchDetails){

        system.debug('--createLead');   

        matchDetails = matchDetails + '  Serial Number provided during registration: ' + request.serialNumber;      

        Lead l = new Lead(
            FirstName = request.firstName,
            LastName = request.lastName,
            Company = request.company,
            Email = request.email,
            Phone = request.phone,
            Title = request.title,
            Street = request.address1 + (String.isNotBlank(request.address2) ? + ' ' + request.address2 : ''),
            City = request.city,
            State = request.state,
            PostalCode = request.zip,
            Country = request.country,            
            Description = matchDetails,
            CTS_IOT_Lead_Match_Detail__c = matchDetails,
            CTS_IOT_Community_Status__c = pendingReviewStatus,
            CTS_IOT_Match_Type__c = defaultLeadMatchType,
            CTS_IOT_Registration_Date__c = Datetime.now()                
        );

        // Make sure assignment rules fire
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;  
        l.setOptions(dmo);        

        l = assignCommunitySettingsToNewLead(l, communitySettings, false);

        insert l;
        return l;
    }

    public static Lead assignCommunitySettingsToNewLead(Lead l, CTS_IOT_Community_Administration__c communitySettings, Boolean serviceRequest){

        if (communitySettings != null){
            
            if (serviceRequest && String.isNotBlank(communitySettings.Default_Service_Request_Owner_NA__c)){
                l.OwnerId = communitySettings.Default_Service_Request_Owner_NA__c;
            }
            else if (!serviceRequest && String.isNotBlank(communitySettings.Default_Lead_Owner_NA__c)){
                l.OwnerId = communitySettings.Default_Lead_Owner_NA__c;
            }  

            if (serviceRequest && String.isNotBlank(communitySettings.New_Service_Request_Record_Type_ID_NA__c)){
                l.RecordTypeId = communitySettings.New_Service_Request_Record_Type_ID_NA__c;
            }
            else if (!serviceRequest && String.isNotBlank(communitySettings.New_Lead_Record_Type_ID_NA__c)){
                l.RecordTypeId = communitySettings.New_Lead_Record_Type_ID_NA__c;
            }                  

            if (String.isNotBlank(communitySettings.Default_Lead_Source__c)){
                l.LeadSource = communitySettings.Default_Lead_Source__c;
            }

            if (serviceRequest && String.isNotBlank(communitySettings.Default_Lead_Source1_Service_Requests__c)){
                l.Lead_Source_1__c = communitySettings.Default_Lead_Source1_Service_Requests__c;
            }
            else if (!serviceRequest && String.isNotBlank(communitySettings.Default_Lead_Source1__c)){
                l.Lead_Source_1__c = communitySettings.Default_Lead_Source1__c;
            }

            if (serviceRequest && String.isNotBlank(communitySettings.Default_Lead_Source2_Service_Requests__c)){
                l.Lead_Source_2__c = communitySettings.Default_Lead_Source2_Service_Requests__c;
            }
            else if (!serviceRequest && String.isNotBlank(communitySettings.Default_Lead_Source2__c)){
                l.Lead_Source_2__c = communitySettings.Default_Lead_Source2__c;
            }

            if (String.isNotBlank(communitySettings.Default_SBU_ID_Workflow__c)){
                l.SBU_ID_Workflow__c = communitySettings.Default_SBU_ID_Workflow__c;
            }

            if (String.isNotBlank(communitySettings.Default_Lead_Opportunity_Type__c)){
                l.Opportunity_Type__c = communitySettings.Default_Lead_Opportunity_Type__c;
            }

            if (serviceRequest && String.isNotBlank(communitySettings.Default_Request_Type_Service_Requests__c)){
                l.Request_Type__c = communitySettings.Default_Request_Type_Service_Requests__c;
            }
        }        

        return l;        
    }

    private static Contact createContact(SelfRegisterRequest request, Account assetSite){

        system.debug('--createContact');

        Contact c = new Contact(
            FirstName = request.firstName,
            LastName = request.lastName,            
            Email = request.email,
            Phone = request.phone,
            Title = request.title,            
            MailingStreet = request.address1 + (String.isNotBlank(request.address2) ? + ' ' + request.address2 : ''),
            MailingCity = request.city,
            MailingState = request.state,
            MailingPostalCode = request.zip,
            MailingCountry = request.country
        );

        if (assetSite != null){
            c.AccountId = assetSite.Id;
            c.OwnerId = assetSite.OwnerId;
        }

        if (communitySettings != null){
            
            if (String.isNotBlank(communitySettings.Default_Contact_Owner_NA__c)){
                c.OwnerId = communitySettings.Default_Contact_Owner_NA__c;
            }

            if (String.isNotBlank(communitySettings.New_Contact_Record_Type_ID_NA__c)){
                c.RecordTypeId = communitySettings.New_Contact_Record_Type_ID_NA__c;
            }                    
        }          

        insert c;
        return c;
    }    
}