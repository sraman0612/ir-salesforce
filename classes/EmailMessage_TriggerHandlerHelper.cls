/*
* Revisions:
05-June-2024 : Update code against Case #03118925 - AMS Team
10-Sept-2024 : Updated logic for Case #03195484, #03237971, #03245535 by AMS Team
12-Feb-2025 : Update code against Case #03409124 - AMS Team
*/
public without sharing class EmailMessage_TriggerHandlerHelper {
    
    public enum CompanyDivision {CTS_OM} //, CLUB_CAR}
    //To count the recursion entry.
    public static Integer counter = 0;
    //Latest related case ID.     
    private static String previousCaseId;
    public static boolean isTest = false;
    public static Set<Id> setCheckRecursiveCaseId;
    
    //commneted as part of descoped data in August by Capgemini
    /*public static Boolean isPowerToolsOpportunityEmail(String subject)
    {
        if(subject.contains('~ 1.RFQ attached ~')) return true;
        else if(subject.contains('~ 3.RFQ incomplete ~')) return true;
        else if(subject.contains('~ 4.Quote complete ~')) return true;
        else if(subject.contains('~ 5.Quote incomplete ~')) return true;
        return false;
    }*/
    /*
    * Revisions: 10-Sept-2024: #03195484 (SOQL update), #03245535 (Updated check related case logic)  : AMS Team 
    * Revisions: 10-Sept-2024: #03409124 Populating Team field on newly created cases from AI.
	*/
    // Since ParentID cannot be updated we must reparent the email on Before insert
    public static void createNewCaseAndReparentEmail(List<EmailMessage> emails,Set<Id> caseRecordTypeIds, Id defaultCaseOwnerId, CompanyDivision cDivision){
        /*Start of ZEKS Split changes*/
        //system.debug('<<<<<emails<<<<<<<'+emails);
        ID recordTypeId = null;
        Set<Id>NWCRtIdSet = new Set<Id>();
        Set<Id>ZEKSRtIdSet = new Set<Id>();
        Set<Id>AmericasRtIdSet = new Set<Id>();
        Set<Id> AIRTIdSet = new Set<Id>();
        Id ZEKSRtId,AmericasRtId,NWCRtId;
        //Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        //added by @Capgemini
        Email_Message_Setting__mdt settings = [SELECT DeveloperName,CTS_Default_Case_Owner_ID__c,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_ZEKS_Default_Case_Owner_Id__c,CTS_OM_Americas_Default_Case_Owner_Id__c,CTS_OM_Email_Case_Record_Type_IDs_1__c, CTS_OM_Email_Case_Record_Type_IDs_2__c, CTS_OM_Email_Case_Record_Type_IDs_3__c, CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c,CTS_OM_NWC_Default_Case_Owner_Id__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case' LIMIT 1];
        
        
        //Email_Message_Setting__mdt settings= Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
        //end
        Id ZEKSdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_ZEKS_Default_Case_Owner_Id__c) ? settings.CTS_OM_ZEKS_Default_Case_Owner_Id__c : null;
        Id AmericasdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_Americas_Default_Case_Owner_Id__c) ? settings.CTS_OM_Americas_Default_Case_Owner_Id__c : null;
        Id NWCdefaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_OM_NWC_Default_Case_Owner_Id__c) ? settings.CTS_OM_NWC_Default_Case_Owner_Id__c : null;
        for(RecordType rt:RecordTypeUtilityClass.getRecordTypeList('Case')){
            if(rt.DeveloperName=='CTS_Non_Warranty_Claims'||rt.DeveloperName=='CTS_Non_Warranty_Claims_Closed' ){
                if(rt.DeveloperName=='CTS_Non_Warranty_Claims'){
                    NWCRtId = rt.Id;
                }
                NWCRtIdSet.add(rt.Id);
            }
            /*if(rt.DeveloperName=='CTS_OM_ZEKS'||rt.DeveloperName=='CTS_OM_ZEKS_Closed' ){
                if(rt.DeveloperName=='CTS_OM_ZEKS'){
                    ZEKSRtId = rt.Id;    
                }
                ZEKSRtIdSet.add(rt.Id); 
            }
            if(rt.DeveloperName=='CTS_OM_Americas' || rt.DeveloperName=='CTS_OM_Americas_Closed'||rt.DeveloperName=='CTS_OM_Americas_Transfers'){
                if(rt.DeveloperName=='CTS_OM_Americas'){
                    AmericasRtId = rt.Id;    
                }
                AmericasRtIdSet.add(rt.Id);
            }*/
            if(rt.DeveloperName=='Action_Item'||rt.DeveloperName=='Internal_Case'){
                AIRTIdSet.add(rt.Id);
            }
        }
        /*End of ZEKS Split changes*/        
        system.debug('<<<<<<AIRTIdSet<<<<'+AIRTIdSet);
        Set<Id> caseIds = new Set<Id>();
        List<EmailMessage> emailsToProcess = new List<EmailMessage>();
        
        for (EmailMessage email : emails){
            //Need to filter out Power Tools Opportunity emails
            if (email.Incoming ) { //&& !isPowerToolsOpportunityEmail(email.Subject)){ //Commented as part of descoped data in august by Capgemini
                emailsToProcess.add(email);
            }
        }
        //system.debug('<<<<<<emailsToProcess<<<<'+emailsToProcess);
        
        if (!emailsToProcess.isEmpty()){
            Map<Id, EmailMessage> parentIdEmailMessage = new Map<Id, EmailMessage>();
            Set<String> fromAddressEmailList = new Set<String>();
            for (EmailMessage email : emailsToProcess){
                if(email.ParentId!=null){
                    caseIds.add(email.ParentId);
                }
                parentIdEmailMessage.put(email.ParentId, email);
                fromAddressEmailList.add(email.FromAddress);
            }
            
            Map<Id, Case> openChildCases = new Map<Id, Case>();
            /*Updated by Aman kumar Capgemini.
            * US SFAITSA-430 Changes START.
            */            
            if (!caseIds.isEmpty()){
                
                // Filter out cases to only ones that are closed and have applicable record types
                /* Added values for enhancement case : #03195484 by AMS Team (Darshan) - Sept24 Release */
                Map<Id, Case> cases = new Map<Id, Case>([Select Id, OwnerId, Owner.isActive, team__r.Name ,Parent.team__c,Team__c,Company_Brand__c,GDI_Department__c,Product_Category__c,Brand__c,Assigned_From_Address_Picklist__c,RecordTypeId, ContactId,Contact.email, AccountId, Subject,Status,ParentId,Parent.Status,
                                                         Parent.OwnerId,Parent.ContactId,Parent.AccountId,Parent.Subject,Parent.Description,Parent.RecordTypeId,CTS_TechDirect_Category__c,CTS_TechDirect_Sub_Category__c,AssetId,parent.contact.email,Parent.CTS_TechDirect_Category__c,Parent.CTS_TechDirect_Sub_Category__c
                                                         From Case Where Id in :caseIds and RecordTypeId in :caseRecordTypeIds and Status = 'Closed']);
                
                contact con = new Contact();
                if(!fromAddressEmailList.isEmpty())
                    try{
                        con = [Select id, email, accountid from contact where email IN: fromAddressEmailList LIMIT 1];
                    }
                catch(System.QueryException qe){
                    
                }
                catch(Exception e){
                    
                }
                
                 // To get the latest related case.
                /* Added if condition to check if current case is closed then check for related case : #03245535 */
                if(!cases.isEmpty()){
                    getLatestCaseNode(new List<Id>(cases.keySet())[0]);
                }else{
                    previousCaseId = new List<Id>(caseIds)[0];
                }
                
                // Get the Status of Latest Case.
                List<Case> latestCase = new List<Case>();
                latestCase = [Select id,Status,ContactId,contact.email,AccountId,Subject,Team__r.name,Assigned_From_Address_Picklist__c,Brand__c,Product_Category__c,GDI_Department__c,Company_Brand__c,Description,RecordTypeId,OwnerId,ParentId,assetid,CTS_TechDirect_Category__c,CTS_TechDirect_Sub_Category__c,Team__c,Owner.isActive From Case where id =:previousCaseId]; 
                
                // Create a new case for each case associated
                Case[] newCases = new Case[]{};
                    //Added to get the queueID related to team value of case record
                    Set <String> allTargetTeams = new set<String>();
                for(Case cs : cases.values()){
                    allTargetTeams.add(cs.team__r.Name);
                }
                Map<String, Id> groupNameById = New Map<String,Id>();
                /* Updated and optimised code for Case-03118925 by AMS team (Darshan) */
                for(Group gp : [SELECT Id, Name FROM GROUP WHERE Name IN: allTargetTeams AND Type = 'Queue']){
                    groupNameById.put(gp.name, gp.Id);
                }
                
                for (Case c : cases.values()){
                    
                    // Do not create a new child case if an open one already exists
                    Case openChildCase = openChildCases.get(c.Id);
                    
                    if (openChildCase == null){
                        if((AIRTIdSet.contains(c.RecordTypeId) && (c.Status=='Closed' || c.Status=='Closed as Merged') && (c.Parent.Status=='Closed' || c.Parent.Status == 'Closed as Merged')) || isTest){
                            System.debug('entered ext');
                            //Added this code for hypercare defect SGOM-597 to populate Contact from related Case of contact email id is same as sender
                            Id conId;
                            Id accId;
                            if(con.id != null && c.Parent.ContactId != null && con.email == c.Parent.contact.email){
                                conId = c.Parent.contact.id;
                                accId = c.Parent.accountId;
                            }else if(con.id != null){
                                conId = con.id;
                                accId = con.AccountId;
                            }
                            
                            Case newCase = new Case(
                                OwnerId = c.Parent.OwnerId, 
                                Related_Case__c = c.ParentId,
                                Email_Waiting_Icon__c=true,
                                RecordTypeId =c.Parent.RecordTypeId,
                                Status='New', // US_1215
                                Origin='Email',
                                ContactId = conId,
                                CTS_TechDirect_Category__c = c.Parent.CTS_TechDirect_Category__c,
                                CTS_TechDirect_Sub_Category__c = c.Parent.CTS_TechDirect_Sub_Category__c,
                                Company_Brand__c= latestCase[0].Company_Brand__c,
                                GDI_Department__c= latestCase[0].GDI_Department__c,
                                Product_Category__c= latestCase[0].Product_Category__c,
                                Brand__c= latestCase[0].Brand__c,
                                Assigned_From_Address_Picklist__c= latestCase[0].Assigned_From_Address_Picklist__c,
                                AssetId = c.AssetId,
                                AccountId = accId,
                                Subject = parentIdEmailMessage.get(c.id).subject,
                                Description = c.Parent.Description != null ? c.Parent.Description :String.isNotBlank(parentIdEmailMessage.get(c.Id).TextBody) ? parentIdEmailMessage.get(c.Id).TextBody.left(32000) : '',
                                Team__c = c.Parent.Team__c //Case #03409124: Populating Team from Parent case of AI.
                            );
                            
                            System.debug('DML Options');
                            Database.DMLOptions dmo = new Database.DMLOptions();
                            dmo.assignmentRuleHeader.useDefaultRule  = false;
                            newCase.setOptions(dmo);
                            System.debug(dmo);
                            newCases.add(newCase);
                        }
                        else{
                            if(!AIRTIdSet.contains(c.RecordTypeId) && !latestCase.isEmpty() && latestCase[0] != null && (latestCase[0].status == 'Closed' || latestCase[0].status == 'Closed as Merged')){
                                /*Start of ZEKS Split and NWC changes*/
                                if(AmericasRtIdSet.Contains(c.RecordTypeId))/*{
                                    recordTypeId = AmericasRtId;
                                    defaultCaseOwnerId = ZEKSdefaultCaseOwnerId;
                                    }*/
                                    if(ZEKSRtIdSet.Contains(c.RecordTypeId)){
                                        recordTypeId = ZEKSRtId;
                                        defaultCaseOwnerId = AmericasdefaultCaseOwnerId;
                                    }
                                
                                if(NWCRtIdSet.Contains(c.RecordTypeId)){
                                    recordTypeId = NWCRtId;
                                    defaultCaseOwnerId = NWCdefaultCaseOwnerId;
                                }
                                //Added this code for hypercare defect SGOM-597 to populate Contact from related Case of contact email id is same as sender
                                Id conId;
                                Id accId;
                                if(con.id != null && latestCase[0].ContactId != null && con.email == latestCase[0].contact.email){
                                    conId = latestCase[0].contact.id;
                                    accId = latestCase[0].accountId;
                                }else if(con.id != null){
                                    conId = con.id;
                                    accId = con.accountId;
                                }
                                
                                /*End of ZEKS Split changes*/
                                Case newCase = new Case(
                                    /* Case - 03025498 by AMS Team (Sandeep) */
                                    /* Added more logic in OwnerId by AMS Team (Darshan) - Case #03118925 */
                                    //OwnerId = (latestCase[0].OwnerId != null && latestCase[0].Owner.isActive) ? latestCase[0].OwnerId :(c.Owner.isActive || defaultCaseOwnerId == null || c.OwnerId.getSObjectType() == Group.getSObjectType()) ? c.OwnerId : defaultCaseOwnerId,
                                    OwnerId = (latestCase[0].Team__c != null && latestCase[0].Team__r.name != null && groupNameById.containsKey(latestCase[0].Team__r.name)) ? groupNameById.get(latestCase[0].Team__r.name) : (c.Owner.isActive || defaultCaseOwnerId == null || c.OwnerId.getSObjectType() == Group.getSObjectType()) ? c.OwnerId : defaultCaseOwnerId, 
                                    
                                    Related_Case__c = latestCase[0].id != null ? latestCase[0].id : c.id,
                                    RecordTypeId = latestCase[0].RecordTypeId != null ? latestCase[0].RecordTypeId :/*recordTypeId != null ? recordTypeId :*/ c.RecordTypeId,
                                    ContactId = conId,
                                    AccountId = accId,
                                    Status='New',
                                    Team__c = latestCase[0].Team__c,
                                    CTS_TechDirect_Category__c = latestCase[0].CTS_TechDirect_Category__c,
                                    CTS_TechDirect_Sub_Category__c = latestCase[0].CTS_TechDirect_Sub_Category__c,
                                    Company_Brand__c= latestCase[0].Company_Brand__c,
                                    GDI_Department__c= latestCase[0].GDI_Department__c,
                                    Product_Category__c= latestCase[0].Product_Category__c,
                                    Brand__c= latestCase[0].Brand__c,
                                    Assigned_From_Address_Picklist__c= latestCase[0].Assigned_From_Address_Picklist__c,
                                    AssetId = latestCase[0].assetId,
                                    Subject = parentIdEmailMessage.get(latestCase[0].id) != null ? parentIdEmailMessage.get(latestCase[0].id).subject :latestCase[0].Subject,
                                    Description = latestCase[0].Description != null ? latestCase[0].Description :String.isNotBlank(parentIdEmailMessage.get(c.Id).TextBody) ? parentIdEmailMessage.get(c.Id).TextBody.left(32000) : '',
                                    Origin = 'Email'
                                );
                                system.debug('****** case owner is*****'+ newCase.OwnerId);
                                //CK 7/5/22 Added logic to set email if contact not found
                                if(con.Id == null){
                                    newCase.SuppliedEmail = emailsToProcess[0].FromAddress;
                                }
                                System.debug('DML Options');
                                Database.DMLOptions dmo = new Database.DMLOptions();
                                dmo.assignmentRuleHeader.useDefaultRule  = false;
                                newCase.setOptions(dmo);
                                System.debug(dmo);
                                newCases.add(newCase);
                            }
                        }
                    }
                    else if(!latestCase.isEmpty() && latestCase[0] != null && latestCase[0].status == 'Open'){
                        latestCase[0].Email_Waiting_Icon__c = true;
                        newCases.add(latestCase[0]);
                    }
                }
                
                if (newCases.size() > 0){
                    System.debug('upsert operation');
                    Database.upsert(newCases, false);
                }
                
                // Build a map of parent Id and child Id
                Map<Id, Id> parentIdChildId = new Map<Id, Id>();    
                
                for (Case newCase : newCases){
                    if (newCase.Id != null && newCase.id != latestCase[0].id){
                        parentIdChildId.put(newCase.Related_Case__c, newCase.Id);
                    }
                }       
                
                for (EmailMessage email : emailsToProcess){
                    if(parentIdChildId == null || parentIdChildId.isEmpty()){
                        email.ParentId =  previousCaseId;                            
                    }
                    for(String newCase : parentIdChildId.keySet()){
                        if(previousCaseId == newCase){
                            email.ParentId =  parentIdChildId.get(previousCaseId);                            
                        }
                        else{
                            email.ParentId = parentIdChildId.get(newCase);
                        }
                        /*Updated by Aman kumar Capgemini.
                        * US SFAITSA-430 Changes END.
                        */
                    }
                }
            }    
        }
    }    
    
    public static void preventDuplicateEmails(List<EmailMessage> emails, Set<Id> caseRecordTypeIds){
        
        system.debug('---preventDuplicateEmails');            
        
        Map<Id, Case> caseMap = getCaseMap(emails, caseRecordTypeIds);
        
        if (!caseMap.isEmpty()){
            
            List<EmailMessage> emailsFromThreads = new List<EmailMessage>();
            
            for (EmailMessage email : emails) {
                //System.debug('===== Email fields: '+email.Incoming+' - '+email.ParentId+' - '+email.ParentId.getSObjectType()+' - '+email.Headers+' - '+caseMap.keySet().contains(email.ParentId));
                // Process incoming case emails with headers
                if (email.Incoming && email.ParentId != null && email.ParentId.getSObjectType() == Case.getSObjectType() && email.Headers != null && caseMap.keySet().contains(email.ParentId)) {
                    
                    system.debug('Case ID: ' + email.ParentId);
                    //system.debug('Headers are:\n' + email.Headers);
                    system.debug('before thread: ' + email.Thread_ID__c);
                    String headersUpper = email.Headers.toUpperCase();
                    
                    // Check to see if this is the original email in a thread
                    if (headersUpper.substringBetween('MESSAGE-ID: <', '>') != null){
                        
                        email.Message_ID__c = headersUpper.substringBetween('MESSAGE-ID: <', '>');
                        email.Thread_ID__c = headersUpper.substringBetween('MESSAGE-ID: <', '>');
                        system.debug('Found original/first message in thread: ' + email.Thread_ID__c);
                    }
                    
                    // Check to see if this is a subsequent/reply/forward email from a thread
                    if (email.ParentId != null && headersUpper.substringBetween('REFERENCES: <', '>') != null) {
                        system.debug('In Duplicate email------------>');
                        email.Thread_ID__c = headersUpper.substringBetween('REFERENCES: <', '>');
                        //system.debug('===== Subsequent email.Thread_ID__c: ' + email.Thread_ID__c);
                        emailsFromThreads.add(email);
                    }
                    
                    system.debug('Message ID: ' + email.Message_ID__c);
                    system.debug('Thread ID: ' + email.Thread_ID__c);
                }           
            }   
            
            if (!emailsFromThreads.isEmpty()){              
                
                //system.debug('Processing emails from threads: ' + emails);
                
                Set<String> threadIds = new Set<String>();
                Set<String> originalParentIds = new Set<String>();
                
                for (EmailMessage em : emailsFromThreads){
                    if (String.isNotBlank(em.Thread_ID__c)){
                        threadIds.add(em.Thread_ID__C);
                        originalParentIds.add(em.ParentId);
                    }
                }
                
                Map<String, Id> threadParentCaseMap = new Map<String, Id>();
                
                // Find the original case (if this isn't the first email) that came from the same thread
                for (EmailMessage em : [Select Id, Subject, ParentId, Thread_ID__c From EmailMessage Where ParentId != null and Thread_ID__c in :threadIds ORDER BY LastModifiedDate DESC ]){
                    
                    if (!threadParentCaseMap.containsKey(em.Thread_ID__c)){
                        threadParentCaseMap.put(em.Thread_ID__c, em.ParentId);
                    }
                }
                
                system.debug('threadParentCaseMap: ' + threadParentCaseMap);
                
                if (!threadParentCaseMap.isEmpty()){
                    
                    Map<Id, Case> originalParentCaseMap = new Map<Id, Case>([Select Id, CaseNumber, IsClosed From Case Where Id in :originalParentIds]);
                    
                    Case[] casesToDelete = new Case[]{};
                        //System.debug('=====emailsFromThreads.size() :'+emailsFromThreads.size());
                        // Prevent the email (and case) from being submitted into the database if there is already a case from the same thread
                        for (EmailMessage em : emailsFromThreads){
                            
                            Id parentCaseId = threadParentCaseMap.get(em.Thread_ID__c);
                            //System.debug('=====parentCaseId :'+parentCaseId);
                            //System.debug('=====em.ParentId :'+em.ParentId);
                            
                            if (String.isNotBlank(em.Thread_ID__c) && parentCaseId != em.ParentId){
                                
                                //system.debug('Parent case found: ' + parentCaseId + ' for email: ' + em);
                                
                                // Block the new/duplicate case from being created
                                // Skip if the original case is closed
                                Case originalParentCase = originalParentCaseMap.get(em.ParentId);
                                system.debug('originalParentCase: ' + originalParentCase);
                                
                                if (originalParentCase != null && !originalParentCase.IsClosed){
                                    casesToDelete.add(new Case(Id = originalParentCase.Id));
                                    //Reparent the email's case to the original from the thread
                                    em.ParentId = parentCaseId; 
                                }
                                // Commented for case duplication issue
                                //em.ParentId = parentCaseId;
                            }
                        }                       
                    
                    system.debug('casesToDelete: ' + casesToDelete);
                    
                    if (!casesToDelete.isEmpty()){
                        Database.delete(casesToDelete, false);
                    }
                }
            }
        }
    }   
    
    // Query cases to get record types so emails not related to the proper case record types can be filtered out of processing
    private static Map<Id, Case> getCaseMap(List<EmailMessage> emails, Set<Id> caseRecordTypeIds){
        
        Set<Id> caseIds = new Set<Id>();
        
        for (EmailMessage email : emails) {
            // Process incoming case emails with headers
            if (email.Incoming && email.ParentId != null && email.ParentId.getSObjectType() == Case.getSObjectType()) {
                caseIds.add(email.ParentId);
            }
        }
        
        Map<Id, Case> caseMap = new Map<Id, Case>([Select Id, Subject From Case Where Id in :caseIds and RecordTypeId in :caseRecordTypeIds]);    
        
        system.debug('caseMap: ' + caseMap);
        
        return caseMap;
    }     
    
    /*Craeted By Aman Kumar Capgemini.
    * To get the Latest retalted Case of the Case on which the mail is coming using recursion.
    * Revisions: 10-Sept-2024: #03237971 : SOQL error removeal from recurrsive loop : AMS Team
    */
    private static void getLatestCaseNode (String mainCaseId){
        List<Case> caseId = new List<Case>();
        setCheckRecursiveCaseId = new Set<Id>();
        /* If set contains the case id then we have to return it because it is recursive loop which is giving SOQL 101 error : #03237971 */
        if(setCheckRecursiveCaseId.contains(mainCaseId))
            return;
        setCheckRecursiveCaseId.add(mainCaseId);
        
        caseId = [Select id,Related_Case__c From Case where Related_Case__c =: mainCaseId];
        
        if(!caseId.isEmpty() && caseId[0] != null){
            counter++;
            previousCaseId = caseId[0].id ;
            getLatestCaseNode(caseId[0].id);
        }
        if(caseId.isEmpty() && counter == 0){
            previousCaseId = mainCaseId;
        }
    }
}