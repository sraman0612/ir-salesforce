global class CC_BatchAccountTeamLastUpdatedPartner implements Database.Batchable<sObject>{
	global final string queryStr;  
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        
        if(queryStr != null && queryStr != ''){
            query = queryStr;
        }
        else {
            Datetime lastday = datetime.now().addhours(-24);
            // collect the batches of records or objects to be passed to execute
            query = ' SELECT Id,AccountFromId,AccountToId,IsDeleted,AccountFrom.lastModifiedDate, '+
                    ' CreatedDate,LastModifiedDate,accountFrom.CC_AccountTeamLastUpdate__c FROM Partner '+
                    ' WHERE LastModifiedDate >= yesterday ALL ROWS';
        }
      return Database.getQueryLocator(query);
    }
    
    
    public void execute(Database.BatchableContext BC, List<Partner> scope){
      List<Account> accListToUpdate = new List<Account>();
      Set<Id> acctIds = new Set<Id>();
      
      for(Partner singlePartner : scope){
        if (singlePartner.LastModifiedDate >= singlePartner.accountFrom.lastModifiedDate){
          acctIds.add(singlePartner.AccountToId);
        }
      }
      
      if(!acctIds.isEmpty()){
        for (Id aid : acctIds){
          Account acc = new Account();
          acc.Id = aid;
          acc.CC_AccountTeamLastUpdate__c = system.today();
          accListToUpdate.add(acc);
        }
      }
      
      Database.SaveResult[] srList = Database.update(accListToUpdate, false);//Database method to update the records in List
        // Iterate through each returned result by the method
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // This condition will be executed for successful records and will fetch the ids of successful records
                System.debug('Successfully Updated Account ID: ' + sr.getId());//Get the invoice id of inserted Account
            }
           else {
                // This condition will be executed for failed records
                for(Database.Error objErr : sr.getErrors()) {
                    System.debug('The following error has occurred.');  //Printing error message in Debug log
                    System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                    System.debug('Fields which are affected by the error: ' + objErr.getFields());
                }
            }
        }    
  }
    
    public void finish(Database.BatchableContext BC){
      
      
    }
  

}