public class AccountQuaueableUpdateFlag implements System.Queueable {

    private final List<Account> accList;
    
    public AccountQuaueableUpdateFlag(List<Account> accoutList){
        system.debug('accoutList==='+accoutList);
        this.accList = accoutList;   
        
    }
    public void execute(QueueableContext context) {
        system.debug('Flag updated==='+accList);
        
        Set<Id> accountIds = new Set<Id>();
        for(Account acc:accList){
            if(acc.WasConverted__c){
                accountIds.add(acc.id);
            }
        }        
        system.debug('accountIds==='+accountIds);
        List<Account> accToUpdate= [select id,ShippingStreet,ShippingCity,ShippingCountry,ShippingPostalCode,wasconverted__c from Account where id IN :accountIds];
        List<Lead> convertedLeads;
        if(accountIds.size() > 0){
        	convertedLeads = [select id,Street,City,PostalCode,State,Country,ConvertedAccountId from Lead where ConvertedAccountId IN:accountIds];
        }
        system.debug('Updating Shipping Address************'+convertedLeads);
        List<Account> updateAccList = new List<Account>();
        if(convertedLeads <> null){
        for(Account acc:accToUpdate){
            for(Lead led :convertedLeads){
                if(acc.WasConverted__c && acc.id == led.ConvertedAccountId){
                    if(led.Street != null && led.Street != '')
                    	acc.ShippingStreet = led.Street;
                    if(led.City != null && led.City != '')
                    	acc.ShippingCity = led.City;
                    if(led.PostalCode != null && led.PostalCode != '')
                    	acc.ShippingPostalCode = led.PostalCode;
                    if(led.State != null && led.State != '')
                    	acc.ShippingState = led.State;
                    if(led.Country != null && led.Country != '')
                    	acc.ShippingCountry = led.Country;
                    //acc.wasconverted__c = false;
                    updateAccList.add(acc);
                }                
            }
        }  
    }
      
        update updateAccList;
        system.debug('Flag updated==='+updateAccList);
    }

}