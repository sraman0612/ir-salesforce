/**
* @createdBy   Sangram Ray
* @date         08/01/2016
* @description  Test Class for CC_PavilionConfirmationcnt
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest
public class CC_PavilionConfirmationcnt_Test {
    static PavilionSettings__c pavillionCommunityProfileId;
    static PavilionSettings__c  pavilionPublicGroupID;
    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Account account;
    static CC_Order__c order;
    static CC_Order_Item__c orderItem;
    static PavilionSettings__c hazMatFee;
    static user portalAccountOwner1;
    static Pavilion_Navigation_Profile__c pNavProfile;
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    
    public static void setup(integer i) {
        testUser = createPartnerUser(i);
        
        system.runAs(portalAccountOwner1){
            //AccountTeamMember for Account share error
            
            AccountTeamMember atm = TestUtilityClass.createATM(account.Id,testUser.Id);
            insert atm;
            
            AccountShare accShare = TestUtilityClass.createAccShare(account.Id,testUser.Id);
            insert accShare;
        }    
        
        
    }
    
    // test submitOrder freeFreight
    @isTest
    public static void submitOrder_freeFreight_Test() {
        setup(1);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C1001',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C1001',
                CC_Domestic_MF__c = false,
                CC_Domestic_Parcel__c = false,
                CC_International_MF__c = false,
                CC_International_Parcel__c = false,
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            

            
            CC_PavilionConfirmationcnt.submitOrder(
                order.Id, 
                true, 
                '123456', 
                false,
                true,
                false
            );
        }
    }
    
    // test submitOrder isHazMat
    @isTest
    public static void submitOrder_isHazMat_Test() {
        setup(2);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'HAZMAT',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'HAZMAT',
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            
            /*CC_PavilionConfirmationcnt.determineFreeFreight(
                String oId,
                Boolean shipComplete,
                String PONum,
                Boolean suspended,
                Boolean freeFreight,
                boolean isHazMat
            );*/
            
            CC_PavilionConfirmationcnt.submitOrder(
                order.Id, 
                true, 
                '123456', 
                false,
                false,
                true
            );
        }
    }
    
    // test determineFreeFreight carrier OCEAN
    @isTest
    public static void determineFreeFreight_carrier_OCEAN_Test() {
        setup(3);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C123',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C123',
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            

            
            String shipDate = '12/27/2009';
            
            CC_PavilionConfirmationcnt.determineFreeFreight(
                'Dealer',
                'Channel Partner', 
                'OCEAN', 
                'High', 
                'USA', 
                1000, 
                shipDate, 
                'IN', 
                true, 
                true,
                true,
                true,
                '12345'
            );
        }
    }
    
    // test determineFreeFreight
    @isTest
    public static void determineFreeFreight_Test() {
        setup(4);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C123',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C123',
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            
  
            
            String shipDate = '12/27/2009';
            
            CC_PavilionConfirmationcnt.determineFreeFreight(
                'Dealer', 
                'Channel Partner',
                'UPSR', 
                'High', 
                'USA', 
                1000, 
                shipDate, 
                'US', 
                true, 
                true,
                false,
                true,
                '12345'
            );
        }
    }
    
    // test getOrderItems
    @isTest
    public static void getOrderItems_Test() {
        setup(5);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert orderItem
            orderItem = new CC_Order_Item__c(
                CC_Product_Code__c = 'C123',
                CC_Description__c = 'Test Desc',
                CC_UnitPrice__c = 10,
                CC_Quantity__c = 1,
                CC_StockAvailability__c = 'In Stock',
                CC_TotalPrice__c = 10,
                Order__c = order.Id
            );
            insert orderItem;
            
            // insert product
            Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'C123',
                RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionConfirmationcnt.getOrderItems(order.Id);
        }
    }
    
    // test getOrderHeader
    @isTest
    public static void getOrderHeader_Test() {
        setup(6);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionConfirmationcnt.getOrderHeader(order.Id);
        }
    }
    
    // test constructor
    @isTest
    public static void constructor_Test() {
        setup(7);
        System.runAs (testUser) {
            hazMatFee = new PavilionSettings__c(
                Name = 'HAZMATPRICE',
                Value__c = '10'
            );
            insert hazMatFee;
            
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                isSystemCreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionOrderConfirmation'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionConfirmationcnt cc_PavilionConfirmationcnt = new CC_PavilionConfirmationcnt(cc_PavilionTemplateController);
        }
    }
    
       public static  user createPartnerUser(integer i){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
         portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='User', Lastname='Deactivated',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           

            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
              //creating state country custom setting data
            TestDataUtility.createStateCountries();
            
            pNavProfile = new Pavilion_Navigation_Profile__c(
            Name = 'ccircotst',
            Description__c = 'Test',
            Region__c = 'Test',
            Agreement_Sub_Type__c = 'Test',
            Display_Name__c = 'Test'
        );
        insert pNavProfile;

             //creating sales rep here
             TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;
            
           
            account = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
            account.CC_Sales_Rep__c = salesRepacc.Id;
    account.Name='Test';
   account.BillingCountry = 'USA';
            account.BillingCity = 'Augusta';
            account.BillingState = 'GA';
            account.BillingStreet = '2044 Forward Augusta Dr';
            account.BillingPostalCode = '566';
            account.CC_Shipping_Billing_Address_Same__c = true;
            account.CC_Customer_Number__c = '5621'+i;
    insert account;

            //Create contact
            contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = account.Id,
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com',
                                 RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId());
            Database.insert(contact);

            //Create user\
            Profile portalProfile = [SELECT Id FROM Profile where name ='CC_PavilionCommunityUser' LIMIT 1];
             testUser = new User(Username = System.now().millisecond() + 'test123456@test.ingersollrand.com',ProfileId = portalProfile.id,Alias = 'test1236',ContactId=contact.Id,
                        Email = 'test123456@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test123456',
                        TimeZoneSidKey = 'America/Los_Angeles',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',CC_Pavilion_Navigation_Profile__c='ccircotst',
                        Pavilion_Tiles__c = ';Custom Solutions Project Request;CustomerVIEW;Marketing Events;PreApproved Requests;Transportation Requests;Marketing_MyCo-OpAccount;Marketing Hole-In-One');
            
            //Added by @Priyanka Baviskar for ticket #8077192: Please add the Active and Partner User Checkmarks on Contact layout on 3/15/2019      
            Test.startTest();
    insert testUser;
            Test.stopTest();
        }
       return testUser; 
    }
    
}