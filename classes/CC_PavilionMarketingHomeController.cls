public class CC_PavilionMarketingHomeController {
    public CC_Pavilion_Content__c HomePageText{get;set;}

    public CC_PavilionMarketingHomeController(CC_PavilionTemplateController controller) {
    
    HomePageText=[select CC_Body_1__c,CC_Body_2__c,Link__c  from CC_Pavilion_Content__c where  Title__c='Marketing Home Text' and  RecordType.Name='Home Page Text'];
    
    }
    
}