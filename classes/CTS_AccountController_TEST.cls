@istest
public class CTS_AccountController_TEST {
    
  public testmethod static void test1(){
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct 1';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
    a.ShippingStreet = '13, street2';
    a.ShippingPostalCode = '123';  
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
    a.Account_Division__c='a2c4Q000006rW5NQAU'; //Ritesh:Added division to avoid division validation on Accounts.
    
    insert a;
    Id airOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('NA_Air').getRecordTypeId();
    Opportunity o = new Opportunity();
    o.name = 'Test Opportunity';
    o.stagename = 'Qualify';
    o.amount = 1000000;
    o.closedate = system.today();
    o.RecordTypeId = airOppRTID;
    o.AccountId = a.Id;
      
    insert o;    
    test.startTest();
    //CTS_AccountController.setBillAccount(o.Id,a.id); Ritesh: Commented and added below line to select correct bill to
    CTS_AccountController.setBillAccount(o.Id,'0014Q00002H5f4GQAR');
    CTS_AccountController.fetchAccts('abc123');
    test.stopTest();
  }
}