global class C_Utils{

   
    webservice static String saveCase(String CaseId){
        try{
            Case opp = new Case(Id=CaseId);
            opp.Status = 'Closed';
            update opp;
        }
        catch(DMLException e){
            return e.getDmlMessage(0);
        }
        return '-';
    }   
    
    
    global static void duplicateAttachments(List<Id> attachmentIds){
        System.debug(attachmentIds);
        List<Id> parentIds = new List<Id>();
        List<Attachment> theAttachments = [SELECT Name, ParentId, Body, ContentType FROM Attachment WHERE Id IN : attachmentIds];
        
        for(Attachment a : theAttachments){
            parentIds.add(a.ParentId);
        }
        
        Map<Id, Case> theCases = new Map<Id,Case>([SELECT Id, ParentId FROM Case WHERE Id IN :parentIds AND ParentId != null]);
        
        List<Attachment> clones = new List<Attachment>();
        
        for(Attachment a : theAttachments){
            Attachment newAttachment = a.clone();
            Case c = theCases.get(a.ParentId);
            if(c != null){
                newAttachment.ParentId = c.ParentId;
                clones.add(newAttachment);
            }
        }
        
        if(clones.size() > 0){
            insert clones;
        }
    }
    
   @InvocableMethod
    global static void sendTemplatedEmail(List<String> Ids){
         System.debug('entered');
		
        Case theCase = [SELECT Id, ContactId,RecordTypeId, RecordTypeName__c ,Origin, Status, IsNew__c,/* CG_Org_Channel__c, */Contact.AccountId, SuppliedEmail, 
                        Org_Wide_Email_Address_ID__c, New_Case_Notification_Sent_to_Contact__c  FROM Case WHERE Id =: Ids[0]];
        
        //String recordtypeName = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosById().get(thecase.RecordTypeId).getName();
        
        System.debug('the Case '+theCase);
        Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage();
        if(theCase.Origin == 'Email'){
            try{
                EmailMessage em = [SELECT Id, ToAddress, CcAddress FROM EmailMessage WHERE Incoming = TRUE AND ParentId =: theCase.Id ORDER BY CreatedDate ASC LIMIT 1];
                System.debug('EmailMessage - '+em);
                List<String> addresses = new List<String>();
                if(!String.isEmpty(em.CcAddress)){
                    addresses.addAll(em.CcAddress.split(';'));
                }      
                theEmail.setCCAddresses(addresses);
            }
            catch(Exception e){
                System.debug(e);
            }
        }
        System.debug('after catch ');
        System.debug('theEmail - '+theEmail);
        Email_Template_Admin__c ET = Email_Template_Admin__c.getOrgDefaults();
        //Map<String, IR_Email_Template__mdt> EmailTemplate = IR_Email_Template__mdt.getAll(); 
        Map<String, IR_Email_Template__mdt> EmailTemplate = new Map<String, IR_Email_Template__mdt>([Select Id, Record_Type__c, Criteria__c, Template_Id__c from IR_Email_Template__mdt]);
        String[] toAddresses =new List<String>{theCase.SuppliedEmail};
        theEmail.setWhatId(theCase.Id);
        
        if(Test.isRunningTest()){
            theEmail.setPlainTextBody('test body');
            theEmail.setToAddresses(new List<String>{'service-gdi@canpango.com'});
        }
        else{
           /* if( theCase.CG_Org_Channel__c =='GD'){
                if(theCase.Contact.AccountId != null && theCase.Contact.AccountId != '0015C00000lEUFe'){
                    theEmail.setTargetObjectId(theCase.ContactId);
                    theEmail.setTemplateId(String.valueOf(ET.get('Case_With_Contact__c')));
        	
                }
                else{
                    theEmail.setTargetObjectId(theCase.ContactId);
                    theEmail.setTemplateId(String.valueOf(ET.get('Case_Without_Contact__c')));
        
                }
            }*/
           // else
            //{
                for(String temp: EmailTemplate.KeySet()){
                    if((EmailTemplate.get(temp).Record_Type__c) == theCase.RecordTypeName__c){
                        if(EmailTemplate.get(temp).Criteria__c == 'Creation' && theCase.Status == 'New'){
                            theEmail.setTargetObjectId(theCase.ContactId);
                    		theEmail.setTemplateId(String.valueOf(EmailTemplate.get(temp).Template_Id__c));
                        }
                        else if(EmailTemplate.get(temp).Criteria__c == 'Updation' && theCase.Status != 'New' && theCase.Status != 'Closed'){
                            theEmail.setTargetObjectId(theCase.ContactId);
                    		theEmail.setTemplateId(String.valueOf(EmailTemplate.get(temp).Template_Id__c));
                        }
                        else{
                            if(EmailTemplate.get(temp).Criteria__c == 'Closed' && theCase.Status == 'Closed'){
                            theEmail.setTargetObjectId(theCase.ContactId);
                    		theEmail.setTemplateId(String.valueOf(EmailTemplate.get(temp).Template_Id__c));
                        	}
                        }
                    }
                }
                
            //}
    	}
        System.debug('theEmail >>>: ' + theEmail);
        System.debug('theCase.Org_Wide_Email_Address_ID__c: ' + theCase.Org_Wide_Email_Address_ID__c);
        theEmail.setOrgWideEmailAddressId(theCase.Org_Wide_Email_Address_ID__c);
        theEmail.setUseSignature(false);
        System.debug('theEmail >>>: ' + theEmail);
        System.debug('RecordTypeName >>>: ' + theCase.RecordTypeName__c);
        if(theCase.RecordTypeName__c == 'CTS_LATAM_Brazil_Application_Engineering' || theCase.RecordTypeName__c == 'Customer_Care' || theCase.RecordTypeName__c == 'High_Pressure' || 
      	   theCase.RecordTypeName__c == 'Belliss_Morcom' || (theCase.Origin == 'Web Inquiry' && ( 
           theCase.RecordTypeName__c == 'CTS_TechDirect_Ask_a_Question'  || theCase.RecordTypeName__c == 'CTS_TechDirect_Issue_Escalation'  || theCase.RecordTypeName__c == 'CTS_TechDirect_Start_Up') ))
        {
             Messaging.SendEmailResult[] theResult = Messaging.sendEmail(new Messaging.Email[]{theEmail}, true);
        }
        theCase.New_Case_Notification_Sent_to_Contact__c = true;
        update theCase;
    }
    
    public static void updateParentStatus(String caseId){
        Case parentCase = new Case();
        parentCase.Id = caseId;
        parentCase.Status = 'Pending Response';          
        update parentCase;                        
    }        
    
}