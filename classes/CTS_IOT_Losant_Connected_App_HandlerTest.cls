/**
 * Test class of CTS_IOT_Losant_Connected_App_Handler
 **/
@isTest
private class CTS_IOT_Losant_Connected_App_HandlerTest {
    
    @testSetup
    static void setup(){
        User accOwner = CTS_TestUtility.createUser(false);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        accOwner.UserRoleId = portalRole.Id;
        Database.insert(accOwner);
        System.runAs(accOwner){
            Account acc = CTS_TestUtility.createAccount('Test Account', false);
            acc.Siebel_Ship_To_Id__c = '1234';
            acc.OwnerId = accOwner.Id;
            insert acc;
            CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
            Contact con = CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false);
            con.CTS_IOT_Community_Status__c = 'Approved';
            Database.insert(con);
            Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        
            User communityUser = CTS_TestUtility.createUser(con.Id, profileId, false);
            Database.insert(communityUser);
        }
    }
    @isTest
    static void testCustomAttributes(){
        List<User> users = [select id from User where Contact.lastName = 'Contact123' limit 1];
        if(!users.isEmpty()){
            System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_Losant_Connected_App_Handler handler = new CTS_IOT_Losant_Connected_App_Handler();
                Map<String,String> attributes = handler.customAttributes(users.get(0).Id,null, new Map<String,String>(), null);
                System.assert(attributes != null);
                System.assert(attributes.get('Language') == 'en_US', 'Actual: '+attributes.get('Language'));
                Test.stopTest();
           }
        }
        
    }
    
}