@isTest
private class RS_CRS_GetAssetModelNumControllerTest {

    static testmethod void testGetAssetModelNum() {
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        //Create test user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id accRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Account', Label.RS_CRS_Person_Account_Record_Type);
        
        //Getting Asset Record Type Id through Schema.Describe Class
        Id assetRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Asset', Label.RS_CRS_Asset_Record_Type);
        
        // This code runs as the system user
        System.runAs(userRec){
            RS_CRS_WebService_Data__c webServiceData = new RS_CRS_WebService_Data__c();
            webServiceData.Name = 'RS_CRS_WebService';
            webServiceData.Get_Asset_Model_Num_URL__c = 'https://www.comfortsite.com/ebiz/Webservices/TraneSupply/TsApi.svc/GetProductBySerial';
            insert webServiceData;
            
            Account acc = new Account();
            acc.recordTypeId = accRecId;
            acc.LastName = 'Test Account 1';
            acc.Phone = '123456789';
            acc.Email_ID__c = 'first.last@email.com';
            insert acc; 
            
            Asset assetRec = new Asset();
            assetRec.recordTypeId = assetRecId;
            assetRec.Name = '16273YUK2F';
            assetRec.SerialNumber = '16273YUK2F';
            assetRec.accountId = acc.Id;
            assetRec.Manufacturer__c = 'Trane';
            insert assetRec;
            
            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new RS_CRS_MockHttpResponseGenerator());
            
            Test.startTest();
            // Call method to test.
            // This causes a fake response to be sent
            // from the class that implements HttpCalloutMock. 
            Boolean isValidResp = RS_CRS_GetAssetModelNumController.getAssetModelNum(assetRec.Id);
            
            Test.stopTest();
            
            System.assertEquals(true, isValidResp); 
            
            Asset asset = [Select id, Model_Name__c From Asset Where Id =: assetRec.Id Limit 1];
            System.assertEquals('4TTR7060A1000BA', asset.Model_Name__c); 
        }
        
    }
}