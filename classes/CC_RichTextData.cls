/**
	@author Ben Lorenz
	@date 26JAN19
	@description Controller for Ltng Cmp CC_RichTextEditor.cmp
*/

public class CC_RichTextData {
	
    @AuraEnabled
    public static Object getData(String objectName, String fields, String filterField, String filterValue) {
        fields = fields.replaceAll(' ', '');
        String q = 'SELECT ' + fields + ' FROM ' + objectName + ' WHERE ' + filterField + ' =: filterValue';
        try {
            List<Object> res = Database.query(q);
        	if(res.size() > 0) return res[0];
            else return null;
        } catch(Exception e) {
            throw new AuraException(e.getMessage());
        }
    }
}