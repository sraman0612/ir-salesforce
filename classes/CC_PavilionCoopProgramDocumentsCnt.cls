public with sharing class CC_PavilionCoopProgramDocumentsCnt {
  public List < ContentVersion > contentList {get;set;}
  
  public CC_PavilionCoopProgramDocumentsCnt(CC_PavilionTemplateController controller) {
    contentList = new List < ContentVersion > ();
    List<ContentVersion> unfilteredContentLst = new List<ContentVersion>();
    Id  usrId  = userinfo.getUserId();
    Account acc = [SELECT Type,CC_Global_Region__c FROM Account WHERE id=:controller.acctId];
    unfilteredContentLst = [SELECT id, Description, Title, CC_Type__c, CC_Region__c, FileType, ContentUrl, CS_Category__c, RecordType.Name,Agreement_Type__c
                              FROM ContentVersion
                             WHERE RecordType.Name = 'ProgramDocuments' and IsLatest = True and  CC_Region__c INCLUDES(:acc.CC_Global_Region__c)];
    //get agreements
    set<string> subtypeSet = new set<string>();
    Contract []  cLst = [SELECT id,name,CC_Sub_Type__c,CC_Type__c 
                           FROM Contract 
                          WHERE AccountId=:acc.id and CC_Type__c='Dealer/Distributor Agreement' and 
                                CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and 
                                CC_Contract_End_Date__c >=:system.today()];
    for(Contract c: cLst){if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != ''){subtypeSet.add(c.CC_Sub_Type__c); }} 
    
    for(ContentVersion cv : unfilteredContentLst){
      for(String s : cv.Agreement_Type__c.split(';')){if(subTypeSet.contains(s)){contentList.add(cv);break;}}
    }
  }
}