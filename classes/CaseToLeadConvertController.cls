/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 07-27-2023
 * @last modified by  : RISHAB GOYAL
**/
public class CaseToLeadConvertController {
    
    private static void setOwnerByBrand(Case MyCase, Lead myLead) {
        
        if (String.isNotBlank(MyCase.CTS_Region__c)){
            myLead.CTS_Lead_Region_Mappin__c  = MyCase.CTS_Region__c;
        }

        if (String.isNotBlank(MyCase.Brand__c)){

            myLead.VPTECH_Brand__c  = MyCase.Brand__c;

            switch on MyCase.Brand__c.toUpperCase() {
                when 'TRICONTINENT' {
                    myLead.OwnerId = MyCase.CTS_Region__c == 'EU' ? Id.valueOf(Label.TriContinent_EMEIA) : Id.valueOf(Label.TriContinent);
                }
                when 'ILS' {
                    myLead.OwnerId = Id.valueOf(Label.TriContinent);
                }
                when 'ZINSSER ANALYTIC' {
                    myLead.OwnerId = Id.valueOf(Label.TriContinent);
                }
                when 'THOMAS' {
                    myLead.OwnerId = Id.valueOf(Label.Thomas);
                }
                when 'OINA' {
                    myLead.OwnerId = Id.valueOf(Label.Thomas);
                }
                when 'WELCH' {
                    myLead.OwnerId = MyCase.CTS_Region__c == 'EU' ? Id.valueOf(Label.Welch_EMEIA) : Id.valueOf(Label.Welch);
                }
                when else {
                    
                }
            }
        }
    }
    
    private static Lead setFields(Lead l, Case MyCase) {
		l.FirstName = MyCase.LFS_First_Name__c;
		l.LastName = MyCase.LFS_Last_Name__c; 
		l.Company = MyCase.SuppliedCompany;
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard Lead').getRecordTypeId();
        setOwnerByBrand(MyCase, l);
        return l;        
    }
    
    private static Case setCase(Case MyCase) {
		MyCase.Sub_Status__c ='Case Converted to Lead';
        MyCase.Type = 'Converted to Lead';
        MyCase.CTS_OM_Category__c = 'Converted to Lead';
        MyCase.CTS_OM_Disposition_Category__c = 'Converted to Lead';
		MyCase.Status = 'Closed';
		return MyCase;
    }
    
    private static boolean requirementsCheck(Case myCase) {
        return String.isNotBlank(myCase.Brand__c) && String.isNotBlank(myCase.SuppliedCompany) && 
            String.isNotBlank(myCase.LFS_First_Name__c) && String.isNotBlank(myCase.LFS_Last_Name__c) && 
            String.isNotBlank(myCase.Origin) && String.isNotBlank(myCase.CTS_Region__c);
    }
    
    @AuraEnabled
    public static string caseMapping(string lead_id) {
        String baseQry = 'ContactId, id FROM Case where id =: lead_id';
        Map<string, string> MapMappingTable = new map<string,string>();
        Case objCase = Database.query('select ' + 'Brand__c, SuppliedCompany, LFS_First_Name__c, LFS_Last_Name__c, Origin, CTS_Region__c,' + baseQry);
        String qry;
        if(requirementsCheck(objCase)) {
            try {
                qry='';
                for (Case_To_Lead__c mappingTableRec : Case_To_Lead__c.getall().Values()) {
                    if (mappingTableRec.Name != null && mappingTableRec.Lead_Field__c != Null ) {
                        MapMappingTable.put(mappingTableRec.Name, mappingTableRec.Lead_Field__c);
                        qry += mappingTableRec.Name + ',';
                    }
                }
                Case MyCase = Database.query('select ' + qry + 'LFS_First_Name__c, LFS_Last_Name__c, SuppliedCompany, Brand__c,' + baseQry);
                Lead ObjLead = new Lead();
                objLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Standard Lead').getRecordTypeId();
                for( String sMyLeadField: MapMappingTable.keySet() ) {
                    String sLeadField = MapMappingTable.get(sMyLeadField);
                    ObjLead.put(sLeadField, MyCase.get(sMyLeadField));
                }	
                insert setFields(ObjLead, MyCase);
                update setCase(MyCase);
                return 'Success';
            } catch(Exception ex) {
                system.debug('ex' + ex);
                return 'Lead Not Created - ' + ex.getMessage();
            }
        } else {
            return 'Please Enter Required Fields: Brand, Company Name, First Name, Last Name, Origin, Region';
        }
    }
}