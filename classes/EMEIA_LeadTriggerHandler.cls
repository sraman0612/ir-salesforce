/* Revisions:

13-Sept-2024 : Updated logic for Case #03210798 by AMS Team
*/

public class EMEIA_LeadTriggerHandler {
    public static Map<Id, String> leadRTMap = new Map<Id, String>();
    public static Map<String, List<Lead>> SBUTriggerNewMap = new Map<String, List<Lead>>();
    public static Map<String, List<Lead>> SBUTriggerOldMap = new Map<String, List<Lead>>();
    public static List<Lead> airNALeadNew = new List<Lead>(); 
    public static Id AirNALeadRT_ID;
    public static Id STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Standard_Lead').getRecordTypeId();
    public static Id NEW_STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('New_Standard_Lead').getRecordTypeId();
    public static Id CTS_NEW_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('CTS_New_Lead').getRecordTypeId();
    public static Id NEW_GLOBAL_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('NA_Air').getRecordTypeId();
    public static Id EMEIA_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('CTS_MEIA_Lead').getRecordTypeId();
    //Changes by Capgemini Devender Singh Date 13/6/2023. START US SGGC-70
    public static Id PT_Lead_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('PT_Lead').getRecordTypeId();
    public static Id Hibon_Lead_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Hibon_Lead').getRecordTypeId();
    public static Id GD_Parent_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent_New').getRecordTypeId();
    public static Id GD_Parent_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
    public static Id End_Customer_RT_ID = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
    public static Id Milton_Roy_RT_ID = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Contact').getRecordTypeId();
    public static Id Hibon_User=System.Label.L_GD_Compressor_Hibon_User_Label; 
    // Changes by Capgemini Devender Singh Date 13/6/2023. END US SGGC-70
    public static Boolean hasRanOnce = false;
    public static User user = new User();
    public static Final String Hibon_Queue='Hibon_Lead';
    
    public static List<String> listOfIRQueues = new List<String>{
        'CTS_AssignmentQueue_Africa','CTS_AssignmentQueue_APAC',
            'CTS_AssignmentQueue_Europe','CTS_AssignmentQueue_Latin_America',
            'CTS_AssignmentQueue_Middle_East','CTSAssignmentQueueIndia'
            };
                public static List<String> listOfGDQueues = new List<String>{
                    'GD_Comp_Assignment_Queue_EU','GD_Comp_Assignment_Queue_MEIA'
                        };
                            // Changes by Capgemini Devender Singh Date 13/6/2023. START Defect VPTECH-80
                            public static List<String> listOfPTLeadQueues = new List<String>{
                                'PT_Leads'
                                    };
                                        // Changes by Capgemini Devender Singh Date 13/6/2023. END Defect VPTECH-80
                                        
                            // Changes by Harish 04/12/2023. START 
                            public static List<String> listOfVPTechLeadQueues = new List<String>{
                                'Hypercare_EMEIA_English','Hypercare_EMEIA_French','Hypercare_EMEIA_German','Hypercare_EMEIA_Italian','Hypercare_EMEIA_Spanish'
                                    };
                                        
                            
                            public static List<String> listOfLFSLeadQueues = new List<String>{
                                'Welch_Queue','ADI','Thomas_Queue','TriContinent_Queue','LS_Assignment_Queue'     
                                    };
                                 // Changes by Harish 04/12/2023. END               
                                        
                                        public static List<Lead> leadTriggerMethod(List<Lead> newLeads){
                                            List<Lead> airNALeadNew = new List<Lead>();
                                            Map<Id, String> leadRTMap = new Map<Id, String>();
                                            //  Map<String, List<Lead>> SBUTriggerNewMap = new Map<String, List<Lead>>();
                                            // Map<String, List<Lead>> SBUTriggerOldMap = new Map<String, List<Lead>>();
                                            Id AirNALeadRT_ID;
                                            
                                            System.debug('leadTriggerMethod');
                                            for (RecordType rt :RecordTypeUtilityClass.getRecordTypeList('Lead')){
                                                System.debug('leadTriggerMethod1');
                                                leadRTMap.put(rt.Id,rt.Name);
                                                SBUTriggerNewMap.put(leadRTMap.get(rt.Id),new List<Lead>());
                                                SBUTriggerOldMap.put(leadRTMap.get(rt.Id),new List<Lead>());
                                                //  if(rt.DeveloperName=='PT_Lead'){AirNALeadRT_ID=rt.Id;}
                                                if(rt.DeveloperName=='NA_Air'){AirNALeadRT_ID=rt.Id;}
                                                
                                            }
                                            if (!newLeads.isEmpty()){
                                                for(Lead newL : newLeads) {
                                                    system.debug('### the lead is ' + newL);
                                                    if(null==newL.RecordTypeId){
                                                        newL.RecordTypeId=AirNALeadRT_ID;
                                                        system.debug('### we updated the record type to NA Air');
                                                        system.debug('record type id: ' + newL.RecordTypeId);
                                                    } 
                                                    SBUTriggerNewMap.get(leadRTMap.get(newL.RecordTypeId)).add(newL);
                                                    if (newL.RecordTypeId==AirNALeadRT_ID || newL.RecordTypeId==STANDARD_LEAD_RT_ID){
                                                        airNALeadNew.add(newL);
                                                    }
                                                     if(newL.CTS_Product_Type__c=='Medical'){
                 airNALeadNew.add(newL);   
				}
                                                }
                                            }
                                            return airNALeadNew;
                                        } 
    
    
    public static void beforeInsert(List<Lead> newLead, List<Lead> oldLead,User user){ 
        
        if (oldLead != null){
            for(Lead oldL : oldLead) {
                SBUTriggerOldMap.get(leadRTMap.get(oldL.RecordTypeId)).add(oldL);}}
        
        airNALeadNew=leadTriggerMethod(newLead); 
        if (!airNALeadNew.isEmpty()){Lead_TriggerHandler.beforeInsert(airNALeadNew);}
        system.debug('****size of SBUTriggerNewMap ***'+ SBUTriggerNewMap.size() );
        system.debug('****size of SBUTriggerOldMap ***'+ SBUTriggerOldMap.size() );
        PT_LeadTriggerHandler.run(SBUTriggerNewMap, SBUTriggerOldMap);
        CTS_LeadAssignedDateTimeCalculation.calculateLeadAssignedDateTime(newLead, null);
        
        
        // Changes by Capgemini Aman Kumar Date 16/2/2023. START Defect VPTECH-668
        businessRelationshipInactiveCheck(newLead,user);
        // Changes by Capgemini Aman Kumar Date 16/2/2023. END Defect VPTECH-668
    }
    /* Revisions:
	13-Sept-2024 : Updated logic for Case #03210798 by AMS Team
	*/
    public static void beforeUpdate(List<Lead> newLead, List<Lead> oldLead, Map<Id,Lead> oldLeadMap,User user){
        //system.debug('newLead'+ newLead );
        //system.debug('oldLead'+ oldLead );
        system.debug('Inside beforeUpdate >>>>');
        airNALeadNew=leadTriggerMethod(newLead);
        if (!airNALeadNew.isEmpty()){Lead_TriggerHandler.beforeUpdate(airNALeadNew, oldLeadMap);}
        //Commented by Capgemini Devender Singh Date 22/6/2023. 
        //because it must be run after Record type get change START -70
        //PT_LeadTriggerHandler.run(SBUTriggerNewMap, SBUTriggerOldMap); 
        //Changes by Capgemini Devender Singh Date 22/6/2023. END -70
        AssignmentEngineV2.checkOutOfOffice(newLead,oldLeadMap); // changes for enhancement case : #03210798 by AMS Team - Sept24 Release
        CTS_LeadAssignedDateTimeCalculation.calculateLeadAssignedDateTime(newLead, oldLeadMap);
        CTS_LeadAssignedDateTimeCalculation.updateElapsedTime(newLead, oldLeadMap);
        // Changes by Capgemini Aman Kumar Date 6/3/2023. START VPTECH-1272
        Lead_TriggerHandler.setResponseTime(newLead, oldLeadMap);
        // Changes by Capgemini Aman Kumar Date 6/3/2023. END VPTECH-1272
        
        // Updated by Artur Poiata - #03318761 - 8NOV2024 - Added if condition to bypass this piece of the code, for integration users.
        if(!System.label.IntegrationByPassUsers.contains(UserInfo.getName())){
            //Changes by Capgemini Aman Kumar Date 3/2/2023 for US SFAVPTECH-581. START  
            List<id> accountIds = new List<id>();
            
            for(Lead leadTemp : newLead){
                if(leadTemp.IsConverted && leadTemp.RecordTypeId == STANDARD_LEAD_RT_ID){
                    accountIds.add(leadTemp.ConvertedAccountId);        
                }
            }
            
            List<Account> accList = [Select id,(Select id,CTS_Channel__c,Type__c,Business__c from Business_Relationships__r) 
                                     from Account where id IN:accountIds];
            for(Lead leadTemp :newLead){
                if(leadTemp.IsConverted && !accList.isEmpty()){
                    for(Account acc:accList){
                        if(acc.id == leadTemp.ConvertedAccountId){
                            for(Business_Relationship__c br : acc.Business_Relationships__r){
                                if(br.Business__c !=null && leadTemp.Business__c != null && br.Business__c.equalsIgnoreCase(leadTemp.Business__c)){
                                    
                                    If((leadTemp.Indirect_Lead__c && !br.CTS_Channel__c.equalsIgnoreCase('Distributor') && !br.Type__c.equalsIgnoreCase('Distributor End User'))
                                       || (!leadTemp.Indirect_Lead__c && br.CTS_Channel__c.equalsIgnoreCase('Distributor')))
                                    {
                                        system.debug('Inside True!!!! ');
                                        leadTemp.addError(System.Label.VPTECH_Business_Relationship_Channel_check);   
                                    }
                                }
                            }
                        }                
                    }
                }
            }//Changes by Capgemini Aman Kumar Date 3/2/2023 for US SFAVPTECH-581. END 
        }
        
        // Changes by Capgemini Aman Kumar Date 16/2/2023. START Defect VPTECH-668
        businessRelationshipInactiveCheck(newLead,user);
        // Changes by Capgemini Aman Kumar Date 16/2/2023. END Defect VPTECH-668
        
        
        
        // Changes by Capgemini Aman Kumar Date 24/5/2023. START US SGGC-69, SGGC-134
        String HibonQueueId = '';
        List<String> IRQueueIds = new List<String>(); 
        List<String> GDQueueIds = new List<String>();
        // Changes by Capgemini Devender Singh Date 13/6/2023. START US SGGC-70
        List<String> PTQueueIds = new List<String>();
        // Changes by Capgemini Devender Singh Date 13/6/2023. END US SGGC-70 
        // Changes by Harish 04/12/2023. Start 
        List<String> VPTechQueueIds = new List<String>();
         List<String> LFSQueueIds = new List<String>();
        // Changes by Harish 04/12/2023. END 
        // we have added the one more condtion to the query for get the listOfPTLeadQueues records
        for(Group queue:[SELECT Id, Name, DeveloperName, Type FROM Group Where Type = 'Queue' AND (DeveloperName IN:listOfIRQueues OR DeveloperName IN:listOfGDQueues OR DeveloperName IN:listOfPTLeadQueues OR DeveloperName IN:listOfLFSLeadQueues OR DeveloperName IN:listOfVPTechLeadQueues OR DeveloperName =:Hibon_Queue) ] ){
            if(listOfIRQueues.contains(queue.DeveloperName)){
                IRQueueIds.add(queue.id);
            }
            if(listOfGDQueues.contains(queue.DeveloperName)){
                GDQueueIds.add(queue.id);
            }
            if(Hibon_Queue.equals(queue.DeveloperName)){
                HibonQueueId = queue.id;
                system.debug('HibonQueueId1>>>'+ HibonQueueId);
            }
            // Changes by Capgemini Devender Singh Date 13/6/2023. START US SGGC-70
            if(listOfPTLeadQueues.contains(queue.DeveloperName)){
                PTQueueIds.add(queue.id);
            }
            // Changes by Capgemini Devender Singh Date 13/6/2023. END US SGGC-70
             // Changes by Harish 04/12/2023. START 
            if(listOfVPTechLeadQueues.contains(queue.DeveloperName)){
                VPTechQueueIds.add(queue.id);
            }
            if(listOfLFSLeadQueues.contains(queue.DeveloperName)){
                LFSQueueIds.add(queue.id);
            }
           // Changes by Harish 04/12/2023. END 
        }   
        Map<String, L_GD_COMP_Country_Code_to_Name__mdt> mapCountryCodes = L_GD_COMP_Country_Code_to_Name__mdt.getAll();
        system.debug('HibonQueueId2>>>'+ HibonQueueId);
        for(Lead leadTemp :newLead){
            system.debug('Hibon_User>>>'+ Hibon_User);
            system.debug('leadTemp.OwnerId>>>'+ leadTemp.OwnerId);
            if((leadTemp.RecordTypeId == NEW_STANDARD_LEAD_RT_ID || leadTemp.RecordTypeId == STANDARD_LEAD_RT_ID) && IRQueueIds.contains(leadTemp.OwnerId) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = NEW_GLOBAL_LEAD_RT_ID;
                leadTemp.Country = leadTemp.Country !=null ? mapCountryCodes.get(leadTemp.Country).Country_Name__c : leadTemp.Country;
            }
            // Changes by Capgemini Devender Singh Date 13/6/2023. START US SGGC-70
            if(( leadTemp.RecordTypeId == STANDARD_LEAD_RT_ID) && PTQueueIds.contains(leadTemp.OwnerId) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = PT_Lead_RT_ID;
                leadTemp.Country = leadTemp.Country !=null ? mapCountryCodes.get(leadTemp.Country).Country_Name__c : leadTemp.Country;
                
            }
            if(( leadTemp.RecordTypeId == STANDARD_LEAD_RT_ID) && (Hibon_User==leadTemp.OwnerId || (String.isNotBlank(HibonQueueId) && HibonQueueId == leadTemp.OwnerId))&& oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = Hibon_Lead_RT_ID;
                leadTemp.Country = leadTemp.Country !=null ? mapCountryCodes.get(leadTemp.Country).Country_Name__c : leadTemp.Country;

            }
            // Changes by Capgemini Devender Singh Date 13/6/2023. END US SGGC-70
            
            // Changes by Harish 04/12/2023. START
            if(( leadTemp.RecordTypeId == NEW_GLOBAL_LEAD_RT_ID) && (VPTechQueueIds.contains(leadTemp.OwnerId) || LFSQueueIds.contains(leadTemp.OwnerId)) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = STANDARD_LEAD_RT_ID;
                    for(L_GD_COMP_Country_Code_to_Name__mdt mdt:mapCountryCodes.values()){
                    if(mdt.Country_Name__c == leadTemp.Country){
                        system.debug('mdt.Country_Name__c>>>'+ mdt.Country_Name__c);
                        leadTemp.Country = leadTemp.Country !=null ? mdt.DeveloperName : leadTemp.Country;
                    }
                }
                
            }
            
            if(( leadTemp.RecordTypeId == NEW_GLOBAL_LEAD_RT_ID) && PTQueueIds.contains(leadTemp.OwnerId) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = PT_Lead_RT_ID;
				//leadTemp.Country = leadTemp.Country !=null ? mapCountryCodes.get(leadTemp.Country).Country_Name__c : leadTemp.Country;
            }
             if(( leadTemp.RecordTypeId == PT_Lead_RT_ID) && (GDQueueIds.contains(leadTemp.OwnerId) || VPTechQueueIds.contains(leadTemp.OwnerId) || LFSQueueIds.contains(leadTemp.OwnerId)) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = STANDARD_LEAD_RT_ID;
                for(L_GD_COMP_Country_Code_to_Name__mdt mdt:mapCountryCodes.values()){
                    if(mdt.Country_Name__c == leadTemp.Country){
                        system.debug('mdt.Country_Name__c>>>'+ mdt.Country_Name__c);
                        leadTemp.Country = leadTemp.Country !=null ? mdt.DeveloperName : leadTemp.Country;
                    }
                }
            }
			

            if(( leadTemp.RecordTypeId == PT_Lead_RT_ID) && (IRQueueIds.contains(leadTemp.OwnerId)) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                leadTemp.RecordTypeId = NEW_GLOBAL_LEAD_RT_ID;
            }
            
            // Changes by Harish 04/12/2023. END
            
            if(leadTemp.RecordTypeId == NEW_GLOBAL_LEAD_RT_ID && GDQueueIds.contains(leadTemp.OwnerId) && oldLeadMap.get(leadTemp.id).OwnerId !=leadTemp.OwnerId){
                system.debug('leadTemp.OwnerId>>>'+ leadTemp.OwnerId );
                system.debug('GDQueueIds>>>'+ GDQueueIds);
                leadTemp.RecordTypeId = STANDARD_LEAD_RT_ID;
                for(L_GD_COMP_Country_Code_to_Name__mdt mdt:mapCountryCodes.values()){
                    if(mdt.Country_Name__c == leadTemp.Country){
                        system.debug('mdt.Country_Name__c>>>'+ mdt.Country_Name__c);
                        leadTemp.Country = leadTemp.Country !=null ? mdt.DeveloperName : leadTemp.Country;
                    }
                }
            }
            
        }
        //Changes by Capgemini Devender Singh Date 22/6/2023. START --70
        airNALeadNew=leadTriggerMethod(newLead);
        system.debug('PT_LeadTriggerHandler is run in before');
        PT_LeadTriggerHandler.run(SBUTriggerNewMap, SBUTriggerOldMap);
        //Changes by Capgemini Devender Singh Date 22/6/2023. END --70
        
        // Changes by Capgemini Aman Kumar Date 24/5/2023. END US SGGC-69, SGGC-134 
        
        
    }
    
    public static void afterInsert(List<Lead> newLead, List<Lead> oldLead){
        airNALeadNew=leadTriggerMethod(newLead);
        PT_LeadTriggerHandler.run(SBUTriggerNewMap, SBUTriggerOldMap);
    }
    
    public static void afterUpdate(List<Lead> newLead, Map<Id,Lead> oldLeadMap, List<Lead> oldLead){
        airNALeadNew=leadTriggerMethod(newLead); 
        if (!airNALeadNew.isEmpty()){Lead_TriggerHandler.afterUpdate(airNALeadNew, oldLeadMap);}  //record approved for deletion is delete.
        
        // Changes by Capgemini Aman Kumar Date 10/1/2023 for US SFAVPTECH-168. START        
        List<id> contactIds = new List<id>();
        List<id> accountIds = new List<id>();
        List<id> opportunityIds = new List<id>();
        List<Lead> convertedLead = new List<Lead>();
        List<Contact> contacts = new List<Contact>();
        List<Account> accounts = new List<Account>();
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();
        Map<id,Contact> contactMap = new Map<id,Contact>();
        List<Account> accountsToUpdate = new List<Account>();
        
        for(Lead leadTemp : newLead){
            if(leadTemp.IsConverted && leadTemp.RecordTypeId == STANDARD_LEAD_RT_ID){
                contactIds.add(leadTemp.ConvertedContactId);
                accountIds.add(leadTemp.ConvertedAccountId);
                opportunityIds.add(leadTemp.ConvertedOpportunityId);
                convertedLead.add(leadTemp);
            }
        }
        if(!contactIds.isEmpty()){
            contacts = [Select id,Business__c From Contact Where id in : contactIds];
            
        }
        if(!accountIds.isEmpty()){
            accounts = [Select id,(Select id,FWA__c,Business__c from Business_Relationships__r) From Account Where id in : accountIds];
            
        }
        if(!opportunityIds.isEmpty()){
            opportunities = [Select id,Framework_Flag__c,Business__c From Opportunity Where id in : opportunityIds];
            
        }
        for(Lead leadTemp : convertedLead){
            for(Contact con : contacts){
                if(con.id == leadTemp.ConvertedContactId ){
                    if(con.Business__c == null){
                        con.Business__c = leadTemp.Business__c;
                        contactsToUpdate.add(con);  
                    } else{
                        if(!con.Business__c.containsIgnoreCase(leadTemp.Business__c)){
                            system.debug('Inside If Condition!! ');
                            List<String> contactBusiness = con.Business__c.split(';');
                            contactBusiness.add(leadTemp.Business__c);
                            con.Business__c = String.join(contactBusiness, ';');
                            contactsToUpdate.add(con);
                            system.debug('contactsToUpdate ' + contactsToUpdate); 
                        }
                    }
                } 
                
            }
            
            // Changes by Capgemini Aman Kumar Date 31/1/2023 for US SFAVPTECH-480. START 
            system.debug('accounts ' + accounts); 
            for(Account acc : accounts){
                system.debug('leadTemp.ConvertedAccountId ' + leadTemp.ConvertedAccountId);
                system.debug('acc.Business_Relationships__r ' + acc.Business_Relationships__r);
                
                
                if(acc.id == leadTemp.ConvertedAccountId){
                    acc.LeadConversion__c = ''; 
                    accountsToUpdate.add(acc);
                }
                
                if(leadTemp.Business__c != null && leadTemp.Business__c != 'Compressor' && acc.id == leadTemp.ConvertedAccountId && acc.Business_Relationships__r != null){
                    for(Business_Relationship__c br : acc.Business_Relationships__r){
                        system.debug('br.Business__c ' + br.Business__c);
                        system.debug('leadTemp.Business__c ' + leadTemp.Business__c);
                        system.debug('br.FWA__c ' + br.FWA__c);
                        if(br.Business__c !=null && leadTemp.Business__c != null && br.Business__c.equalsIgnoreCase(leadTemp.Business__c) &&  br.FWA__c){
                            for(opportunity opp : opportunities){
                                system.debug('opp:  ' + opp);
                                if(opp.id == leadTemp.ConvertedOpportunityId){
                                    opp.Framework_Flag__c = true;
                                    opportunitiesToUpdate.add(opp);
                                }
                            }
                        }
                    }
                }
            }
            
        }
        system.debug('opportunitiesToUpdate:  ' + opportunitiesToUpdate);
        if(!opportunitiesToUpdate.isEmpty()){
            system.debug('Inside Update opportunity!! ');
            Update opportunitiesToUpdate;
        }
        // Changes by Capgemini Aman Kumar Date 31/1/2023 for US SFAVPTECH-480. END 
        
        
        if(!contactsToUpdate.isEmpty()){
            system.debug('Inside Update Contact!! ');
            Update contactsToUpdate;
        }
        
        if(!accountsToUpdate.isEmpty()){
            system.debug('Inside Update Account!! ');
            Update accountsToUpdate;
        }
        
        // Changes by Capgemini Aman Kumar Date 10/1/2023 for US SFAVPTECH-168. END
        
    }
    // Changes by Capgemini Aman Kumar Date 10/1/2023. START    
    public static void validateBusiness(List<Lead> leads,Boolean isInsert, Map<id,Lead> oldLeads,User user) {
        
    
        
        // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
        Set< Id > setOfContactId = New Set< Id >();
        Set< Id > setOfOpportunityId = New Set< Id >();
        Set< Id > setOfaccountsId  = new Set< Id >();
        Map< Id, Contact > mapOfContact = New Map< Id, Contact >(); 
        Map< Id, Opportunity > mapOfOpportunity = New Map< Id, Opportunity >(); 
        Map< Id, Account > mapOfAccount = New Map< Id, Account >(); 
        // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
        
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        //User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
        for (Lead lead : leads) {
            // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
            if( lead.IsConverted && !oldLeads.get(lead.Id).IsConverted ){
                setOfContactId.add(lead.ConvertedContactId);
                setOfOpportunityId.add(lead.ConvertedOpportunityId);
                setOfaccountsId.add(lead.ConvertedAccountId);
            }
            // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
            if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){
                if (isInsert){ 
                    if(lead.RecordTypeid == NEW_STANDARD_LEAD_RT_ID && user.Business__c !=null && !user.Business__c.containsIgnoreCase(lead.Business__c)) {
                        System.debug('Different Business!!! ');
                        lead.addError(System.Label.VPTECH_Lead);
                    }
                }
				 else{
                    System.debug('Indide Update 1!!! ');
                     if(lead.Business_Field_Override_Flag__c != true )
                        {
                     	if(oldLeads.get(lead.Id).Business__c != null)
                        {
                             if (lead.RecordTypeid == STANDARD_LEAD_RT_ID && oldLeads.get(lead.Id) != null &&  oldLeads.get(lead.Id).Id == lead.Id && user.Business__c !=null && 
                            !user.Business__c.containsIgnoreCase(lead.Business__c)) {
                                System.debug('Indide Update 2!!! ' + user.Business__c +' ' +  lead.Business__c + ' '+ user.Business__c.containsIgnoreCase(lead.Business__c));
                                lead.addError(System.Label.VPTECH_Lead);
                            }
                        }
                         if(oldLeads.get(lead.Id).Business__c == null  && lead.Business__c != null){
                             if (user.Business__c !=null && !user.Business__c.containsIgnoreCase(lead.Business__c))
                             {
                                 lead.addError(System.Label.VPTECH_Lead);
                             }
                     }
                        }
                    
                }
            }
        }
        // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
        if (!isInsert){
            if(!setOfContactId.isEmpty()){
                for(Contact objCon :[SELECT Id,RecordType.Name,RecordTypeid  FROM Contact WHERE ID in:setOfContactId]){
                    mapOfContact.put( objCon.Id ,objCon); 
                }
            }
            if(!setOfOpportunityId.isEmpty()){
                for(Opportunity objOpp :[SELECT Id,RecordType.Name,RecordTypeid  FROM Opportunity WHERE ID in:setOfOpportunityId]){
                    mapOfOpportunity.put( objOpp.Id ,objOpp); 
                }
            }
            if(!setOfaccountsId.isEmpty()){
                for(Account objAcc :[SELECT Id,RecordType.Name,RecordTypeid  FROM Account WHERE ID in:setOfaccountsId]){
                    mapOfAccount.put( objAcc.Id ,objAcc); 
                }
            }
            
            for (Lead lead : leads) {
                System.debug('357 Lead ----> '+lead);
                if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){
                   
                    if(mapOfContact.size()>0){
                           if(lead.RecordTypeid !=STANDARD_LEAD_RT_ID  && mapOfContact.get(lead.ConvertedContactId).RecordTypeid == End_Customer_RT_ID && lead.Business__c ==null  ) {
                               lead.addError(System.Label.Lead_Conversion_to_New_Contact_Message_Label);
                               //break;
                           }else if(lead.RecordTypeid ==STANDARD_LEAD_RT_ID  && mapOfContact.get(lead.ConvertedContactId).RecordTypeid != End_Customer_RT_ID  && lead.Business__c == 'Compressor'  ) {
                               lead.addError(System.Label.Lead_Conversion_to_New_Contact_Message_Label);
                              // break;
                           } 
                           //Milton Roy Change
                           else if(lead.RecordTypeid !=STANDARD_LEAD_RT_ID  && mapOfContact.get(lead.ConvertedContactId).RecordTypeid == Milton_Roy_RT_ID && lead.Business__c ==null  ) {
                            lead.addError(System.Label.Lead_Conversion_to_New_Contact_Message_Label);
                            }
                             else if(lead.RecordTypeid ==STANDARD_LEAD_RT_ID  && mapOfContact.get(lead.ConvertedContactId).RecordTypeid != Milton_Roy_RT_ID  && (lead.Business__c == 'Milton Roy' || lead.Business__c == 'ARO' )) {
                            lead.addError(System.Label.Lead_Conversion_to_New_Contact_Message_Label);
                            }    
                        
                    }
                    
                  /*  if(mapOfOpportunity.size()>0){ 
                        if(getRecordTypeNameById('Lead',lead.RecordTypeid) !='Standard_Lead' && ( getRecordTypeNameById('Opportunity',mapOfOpportunity.get(lead.ConvertedOpportunityId).RecordTypeid).contains('Standard_Opportunity') || getRecordTypeNameById('Opportunity',mapOfOpportunity.get(lead.ConvertedOpportunityId).RecordTypeid).contains('New_Standard_Opportunity')) && lead.Business__c ==null   ) {
                            lead.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label);
                           // break;
                        }else if(getRecordTypeNameById('Lead',lead.RecordTypeid) =='Standard_Lead' && ( !getRecordTypeNameById('Opportunity',mapOfOpportunity.get(lead.ConvertedOpportunityId).RecordTypeid).contains('Standard_Opportunity') && !getRecordTypeNameById('Opportunity',mapOfOpportunity.get(lead.ConvertedOpportunityId).RecordTypeid).contains('New_Standard_Opportunity')) && lead.Business__c == 'Compressor'    ) {
                            lead.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label);
                            //break;
                        }
                    }*/
                }
            }
            
        }
        // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
    }
    // Changes by Capgemini Aman Kumar Date 10/1/2023. END 
    
    // Changes by Capgemini Aman Kumar Date 16/2/2023. START Defect VPTECH-668
    
    public static void businessRelationshipInactiveCheck(List<Lead> newLead,User user){
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        // User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
        if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){            
            
            List<Id> accountIds = new List<Id>();
            List<Account> accounts = new List<Account>();
            for(Lead ld : newLead){
                if(ld.Distribution_Account__c != null && (ld.RecordTypeId == STANDARD_LEAD_RT_ID || ld.RecordTypeId == NEW_STANDARD_LEAD_RT_ID)){
                    accountIds.add(ld.Distribution_Account__c);
                }
            }
            if(!accountIds.isEmpty()){
                accounts = [Select id,(Select id,status__c,Business__c from Business_Relationships__r) From Account Where id in : accountIds Limit 49999] ;
            }
            for(Lead ld : newLead){
                if(ld.RecordTypeId == STANDARD_LEAD_RT_ID || ld.RecordTypeId == NEW_STANDARD_LEAD_RT_ID){
                    for(Account acc : accounts){
                        if(acc.id == ld.Distribution_Account__c){
                            for(Business_Relationship__c br : acc.Business_Relationships__r){
                                if(br.Business__c.equalsIgnoreCase(ld.Business__c) && br.Status__c == 'Inactive'){
                                    ld.addError(System.Label.VPTECH_Opportunity_inactive_Account_Error);  
                                }
                            } 
                        }
                    }
                    
                }
            }
        }
    }
    
    // Changes by Capgemini Aman Kumar Date 16/2/2023. END  Defect VPTECH-668
    // 
    //Changes by Capgemini Devender Singh Date 12/6/2023. START -80
    public static String getRecordTypeNameById(String objectName, Id strRecordTypeId) {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(strRecordTypeId).getDeveloperName();
    } 
    //Changes by Capgemini Devender Singh Date 12/6/2023. END -80
    
}