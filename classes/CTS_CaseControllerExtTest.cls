@isTest
public class CTS_CaseControllerExtTest {
    static testMethod void testCaseCreation(){
        PageReference CTSCarePage = Page.CTS_Case_Creation;
        test.setCurrentPage(CTSCarePage);
        Case caseObj = new Case();
        Apexpages.StandardController sc = new Apexpages.standardController(caseObj);
        Test.StartTest(); 
        CTS_CaseControllerExt ext = new CTS_CaseControllerExt(sc);
        ext.fileName = 'TestFile';
        ext.afile = Blob.valueOf('Unit Test ContentVersion Body'); 
        ext.contentType = 'pdf';
        ext.createCase();
        //system.assert(caseObj.Id!=null);
        //system.assertEquals(true, ext.displayPopup);
        PageReference pageRef = ext.closePopup();
        //system.assertEquals(false, ext.displayPopup);
        //system.assertEquals(CTSCarePage.getUrl(),pageRef.getUrl());        
        Test.StopTest();
    }
    
}