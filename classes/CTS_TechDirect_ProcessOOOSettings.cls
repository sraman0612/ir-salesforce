/*
* Revisions:
22-May-2024 : Update code against Case #03122107 - AMS Team
14-Jan-2025 : Updated code against case #03394776 - AMS Team
*/
public with sharing class CTS_TechDirect_ProcessOOOSettings {
    
    private static Map<Id, Id> originalAssigneeMap = new Map<Id, Id>(); 
    
    private static integer runs = 6; // number of recursive calls to be allowed -
    // the number of times to continue checking OOO settings after reassignment
    
    @InvocableMethod(
        label = 'Process Out of Office Settings' 
        description = 'Process out of office settings for the proposed assigned case owner and will reassign the owner if applicable.')    
    public static Case[] processCases(List<Case> cases) {
        
        system.debug('CTS_TechDirect_ProcessOOOSettings cases: ' + cases);
        
        //setup user to oooSetting map
        Map<Id,Out_of_Office_Settings__c> userToSettingsMap = new Map<Id,Out_of_Office_Settings__c>();
        //setup user to caseRoutings map       
        //Map from user id to Map from record type to backup owner
        Map<Id,Map<String,Id>> userToRoutingsMap = new Map<Id,Map<String,Id>>();
        
        //create refined list of cases to be reassigned
        List<Case> casesToReassign = new List<Case>();
        
        //create list for reassigned cases to be updated
        List<Case> reassignedCases = new List<Case>();
        
        //Out of office settings for all case owners
        Out_of_Office_Case_Routings__c[] caseRoutings; 
        
        Out_of_Office_Settings__c tempSetting;
        
        for (Case c : cases){
            // 14-Jan-2025 : case #03394776 - Added If condition to check if delegated user is actual user and not Queue.
            if(string.valueOf(c.OwnerId).startsWith('005')){
                if(!userToSettingsMap.containsKey(c.OwnerId) ){
                    if(Out_of_Office_Settings__c.getInstance(c.OwnerId).Id != null){
                        
                        tempSetting = Out_of_Office_Settings__c.getInstance(c.OwnerId);
                        
                        //check if case routing is enabled for owner and current date/time is within start/end date of OOO
                        if(tempSetting.Enabled__c == true && tempSetting.Start_Date__c < DateTime.now() && tempSetting.End_Date__c > DateTime.now()){
                            userToSettingsMap.put(c.OwnerId, tempSetting);
                            casesToReassign.add(c);
                        }
                    }
                }
                else{
                    casesToReassign.add(c);
                }
            }
        }
        
        //get Case Routings for Case Owners
        caseRoutings = [Select Id, Name, User_ID__c, Record_Type_ID__c, Backup_User_Queue_ID__c From Out_of_Office_Case_Routings__c Where User_ID__c IN :userToSettingsMap.keySet()];
        
        //temporary map used within loops
        Map<String,Id> tempMap;
        
        //populate usertoRoutingsMap in order to reassign each case based on user preferences
        for (Out_of_Office_Case_Routings__c oooCR : caseRoutings){
            
            if(!userToRoutingsMap.containsKey(oooCR.User_ID__c)){
                
                tempMap = new Map<String,Id>();
                
                if(oooCR.Record_Type_ID__c == null || oooCR.Record_Type_ID__c == ''){
                    tempMap.put('primary', oooCR.Backup_User_Queue_ID__c);
                }
                else{
                    tempMap.put(oooCR.Record_Type_ID__c, oooCR.Backup_User_Queue_ID__c);
                }
                userToRoutingsMap.put(oooCR.User_ID__c, tempMap);
            } 
            else{
                
                tempMap = userToRoutingsMap.get(oooCR.User_ID__c);
                
                if(oooCR.Record_Type_ID__c == null || oooCR.Record_Type_ID__c == '')
                    tempMap.put('primary', oooCR.Backup_User_Queue_ID__c);
                else
                    tempMap.put(oooCR.Record_Type_ID__c, oooCR.Backup_User_Queue_ID__c);
                
                userToRoutingsMap.put(oooCR.User_ID__c, tempMap);
            }
        }
        
        system.debug('casesToReassign: ' + casesToReassign);
        
        //reassign cases based on routing
        for(Case c : casesToReassign){
            
            tempMap = userToRoutingsMap.get(c.OwnerId);
            
            if(tempMap.containsKey(c.RecordTypeId)){
                
                Case newCase = new Case(Id = c.Id, OwnerId = tempMap.get(c.RecordTypeId));
                
                if (!originalAssigneeMap.containsKey(c.Id)){
                    originalAssigneeMap.put(c.Id, c.OwnerId);
                    //Commented as part of EMEIA Cleanup
                    //newCase.RS_CRS_Previous_Owner__c = c.OwnerId;
                }            	
                
                reassignedCases.add(newCase);                
            } 
            else if(tempMap.containsKey('primary')){
                
                Case newCase = new Case(Id = c.Id, OwnerId = tempMap.get('primary'));
                
                if (!originalAssigneeMap.containsKey(c.Id)){
                    originalAssigneeMap.put(c.Id, c.OwnerId);
                    //Commented as part of EMEIA Cleanup
                    //newCase.RS_CRS_Previous_Owner__c = c.OwnerId;
                }
                
                reassignedCases.add(newCase);     
            }
            //else do nothing 
        }
        
        system.debug('reassignedCases: ' + reassignedCases);
        
        //update Cases to be reassigned
        try{
            
            if(!reassignedCases.isEmpty()){
                update reassignedCases;
                //Updated by AMS Team (Mahesh) - #03122107 - 22MAY2024  
                sendEmailtoOwner(reassignedCases);
            }
        }
        catch(DmlException e){
            System.debug('Case Reassignment Error: ' + e.getMessage());
        }
        
        //TODO - make recursive call - need to test
        if(!reassignedCases.isEmpty()){
            //make recursive call
            if(runs > 0){
                runs = runs - 1;
                system.debug('RUNS***: ' + runs);
                processCases(reassignedCases);
            }
        }
        
        return cases;
    }
/*
@author       	: AMS Team (Mahesh)
@description    : method is used to send an email to owner : #03122107
@param          : list of cases
@date         	: 22 May 2024
Revision: 14-Jan-2025 case #03394776 - Optimesed the code.

*/  
    Public static void sendEmailtoOwner(list<case> lstCases){
        List<OrgWideEmailAddress> lstOwdAddress = [select Id from OrgWideEmailAddress where Address = 'sfdc_no_reply@irco.com'];
        
        List<EmailTemplate> lstEmailTemplate = [Select Id,Subject,Description,HtmlValue,DeveloperName,Body from EmailTemplate where DeveloperName = 'CTS_TechDirect_Case_Assignment_from_OOO'];
        List<Messaging.SingleEmailMessage> messgeList = new List<Messaging.SingleEmailMessage>();
        if(!lstOwdAddress.isEmpty() && !lstEmailTemplate.isEmpty()){
            for(case cs:lstCases){
                if(string.valueOf(cs.OwnerId).startsWith('005')){
                    Messaging.SingleEmailMessage message = Messaging.renderStoredEmailTemplate(lstEmailTemplate[0].Id, cs.ownerId, cs.id);
                    message.setOrgWideEmailAddressId(lstOwdAddress[0].Id);
                    message.setSaveAsActivity(false);
                    messgeList.add(message);
                }
            } 
            //Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {messgeList};
			Messaging.SendEmailResult[] results = Messaging.sendEmail(messgeList);
        }
    }
}