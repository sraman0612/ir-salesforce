/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SSO_AD_MatchOnCase_test {

    static testMethod void myUnitTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        
        User testUser = new User();
        testUser.firstName='testUser';
        testUser.lastName='testUser';
        testUser.email='testSSOJOT@test.ingersollrand.com';
        testUser.alias='jitTest';
        testUser.FederationIdentifier='ccabcd';
        testUser.username='testJitSSO@test.ingersollrand.com';
        testUser.profileId=p.Id;
        testUser.EmailEncodingKey='UTF-8';
        testUser.LocaleSidKey='en_US';
        testUser.LanguageLocaleKey='en_US';
        testUser.TimeZoneSidKey='America/Los_Angeles';
        insert(testUser);
        System.debug('created user '+testUser);
		System.assert(testUser.Id!=null);
		
		
        Map<String, String> m = new Map<String, String>(); 
		m.put('url', 'First entry');      //required not used           
		m.put('fed id', 'ccabcd');        //required not used   
        
        SSO_AD_MatchOnCase ssoJit = new SSO_AD_MatchOnCase();
        
        
        ssoJit.createUser('0LE11000000Caks', null, null, 'ccABcd', m, 'fake assertion');
        
        User testUser2 = [SELECT FederationIdentifier FROM user WHERE id=:testUser.Id]; 
        System.debug('User Fed ID:'+testUser2.FederationIdentifier);
        System.assert(testUser2.FederationIdentifier=='ccABcd');
        
        
        //does nothing
        ssoJit.updateUser(testUser.Id, '0LE11000000Caks', null, null, 'ccABcd', m, 'fake assertion');
        
    }
}