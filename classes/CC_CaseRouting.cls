public class CC_CaseRouting {
  
  public static void assignCommunityCreatedCases(Case[] cases){
    Map<String, CC_Community_Case_Create_Map__mdt> caseTypeMap = new Map<String, CC_Community_Case_Create_Map__mdt>{};
    Set<Id> recTypeIds = new Set<Id>();

    for (RecordType rt: [SELECT Id FROM RecordType WHERE DeveloperName LIKE 'Club_Car%' AND SobjectType ='Case']){
      recTypeIds.add(rt.Id);
    }
    
    for(CC_Community_Case_Create_Map__mdt cccm : [ select MasterLabel, OwnerId__c, Reason__c, Sub_Reason__c, Root_Cause__c from CC_Community_Case_Create_Map__mdt ]) {
      caseTypeMap.put(cccm.MasterLabel, cccm);
    }
    
    for (Case c :cases) {
      if (c.CC_PavilionReason_for_Case__c != null && c.Origin == 'Community' && c.RecordTypeId != null && recTypeIds.contains(c.RecordTypeId) ) {
        CC_Community_Case_Create_Map__mdt cccmp = caseTypeMap.get(c.CC_PavilionReason_for_Case__c);
        if(null != cccmp){
          c.OwnerId = cccmp.OwnerId__c;
          c.Reason = cccmp.Reason__c;
          c.Sub_Reason__c = cccmp.Sub_Reason__c;
          c.CC_Root_Cause__c = cccmp.Root_Cause__c;
        }
        
      }
    }
    
  }

  public static void AssignByTeamRole(Case[] cases) {
    Map<Id, string> queue2role = new Map<Id, string>{};
    Set<Id> recTypeIds = new Set<Id>();
    Set<Id> acctIds = new Set<Id>();
    List<Case> casesToAssign = new List<Case>{};
    
    for (RecordType rt: [SELECT Id FROM RecordType WHERE DeveloperName LIKE 'Club_Car%' AND SobjectType ='Case']){
      recTypeIds.add(rt.Id);
    }
    
    for(CC_Queue_Name_To_Role_Name__mdt q2r : [ select role_name__c, queue_Id__c from CC_Queue_Name_To_Role_Name__mdt ]) {
      queue2role.put(q2r.queue_Id__c, q2r.role_name__c);
    }
    
    for (Case c :cases) {
      if ((c.AccountId != null) && (c.OwnerId != null) && 
          (String.valueOf(c.OwnerId).startsWith('00G')) && queue2role.keyset().contains(c.OwnerId) &&
          (c.RecordTypeId != null) && recTypeIds.contains(c.RecordTypeId) ) {
        acctIds.add(c.AccountId);
        casesToAssign.add(c);
      }
    }
    
    if (casesToAssign.size() > 0) {
      // Get Account Team Members
      Map <String, Id> teamMap = new Map<String, Id>();
      for (CC_My_Team__c mt :[SELECT Id, Account__c, My_Team_Role__c, User__c FROM CC_My_Team__c WHERE Account__c IN :acctIds and My_Team_Role__c != null and User__c != null ]){
        teamMap.put(mt.Account__c + mt.My_Team_Role__c, mt.User__c);    
      }
      // Assign Case Owners
      for (Case c :casesToAssign) {
        Id ownerId = teamMap.get(c.AccountId + queue2role.get(c.OwnerId)); 
        if (ownerId != null) {
          c.OwnerId = ownerId;
        }
      }
    }    
  }
}