/**
    @author Providity
    @date 15FEB19
    @description Test class for PicklistSelectController
*/

@isTest
private class PicklistSelectControllerTest {
    @isTest
    static void test_get_field_label() {        
        Test.startTest();        
        String fieldLabel = PicklistSelectController.getFieldLabel( 'Account', 'Type' );        
        Test.stopTest();        
        System.assertEquals( Account.Type.getDescribe().getLabel(), fieldLabel );        
    }
    
    @isTest
    static void test_get_picklist_values() {
        String CTS_TD_Case_Record_Types = 'IR Comp Tech Direct Article Feedback,IR Comp Tech Direct Site Feedback,IR Comp TechDirect Ask a Question,IR Comp TechDirect Issue Escalation,IR Comp TechDirect Start Up';
        Test.startTest();        
        List<Object> optionsObj = PicklistSelectController.getPicklistOptions( 'Knowledge__kav', 'CTS_TechDirect_Article_Type__c', null, 'Tech Note');
        List<PicklistSelectController.PicklistOption> options = (List<PicklistSelectController.PicklistOption>)optionsObj;
        
        List<Object> optionsObjCase = PicklistSelectController.getPicklistOptions( 'Case', 'Status', CTS_TD_Case_Record_Types, null);        
        Test.stopTest();
        System.assert(options.size() == 1);
        System.assert(optionsObjCase.size() > 0);
    }
}