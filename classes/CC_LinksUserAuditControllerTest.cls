@isTest
private class CC_LinksUserAuditControllerTest {

	static testmethod void test1(){
		List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
		psettingList = TestUtilityClass.createPavilionSettings();
		insert psettingList;
    Account newAccount = TestUtilityClass.createAccount();
    insert newAccount;
    Contact sampleContact = TestUtilityClass.createContact(newAccount.id);
    sampleContact.CC_Pavilion_Admin__c = TRUE;
    insert sampleContact;
    Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile('test pavilion navigation profile');
    insert newPavilionNavigationProfile;
    User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', sampleContact.id);
    insert newUser;
		User u1 = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1];
		system.runAs(u1){
      CC_CreateComplianceRecords.createComplianceRecords();
      CC_Partner_User_Compliance__c [] cLst = CC_LinksUserAuditController.getUserComplianceAuditRecords(newAccount.Id);
      system.assertEquals(1,cLst.size());
      String retStr = CC_LinksUserAuditController.setStatus(u1.Id,cLst[0].Id,'Deactivated');
      system.assertEquals('OK',retStr);
      CC_PavilionTemplateController controller = new CC_PavilionTemplateController() ;
      controller.acctId = newUser.AccountId;
      CC_LinksUserAuditController customcontroller = new CC_LinksUserAuditController(controller);
    }
	}
}