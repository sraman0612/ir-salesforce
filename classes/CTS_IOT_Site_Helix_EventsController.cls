public with sharing class CTS_IOT_Site_Helix_EventsController {

    @TestVisible
    private class EventsResponse extends LightningResponseBase{
        @AuraEnabled public HelixEvent[] events = new HelixEvent[]{};
        @AuraEnabled public Integer totalRecordCount = 0;
    }

    @TestVisible
    private class HelixEvent{
        @AuraEnabled public String assetRecordURL = '';
        @AuraEnabled public String assetNickname = '';
        @AuraEnabled public String eventURL = '';
        @AuraEnabled public String eventName = '';
        @AuraEnabled public DateTime eventTimestamp;
        @AuraEnabled public String eventCode = '';
        @AuraEnabled public String eventMessage = '';
    }

    @AuraEnabled
    public static EventsResponse getEvents(String siteId, String searchTerm, Integer pageSize, Integer offSet){

        EventsResponse response = new EventsResponse();

        try{

            String objectName = 'CTS_RMS_Event__c';
            String whereClause = 'Where Asset__r.AccountId = \'' + siteId + '\'';

            CTS_RMS_Event__c[] rmsEvents = CTS_IOT_Data_Service.getRecords(
                objectName, 
                'Id,Asset__r.Asset_Name_Displayed__c,Name,Event_Timestamp__c,Event_Code__c,Event_Message__c', 
                whereClause, 
                'Order By Event_Timestamp__c DESC', 
                searchTerm, 
                pageSize, 
                offSet);

            for (CTS_RMS_Event__c rmsEvent : rmsEvents){

                HelixEvent helixEvent = new HelixEvent();
                helixEvent.assetRecordURL = '/asset/' + rmsEvent.Asset__c;
                helixEvent.assetNickname = rmsEvent.Asset__r.Asset_Name_Displayed__c;
                helixEvent.eventURL = '/detail/' + rmsEvent.Id;
                helixEvent.eventName = rmsEvent.Name;
                helixEvent.eventTimestamp = rmsEvent.Event_Timestamp__c;
                helixEvent.eventCode = rmsEvent.Event_Code__c;
                helixEvent.eventMessage = rmsEvent.Event_Message__c;
                response.events.add(helixEvent);
            }

            response.totalRecordCount = CTS_IOT_Data_Service.getRecordCount(
                objectName, 
                whereClause, 
                searchTerm);              
        }
        catch(Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}