global with sharing class CC_PavilionOrderLiterature {
    Public String zipLogoURL{get;set;}

    /* constructor */  
    public CC_PavilionOrderLiterature(CC_PavilionTemplateController controller) {
        try{
            zipLogoURL='servlet/servlet.FileDownload?file='+[SELECT Id FROM Document WHERE Name = 'ZipFileLogo'].id;
        }
        catch(Exception e){
            system.debug(e);
        }

    }
    
    /* remote action to display ContentVersion records*/
    @RemoteAction
    public static set<ContentVersion> getMarketingImages() {  
        Id  usrId  = userinfo.getUserId();
        List<Contract> cont = new List<Contract>();
        List<ContentVersion> contentResource = new List<ContentVersion>();
        List<ContentVersion> FinalAllcontentResource = new List<ContentVersion>();
        Set<ContentVersion> finalcontentResource = new Set<ContentVersion>();
        List<string> agrContent= new List<string>();
        map<string,string> accountmap  = new map<string,string>();

        contentResource = [select id,FileExtension,FileType,Description,TextPreview,CC_Publication_Number__c,
                           RecordType.Name from ContentVersion
                           where RecordType.Name='Order Literature'];
                           
         for(ContentVersion c: contentResource){
                       finalcontentResource.add(c); 
                   }
          
        // FinalAllcontentResource.addAll(finalcontentResource);
        return finalcontentResource;
        
    }
   
}