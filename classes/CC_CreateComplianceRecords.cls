//Club Car
//Cal Steele 11-15-17
//Ben Lorenz Consulting LLC

global class CC_CreateComplianceRecords {
    
    @InvocableMethod
    global static void createComplianceRecords() {
      //Generate list of Users to create compliance records for
    	List <User> userList = [SELECT Id, Name, Contact.Id, Contact.CC_Open_Partner_Compliance_Issues__c, Contact.AccountId, Contact.Email, Contact.CC_Pavilion_Admin__c,
    	                        CC_Part_Cart__c, CC_View_Parts_Pricing__c, CC_View_Sales_Bulletins__c, CC_View_Sales_Pricing__c, CC_View_Service_Bulletins__c, Warranty_Tavant__c,
    	                        CC_View_Parts_Order_Invoice_Pricing__c, CC_View_Parts_Bulletins__c, CC_CustomerVIEW__c, CC_Sales__c, Contact.FirstName, Contact.LastName,
    	                        Contact.Title, Contact.Phone, Contact.OtherPhone, Contact.Department, UserRole.Name, Profile.Name, Contact.CC_User_Validation__c
    	                        FROM User WHERE IsActive = TRUE and Profile.Name like 'CC_%' and Contact.AccountId != NULL];
    	
    	List<CC_Partner_User_Compliance__c> PartnerCompList = new List<CC_Partner_User_Compliance__c>();
    	List<Contact> contactList = new List<Contact>();
    	Map<Id, Id> acctUserMap = new Map<Id, Id>();
    	
    	//Map of users accounts for compliance record owners
    	for (User user1 : userList) {
    	    if (user1.Contact.CC_User_Validation__c) {acctUserMap.put(user1.Contact.AccountId, user1.Id);}
    	}
        
      //Generate and insert Compliance records
      for (User user2 : userList) {
          CC_Partner_User_Compliance__c compRecord = New CC_Partner_User_Compliance__c(
          User__c = user2.Id,
          Account__c = user2.Contact.AccountId,
          CC_Part_Cart__c = user2.CC_Part_Cart__c,
          CC_Parts_Pricing_Bulletins__c = user2.CC_View_Parts_Pricing__c,
          CC_Sales_Bulletins__c = user2.CC_View_Sales_Bulletins__c,
          CC_Sales_Pricing_Bulletins__c = user2.CC_View_Sales_Pricing__c,
          CC_Service_Bulletins__c = user2.CC_View_Service_Bulletins__c,
          CC_Warranty_Tavant__c = user2.Warranty_Tavant__c,
          CC_Parts_Order_Invoice_Pricing__c = user2.CC_View_Parts_Order_Invoice_Pricing__c,
          CC_Parts_Bulletins__c = user2.CC_View_Parts_Bulletins__c,
          CC_SFA_CustomerVIEW__c = user2.CC_CustomerVIEW__c,
          CC_First_Name__c = user2.Contact.FirstName,
          CC_Last_Name__c = user2.Contact.LastName,
          CC_Email__c = user2.Contact.Email,
          CC_Job_Title__c = user2.Contact.Title,
          CC_Business_Number__c = user2.Contact.Phone,
          CC_Mobile_Number__c = user2.Contact.OtherPhone,
          CC_Department__c = user2.Contact.Department,
          CC_Role__c = user2.UserRole.Name,
          CC_Profile__c = user2.Profile.Name);
          if (NULL != acctUserMap.get(user2.Contact.AccountId)) {compRecord.OwnerId = acctUserMap.get(user2.Contact.AccountId);}
          PartnerCompList.add(compRecord);
      }
      insert PartnerCompList;  
        
      //Update Pavilion Admin Contacts
      for (User user3 : userList) {
          if (user3.Contact.CC_User_Validation__c) {
              Contact contact = new Contact();
              contact.CC_Open_Partner_Compliance_Issues__c = TRUE;
              contact.Id = user3.Contact.Id;
              contactList.add(contact);
          }
      }
      //switched from standard DML to batch due to apex cpu time errors updating contact reords in standard batch sizes of 200
      CC_ComplianceContactUpdate ccu = new CC_ComplianceContactUpdate(contactList);
  	  Database.executebatch(ccu,1);  
    }
}