/*******************************************************
 *
 * Class Name : HttpCalloutInterlynx
 * Purpose : To send the leads to Interlynx System 
 * Developer : Manoj Bhujadi
 * Organization : Cognizant
 * 
********************************************************/
public class HttpCalloutInterlynx {
    public static Map<Id, Boolean> interlynxLeadMap = new Map<Id, Boolean>();
    /* input : newly created lead e.g. Trigger Context Variable Trigger.new 
     * Purpose :To send the list of new leadIds to future method 
	 */
    public static void afterInsert(List<Lead> newLeads){
        Set<Id> leadIds = new Set<Id>();
        for(Lead item : newLeads){
            if(!interlynxLeadMap.containsKey(item.Id) && item.Ready_to_share_with_Interlynx__c == true 
               && item.Lead_Shared_To_Interlynx__c == false){
                leadIds.add(item.Id);
                interlynxLeadMap.put(item.Id, true);
            }
        }
        if(leadIds.size() > 0)
            HttpCalloutInterlynx.getCalloutResponse(leadIds);
   }
    /* input : newly created lead e.g. Trigger Context Variable Trigger.new and Trigger.oldMap
     * Purpose :To send the list of new leadIds to future method 
	 */
    public static void afterUpdate(List<Lead> newLeads, Map<Id,Lead> oldLeadMap){
        //To send the list of updated leadIds to future method
        Set<Id> leadIds = new Set<Id>();
        for(Lead item : newLeads){
            if(!interlynxLeadMap.containsKey(item.Id) && item.Ready_to_share_with_Interlynx__c == true 
               && item.Lead_Shared_To_Interlynx__c == false 
               && oldLeadMap.get(item.Id).Ready_to_share_with_Interlynx__c == false){
                leadIds.add(item.Id);
                interlynxLeadMap.put(item.Id, true);
            }
        }
        if(leadIds.size() > 0)
            HttpCalloutInterlynx.getCalloutResponse(leadIds);
    }
    /* input : newly created Opportunity e.g. Trigger Context Variable Trigger.new
     * Purpose :To send the list of new leadIds to future method 
	 */
    public static void oppAfterInsert(List<Opportunity> newOpps){
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : newOpps){
            if(String.isNotBlank(opp.Lead_Id__C) && opp.Lead_Shared_To_Interlynx__c == true)
                oppIds.add(opp.Id);
        }
        if(oppIds.size() > 0)
            HttpCalloutInterlynx.convertedLeads(oppIds);
    }
    /* input : leadId to fetch the lead Information and generate payload to send to channel partner
     * Purpose :To connect with Interlynx System and send the payload.
	 */
    @future(callout=true)
    public static void getCalloutResponse(Set<Id> LeadIds) {
        List<Lead> allLeads = new List<Lead>();
        List<Lead> leads = [SELECT LUID__c, RSM_ID__c, CreatedDate, LastModifiedDate, VPTECH_Brand__c,Title, FirstName, 
                                LastName, Salutation, Email, Company, Street, City, State, PostalCode, 
                                Country, Website, LeadSource, Lead_Source_1__c, Lead_Source_2__c, Lead_Source_3__c, 
                                Lead_Source_4__c, Assigner_Comments__c, Description, Industry, First_Quote_Date__c, 
                                Distributor_overall_response_time__c, Id, Phone, Product__c, Partner_Comments__c, 
                                SIC_Description__c, NAICS_Description__c, Interlynx_Integration_Error__c, AIRD_Closed_Won_Total__c,
                                Distributor_Sales_Person__c, Dist_ID__c,Lead_Shared_To_Interlynx__c, Brand__c FROM Lead Where Id IN: LeadIds];
        try{
            User eloquaUser = [SELECT FirstName FROM User where LastName = 'Integraton' LIMIT 1];
            Interlynx_System__mdt integrationInfo = Interlynx_System__mdt.getInstance('Intergration_Information');
            
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(integrationInfo.Interlynx_Endpoint__c);
            req.setMethod('POST');
            req.setHeader('authorization', 'Bearer '+integrationInfo.Bearer_Token__c);
            List<Payload> genPayload = new List<Payload>();
            for(Lead item : leads){
                Lead thisLead = new Lead();
                thisLead.Id = item.id; //current lead
                Payload payload = new Payload();
                payload.RSMID = item.RSM_ID__c; 
                payload.CreatedDate = item.CreatedDate.date(); //yyyy-mm-dd
                if(item.VPTECH_Brand__c == 'Tuthill Pumps')
                    payload.Brand = 'Ingersoll Rand';
                else
                    payload.Brand = item.VPTECH_Brand__c;
                payload.FirstName = item.FirstName;
                payload.LastName = item.LastName;
                payload.Title =  item.Title;
                payload.LeadEmail = item.Email;
                payload.Company = item.Company;
                payload.Street = item.Street;
                payload.City = item.City;
                payload.State = item.State;
                payload.PostalCode = item.PostalCode;
                payload.Country = item.Country;
                payload.Website = item.Website;
                payload.LeadSource = item.LeadSource;
                payload.LeadSource1 = item.Lead_Source_1__c;
                payload.LeadSource2 =  item.Lead_Source_2__c;
                payload.LeadSource3 = item.Lead_Source_3__c;
                payload.LeadSource4 = item.Lead_Source_4__c;
                payload.AssignerComments = item.Assigner_Comments__c;
                payload.Industry = item.Industry;
                payload.SalesforceLeadID = item.Id; 
                payload.ChannelPartnerID =item.Dist_ID__c;
                payload.Phone = item.Phone;
                payload.Product = item.Product__c;
                if(String.isNotBlank(item.Brand__c)){
                    payload.Description = 'Model : '+item.Brand__c+'\n Description : '+item.Description;
                }
                else{
                    payload.Description = 'Description : '+ item.Description;
                }
                //payload.Model = item.Brand__c;
                genPayload.add(payload);
                String jsonString = JSON.serialize(genPayload);
                System.debug('Lead Id '+item.id+' JSON Sent to Interlynx System: '+jsonString);
                req.setBody(jsonString);
                HttpResponse response = http.send(req);
                Lead updateLead = new Lead();
                updateLead.Id = item.id; //current lead
                Integer statusCode = response.getStatusCode();
                switch on statusCode {
                    when 200 {
                        updateLead.Lead_Shared_To_Interlynx__c = true;
                        updateLead.Interlynx_Error_Banner__c = false;
                    }
                    when 400 {
                        if(String.isNotBlank(item.Interlynx_Integration_Error__c)){
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+ ':- \n'+
                                'Error Code -'+response.getStatusCode()+': The required fields for Interlynx System are may be Null.'+
                                'If Not, Kindly contatct your administartor \n \n'+item.Interlynx_Integration_Error__c;
                        }else{
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+ ':- \n'+
                                'Error Code -'+response.getStatusCode()+': The required fields for Interlynx System are may be Null.'+
                                'If Not, Kindly contatct your administartor';
                        }
                        updateLead.Ready_to_share_with_Interlynx__c = false;
                        updateLead.Interlynx_Error_Banner__c = true;
                    }
                    when 401 {
                        if(String.isNotBlank(item.Interlynx_Integration_Error__c)){
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+ ':- \n'+
                                'Error Code -'+response.getStatusCode()+': Unauthorized access. Kindly contatct your administartor \n \n'+item.Interlynx_Integration_Error__c;
                        }else{
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+ ':- \n'+
                                'Error Code -'+response.getStatusCode()+': Unauthorized access. Kindly contatct your administartor';
                        }
                        updateLead.Ready_to_share_with_Interlynx__c = false;
                        updateLead.Interlynx_Error_Banner__c = true;
                    }
                    /*when 500 {
if(String.isNotBlank(item.Interlynx_Integration_Error__c)){
updateLead.Interlynx_Integration_Error__c = system.today().format()+'\n'+
'Error Code -'+response.getStatusCode()+': '+
'The authentication error occurred.Kindly contatct your administartor \n \n'+item.Interlynx_Integration_Error__c;
}else{
updateLead.Interlynx_Integration_Error__c = system.today().format()+'\n'+
'Error Code -'+response.getStatusCode()+': '+
'The authentication error occurred.Kindly contatct your administartor';
}
updateLead.Ready_to_share_with_Interlynx__c = false;
updateLead.Interlynx_Error_Banner__c = true;
}*/
                    when else{
                        if(String.isNotBlank(item.Interlynx_Integration_Error__c)){
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+'\n'+
                                'Error Code -'+response.getStatusCode()+': '+
                                'Unhandled error, Kindly contatct your administartor \n \n'+item.Interlynx_Integration_Error__c;
                        }else{
                            updateLead.Interlynx_Integration_Error__c = system.today().format()+'\n'+
                                'Error Code -'+response.getStatusCode()+': '+
                                'Unhandled error, Kindly contatct your administartor';
                        }
                        updateLead.Ready_to_share_with_Interlynx__c = false;
                        updateLead.Interlynx_Error_Banner__c = true;
                    }
                }
                allLeads.add(updateLead);
            }
        }
        catch(Exception ex){
            for(Lead item : leads){
                Lead thisLead = new Lead();
                thisLead.Id = item.Id;
                thisLead.Ready_to_share_with_Interlynx__c = false;
                thisLead.Interlynx_Error_Banner__c = true;
                if(String.isNotBlank(item.Interlynx_Integration_Error__c)){
                    thisLead.Interlynx_Integration_Error__c = System.today().format()+'\n'+'Technical issue occurred while interfacing the lead with Interlynx,'+ 
                    																'Kindly try again or contatct your administartor \n \n'+item.Interlynx_Integration_Error__c;
                }else{
                    thisLead.Interlynx_Integration_Error__c = System.today().format()+'\n'+'Technical issue occurred while interfacing the lead with Interlynx,'+ 
                    																'Kindly try again or contatct your administartor';
                }
                allLeads.add(thisLead);
            }
        }
        finally{
            if(allLeads.size() > 0){
                updateMethod(allLeads);
            }
        }
    }
     /* Input : converted opportunity Ids
      * Purpose :To connect with Interlynx System and send the payload.
	  */
    @future(callout=true)
    public static void convertedLeads(Set<Id> oppIds){
        List<Opportunity> opps = [SELECT Id, Lead_Id__c FROM Opportunity WHERE Id IN: oppIds];
        try{
            Interlynx_System__mdt integrationInfo = Interlynx_System__mdt.getInstance('Intergration_Information');
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(integrationInfo.Interlynx_Endpoint__c);
            req.setMethod('POST');
            req.setHeader('authorization', 'Bearer '+integrationInfo.Bearer_Token__c);
            
            List<LeadStatusPayload> statusPayload = new List<LeadStatusPayload>();
            for(Opportunity opp : opps){
                LeadStatusPayload updatePayload = new LeadStatusPayload();
                updatePayload.LeadId = opp.Lead_Id__c;
                updatePayload.LeadStatus = 'Converted';
                updatePayload.OpportunityId = opp.Id;
                statusPayload.add(updatePayload);
                req.setBody(JSON.serialize(statusPayload));
                System.debug('Lead Id '+opp.Lead_Id__c+' Lead conversion payload '+statusPayload);
                HttpResponse response = http.send(req);
                if(response.getStatusCode() != 200){
                    sendMail(opps);    
                }
            }
        }
        catch(Exception ex){
			sendMail(opps);           
        }
    }
	 /* input : list of lead records 
      * Purpose :Update the lead records 
	  */
    public static void updateMethod(List<Lead> allLeads){
        List<Lead> failedLeads = new List<Lead>();
        Database.SaveResult[] result = Database.update(allLeads, false);
        for(Database.SaveResult updateResult : result) {
            if (!updateResult.isSuccess()) {
                for(Database.Error error : updateResult.getErrors()) {
                    Lead errorLead = new Lead();
                    errorLead.Id = updateResult.getId();
                    errorLead.Interlynx_Integration_Error__c ='Database Error: '+ error.getStatusCode() +': '+error.getMessage() +
                                                              '. Lead fields that affected this error: ' + error.getFields();
                    errorLead.Ready_to_share_with_Interlynx__c = false;
                    failedLeads.add(errorLead);
                }
            }
        }
    }
     /* 
      * Purpose :Fields which needs to be sent to Interlynx System as part of Integration Story.
	 */
    public class Payload {
        public String RSMID;
        public Datetime CreatedDate;
        public String Brand;
        public String FirstName;
        public String LastName;
        public String Title;
        public String LeadEmail;
        public String Company;
        public String Street;
        public String City;
        public String State;
        public String PostalCode;
        public String Country;
        public String Website;
        public String LeadSource;
        public String LeadSource1;
        public String LeadSource2;
        public String LeadSource3;
        public String LeadSource4;
        public String AssignerComments;
        public String Industry;
        public String SalesforceLeadID;
        public String ChannelPartnerID ;
        public String Phone;
        public String Product;
        public String Description;
        //public String Model; //MR business needs this Prashanth confirmed
        
    }
    /* 
     * Purpose : Fields which needs to be sent to Interlynx System as part of Integration Story post lead conversion
	 */
    public static void sendMail(List<Opportunity> opps){
        OrgWideEmailAddress fromAddress = [Select Id, Address FROM orgWideEmailAddress WHERE Address = 'sfdc_no_reply@irco.com' LIMIT 1];
        List<String> toAddress = new List<String>();
        List<String> ccAddresses = new List<String>();
        String emailHeader = '<html><body><p>Hello Team,<p> As we have encountered technical issue while connecting to Interlynx,'+ 
            'Sharing the Leads converted to opportunity over email. PFB the Opportunity Id against the Lead Id </p>';
        String emailBody = '';
        for(Opportunity opp : opps){
            emailBody = emailBody+'<P><b>Lead Id : '+opp.Lead_Id__c +'</b><br/>'
                +'<b>Opportunity Id : '+opp.Id +'</b><br/>'
                +'<b>Status : Converted </b></P> Thank you !</body></html>';
        }
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setOrgWideEmailAddressId(fromAddress.Id);
        toAddress.add('manojdinkar.bhujadi@cognizant.com');//support@irleads.com
        ccAddresses.add('lijisha.lnu@cognizant.com');//SFDC_Interlynx_Integration@irco.com
        email.setToAddresses(toAddress);
        email.setCCAddresses(ccAddresses);
        email.setSubject('Lead Converted To Opportunity');
        email.setHtmlBody(emailHeader+' \n '+emailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
    public class LeadStatusPayload{
        public String LeadId;
        public String LeadStatus;
        public String OpportunityId;
    }
}