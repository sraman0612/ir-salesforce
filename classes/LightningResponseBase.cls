// Baseline for minimum attributes of an AuraEnabled response
public with sharing virtual class LightningResponseBase {
    
	@AuraEnabled public Boolean success = true;
	@AuraEnabled public String errorMessage = '';    
}