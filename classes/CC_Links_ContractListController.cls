global without sharing class CC_Links_ContractListController{
  public static Id accId;
  public static boolean showcontracts{get;set;}
  
  public CC_Links_ContractListController(CC_PavilionTemplateController controller) {
    accId = controller.acctid;
  }
  
  global static List<contract> getcontractsList(){
    List<contract> contrList =  new List<contract>();
    contrList = [SELECT Id,ContractNumber, CC_Lead_Owner__c, CC_Lead_Owner__r.Name,
                        Account.Name, CC_Sub_Type__c 
                   FROM Contract
                  WHERE (AccountId =:accId OR Account.ParentId =:accId) AND
                       CC_Contract_Status__c = 'Current']; 
    showcontracts = true;
    return contrList; 
  }
  
    public static List<SelectOption>getUsersList(){
        Id currentuserid = userinfo.getUserId();
        List <SelectOption> SelectusersList = new List <SelectOption> ();
        for(user u :[SELECT id,Name,Email 
                       FROM User 
                      WHERE isActive = true AND AccountID =:accId AND CC_Sales__c=TRUE]){
              SelectusersList.add(new SelectOption(u.Id, u.name));        
         }
        
        return SelectusersList;
    }
    
  @RemoteAction
  public static String saveleadOwnerAssigned(String selectedcontractId, String selecteduserId){
    contract cont = new contract(id =selectedcontractId, CC_Lead_Owner__c = selecteduserId );
    update cont;
    return 'Success';
  }
}