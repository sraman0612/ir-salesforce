@isTest
public Class CPQProductSearchControllerTest{
  public testmethod static void test1(){
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_OEM_EU').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for CPQ Embedded CTLR';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'United States';
    a.ShippingState = 'CA';
    a.ShippingStreet = '13, street2';
    a.ShippingPostalCode = '123';  
    a.CTS_Global_Shipping_Address_1__c = '13';
    a.CTS_Global_Shipping_Address_2__c = 'street2';     
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
    //a.Account_Division__c = 'a2c4Q000006rW5N';
    insert a;
     Id airOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_OEM_EU').getRecordTypeId();
    Opportunity o = new Opportunity();
    o.name = 'Test Opportunity';
    o.stagename = 'Qualify';
    o.amount = 1000000;
    o.closedate = system.today();
    o.RecordTypeId = airOppRTID;
    o.AccountId = a.Id;
    insert o; 
    Test.startTest();
    PageReference testPage = Page.CPQProductSearch;
    testPage.getHeaders().put('Referer', 'https://www.salesforce.com?id='+ o.Id);
    Test.setCurrentPage(testPage);
    ApexPages.StandardController sc =new ApexPages.StandardController(a);
    CPQProductSearchController scext =new CPQProductSearchController(sc);
  }
}