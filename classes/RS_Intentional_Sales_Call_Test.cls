@isTest
private class RS_Intentional_Sales_Call_Test {

  // Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
  // Developer Name of the New Business Record Type
  public static final string RECORD_TYPE_NEW_BUSINESS = 'RS_Business';
  // Default Document Values
  private static final String DOC_TITLE = 'someOtherTitle';
  private static final String DOC_PATH = 'someOtherTitle.pdf';
  private static final Blob DOC_DATA = Blob.valueOf('This is test data');
  // Key initiatives to create on Setup
  private static final Integer KI_SIZE = 5;
  // Days offset for the key initiatives
  private static final Integer DAYS_OFFSET = 3;

  /**
   *	Sets the data need for the tests
   */
  @testSetup static void mySetup() {
    // Insert three Key Initiatives
    List<Key_Initiatives__c> initiatives = new List<Key_Initiatives__c>();
    for (Integer i=0;i < KI_SIZE;i++){
      initiatives.add(new Key_Initiatives__c(Start_Date__c = Date.today().addDays((-1) * DAYS_OFFSET), End_Date__c = Date.today().addDays(DAYS_OFFSET)));
    }
    insert initiatives;
    // Insert a Document for each Initiative
    List<ContentVersion> contentVersions = new List<ContentVersion>();
    for (Integer i=0;i < KI_SIZE;i++){
      contentVersions.add(new ContentVersion(Title = DOC_TITLE + i, Account_Initiative__c = initiatives.get(i).Id, PathOnClient = DOC_PATH, VersionData = DOC_DATA));
    }
    insert contentVersions;
  }

  static testMethod void testIntentionalSalesCall(){
    String checkQuery = '';
    // Set up the Account record.
    Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    String accountName = 'Test Account';
    String devPlan = 'Test Plan For RS_Intentional_Sales_Call_Test';
    Account a = new Account(RecordTypeId = rshvacRTID, Name = accountName, RS_Development_Plan__c = devPlan);
    insert a;

    // sanity check on new account
    checkQuery = 'SELECT Name FROM Account WHERE Id = \'' + a.Id + '\'';
    List<Account> createdAccount = Database.query(checkQuery);
    System.assertEquals('Test Account', createdAccount[0].Name);

    String newAccountId = a.id;

    // Insert junction records to the key initiatives
    List<Key_Initiatives__c> initiatives = [SELECT Id FROM Key_Initiatives__c WHERE End_Date__c > TODAY];
    List<Account_Initiatives_Junction__c> junctions = new List<Account_Initiatives_Junction__c>();
    for (Key_Initiatives__c i : initiatives){
      junctions.add(new Account_Initiatives_Junction__c(Account__c = a.Id, Key_Initiative__c = i.Id, Status__c = 'Open'));
    }
    insert junctions;

    // Setup Tasks
    Task responseTask = new Task(RS_Development_Plan__c = false,
                        Subject = 'Test Response Task',
                        Description = 'Test Response Task Description',
                        Status = 'Not Started',
                        WhatId = a.Id);
    insert responseTask;

    // sanity check on new task
    checkQuery = 'SELECT Subject FROM Task WHERE Id = \'' + responseTask.Id + '\'';
    List<Task> createdTask1 = Database.query(checkQuery);
    System.assertEquals('Test Response Task', createdTask1[0].Subject);

    Task devTask = new Task(RS_Development_Plan__c = true,
                        Subject = 'Test Dev Task',
                        Description = 'Test Dev Task Description',
                        Status = 'Not Started',
                        WhatId = a.Id);
    insert devTask;

    // sanity check on new task
    checkQuery = 'SELECT Subject FROM Task WHERE Id = \'' + devTask.Id + '\'';
    List<Task> createdTask2 = Database.query(checkQuery);
    System.assertEquals('Test Dev Task', createdTask2[0].Subject);

    // Setup Opportunity
    Id newBusinessRTID = [SELECT Id FROM RecordType WHERE DeveloperName = :RECORD_TYPE_NEW_BUSINESS AND SObjectType = 'Opportunity' LIMIT 1].Id;
    String oppName = 'My Opportunity RS_Intentional_Sales_Call_Test_Opp';
    Opportunity opp = new Opportunity(Name = oppName, AccountId = a.Id, RecordTypeId = newBusinessRTID, StageName = 'Prospecting', CloseDate=Date.today());
    insert opp;

    // check new opp
    checkQuery = 'SELECT Name FROM Opportunity WHERE Id = \'' + opp.Id + '\'';
    List<Opportunity> createdOpportunity = Database.query(checkQuery);
    System.assertEquals('My Opportunity RS_Intentional_Sales_Call_Test_Opp', createdOpportunity[0].Name);

    // Setup Page class
    Test.startTest();
      PageReference pageRef = Page.RS_Intentional_Sales_Call;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('id', a.id);
      RS_Intentional_Sales_Call controller = new RS_Intentional_Sales_Call();
      // Get the Account properties
      Account controllerAccount = controller.currentAccount;
      String controllerAccountName = controller.getAccountName();
      String controllerPlan = controller.getAccountPlan();
      // Get the task lists
      List<Task> controllerResponseTasks = controller.responseTasks;
      List<Task> controllerDevTasks = controller.devTasks;
      // Get the Business Opportunity
      List<Opportunity> controllerOpps = controller.newBusinessOpportunities;
      // Get the junction wrappers
      List<RS_Intentional_Sales_Call.JunctionWrapper> wrappers = controller.junctionWrappers;
      // Get the counts of the lists
      Integer devTaskCount = controller.getDevTaskCount();
      Integer responseTaskCount = controller.getResponseTaskCount();
      Integer oppCount = controller.getNewBusinessOpportunitiesCount();
      Integer junctionCount = controller.getKeyInitiativesCount();
    Test.stopTest();

    // Verify that the Account retrieved is the correct
    System.assertEquals(newAccountId, controllerAccount.Id);
    // Verify the Account Name from the controller is correct
    System.assertEquals(accountName, controllerAccountName);
    // Verify the Account Name from the controller is correct
    System.assertEquals(devPlan, controllerPlan);

    // Verify the response task count is correct
    System.assertEquals(1, devTaskCount);

    // Verify the junction count is correct
    System.assertEquals(1, responseTaskCount);

    // Verify the junction count is correct
    System.assertEquals(1, oppCount);

    // Verify the junction count is correct
    System.assertEquals(KI_SIZE, junctionCount);
  }
}