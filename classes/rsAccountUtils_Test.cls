@isTest
private class rsAccountUtils_Test {
    
    static testMethod void rsAccountUtilsTest() {
    
        String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'RS HVAC'].Id;
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.County__c = 'USA';
        acc.RecordTypeId = strRecordTypeId;
        acc.Sales_Person_Id__c = userInfo.getUserId();
        insert acc;
        
        String getUser = userInfo.getUserId();
        rsAccountUtils rs = new rsAccountUtils();
        rs.assignRshvacUsersToAccounts();
        rs.assignRshvacUser(getUser);
        
   }
    
 }