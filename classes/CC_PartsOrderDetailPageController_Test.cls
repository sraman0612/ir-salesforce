/**
* @createdBy   Sangram Ray
* @date         07/30/2016
* @description  Test Class for CC_PartsOrderDetailPageController
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest
public class CC_PartsOrderDetailPageController_Test {
    static PavilionSettings__c pavillionCommunityProfileId;
    static PavilionSettings__c  pavilionPublicGroupID;
    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Account account;
    static CC_Order__c order;
    static CC_Order_Item__c orderItem;
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
    
    // test constuctor
    @isTest
    public static void constuctor_Test() {
        setup();
        System.runAs (testUser) {
            // insert order
            order = TestUtilityClass.createOrder(account.Id);
            order.CC_Quote_Order_Date__c = Date.today();
            order.CC_Addressee_name__c = 'Test Name';
            order.PO__c = '123';
            order.CC_Order_Number_Reference__c = '123';
            order.Hold_Code__c = 'code';
            order.Complete_Date__c = Date.today();
            order.CC_Status__c = 'Open';
            order.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId();
            insert order;
            
            // insert shipment
            orderItem = new CC_Order_Item__c(
                Order__c = order.Id,
                CC_Description__c = 'Test Desc',
                CC_Quantity__c = 1,
                CC_UnitPrice__c = 10,
                CC_TotalPrice__c = 10
            );
            insert orderItem;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PartsOrderDetailPage'));
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PartsOrderDetailPageController cc_PartsOrderDetailPageController = new CC_PartsOrderDetailPageController(cc_PavilionTemplateController);
        }
    }
    
    // test constuctor without Id
    @isTest
    public static void constuctorWithoutId_Test() {
        setup();
        System.runAs (testUser) {
            // insert order
            order = TestUtilityClass.createOrder(account.Id);
            order.CC_Quote_Order_Date__c = Date.today();
            order.CC_Addressee_name__c = 'Test Name';
            order.PO__c = '123';order.CC_Order_Number_Reference__c = '123';
            order.Hold_Code__c = 'code';
            order.Complete_Date__c = Date.today();
            order.CC_Status__c = 'Open';
            order.RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId();
            insert order;
            
            // insert shipment
            orderItem = new CC_Order_Item__c(
                Order__c = order.Id,
                CC_Description__c = 'Test Desc',
                CC_Quantity__c = 1,
                CC_UnitPrice__c = 10,
                CC_TotalPrice__c = 10
            );
            insert orderItem;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PartsOrderDetailPage'));
            try{
            CC_PartsOrderDetailPageController cc_PartsOrderDetailPageController = new CC_PartsOrderDetailPageController(cc_PavilionTemplateController);
            }
            Catch(Exception e){}
        }
    }
    
    public static void setup() {
        // for CAD Pricing 
          account = TestUtilityClass.createAccount();
         account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
         account.name = 'Test';
         account.CC_Customer_Number__c = '5621';
        insert account;

        contact = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN',
            FirstName = 'FS',
            AccountId = account.Id,
            Email = 'test@test.ingersollrand.com'
        );
        insert contact;

        portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%CC_PavilionCommunityUser%' Limit 1];
        
        testUser = new User(
            Username = System.now().millisecond() + 'test12345@test.ingersollrand.com',
            ContactId = contact.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert testUser;
        
         //AccountTeamMember for Account share error
        
        AccountTeamMember atm = TestUtilityClass.createATM(account.Id,testUser.Id);
        insert atm;
        
        AccountShare accShare = TestUtilityClass.createAccShare(account.Id,testUser.Id);
        insert accShare;
        
        
    }
}