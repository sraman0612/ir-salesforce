@isTest
public with sharing class Manage_Notification_Subscriptio_CtrlTest {
    
    @TestSetup
    private static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account acc = CTS_TestUtility.createAccount('Test Account', true);

        Contact communityContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false);
        communityContact.CTS_IOT_Community_Status__c = 'Approved';
        communityContact.MobilePhone = '555-555-5555';
        insert communityContact;

        Id profileId = [SELECT Id FROM Profile WHERE Name = :settings.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(communityContact.Id, profileId, false);    
        communityUser.LastName = 'Comm_User_Test1';
        insert communityUser;    

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc.Id, assetRecTypeId, false); 
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', acc.Id, assetRecTypeId, false);   
        insert new Asset[]{asset1, asset2};

        Notification_Type__c notificationType1 = NotificationsTestDataService.createNotificationType(false, 'Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test');

        Notification_Type__c notificationType2 = NotificationsTestDataService.createNotificationType(false, 'Trip Test', false, 'Trip',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test'); 
            
        insert new Notification_Type__c[]{notificationType1, notificationType2};
    }
}
    @isTest
    private static void testInitGlobal(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');   
            
        insert new Notification_Subscription__c[]{subscription1};

        Test.startTest();

        System.runAs(communityUser){
            
            Manage_Notification_Subscriptions_Ctrl.InitResponse response = Manage_Notification_Subscriptions_Ctrl.getInit(null);
            system.assertEquals(true, response.isPortalUser);
            system.assertEquals(communityUser.Id, response.currentUserId); 
            system.assertEquals('555-555-5555', response.mobilePhone); 
            system.assertEquals(null, response.record);
            system.assertEquals(null, response.objectType);
            system.assertEquals(2, response.subscriptionOptions.size()); 
            
            system.assertEquals(subscription1.Id, response.subscriptionOptions[0].subscription.Id);
            system.assertEquals(notificationTypes[0].Id, response.subscriptionOptions[0].notificationType.Id);
            system.assertEquals(true, response.subscriptionOptions[0].subscribed);
            system.assertEquals(false, response.subscriptionOptions[0].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[0].optedOut);
            system.assertEquals(null, response.subscriptionOptions[0].overarchingSubscription);           

            system.assertEquals(null, response.subscriptionOptions[1].subscription);
            system.assertEquals(notificationTypes[1].Id, response.subscriptionOptions[1].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[1].subscribed);  
            system.assertEquals(true, response.subscriptionOptions[1].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[1].optedOut);
            system.assertEquals(null, response.subscriptionOptions[1].overarchingSubscription);                        
        }

        Test.stopTest();
    }

    @isTest
    private static void testInitSiteSpecific(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, acct.Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');  
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');               
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser){
            
            Manage_Notification_Subscriptions_Ctrl.InitResponse response = Manage_Notification_Subscriptions_Ctrl.getInit(acct.Id);
            system.assertEquals(true, response.isPortalUser);
            system.assertEquals(communityUser.Id, response.currentUserId); 
            system.assertEquals('555-555-5555', response.mobilePhone); 
            system.assertEquals(acct.Id, response.record.Id);
            system.assertEquals('Account', response.objectType);
            system.assertEquals(2, response.subscriptionOptions.size()); 
            
            system.assertEquals(subscription1.Id, response.subscriptionOptions[0].subscription.Id);
            system.assertEquals(notificationTypes[0].Id, response.subscriptionOptions[0].notificationType.Id);
            system.assertEquals(true, response.subscriptionOptions[0].subscribed);
            system.assertEquals(false, response.subscriptionOptions[0].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[0].optedOut);
            system.assertEquals('All Sites', response.subscriptionOptions[0].overarchingSubscription.Scope__c);             

            system.assertEquals(null, response.subscriptionOptions[1].subscription);
            system.assertEquals(notificationTypes[1].Id, response.subscriptionOptions[1].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[1].subscribed);      
            system.assertEquals(true, response.subscriptionOptions[1].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[1].optedOut);
            system.assertEquals(null, response.subscriptionOptions[1].overarchingSubscription);                   
        }

        Test.stopTest();
    }    

    @isTest
    private static void testInitAssetSpecific(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');  
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');               
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser){
            
            Manage_Notification_Subscriptions_Ctrl.InitResponse response = Manage_Notification_Subscriptions_Ctrl.getInit(assets[0].Id);
            system.assertEquals(true, response.isPortalUser);
            system.assertEquals(communityUser.Id, response.currentUserId); 
            system.assertEquals('555-555-5555', response.mobilePhone); 
            system.assertEquals(assets[0].Id, response.record.Id);
            system.assertEquals('Asset', response.objectType);
            system.assertEquals(2, response.subscriptionOptions.size()); 
          
            system.assertEquals(subscription1.Id, response.subscriptionOptions[0].subscription.Id);
            system.assertEquals(notificationTypes[0].Id, response.subscriptionOptions[0].notificationType.Id);
            system.assertEquals(true, response.subscriptionOptions[0].subscribed);
            system.assertEquals(false, response.subscriptionOptions[0].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[0].optedOut);
            system.assertEquals('All Sites', response.subscriptionOptions[0].overarchingSubscription.Scope__c);             

            system.assertEquals(null, response.subscriptionOptions[1].subscription);
            system.assertEquals(notificationTypes[1].Id, response.subscriptionOptions[1].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[1].subscribed); 
            system.assertEquals(true, response.subscriptionOptions[1].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[1].optedOut);
            system.assertEquals(null, response.subscriptionOptions[1].overarchingSubscription);            
        }

        Test.stopTest();
    }    

    @isTest
    private static void testInitAssetSpecificAssetOptOut(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');  
        subscription1.Opt_Out__c = true;

        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser.Id, acct.Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');           

        Notification_Subscription__c subscription3 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');         
            
        insert new Notification_Subscription__c[]{subscription1, subscription2, subscription3};

        Test.startTest();

        System.runAs(communityUser){
            
            Manage_Notification_Subscriptions_Ctrl.InitResponse response = Manage_Notification_Subscriptions_Ctrl.getInit(assets[0].Id);
            system.assertEquals(true, response.isPortalUser);
            system.assertEquals(communityUser.Id, response.currentUserId); 
            system.assertEquals('555-555-5555', response.mobilePhone); 
            system.assertEquals(assets[0].Id, response.record.Id);
            system.assertEquals('Asset', response.objectType);
            system.assertEquals(2, response.subscriptionOptions.size()); 
          
            system.assertEquals(subscription1.Id, response.subscriptionOptions[0].subscription.Id);
            system.assertEquals(notificationTypes[0].Id, response.subscriptionOptions[0].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[0].subscribed);
            system.assertEquals(true, response.subscriptionOptions[0].unsubscribed);
            system.assertEquals(true, response.subscriptionOptions[0].optedOut);
            system.assertEquals(acct.Id, response.subscriptionOptions[0].overarchingSubscription.Record_ID__c);             

            system.assertEquals(null, response.subscriptionOptions[1].subscription);
            system.assertEquals(notificationTypes[1].Id, response.subscriptionOptions[1].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[1].subscribed); 
            system.assertEquals(true, response.subscriptionOptions[1].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[1].optedOut);
            system.assertEquals(null, response.subscriptionOptions[1].overarchingSubscription);            
        }

        Test.stopTest();
    }      

    @isTest
    private static void testInitAssetSpecificSiteOptOut(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');  
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser.Id, acct.Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');   
        subscription2.Opt_Out__c = true;

        Notification_Subscription__c subscription3 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');         
            
        insert new Notification_Subscription__c[]{subscription1, subscription2, subscription3};

        Test.startTest();

        System.runAs(communityUser){
            
            Manage_Notification_Subscriptions_Ctrl.InitResponse response = Manage_Notification_Subscriptions_Ctrl.getInit(assets[0].Id);
            system.assertEquals(true, response.isPortalUser);
            system.assertEquals(communityUser.Id, response.currentUserId); 
            system.assertEquals('555-555-5555', response.mobilePhone); 
            system.assertEquals(assets[0].Id, response.record.Id);
            system.assertEquals('Asset', response.objectType);
            system.assertEquals(2, response.subscriptionOptions.size()); 
          
            system.assertEquals(subscription1.Id, response.subscriptionOptions[0].subscription.Id);
            system.assertEquals(notificationTypes[0].Id, response.subscriptionOptions[0].notificationType.Id);
            system.assertEquals(true, response.subscriptionOptions[0].subscribed);
            system.assertEquals(false, response.subscriptionOptions[0].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[0].optedOut);
            system.assertEquals(null, response.subscriptionOptions[0].overarchingSubscription);             

            system.assertEquals(null, response.subscriptionOptions[1].subscription);
            system.assertEquals(notificationTypes[1].Id, response.subscriptionOptions[1].notificationType.Id);
            system.assertEquals(false, response.subscriptionOptions[1].subscribed); 
            system.assertEquals(true, response.subscriptionOptions[1].unsubscribed);
            system.assertEquals(false, response.subscriptionOptions[1].optedOut);
            system.assertEquals(null, response.subscriptionOptions[1].overarchingSubscription);            
        }

        Test.stopTest();
    }     
    
    @isTest
    private static void testUpdateSubscriptionsGlobal(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = null;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(null, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email;SMS', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Weekly', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }

    @isTest
    private static void testUpdateSubscriptionsGlobalSubUpdate(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');   
            
        insert new Notification_Subscription__c[]{subscription1};        
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = null;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = subscription1;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;            
            option1.selectedDeliveryMethods = 'Email';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(null, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Weekly', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }    

    @isTest
    private static void testUpdateSubscriptionsSiteSpecific(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = acct.Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;            
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(acct.Id, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email;SMS', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Weekly', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }   
    
    @isTest
    private static void testUpdateSubscriptionsAssetSpecific(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = assets[0].Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;            
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(assets[0].Id, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email;SMS', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Weekly', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }  
    
    @isTest
    private static void testUpdateSubscriptionsAssetSpecificGlobalOverride(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);

        Notification_Subscription__c overarchingSubscription = NotificationsTestDataService.createSubscription(true, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');         
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = assets[0].Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;            
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;
            option1.overarchingSubscription = overarchingSubscription;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c Where Id != :overarchingSubscription.Id];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(assets[0].Id, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email;SMS', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Weekly', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    } 
    
    @isTest
    private static void testUpdateSubscriptionsAssetSpecificGlobalMatch(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);

        Notification_Subscription__c overarchingSubscription = NotificationsTestDataService.createSubscription(true, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');         
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = assets[0].Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = true;
            option1.unsubscribed = false;
            option1.optedOut = false;            
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = true;
            option1.weeklyEmailDigest = false;
            option1.overarchingSubscription = overarchingSubscription;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c
                From Notification_Subscription__c];
            
            system.assertEquals(overarchingSubscription.Id, subscription.Id);
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(null, subscription.Record_ID__c);
            system.assertEquals(false, subscription.Opt_Out__c);
            system.assertEquals('Email;SMS', subscription.Delivery_Methods__c);
            system.assertEquals('Real-Time;Daily', subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }       
    
    @isTest
    private static void testUpdateSubscriptionsAssetSpecificUnsubscribe(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');  
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');               
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};        
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = assets[0].Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = subscription1;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = false;
            option1.unsubscribed = true;
            option1.optedOut = false;
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c[] subscriptions = 
               [Select Id, OwnerId, Scope__c, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c 
                From Notification_Subscription__c];
            
            system.assertEquals(1, subscriptions.size());
            system.assertEquals('All Sites', subscriptions[0].Scope__c);
        }

        Test.stopTest();
    }  
    
    @isTest
    private static void testUpdateSubscriptionsAssetSpecificOptOut(){

        Account acct = [Select Id, Name From Account LIMIT 1];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c DESC];
        system.assertEquals(0, [Select Count() From Notification_Subscription__c]);
        
        Test.startTest();

        System.runAs(communityUser){

            Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest request = new Manage_Notification_Subscriptions_Ctrl.UpdateSubscriptionsRequest();
            request.recordId = assets[0].Id;

            Manage_Notification_Subscriptions_Ctrl.SubscriptionOption option1 = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption();
            option1.subscription = null;
            option1.notificationType = notificationTypes[0];   
            option1.subscribed = false;
            option1.unsubscribed = false;
            option1.optedOut = true;            
            option1.selectedDeliveryMethods = 'Email;SMS';        
            option1.dailyEmailDigest = false;
            option1.weeklyEmailDigest = true;

            request.subscriptionOptions = new Manage_Notification_Subscriptions_Ctrl.SubscriptionOption[]{option1};

            LightningResponseBase response = Manage_Notification_Subscriptions_Ctrl.updateSubscriptions(JSON.serialize(request));
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            Notification_Subscription__c subscription = 
               [Select Id, OwnerId, Record_ID__c, Notification_Type__c, Delivery_Methods__c, Delivery_Frequencies__c, Opt_Out__c 
                From Notification_Subscription__c];
            
            system.assertEquals(communityUser.Id, subscription.OwnerId);
            system.assertEquals(notificationTypes[0].Id, subscription.Notification_Type__c);
            system.assertEquals(true, subscription.Opt_Out__c);
            system.assertEquals(assets[0].Id, subscription.Record_ID__c);
            system.assertEquals(null, subscription.Delivery_Methods__c);
            system.assertEquals(null, subscription.Delivery_Frequencies__c);
        }

        Test.stopTest();
    }     
}