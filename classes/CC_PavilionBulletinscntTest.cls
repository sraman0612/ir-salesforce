@isTest
public class CC_PavilionBulletinscntTest 
{
    static testmethod void UnitTest_CC_PavilionBulletinscnt() {
        List<ContentVersion> contentVersionList =new  List<ContentVersion>();
        CC_PavilionTemplateController controller;
        // Creation of Test Data
        Account newAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert newAccount;
        System.assertEquals('Sample Account',newAccount.Name);
        System.assertNotEquals(null, newAccount.Id);
        Account secondAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert secondAccount;
        System.assertEquals('Sample Account',secondAccount.Name);
        System.assertNotEquals(null, secondAccount.Id);
        Contract newContract =TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Commercial Utility',newAccount.Id);
        insert newContract;
        System.assertEquals('Commercial Utility', newContract.CC_Sub_Type__c);
        System.assertNotEquals(null, newContract.Id);
        ContentVersion contentVersionInsertFirst = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Parts');
        contentVersionList.add(contentVersionInsertFirst); 
        ContentVersion contentVersionInsertSecond = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Sales');
        contentVersionList.add(contentVersionInsertSecond); 
        ContentVersion contentVersionInsertThird = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Service & Warranty');
        contentVersionList.add(contentVersionInsertThird); 
        ContentVersion contentVersionInsertFourth = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Parts Pricing');
        contentVersionList.add(contentVersionInsertFourth); 
        ContentVersion contentVersionInsertFifth = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Sales Pricing');
        contentVersionList.add(contentVersionInsertFifth); 
        insert contentVersionList;  
        PavilionSettings__c newPavilionSettings = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',secondAccount.Id);
        insert newPavilionSettings;
        Test.startTest();
        CC_PavilionBulletinscnt pavBulletins = new CC_PavilionBulletinscnt(controller);
        CC_PavilionBulletinscnt.getMarkets(newAccount.id);
        CC_PavilionBulletinscnt.getYears();
        CC_PavilionBulletinscnt.getBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id);
        try
        {
            CC_PavilionBulletinscnt.searchBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id, 'Parts');
            CC_PavilionBulletinscnt.searchBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id, 'Sales');
            CC_PavilionBulletinscnt.searchBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id, 'Service & Warranty');
            CC_PavilionBulletinscnt.searchBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id, 'Parts Pricing');
            CC_PavilionBulletinscnt.searchBulletins(true, true, true, true, true, 'Japan', 'USD', newAccount.id, 'Sales Pricing');
        }
        Catch(Exception e){}
        Test.stopTest();
    }
}