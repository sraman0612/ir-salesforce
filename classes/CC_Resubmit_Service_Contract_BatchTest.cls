@isTest
public with sharing class CC_Resubmit_Service_Contract_BatchTest {
    
    @TestSetup
    private static void createData(){	
    	
        TestDataUtility dataUtility = new TestDataUtility();
            	
    	User user1 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user1.Alias = 'alias123';
    	
    	User user2 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user2.Alias = 'alias124';
    	
    	User user3 = TestUtilityClass.createUser('CC_Sales Rep', null);
    	user3.Alias = 'alias125';    
    	
    	insert new User[]{user1, user2, user3};	        

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;           
        
        Account acct = [Select Id From Account LIMIT 1];

    	Contract[] contracts = new Contract[]{};
    	
        for (Integer i = 0; i < 15; i++){       
        	
        	Id userId;
        	
        	if (i < 5){
        		userId = user1.Id;
        	}
        	else if (i < 10){
        		userId = user2.Id;
        	}
        	else{
        		userId = user3.Id;
        	}
        	 	
        	Contract c = TestDataUtility.createCCServiceContract(acct.Id, 'Monthly');
        	c.CC_Contract_Status__c = 'Approved'; 	
        	c.CC_Unlock_Requested__c = true;
        	c.CC_Unlock_Requestor__c = userId;
        	 	
			contracts.add(c);  	
        }
        
        for (Integer i = 0; i < 5; i++){       
        	
        	Contract c = TestDataUtility.createCCServiceContract(acct.Id, 'Monthly');
        	c.CC_Contract_Status__c = 'Approved'; 	
        	c.CC_Unlock_Requested__c = false;
        	c.CC_Unlock_Requestor__c = null;
        	 	
			contracts.add(c);  	
        }         	
        
        insert contracts;
    }
    
    @isTest
    private static void testBatchSuccess(){
    	
    	Contract[] contracts = [Select Id, CC_Contract_Status__c, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(20, contracts.size());
    	
    	Integer unlockRequestCount = 0;
    	
    	for (Contract c : contracts){
    		
    		system.assertEquals('Approved', c.CC_Contract_Status__c);
    		
    		if (c.CC_Unlock_Requested__c && c.CC_Unlock_Requestor__c != null){
    			unlockRequestCount++;
    		}
    	}
    	
    	// Lock all of the contracts before running the batch job
		Approval.lock(contracts);
		    				  	
    	Test.startTest();
    	
    	Database.executeBatch(new CC_Resubmit_Service_Contract_Batch(), 25);
    	
    	Test.stopTest();
    	
		// Verify the results
		contracts = [Select Id, CC_Contract_Status__c, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(20, contracts.size());
    	
    	Integer unlockCount = 0;
    	
    	for (Contract c : contracts){
    		    		
    		if (c.CC_Contract_Status__c == CC_Resubmit_Service_Contract_Batch.defaultContractStatus && !c.CC_Unlock_Requested__c && c.CC_Unlock_Requestor__c == null){
    			unlockCount++;
    		}
    	}	
    	
    	system.assertEquals(15, unlockCount);	    	
    	system.assertEquals(15, EmailService.emailsSent);
    }
    
   @isTest
    private static void testBatchFailure(){
    	
    	Contract[] contracts = [Select Id, CC_Contract_Status__c, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(20, contracts.size());
    	
    	Integer unlockRequestCount = 0;
    	
    	for (Contract c : contracts){
    		
    		system.assertEquals('Approved', c.CC_Contract_Status__c);
    		
    		if (c.CC_Unlock_Requested__c && c.CC_Unlock_Requestor__c != null){
    			unlockRequestCount++;
    		}
    	}
    	
    	// Lock all of the contracts before running the batch job
		Approval.lock(contracts);
		    				  	
    	Test.startTest();
    	
        CC_Resubmit_Service_Contract_Batch.forceErrors = true;
        
    	Database.executeBatch(new CC_Resubmit_Service_Contract_Batch(), 25);
    	
    	Test.stopTest();
    	
		// Verify the results
		contracts = [Select Id, CC_Contract_Status__c, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(20, contracts.size());
    	
    	Integer unlockCount = 0;
    	
    	for (Contract c : contracts){
    		    		
    		if (c.CC_Contract_Status__c == CC_Resubmit_Service_Contract_Batch.defaultContractStatus && !c.CC_Unlock_Requested__c && c.CC_Unlock_Requestor__c == null){
    			unlockCount++;
    		}
    	}	
    	
    	system.assertEquals(0, unlockCount);	    	
    	system.assertEquals(15, EmailService.emailsSent);
    }    
}