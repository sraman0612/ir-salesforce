/**
 * Test class of CTS_IOT_Navigation_Controller
 **/
@isTest
private class CTS_IOT_Navigation_ControllerTest {
    
    @testSetup
    static void setup(){
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
    }
    @isTest
    static void testGetInit(){
        Test.startTest();
        CTS_IOT_Navigation_Controller.InitResponse initRes = CTS_IOT_Navigation_Controller.getInit();
        System.assert(initRes != null);
        Test.stopTest();
    }
}