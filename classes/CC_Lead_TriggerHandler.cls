public class CC_Lead_TriggerHandler {
    
    public static void leadassignmentProcess(Map<String, List<Lead>> newLeadMap){
        Id clubcarQueueId = [SELECT id,Name FROM Group WHERE type='Queue' AND Name=:StringConstantsFinalClass.clubCarqueueName LIMIT 1].id;
        List<Lead> leadsList = new List<Lead>();
        for(String recType : newLeadMap.keySet()){
            if(recType.contains('Club Car') && !newLeadMap.get(recType).isEmpty()){leadsList.addAll(newLeadMap.get(recType));}
        }
        
        for(Lead l : leadsList){
            system.debug('@@@ starting lead assignment for Lead ' + l.Id);
            system.debug('@@@ Lead Owner is ' + l.OwnerId);
            system.debug('@@@ CC Lead Assign Q is ' + clubcarQueueId);
            system.debug('@@@ Product Authorization is ' + l.CC_Prod_Authorization__c);
            system.debug('@@@ Country is ' + l.Country);
            system.debug('@@@ PostalCode is ' + l.PostalCode);
            if(l.OwnerId == clubcarQueueId && l.CC_Prod_Authorization__c != NULL && l.Country != NULL ){ 
                system.debug('@@@ initiating partner search');
                CC_dealerSearch.DealerInfo dlr = CC_dealerSearch.getPartnerForLeadAssignment(l.Country,l.PostalCode,l.CC_Prod_Authorization__c);
                if(null != dlr){
                    system.debug('@@@ partner to assign is ' + dlr);
                    CC_Sales_Rep__c sr = [SELECT Id, CC_Sales_Rep__c FROM CC_Sales_Rep__c WHERE CC_Sales_Person_Number__c = :dlr.SalesPersonNumber];
                    if(dlr.OverridePartner){
                        l.OwnerId = sr.CC_Sales_Rep__c;
                        l.CC_Sales_Rep__c = sr.Id;
                    } else {
                        Contract c = [SELECT CC_Lead_Owner__c FROM Contract WHERE Id = :dlr.AgrId];
                        l.OwnerId = null != c.CC_Lead_Owner__c ? c.CC_Lead_Owner__c : l.OwnerId;
                        l.CC_Sales_Rep__c = sr.Id;
                    }
                }
            }
        }
    }
    
    public static void setcountrystates(Map<String, List<Lead>> newLeadMap,Map<String, List<Lead>> oldLeadMap){
        Map<String,String> stateCodeMap = new Map<String,String>();
        Map<String,String> countryCodeMap = new Map<String,String>();
        List<Lead> leadsList = new List<Lead>();
        Map<Id,Lead> oldMap = new Map<Id,Lead>();
        for (StatesCountries__c sc : StatesCountries__c.getall().values()){
          if (sc.Type__c=='S'){
            if (sc.Country__c=='USA'){stateCodeMap.put(sc.Name, sc.Code__c);}
          } else { 
            countryCodeMap.put(sc.Code__c, sc.Name);
          }
        }
        for(String recType : newLeadMap.keySet()){
            if(recType.contains('Club Car') && !newLeadMap.get(recType).isEmpty()){
             leadsList.addAll(newLeadMap.get(recType));   
            }
        }
        if(null!=oldLeadMap){
            for(String recType : oldLeadMap.keySet()){
                if(recType.contains('Club Car') && !oldLeadMap.get(recType).isEmpty()){
                    for(Lead ldRec : oldLeadMap.get(recType)){
                        oldMap.put(ldRec.Id,ldRec); 
                     } 
                }
            }
        }
        
        for(Lead l : leadsList){
            Lead oldlead = null;
            if(null!=oldMap) { oldlead = oldMap.get(l.Id);}
            if(l.Country != NULL && (trigger.isInsert || (trigger.isUpdate && l.Country != oldlead.Country))){
                if(l.Country.equalsIgnoreCase('United States')){l.Country = 'USA';}
                else if(countryCodeMap.containsKey(l.Country.toUpperCase())){l.Country = countryCodeMap.get(l.Country.toUpperCase());} 
            }
        } 
    } 
}