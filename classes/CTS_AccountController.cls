public with sharing class CTS_AccountController {
  
  @AuraEnabled
  public static List<Account> fetchAccts(string Bill_Acc){
    List<Account> Accnts = new List<Account>();
    Id ctsBillToRTId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
    Set<String> validPricelists = new Set<String>{'ITS_PRIMARY','ITS_CAN_PRIMARY'};
    Set<String> billToNumbers = new Set<String>(); 
    for (Customer_Relationship__c cr : [SELECT Name FROM Customer_Relationship__c WHERE Customer_Relationship__c.Shipping_Customer__c = :Bill_Acc]){
        billToNumbers.add(cr.Name);
    }
    
    if(Bill_Acc != null){
      Accnts =[SELECT Id,Name, Type, Oracle_Number__c ,ShippingStreet,ShippingCity,ShippingState, ShippingPostalCode 
                 FROM account 
                WHERE Type='Bill To' and 
                      Status__c='Active' and 
                      RecordTypeId = :ctsBillToRTId and 
                      PriceList__c IN :validPricelists and 
                      Oracle_Number__c = :billToNumbers 
                ];
    }
    return Accnts;
  }
    
  @AuraEnabled
  public static void setBillAccount(Id OpId, Id AccId){
    List<Opportunity> oops = new List<Opportunity>();
    oops=[select Id from opportunity where Id=:OpId];
    for(Opportunity o : oops){
      o.CTS_BillToAccount__c=AccId;
      o.CTS_OVERRIDE_BILL_TO__c=true;
    }
    update oops;
  }
}