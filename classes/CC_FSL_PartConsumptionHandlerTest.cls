@isTest
public class CC_FSL_PartConsumptionHandlerTest {
    @TestSetup
    private static void createData(){
        //create service terr
        Id uId = [SELECT Id FROM User Where isActive=TRUE and X18_Digit_User_ID__c!=null and Profile.Name LIKE 'CC_Sys%' LIMIT 1].Id;
        OperatingHours oh  = new OperatingHours(Name='test');
        insert oh;
        ServiceTerritory st = new ServiceTerritory(isActive=TRUE,Name='TestSvcTerr',Product_Transfer_Approver__c=uId,operatingHoursID=oh.Id);
        insert st;  
        //create warehouse 
        Schema.Location whse = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=FALSE,LocationType='Warehouse',Name='testWhse',
                                                   TimeZone='America/New_York',Service_Territory__c=st.Id);
        insert whse;
        //create location 1
        Schema.Location loc = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc1',parentlocationId=whse.Id,TimeZone='America/New_York');
        insert loc;
        //create location 2
        Schema.Location loc2 = new Schema.Location(IsInventoryLocation=TRUE,IsMobile=TRUE,LocationType='Van',Name='testLoc2',parentlocationId=whse.Id,TimeZone='America/New_York');
        insert loc2;
        //create product
        Product2 p = new Product2(name = 'testFSLProduct',Family = 'Club Car Parts');
        insert p;
        Pricebook2 pb = TestUtilityClass.createPriceBook2();
        insert pb;
        PricebookEntry pbe = new PricebookEntry(Product2Id = p.Id,Pricebook2Id = pb.Id,UnitPrice = 50,isActive = true,UseStandardPrice=FALSE);
        insert pbe;
        //create account
        Account a = TestUtilityClass.createAccount();
        a.ShippingCountry='USA';
        a.CC_Customer_Number__c ='9999920';
        a.Name='CLUB CAR DEPT EXP US';
        insert a;
        CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
        hcs.FS_No_Charge_Acct_Id__c=a.Id;
        hcs.FS_No_Charge_Pricebook_ID__c=pb.Id;
        upsert hcs CCProcessBuilderIDs__c.Id;
        //create work order
        WorkOrder wo = TestUtilityClass.createWorkOrder(a.id,'CC_Service');
        wo.Work_Order_status__c='Work Completed';
        wo.SLAM_Report__c='Completed';
        wo.Subject='Test';
        insert wo;
        wo.Pricebook2Id=pb.id;
        update wo;
        //create productitem
        ProductItem pi = new ProductItem(LocationId=loc.Id, Product2id=p.Id, QuantityOnHand=5);
        insert pi;
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId=wo.id, Quantity=2,UnitPrice=1,Part__c = pi.Id,PricebookEntryId=pbe.Id);
        insert woli;
        /* 
            for some reason creating a productconsumed error throws a cannot change pricebook error
        
            //create product consumed
            ProductConsumed pc = new ProductConsumed();
            pc.WorkOrderId = wo.Id;
            pc.QuantityConsumed=1;
            pc.ProductItemId = pi.Id;
            pc.WorkOrderLineItemId = woli.Id;
            insert pc;
        */
    }
    static testmethod void test1(){
        test.startTest();
        Id [] idLst = new List<Id>();
        for(WorkOrder wo : [SELECT Id FROM WorkOrder]){idLst.add(wo.Id);}
        String [] msgReturnLst = CC_FSL_PartConsumptionHandler.consumeParts(idLst);
        WorkOrderLineItem woli = [SELECT Id FROM WorkOrderLineItem];
        woli.Quantity =4;
        update woli;
        String [] msgReturnLst2 = CC_FSL_PartConsumptionHandler.consumeParts(idLst);
        woli.Quantity =6;
        update woli;
        String [] msgReturnLst3 = CC_FSL_PartConsumptionHandler.consumeParts(idLst);
        test.stopTest();
    }
}