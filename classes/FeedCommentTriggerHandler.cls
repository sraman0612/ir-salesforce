/*
@Author : Snehal Chabukswar
@BuiltDate : 12 March 2021
@Description : called from CTS_FeedCommentTrigger
@Company : cognizant
*/
public class FeedCommentTriggerHandler implements ITriggerHandler {
    
    ///Use this variable to disable this trigger from transaction
    public static Boolean TriggerDisabled = false;
    
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return TriggerDisabled;
    }
    
    public void beforeInsert(List<sObject> newList) {}
    
    public void afterInsert( Map<Id,sObject> newMap) {
        system.debug('-----Start of After Insert--------');
       system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@'+newMap.values());  
        updateCase(newMap.values());
        system.debug('-----End of After Insert--------');
    }
    
    public void beforeUpdate( Map<Id, sObject> newMap, Map<Id, sObject> oldMap) {}
    
    public void afterUpdate(Map<Id, sObject> newMap,Map<Id, sObject> oldMap) {
        
    }
    
    public void beforeDelete(Map<Id, sObject> oldMap) {}
    
    public void afterDelete( Map<Id, sObject> oldMap) {
        
    }
    
    public void afterUnDelete( Map<Id, sObject> newMap) {
        
    }
    
    
    /*
@Author : Snehal Chabukswar
@Description : called to update Case unread Flag Checkbox
@params : List<SObject> newList
*/
    public void updateCase(List<SObject> newList){    
        Set<Id>caseIds = new Set<Id>();
        List<Case>caseToBeUpdated = new List<Case>();
        Map<Id,FeedComment>caseIdFCMap = new Map<Id,FeedComment>();
        for (SObject obj : newList) {
            FeedComment newFeed = (FeedComment)obj;
            if (newFeed.ParentId.getSObjectType() == Case.SObjectType ) {
                caseIds.add(newFeed.ParentId);
                caseIdFCMap.put(newFeed.ParentId,newFeed);
            }
        }
        For(Case caseObj: [SELECT Id,CreatedById,RecordType.developerName,Email_Waiting_Icon__c,OwnerId 
                           FROM Case    
                           WHERE ID IN:caseIds AND 
                           RecordType.developerName = 'CTS_LATAM_Brazil_Application_Engineering']){
                               if(caseIdFCMap.get(caseObj.Id).CreatedById !=caseObj.OwnerId){
                                   caseObj.Email_Waiting_Icon__c=true;
                                   caseToBeUpdated.add(caseObj);
                               }
                           }
        if(!caseToBeUpdated.isEmpty()){
            update caseToBeUpdated;
        }
        
    }
}