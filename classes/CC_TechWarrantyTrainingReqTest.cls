@isTest
public class CC_TechWarrantyTrainingReqTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
     }
    @isTest  
    static  void UnitTest_TechWarrantyTrainingReq()
    {
        
        CC_TrainingRequest tr = new CC_TrainingRequest();
        tr.CC_Access_Code='';
        tr.CC_Attendee_Email='';
        tr.CC_Attendee_Name='';
        tr.CC_Attendee_Telephone='';
        tr.Attendee_Type='';
        tr.CC_Date_Of_Course=system.today();
        tr.CC_Time_Of_Course='';
        tr.CC_Company_Address='';
        tr.CC_Company_Email='';
        tr.CC_Company_Name='';
        tr.CC_Company_Telephone='';
        tr.CC_Courses='';
        tr.CC_Mode_of_Payment='';
        tr.CC_Clubcar_Customer='';
        tr.CC_Sales_Rep_Name='';
        tr.ContactId='';
        tr.AccountId='';
        tr.RecordTypeId='';
        tr.OwnerId='';
        tr.SalesRepId='';
        tr.CaseSubreason='';
        User uu =TestUtilityClass.createPortalUserCC();
        insert uu;
        System.runAs(uu) {
            
            Test.startTest();     
            CC_PavilionTemplateController controller;            
            CC_TechWarrantyTrainingReq TechWarrTrainingReq = new CC_TechWarrantyTrainingReq(controller);   
            SelectOption [] lst1 = TechWarrTrainingReq.getAttendeeTypes();
            SelectOption [] lst2 = TechWarrTrainingReq.getCourses();
            SelectOption [] lst3 = TechWarrTrainingReq.getPaymentModes();
            TechWarrTrainingReq.createTrainingRegistration();
            Test.stopTest();
        }

    }
    
}