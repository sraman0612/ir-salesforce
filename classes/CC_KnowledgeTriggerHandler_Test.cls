@isTest
public class CC_KnowledgeTriggerHandler_Test {

    static testMethod void TestBuildSearchText() {
        
        Knowledge__kav k = new Knowledge__kav(UrlName='blah',CTS_TechDirect_Article_Type__c='Reference',Title='blah',CC_Accessories__c = 'Visage',RecordTypeId = CC_KnowledgeTriggerHandler.getCCRecordTypeId());
        insert k;
        CC_KnowledgeTriggerHandler.BuildSearchText(new Knowledge__kav[] {k});
        system.assert(k.CC_Search_Terms__c.contains(k.CC_Accessories__c));
        k.Title = 'blahblahblahblah';
        update k;
        Knowledge__kav [] kLst = new List<Knowledge__kav>();
        kLst.add(k);
        CC_KnowledgeTriggerHandler.SetCCDataCategory(kLst);
        Test.startTest();
        CC_DataCategoryBatchUpdate b = new CC_DataCategoryBatchUpdate();
        Database.executebatch(b,1);
        CC_DataCategoryBatchUpdate sb = new CC_DataCategoryBatchUpdate();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test check', sch, sb); 
        Test.stopTest();  
    }

}