/*****************************************************************************
Name:       RS_CRS_OpenGuide_ControllerTest
Purpose:    Test class for RS_CRS_OpenGuide_Controller. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Ashish Takke    01/18/2017  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_OpenGuide_ControllerTest {
    
    /******************************************************************************
	Method Name : fetchConcessionGuideId_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing fetchConcessionGuideId Method
	******************************************************************************/
    Static testmethod void fetchConcessionGuideId_Test(){
        //Getting Case Record Type Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        //insert test case
        Case caseObj = new Case();
        caseObj.RecordTypeId = caseRecId;
        caseObj.Status = 'Customer Action';
        caseObj.RS_CRS_Send_Attachment_Email__c = false;
        insert caseObj;
        
        //insert test content document
        ContentVersion cv = new ContentVersion();
        cv.title = Label.RS_CRS_Concession_Guideline;      
        cv.PathOnClient ='test';           
        cv.VersionData =Blob.valueOf('Unit Test Attachment Body');          
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink documentObject = new ContentDocumentLink();    
        documentObject.LinkedEntityId = caseObj.id;
        documentObject.ContentDocumentId = testContent.ContentDocumentId;
        documentObject.Visibility = 'AllUsers';
        documentObject.shareType = 'v';
        insert documentObject;
        
        Test.startTest();
        Id guideId = RS_CRS_OpenConcessionGuide_Controller.fetchConcessionGuideId();
        Test.stopTest();
    }
}