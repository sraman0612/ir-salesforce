@isTest
public class CC_ServiceReportEventHandlerTest {

  public static testmethod void test1(){
    List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
    psettingList = TestUtilityClass.createPavilionSettings();        
    insert psettingList;
    IR_API_Bypass__c iab = IR_API_Bypass__c.getInstance(userinfo.getUserId());
    iab.CC_FS_Edit_Synced_WorkOrder__c = TRUE;
    insert iab;
    Product2 p = TestUtilityClass.createInboundProduct2();
    insert p;
    Product2 pNonInv = TestUtilityClass.createInboundProduct2();
    pNonInv.ERP_Item_Number__c='ADAM12';
    pNonInv.ProductCode='ADAM12';
    pNonInv.CC_Non_Inventory__c=TRUE;
    insert pNonInv;
    Pricebook2 pb = TestUtilityClass.createPriceBook2();
    insert pb;
    PricebookEntry pbeCustom = TestUtilityClass.createPriceBookEntry(pb.Id,p.Id);
    insert pbeCustom;
    Account a = TestUtilityClass.createAccount();
    a.ShippingCountry='USA';
    a.CC_Customer_Number__c ='9999920';
    a.Name='CLUB CAR DEPT EXP US';
    insert a;
    CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
    hcs.FS_No_Charge_Acct_Id__c=a.Id;
    hcs.FS_No_Charge_Pricebook_ID__c=pb.Id;
    upsert hcs CCProcessBuilderIDs__c.Id;
    //create work order
    WorkOrder wo = TestUtilityClass.createWorkOrder(a.id,'CC_Service');
    wo.Work_Order_status__c='Work Completed';
    wo.SLAM_Report__c='Completed';
    wo.Subject='Test';
    insert wo;
    //insert sa
    ServiceAppointment sa = new ServiceAppointment(ParentRecordId=wo.Id, Work_Order__c=wo.Id);
    insert sa;
    ServiceAppointment satest = [SELECT ParentRecordId ,ParentRecordType, Work_Order__c, Work_Order__r.isServiceReportSigned__c
                                     FROM ServiceAppointment WHERE ParentRecordId=:wo.Id];
    system.debug('### SVC APPT IS ' + satest);
    ContentVersion contentVersion = new ContentVersion(
      Title = 'testcv',
      PathOnClient = 'testcv.png',
      VersionData = Blob.valueOf('Test Content'),
      IsMajorVersion = true
    );
    insert contentVersion;
    List<ContentDocument> d = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];
    ServiceReport sr = new ServiceReport();
    //sr.ContentVersionDocumentId = d[0].Id ;
    sr.DocumentBody = Blob.valueOf('Test Content');
    sr.DocumentContentType ='application/pdf';
    sr.DocumentName='Test';
    sr.ParentId = wo.Id ; 
    sr.isSigned=TRUE;
    insert sr ;
  }
}