public with sharing class CC_STL_Car_Info_Edit_Controller {
 
 	private class CC_STL_Car_Info_Edit_ControllerException extends Exception{}
 
 	@TestVisible
 	private static Boolean forceError = false;
 
 	@TestVisible
 	private class InitResponse extends LightningResponseBase{
         @AuraEnabled public Product2[] items = new Product2[]{};
         @AuraEnabled public CC_Short_Term_Lease__c stl = new CC_Short_Term_Lease__c();
 	}

 	@AuraEnabled(Cacheable = true)
	public static InitResponse getInit(Id carInfoId){
		
		InitResponse response = new InitResponse();
		
		try{		
			
			if (Test.isRunningTest() && forceError){
				throw new CC_STL_Car_Info_Edit_ControllerException('Test error');
			}
			
            response.items = [Select Id, Name, ProductCode From Product2 Where IsActive = true and CC_Item_Class__c = 'LCAR' and ProductCode != null Order By ProductCode ASC];
            
            CC_STL_Car_Info__c carInfo = [Select Id, Short_Term_Lease__c From CC_STL_Car_Info__c Where Id = :carInfoId];
            response.stl = [Select Id, Name, Lease_Extension__c From CC_Short_Term_Lease__c Where Id = :carInfo.Short_Term_Lease__c];
		}
		catch(Exception e){
			system.debug('exception: ' + e.getMessage());
			response.success = false;
			response.errorMessage = e.getMessage();
		}
		
		return response;
	}   
}