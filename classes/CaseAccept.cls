public with sharing class CaseAccept {    
    
    @AuraEnabled
    public static Case getCase(Id caseId){
        //make your SOQL here from your desired object where you want to place your lightning action
        return [SELECT Id, ContactEmail,ContactId,OwnerId, Status,RecordType.DeveloperName,Org_Wide_Email_Address_ID__c,Email_Waiting_Icon__c, GDI_Department__c,
                Product_Category__c,Brand__c,Assigned_From_Address_Picklist__c,contact.Language__c,SuppliedEmail,Case_Accepted__c FROM Case WHERE Id = :caseId];
    }

    @AuraEnabled
    public static Case updateCase(Case currCase){
        //you can make your update here.
        User currUser = [SELECT Id, Department__c,Default_Product_Category__c,Brand__c,Assigned_From_Email_Address__c FROM User WHERE Id = :UserInfo.getUserId()];
        
		OrgWideEmailAddress orgWide = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE Address=:currUser.Assigned_From_Email_Address__c];
        //if (currCase != null) {
        String oldUserId = currCase.OwnerId;  
        currCase.OwnerId = currUser.Id;
        currCase.Status = 'Open';
        currCase.Email_Waiting_Icon__c = false;
        if(currCase.Assigned_From_Address_Picklist__c == null && currUser.Department__c != null && currUser.Default_Product_Category__c != null && currUser.Brand__c != null && currUser.Assigned_From_Email_Address__c != null){
            currCase.GDI_Department__c = currUser.Department__c;
            currCase.Product_Category__c = currUser.Default_Product_Category__c;
            currCase.Brand__c = currUser.Brand__c;
            currCase.Assigned_From_Address_Picklist__c = currUser.Assigned_From_Email_Address__c;
            if(currCase.Org_Wide_Email_Address_ID__c == null){
            currCase.Org_Wide_Email_Address_ID__c = orgWide.Id;    
            }
            
        }
        
              //processRecords(currCase.id);
        upsert currCase;
        Map<String, Object> Params = new Map<String, Object>();
        
        if((currCase.RecordType.DeveloperName == 'CTS_OM_Account_Management' || currCase.RecordType.DeveloperName == 'Customer_Service') &&
           String.valueOf(currCase.OwnerId).startsWith('005') && currCase.Case_Accepted__c == false &&  currCase.SuppliedEmail != null ){
                          	
                
                if(currCase.ContactId != null && currCase.OwnerId != UserInfo.getUserId() && oldUserId != currCase.OwnerId ){
                  	Params.put('varCaseID',currCase.id);
            		Params.put('varLanguage', currCase.contact.Language__C );
            		Params.put('varToAddresses', currCase.SuppliedEmail);
                }else{
                    Params.put('varCaseID',currCase.id);
            		Params.put('varToAddresses', currCase.SuppliedEmail);
                }                
            
            Flow.Interview.Case_Accepted_Case_Email_Reply callFlow = new Flow.Interview.Case_Accepted_Case_Email_Reply(Params);
            callFlow.start();
        
        }
        if(currCase.RecordType.DeveloperName == 'CTS_Non_Warranty_Claims' || currCase.RecordType.DeveloperName == 'ETO_Support_Case'){
        	Params.put('varCaseID',currCase.id);
            Flow.Interview.NWC_Case_Accepted_Flow callFlow = new Flow.Interview.NWC_Case_Accepted_Flow(Params);
            callFlow.start();
        
        }
        return currCase;
    }
    

}