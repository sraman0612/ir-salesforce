public with sharing class CTS_IOT_Data_Service {

    /* Sharing sets should drive account record access so this class should be able to stay as "with sharing" and no need to add a where clause to filter accounts */

    private class CTS_IOT_Data_ServiceException extends Exception{}

    // Private variables
    private static CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();        

    public static Set<Id> getAccountRecordTypeIds(){
        return getRecordTypeIdsFromCustomSettings('Account_Matching_Record_Types__c');
    }

    public static Set<Id> getAssetRecordTypeIds(){
        
        Set<Id> recordTypeIds = getRecordTypeIdsFromCustomSettings('Asset_Matching_Record_Types__c');
        recordTypeIds.addAll(getRecordTypeIdsFromCustomSettings('Asset_Matching_Record_Types2__c'));
        return recordTypeIds;
    }    

    private static Set<Id> getRecordTypeIdsFromCustomSettings(String fieldName){

        Set<Id> recordTypeIds = new Set<Id>();
        
        if (communitySettings != null && String.isNotBlank((String)communitySettings.get(fieldName))){

            List<String> recordTypeList = ((String)communitySettings.get(fieldName)).split(',');

            for (String recordTypeId : recordTypeList){
                recordTypeIds.add(Id.valueOf(recordTypeId.trim()));
            }
        }

        return recordTypeIds;
    }

    public static Integer getCommunityUserSitesCount(String searchTerm){       

        if (String.isBlank(searchTerm)){
            return Database.countQuery('Select Count() From Account');        
        }
        else{
            return Search.query('FIND :searchTerm RETURNING Account')[0].size();
        }
    }     

    public static Account[] getCommunityUserSites(Id siteId){
        return getCommunityUserSites(siteId, null, null, null, false);
    }        

    public static Account[] getCommunityUserSites(Id siteId, String searchTerm, Integer pageSize, Integer offSet, Boolean returnBillToFirst){

        String fields = 'Id, Name, Account_Name_Displayed__c, Siebel_Ship_To_Id__c, ' + 
            'ShippingAddress, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ' + 
            'BillingAddress, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Bill_To_Account__r.Name';
        
        String whereClause = '';

        if (siteId != null){
            whereClause += ' Where Id = \'' + siteId + '\'';
        }

        return (Account[])getRecords('Account', fields, whereClause, 'Order By ' + (returnBillToFirst ? 'Bill_To_Account__c NULLS FIRST, Name ASC' : 'Name ASC'), searchTerm, pageSize, offSet);  
    }

    public static Map<Id, Boolean> getRemoteMonitoringSiteMap(Account[] sites){

        Map<Id, Boolean> remoteMonitoringEnabledSiteMap = new Map<Id, Boolean>();

        for (Asset asset : [Select Id, AccountId, IRIT_RMS_Flag__c From Asset Where AccountId in :sites and IRIT_RMS_Flag__c = true]){
            remoteMonitoringEnabledSiteMap.put(asset.AccountId, true);
        }

        // Add any site to the map that did not have a qualified asset
        for (Account site : sites){

            if (!remoteMonitoringEnabledSiteMap.containsKey(site.Id)){
                remoteMonitoringEnabledSiteMap.put(site.Id, false);
            }
        }

        return remoteMonitoringEnabledSiteMap;
    }
    
    public class UserSiteAccess{

        @AuraEnabled public Account account;
        @AuraEnabled public Boolean hasAccess = false;
        @AuraEnabled public Boolean isUpdateable = false;

        public UserSiteAccess(Account account, Boolean hasAccess, Boolean isUpdateable){
            this.account = account;
            this.hasAccess = hasAccess;
            this.isUpdateable = isUpdateable;
        }
    }

    public static UserSiteAccess[] getContactSiteAccess(Id contactId){

        UserSiteAccess[] userSites = new UserSiteAccess[]{};

        // Super users have access to all accounts in the bill to/ship to hierarchy
        // Standard users have access to accounts via account-contact relation
        CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse ccr = CTS_IOT_Data_Service_WithoutSharing.getContacts(contactId);
        CTS_IOT_Data_Service_WithoutSharing.CommunityContact communityContact = ccr.communityContacts[0];

        userSites.add(new UserSiteAccess(communityContact.Site, true, false));

        if (communityContact.IsSuperUser){

            for (Account a : [Select Id, Name, Account_Name_Displayed__c, Siebel_Ship_To_Id__c, BillingCity, BillingState, Bill_To_Account__c From Account Where Bill_To_Account__c = :communityContact.Site.Bill_To_Account__c]){
                userSites.add(new UserSiteAccess(a, true, true));
            }
        }
        else{

            for (AccountContactRelation acr : [Select Id, Account.Name, Account.Account_Name_Displayed__c, Account.Siebel_Ship_To_Id__c, Account.BillingCity, Account.BillingState, Account.Bill_To_Account__c From AccountContactRelation Where ContactId = :communityContact.ContactId and AccountId != :communityContact.Site.Id]){
                userSites.add(new UserSiteAccess(acr.Account, true, true));
            }
        }    

        // Get the remaining sites in the hierarchy that the user does not have access to
        Set<Id> siteIds = new Set<Id>();

        for (UserSiteAccess siteAccess : userSites){
            siteIds.add(siteAccess.account.Id);
        }

        for (Account a : [Select Id, Name, Account_Name_Displayed__c, BillingCity, BillingState, Bill_To_Account__c From Account Where Id not in :siteIds LIMIT 500]){
            userSites.add(new UserSiteAccess(a, false, true));
        }

        return userSites;
    }

    public class SiteHierarchyResponse{
        @AuraEnabled public Account siteParent;
        @AuraEnabled public Account[] siteChildren;
    }

    public static SiteHierarchyResponse getCommunityUserSiteHierarchy(String searchTerm, Integer pageSize, Integer offSet){

        SiteHierarchyResponse response = new SiteHierarchyResponse();

        Account[] allAccounts = getCommunityUserSites(null, searchTerm, pageSize, offSet, true);      

        Account parentAccount;

        response.siteChildren = new Account[]{};

        for (Account a : allAccounts){

            if (a.Bill_To_Account__c != null){
                response.siteChildren.add(a);
            }
            else{
                parentAccount = a;
            }
        }

        response.siteParent = parentAccount != null ? parentAccount : new Account();

        return response;
    }  

    public class AssetDetail{
        @AuraEnabled public Asset asset = new Asset();
        @AuraEnabled public Boolean helixUpgradeable = false;
        @AuraEnabled public String assetType = '';
    }

    public static Integer getCommunityUserAssetsCount(String siteId){                
        return getCommunityUserAssetsCount(siteId, null, null);        
    }    

    public static Integer getCommunityUserAssetsCount(String siteId, String searchTerm, String whereClauseFilters){                

        String whereClause = '';
        
        if (siteId != null){
            whereClause += ' Where AccountId = \'' + siteId + '\'';
        }

        if (String.isNotBlank(whereClauseFilters)){
            whereClause += whereClause.contains('Where') ? ' and ' + whereClauseFilters : ' Where ' + whereClauseFilters;
        }            

        return getRecordCount('Asset', whereClause, searchTerm);        
    } 

    public static AssetDetail[] getCommunityUserAssets(String searchTerm, Id siteId, Integer pageSize, Integer offSet){
        return getCommunityUserAssets(searchTerm, siteId, null, pageSize, offSet);
    }  

    public static AssetDetail[] getCommunityUserAssets(Id siteId){
        return getCommunityUserAssets(siteId, null, null, null);
    } 

    public static AssetDetail getCommunityUserAsset(Id assetId){
        return getCommunityUserAssets(null, null, assetId, null, null)[0];
    }     

    public static AssetDetail[] getCommunityUserAssets(String searchTerm, Id siteId, Id assetId, Integer pageSize, Integer offSet){
        return getCommunityUserAssets(searchTerm, siteId, assetId, pageSize, offSet, null);
    }

    public static AssetDetail[] getCommunityUserAssets(String searchTerm, Id siteId, Id assetId, Integer pageSize, Integer offSet, String whereClauseFilters){

        String fields = 'Id, Name, Asset_Name_Displayed__c, AccountId, Account.Name, Account.Account_Name_Displayed__c, CTS_IOT_Operating_Status__c, SerialNumber, Model_Name__c, Category__c, ' + 
                        'InstallDate, Ship_Date__c, Helix_Connectivity_Status__c, Manufacturer__c, HP__c, IRIT_Frame_Type__c, IRIT_RMS_Flag__c, Controller_Type_RMS_Compatible__c';
        
        String whereClause = '';

        if (siteId != null){
            whereClause += ' Where AccountId = \'' + siteId + '\'';
        }
        else if (assetId != null){
            whereClause += ' Where Id = \'' + assetId + '\'';
        }

        if (String.isNotBlank(whereClauseFilters)){
            whereClause += whereClause.contains('Where') ? ' and ' + whereClauseFilters : ' Where ' + whereClauseFilters;
        }

        Asset[] assets = getRecords('Asset', fields, whereClause, 'Order By Name ASC', searchTerm, pageSize, offSet);      

        AssetDetail[] assetDetails = new AssetDetail[]{};

        for (Asset asset : assets){

            AssetDetail assetDetail = new AssetDetail();
            assetDetail.asset = asset;

            if (asset.Controller_Type_RMS_Compatible__c){
                assetDetail.helixUpgradeable = true;
            }

            //CTS_IOT_Frame_Type__c frameType = String.isNotBlank(asset.IRIT_Frame_Type__c) ? frameTypeMap.get(asset.IRIT_Frame_Type__c) : null;

            //if (frameType != null){

                //assetDetail.assetType = frameType.CTS_IOT_Type__c;

                // if (!asset.IRIT_RMS_Flag__c && frameType.CTS_IOT_RMS_Compatible__c && (frameType.CTS_IOT_RMS_Compatible_From_Date__c == null || (asset.Ship_Date__c != null && asset.Ship_Date__c >= frameType.CTS_IOT_RMS_Compatible_From_Date__c))){
                //     assetDetail.helixUpgradeable = true;
                // }
            //}

            assetDetails.add(assetDetail);
        }

        return assetDetails;
    }   
    
    public static sObject[] getRecords(String objectName, String fieldNames, String whereClause, String orderClause, String searchTerm, Integer pageSize, Integer offSet){

        String query = String.isBlank(searchTerm) ?
            'Select ' + fieldNames + ' From ' + objectName : 
            'FIND :searchTerm RETURNING ' + objectName + '(' + fieldNames;

        if (String.isNotBlank(whereClause)){
            query += ' ' + whereClause;
        }

        if (String.isNotBlank(orderClause)){
            query += ' ' + orderClause;
        }

        if (pageSize != null){
            query += ' LIMIT ' + pageSize;
        }

        if (offSet != null){
            query += ' OFFSET ' + offSet;
        }         

        if (String.isNotBlank(searchTerm)){
            query += ')';
        }
        
        system.debug('final query: ' + query);
        
        sObject[] records;
        
        if (String.isNotBlank(searchTerm)){
            records = Search.query(query)[0];
        }
        else records = Database.query(query);        

        return records;
    }    

    public static Integer getRecordCount(String objectName, String whereClause, String searchTerm){                

        String query = 'Select Count() From ' + objectName;
        
        query = String.isNotBlank(searchTerm) ? 'FIND :searchTerm RETURNING ' + objectName + '(Id' : query;        

        if (String.isNotBlank(whereClause)){
            query += ' ' + whereClause;
        }     

        if (String.isNotBlank(searchTerm)){
            query += ')';
        }        

        system.debug('final count query: ' + query);

        if (String.isNotBlank(searchTerm)){
            return Search.query(query)[0].size();
        }

        return Database.countQuery(query);        
    }    
}