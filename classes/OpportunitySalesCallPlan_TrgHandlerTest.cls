@isTest
public class OpportunitySalesCallPlan_TrgHandlerTest {
    
    @isTest static void salesCallPlanNotesLastMeetingUpdate(){
       	Test.startTest();
        Id accRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
        TestDataUtility objTestDataUtility = new TestDataUtility();
        
        Account acc = objTestDataUtility.createAccount('GD Parent');
        acc.ShippingCountry = 'USA';
        acc.ShippingCity = 'Augusta';
        acc.ShippingState = 'GA';
        acc.ShippingStreet = '2044 Forward Augusta Dr';
        acc.ShippingPostalCode = '566';
        
        insert acc;
        Opportunity opp1 = new Opportunity();
        opp1.name = 'Test Opty1';
        opp1.stagename = 'Stage 1. Qualify';
        opp1.amount = 8000;
        opp1.closedate = system.today();
        opp1.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        try{
            insert opp1;
        }Catch(DMLException e){
            System.assert( e.getMessage().contains('Insert failed. First exception on ' +
                                                   'row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION'), 
                          e.getMessage() );
        }
        
        List<Opportunity_Sales_Call_Plan__c> scpList = new List<Opportunity_Sales_Call_Plan__c> ();
        
        // Create a test Opportunity_Sales_Call_Plan__c record
        Opportunity_Sales_Call_Plan__c salesCallPlan = new Opportunity_Sales_Call_Plan__c(Name = 'Test Plan',	Account__c=acc.Id,Opportunity__c=opp1.Id);
        Opportunity_Sales_Call_Plan__c salesCallPlan1 = new Opportunity_Sales_Call_Plan__c(Name = 'Test Plan1',	Account__c=acc.Id,Opportunity__c=opp1.Id);
        
        scpList.add(salesCallPlan);
        scpList.add(salesCallPlan1);
        
        insert scpList;
        
        // Create a test ContentVersion record
        ContentVersion cv = new ContentVersion(
            Title = 'Test Content',
            VersionData = Blob.valueOf('Test content body'),
            PathOnClient = 'TestPath.SNOTE',
            CreatedDate = Date.today(),
            IsMajorVersion = true
        );
        
        insert cv;
        
        cv=  [SELECT Id, createdDate, ContentDocumentId, VersionData FROM ContentVersion WHERE Id = :cv.Id];
        
        cv.title = 'Untitled Note';
        update cv; 
        
        ContentDocumentLink cdl = new ContentDocumentLink(
            ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId,
            LinkedEntityId = salesCallPlan.Id,
            ShareType = 'V'
        );
        insert cdl;
     
        Opportunity_Sales_Call_Plan__c salesCallPlan2 = new Opportunity_Sales_Call_Plan__c(Name = 'Test Plan 2', Account__c=acc.Id, Opportunity__c=opp1.Id);
        insert salesCallPlan2;
        
        
        Test.stopTest();
        
    }
}