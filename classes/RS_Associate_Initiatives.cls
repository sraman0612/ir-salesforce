public with sharing class RS_Associate_Initiatives {
    public String AccountId = ApexPages.currentPage().getParameters().get('id');
    List<id> relatedInitiativeIds = new List<id>();

    public List<keyInitiative> initList {get; set;}

    // Url to redirect after doing the associations
    public String REDIRECT_URL { get { return '/apex/RS_Intentional_Sales_Call?id=' + (AccountId != null ? AccountId : '');}}

    public List<keyInitiative> getInitiatives() {
        if(initList == null) {
            // This list will have all the Content records that the user has access to
            //List<ContentVersion> contents = getContentItems();
            initList = new List<keyInitiative>();

            for(Key_Initiatives__c ki: [SELECT Id, Name, CreatedDate, (SELECT Id, Title FROM Document_Library__r ORDER BY CreatedDate DESC) FROM Key_Initiatives__c
                                          WHERE Id NOT IN (SELECT Key_Initiative__c FROM Account_Initiatives_Junction__c WHERE Account__c = :AccountId)
                                                AND Start_Date__c < TODAY AND End_Date__c > TODAY ORDER BY CreatedDate DESC]) {
                // Only show the Key Initiative if the User can see one of its related contents
                if (ki.Document_Library__r.size() > 0){
                  keyInitiative extended = new keyInitiative(ki);
                  extended.contentTitle = ki.Document_Library__r.get(0).Title;
                  initList.add(extended);
                }
                relatedInitiativeIds.add(ki.Id);
            }
        }
        return initList;
    }

    public Integer getKeyInitiativesCount() {
        return initList.size();
    }

    public PageReference processSelected() {
        List<Key_Initiatives__c> selectedInits = new List<Key_Initiatives__c>();

        for(keyInitiative ki: getInitiatives()) {
            if(ki.selected == true) {
                selectedInits.add(ki.init);
            }
        }

        System.debug('These are the selected Initiatives...');
        // Use a list to insert all the Key Initiatives in one dml statement
        List<Account_Initiatives_Junction__c> junctions = new List<Account_Initiatives_Junction__c>();
        for(Key_Initiatives__c init: selectedInits) {
            system.debug(init);
            junctions.add(new Account_Initiatives_Junction__c(Status__c = 'Open', Key_Initiative__c = init.Id, Account__c = AccountId));
        }

        String msg = 'You selected ' + selectedInits.size() + ' Key Initiatives.';
        try {
            insert junctions;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, msg));
            //redirectPage = new PageReference(pageUrl);
        } catch (Exception e) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }

        initList = null;
        return null;
    }

    public class keyInitiative {
        public Key_Initiatives__c init {get; set;}
        public Boolean selected {get; set;}
        public String contentTitle {get; set;}

        public keyInitiative(Key_Initiatives__c i) {
            init = i;
            selected = false;
            contentTitle = '';
        }
    }

    public String getAccountName() {
        String AccountName = AccountId != null ? [SELECT Name FROM Account WHERE Id = :AccountId LIMIT 1].Name : '';
        return AccountName;
    }
}