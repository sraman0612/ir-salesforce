@isTest
public with sharing class CC_Manage_Approvals_ControllerTest {
    
    @TestSetup
    private static void createData(){

    	TestDataUtility dataUtility = new TestDataUtility();
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;    	

        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();
        batchControls.Mgr_Approval_Override_Supported_Objects__c = 'CC_Order__c,CC_Short_Term_Lease__c';
        upsert batchControls;
        
        CC_Short_Term_Lease_Settings__c leaseSettings = CC_Short_Term_Lease_Settings__c.getOrgDefaults();
        leaseSettings.Restricted_Finance_Approval_Roles__c = 'CC_Finance_Leadership,CC_Finance_Team';
        insert leaseSettings;
    	
    	// Create users to use in the sales hiearchy
    	User adminUser = TestUtilityClass.createUser('System Administrator', null);
    	adminUser.Alias = 'xyz123';

    	User salesRepUser = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepUser.Alias = 'alias122';
    	
    	User salesRepMgr = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepMgr.Alias = 'alias123';
    	salesRepMgr.PS_Purchasing_LOA__c = 50000;
    	
    	User salesRepDir = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepDir.Alias = 'alias124';
    	
    	User salesRepVP = TestUtilityClass.createUser('CC_Sales Rep', null);
    	salesRepVP.Alias = 'alias125';    
    	    	
    	User casUser = TestUtilityClass.createUser('CC_General Admin', null); 	
    	casUser.LastName = 'CASTestUser';
    	
    	insert new User[]{salesRepUser, salesRepMgr, salesRepDir, salesRepVP, casUser, adminUser};
    	
    	salesRepUser.ManagerId = salesRepMgr.Id;
    	salesRepMgr.ManagerId = salesRepDir.Id;
        salesRepMgr.DelegatedApproverId = salesRepDir.Id;
    	salesRepDir.ManagerId = salesRepVP.Id;
    	
    	update new User[]{salesRepUser, salesRepMgr, salesRepDir};    	
    	
    	User financeUser;
    	
    	// Avoid mixed DML since role will be assigned to the finance user
    	system.runAs(adminUser){	
    	
    		UserRole leaseRole = [Select Id From UserRole Where DeveloperName = 'CC_Aftermarket_Team' LIMIT 1];
    	
	    	User leaseUser = TestUtilityClass.createUser('CC_Pavilion Admin', null);
	    	leaseUser.FirstName = 'Lease1';
	    	leaseUser.LastName = 'User';
	    	leaseUser.Alias = 'xyz124';  
	    	leaseUser.UserRoleId = leaseRole.Id;
    	
	        financeUser = TestUtilityClass.createUser('CC_General Admin', null);
	        financeUser.LastName = 'FinanceTestUser';    	
	        financeUser.UserRoleId = [Select Id From UserRole Where DeveloperName = 'CC_Finance_Team' LIMIT 1].Id;
            financeUser.Username = financeUser.Username + 'FINUSR';

	    	insert new User[]{financeUser, leaseUser};
	    	
    		// Assign the lease permission set to the lease user
			PermissionSet leasePermSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Leases'];	    	
	    	
			PermissionSetAssignment permSetAssign = new PermissionSetAssignment(
				AssigneeId = leaseUser.Id,
				PermissionSetId = leasePermSet.Id
			);    	
			
			insert permSetAssign;	    	
    	}
    	
    	// Create a sales rep that has a manager, director, and VP in the hierarchy
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', salesRepUser.Id);
    	insert salesRep;

        Set<String> objectsConfigured = new Set<String>();

        for (CC_Approval_Column__mdt approvalColumn : [Select Object_API_Name__c From CC_Approval_Column__mdt]){
            objectsConfigured.add(approvalColumn.Object_API_Name__c);
        }

        if (objectsConfigured.contains('CC_Short_Term_Lease__c')){

  	        // Create Customer Accounts 
            Account acct1 = TestUtilityClass.createAccount2();
            acct1.BillingCountry = 'USA';
            acct1.ShippingCountry = 'USA';
            acct1.CC_Customer_Number__c = '0123456789';
            insert acct1;
            
            CC_Order__c order1 = TestUtilityClass.createNatAcctOrder(acct1.Id, acct1.Id);
            insert order1;
            
            // Create Master Lease 
            CC_Master_Lease__c ml1 = TestDataUtility.createMasterLease();
            
            // Assign new Account to Master Lease
            ml1.Customer__c = acct1.Id;
            insert ml1;
            
            CC_Short_Term_Lease__c[] leases = new CC_Short_Term_Lease__c[]{};
            
            for (Integer i = 0; i < 5; i++){
                
                CC_Short_Term_Lease__c lease = TestDataUtility.createShortTermLease();
                lease.Master_Lease__c = ml1.Id;
        	    lease.Sales_Rep__c = salesRep.Id;
                lease.Sales_Rep_User__c = salesRepUser.Id;                
                leases.add(lease);
            } 	
            
            insert leases;   

            // Submit the leases for approval
            Approval.ProcessSubmitRequest[] approvalRequests = new Approval.ProcessSubmitRequest[]{};

            for (CC_Short_Term_Lease__c lease : leases){

                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setSkipEntryCriteria(true);
                approvalRequest.setObjectId(lease.Id);
                approvalRequest.setProcessDefinitionNameOrId('Revenue_Lease_Approval');
                approvalRequests.add(approvalRequest);                           
            }

            Approval.ProcessResult[] approvalResults = sObjectService.processApprovalRequestsAndLogErrors(approvalRequests, 'CC_Manage_Approvals_ControllerTest', 'createData');
            system.debug('approvalResults: ' + approvalResults);
        }

        system.assertEquals(0, [Select Count() From Apex_Log__c]);
    }

    @isTest
    private static void testGetApprovalsSuccess(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepMgr = [Select Id From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];

            Test.startTest();
            
            system.runAs(salesRepMgr){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = false;
                request.viewModes = new String[]{'mine'};
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(totalApprovalsExpected, response.approvalCount);
                system.assertEquals(1, response.approvalsByObj.size());
                system.assertEquals(request.showLimit, response.approvalsByObj[0].approvals.size());
            }

            Test.stopTest();
        }
    }

    @isTest
    private static void testGetApprovalsManagerOverrideSuccess(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepMgr = [Select Id From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];

            // Assign the manager override perm set
            PermissionSet permSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Lease_Manager_Approval_Override'];
            insert new PermissionSetAssignment(AssigneeId = salesRepMgr.Id, PermissionSetId = permSet.Id);

            Test.startTest();
            
            system.runAs(salesRepMgr){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = false;
                request.viewModes = new String[]{'mine','myteam'};
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(totalApprovalsExpected, response.approvalCount);
                system.assertEquals(1, response.approvalsByObj.size());
                system.assertEquals(request.showLimit, response.approvalsByObj[0].approvals.size());

                // Verify the last column includes the override action
                LightningDataTableService.Column actionsColumn = response.approvalsByObj[0].columns[response.approvalsByObj[0].columns.size() - 1];
                system.assertEquals('Actions', actionsColumn.label);
                system.assertEquals(null, actionsColumn.fieldName);
                system.assertEquals('action', actionsColumn.type);
                system.assertEquals(false, actionsColumn.sortable);
                system.assertEquals(1, ((LightningDataTableService.ColumnTypeAttributesAction)actionsColumn.typeAttributes).rowActions.size());
            }

            Test.stopTest();
        }
    }

    @isTest
    private static void testGetApprovalsFailure(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepMgr = [Select Id From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];

            Test.startTest();
            
            system.runAs(salesRepMgr){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = false;
                request.viewModes = null;
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(false, response.success);
                system.assertNotEquals('', response.errorMessage);
                system.assertEquals(0, response.approvalsByObj.size());
            }

            Test.stopTest();
        }
    }    

    @isTest
    private static void testGetApprovalsShowAllSuccess(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepMgr = [Select Id From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];

            Test.startTest();
            
            system.runAs(salesRepMgr){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = true;
                request.viewModes = new String[]{'mine'};
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(totalApprovalsExpected, response.approvalCount);
                system.assertEquals(1, response.approvalsByObj.size());
                system.assertEquals(totalApprovalsExpected, response.approvalsByObj[0].approvals.size());
            }

            Test.stopTest();
        }
    }    

    @isTest
    private static void testGetApprovalsShowMyTeamSuccess(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepDir = [Select Id From User Where Alias = 'alias124' Order By CreatedDate DESC LIMIT 1];

            Test.startTest();
            
            system.runAs(salesRepDir){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = false;
                request.viewModes = new String[]{'mine','mydelegates'};
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(totalApprovalsExpected, response.approvalCount);
                system.assertEquals(1, response.approvalsByObj.size());
                system.assertEquals(request.showLimit, response.approvalsByObj[0].approvals.size());
            }

            Test.stopTest();
        }
    }    

   @isTest
    private static void testGetApprovalsShowMyDelegatesSuccess(){

        CC_Approval_Column__mdt[] approvalColumns = [Select MasterLabel, DeveloperName, Column_Order__c, Object_API_Name__c, Column_Name__c, Sortable__c, Type__c, Type_Attributes__c 
                                                     From CC_Approval_Column__mdt 
                                                     Order By Column_Order__c ASC];

        Integer totalApprovalsExpected = [Select Count() From CC_Short_Term_Lease__c];
        system.assertEquals(totalApprovalsExpected, [Select Count() From ProcessInstanceWorkitem]);

        if (approvalColumns.size() > 0){

            User salesRepDir = [Select Id From User Where Alias = 'alias124' Order By CreatedDate DESC LIMIT 1];

            Test.startTest();
            
            system.runAs(salesRepDir){

                CC_Manage_Approvals_Controller.GetApprovalsRequest request = new CC_Manage_Approvals_Controller.GetApprovalsRequest();
                request.showAll = false;
                request.viewModes = new String[]{'mine','myteam'};
                request.showLimit = 2;

                CC_Manage_Approvals_Controller.GetApprovalsResponse response = CC_Manage_Approvals_Controller.getApprovals(JSON.serialize(request));
                system.assertEquals(true, response.success);
                system.assertEquals('', response.errorMessage);
                system.assertEquals(totalApprovalsExpected, response.approvalCount);
                system.assertEquals(1, response.approvalsByObj.size());
                system.assertEquals(request.showLimit, response.approvalsByObj[0].approvals.size());
            }

            Test.stopTest();
        }
    } 

    @isTest
    private static void testSubmitManagerApprovalOverrideApprove(){

        User salesRepMgr = [Select Id, Name From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];
        ProcessInstanceWorkitem piw = [Select Id, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId, ProcessInstance.ProcessDefinition.Name, OriginalActor.Name, Actor.Name From ProcessInstanceWorkitem LIMIT 1];

        Test.startTest();
        
        system.runAs(salesRepMgr){
            
            LightningResponseBase response = CC_Manage_Approvals_Controller.submitManagerApprovalOverride(piw.ProcessInstance.TargetObjectId, piw.Id, 'This is OK.', true);

            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
        }

        // Verify the record approval override fields were updated properly
        sObject record = Database.query(
             'Select Id, ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName) + 
            ' From ' + String.escapeSingleQuotes(piw.ProcessInstance.ProcessDefinition.TableEnumOrId) + 
            ' Where ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideFieldName) + ' = true'
        );

        system.assertEquals(piw.ProcessInstance.TargetObjectId, record.Id);
        
        // Verify the approval override request attributes that will be processed in the batch job
        CC_Manage_Approvals_Controller.ApprovalOverrideRequest overrideRequest = (CC_Manage_Approvals_Controller.ApprovalOverrideRequest)JSON.deserialize((String)record.get(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName), CC_Manage_Approvals_Controller.ApprovalOverrideRequest.class);
        
        system.assertEquals(salesRepMgr.Id, overrideRequest.overrideUserId);

        Approval.ProcessWorkitemRequest req = overrideRequest.pwr;

        system.assertEquals('Approve', req.getAction());
        system.assertEquals(piw.Id, req.getWorkitemId());
        system.assertEquals('This is OK.', req.getComments());

        Test.stopTest();
    }

    @isTest
    private static void testSubmitManagerApprovalOverrideReject(){

        User salesRepMgr = [Select Id, Name From User Where Alias = 'alias123' Order By CreatedDate DESC LIMIT 1];
        ProcessInstanceWorkitem piw = [Select Id, ProcessInstance.TargetObjectId, ProcessInstance.ProcessDefinition.TableEnumOrId, ProcessInstance.ProcessDefinition.Name, OriginalActor.Name, Actor.Name From ProcessInstanceWorkitem LIMIT 1];

        Test.startTest();
        
        system.runAs(salesRepMgr){
            
            LightningResponseBase response = CC_Manage_Approvals_Controller.submitManagerApprovalOverride(piw.ProcessInstance.TargetObjectId, piw.Id, 'This is NOT OK.', false);

            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);
        }

        // Verify the record approval override fields were updated properly
        sObject record = Database.query(
             'Select Id, ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName) + 
            ' From ' + String.escapeSingleQuotes(piw.ProcessInstance.ProcessDefinition.TableEnumOrId) + 
            ' Where ' + String.escapeSingleQuotes(CC_Manage_Approvals_Controller.approvalOverrideFieldName) + ' = true'
        );

        system.assertEquals(piw.ProcessInstance.TargetObjectId, record.Id);
        
        // Verify the approval override request attributes that will be processed in the batch job
        CC_Manage_Approvals_Controller.ApprovalOverrideRequest overrideRequest = (CC_Manage_Approvals_Controller.ApprovalOverrideRequest)JSON.deserialize((String)record.get(CC_Manage_Approvals_Controller.approvalOverrideRequestFieldName), CC_Manage_Approvals_Controller.ApprovalOverrideRequest.class);
        
        system.assertEquals(salesRepMgr.Id, overrideRequest.overrideUserId);

        Approval.ProcessWorkitemRequest req = overrideRequest.pwr;

        system.assertEquals('Reject', req.getAction());
        system.assertEquals(piw.Id, req.getWorkitemId());
        system.assertEquals('This is NOT OK.', req.getComments());

        Test.stopTest();
    }    
}