@isTest
public class CC_PavilionTavantHomeController_Test{
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    public static testmethod void UnitTest_CC_PavilionTavantHomeController() 
    {
        Account a=TestUtilityClass.createCustomAccount1();
        insert a;
        System.assertNotEquals(null,a);
        Contact c=TestUtilityClass.createCustomContact1(a.id);
        insert c;
        System.assertNotEquals(null,c);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(c.Id,u.ContactId);
        System.runAs(u)
        {
        Id recordtypid = [select id from RecordType where RecordType.Name= 'Warranty' and RecordType.DeveloperName='CC_Warranty' and sobjecttype='CC_Pavilion_Content__c'].id;
        CC_Pavilion_Content__c pavContent = new CC_Pavilion_Content__c();
        pavContent.Title__c='Important Message';
        pavContent.RecordTypeId =recordtypid;
        pavContent.CC_Body_1__c='testdata';
        pavContent.CC_Body_2__c='testdatas';
        insert pavContent;
        CC_PavilionTemplateController Controller ;
        CC_PavilionTavantHomeController  tavantHomeController = new CC_PavilionTavantHomeController(Controller);
        Test.startTest();
        CC_PavilionTavantHomeController.getTavantBody();
        CC_PavilionTavantHomeController.getTavantHeading();
        tavantHomeController.getTavantURL();
        Test.stopTest();
        }
    }
}