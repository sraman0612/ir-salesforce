@isTest
private class CTS_IOT_Dashboard_ControllerTest {
    
    @testSetup
    static void setup(){
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', acc.Id, assetRecTypeId, true);
        
    }
    @isTest
    static void testGetInit(){
        Test.startTest();
        CTS_IOT_Dashboard_Controller.InitResponse initRes = CTS_IOT_Dashboard_Controller.getInit();
        System.assert(initRes != null);
        Test.stopTest();
    }
    @isTest
    static void testGetSiteInit(){
        List<Account> accounts = [select id from Account limit 1];
        if(!accounts.isEmpty()){
            Test.startTest();
            CTS_IOT_Dashboard_Controller.SiteInitResponse initRes = CTS_IOT_Dashboard_Controller.getSiteInit(accounts.get(0).Id);
            System.assert(initRes.site != null);
            system.assertEquals(false, initRes.hasConnectedAssets);
            Test.stopTest();
        }
    }
    @isTest
    static void testGetAssetInit(){
        List<Asset> assets = [select id from Asset limit 1];
        if(!assets.isEmpty()){
            Test.startTest();
            CTS_IOT_Dashboard_Controller.AssetInitResponse initRes = CTS_IOT_Dashboard_Controller.getAssetInit(assets.get(0).Id);
            System.assert(initRes != null);
            System.assert(initRes.assetDetail != null);
            Test.stopTest();
        }
    }
    
    @isTest
    static void testGetAssetInitException(){
        Test.startTest();
        // Passing invalid Id to cover exception part 
        CTS_IOT_Dashboard_Controller.AssetInitResponse initRes = CTS_IOT_Dashboard_Controller.getAssetInit('001K000001Zchx5');
        System.assert(initRes != null);
        System.assert(initRes.errorMessage.contains('List index out of bounds: 0'));
        Test.stopTest();
    }
    
    @isTest
    static void testGetSiteInitException(){
        Test.startTest();
        // Passing invalid Id to cover exception part 
        CTS_IOT_Dashboard_Controller.SiteInitResponse initRes = CTS_IOT_Dashboard_Controller.getSiteInit('001K000001Zchx5');
        System.assert(initRes != null);
        System.assert(initRes.errorMessage.contains('List index out of bounds: 0'));
        Test.stopTest();
    }
}