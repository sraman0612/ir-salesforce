/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Trigger Handler for the "Account" SObject.
              This class should only be called by the trigger "rshvacQuoteTrigger"
              And should be without sharing since triggers execute in System mode
------------------------------------------------------------*/

public without sharing class RshvacAccountTriggerHandler{
  
  // Instance of the helper class OpportunityUtils
  public OpportunityUtils oppUtils;
  // Developer Name of the RS_HVAC Record type
  public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
  // Id of the RS_HVAC Record type
  public static Id RS_HVAC_RT_ID;
  // Developer Name of the IWD Record type
  public static final String IWD_RT_DEV_NAME = 'RS_HVAC_IWD';    
  // Id of the IWD Record type
  public static Id IWD_RT_ID;
  // Account Types
  public static final String DEALER = 'Dealer';
  public static final String DISTRIBUTOR = 'Distributor';  
  public static final String ALTERNATE_WHOLESALE = 'Alternate Wholesale'; 
  public static final String DIRECT = 'Direct';
  // Account Status
  public static final String POTENTIAL = 'Potential';
  public static final String CUSTOMER = 'Customer';
  public static final String PROSPECT = 'Prospect';
  // Account Sales Channel
  public static final String DSO = 'DSO';
  public static final String IWD = 'IWD';
  // Map that will contain all the Opportunities related to the Accounts being processed for the current year
  // The key of this map will be the Id of the Account related to it
  public Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();
  // List that will contain all the Opportunities that we might need to insert
  public List<Opportunity> opportunitiesToInsert = new List<Opportunity>();
  // Map that will contain all the Revenue Schedules that we might need to insert
  // The key of this Map will be the Account that relates to the Opportunity that will be related to the Revenue
  public Map<Id, List<Opportunity_Revenue_Schedule__c>> revenuesByAccount = new Map<Id, List<Opportunity_Revenue_Schedule__c>>();
  // Map that will contain the Seasonal Schedule by District
  public Map<String, RS_District_Seasonal_Schedule__c> seasonalSchedule = new Map<String, RS_District_Seasonal_Schedule__c>();
  // Map that will contain the User Ids that corresonds to each Sales Person Id
  public Map<String, Id> userIdBySalesPerson = new Map<String, Id>();
  // List that will contain Cost Center Number of accounts to be processed
  public List<String> costCenterDefaultBranchList = new List<String>();
  // Map that will contain pair of Cost Center Number and Cost Center records of accounts to be processed
  static Map<String, Cost_center__c> costCenterMap = new Map<String, Cost_Center__c>();
  // Static initializer
  static{
    RS_HVAC_RT_ID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
     }

  public RshvacAccountTriggerHandler(){
    // Get an instance of Oppo
    oppUtils = new OpportunityUtils();
  }

  public void bulkBefore(){
    if (Trigger.isInsert || Trigger.isUpdate){
      // If it's a trigger or an update, collect all the Sales_Person_Id__c of the accounts being processed
      Set<String> salesPersonIds = new Set<String>();
      for (SObject sObj : Trigger.new){
        Account a = (Account) sObj;
        if (String.isNotBlank(a.Sales_Person_Id__c)){
          salesPersonIds.add(a.Sales_Person_Id__c);
        }
        if (a.Default_Branch__c != null){
          costCenterDefaultBranchList.add(a.Default_Branch__c);  
        }  
      }
      // If there are SalesPerson Ids populated
      if (!salesPersonIds.isEmpty()){
        for (User u : [SELECT Id, Sales_Person_Id__c FROM User WHERE Sales_Person_Id__c IN :salesPersonIds]){
          userIdBySalesPerson.put(u.Sales_Person_Id__c, u.Id);
        }
      }
      // If there are Cost Centers in Accounts
      if (!costCenterDefaultBranchList.isEmpty()){
        for (Cost_Center__c c: [SELECT Id, MDM__c,Channel__c, BDM__c, Cost_Center_Number__c, Default_Branch__c, OwnerId from Cost_Center__c where Default_Branch__c in :costCenterDefaultBranchList]){
          costCenterMap.put(c.Default_Branch__c, c);
        }
      }
    }
  }

  public void bulkAfter(){
    for (RS_District_Seasonal_Schedule__c districtSchedule: RS_District_Seasonal_Schedule__c.getAll().values()){
      seasonalSchedule.put(districtSchedule.District__c, districtSchedule);
    }
    // On updates, we need to check if the Existing Business Opportunity has been created for the current year
    if (Trigger.isUpdate){
      // Get the Id of the Opportunity Existing BUsiness Record Type
      Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;
      // Get the Existing Business Opportunities of the current year for the Accounts being processed (if any)
      for (Opportunity o : [SELECT Id, AccountId FROM Opportunity WHERE AccountId IN :Trigger.newMap.keySet() AND CloseDate = THIS_YEAR AND RecordTypeId = :existingBusinessRecTypeId]){
        opportunitiesMap.put(o.AccountId, o);
      }
    }
  }
  
    
  public void beforeInsert(Account newAccount){
    // If Account contains Default_Branch__c 
    if (newAccount.Default_Branch__c!=null && costCenterMap.containsKey(newAccount.Default_Branch__c)){
      newAccount.Cost_Center_Name__c = costCenterMap.get(newAccount.Default_Branch__c).Id;
      // If related cost center is of DSO Channel 
      if (costCenterMap.get(newAccount.Default_Branch__c).Channel__c != Null && costCenterMap.get(newAccount.Default_Branch__c).Channel__c == DSO){
        // If the record type is RS_HVAC, set the field to true
        //newAccount.Data_com_Does_Not_Auto_Update__c = true; // Case-10842 - Inactivated by Ashwini on 05-JUN-17
        // If SalesPerson is populated and there's a User with that SalesPerson Id
        String salesPersonId = newAccount.Sales_Person_Id__c;
        if (String.isNotBlank(salesPersonId) && userIdBySalesPerson.get(salesPersonId) != null){
          // Set it as the Owner
          newAccount.OwnerId = userIdBySalesPerson.get(salesPersonId);
        }
      }
      else if (costCenterMap.get(newAccount.Default_Branch__c).Channel__c != Null && costCenterMap.get(newAccount.Default_Branch__c).Channel__c != DSO){
        // Set Regional Manager of Cost Center as owner of the account
        newAccount.OwnerId = costCenterMap.get(newAccount.Default_Branch__c).OwnerId;      
      }  
    }
    // If Status is prospect and MADIS_Number__c is not blank, set the status to Prospect
    if (String.isNotBlank(newAccount.MADIS_Number__c) && PROSPECT.equalsIgnoreCase(newAccount.Status__c)){
      newAccount.Status__c = POTENTIAL;
    }
    
    /* Dhilip commented this since this is not necessary. RS Siebel is sending the Address in the standard API Fields. 
    //Update Shipping Street if blank
    if (String.isBlank(newAccount.ShippingStreet) || newAccount.ShippingStreet == null) {
        newAccount.ShippingStreet = newAccount.Address_1__c;
    }
    //Update Shipping State if blank
    if (String.isBlank(newAccount.ShippingState) || newAccount.ShippingState == null) {
        newAccount.ShippingState = newAccount.State_Code__c;     
    }    
    End comment -- Dhilip */
  }

  public void afterInsert(Account newAccount){
    // If criteria met, create existing business opportunity and assign owner.
    if (shouldHaveExistingBusinessOpportunity(newAccount)){
      // Create the Opportunity
      Opportunity opp = oppUtils.createExistingBusinessOpportunity(newAccount);
      if (newAccount.OwnerId != Null) {
          opp.OwnerId = newAccount.OwnerId;
      }
      opportunitiesToInsert.add(opp);
      RS_District_Seasonal_Schedule__c schedule = newAccount.Default_Branch__c != null ? seasonalSchedule.get(newAccount.Default_Branch__c) : null;
      // Create Opportunity Revenue Schedules and build Revenue Schedules map for DSO Account.
      List<Opportunity_Revenue_Schedule__c> revenues = oppUtils.createRevenueSchedule(schedule, opp.Sales_Budget__c);
      revenuesByAccount.put(newAccount.Id, revenues);
    } 
  }


  public void beforeUpdate(Account oldAccount, Account newAccount){
    // Ronnie Stegall - Case 11219 - Update code to allow accounts without an Oracle default branch to be associated
    // to a cost center.
    // Original comment - If Account's Default_Branch__c is blank, update related cost center as null. 
    /*
    if (newAccount.Default_Branch__c == null || String.isBlank(newAccount.Default_Branch__c) || !costCenterMap.containsKey(newAccount.Default_Branch__c)){
      newAccount.Cost_Center_Name__c = null;          
    }
    */
    // If Account has Default Branch.
    if (newAccount.Default_Branch__c != null){
      String defaultBranch = newAccount.Default_Branch__c; 
      
      // Ronnie Stegall - Case 21899 - Check if defaultBranch exists in cost center table
      // before trying to pull cost center name associated to defaultBranch.
      // This is to prevent exception "Attempt to de-reference a null object Class".
      If (costCenterMap.containsKey(defaultBranch)){
        // Ronnie Stegall - Case 11219 - Don't allow cost center name to be changed if account has an Oracle default branch.
        // Cost center should always align with the default branch.
        If (newAccount.Cost_Center_Name__c != costCenterMap.get(defaultBranch).Id){
          newAccount.Cost_Center_Name__c = costCenterMap.get(defaultBranch).Id;
        }
      
        // Ronnie Stegall - Case 21899 - No longer need to check costCenterMap.containsKey(defaultBranch) here
        // because that is being done in outer if statement
        // if ((oldAccount.Default_Branch__c == NULL || defaultBranch != oldAccount.Default_Branch__c) && costCenterMap.containsKey(defaultBranch)){
        // If there is a change in default branch, update related cost center. Assign Non DSO Cost Center's Owner as owner to the Account.
        if (oldAccount.Default_Branch__c == NULL || defaultBranch != oldAccount.Default_Branch__c){
          newAccount.Cost_Center_Name__c = costCenterMap.get(defaultBranch).Id;
          if(costCenterMap.get(newAccount.Default_Branch__c).Channel__c != NULL && costCenterMap.get(newAccount.Default_Branch__c).Channel__c != DSO ){
            newAccount.OwnerId = costCenterMap.get(defaultBranch).OwnerId;       
          }
        } 
      
        // For DSO Account
        if (costCenterMap.get(newAccount.Default_Branch__c).Channel__c != NULL && costCenterMap.get(newAccount.Default_Branch__c).Channel__c == DSO ){
          // If SalesPerson is changed and there's a User with that SalesPerson Id
          String salesPersonId = newAccount.Sales_Person_Id__c;
          if (salesPersonId != NULL && salesPersonId != oldAccount.Sales_Person_Id__c && userIdBySalesPerson.get(salesPersonId) != null){
            // Set it as the Owner
            newAccount.OwnerId = userIdBySalesPerson.get(salesPersonId);
          }
        }    

      }
    }
    // If Status is prospect and MADIS_Number__c was blank and now isn't, set the status to Potential
    if (String.isNotBlank(newAccount.MADIS_Number__c) && PROSPECT.equalsIgnoreCase(newAccount.Status__c)){
      newAccount.Status__c = POTENTIAL;
    }
    /* Dhilip commented this since this is not necessary. RS Siebel is sending the Address in the standard API Fields. 
    //Update Shipping Street if blank
    if (String.isBlank(oldAccount.ShippingStreet) || oldAccount.ShippingStreet == null) {
        newAccount.ShippingStreet = newAccount.Address_1__c;
    }
    //Update Shipping State if blank
    if (String.isBlank(oldAccount.ShippingState) || oldAccount.ShippingState == null) {
        newAccount.ShippingState = newAccount.State_Code__c;
    }
    //Update Shipping Country if blank
    if (String.isBlank(oldAccount.ShippingCountry) || oldAccount.ShippingCountry == null) {
        newAccount.ShippingCountry = newAccount.Country_Code__c;
    }
    End comment -- Dhilip */
  }

  public void afterUpdate(Account oldAccount, Account newAccount){
    if (shouldHaveExistingBusinessOpportunity(newAccount) && opportunitiesMap.get(newAccount.Id) == null){
      // If it should have an Opportunity and it doesn't already, create it
      if(newAccount.Default_Branch__c != NULL && costCenterMap.get(newAccount.Default_Branch__c).Channel__c != NULL && costCenterMap.get(newAccount.Default_Branch__c).Channel__c == DSO){
        // Set Account Owner as the opportunity owner
        Opportunity opp = oppUtils.createExistingBusinessOpportunity(newAccount);
        opp.OwnerId = newAccount.OwnerId;
        opportunitiesToInsert.add(opp);
        RS_District_Seasonal_Schedule__c schedule = newAccount.Default_Branch__c != null ? seasonalSchedule.get(newAccount.Default_Branch__c) : null;
        List<Opportunity_Revenue_Schedule__c> revenues = oppUtils.createRevenueSchedule(schedule, opp.Sales_Budget__c);
        revenuesByAccount.put(newAccount.Id, revenues);
      }  
    }
  }

  public void andFinally(){
    // If there are opportunities to be inserted.
    if (!opportunitiesToInsert.isEmpty()){
      insert opportunitiesToInsert;
      List<Opportunity_Revenue_Schedule__c> revenuesToInsert = new List<Opportunity_Revenue_Schedule__c>();
      // Update revenue schedule with related opportunity. 
      for (Opportunity opp : opportunitiesToInsert){
        for (Opportunity_Revenue_Schedule__c ors : revenuesByAccount.get(opp.AccountId)){
          ors.Opportunity__c = opp.Id;
          revenuesToInsert.add(ors);
        }
      }
      insert revenuesToInsert;
    }
  }

  // Helper method to check the criteria for creating Existing Business Opportunity for DSO Account. 
  // Ashwini: Removed Acctype check on 01-AUG-2017 ->> DEALER.equalsIgnoreCase(acc.Type)
  private Boolean shouldHaveExistingBusinessOpportunity(Account acc){
    if (acc.Default_Branch__c != NULL && costCenterMap.containsKey(acc.Default_Branch__c) && costCenterMap.get(acc.Default_Branch__c).Channel__c != NULL && costCenterMap.get(acc.Default_Branch__c).Channel__c == DSO && (POTENTIAL.equalsIgnoreCase(acc.Status__c) || CUSTOMER.equalsIgnoreCase(acc.Status__c))){
        return true;
        }        
    
    else{
      return false;          
      }       
  }
}