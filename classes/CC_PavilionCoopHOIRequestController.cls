global class CC_PavilionCoopHOIRequestController {

    /* member variable */

    public String userAccountId{get; set;}
        
    /* constructor */    

    public CC_PavilionCoopHOIRequestController()
    {    
        List<User> lstUser = [SELECT AccountId, ContactId FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        if(lstUser.isEmpty()) return;
         UserAccountId = lstUser[0].AccountId;
         
         
    }
    
    /*Remote method to be called from CC_PavilionCoopHOIRequests page via JS Remoting*/
    
    @RemoteAction
    global static String getCoopHOIRequestJson(String userAccountId)
    {
         List<RecordType> rt = [select id, Name from RecordType where SobjectType = 'CC_Hole_in_One__c' and Name = 'Hole in Request'  Limit 1 ];
         
        List<CC_Hole_in_One__c> lstCoopClaim = new List<CC_Hole_in_One__c>();
        if(userAccountId != null)
        {
            lstCoopClaim =[SELECT Id, Name, Date_of_the_first_day_of_Tournament__c, CC_Status__c, Complete_Name_of_Tournament__c, 
                           Course_Name__c, Vehicle__c from CC_Hole_in_One__c where Contract__r.AccountId =: userAccountId AND RecordTypeId =: rt[0].id 
                           AND Claim_Status__c ='' AND CreatedDate = LAST_N_DAYS:90];
        }
        if(!lstCoopClaim.isEmpty())
        {
            String jsonData = (String)JSON.serialize(lstCoopClaim);
            return jsonData;
        }
        else
        {
            return 'No data found';
        }
    }
}