public class CC_CE_Cert
{
    public static String SerialNum{get;set;}
    public String ProductDesc{get;set;}
    public String ProductDirectives{get;set;}
    public String ProductStandards{get;set;}
    public static String ProductId;
    public String CEPartId;
    public static String Asset;
    public List<CC_CE_Parts__c> CEPart;
    public static List<CC_Order_Item__c> SVPItemsList;
    public static String OrderNum;
    
    public CC_CE_Cert()
    {
        SerialNum = ApexPages.currentPage().getParameters().get('Serial');
        CEPartId = ApexPages.currentPage().getParameters().get('Product');
        
    }
    
    public void GetProductDetails()
    {
        if(CEPartId != null)
        {
            CEPart = [Select Id,Vehicle_Description__r.Name,Directives__c,Standards__c from CC_CE_Parts__c Where Id = :CEPartId];
            if(!CEPart.isEmpty())
            {
                ProductDesc = CEPart.get(0).Vehicle_Description__r.Name;
                ProductDirectives = CEPart.get(0).Directives__c;
                ProductStandards = CEPart.get(0).Standards__c;
            }
        }
    }
    
    @InvocableMethod
    public static void getAssetId(List<ID>AssetId)
    {
     try 
     {
            List<CC_Asset__c> AssetList = [SELECT Id,Order__c,Serial__c,Product__c, Order__r.Name from CC_Asset__c where Id in: AssetId];
            if(!AssetList.isEmpty())
            {
              for (CC_Asset__c ccassetlistid: AssetList)
              {           
                    string CEPartRowId=null;
                    Asset = ccassetlistid.Id;
                    SerialNum = ccassetlistid.Serial__c;
                    ProductId = ccassetlistid.Product__c;
                    OrderNum = ccassetlistid.Order__r.Name;
                    List<CC_CE_Parts__c> CEPartList = [SELECT CE_Compliance_PN__c, Required_PN_1__c, Required_PN_2__c, Required_PN_3__c, Required_PN_4__c from CC_CE_Parts__c where Vehicle_Description__c =: ProductId];
                    if(!CEPartList.isEmpty())
                    {
                        System.debug('CEPartList:'+CEPartList);  
                        for (CC_CE_Parts__c CEPart : CEPartList)
                        {
                            System.debug('CECompliance:'+CEPart);
                            if(CEPart.CE_Compliance_PN__c != null)
                            {
                                List<ID> PartIds = new List<ID>();
                                
                                PartIds.add(CEPart.CE_Compliance_PN__c);
                                if(CEPart.Required_PN_1__c!= null)
                                {
                                    PartIds.add(CEPart.Required_PN_1__c);
                                    if(CEPart.Required_PN_2__c!= null)
                                    {
                                         PartIds.add(CEPart.Required_PN_2__c);
                                         if(CEPart.Required_PN_3__c!= null)
                                         {    
                                             PartIds.add(CEPart.Required_PN_3__c);
                                             if(CEPart.Required_PN_4__c!= null)
                                             {
                                                 PartIds.add(CEPart.Required_PN_4__c);
                                             }
                                          }
                                     }
                                 }                
                                 
                                System.debug('PartIds:'+PartIds);
                                System.debug('PartIdssize:'+PartIds.size());
                                
                                List<CC_Order_Item__c> OrderLineItems = [SELECT Id from CC_Order_Item__c where Order__c=: ccassetlistid.Order__c and product__c in: PartIds];
                                                                  
                                System.debug('OrderLineItems:'+OrderLineItems);
                                System.debug('OrderLineItemsSize:'+OrderLineItems.size());
                                
                               
                                if(PartIds.size() == OrderLineItems.size())
                                {
                                    CEPartRowId= CEPart.Id;
                                    break;
                                }
                            }
                        }
                        
                             
                        if(CEPartRowId!= null)
                        {
                            List<CC_Order_Item__c> OrderLineItemsList = [SELECT Id from CC_Order_Item__c where Order__c=: ccassetlistid.Order__c and product__r.CC_Structure_Type__c='Customizable'];
                            system.debug ('OrderLineItemsList1 ' + OrderLineItemsList.size());    
                            SVPItemsList = [SELECT Id from CC_Order_Item__c where Order__c=: ccassetlistid.Order__c and product__r.ProductCode like 'SVP%' Limit 1];
                            
                            system.debug('svplist ' + SVPItemsList);
                             system.debug('svplist ' + SVPItemsList.size()); 
                             
                            if(OrderLineItemsList.size()==1)
                            {        
                                PageReference cepdf = new pagereference('/apex/CC_CreateCECert');
                                cepdf.getParameters().put('Serial', SerialNum);
                                cepdf.getParameters().put('Product', CEPartRowId);
                                cepdf.setRedirect(true);
                                            
                                Blob Body;
                                if(!test.isRunningTest())
                                {
                                    Body= cepdf.getContent();
                                }
                                else
                                {
                                    Body= blob.valueof('TESTPDF');
                                }
                                
                                Attachment attach = new Attachment();
                                attach.Name = 'CE_CERT_'+ SerialNum + '_' + Date.today() +'.pdf';
                                attach.ParentId = Asset;
                                attach.Body = Body;
                                attach.contenttype = 'application/pdf';
                                insert attach;
                            }   
                            
                           
                                                
                            else if(OrderLineItemsList.size()>1)
                            {
                        
                              system.debug ('OrderLineItemsList2 ' + OrderLineItemsList.size());      
                          
                              messaging.SingleEmailMessage eml = new Messaging.SingleEmailMessage();
                              List<String> sToAddress = (Label.CC_CE_Exception_Emails).split(';'); 
                              eml.setToAddresses(sToAddress);
                              eml.setSenderDisplayName('CE Compliance Salesforce.com');
                              eml.setSubject('CE Compliance Certification Exception for Order # ' + ccassetlistid.Order__r.Name);
                              string msg = 'CE Compliance Certificate was not generated for Order # ' + ccassetlistid.Order__r.Name + ' because it has multiple configurable cars.';
                              eml.setPlainTextBody(msg);
                              Messaging.sendEmail(new Messaging.SingleEmailMessage[]{eml});
                               
                              break;  
                            } 
                        }  
                    }
               
               
             }//end for   
             
             //MMattawar:8-Apr-2019: RT 8260045: To notify for SVP Parts on CE Certified Assets.
             
             if(SVPItemsList.size()!=0)
             {
                  messaging.SingleEmailMessage emlsvp = new Messaging.SingleEmailMessage();
                  List<String> sToAddresssvp = (Label.CC_CE_Exception_Emails).split(';'); 
                  emlsvp.setToAddresses(sToAddresssvp);
                  emlsvp.setSenderDisplayName('CE Compliance Salesforce.com');
                  emlsvp.setSubject('SVP Part found on CE Compliant Order # ' + OrderNum);
                  string msgsvp = 'SVP Part found on CE Compliant Order # ' + OrderNum;
                  emlsvp.setPlainTextBody(msgsvp);
                  Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emlsvp});  
             }
             
            } //end if 
        } //end of try
        
       catch(Exception e) 
           {
               system.debug ('Error: ' +e.getmessage());
              // throw e;
           }
       
    } // end of Invocable
    
}