/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Trigger Handler for the "Opportunity Revenue Schedule" SObject.
              This class should only be called by the trigger "RshvacOppRevenueScheduleTrigger"
              And should be without sharing since triggers execute in System mode
------------------------------------------------------------*/

public without sharing class RshvacOppRevenueScheduleTriggerHandler{

  // Separator used to form the key of the maps of residential quota
  public static final String KEY_SEP = '#';
  // Id of the Owners of the Accounts by the Opportunity Id
  public Map<Id, Id> iwdAccountOwnersByOppId = new Map<Id, Id>();
  // Id of the DSO Owners of the Accounts by the Opportunity Id
  public Map<Id, Id> dsoAccountOwnersByOppId = new Map<Id, Id>();
  // Id of the IWD Owners of the Accounts by the Opportunity Id
  public Map<Id, Id> accountOwnersByOppId = new Map<Id, Id>();
  // Map that will contain Opportunity of Revenue Schedule
  public Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
  // Map that will contain all the Residential Quotas that could be updated
  // The key of this map will be a generated string with the following format USER_ID#YEAR#MONTH
  // Where USER is the Id of the User for which the Quota is Assigned
  // YEAR and MONTH are taken from the Forecast Month
  public Map<String, Residential_Quota__c> allResidentialQuotas = new Map<String, Residential_Quota__c>();
  // Quotas to be updated
  public Map<Id, Residential_Quota__c> quotasToUpdate = new Map<Id, Residential_Quota__c>();
  // Map with key as new Opportunity Revenue Schedule Id and value as Old Opportunity Revenue Schedule
  public static Map<String, Opportunity_Revenue_Schedule__c> revScheduleToOldRevSchedule = new Map<String, Opportunity_Revenue_Schedule__c>();
  // Account Sales Channel
  public static final String DSO = 'DSO';
  public static final String IWD = 'IWD';
    
  public RshvacOppRevenueScheduleTriggerHandler(){
  }

  public void bulkBefore(List<Opportunity_Revenue_Schedule__c> revSchedulesList){
    List<Id> accountList = new List<Id>();
    List<Id> costCenterList = new List<Id>(); 
    Set<Integer> previousYearsSet = new Set<Integer>();
    // Iterating through new Revenue Schedules. Collecting previous years of new revenue schedules. 
    for(Opportunity_Revenue_Schedule__c newOrs: revSchedulesList){
      Integer prevYear = newOrs.Forecast_Month__c.Year()-1;
      previousYearsSet.add(prevYear);
      opportunityMap.put(newOrs.Opportunity__c, new Opportunity());
    }
    // Iterating through existing business opportunities of revenue schedules. Collecting Accounts and Cost Centers of opportunities. 
    for(Opportunity opp: [Select Id, AccountId, Cost_Center_Name__c, Account.Channel__c from Opportunity where Id IN :opportunityMap.keySet()]){
      opportunityMap.put(opp.Id, opp);
      if(opp.Cost_Center_Name__c != Null)
        costCenterList.add(opp.Cost_Center_Name__c);
      if(opp.AccountId != Null)
        accountList.add(opp.AccountId); 
    }
    // Collecting previous year revenue schedules of new revenue schedules.
    for(Opportunity_Revenue_Schedule__c oldOrs: [Select Id, Opportunity__r.AccountId, Opportunity__r.Account.Channel__c, Opportunity__r.Cost_Center_Name__c, Actual__c,Forecast_Month__c 
                                                   from Opportunity_Revenue_Schedule__c where (Opportunity__r.Cost_Center_Name__c IN :costCenterList OR Opportunity__r.AccountId IN :accountList) AND CALENDAR_YEAR(Forecast_Month__c) IN :previousYearsSet]){
      if (oldOrs.Opportunity__r.AccountId != Null && oldOrs.Opportunity__r.Account.Channel__c != Null && oldOrs.Opportunity__r.Account.Channel__c == DSO){
        // Generate key of revScheduleToOldRevSchedule Map using Account Id for DSO revenue schedules.
        String key = getOppRevenueScheduleKey(oldOrs, oldOrs.Opportunity__r.AccountId);
        revScheduleToOldRevSchedule.put(key, oldOrs);
      }
      else if (oldOrs.Opportunity__r.Cost_Center_Name__c != Null && (oldOrs.Opportunity__r.Account.Channel__c == Null || oldOrs.Opportunity__r.Account.Channel__c != DSO)){
        // Generate key of revScheduleToOldRevSchedule Map using Cost Center Id for DSO revenue schedules.
        String key = getOppRevenueScheduleKey(oldOrs, oldOrs.Opportunity__r.Cost_Center_Name__c);
        revScheduleToOldRevSchedule.put(key, oldOrs);
      }       
    } 
  }
    
  // Helper method to update Prior_Year_Sales__c.
  public void updatePriorYearSales(Opportunity_Revenue_Schedule__c ors){
    String Key ;
    Opportunity_Revenue_Schedule__c oldOrs = new Opportunity_Revenue_Schedule__c();
    if (opportunityMap.get(ors.Opportunity__c).AccountId != Null && opportunityMap.get(ors.Opportunity__c).Account.Channel__c != Null && opportunityMap.get(ors.Opportunity__c).Account.Channel__c == DSO){
      key = '';
      key +=  opportunityMap.get(ors.Opportunity__c).AccountId + KEY_SEP + String.valueOf(ors.Forecast_Month__c.year()-1) + KEY_SEP + String.valueOf(ors.Forecast_Month__c.month());      
      // For DSO revenue schedule generate key using Account Id and update prior year sales with old revenue schedule's Actual. 
      if(revScheduleToOldRevSchedule.containsKey(key)){
        oldOrs = revScheduleToOldRevSchedule.get(key);
        ors.Prior_Year_Sales__c = oldOrs.Actual__c;
      }
    }
    else if (opportunityMap.get(ors.Opportunity__c).Cost_Center_Name__c != Null && (opportunityMap.get(ors.Opportunity__c).Account.Channel__c == Null || opportunityMap.get(ors.Opportunity__c).Account.Channel__c != DSO) && ors.Forecast_Month__c != Null){
      key = '';
      key +=  opportunityMap.get(ors.Opportunity__c).Cost_Center_Name__c + KEY_SEP + String.valueOf(ors.Forecast_Month__c.year()-1) + KEY_SEP + String.valueOf(ors.Forecast_Month__c.month());      
      // For non DSO revenue schedule generate key using Cost Center Id and update prior year sales with old revenue schedule's Actual. 
      if(revScheduleToOldRevSchedule.containsKey(key)){
        oldOrs = revScheduleToOldRevSchedule.get(key);
        ors.Prior_Year_Sales__c = oldOrs.Actual__c;
      }
    }
  }
    
  
    
  public void bulkAfter(){
    if (Trigger.isInsert || Trigger.isUpdate){
      // Dates used to limit the range date used to query the Residential Quotas
      Date minDate = Date.newInstance(Date.today().year() + 20, 1, 1);
      Date maxDate = Date.newInstance(Date.today().year() - 20, 1, 1);
      Set<Id> oppIds = new Set<Id>();
      // Collect the Opportunity Ids
      for (SObject sObj : Trigger.new){
        Opportunity_Revenue_Schedule__c revSchedule = (Opportunity_Revenue_Schedule__c) sObj;
        // Only process the Revenue Schedule if its forecast month is not null (it shouuldn't)
        if (revSchedule.Forecast_Month__c != null){
          oppIds.add(revSchedule.Opportunity__c);
          if (revSchedule.Forecast_Month__c < minDate){
            minDate = Date.newInstance(revSchedule.Forecast_Month__c.year(), 1, 1);
          }
          if (revSchedule.Forecast_Month__c > maxDate){
            maxDate = Date.newInstance(revSchedule.Forecast_Month__c.year() + 1, 1, 1);
          }
        }
      }
      
      // Get the Id of the Opportunity Existing BUsiness Record Type
      Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;
      // Preparing map with key as opportunity id and value as Account ownerid for DSO opportunities. 
      for (Opportunity o : [SELECT Id, Account.OwnerId, Cost_Center_Name__r.OwnerId, RecordTypeId FROM Opportunity WHERE Id IN :oppIds AND Account.Channel__c = :DSO AND RecordTypeID = :existingBusinessRecTypeId]){
        dsoAccountOwnersByOppId.put(o.Id, o.Account.OwnerId);
      }
      // Preparing map with key as opportunity id and value as Cost Center's ownerid for non DSO opportunities. 
      for (Opportunity o : [SELECT Id, Account.OwnerId, Cost_Center_Name__r.OwnerId, RecordTypeId FROM Opportunity WHERE Id IN :oppIds AND ID NOT IN :dsoAccountOwnersByOppId.keySet() AND RecordTypeID = :existingBusinessRecTypeId]){
        iwdAccountOwnersByOppId.put(o.Id, o.Cost_Center_Name__r.OwnerId);      
      }
        
      accountOwnersByOppId.putAll(dsoAccountOwnersByOppId);
      accountOwnersByOppId.putAll(iwdAccountOwnersByOppId);
      // Query the Residential Quota
      for (Residential_Quota__c q : [SELECT Id, Actual__c, Forecast_Month__c, User__c FROM Residential_Quota__c
                                      WHERE (User__c IN :dsoAccountOwnersByOppId.values() OR User__c IN :iwdAccountOwnersByOppId.values()) AND Forecast_Month__c >= :minDate AND Forecast_Month__c <= :maxDate]){
        String key = getResidentialQuotaKey(q);
        if (key != null){
          allResidentialQuotas.put(key, q);
        }
      }
    }
  }

  public void afterInsert(Opportunity_Revenue_Schedule__c newRevSchedule){
    // Only update the quota if the Actual value is populated
    if (newRevSchedule.Actual__c != null && newRevSchedule.Actual__c != 0){
      updateQuota(newRevSchedule, newRevSchedule.Actual__c);
    }
  }

  public void afterUpdate(Opportunity_Revenue_Schedule__c oldRevSchedule, Opportunity_Revenue_Schedule__c newRevSchedule){
    // Only update the quota if the Actual value has changed
    Decimal newActual = newRevSchedule.Actual__c != null ? newRevSchedule.Actual__c : 0;
    newActual -= oldRevSchedule.Actual__c != null ? oldRevSchedule.Actual__c : 0;
    if (newActual != 0){
      updateQuota(newRevSchedule, newActual);
    }
  }

  public void andFinally(){
    if (!quotasToUpdate.isEmpty()){
      update quotasToUpdate.values();
    }
  }

  private void updateQuota(Opportunity_Revenue_Schedule__c revSchedule, Decimal actualToSum){
    Id accOwnerId = accountOwnersByOppId.get(revSchedule.Opportunity__c);
    // The only way that this could be null is if the Forecast Month was also null
    if (accOwnerId != null){
      // Get the key to find the residential quota
      String key = getOppRevenueScheduleKey(revSchedule, accOwnerId);
      if (key != '' && allResidentialQuotas.get(key) != null){
        Residential_Quota__c quota = allResidentialQuotas.get(key);
        // Check if we already have the quota on the map, that way we make sure we're updating the proper value
        quota = quotasToUpdate.get(quota.Id) != null ? quotasToUpdate.get(quota.Id) : quota;
        // Update the actual value
        quota.Actual__c = quota.Actual__c != null ? quota.Actual__c : 0;
        quota.Actual__c += actualToSum;
        // Put it in the map to be updated
        quotasToUpdate.put(quota.Id, quota);
      }
    }
  }

  // Helper method to generate key 
  private String getOppRevenueScheduleKey(Opportunity_Revenue_Schedule__c revSchedule, String accountOwner){
    String key = '';
    if (accountOwner != null && revSchedule.Forecast_Month__c != null){
      key += accountOwner + KEY_SEP;
      Date revenueMonth = revSchedule.Forecast_Month__c;
      key += revenueMonth.year() + KEY_SEP + revenueMonth.month();
    }
    return key;
  }

  // Helper method to generate key
  private String getResidentialQuotaKey(Residential_Quota__c quota){
    String key = '';
    if (quota.User__c != null && quota.Forecast_Month__c != null){
      key += quota.User__c + KEY_SEP;
      Date quotaMonth = quota.Forecast_Month__c;
      key += quotaMonth.year() + KEY_SEP + quotaMonth.month();
    }
    return key;
  }
}