/**
 * @description       : 
 * @author            : Ben Lorenz
 * @group             : 
 * @last modified on  : 10-01-2020
 * @last modified by  : Ben Lorenz
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   10-01-2020   Ben Lorenz   Initial Version
**/
public without sharing class CC_PavilionDownloadPricingCtrl {
    
    public class ProductSearchResult{
    	public List<PricebookData> pbes = new List<PricebookData>();
    	public List<CC_Product_Rule__c> rules = new CC_Product_Rule__c[]{};
    }
    
    public class PricebookData {
    	public Id productId;
        public String productCode;
        public String productName;
        public String UM;
        public Double listPrice;
        public Double discountPrice;
        public Integer qtyDiscount;
        public Double qtyPrice;
        public String code;
        public Double inventory;
        
        public PricebookData(Id productId, String productCode, String productName, String UM, Double listPrice, Double discountPrice, Integer qtyDiscount, Double qtyPrice, String code, Decimal inventory) {
            this.productId = productId;
            this.productCode = productCode;
            this.productName = productName;
            this.UM = UM;
            this.listPrice = listPrice;
            this.discountPrice = discountPrice;
            this.qtyDiscount = qtyDiscount;
            this.qtyPrice = qtyPrice;
            this.code = code;
            this.inventory = inventory;
        }
    }
    
    // Constructor
    public CC_PavilionDownloadPricingCtrl(CC_PavilionTemplateController ctrl) {
    
    }
    
    @ReadOnly
    @RemoteAction
    public static ProductSearchResult checkItemPricing(String partNumber) {
    	
    	ProductSearchResult searchResult = new ProductSearchResult();
    	
    	Map<String, List<PricebookData>> pbeMap = getOrderItemsFromPriceBookEntry(partNumber);
    	
    	if (pbeMap.size() == 1){
    		
    		for (String key : pbeMap.keySet()){
    			searchResult.pbes = pbeMap.get(key);
    		}
    		
    		if (searchResult.pbes.size() > 0){
    		
    		    String replacePartNumber = '';

	    		for (CC_Product_Rule__c rule : [SELECT CC_Type__c, CC_Obsolete_Item__r.ProductCode, CC_Replacement_Item__r.ProductCode, CC_Message__c 
	    				 FROM CC_Product_Rule__c 
                        WHERE Request_Delete__c = FALSE AND
                              CC_Obsolete_Item__c = :searchResult.pbes[0].productId and 
                              (CC_Type__c != 'Replace' or (CC_Type__c = 'Replace' and CC_Replacement_Item__r.ProductCode != null and (CC_Substitute_Effective_Date__c = null or CC_Substitute_Effective_Date__c <= TODAY)))
	    				 Order By CC_Substitute_Effective_Date__c DESC]){
	    		
	    			if (rule.CC_Type__c == 'Replace' && String.isBlank(replacePartNumber)){
	    				
	    				if (String.isBlank(rule.CC_Message__c)){
	    					rule.CC_Message__c = 'This part is obsolete and has been replaced by part ' + rule.CC_Replacement_Item__r.ProductCode;
	    				}
	    				
	    				replacePartNumber = rule.CC_Replacement_Item__r.ProductCode;
	    			}
	    		
	    			searchResult.rules.add(rule);		 	
				 }
				 
				 system.debug('replacement part: ' + replacePartNumber);
				 
				 // If the part is obsolete and there is a replacement part declared then return the replacement part instead
				 if (String.isNotBlank(replacePartNumber)){
				 	
				 	Map<String, List<PricebookData>> replacepbeMap = getOrderItemsFromPriceBookEntry(replacePartNumber);
				 	
			    	if (replacepbeMap.size() == 1){
			    		
			    		for (String key : replacepbeMap.keySet()){
			    			
			    			if (replacepbeMap.get(key).size() == 1){
			    				searchResult.pbes = replacepbeMap.get(key);
			    			}
			    		}	
			    	}		 	
				 }
	    	}
    	}
    	
    	return searchResult;
    }    
    
    @ReadOnly
    @RemoteAction
    public static Map<String, List<PricebookData>> getOrderItemsFromPriceBookEntry(String partNumber) {
        
        // get plistPriceBook items
        Map<String, PricebookEntry> plistPriceMap = new Map<String, PricebookEntry>();
        Id PLISTID;
        
        try {
            PLISTID = [SELECT ID FROM Pricebook2 WHERE Name = 'PLIST'].ID;    
        }         	
        catch(Exception e) {
            System.debug(e);
        }
        
        try {
            plistPriceMap = getPriceBookEntriesByPricebook2Id(PLISTID, partNumber);
        }         
        catch(Exception e) {
            System.debug(e);
        }
        
        // get customerPricebook items
        Map<String, PricebookEntry> customerPriceMap = new Map<String, PricebookEntry>();
        String customerAccountId;
        
        try {
            customerAccountId = [SELECT AccountId FROM User WHERE ID = :userinfo.getuserid()].AccountId;
        }         	
        catch(Exception e) {
            System.debug(e);
        }
        
        String customerPricebookName;
        
        try {
            customerPricebookName = [SELECT CC_Price_List__c FROM Account WHERE ID = :customerAccountId].CC_Price_List__c;
        }         	
        catch(Exception e) {
            System.debug(e);
        }

        system.debug('customer pricebook name: ' + customerPricebookName);
        
        Id customerPriceBookID;
        
        try {
            customerPriceBookID = [SELECT ID FROM Pricebook2 WHERE Name = :customerPricebookName].ID;
        }       	
        catch(Exception e) {
            System.debug(e);
        }
        
        try {
            customerPriceMap = getPriceBookEntriesByPricebook2Id(customerPriceBookID, partNumber); 
        }         	
        catch(Exception e) {
            System.debug(e);
        }
        
        // get CADPricing
        Boolean CADPricing = false;
        
        try {
            
            if(String.valueOf([SELECT CC_Customer_Number__c FROM Account WHERE ID = :customerAccountId].CC_Customer_Number__c).endsWith('21')) { // ends with 21
                CADPricing = true;
            }    
        }       	
        catch(Exception e) {
            System.debug(e);
        }
        
        List<PricebookData> priceBookList = new List<PricebookData>();
        
        system.debug('plist price book map: ' + plistPriceMap);

        for(PricebookEntry elem : plistPriceMap.values()) {

            String productCode   = String.valueOf(elem.Product2.ProductCode);
            String productName   = String.valueOf(elem.Product2.Name);
            String UM            = String.valueOf(elem.CC_UM__c);

            /* Varies as per conditions below */
            Double listPrice     = 0;
            Double discountPrice = 0;
            Integer qtyDiscount  = 0;
            Double qtyPrice      = 0;
            /****************END***************/
            
            String code = String.valueOf(elem.CC_Code__c);
            
            if(customerPriceMap.containsKey(elem.Product2.ProductCode)) { // get data from customer pricebookentry

                PricebookEntry customerPriceBookEntryItem = customerPriceMap.get(elem.Product2.ProductCode);               

                if(elem.CC_Price_Change_Date__c >= Date.today() && elem.CC_Price_Change_Date__c <= Date.today().addDays(30)) { // pick new fields from customerPriceBookEntry
                    
                    if(CADPricing) { // get new CAD fields
                        listPrice     = Double.valueOf(elem.CC_New_CAD_Unit_Price__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(customerPriceBookEntryItem.CC_New_CAD_Unit_Price__c);
                        qtyDiscount   = Integer.valueOf(customerPriceBookEntryItem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(customerPriceBookEntryItem.CC_New_CAD_Quantity_Price__c);
                    } 
                    else { // get new non-CAD fields
                        listPrice     = Double.valueOf(elem.CC_New_UnitPrice__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(customerPriceBookEntryItem.CC_New_UnitPrice__c);
                        qtyDiscount   = Integer.valueOf(customerPriceBookEntryItem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(customerPriceBookEntryItem.CC_New_Quantity_Price__c);
                    }
                } 
                else { // pick non-new fields from customerPriceBookEntry
                    
                    if(CADPricing) { // get non-new CAD fields
                        listPrice     = Double.valueOf(elem.CC_CAD_Unit_Price__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(customerPriceBookEntryItem.CC_CAD_Unit_Price__c);
                        qtyDiscount   = Integer.valueOf(customerPriceBookEntryItem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(customerPriceBookEntryItem.CC_CAD_Quantity_Price__c);
                    } 
                    else { // get non-new non-CAD fields
                        listPrice     = Double.valueOf(elem.UnitPrice); // always from plist pricebookentry
                        discountPrice = Double.valueOf(customerPriceBookEntryItem.UnitPrice);
                        qtyDiscount   = Integer.valueOf(customerPriceBookEntryItem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(customerPriceBookEntryItem.CC_Quantity_Price__c);
                    }
                }    
            } 
            else { // get data from plist pricebookentry
                
                if(elem.CC_Price_Change_Date__c >= Date.today() && elem.CC_Price_Change_Date__c <= Date.today().addDays(30)) { // pick new fields from plist priceBookEntry
                    
                    if(CADPricing) { // get new CAD fields
                        listPrice     = Double.valueOf(elem.CC_New_CAD_Unit_Price__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(elem.CC_New_CAD_Unit_Price__c);
                        qtyDiscount   = Integer.valueOf(elem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(elem.CC_New_CAD_Quantity_Price__c);
                    } 
                    else { // get new non-CAD fields
                        listPrice     = Double.valueOf(elem.CC_New_UnitPrice__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(elem.CC_New_UnitPrice__c);
                        qtyDiscount   = Integer.valueOf(elem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(elem.CC_New_Quantity_Price__c);
                    }
                } 
                else {
                    
                    if(CADPricing) { // get non-new CAD fields
                        listPrice     = Double.valueOf(elem.CC_CAD_Unit_Price__c); // always from plist pricebookentry
                        discountPrice = Double.valueOf(elem.CC_CAD_Unit_Price__c);
                        qtyDiscount   = Integer.valueOf(elem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(elem.CC_CAD_Quantity_Price__c);
                    } 
                    else { // get non-new non-CAD fields
                        listPrice     = Double.valueOf(elem.UnitPrice); // always from plist pricebookentry
                        discountPrice = Double.valueOf(elem.UnitPrice);
                        qtyDiscount   = Integer.valueOf(elem.CC_Quantity_Discount__c);
                        qtyPrice      = Double.valueOf(elem.CC_Quantity_Price__c);
                    }
                }
            }
            
            PricebookData pData = new PricebookData(
                elem.Product2Id,
                productCode,
                productName,
                UM,
                listPrice,
                discountPrice,
                qtyDiscount,
                qtyPrice,
                code,
                elem.Product2.CC_Inventory_Count__c
            );           

            priceBookList.add(pData);
        }
        
        Map<String, List<PricebookData>> data = new Map<String, List<PricebookData>>();
        data.put(customerPricebookName, priceBookList);
        
        system.debug('returning data--');
        system.debug(data);

        return data;
    }
    
    private static Map<String, PricebookEntry> getPriceBookEntriesByPricebook2Id(Id Pricebook2Id, String partNumber) {
    	
        Map<String, PricebookEntry> priceBookEntries = new Map<String, PricebookEntry>();
        
        //@Priyanka Baviskar added active flag in query for ticket #8443996: PRICEBOOK DOWNLOADED FROM CLUB CAR LINKS
        String query = 'SELECT ID, Product2.ProductCode, Product2.Name, CC_UM__c, UnitPrice, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c,CC_Quantity_Discount__c, ' +  
            					'CC_New_UnitPrice__c, CC_New_CAD_Unit_Price__c, CC_New_Quantity_Price__c, CC_New_CAD_Quantity_Price__c,CC_Price_Change_Date__c, CC_Code__c, ' +
            					'Product2.CC_Inventory_Count__c ' +
        					'FROM PriceBookEntry ' +
        					'WHERE IsActive = true and PriceBook2ID = :Pricebook2Id';
        					
		if (String.isNotBlank(partNumber)){
			query += ' and ProductCode = \'' + String.escapeSingleQuotes(partNumber) + '\'';
		}

		system.debug('query: ' + query);
		system.debug('Pricebook2Id: ' + Pricebook2Id);
	
        for(PriceBookEntry pbe : Database.query(query)){
            system.debug('pbe: ' + pbe);
        	priceBookEntries.put(pbe.Product2.ProductCode, pbe);
        }
        
        return priceBookEntries;
    }
}