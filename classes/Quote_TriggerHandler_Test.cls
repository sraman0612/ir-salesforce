//
// (c) 2015, Appirio Inc
//
// Test class for Quote Trigger
//
// Apr 28, 2015     George Acker     original
// Aug 7, 2015      Surabhi Sharma   Modified
// Apr 13, 2015     Sujan Singh      Added code for adding product offerings

@isTest
private class Quote_TriggerHandler_Test {
    
    // Method to test quote trigger
    static testMethod void testQuoteTrigger() {
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.stagename = 'Stage 1. Qualify';
        o.closedate = system.today();
        o.Closed_Lost_Reason__c = 'Promoted To Order';
        o.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        insert o;
        
        o.stagename = 'Stage 1. Qualify';
        update o;
        
        Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c();
        oli.opportunity__c = o.id;
        oli.amount__c = 100000;
        oli.quantity__c = 2;
        insert oli;       

        
        Quote__c q = new Quote__c();
        q.name = 'Test Quote';
        q.opportunity__c = o.id;
        //q.Quote_SalesPrice__c = 0;
        insert q; 
        
        Quote_Line_Item__c qlt = new Quote_Line_Item__c();
        qlt.Name = 'test qlt';
        qlt.Quote__c = q.id;
        qlt.NRC_Subtotal__c = 1000;
        insert qlt;
        
        // Query the Quote__c again to get the updated rollup summary field.
        Quote__c QuoteAfterInsert = [Select Quote_SalesPrice__c from Quote__c where Id= :q.Id];
            System.assertEquals(1000, QuoteAfterInsert.Quote_SalesPrice__c);
        
        Test.startTest();
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
         //System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c,o.amount); //Dhilip commented since the Amount on Oppty is independent of Quote Amount
        
        Quote__c q2 = new Quote__c();
        q2.name = 'Test Quote';
        q2.opportunity__c = o.id;
        //q2.Quote_SalesPrice__c = 200000;
        q2.primary__c = true;
        insert q2; 
        
        Quote_Line_Item__c qlt2 = new Quote_Line_Item__c();
        qlt2.Name = 'test qlt';
        qlt2.Quote__c = q2.id;
        qlt2.NRC_Subtotal__c = 50;
        insert qlt2;
        
        // Query the Quote__c again to get the updated rollup summary field.
        Quote__c QuoteAfterInsert2 = [Select Quote_SalesPrice__c from Quote__c where Id= :q2.Id];
            System.assertEquals(50, QuoteAfterInsert2.Quote_SalesPrice__c);
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
        //    System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c + QuoteAfterInsert2.Quote_SalesPrice__c,o.amount);
        
        Quote__c q3 = new Quote__c();
        q3.name = 'Test Quote';
        q3.opportunity__c = o.id;
        q3.promoted_to_order__c = true;
        insert q3; 
        
        Quote_Line_Item__c qlt3 = new Quote_Line_Item__c();
        qlt3.Name = 'test qlt';
        qlt3.Quote__c = q3.id;
        qlt3.NRC_Subtotal__c = 2000;
        insert qlt3;
        
        // Query the Quote__c again to get the updated rollup summary field.
        Quote__c QuoteAfterInsert3 = [Select Quote_SalesPrice__c from Quote__c where Id= :q3.Id];
            System.assertEquals(2000, QuoteAfterInsert3.Quote_SalesPrice__c);
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
            System.assertEquals(200000,o.amount);
        
            q.promoted_to_order__c = true;
        update q;
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
         //   System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c + QuoteAfterInsert3.Quote_SalesPrice__c,o.amount);
        
            q2.promoted_to_order__c = true;
        update q2;
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
       //     System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c + QuoteAfterInsert2.Quote_SalesPrice__c + QuoteAfterInsert3.Quote_SalesPrice__c,o.amount);
        
            q.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
        update q;
        
        sObject[] sObjQuery = [SELECT Id FROM Quote__c WHERE Id = :q.Id];
            System.assertEquals(0, sObjQuery.size());     
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
       //     System.assertEquals(QuoteAfterInsert2.Quote_SalesPrice__c + QuoteAfterInsert3.Quote_SalesPrice__c,o.amount);
        
        Test.stopTest();
    }
}