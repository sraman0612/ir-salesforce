/*****************************************************************
Name:      RS_CRS_AssignRegionQueue_Batch
Purpose:   Batch class used to assign Queue based on Account state
History                                                            
-------                                                            
VERSION     AUTHOR          DATE            DETAIL
1.0         Ashish Takke    02/05/2018      INITIAL DEVELOPMENT
*****************************************************************/
global class RS_CRS_AssignRegionQueue_Batch implements Database.Batchable<sObject> {
    
    global final String queryStr;
    
    /*****************************************************************************
	Purpose            :  Constructor used to prepare query
	******************************************************************************/
    global RS_CRS_AssignRegionQueue_Batch(){
        Id caseRecordTypeId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        List<Group> queueList = [SELECT Id, Name 
                                 FROM Group 
                                 WHERE Name =: Label.RS_CRS_Escalation_Queue ];
        
        queryStr = 'Select Id, OwnerId, AccountId From Case ' +
            ' Where RecordTypeId = \'' + caseRecordTypeId + '\' AND ' +
            ' OwnerId = \'' + queueList.get(0).Id + '\' AND ' + 
            ' AccountId != null ';
    }
    
    /*****************************************************************************
	Purpose            :  Triggers query string                                                                                                      
	******************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(queryStr);
    }
    
    /*****************************************************************************
	Purpose            :  Assigns queue based on Account region                                                                                                      
	******************************************************************************/
    global void execute(Database.BatchableContext BC, List<sObject> caseList){
        try{
            //get Assignment rule
            List<AssignmentRule> assignRuleList = [SELECT Id, Name 
                                                   FROM AssignmentRule 
                                                   WHERE Name =: Label.RS_CRS_Assignment_Rule ];
            
            for(sObject caseRec: caseList){
                //assign rule to case records
                Database.DMLOptions dmo = new Database.DMLOptions();                
                dmo.assignmentRuleHeader.assignmentRuleId = assignRuleList.get(0).Id;
                caseRec.setOptions(dmo);
            }
            
            //update case records having assignment rule
            update caseList;
        } catch(Exception ex){
            System.debug('## Exception occured in Batch \'RS_CRS_AssignRegionQueue_Batch\' '+ ex.getMessage());
        }
    }
    
    /*****************************************************************************
	Purpose            :  Finish method                                                                                                      
	******************************************************************************/
    global void finish(Database.BatchableContext BC){
    }
}