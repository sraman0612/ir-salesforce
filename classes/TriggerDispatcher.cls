/*
@Author : Snehal Chabukswar
@CreatedDate : 12 March 2020
@Description : Trigger Dispatcher.
*/
public class TriggerDispatcher {
     
    /*
    @Author : Snehal Chabukswar
    @CreatedDate : 12 March 2020
    @Description : It will invoke the appropriate methods on the handler depending on the trigger context.
    @Parameters : ITriggerHandler handler
    */
    public static void run(ITriggerHandler handler, string triggerName){
        
        //Check if the trigger is disabled
        if (handler.IsDisabled()){
            return;
        }
        //Check trigger context from trigger operation type
        switch on Trigger.operationType {
            
            when BEFORE_INSERT {
                //Invoke before insert trigger handler
                handler.beforeInsert(trigger.new);
            }
            when AFTER_INSERT {
                //Invoke after insert trigger handler
                handler.afterInsert(trigger.newMap);
            }
            when BEFORE_UPDATE {
                //Invoke before update trigger handler
                handler.beforeUpdate(trigger.newMap,trigger.oldMap);
            }
            when AFTER_UPDATE {
                //Invoke after update trigger handler
                handler.afterUpdate(trigger.newMap,trigger.oldMap);
            }
            when BEFORE_DELETE {
                //Invoke before delete trigger handler
                handler.beforeDelete(trigger.oldMap);
            }
            when AFTER_DELETE {
                //Invoke after delete trigger handler
                handler.afterDelete(trigger.oldMap);
            }
            when AFTER_UNDELETE {
                //Invoke after undelete trigger handler
                handler.afterUnDelete(trigger.newMap);
            }
        }
        
    }
}