@isTest
public class CC_PavilionDealerInvoiceControllerTest {
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           psettingList = TestUtilityClass.createPavilionSettings();
                
           insert psettingList;
    }
    
    static testMethod void dealerInvoiceCtrlTest(){
        
        Account Acc = TestUtilityClass.createAccount();
        insert Acc;
        Contact Con =  TestUtilityClass.createContact(Acc.Id);
        insert Con;
        User Portaluser=TestUtilityClass.createUser('CC_PavilionCommunityUser',Con.Id);
        insert Portaluser;
        System.runAs (Portaluser) 
        {
           
            CC_PavilionTemplateController controller;
            CC_PavilionDealerInvoiceController objController1 = new CC_PavilionDealerInvoiceController(controller);
            objController1.getTypeOfCredit();
            objController1.getSalesRepresentative();
            CC_PavilionDealerInvoiceController.submitCoopInvoiceData('12345','Demo',Portaluser.Id,String.valueOf(system.today().format()),'e','f', '{ "invoiceItems" : [{ "quantity":2,"chargeCode":"10234","cost":2}]}');
            System.assertEquals(CC_PavilionDealerInvoiceController.submitCoopInvoiceData('a','b','c',String.valueOf(system.today().format()),'e','f', '{ "invoiceItems" :}') ,'Failure');
            //CC_PavilionDealerInvoiceController.sendNotificationEmail('test','testException');
            CC_PavilionDealerInvoiceController.submitCoopInvoiceData('a','b','c','','e','f', '{ "invoiceItems" : }');
       }
        
    }
}