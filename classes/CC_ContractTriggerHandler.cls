public with sharing class CC_ContractTriggerHandler {

    private static Id svcContractRecordTypeId = sObjectService.getRecordTypeId(Contract.sObjectType, 'Club Car Service Contract');
    private static final String financeMyTeamRole = 'Customer Financing Representative';

    @TestVisible
    private static Boolean getIsTriggerEnabled(){
        return CC_Contract_Settings__c.getOrgDefaults() != null ? CC_Contract_Settings__c.getOrgDefaults().CC_Contract_Trigger_Handler_Enabled__c : false;    
    }    
    
    public static void onBeforeInsert(List<Contract> contracts){
        
        if (getIsTriggerEnabled()){
        	lookupCustomerFinanceRep(contracts);
        }
    }

    public static void onBeforeDelete(List<Contract> contracts){
        
        if (getIsTriggerEnabled()){
        	validateDeletions(contracts);
        }
    }    

    private static void validateDeletions(List<Contract> contracts){
        
        CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();

        // Only enforce the service contract restrictions on users with the service contracts permission set (i.e. exclude admins and api accounts)
        Integer manageSvcContractsCheck = [Select Count() From PermissionSetAssignment Where AssigneeId = :UserInfo.getUserId() and PermissionSet.Name = 'CC_Service_Contract_Status_Edit'];

        if (manageSvcContractsCheck > 0){

            Boolean allowedStatusesDefined = String.isNotBlank(contractSettings.Allowed_Svc_Contract_Deletion_Statuses__c);

            for (Contract c : contracts){

                // Only allow deletions on club car service contracts that are in allowed statuses
                if (c.RecordTypeId == svcContractRecordTypeId){ 

                    if (allowedStatusesDefined && !contractSettings.Allowed_Svc_Contract_Deletion_Statuses__c.contains(c.CC_Contract_Status__c)){
                        c.addError('Only service contracts with the following statuses are allowed to be deleted: ' + contractSettings.Allowed_Svc_Contract_Deletion_Statuses__c);
                    }
                    else if (!allowedStatusesDefined){
                        c.addError('Service contracts are not allowed to be deleted.');
                    }                    
                }                
            }
        }
    }
    
    // Process new service contracts and try to find the associated customer finance rep under the account's My Team records
    private static void lookupCustomerFinanceRep(List<Contract> contracts){
        
        // Get a set of account Ids to work with
        Set<Id> accountIds = new Set<Id>();
        Contract[] contractsToProcess = new Contract[]{};
        
        for (Contract c : contracts){
            
            if (c.RecordTypeId == svcContractRecordTypeId && c.AccountId != null && c.CC_Customer_Finance_Rep__c == null){
                contractsToProcess.add(c);
                accountIds.add(c.AccountId);
            }
        }
        
        system.debug('contractsToProcess: ' + contractsToProcess.size());
        
        if (contractsToProcess.size() > 0){
            
            // Find the assigned finance rep on each account
			Map<Id, Map<String, Id>> accountUserRoleMap = CC_My_Team_Service.lookupUsersByAccountAndRole(new Set<String>{financeMyTeamRole}, accountIds);
            CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
            
            // Go through each contract and assign the finance user if found
            for (Contract c : contractsToProcess){
                
                system.debug('contract: ' + c);
                
                Map<String, Id> roleMap = accountUserRoleMap.get(c.AccountId);
                Id financeRepId = roleMap != null && roleMap.containsKey(financeMyTeamRole) ? roleMap.get(financeMyTeamRole) : null;
                
                system.debug('financeRepId to assign: ' + financeRepId);
                
                if (financeRepId != null){
                    c.CC_Customer_Finance_Rep__c = financeRepId;
                }
                else if (contractSettings != null && String.isNotBlank(contractSettings.CC_Default_Finance_Approver_ID__c)){
					c.CC_Customer_Finance_Rep__c = contractSettings.CC_Default_Finance_Approver_ID__c;
                }
            }
        }
    }
}