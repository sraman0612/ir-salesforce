@isTest
public with sharing class CC_KochUtilitiesTest {

    @TestSetup
    private static void createData(){

        Profile testProfile = [Select Id From Profile Where Name = 'CC_System Administrator'];
        
        User testUser = TestUtilityClass.createNonPortalUser(testProfile.Id);
        testUser.LastName = 'Test123456789';
        testUser.userName += 'abc123';
        insert testUser;

        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        CC_Order__c ord1 = TestUtilityClass.createCustomOrder(acc.id, 'OPEN', 'N');
        ord1.CC_MAPICS_Order_Key__c = '2158783';

        insert new CC_Order__c[]{ord1};
        
        CC_Order_Item__c orditem1 = TestUtilityClass.createOrderItem(ord1.Id);
        insert new CC_Order_Item__c[]{orditem1};               

        CC_Car_Item__c carItem1 = TestUtilityClass.createCarItem(orditem1.Id);
        carItem1.Car_Item_Key__c = '01_1_2158783_1_1';

        insert new CC_Car_Item__c[]{carItem1};        
    }

    @isTest
    private static void testUpdateCarItemKochFields(){

        CC_Car_Item__c carItem = [Select Id, Koch_Last_Update_Date__c, Koch_Scheduled_Pickup_Date__c, Koch_Actual_Pickup_Date__c, Koch_Scheduled_Delivery_Date__c,
                                    Koch_Estimated_Delivery_Date__c, Koch_Actual_Delivery_Date__c, Koch_Last_Check_Date__c, Koch_Load_Code__c
                                  From CC_Car_Item__c];

        system.assertEquals(null, carItem.Koch_Scheduled_Pickup_Date__c);
        system.assertEquals(null, carItem.Koch_Actual_Pickup_Date__c);
        system.assertEquals(null, carItem.Koch_Scheduled_Delivery_Date__c);
        system.assertEquals(null, carItem.Koch_Estimated_Delivery_Date__c);
        system.assertEquals(null, carItem.Koch_Actual_Delivery_Date__c);
        system.assertEquals(null, carItem.Koch_Last_Update_Date__c);
        system.assertEquals(null, carItem.Koch_Last_Check_Date__c);                                  
        system.assertEquals(null, carItem.Koch_Load_Code__c);                                  

        Test.startTest();

        User runningUser = [Select Id From User Where isActive = true and LastName = 'Test123456789' LIMIT 1];
  
        system.runAs(runningUser){

            CC_KochUtilities.updateCarItemKochFields(
                carItem,                                                         
                '10/6/2020 11:00:00 PM', 
                '10/7/2020 11:00:00 PM',
                '10/8/2020 11:00:00 PM', 
                '10/9/2020 11:00:00 PM', 
                '10/10/2020 11:00:00 PM', 
                '1.55',
                '10/11/2020 11:00:00 PM',
                '-1.5',
                'ABC'
            );

            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/6/2020 11:00:00 PM'), carItem.Koch_Last_Update_Date__c);
            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/7/2020 11:00:00 PM'), carItem.Koch_Scheduled_Pickup_Date__c);
            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/8/2020 11:00:00 PM'), carItem.Koch_Actual_Pickup_Date__c);
            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/9/2020 11:00:00 PM'), carItem.Koch_Scheduled_Delivery_Date__c);            
            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/10/2020 11:00:00 PM'), carItem.Koch_Estimated_Delivery_Date__c); 
            system.assertEquals(1.55, carItem.Koch_Remaining_Miles__c);                                                                
            system.assertEquals(CC_KochUtilities.getDateTimeFromKochData('10/11/2020 11:00:00 PM'), carItem.Koch_Actual_Delivery_Date__c);     
            system.assertEquals(-1.5, carItem.Koch_Time_Zone_Offset__c);   
            system.assertEquals('ABC', carItem.Koch_Load_Code__c); 
            system.assert(carItem.Koch_Last_Check_Date__c != null && carItem.Koch_Last_Check_Date__c <= DateTime.now()); 
        }

        Test.stopTest();    
    }

    @isTest
    private static void testGetDateTimeFromKochData(){
                             
        Test.startTest();

        User runningUser = [Select Id From User Where isActive = true and LastName = 'Test123456789' LIMIT 1];
  
        system.runAs(runningUser){

            DateTime dt1 = CC_KochUtilities.getDateTimeFromKochData('10/6/2020 11:00:00 PM');
            DateTime dt2 = CC_KochUtilities.getDateTimeFromKochData(CC_KochUtilities.EMPTY_VALUE);
            DateTime dt3 = CC_KochUtilities.getDateTimeFromKochData(null);

            DateTime dt1Local = DateTime.parse('10/6/2020 11:00 PM');
            system.assertEquals(DateTime.newInstanceGMT(dt1Local.date(), dt1Local.time()), dt1); 
            system.assertEquals(null, dt2); 
            system.assertEquals(null, dt3); 
        }

        Test.stopTest();    
    }   
    
    @isTest
    private static void testGetCarItemUniqueKeyPrefix(){
                             
        Test.startTest();

        User runningUser = [Select Id From User Where isActive = true and LastName = 'Test123456789' LIMIT 1];
  
        system.runAs(runningUser){
            system.assertEquals('01_1_', CC_KochUtilities.getCarItemUniqueKeyPrefix());
        }
    }

    @isTest
    private static void testGetKochCarItemUniqueKey(){
                             
        Test.startTest();

        User runningUser = [Select Id From User Where isActive = true and LastName = 'Test123456789' LIMIT 1];
  
        system.runAs(runningUser){

            String key1 = CC_KochUtilities.getKochCarItemUniqueKey('1234567-1-1');
            String key2 = CC_KochUtilities.getKochCarItemUniqueKey('1234567--');
            String key3 = CC_KochUtilities.getKochCarItemUniqueKey(null);

            system.assertEquals(CC_KochUtilities.getCarItemUniqueKeyPrefix() + '1234567_1_1', key1);
            system.assertEquals(CC_KochUtilities.getCarItemUniqueKeyPrefix() + '1234567_1_1', key2);
            system.assertEquals(null, key3);
        }
    }    

    @isTest
    private static void testCleanNearestGPSLocation(){
                             
        Test.startTest();

        User runningUser = [Select Id From User Where isActive = true and LastName = 'Test123456789' LIMIT 1];
  
        system.runAs(runningUser){

            String locationBase = '15 miles southeast of Detroit';
            String location1 = CC_KochUtilities.cleanNearestGPSLocation(locationBase + '[IGN:X]');
            String location2 = CC_KochUtilities.cleanNearestGPSLocation(locationBase + '[IGN:N]');
            String location3 = CC_KochUtilities.cleanNearestGPSLocation(locationBase + '[IGN:Y]');
            String location4 = CC_KochUtilities.cleanNearestGPSLocation(null);

            system.assertEquals(locationBase, location1);
            system.assertEquals(locationBase, location2);
            system.assertEquals(locationBase, location3);
            system.assertEquals(null, location4);
        }
    }        
}