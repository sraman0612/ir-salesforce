public class CC_STL_Confirmation_CONTRL {

    public CC_Short_Term_Lease__c leaseData{get;set;}
    public string lease_Id{get;set;}
    public List<CC_STL_Car_Info__c> CarsOnLease{get;set;}

    public CC_STL_Confirmation_CONTRL(ApexPages.StandardController controller){
    	
        CarsOnLease = new List<CC_STL_Car_Info__c>();
        lease_Id =  ApexPages.currentPage().getParameters().get('id');
        leaseData = [select id,Name,Master_Lease__r.Name,Master_Lease__r.Customer__r.Name, 
                            Master_Lease__r.Customer__r.Address_1__c,Master_Lease__r.Customer__r.Address_2__c,Master_Lease__r.Customer__r.Address_3__c,
                            Master_Lease__r.Customer__r.CC_City__c,Master_Lease__r.Customer__r.CC_MAPICS_City__c,
                            CreatedDate, Billing_Contact__r.Name, Billing_Contact__r.Phone,Billing_Contact__r.Email,
                            Master_Lease__r.Bill_To_Name__c, Master_Lease__r.Bill_To_Address__c, Master_Lease__r.Bill_To_City__c, Master_Lease__r.Bill_To_State__c, Master_Lease__r.Bill_To_Zip__c, Master_Lease__r.Bill_To_Country__c,
                            Shipping_Address__c, Shipping_City__c, Shipping_State__c, Shipping_Zip__c, Shipping_Country__c, Beginning_Date__c, Shipping_Contact__r.Name,Shipping_Contact__r.Phone,Shipping_Contact__r.Email,
                            Ending_Date__c, Total_Car_Cost__c, Invoice_Terms__c,Remaining_Payments__c, 
                            Service_Contract_Fee__c, Options_on_Cars__c,Liability_End_Date__c,
                            Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Total_Car_Internal_Cost__c, Total_Number_of_Cars__c,
                            Number_of_Days__c, Total_Quote_Price__c, Adj_Cost_Before_Transportation__c,
                            Confirmation_Subtotal__c, Total_Lease_Price__c, Estimated_Tax__c, Standard_Monthly_Billing__c, 
                            Num_of_Payments__c, First_Bill__c, Shipping_To_Name__c, Additional_Discount__c , One_time_Charge__c
                       from CC_Short_Term_Lease__c 
                      where Id = :lease_Id 
                      Limit 1];
                      
        for(CC_STL_Car_Info__c list_of_cars : [select id,Quantity__c, Description__c, Base_Cost__c, Total_Cost__c, Per_Car_Cost__c, Internal_Cost__c, Part_Number__c
                                                from CC_STL_Car_Info__c 
                                               Where Short_Term_Lease__r.id = :lease_Id and Quantity__c > 0
                                            Order by CreatedDate Asc]){
            CarsOnLease.add(list_of_cars);                                       
        }
    }
    
    public PageReference buildAndSend(){
    	
        lease_Id =  ApexPages.currentPage().getParameters().get('id');    
        PageReference pdf = Page.CC_STL_Confirmation_PDF;
        pdf.getParameters().put('id',lease_Id);
        Attachment attach = new Attachment();
        Blob body;
        
        try{
            body = pdf.getContent();
        }
        catch(VisualforceException e){
            body = Blob.valueOf('Exception is thrown while creating Pdf Page');
        }
        
        ContentVersion contVer = new ContentVersion();
        contVer.Title=leaseData.Name+' Confirmation '+System.now();
        contVer.pathOnClient =leaseData.Name+' '+System.now()+'.pdf';
        contVer.versionData = body;
        
        try{
            insert contVer;
        }
        catch(Exception e){
            System.debug('Exception due to :'+e);
        }
        
        contVer = [select id,ContentDocumentId,versionData from ContentVersion where Id=:contVer.id limit 1];
        ContentDocumentLink contDocLink = new ContentDocumentLink();
        
        if(contVer!=null){
        	
            contDocLink.ContentDocumentId = contVer.ContentDocumentId;
            contDocLink.LinkedEntityId = leaseData.id;
            contDocLink.ShareType = 'V';
            contDocLink.Visibility = 'AllUsers';
            
            try{
            	insert contDocLink;
            }
            catch(Exception e){
                System.debug('Exception due to :'+e);
            }
        }
            
        attach.Body = body;
        attach.Name = leaseData.Name+'Confirmation'+System.now()+'.pdf';
        attach.IsPrivate = false;
        attach.ParentId = lease_Id;
        insert attach;
        
        CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
        
        return new PageReference('/apex/echosign_dev1__AgreementTemplateProcess?masterid='+lease_Id+'&templateId='+hcs.STL_CONF_ESIGN_TEMPLATEID__c);
    }
}