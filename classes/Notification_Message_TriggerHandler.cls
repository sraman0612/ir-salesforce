public with sharing class Notification_Message_TriggerHandler {

    private static DateTime runTime = DateTime.now();
    Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
    private Map<Id, Notification_Message__c> oldMap, newMap;

    public Notification_Message_TriggerHandler(Map<Id, Notification_Message__c> oldMap, Map<Id, Notification_Message__c> newMap){
        this.oldMap = oldMap;
        this.newMap = newMap;
    }

    public void onAfterInsert(){

        if (notificationSettings.Enable_Real_Time_Message_Delivery__c){
            deliverRealTimeMessages();
        }
    }

    private void deliverRealTimeMessages(){

        Notification_Message__c[] messagesInScope = new Notification_Message__c[]{};

        for (Notification_Message__c msg : newMap.values()){
            
            if (msg.Delivery_Frequency__c == NotificationService.REAL_TIME){
                messagesInScope.add(msg);
            }
        }

        system.debug('real-time messages to send: ' + messagesInScope.size());

        if (messagesInScope.size() > 0){

            // Requery to get all related object fields needed
            messagesInScope = Database.Query(NotificationDeliveryService.getNotificationDeliveryQuery(null, messagesInScope));
            NotificationDeliveryService.deliverNotificationMessages(messagesInScope);
        }
    }
}