// Created on 24/02/2020
// Harish updated conditions
// CustomerContactPrimaryCheck
// This Class prevents multiple primary Customer Contacts on one Account 
public class CustomerContactPrimaryCheck {
    Public static void preventCreatePrimaryContactOnInsert(List<Contact> contactList){
        Set<Id> accountIdSet = New Set<id>();
        Map<Id, Contact> accountIdContactMap = new Map<Id, Contact>();
        
        for(Contact contactObject : contactList){
            //Preparing the Acocuntid set if the newly added contact is Customer Contact and made primary add it to accountIdSet
            if(contactObject.Primary__c==true && contactObject.AccountId !=null && (contactObject.RecordTypeName__c=='Customer_Contact' || contactObject.RecordTypeName__c=='CTS_AIRD_Contact')){
                accountIdSet.add(contactObject.AccountId);
                system.debug('Account Id set in Insert loop is'+accountIdSet); 
                system.debug('Account Id set is'+accountIdSet); 
                system.debug('Account Id size is'+accountIdSet.size());
            }
        }
        
        if(accountIdSet!=null && accountIdSet.size()>0){
            //Preparation of Map to hold AccountId and all its contact
            system.debug('Inside if loop');
            for(Contact contObj:[select id , AccountId ,RecordTypeName__c from Contact WHERE RecordTypeName__c IN ('Customer_Contact' , 'CTS_AIRD_Contact') and Primary__c =true and AccountId in:accountIdSet]){
                accountIdContactMap.put(contObj.AccountId , contObj);
                system.debug('Account and Contact map is '+accountIdContactMap); 
            }
        }
        
        //Validation using the map formed above
        for(Contact contactObject:contactList){
            if(contactObject.Primary__c==true && contactObject.AccountId!=null && (contactObject.RecordTypeName__c=='Customer_Contact' || contactObject.RecordTypeName__c=='CTS_AIRD_Contact') && accountIdContactMap.get(contactObject.AccountId)!=null){
                system.debug('This is '+accountIdContactMap.get(contactObject.AccountId)); 
                contactObject.addError(system.Label.Only_One_Primary_Contact_allowed);
            }
        }
        
    }
    Public static void preventPrimaryContactOnUpdate(Map<Id, Contact> contactNewMap, Map<id , Contact> contactOldMap){
        Set<Id> accountIdSet = New Set<Id>();
        Map<Id, Contact> accountIdContactMap = New Map<Id, Contact>();
        
        for(Contact contactObject :contactNewMap.values()){
            if(contactNewMap.get(contactObject.Id).Primary__c!=contactOldMap.get(contactObject.Id).Primary__c && contactNewMap.get(contactObject.Id).Primary__c==true && contactNewMap.get(contactObject.Id).AccountId!=null && (contactNewMap.get(contactObject.Id).RecordTypeName__c=='Customer_Contact' || contactNewMap.get(contactObject.Id).RecordTypeName__c=='CTS_AIRD_Contact')){
                accountIdSet.add(contactNewMap.get(contactObject.Id).AccountId);
                system.debug('Account Id set in Update loop is  '+accountIdSet); 
            }
        }
        
        if(accountIdSet!=null && accountIdSet.size()>0){
            //Preparation of Map to hold AccountId and all its contact
            for(Contact contObj:[select id , AccountId ,RecordTypeName__c from Contact WHERE RecordTypeName__c IN ('Customer_Contact' , 'CTS_AIRD_Contact') and Primary__c =true and AccountId in:accountIdSet]){
                accountIdContactMap.put(contObj.AccountId , contObj);
                system.debug('Account and Contact map is '+accountIdContactMap); 
            }
        }
        
        for(Contact contactObject :contactNewMap.values()){
            if(contactNewMap.get(contactObject.Id).Primary__c!=contactOldMap.get(contactObject.Id).Primary__c && contactNewMap.get(contactObject.Id).Primary__c==true && contactNewMap.get(contactObject.Id).AccountId!=null && (contactNewMap.get(contactObject.Id).RecordTypeName__c=='Customer_Contact'|| contactNewMap.get(contactObject.Id).RecordTypeName__c=='CTS_AIRD_Contact') && accountIdContactMap.get(contactNewMap.get(contactObject.Id).AccountId)!=null){
                system.debug('This is '+accountIdContactMap.get(contactObject.AccountId)); 
                contactObject.addError(system.Label.Only_One_Primary_Contact_allowed);
            }
        }
    }
    
    Public static void updateFirstContactAsPrimaryContact(List<Contact> contactList){
        Set<Id> accountIdSet = New Set<id>();
        Map<Id, Contact> accountIdContactMap = new Map<Id, Contact>();
        
        for(Contact contactObject : contactList){
            //Preparing the Acocuntid set if the newly added contact is Customer Contact and made primary add it to accountIdSet
            if(contactObject.Primary__c==false && contactObject.AccountId !=null && (contactObject.RecordTypeName__c=='Customer_Contact' || contactObject.RecordTypeName__c=='CTS_AIRD_Contact')){
                accountIdSet.add(contactObject.AccountId);
                system.debug('Account Id set in Insert loop is'+accountIdSet); 
                system.debug('Account Id set is'+accountIdSet); 
                system.debug('Account Id size is'+accountIdSet.size());
            }
        }
        
        if(accountIdSet!=null && accountIdSet.size()>0){
            //Preparation of Map to hold AccountId and all its contact
            system.debug('Inside if loop');
            for(Contact contObj:[select id , AccountId ,RecordTypeName__c from Contact WHERE RecordTypeName__c IN ('Customer_Contact' , 'CTS_AIRD_Contact') and Primary__c =true and AccountId in:accountIdSet]){
                accountIdContactMap.put(contObj.AccountId , contObj);
                system.debug('Account and Contact map is '+accountIdContactMap); 
            }
        }
        
        //Validation using the map formed above
        for(Contact contactObject:contactList){
            if(contactObject.Primary__c==false && contactObject.AccountId!=null && (contactObject.RecordTypeName__c=='Customer_Contact' || contactObject.RecordTypeName__c=='CTS_AIRD_Contact') && accountIdContactMap.get(contactObject.AccountId)==null){
                system.debug('This is '+accountIdContactMap.get(contactObject.AccountId)); 
                contactObject.Primary__c = true;
            }
        }
        
    }
}