/**
* @createdBy   Himaja Siddhanthi
* @date         08/01/2016
* @description  Test Class for CC_PavilionPartsOrderController
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest 
public class CC_PavilionPartsOrderController_Test {
    static PavilionSettings__c pavillionCommunityProfileId;
    static PavilionSettings__c  pavilionPublicGroupID;
    static PavilionSettings__c pavillionCommunityProfileId2;
    static PavilionSettings__c  pavilionPublicGroupID2;
    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Account account;
    static User testUser2;
    static Contact contact2;
    static Profile portalProfile2;
    static Account account2;
    static Product2 product;
    static Product2 product2;
    static Product2 product3;
    static Product2 nonStandard;
    static Product2 discontinued;
    static Product2 product4;
    static Product2 product5;
    static Pricebook2 plistPricebook;
    static Pricebook2 customerPricebook;
    
    static {
        
        TestDataUtility testData = new TestDataUtility();
        testUser = createPartnerUser(1);
        Profile  testProfile = [SELECT Id, Name FROM Profile WHERE Name='System Administrator'];
        testUser2 = new User (LastName = 'CreateTest', Email ='CreateTest@gmail.com', 
                             CommunityNickname = 'CreateTest', Alias = 'crttst', 
                             Username = 'crttst@gmail.com',             
                             TimeZoneSidKey  = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US', ProfileId = testProfile.Id,
                             EmailEncodingKey = 'UTF-8',CC_Pavilion_Navigation_Profile__c = 'TestProf', CC_View_Parts_Order_Invoice_Pricing__c = true,
                             Pavilion_Tiles__c = ';Custom Solutions Project Request;CustomerVIEW;Marketing Events;PreApproved Requests;Transportation Requests;Marketing_MyCo-OpAccount;Marketing Hole-In-One');          
        insert testUser2;
        system.runAs(testUser2){
            portalProfile = [SELECT Id FROM Profile WHERE Name LIKE 'CC_PavilionCommunityUser' Limit 1];
            // populate custom settings before user creation
            pavillionCommunityProfileId = new PavilionSettings__c(Name = 'PavillionCommunityProfileId',Value__c = portalProfile.Id);
            insert pavillionCommunityProfileId;
            pavilionPublicGroupID = new PavilionSettings__c(Name = 'PavilionPublicGroupID',Value__c = portalProfile.Id);
            insert pavilionPublicGroupID;
            //AccountTeamMember for Account share error
            AccountTeamMember atm = TestUtilityClass.createATM(account.Id,testUser.Id);
            insert atm;
            AccountShare accShare = TestUtilityClass.createAccShare(account.Id,testUser.Id);
            insert accShare;
            product = new Product2(
                Name = 'STD LIGHT DUTY 4PSG KIT #1 BEG',
                ProductCode = 'AM10708',
                RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product;
            product2 = new Product2(
                Name = 'Beer',
                ProductCode = 'AM19999',
                ERP_Item_Number__c='AM19999',
                RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product2;
            product3 = new Product2(
                Name = 'Laptop',
                ProductCode = 'AM10003',
                RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product3;
            product4 = new Product2(
                Name = 'Mouse',
                ProductCode = 'AM19993',
                RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product4;
            product5 = new Product2(
                Name = 'TV',
                ProductCode = 'AM10444',
                RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
            );
            insert product5;
            nonStandard = new Product2(Name = 'NON STANDARD',ProductCode = 'NON STANDARD');
            insert nonStandard;
            discontinued = new Product2(Name = 'DISCONTINUED',ProductCode = 'DISCONTINUED');
            insert discontinued;
            plistPricebook = new Pricebook2(Name = 'PLIST',PBNAME__c ='PLIST',isActive=TRUE);
            insert plistPricebook;
            customerPricebook = new Pricebook2(Name = 'PCOPI');
            insert customerPricebook;
        }     
    }

    public static  user createPartnerUser(integer i){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
        Database.insert(portalAccountOwner1);
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
            //creating state country custom setting data
            TestDataUtility.createStateCountries();
            //creating sales rep here
            TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
            salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
            insert salesRepacc;
            //Create account
            account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,CC_Global_Region__c = 'Test',
                                                BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566',
                                  CC_Billing_County__c = county.id,BillingState = 'GA',CC_Shipping_Billing_Address_Same__c=true, CC_Sales_Rep__c = salesRepacc.Id);
            Database.insert(account);
            //Create contact
            contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = account.Id,
                                          RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            Database.insert(contact);
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile where name ='CC_PavilionCommunityUser' LIMIT 1];
            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+i,
                                 ContactId = contact.Id,ProfileId = portalProfile.Id,
                                 Alias = 'test123',Email = 'test12345@test.ingersollrand.com',
                                 EmailEncodingKey = 'UTF-8',LastName = 'McTesty',
                                 CommunityNickname = 'test12345',TimeZoneSidKey = 'America/New_York',country='USA',
                                 LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US');
            Database.insert(user1);
        }
        return user1; 
    }

    public static testmethod void getPartsCalendarDatestest(){
        String [] strLst = CC_PavilionPartsOrderController.getPartsCalendarDates(system.today(), system.today()+10);
    }

    public static testmethod void processSelectedtest() {
        system.runAs(testuser){
            CC_PavilionPartsOrderController.uploadTaxCertificate(account.id, 'new file', Blob.valueOf('Unit Test Attachment Body'), 'text');     
            CC_PavilionPartsOrderController.createCart(account.id, 'new cart');        
            PriceBookEntry pricebookentry4 = new PriceBookEntry(
                Pricebook2Id = plistPricebook.Id,
                Product2Id = product5.Id,
                UnitPrice = 10,
                CC_CAD_Unit_Price__c = 10,
                CC_Quantity_Discount__c = 1,
                CC_Quantity_Price__c = 1,
                CC_CAD_Quantity_Price__c = 5,
                IsActive=TRUE, 
                UseStandardPrice=FALSE
            );
            insert pricebookentry4;
            PriceBookEntry pricebookentry5 = new PriceBookEntry(
                Pricebook2Id = plistPricebook.Id,
                Product2Id = product2.Id,
                UnitPrice = 10,
                CC_CAD_Unit_Price__c = 10,
                CC_Quantity_Discount__c = 1,
                CC_Quantity_Price__c = 1,
                CC_CAD_Quantity_Price__c = 5,
                IsActive=TRUE, 
                UseStandardPrice=FALSE
            );
            insert pricebookentry5;             
            system.assertEquals('AM19999',[SELECT Product2.ERP_Item_Number__c from PricebookEntry WHERE Id = :pricebookEntry5.Id].Product2.ERP_Item_Number__c);
            CC_Order__c order = new CC_Order__c(
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),CC_Account__c=account.Id,isSystemCreated__c=TRUE
            );
            insert order;           
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_Order_Item__c item = new CC_Order_Item__c(
                RecordTypeId = Schema.SObjectType.CC_Order_Item__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),
                CC_Quantity__c = 1,
                CC_UnitPrice__c = 10,
                CC_TotalPrice__c = 10,
                CC_Product_Code__c = 'AM10444',
                Order__c = order.Id
            );            
            insert item;            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();            
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));            
            String addtocartstring = '["AM10708:1","AM10444:1"]';
            ApexPages.currentPage().getParameters().put('selectedids', addtocartstring);  
            CC_PavilionPartsOrderController partsOrderController = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);
            CC_PavilionPartsOrderController.getShoppingCarts(account.id);
            CC_PavilionPartsOrderController.getCartItems(order.id);
            CC_PavilionPartsOrderController.processSelected(addtocartstring, false, 'pb', account.id, 'cusnum21', order.Id);
            CC_PavilionPartsOrderController.processSelected('["AM104443:3"]', TRUE, 'pb', account.id, 'cusnum21', order.Id);
            CC_PavilionPartsOrderController.processSelected('["AM19999:1"]', false, 'pb', account.id, 'cusnum21', order.Id);          
            CC_PavilionPartsOrderController.deleteCart(order.id);
        }
    }

    @isTest
    public static void CC_PavilionPartsOrderConstructor_Test() {
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));
            
            CC_PavilionPartsOrderController partsOrderController = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);

        }
    }
    
    // test constructor invalid id
    @isTest
    public static void CC_PavilionPartsOrderConstructor_Test_InvalidId() {
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));
            
            ApexPages.currentPage().getParameters().put('id', '');
            
            CC_PavilionPartsOrderController partsOrderController = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);
        }
    }
    
    // test constructor valid cid
    @isTest
    public static void CC_PavilionPartsOrderConstructor_Test_InvalidCId() {
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));
            
            ApexPages.currentPage().getParameters().put('cid', contact.Id);
            
            CC_PavilionPartsOrderController partsOrderController = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);
        }
    }


    
    // test constructor invalid contact or account
    @isTest
    public static void CC_PavilionPartsOrderConstructor_Test_InvalidAccountOrContact() {
        system.runAs(testuser2){
        PriceBookEntry pricebookentry4 = new PriceBookEntry(
            Pricebook2Id = plistPricebook.Id,
            Product2Id = product5.Id,
            UnitPrice = 10,
            CC_CAD_Unit_Price__c = 10,
            CC_Quantity_Discount__c = 1,
            CC_Quantity_Price__c = 10,
            CC_CAD_Quantity_Price__c = 5,
            IsActive=TRUE, 
            UseStandardPrice=FALSE
        );
        insert pricebookentry4;
             
        
        CC_Order__c order = new CC_Order__c(
            RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),CC_Account__c=account.Id,isSystemCreated__c=TRUE
        );
        insert order;
        
        CC_Order_Item__c item = new CC_Order_Item__c(
            RecordTypeId = Schema.SObjectType.CC_Order_Item__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),
            CC_Quantity__c = 2,
            CC_UnitPrice__c = 10,
            CC_TotalPrice__c = 10,
            CC_Product_Code__c = 'AM10444',
            Order__c = order.Id
        );
        
        insert item;
        
        CC_Order_Item__c item2 = new CC_Order_Item__c(
            RecordTypeId = Schema.SObjectType.CC_Order_Item__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),
            CC_Quantity__c = 1,
            CC_UnitPrice__c = 10,
            CC_TotalPrice__c = 10,
            CC_Product_Code__c = 'AM10444',
            Order__c = order.Id
        );
        
        insert item2;
        
        CC_PavilionTemplateController cc_PavilionTemplateController; // = new CC_PavilionTemplateController()
        
        Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));
        
        ApexPages.currentPage().getParameters().put('cid', '');
        
        CC_PavilionPartsOrderController partsOrderController = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);
        }      
    }
    
    //test filter on inactive price book entries
    @isTest
    public static void CC_PavilionPartsOrderSearchFilter_Test_IsActive() {
        System.runAs (testUser) {            
            Product2 activeTestProduct = new Product2(
            Name = 'Test_Product2',
            ProductCode = '000005555566666',
            RecordTypeId=Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId()
        	);           
            insert activeTestProduct;            
                
            PricebookEntry activePbe = new PricebookEntry(pricebook2id=plistPricebook.id, product2id=activeTestProduct.id,unitprice=1.0, isActive=false);
			insert activePbe;
            
            Map<String,String> activeControlMap = new Map<String,String>();
            Map<String,String> inActiveControlMap = new Map<String,String>();
            activeControlMap.put('000005555566666', ' ::Test_Product2');
            
            //check search failure for inactive pricebookentry
            Map<String,String> testMapActive = CC_PavilionPartsOrderController.getItemSearchList(activeTestProduct.ProductCode);            
            System.assertEquals(testMapActive, inActiveControlMap);
            
            activePbe.IsActive = true;
            update activePbe;
            
            //check search success for active pricebookentry
            Map<String,String> testMapInactive = CC_PavilionPartsOrderController.getItemSearchList(activeTestProduct.ProductCode);            
            System.assertEquals(testMapInactive, activeControlMap);
        }
    }

    // test processSelected cad plist discontinued
    @isTest
    public static void CC_PavilionPartsOrderProcessSelected_PLIST_Test_Discontinued() {
        System.runAs (testUser) {
            Product2 discontd = new Product2(
                Name = 'DISCONTINUED',
                ProductCode = 'DISCONTINUED'
            );
            insert discontd;
            
            Database.insert(new CC_Product_Rule__c[]{
                new CC_Product_Rule__c(
                    CC_Type__c = 'Message',
                    CC_Obsolete_Item__c = product.Id,
                    CC_Message__c = 'FRAMES REQUIRE AUTHORIZATION PRIOR 2 PURCHASE',
                    CC_Last_Change_Date__c = Date.newInstance(2000, 1, 1),
                    CC_Sequence__c = 1,
                    CC_Web_Display__c = true
                ),
                    new CC_Product_Rule__c(
                        CC_Type__c = 'Message',
                        CC_Obsolete_Item__c = product.Id,
                        CC_Message__c = 'AND PREVIOUS DEVLIVERY REPORT IS CHECKED',
                        CC_Last_Change_Date__c = Date.newInstance(2000, 1, 1),
                        CC_Sequence__c = 1,
                        CC_Web_Display__c = true
                    ),
                    new CC_Product_Rule__c(
                        CC_Type__c = 'Replace',
                        CC_Substitute_Effective_Date__c = Date.newInstance(2016, 1, 1),
                        CC_Last_Change_Date__c = Date.newInstance(2000, 1, 1),
                        CC_Sequence__c = 2,
                        CC_Obsolete_Item__c = product.Id,
                        CC_Replacement_Item__c = product2.Id
                    ),
                    new CC_Product_Rule__c(
                        CC_Type__c = 'Replace',
                        CC_Substitute_Effective_Date__c = Date.newInstance(2016, 1, 1),
                        CC_Last_Change_Date__c = Date.newInstance(2000, 1, 1),
                        CC_Sequence__c = 2,
                        CC_Obsolete_Item__c = product3.Id,
                        CC_Replacement_Item__c = nonStandard.Id
                    ),
                    new CC_Product_Rule__c(
                        CC_Type__c = 'Replace',
                        CC_Substitute_Effective_Date__c = Date.newInstance(2016, 1, 1),
                        CC_Last_Change_Date__c = Date.newInstance(2000, 1, 1),
                        CC_Sequence__c = 2,
                        CC_Obsolete_Item__c = product2.Id,
                        CC_Replacement_Item__c = discontd.Id
                    )
                    });
            
            PriceBookEntry pricebookentry = new PriceBookEntry(
                Pricebook2Id = plistPricebook.Id,
                Product2Id = product2.Id,
                UnitPrice = 10,
                CC_CAD_Unit_Price__c = 10,
                CC_Quantity_Discount__c = 1,
                CC_CAD_Quantity_Price__c = 5,CC_Quantity_Price__c=10,
                IsActive=TRUE, 
                UseStandardPrice=FALSE
            );
            insert pricebookentry;
            PriceBookEntry newpricebookentry = new PriceBookEntry(
                Pricebook2Id = customerPricebook.Id,
                Product2Id = product2.Id,
                UnitPrice = 20,
                CC_CAD_Unit_Price__c = 20,
                CC_Quantity_Discount__c = 0,
                CC_CAD_Quantity_Price__c = 4,CC_Quantity_Price__c=11,
                IsActive=TRUE, 
                UseStandardPrice=FALSE
            );
            insert newpricebookentry;
            Map<String, PricebookEntry> pricebookEntryNew = new Map<String, PricebookEntry>();
            pricebookEntryNew.put('new pricebook', pricebookentry);
            pricebookEntryNew.put('another pricebook', newpricebookentry);
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrder'));
            
            // quick order input
            String addtocartstring = '[{"code":"AM10708","quantity":"1"},{"code":"AM19999","quantity":"1"},{"code":"NON STANDARD","quantity":"1"},{"code":"DISCONTINUED","quantity":"1"}]';
            ApexPages.currentPage().getParameters().put('addtocartstring', addtocartstring);
            
            CC_Order__c order = new CC_Order__c(
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),CC_Account__c=account.Id,isSystemCreated__c=TRUE
            );
            insert order;
             CC_Order_Item__c item = new CC_Order_Item__c(
                RecordTypeId = Schema.SObjectType.CC_Order_Item__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId(),
                CC_Quantity__c = 2,
                CC_UnitPrice__c = 10,
                CC_TotalPrice__c = 10,
                CC_Product_Code__c = 'AM19999',
                Order__c = order.Id
            );
            
            insert item; 
            
            List<CC_Parts_Cross_Reference__c> crosrefList = new List<CC_Parts_Cross_Reference__c>();
            crosrefList.add(new CC_Parts_Cross_Reference__c(Account__c=account.id,Name='{code=AM10708, quantity=1}',CC_Club_Car_Item__c=product.id));
            crosrefList.add(new CC_Parts_Cross_Reference__c(Account__c=account.id,Name='{code=AM19999, quantity=1}',CC_Club_Car_Item__c=product2.id));
            crosrefList.add(new CC_Parts_Cross_Reference__c(Account__c=account.id,Name='{code=NON STANDARD, quantity=1}',CC_Club_Car_Item__c=nonStandard.id));
            crosrefList.add(new CC_Parts_Cross_Reference__c(Account__c=account.id,Name='{code=DISCONTINUED, quantity=1}',CC_Club_Car_Item__c=discontinued.id));
            insert crosrefList;
            ApexPages.currentPage().getParameters().put('id', order.Id);
            CC_PavilionPartsOrderController cc_PavilionPartsOrder = new CC_PavilionPartsOrderController(cc_PavilionTemplateController);
            CC_PavilionPartsOrderController.getItemSearchList(account.id);
            CC_PavilionPartsOrderController.CADPricing=true;         
            CC_PavilionPartsOrderController.createItemforUpsert(item,pricebookEntryNew,'new pricebook');
            CC_PavilionPartsOrderController.CADPricing=false;         
            CC_PavilionPartsOrderController.createItemforUpsert(item,pricebookEntryNew,'new pricebook');
              CC_PavilionPartsOrderController.createItemforUpsert(item,pricebookEntryNew,'another pricebook');
                CC_PavilionPartsOrderController.deleteItem(item.Id);
                CC_PavilionPartsOrderController.processSelected(addtocartstring, false, 'pb', account.id, 'cusnum21', item.id);

        }
    }  
}