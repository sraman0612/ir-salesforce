public with sharing class CTSCompTechEmailService implements Messaging.InboundEmailHandler {    
    // Email Service for the CTS Compressor Technology Order Center.
    // 
    // The purpose of the CTSCompTechEmailService Apex class is to process email messages sent from inContact with voice mail attachments.
    //
    // inContact records a customer's voice message and emails the voice message as a .wav file attachment; along with the 'Caller Id'
    // embedded in the body of the email to Salesforce.  The email is forwarded to a Saleforce email address that is associated with 
    // a Salesforce 'Email Service'; which is associated with the 'CTSCompTechEmailService' Apex class.
    // 
    // The 'CTSCompTechEmailService' Apex class will insert a case into Salesforce, if the voice message attached to the email
    // is greater that seven seconds in length (.wav file attachment is greater than 60,200 bytes).  The 'CTSCompTechEmailService' 
    // Apex class will then retrieve the 'Caller ID' from the body of the email and then query 'Contacts' for that phone number.  If
    // a contact is found, it will be associated with the case.

    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelop) {
        
        // Create an InboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

        // Parse email body in order to retrieve 'Caller Id'
        String [] emailBody = email.htmlBody.split('\n',0);
 
        Contact contact;
 
        try {
            // Retrieve Caller Id
            String callerId = emailBody[5].substring(13,23);
 
            // Query contact by phone to determine if contact exists
            If ([select COUNT() from Contact where Phone =: callerId] !=0) {
                contact = [select Id from Contact where Phone =: callerId][0];
            }
            else {
                contact = new Contact();
            }

            // Build case
            buildCase(email, contact);
                
        }           
        catch(Exception e) {
            // Set the success value of result to 'FALSE'
            result.success = false;
 
            // Set message
            result.message = 'Failed';
        }
 
        // Return result
        return result;
    }

    private Messaging.InboundEmailResult buildCase(Messaging.InboundEmail email, Contact contact) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

        Case newCase;

        // Retrieve binary attachments
        List<Attachment> binAttachments = getBinAttachments(email);

        // Create case if email attachment is longer than seven seconds - file length > 60,200 bytes

        If(getMaxAttachSize(binAttachments) > 60200) {

            // Create new case
            newCase = new Case();

            if(contact.id <> null) {
                newCase.contactId = contact.Id;
            }
            // Set Case 'Case Owner'
            newCase.OwnerId = [SELECT DeveloperName, Id FROM Group where DeveloperName='ITSA_Retail_OM_Queue' Limit 1].Id;

            // Set Case 'Case Record Type'
            newCase.RecordTypeId = [Select Id, SobjectType, Name from RecordType where SobjectType = 'Case' and Name = 'CTS OM Americas' Limit 1].Id;

            // Set Case 'Type'
            newCase.type = 'Question';

            // Set Case 'Origin'
            newCase.Origin = 'Phone';

            // Set Case 'subject'
            newCase.subject = 'Compressor OM Missed Contact ';
 
            // Set Case 'description'
            If(email.htmlBody != Null && email.htmlBody != '') {

                // Remove HTML markup
                String emailBody = email.htmlBody.replaceAll('<P>','').replaceAll('</P>','');
                newCase.description = emailBody;
            }

            // Insert new case
            if(Schema.sObjectType.Case.isCreateable()) {
                insert newCase;
            }
 
            // Set parent id in attachments
            SetAttachParent(binAttachments, newCase.id);
                
            // Insert attachments
            if(binAttachments != null) {
                if(Schema.sObjectType.Attachment.isCreateable()) {
                    insert binAttachments;
                }        
            } 
        } 
             
        // Set the success value of result to 'TRUE'
        result.success = true;

        return result;

    }
    
    private List<Attachment> getBinAttachments(Messaging.InboundEmail email) {
        List<Attachment> binAttachments = new List<Attachment>();

        // Retrieve binary attachments
        if(email.binaryAttachments != null) {
            for(Messaging.Inboundemail.BinaryAttachment bAttachment : email.BinaryAttachments) {
                Attachment attachment = new Attachment();
                attachment.Name = bAttachment.fileName;
                attachment.Body = bAttachment.body;
                binAttachments.add(attachment);       
            }
        }

        return binAttachments;
    } 

    private Long getMaxAttachSize (List<Attachment> binAttachments) {
        Long maxSize = 0;

        // Set length of longest attachment

        for(Attachment binAttach: binAttachments) {
            if(binAttach.Body.size() > maxSize) {
                maxSize = binAttach.Body.size();
            }
        }

        return maxSize;
    }

    private void setAttachParent(List<Attachment> binAttachments, ID parentId) {

        // Set parent id in each attachment 
    
        for(Attachment binAttach: binAttachments) {
            binAttach.ParentId = parentId;
        }

    }
}