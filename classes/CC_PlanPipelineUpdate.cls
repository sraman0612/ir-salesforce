global class CC_PlanPipelineUpdate implements Database.Batchable<sObject>, Schedulable{
  global final string query;
  global final date d = system.today().toStartOfMonth();
  
  public Database.QueryLocator start(Database.BatchableContext BC){
    String qryStr;
    if(query != null && query != ''){qryStr = query;}
    else {
      qryStr = 'SELECT Id,Pipeline_Revenue__c,Pipeline_Units__c,Channel_Partner__c,Sales_Rep__c,RecordType.DeveloperName,Plan_Date__c,Product_Line_Lookup__r.Name ' +
               'FROM CC_Plan__c WHERE Plan_Date__c >= :d';
    }
    return Database.getQueryLocator(qryStr);
  }
  
  public void execute(Database.BatchableContext BC, List<CC_Plan__c> scope){
    CC_Plan__c [] plans2Update = new List<CC_Plan__c>();
    for (CC_Plan__c p : scope) {
      Decimal totalRevenue=0.00;
      Decimal totalUnits=0;
      if(p.RecordType.DeveloperName=='Objective'){ //partner objective
        for (OpportunityLineItem oli : [SELECT CC_Club_Car_Product_Line__c, Opportunity.CC_Requested_Ship_Date__c, Total_Club_Car_Price__c, 
                                               Quantity, Opportunity.Probability 
                                          FROM OpportunityLineItem
                                         WHERE Opportunity.PartnerAccountId = :p.Channel_Partner__c and
                                               Opportunity.CC_Requested_Ship_Date__c >= :d and 
                                               Opportunity.IsClosed = FALSE]){
          if(oli.Opportunity.CC_Requested_Ship_Date__c.toStartOfMonth() == p.Plan_date__c && 
             p.Product_Line_Lookup__r.Name == oli.CC_Club_Car_Product_Line__c){
            decimal pb = oli.Opportunity.Probability/100;
            totalRevenue += null == oli.Total_Club_Car_Price__c?0:(oli.Total_Club_Car_Price__c*pb).round(System.RoundingMode.CEILING);
            totalUnits += null == oli.Quantity?0:(oli.Quantity*pb).round(System.RoundingMode.CEILING);
          }
        }
      } else { //internal club car plan
        for (OpportunityLineItem oli : [SELECT CC_Club_Car_Product_Line__c, Opportunity.CC_Requested_Ship_Date__c, Total_Club_Car_Price__c, Quantity,
                                               Opportunity.CC_Sales_Number__c, Opportunity.Probability 
                                          FROM OpportunityLineItem
                                         WHERE Opportunity.CC_Sales_Number__c = :p.Sales_Rep__c and
                                               Opportunity.CC_Requested_Ship_Date__c >= :d and 
                                               Opportunity.IsClosed = FALSE
                                               ]){
                                                  
          if(oli.Opportunity.CC_Requested_Ship_Date__c.toStartOfMonth() == p.Plan_date__c && 
             p.Product_Line_Lookup__r.Name == oli.CC_Club_Car_Product_Line__c){
            decimal pb = oli.Opportunity.Probability/100;
            totalRevenue += null == oli.Total_Club_Car_Price__c?0:(oli.Total_Club_Car_Price__c*pb).round(System.RoundingMode.CEILING);
            totalUnits += null == oli.Quantity?0:(oli.Quantity*pb).round(System.RoundingMode.CEILING);
          }
        }
      
      }
      p.Pipeline_Revenue__c = totalRevenue;
      p.Pipeline_Units__c = totalUnits.intValue();
      plans2Update.add(p);
    }
    Database.SaveResult[] srList = Database.update(plans2Update, false);
  }
  
  public void finish(Database.BatchableContext BC){}
  
  global void execute(SchedulableContext sc) {Database.executeBatch(new CC_PlanPipelineUpdate(),10);}
}