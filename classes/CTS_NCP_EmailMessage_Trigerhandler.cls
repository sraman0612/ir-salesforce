public class CTS_NCP_EmailMessage_Trigerhandler {
    //If parent Case is closed reopen case in inProgress Status 
    public static void reopenCase(List<EmailMessage> emails,String recordTypeName,String reopenStatus){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
        Set<Id>caseIds = new Set<Id>();
        /*Check if parent is case and and create caseId set*/
        for(EmailMessage email:emails){
            if(email.Incoming && email.ParentId != null && email.ParentId.getSObjectType() == Case.getSObjectType() ){
                caseIds.add(email.ParentId);
            }
        }
        /*Query all related Cases*/
        Map<Id,Case>caseList = new Map<Id, Case>([Select Id, OwnerId, Owner.isActive, RecordTypeId,Status 
					                              From Case Where Id IN :caseIds 
                                                                  AND RecordTypeId =:recordTypeId
                                                                  AND Status = 'Closed']);
        List<Case>updatedCaseList = new List<Case>();
        /*Reopen a case with inProgress Status*/
        for(Case caseObj:caseList.values()){
            caseObj.Status = reopenStatus;
            updatedCaseList.add(caseObj);            
        }
        if(!updatedCaseList.isEmpty()){
            update updatedCaseList;
        }
    }
}