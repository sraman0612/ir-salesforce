@isTest
public class CC_PavilionServiceWarrantyBulletinsTest 
{
    static testmethod void UnitTest_CC_PavilionServiceWarrantyBulletinscnt() {
        List<ContentVersion> contentVersionList =new  List<ContentVersion>();
        CC_PavilionTemplateController controller;
        // Creation of Test Data
        Account newAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert newAccount;
        System.assertEquals('Sample Account',newAccount.Name);
        System.assertNotEquals(null, newAccount.Id);
        Account secondAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert secondAccount;
        System.assertEquals('Sample Account',secondAccount.Name);
        System.assertNotEquals(null, secondAccount.Id);
        Contract newContract =TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Commercial Utility',newAccount.Id);
        insert newContract;
        System.assertEquals('Commercial Utility', newContract.CC_Sub_Type__c);
        System.assertNotEquals(null, newContract.Id);
        ContentVersion contentVersionInsertFirst = TestUtilityClass.createContentVersionWithBulletinType('bulletin','Service & Warranty');
        contentVersionList.add(contentVersionInsertFirst);
        insert contentVersionList;  
        PavilionSettings__c newPavilionSettings = TestUtilityClass.createPavilionSettings('INTERNALCLUBCARACCT',secondAccount.Id);
        insert newPavilionSettings;
        Test.startTest();
        CC_PavilionServiceWarrantyBulletinscnt pavBulletins = new CC_PavilionServiceWarrantyBulletinscnt(controller);
        CC_PavilionServiceWarrantyBulletinscnt.getMarkets(newAccount.id);
        CC_PavilionServiceWarrantyBulletinscnt.getYears();
        CC_PavilionServiceWarrantyBulletinscnt.getBulletins(true,'Japan', 'USD', newAccount.id);
        try
        {
            CC_PavilionServiceWarrantyBulletinscnt.searchBulletins(true,'Japan', 'USD', newAccount.id, 'Service & Warranty');
        }
        Catch(Exception e){}
        Test.stopTest();
    }
}