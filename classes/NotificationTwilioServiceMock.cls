@isTest
public with sharing class NotificationTwilioServiceMock implements HttpCalloutMock {

    @TestVisible private static Boolean forceError = false;

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
        res.setStatus('OK');
        res.setHeader('Content-Type', 'application/json');

        String responseBody = '{' +
            '"sid":"SM8b8d7f06c6544ef4b1772b6134b58f46",' +
            '"date_created":"Tue, 22 Jun 2021 17:51:34 +0000",' +
            '"date_updated":"Tue, 22 Jun 2021 17:51:34 +0000",' +
            '"date_sent":null,' +
            '"account_sid":"ACd2b01a26d3a756592c16d41d2b86023c",' +
            '"to":"+15555555555",' +
            '"from":"+12052367631",' +
            '"messaging_service_sid":null,' +
            '"body":"The following warning was detected: warning, system overheating",' +
            '"status":"queued",' +
            '"num_segments":"3",' +
            '"num_media":"0",' +
            '"direction":"outbound-api",' +
            '"api_version":"2010-04-01",' +
            '"price":null,' +
            '"price_unit":"USD",' +
            '"error_code":' + (forceError ? '"1234",' : 'null,') +
            '"error_message":' + (forceError ? '"forced error",' : 'null,') +
            '"uri":"/2010-04-01/Accounts/ACd2b01a26d3a756592c16d41d2b86023c/Messages/SM8b8d7f06c6544ef4b1772b6134b58f46.json",' +
            '"subresource_uris":{' +
            '    "media":"/2010-04-01/Accounts/ACd2b01a26d3a756592c16d41d2b86023c/Messages/SM8b8d7f06c6544ef4b1772b6134b58f46/Media.json"' +
            '}' +
        '}';

        res.setBody(responseBody);        
        return res;
    }
}