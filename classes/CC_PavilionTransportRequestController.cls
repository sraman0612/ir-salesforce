global class CC_PavilionTransportRequestController {
  
  public CC_PavilionTransportRequestController(CC_PavilionTemplateController controller) {}
  
  @RemoteAction
  global static String submitTransportRequest(Case c){
    try{
      c.OwnerId = [SELECT Id FROM Group WHERE Developername = 'Transportation_Request_Queue' and Type = 'Queue' LIMIT 1].Id;
      c.RecordTypeId = [SELECT Id FROM RecordType WHERE sobjectType='Case' AND Developername = 'Club_Car_Transportation_Request' LIMIT 1].Id;
      c.ContactId = [SELECT ContactId FROM User WHERE id = :userinfo.getUserId()].ContactId;
      insert c;
      return 'Success' + c.Id;
    } catch(Exception e) {
      return 'Failure' +e;
    }
  }
}