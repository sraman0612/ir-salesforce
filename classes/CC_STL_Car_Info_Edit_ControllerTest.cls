@isTest
public with sharing class CC_STL_Car_Info_Edit_ControllerTest {
    
    @TestSetup
    private static void createTestData(){

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;            
        
        Profile testProfile = [Select Id From Profile Where Name = 'CC_System Administrator'];
        
        User testUser = TestUtilityClass.createNonPortalUser(testProfile.Id);
        testUser.LastName = 'Test123456789';
        testUser.userName += 'abc123';
        insert testUser;
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.isActive = true;
        prod1.CC_Item_Class__c = 'LCAR';
        prod1.ProductCode = 'xyz1';
        
        // No item class
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.isActive = true;
        prod2.CC_Item_Class__c = null;
        prod2.ProductCode = 'xyz2';        
        
        // No product code
        Product2 prod3 = TestDataUtility.createProduct('Club Car 3');
        prod3.isActive = true;
        prod3.CC_Item_Class__c = 'LCAR';
        prod3.ProductCode = null;
            
        Product2 prod4 = TestDataUtility.createProduct('Club Car 4');
        prod4.isActive = true;
        prod4.CC_Item_Class__c = 'LCAR';
        prod4.ProductCode = 'xyz4';
                               
        insert new Product2[]{prod1, prod2, prod3, prod4};

        CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
        insert salesRep;         
        
        Account acct = [Select Id From Account LIMIT 1];
            
        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        masterLease.Customer__c = acct.Id;
        insert masterLease;  
        
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;    
            
        CC_Short_Term_Lease__c stl1 = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id); 
        stl1.Lease_Extension_Count__c  = 0;

        insert stl1;
                    
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl1.Id;
        car1.Quantity__c = 10;
        car1.Per_Car_Cost__c = 25;    

        insert car1;
    }  
    
    @isTest
    private static void testGetInitFailure(){
    	
        User testUser = [Select Id From User Where LastName = 'Test123456789'];
        CC_STL_Car_Info__c carInfo = [Select Id From CC_STL_Car_Info__c LIMIT 1];
    	
    	system.runAs(testUser){
            
	    	CC_STL_Car_Info_Edit_Controller.forceError = true;
	    	CC_STL_Car_Info_Edit_Controller.InitResponse response = CC_STL_Car_Info_Edit_Controller.getInit(carInfo.Id);
	    	
	    	system.assertEquals(false, response.success);
	    	system.assertEquals('Test error', response.errorMessage);
            system.assertEquals(0, response.items.size());    
            system.assertEquals(null, response.stl.Id);
    	}		
    }
    
    @isTest
    private static void testGetInitSuccess(){
    	
        User testUser = [Select Id From User Where LastName = 'Test123456789'];
        CC_STL_Car_Info__c carInfo = [Select Id From CC_STL_Car_Info__c LIMIT 1];
    	
    	system.runAs(testUser){    	    	
    	
	    	CC_STL_Car_Info_Edit_Controller.InitResponse response = CC_STL_Car_Info_Edit_Controller.getInit(carInfo.Id);
	    	
	    	system.assertEquals(true, response.success);
	    	system.assertEquals('', response.errorMessage);
            system.assertEquals(2, response.items.size());
            system.assert(response.stl.Id != null);
    	}
    }     
}