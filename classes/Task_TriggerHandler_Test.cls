// 
// (c) 2015 Appirio, Inc.
//
// Test Class for Task trigger
//
// Apr 30, 2015      Barkha Jain           Original: T-382779
// Aug 5, 2015       Surabhi Sharma        Modified (I-175039)
//
@isTest
private class Task_TriggerHandler_Test {

    static testMethod void testTaskTrigger() {
        Lead l = new Lead();
        l.firstname = 'George';
        l.lastname = 'Acker';
        l.phone = '1234567890';
        l.City = 'city';
        l.Country = 'USA';
        l.PostalCode = '12345';
        l.State = 'CA';
        l.Street = '12, street1';
        l.company = 'ABC Corp';
        l.county__c = 'test county';
        insert l;
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_EU' 
                                    and SobjectType='Account' limit 1].Id;        
        Account acc = new Account();
        acc.Name = 'testName';
        acc.BillingCity = 'city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '56';
        acc.BillingState = 'CA';
        acc.BillingStreet = 'billStreet';
        acc.ShippingCity = 'shipcity';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = 'shipStreet';
        acc.ShippingPostalCode = '67';
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'testCounty';   
        acc.recordtypeid=AirNAAcctRT_ID;
        insert acc;
        
        
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.stagename = 'Stage 1. Qualify';
        o.amount = 1000000;
        o.Type = 'New Complete Machine';
        o.AccountID = acc.Id;
        o.closedate = system.today();
        insert o;
        
        
        
      /*  Task leadTask = new Task();
        leadTask.WhoId = l.Id;
        leadTask.Type = 'Phone Call';
        leadTask.Subject = 'Task on a Target'; */
        
        Task oppTask = new Task();
        oppTask.WhatId = o.Id;
        oppTask.RecordTypeId=[Select Id, DeveloperName from RecordType where DeveloperName = 'NA_Air_Task' and SobjectType='Task' limit 1].Id;
        oppTask.Type = 'Phone Call';
       // oppTask.Account.Name = 'TestAccName';
        oppTask.Subject = 'TestAccName/Test Opportunity:testSub';
        
        Test.startTest();
      //  insert leadTask;
   //     system.assertEquals('Target', [SELECT Opportunity_Stage__c FROM Task where Id =:leadTask.Id].Opportunity_Stage__c);
        try {
            insert oppTask; 
        }
        catch (Exception e){
            System.debug(e);
        }
        System.debug('im inside trigger handler test' + oppTask.Id);
       
        //system.assertEquals('Target', [SELECT Opportunity_Stage__c FROM Task where Id =:oppTask.Id].Opportunity_Stage__c);
        Test.stopTest();
    }
        static testMethod void testInactiveTask() {
        // Insert a test account
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Status__c = 'Active';
        testAccount.Currency__c = 'USD';
        testAccount.ShippingCountry = 'US';
        testAccount.ShippingStreet = '123';
        testAccount.ShippingPostalCode = '123';
        testAccount.ShippingCity = 'test';
        testAccount.ShippingState = 'CA';
        testAccount.CTS_Global_Shipping_Address_1__c = '13';
        testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
        testAccount.County__c = '123';
        testAccount.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'GD_Parent' LIMIT 1].Id;
        insert testAccount;
        
        // Create a set of account IDs
        Set<ID> accIDs = new Set<ID>();
        accIDs.add(testAccount.Id);

        // Create a test event
        Task testTask = new Task();
        testTask.Subject = 'Test Task';
        testTask.WhatId = testAccount.Id;

        // Create a list of test events
        List<Task> testTask1 = new List<Task>();
        testTask1.add(testTask);

        // Call the method and pass the set of account IDs and list of events
        Task_TriggerHandler.checkInactiveTaskAccount(accIDs, testTask1);

        // Assert that the method did not add any error to the test event
        System.assertEquals(0, testTask.getErrors().size());
        
        testAccount.Status__c = 'Inactive';
        update testAccount;
       	Task_TriggerHandler.checkInactiveTaskAccount(accIDs, testTask1);
        // Assert that the method added an error to the test event
        System.assertEquals(1, testTask.getErrors().size());
        System.assertEquals('This Account is Inactive', testTask.getErrors()[0].getMessage());
    }
}