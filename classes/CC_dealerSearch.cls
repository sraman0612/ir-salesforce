/*************************************************************
 * Description     : Proximity and Exclusive Dealer Search.
 * Created Date    : 09/08/2017
 * **********************************************************/

@RestResource(urlMapping='/searchDealer/*')
global class CC_dealerSearch {  
    public static Boolean hideInactive = true;
    public static String defaultDealerId = '';
    
    // No-Op test
    @HttpPost
    global static CC_dealerSearch.DealerInfo test () {
        return new CC_dealerSearch.DealerInfo();
    }
    
    
    // Webservice entry point
    @HttpGet
    global static void search() {
        apexLogHandler log = new apexLogHandler('CC_dealerSearch','search','');
        CC_dealerSearch.DealerInfo[] dealers = new CC_dealerSearch.DealerInfo[] {};
        String body = '';
        RestResponse response = new RestResponse();
                      
        try {
            // Request parameters  
            RestRequest request = RestContext.request;
            response = RestContext.response;
            log.logRequestInfo(request.requestURI + ' ' + String.valueOf(request.params));             
            String searchCode = (request.params.containsKey('Type')?request.params.get('Type'):NULL);
            String country    = (request.params.containsKey('Country')?request.params.get('Country'):NULL);
            String postcode   = (request.params.containsKey('PostalCode')?request.params.get('PostalCode'):NULL);

            System.debug('@@@ Dealer locator service request for Country: ' + country + ', ' +
                         'Post Code: ' + postcode + ', ' +
                         'Search Code: ' + searchCode);
            if(searchCode != NULL && country != NULL) {
                postcode = (NULL == postcode ? '' : postcode);        
                dealers = searchDealers(searchCode, country, postcode);
            }
            response.addHeader('Content-Type', 'application/json');  
            body = JSON.serialize(dealers);
            response.responseBody = Blob.valueOf(body);                 
        
        // Return custom exceptions to client
        } catch (CC_DealerSearch.DealerLocatorException e) {
            System.debug('@@@ Exception thrown: ' + e);
            log.logException(e);
            response.statusCode   = 400; // Bad request 
            response.addHeader('Content-Type', 'text/plain'); 
            body = e.getMessage();
            response.responseBody = Blob.valueOf(body);                   
        
        // Log and swallow unknown exceptions    
        } catch (Exception e) {
            System.debug('@@@ Exception thrown: ' + e);
            log.logException(e);
        
        } finally {
            log.logResponseInfo(body);       
        } 
        
        log.saveLog();
        System.debug('@@@ Dealer locator response: ' + body);                   
    }
    

    // Logical branching by search code and country
    global static CC_dealerSearch.DealerInfo[] searchDealers(String searchCode, String country, String postcode) {
        CC_dealerSearch.DealerInfo[] dealers = new CC_dealerSearch.DealerInfo[] {};    
        System.debug('@@@ Begin dealer search for Country: ' + country + ', ' +
                     'Post Code: ' + postcode + ', ' +
                     'Search Code: ' + searchCode);

        // Search configuration
        CC_Dealer_Locator_Settings__c dls = CC_Dealer_Locator_Settings__c.getValues(searchCode);
        if (NULL == dls) {
            System.debug('@@@ No configuration settings found for search code: ' + searchCode);
            return dealers;
        }
        
        // Initialize Search 
        CC_dealerSearch.SearchEngine se = new CC_DealerSearch.SearchEngine();
        se.SearchCode = searchCode;
        se.Country = isoToCountry(country);
        se.PostCode = postcode;
        se.SearchType = dls.Search_Type__c;
        se.DealerType  = dls.Value__c;
        se.SearchRadius = Integer.valueOf(dls.Search_Radius__c);
        CC_DealerSearch.defaultDealerId = dls.Default_Account_Id__c;
        dealers = se.doSearch();
        
        // Add Club Car Sales Info
        dealers = AddSalesInfo(dealers);        
        
        // As a last resort...
        if (dealers.size() == 0) {
            System.debug('@@@ Retrieving default account ' + CC_DealerSearch.defaultDealerId + '  for search code ' + se.SearchCode);
            dealers = defaultResponse();      
        }
        
        // Include search method in response
        for (CC_dealerSearch.DealerInfo d :dealers) {
            d.SearchMethod = se.SearchByPostCode ? se.SearchType : 'International';
        }

        System.debug('@@@ Found dealer(s): ' + dealers);
        return  dealers; 
    }
    
    
    // Default account lookup
    static CC_dealerSearch.DealerInfo[] defaultResponse() {
        CC_dealerSearch.DealerInfo[] response = new CC_DealerSearch.DealerInfo[] {};
        for (Account a :[SELECT Id, Name, ShippingAddress, CC_Dealer_Geolocation__c, CC_Locator_Active__c,
                                ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                                CC_Dealer_Geolocation__Latitude__s, CC_Dealer_Geolocation__Longitude__s, CC_Shipping_County__r.Name,
                                Phone, Website, CC_Powerchord_Dealer_Site__c, CC_Black_Gold_Status__c,
                                CC_Primary_Contact_First_Name__c, CC_Primary_Contact_Last_Name__c,
                                CC_Primary_Contact_Email__c
                         FROM Account
                         WHERE id = :cc_dealerSearch.defaultDealerId
                         LIMIT 1])  {
            CC_dealerSearch.DealerInfo d    = new CC_dealerSearch.DealerInfo();
            d.accId               = a.Id;
            d.agrId               = null;
            d.AgreementType       = 'DEFAULT';
            d.DealerName          = a.Name;
            d.Street              = a.ShippingStreet;
            d.City                = a.ShippingCity;
            d.State               = a.ShippingState;
            d.Zip                 = a.ShippingPostalCode;
            d.Country             = a.ShippingCountry;
            d.DealerPhone         = a.Phone;
            d.DealerWebsite       = a.Website;
            d.DealerContact       = (null == a.CC_Primary_Contact_First_Name__c ? '' :
                                             a.CC_Primary_Contact_First_Name__c) + ' ' +
                                    (null == a.CC_Primary_Contact_Last_Name__c ? '' :
                                             a.CC_Primary_Contact_Last_Name__c);
            d.DealerEmail         = a.CC_Primary_Contact_Email__c;                        
            d.Latitude            = String.ValueOf(a.CC_Dealer_Geolocation__Latitude__s);
            d.Longitude           = String.valueOf(a.CC_Dealer_Geolocation__Longitude__s);
            d.County              = a.CC_Shipping_County__r.Name;
            d.BlackandGoldFlag    = a.CC_Black_Gold_Status__c;
            d.PowerChordSite      = a.CC_PowerChord_Dealer_Site__c; 
            d.locatorActive       = a.CC_Locator_Active__c; 
            
           response.add(d);                  
        }
            
        return response;   
    }
    
    
    // Lat/Long/County lookup for postalcode
    static CC_PostalCode__c[] getPostalCode (String postCode, String country) {
        return [SELECT Id, Name ,CC_Lat__c, CC_Long__c, CC_StateOrProvince__c, CC_County__c,
                       County__c, County__r.County_Key__c   
                FROM CC_PostalCode__c
                WHERE Name =:postcode 
                AND CC_Country__c = :country];
    }


    // Distance calculations
    static Double distanceFromPostalCode(CC_PostalCode__c pc, Decimal latitude, Decimal longitude) {
            Double pcLat    = Double.valueOf(pc.CC_Lat__c);
            Double pcLong   = Double.valueOf(pc.CC_Long__c); 
            Location pcLoc  = Location.newInstance(pcLat, pcLong);
            Location loc   = Location.newInstance(latitude, longitude);
            Double distance = Decimal.valueOf(Location.getDistance(loc, pcLoc, 'mi')).setScale(2);       
            return distance ;                    
        }     

    
    // Additional Sales info, including Sales Rep and Partner Overrides
    static DealerInfo[] AddSalesInfo(DealerInfo[] dList) {
        DealerInfo[] newList = new DealerInfo[] {} ;
        newList.addAll(dList);
                
        // Look up Sales rep names
        String[] salesNum = new String[] {};
        for (DealerInfo d :newList) {
            if (null != d.SalesPersonNumber && '' != d.SalesPersonNumber) {
                salesNum.add(d.SalesPersonNumber);
             }
        }        
        Map<String, CC_Sales_Rep__c> salesRepMap = New Map<String, CC_Sales_Rep__c>();
        for (CC_Sales_Rep__c salesRep :[SELECT Id, CC_Sales_Person_Number__c, CC_Sales_Rep__r.Name, CC_Sales_Rep__r.Email
                                        FROM CC_Sales_Rep__c
                                        WHERE CC_Sales_Person_Number__c IN :salesNum]) {
            salesRepMap.put(salesRep.CC_Sales_Person_Number__c, salesRep);                                
        }
               
        // Enrich response with Sales Info
        CC_dealerSearch.DealerInfo[] defaultInfo;
        for (Integer i = 0; i < newList.size(); i++) {
            CC_dealerSearch.DealerInfo d = newList[i];
            
            // Sales rep data  
            CC_Sales_Rep__c sr = salesRepMap.get(d.SalesPersonNumber);
            if (null != sr) {
                d.SalesPersonName  = sr.CC_Sales_Rep__r.Name;
                d.SalesPersonEmail = sr.CC_Sales_Rep__r.Email;
            }
            
            // Partner Overrides (merge default account info w/ Partner info) 
            if (d.OverridePartner) {
                if (defaultInfo == null) {
                    defaultInfo = defaultResponse();     
                }
                if (defaultInfo.size() > 0) {
                    defaultInfo[0].AccId            = d.AccId;
                    defaultInfo[0].AgrId            = d.AgrId;
                    defaultInfo[0].AgreementType    = d.AgreementType;
                    defaultInfo[0].County           = d.County;
                    defaultInfo[0].DealerContact    = d.SalesPersonName;
                    defaultInfo[0].DealerEmail      = d.SalesPersonEmail;
                    defaultInfo[0].SalesPersonName  = d.SalesPersonName;
                    defaultInfo[0].SalesPersonEmail = d.SalesPersonEmail;
                    defaultInfo[0].OverridePartner  = true;
                    newList[i] = defaultInfo[0].clone();
                } 
            }
        }
        
        return newList;
    }
    
        
    // Core Search Logic
    public class SearchEngine {
        public String SearchCode{get;set;}
        public String SearchType{get;set;}
        public String DealerType{get;set;}
        public String Country{get;set;}
        public String PostCode{get;set;}
        public Integer PostCodeSize{get;set;}
        public Integer SearchRadius{get;set;}
        public Boolean searchByPostCode{get;set;}
        private String[] FILTER_BLANKVALUE = New String[]{''} ;     // Use BLANK to explicitly filter for blank values
        private String[] FILTER_ANYVALUE   = null;                  // USE NULL to remove filters and allow all values 
        
        
        // Search entry point
        public CC_dealerSearch.DealerInfo[] doSearch() {
            CC_dealerSearch.DealerInfo[] dealers = new CC_dealerSearch.DealerInfo[] {};
            
            // Search dealers by type and country
            System.debug('@@@ Search method for type ' + DealerType + ' in ' + Country + ': ' + SearchType);
            searchbyPostCode = requirePostalCode(); 
            if (searchByPostCode && (PostCode == null || PostCode == '')) {
                throw new cc_DealerSearch.DealerLocatorException('Postal Code Required for country: ' + Country);    

            } else if (searchByPostCode && SearchType == 'Proximity') {
                dealers = getDealersByRadius();

            } else if (searchType == 'Proximity' && Country != null && Country != '') {
                dealers = getExclusiveDealers('COUNTRY', new String[] {Country});

            } else if (searchByPostCode && SearchType == 'Exclusive') {
                dealers = getExclusiveDealers('POSTALCODE', new String[] {PostCode.left(PostCodeSize)});

            } else if (searchType == 'Exclusive' && Country != null && Country != '') {
                dealers = getExclusiveDealers('COUNTRY', new String[] {Country});
            
            }  else {
                System.debug('@@@ Cannot perform search for this combination of parameters');
            }

            return dealers;        
        }

        
        // Does this search require a postal code?
        private Boolean requirePostalCode() {
           boolean required = false;
           
           // Used SOQL here instead of getValues() due to issues w/ case sensitivity
           StatesCountries__c[] sc = [SELECT Id, Name, CC_PostalCode_Proximity__c, CC_PostalCode_Exclusive__c, CC_PostalCode_Size__c
                                      FROM StatesCountries__c
                                      WHERE Name = :Country
                                      LIMIT 1];
           if ((sc != null) && (!sc.isEmpty())) {
             required = ((SearchType == 'Proximity' && sc[0].CC_PostalCode_Proximity__c) ||
                         (SearchType == 'Exclusive' && sc[0].CC_PostalCode_Exclusive__c));
             if (required && sc[0].CC_PostalCode_Size__c <> null && sc[0].CC_PostalCode_Size__c > 0) {
                 PostCodeSize = (Integer)sc[0].CC_PostalCode_Size__c;
             } 
           }
           
           return required;        
        }

        
        // Helper:  Radius search by zipcode
        private CC_dealerSearch.DealerInfo[] getDealersByRadius () {
    
            // Truncate postal code as needed
            string prefix = PostCode.left(PostCodeSize);
            
            System.debug('@@@ Begin ' + SearchRadius + ' mile radius search for postal code: ' + PostCode);                                                         
            Map<String, CC_dealerSearch.DealerInfo> dealerMap = new Map<String, CC_dealerSearch.DealerInfo>();
            CC_PostalCode__c[] postalcodes = getPostalCode(prefix, Country);
            if (postalcodes.isEmpty()) System.debug('@@@ Postal code not found.');            
    
            if (!postalcodes.IsEmpty()){
                Double lattitude = Double.valueOf(postalcodes[0].CC_Lat__c);
                Double longitude = Double.valueOf(postalcodes[0].CC_Long__c);    
                String soql = 'SELECT Id, CC_Agreement_Number__r.Id, ' +
                                     'CC_Agreement_Number__r.CC_Type__c, CC_Agreement_Number__r.CC_Sub_Type__c, ' +
                                     'CC_Agreement_Number__r.CC_Contract_Status__c, ' +
                                     'CC_Agreement_Number__r.CC_Primary_Sales_Reporting__c, CC_Agreement_Number__r.CC_Override_Partner__c, ' +
                                     'CC_Agreement_Number__r.Account.ShippingAddress, CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__c, ' +
                                     'CC_Agreement_Number__r.AccountId, CC_Agreement_Number__r.Account.Name, CC_Agreement_Number__r.Account.CC_Locator_Active__c, ' +
                                     'CC_Agreement_Number__r.Account.ShippingStreet, CC_Agreement_Number__r.Account.ShippingCity, ' +
                                     'CC_Agreement_Number__r.Account.ShippingState, CC_Agreement_Number__r.Account.ShippingPostalCode, ' + 
                                     'CC_Agreement_Number__r.Account.ShippingCountry, ' + 
                                     'CC_Agreement_Number__r.Account.Phone, CC_Agreement_Number__r.Account.CC_Shipping_County__r.Name, ' +
                                     'CC_Agreement_Number__r.Account.Website, CC_Agreement_Number__r.Account.CC_PowerChord_Dealer_Site__c, ' +
                                     'CC_Agreement_Number__r.Account.CC_Black_Gold_Status__c, ' +
                                     'CC_Agreement_Number__r.Account.CC_Primary_Contact_First_Name__c, CC_Agreement_Number__r.Account.CC_Primary_Contact_Last_Name__c, ' +
                                     'CC_Agreement_Number__r.Account.CC_Primary_Contact_Email__c, CC_Agreement_Number__r.Account.Type, ' +
                                     'CC_Agreement_Number__r.CC_Lead_Owner__r.Name, CC_Agreement_Number__r.CC_Lead_Owner__r.Email, ' +
                                     'CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Latitude__s, CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Longitude__s ' +
                              'FROM CC_Territory__c ' + 
                              'WHERE CC_Agreement_Number__r.CC_Sub_Type__c =: DealerType ' + 
                              'AND   CC_Country__c = :Country ' +
                              'AND   CC_Agreement_Number__r.CC_Type__c = \'Dealer/Distributor Agreement\' ' +
                              'AND   CC_Agreement_Number__r.CC_Contract_Status__c = \'Current\' ' +
                              'AND   DISTANCE(CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__c, ' + 
                                             'GEOLOCATION(:lattitude, :longitude ), \'mi\') < 5000 ' + 
                              (hideInactive ? 'AND CC_Agreement_Number__r.Account.CC_Locator_Active__c = TRUE ' : '') +                                                     
                              'ORDER BY DISTANCE(CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__c, ' +
                                                'GEOLOCATION(:lattitude, :longitude), \'mi\') NULLS LAST ' +
                              'LIMIT 5';
                System.Debug('Territory query: ' + soql);

                boolean radiusMet = false;     
                for (sObject o :Database.query(soql)) {
                    CC_Territory__c terr = (CC_Territory__c)o;                
                    Double distance = distanceFromPostalCode(postalcodes[0], 
                                                             terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Latitude__s, 
                                                             terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Longitude__s);
                    CC_dealerSearch.DealerInfo d    = new CC_dealerSearch.DealerInfo();
                    d.accId               = terr.CC_Agreement_Number__r.Account.Id;
                    d.agrId               = terr.CC_Agreement_Number__r.Id;
                    d.DealerType          = terr.CC_Agreement_Number__r.Account.Type;
                    d.DealerName          = terr.CC_Agreement_Number__r.Account.Name;
                    d.Street              = terr.CC_Agreement_Number__r.Account.ShippingStreet;
                    d.City                = terr.CC_Agreement_Number__r.Account.ShippingCity;
                    d.State               = terr.CC_Agreement_Number__r.Account.ShippingState;
                    d.Zip                 = terr.CC_Agreement_Number__r.Account.ShippingPostalCode;
                    d.Country             = terr.CC_Agreement_Number__r.Account.ShippingCountry;
                    d.DealerPhone         = terr.CC_Agreement_Number__r.Account.Phone;
                    d.DealerWebsite       = terr.CC_Agreement_Number__r.Account.Website;
                    d.DealerContact       = terr.CC_Agreement_Number__r.CC_Lead_Owner__r.Name;
                    d.DealerEmail         = terr.CC_Agreement_Number__r.CC_Lead_Owner__r.Email;                        
                    d.Latitude            = String.ValueOf(terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Latitude__s);
                    d.Longitude           = String.valueOf(terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Longitude__s);
                    d.County              = terr.CC_Agreement_Number__r.Account.CC_Shipping_County__r.Name;
                    d.BlackandGoldFlag    = terr.CC_Agreement_Number__r.Account.CC_Black_Gold_Status__c;
                    d.Distance            = String.valueOf(distance);
                    d.PowerChordSite      = terr.CC_Agreement_Number__r.Account.CC_PowerChord_Dealer_Site__c; 
                    d.locatorActive       = terr.CC_Agreement_Number__r.Account.CC_Locator_Active__c; 
                    d.AgreementType       = terr.CC_Agreement_Number__r.CC_Type__c + ':' + 
                                            terr.CC_Agreement_Number__r.CC_Sub_Type__c; 
                    d.SalesPersonNumber   = terr.CC_Agreement_Number__r.CC_Primary_Sales_Reporting__c;
                    d.OverridePartner     = terr.CC_Agreement_Number__r.CC_Override_Partner__c;

                    // Include all within radius, and 3 closest if none within radius
                    radiusMet = radiusMet || (distance <= SearchRadius);   
                    if ((distance <= SearchRadius) || 
                        (!radiusMet && (dealerMap.values().size() < 3))) {
                        dealerMap.put(d.accId, d);
                    }
                }
                System.Debug('@@@ Found ' + dealerMap.size() + ' matches ' +
                             (radiusMet ? 'INSIDE' : 'OUTSIDE') + 
                             ' search radius');
            }
            
            return dealermap.values();     
        }


        // Helper:  Exclusive territory search with gradually expanding search region
        private CC_dealerSearch.DealerInfo[] getExclusiveDealers (String regionType, String[] regionValues) {
                                                                      
            System.debug('@@@ Begin exclusive search by ' + regionType + ' for value(s): ' + regionValues);                                                         
            CC_dealerSearch.DealerInfo[] dealers = new CC_dealerSearch.DealerInfo[] {};
            CC_PostalCode__c[] postalCodes = new CC_PostalCode__c[] {};
            String[] searchValues = regionValues;
            
            // Postalcode, expanded to county
            if (regionType == 'POSTALCODE') {
                postalCodes = getPostalCode(regionValues[0], Country);   
                dealers.addAll(getDealersByTerritory(FILTER_ANYVALUE, FILTER_ANYVALUE, searchValues));
                
                // Expand zip ==> county
                if (dealers.size() == 0) {       
                   String[] counties = new String[] {};
                   for (CC_PostalCode__c pc :postalCodes) {
                       if ((pc.County__r.County_Key__c != '') && (pc.County__r.County_Key__c != null)) {
                           counties.add(pc.County__r.County_Key__c);  
                       }                                 
                   }
                   if (counties.size() == 0) {
                       counties.add('UNKOWN_COUNTY');
                   }              
                   regionType = 'COUNTY';
                   searchValues = counties;
                   System.debug('@@@ No match.  Expanding to ' + regionType + ' search for values: ' + searchValues); 
                }
            }  
            
            // County, expandeded to state
            if (regionType == 'COUNTY') {
                dealers.addAll(getDealersByTerritory(FILTER_ANYVALUE, searchValues, FILTER_BLANKVALUE));
                
                // Expand county ==> State
                if (dealers.size() == 0) {
                    String[] states = new String[] {};
                    for (CC_Counties__c c: [SELECT State__c 
                                            FROM CC_Counties__c
                                            WHERE County_Key__c IN :searchValues
                                            AND State__c <> ''
                                            AND State__c <> NULL]) {
                        states.add(c.State__c);
                    }
                    if (states.size() == 0) {
                        states.add('UNKNOWN_STATE');
                    }
                    regionType = 'STATE';
                    searchValues = states;
                   System.debug('@@@ No match.  Expanding to ' + regionType + ' search for values: ' + searchValues); 
                }
            }
            
            // State, expanded to country  
            if (regionType == 'STATE') {
                dealers.addAll(getDealersByTerritory(searchValues, FILTER_BLANKVALUE, FILTER_BLANKVALUE));
                
                // Expand state ==> Country
                if (dealers.size() == 0) {
                    regionType = 'COUNTRY';             
                   System.debug('@@@ No match.  Expanding to ' + regionType + ' search for values: ' + country); 
                }
            }
            
            // Country
            if (regionType == 'COUNTRY') {
                dealers.addAll(getDealersByTerritory(FILTER_BLANKVALUE, FILTER_BLANKVALUE, FILTER_BLANKVALUE));
            }                        
    
            // Get distances
            for (CC_dealerSearch.DealerInfo di :dealers) {
                if ((postalcodes.size() > 0) && (di.Latitude != NULL) && (di.Longitude != NULL)) {
                    di.Distance = String.valueOf(distanceFromPostalCode(postalcodes[0],
                                                 Decimal.valueOf(di.Latitude),
                                                 Decimal.valueOf(di.Longitude)));                                                    
                }
            }                                                                                    
                 
            return dealers;
        }
 
        
        // Helper:  Territory search
        private CC_dealerSearch.DealerInfo[] getDealersByTerritory(String[] states, 
                                                                   String[] counties, 
                                                                   String[] postalCodes) {
                                                        
            System.debug('PostalCodes ' + postalCodes);
            System.debug('Counties ' + counties);
            System.debug('States ' + states);
            
            String soql = 'SELECT Id, Name, CC_Country__c, CC_State__c, CC_ZipCode__c, County__r.Name, CC_Agreement_Number__c, ' +
                                 'CC_Agreement_Number__r.Account.Name, CC_Agreement_Number__r.Account.Id, CC_Agreement_Number__r.CC_Override_Partner__c, ' +
                                 'CC_Agreement_Number__r.Account.CC_Locator_Active__c, CC_Agreement_Number__r.Account.CC_Primary_Contact_Email__c, ' + 
                                 'CC_Agreement_Number__r.Account.CC_Primary_Contact_First_Name__c, CC_Agreement_Number__r.Account.CC_Primary_Contact_Last_Name__c, ' +
                                 'CC_Agreement_Number__r.CC_Lead_Owner__r.Name, CC_Agreement_Number__r.CC_Lead_Owner__r.Email, ' + 
                                 'CC_Agreement_Number__r.Account.ShippingStreet, CC_Agreement_Number__r.Account.ShippingCity, ' + 
                                 'CC_Agreement_Number__r.Account.ShippingState, CC_Agreement_Number__r.Account.ShippingPostalCode, ' +
                                 'CC_Agreement_Number__r.Account.ShippingCountry, CC_Agreement_Number__r.Account.Phone, ' +
                                 'CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Latitude__s, CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Longitude__s, ' +
                                 'CC_Agreement_Number__r.Account.CC_PowerChord_Dealer_Site__c, CC_Agreement_Number__r.Account.Website, ' +
                                 'CC_Agreement_Number__r.Account.CC_Black_Gold_Status__c, CC_Agreement_Number__r.Account.Type, ' +
                                 'CC_Agreement_Number__r.CC_Type__c, CC_Agreement_Number__r.CC_Sub_Type__c, ' +
                                 'CC_Agreement_Number__r.CC_Primary_Sales_Reporting__c ' +
                          'FROM CC_Territory__c ' +
                          'WHERE CC_Agreement_Number__r.CC_Sub_Type__c = :DealerType ' + 
                          'AND CC_Agreement_Number__r.CC_Contract_Status__c = \'Current\' ' + 
                          'AND CC_Country__c = :Country ' +
                               (hideInactive ? 'AND CC_Agreement_Number__r.Account.CC_Locator_Active__c = TRUE ' : '');
            if (states != FILTER_ANYVALUE) {
                soql += 'AND CC_State__c IN :states ';
            }
            if (counties != FILTER_ANYVALUE) {
                soql += 'AND County__r.County_Key__c IN :counties ';
            }
            if (postalCodes != FILTER_ANYVALUE) {
                soql += 'AND CC_ZipCode__c IN :postalCodes ';
            }
            soql += 'ORDER BY CC_Agreement_Number__r.Account.CC_Black_Gold_Sort__c ASC NULLS LAST, ' +
                    'CC_Agreement_Number__r.Account.CC_Locator_Sort__c ASC NULLS LAST ';
            System.Debug('Territory Query: ' + soql);   
            
            Map<String, CC_dealerSearch.DealerInfo> dealerMap = new Map<String, CC_dealerSearch.DealerInfo>(); 
            for (SObject obj :Database.query(soql)) {
                CC_dealerSearch.DealerInfo d    = new CC_dealerSearch.DealerInfo();
                CC_Territory__c terr  = (CC_Territory__c)obj;
                d.accId               = terr.CC_Agreement_Number__r.Account.Id;
                d.agrId               = terr.CC_Agreement_Number__c;
                d.DealerType          = terr.CC_Agreement_Number__r.Account.Type;
                d.DealerName          = terr.CC_Agreement_Number__r.Account.Name;
                d.Street              = terr.CC_Agreement_Number__r.Account.ShippingStreet;
                d.City                = terr.CC_Agreement_Number__r.Account.ShippingCity;
                d.State               = terr.CC_Agreement_Number__r.Account.ShippingState;
                d.Zip                 = terr.CC_Agreement_Number__r.Account.ShippingPostalCode;
                d.Country             = terr.CC_Agreement_Number__r.Account.ShippingCountry;
                d.DealerPhone         = terr.CC_Agreement_Number__r.Account.Phone;
                d.DealerWebsite       = terr.CC_Agreement_Number__r.Account.Website;
                d.DealerContact       = terr.CC_Agreement_Number__r.CC_Lead_Owner__r.Name;
                d.DealerEmail         = terr.CC_Agreement_Number__r.CC_Lead_Owner__r.Email;                        
                d.Latitude            = String.ValueOf(terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Latitude__s);
                d.Longitude           = String.valueOf(terr.CC_Agreement_Number__r.Account.CC_Dealer_Geolocation__Longitude__s);
                d.County              = terr.County__r.Name;
                d.BlackandGoldFlag    = terr.CC_Agreement_Number__r.Account.CC_Black_Gold_Status__c;
                d.PowerChordSite      = terr.CC_Agreement_Number__r.Account.CC_PowerChord_Dealer_Site__c;         
                d.AgreementType       = terr.CC_Agreement_Number__r.CC_Type__c + ':' + terr.CC_Agreement_Number__r.CC_Sub_Type__c; 
                d.locatorActive       = terr.CC_Agreement_Number__r.Account.CC_Locator_Active__c; 
                d.SalesPersonNumber   = terr.CC_Agreement_Number__r.CC_Primary_Sales_Reporting__c;  
                d.OverridePartner     = terr.CC_Agreement_Number__r.CC_Override_Partner__c;
                dealerMap.put(d.accId + (null == d.County ? '' : d.County), d);  
            }
            
            // Prioritize Dealer territories over Direct territories
            CC_dealerSearch.DealerInfo[] directDealers = new CC_dealerSearch.DealerInfo[] {};
            CC_dealerSearch.DealerInfo[] channelDealers = new CC_dealerSearch.DealerInfo[] {};
            for (String i :dealerMap.keyset()) {
                if (dealerMap.get(i).AgreementType.startsWithIgnoreCase('Direct')) {
                    directDealers.add(dealerMap.get(i));
                } else {
                    channelDealers.add(dealerMap.get(i));
                }
            }
            
            System.debug('@@@ ' + channelDealers.size() + ' channel dealers found.');
            System.debug('@@@ ' + directDealers.size() + ' direct territories found.');
            return channelDealers.size() > 0 ? channelDealers : directDealers;
        }      
        
    }  
    
    
    // Response
    global class DealerInfo {
        public String AccId{get;set;} 
        public String AgrId{get;set;}
        public String DealerType {get;set;}
        public String DealerName {get;set;}
        public String DealerPhone{get;set;}
        public String DealerWebsite{get;set;}
        public String DealerEmail {get;set;}
        public String DealerContact {get;set;}
        public String PowerChordSite{get;set;}
        public String AgreementType {get;set;}
        public String BlackandGoldFlag{get;set;}
        public String Distance {get;set;}
        public String Street{get;set;}
        public String City{get;set;}
        public String State{get;set;}
        public String Zip{get;set;}
        public String Country{get;set;}
        public String Latitude{get;set;}
        public String Longitude{get;set;}
        public String County{get;set;}
        public Boolean LocatorActive{get;set;} 
        public String SalesPersonNumber{get;set;}
        public String SalesPersonName{get;set;} 
        public String SalesPersonEmail{get;set;}
        public String SearchMethod{get;set;} 
        public Boolean OverridePartner{get;set;}
    }
    
    /* helper for lead trigger handler to borrow partner selection logic */
    global static CC_dealerSearch.DealerInfo getPartnerForLeadAssignment(String country, String zip, String type){
        CC_dealerSearch.DealerInfo[] dealers = new CC_dealerSearch.DealerInfo[] {};
        CC_dealerSearch.SearchEngine se = new CC_dealerSearch.SearchEngine();
        se.Country = country;
        se.PostCode = zip;
        for(CC_Dealer_Locator_Settings__c dls : CC_Dealer_Locator_Settings__c.getall().values()){
        if(dls.Value__c==type){ 
        se.SearchType = dls.Search_Type__c;
        se.SearchRadius = Integer.valueOf(dls.Search_Radius__c); 
        cc_DealerSearch.defaultDealerId = dls.Default_Account_Id__c; 
        }
        }
        se.DealerType  = type;
        dealers = se.doSearch();  
        if(!dealers.isEmpty()){
            return dealers[0];
        }
        return null;
    }
    
    
    // Translate ISO codes to Club Car country names
    private static String isoToCountry(String code) {
        String name = code.trim();
        
        if (code.length() == 2) {
            StatesCountries__c[] sc = [SELECT Id, Name, Code__c
                                       FROM StatesCountries__c
                                       WHERE Code__c = :code
                                       AND Type__c = 'C'
                                       LIMIT 1];
            if (sc.size() > 0) name = sc[0].Name;    
        }
        
        return name;
    }
    
    
    public class DealerLocatorException extends Exception {
    }
        
}