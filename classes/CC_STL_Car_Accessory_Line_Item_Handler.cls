public with sharing class CC_STL_Car_Accessory_Line_Item_Handler {

    @TestVisible private static final String duplicateAccessoryErrorMsg = 'This accessory has already been assigned to this STL car record.';
    @TestVisible private static final String deleteAccessoryActiveLeaseErrorMsg = 'Accessories cannot be removed while the lease is active.';
    
    public static void onBeforeInsert(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){
        preventDuplicateCarAccessories(carAccessoryLineItems, null);
    }
    
    public static void onBeforeUpdate(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems, Map<Id, CC_STL_Car_Accessory_Line_Item__c> oldMap){
        preventDuplicateCarAccessories(carAccessoryLineItems, oldMap);
    }
    
    public static void onBeforeDelete(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){
        preventDeletionsOnActiveLeases(carAccessoryLineItems);
    }    
    
    public static void onAfterInsert(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){
        rollupAccessoryLaborCosts(carAccessoryLineItems, null);
    }
    
    public static void onAfterUpdate(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems, Map<Id, CC_STL_Car_Accessory_Line_Item__c> oldMap){
        rollupAccessoryLaborCosts(carAccessoryLineItems, oldMap);
    }   
    
    public static void onAfterDelete(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){
        rollupAccessoryLaborCosts(carAccessoryLineItems, null);
    } 
    
    public static void onAfterUndelete(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){
        rollupAccessoryLaborCosts(carAccessoryLineItems, null);
    }
    
    // Prevent line item deletions when the car's lease is active
    private static void preventDeletionsOnActiveLeases(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems){       
            
     	Set<Id> stlCarIds = new Set<Id>();
        
        for (CC_STL_Car_Accessory_Line_Item__c lineItem : carAccessoryLineItems){
            
            if (lineItem.STL_Car__c != null){
                stlCarIds.add(lineItem.STL_Car__c);
            }
        }
        
        system.debug('stlCarIds: ' + stlCarIds);
        
        if (stlCarIds.size() > 0){
            	
            Map<Id, CC_STL_Car_Info__c> stlCarMap = new Map<Id, CC_STL_Car_Info__c>(
                [Select Id, Short_Term_Lease__r.Synced_to_MAPICS__c From CC_STL_Car_Info__c Where Id in :stlCarIds]
            );
        
        	for (CC_STL_Car_Accessory_Line_Item__c lineItem : carAccessoryLineItems){
            
                if (lineItem.STL_Car__c != null){
                    
                    CC_STL_Car_Info__c stlCar = stlCarMap.get(lineItem.STL_Car__c);
                    
                    if (stlCar != null && stlCar.Short_Term_Lease__c != null){
                        
                        if (stlCar.Short_Term_Lease__r.Synced_to_MAPICS__c){
                         	lineItem.addError(deleteAccessoryActiveLeaseErrorMsg);
                        }
                    }
                }
            }
        }     
    }
    
    // Total the labor costs from the assigned accessories on the parent STL car info records
    private static void rollupAccessoryLaborCosts(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems, Map<Id, CC_STL_Car_Accessory_Line_Item__c> oldMap){
    	
        // Get the set of all STL car Ids associated in the transaction
        Set<Id> STLcarIds = new Set<Id>();

        for (CC_STL_Car_Accessory_Line_Item__c lineItem : carAccessoryLineItems){
            
            CC_STL_Car_Accessory_Line_Item__c oldLineItem = oldMap != null ? oldMap.get(lineItem.Id) : null;
            
            // Only process if it is a new line item or one of the following fields is changing: STL_Car__c, STL_Car_Accessory__c, Total_Labor_Cost__c
            if (oldLineItem == null || lineItem.STL_Car__c != oldLineItem.STL_Car__c || lineItem.STL_Car_Accessory__c != oldLineItem.STL_Car_Accessory__c || lineItem.Total_Labor_Cost__c != oldLineItem.Total_Labor_Cost__c){             
            	STLcarIds.add(lineItem.STL_Car__c);
            }
        } 
        
        system.debug('STLcarIds: ' + STLcarIds);
        
        if (STLcarIds.size() > 0){
        	CC_STL_Car_Info_Service.updateCarAccessoryLaborCost(STLcarIds);
        }
    } 
    
    // Only 1 unique instance of each car accessory is allowed per STL Car Info record
    private static void preventDuplicateCarAccessories(List<CC_STL_Car_Accessory_Line_Item__c> carAccessoryLineItems, Map<Id, CC_STL_Car_Accessory_Line_Item__c> oldMap){
        
        system.debug('--preventDuplicateCarAccessories');
        
        // Get the set of all STL car Ids associated in the transaction
        Set<Id> STLcarIds = new Set<Id>();
        List<CC_STL_Car_Accessory_Line_Item__c> lineItemsToValidate = new List<CC_STL_Car_Accessory_Line_Item__c>();
        
        for (CC_STL_Car_Accessory_Line_Item__c lineItem : carAccessoryLineItems){
            
            CC_STL_Car_Accessory_Line_Item__c oldLineItem = oldMap != null ? oldMap.get(lineItem.Id) : null;
            
            // Only validate/check if it is a new line item or the car/accessory is changing on the line item
            if (oldLineItem == null || lineItem.STL_Car__c != oldLineItem.STL_Car__c || lineItem.STL_Car_Accessory__c != oldLineItem.STL_Car_Accessory__c){
                lineItemsToValidate.add(lineItem);
            	STLcarIds.add(lineItem.STL_Car__c);
            }
        }
        
        system.debug('lineItemsToValidate: ' + lineItemsToValidate);
        
        // Build a map of all existing line item assignments to those STL cars
        Map<Id, Set<Id>> STLcarAccessoryMap = new Map<Id, Set<Id>>();
        
        for (CC_STL_Car_Accessory_Line_Item__c lineItem : [Select STL_Car__c, STL_Car_Accessory__c From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c in :STLcarIds]){
            
            if (STLcarAccessoryMap.containsKey(lineItem.STL_Car__c)){
                STLcarAccessoryMap.get(lineItem.STL_Car__c).add(lineItem.STL_Car_Accessory__c);
            }
            else{
                STLcarAccessoryMap.put(lineItem.STL_Car__c, new Set<Id>{lineItem.STL_Car_Accessory__c});
            }
        }
        
        system.debug('STLcarAccessoryMap: ' + STLcarAccessoryMap);
        
        // Throw an error if a duplicate accessory is found on a STL car
        for (CC_STL_Car_Accessory_Line_Item__c lineItem : lineItemsToValidate){
        	
            system.debug('lineItem: ' + lineItem);
            system.debug('STLcarAccessoryMap.containsKey(lineItem.STL_Car__c): ' + STLcarAccessoryMap.containsKey(lineItem.STL_Car__c));
            
            if (STLcarAccessoryMap.containsKey(lineItem.STL_Car__c) && STLcarAccessoryMap.get(lineItem.STL_Car__c).contains(lineItem.STL_Car_Accessory__c)){
                system.debug('duplicate found');
                lineItem.addError(duplicateAccessoryErrorMsg);
            }
        }
    }
}