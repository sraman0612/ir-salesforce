global class CC_ComplianceContactUpdate implements Database.Batchable<Contact>{
  global Contact [] ContactLst = new List<Contact>();
  global CC_ComplianceContactUpdate(List<Contact> ContactInputLst){ContactLst = ContactInputLst;}
	global Iterable<Contact> start(Database.BatchableContext BC) {return ContactLst;}
	global void execute(Database.BatchableContext bc, List<Contact> contactLst){update contactLst;}
	global void finish(Database.BatchableContext bc){}  
}