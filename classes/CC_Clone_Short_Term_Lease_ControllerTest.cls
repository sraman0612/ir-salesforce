@isTest
public with sharing class CC_Clone_Short_Term_Lease_ControllerTest {
    
    @TestSetup
    private static void createTestData(){
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;     
        
        TestDataUtility dataUtil = new TestDataUtility(); 
        
        User intUser = dataUtil.createIntegrationUser();
        intUser.LastName = 'Test_Integ_User';
        intUser.userName += 'abc123'; 
        
        Profile ccSysAdminProf = [Select Id From Profile Where Name = 'CC_System Administrator'];
        
        User ccSysAdminUser = TestUtilityClass.createNonPortalUser(ccSysAdminProf.Id);
        ccSysAdminUser.LastName = 'Test_CC_Sys_Admin';
        ccSysAdminUser.userName += 'abc123';        
        
        insert new User[]{intUser, ccSysAdminUser};    
        
        system.runAs(intUser){
        
			// Assign the lease permission set to the cc sys admin user
			PermissionSet leasePermSet = [Select Id From PermissionSet Where Name = 'CC_Short_Term_Leases'];	    	
	    	
			PermissionSetAssignment permSetAssign = new PermissionSetAssignment(
				AssigneeId = ccSysAdminUser.Id,
				PermissionSetId = leasePermSet.Id
			);    	
			
			insert permSetAssign;	
        }                  
        
        system.runAs(ccSysAdminUser){
        
	        Account acct = [Select Id From Account LIMIT 1];
	        CC_Order__c ccOrder = TestUtilityClass.createNatAcctOrder(acct.id, acct.Id);
	        insert ccOrder;
	        
	        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
	        prod1.CC_Item_Class__c = 'LCAR';
	        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
	        prod2.CC_Item_Class__c = 'LCAR';     
	        insert new Product2[]{prod1, prod2};
	            
	    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', intUser.Id);
	    	insert salesRep;                    
	            
	        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
	        masterLease.Customer__c = acct.Id;
	        insert masterLease;  
	        
	        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
	        insert location;    
	            
	        CC_Short_Term_Lease__c stl1 = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
	        stl1.Lease_Category__c = 'Revenue';
	        stl1.Delivery_Quoted_Rate__c = 500;
	        stl1.Pickup_Quoted_Rate__c = 500;
	        stl1.Delivery_Internal_Cost__c = 500;
	        stl1.Pickup_Internal_Cost__c = 500;
	        stl1.Adjustments_and_Prepping_Fees__c = 500;
	        stl1.Approval_Process_Name__c = 'ABC';
	        stl1.Approval_Submission_Date__c = Date.today();        
	        stl1.Cars__c = 'A';
	        stl1.Vehicle_Status__c = 'Delivered';
	        stl1.Carrier__c = 'UC LS OUT'; 
	        stl1.Next_Billing_Date__c = Date.today().addDays(30); 
	        stl1.Remaining_Payments__c = 12; 
			stl1.PO_Number__c = 'ABC123';        
	        
	        CC_Short_Term_Lease__c stl2 = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
	        stl2.Lease_Category__c = 'Revenue';
	        stl2.Lease_Type__c = 'Charity';
	        stl2.Approval_Status__c = 'Not Yet Submitted';
	        stl2.Delivery_Quoted_Rate__c = 500;
	        stl2.Pickup_Quoted_Rate__c = 500;
	        stl2.Delivery_Internal_Cost__c = 500;
	        stl2.Pickup_Internal_Cost__c = 500;
	        stl2.Adjustments_and_Prepping_Fees__c = 500;   
	        stl2.Approval_Process_Name__c = 'ABC';
	        stl2.Approval_Submission_Date__c = Date.today();       
	        stl2.Cars__c = 'A';
	        stl2.Vehicle_Status__c = 'Delivered';     
	        stl2.PO_Number__c = 'ABC123';
	          
	        stl2.Carrier__c = 'UC LS OUT'; 
	        stl2.Next_Billing_Date__c = Date.today().addDays(30); 
	        stl2.Remaining_Payments__c = 12;                 
	                
	        insert new CC_Short_Term_Lease__c[]{stl1, stl2};
	                    
	        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
	        car1.Short_Term_Lease__c = stl1.Id;
	        car1.Quantity__c = 10;
	        car1.Per_Car_Cost__c = 25;
	        
	        CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod2.Id);
	        car2.Short_Term_Lease__c = stl1.Id;
	        car2.Quantity__c = 10;
	        car2.Per_Car_Cost__c = 25;
	        
	        CC_STL_Car_Info__c car3 = TestDataUtility.createSTLCarInfo(prod2.Id);
	        car3.Short_Term_Lease__c = stl2.Id;
	        car3.Quantity__c = 10;
	        car3.Per_Car_Cost__c = 25;
	        
	        insert new CC_STL_Car_Info__c[]{car1, car2, car3};   
	        
	        stl1.Synced_to_MAPICS__c = true;
	        stl1.New_Car_Order__c = ccOrder.Id; 
	        stl2.Synced_to_MAPICS__c = true;
	        stl2.New_Car_Order__c = ccOrder.Id; 
	        
	        update new CC_Short_Term_Lease__c[]{stl1, stl2};
	
	        CC_STL_Car_Accessory__c accessory1 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Deluxe Towbar', 500);
	        CC_STL_Car_Accessory__c accessory2 = TestDataUtility.createSTLCarAccessory(prod1.Id, 'Towbar', 500);
	        CC_STL_Car_Accessory__c accessory3 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Deluxe Towbar', 500);
	        CC_STL_Car_Accessory__c accessory4 = TestDataUtility.createSTLCarAccessory(prod2.Id, 'Towbar', 500);     
	
	        insert new CC_STL_Car_Accessory__c[]{accessory1, accessory2, accessory3, accessory4};
	        
	        CC_STL_Car_Accessory_Line_Item__c lineItem1 = TestDataUtility.createSTLCarAccessoryLineItem(car1.Id, accessory1.Id, 500, 1);
	        CC_STL_Car_Accessory_Line_Item__c lineItem2 = TestDataUtility.createSTLCarAccessoryLineItem(car1.Id, accessory2.Id, 500, 1);
	        CC_STL_Car_Accessory_Line_Item__c lineItem3 = TestDataUtility.createSTLCarAccessoryLineItem(car2.Id, accessory3.Id, 500, 1);
	        CC_STL_Car_Accessory_Line_Item__c lineItem4 = TestDataUtility.createSTLCarAccessoryLineItem(car2.Id, accessory4.Id, 500, 2);
	        
	        insert new CC_STL_Car_Accessory_Line_Item__c[]{lineItem1, lineItem2, lineItem3, lineItem4};
	        
	        update new CC_Short_Term_Lease__c[]{stl1, stl2};
        }
    }
    
    @isTest
    private static void testCloneLeaseRecord(){
    	
    	User testUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(testUser){
    	
	    	CC_Short_Term_Lease__c lease = [Select Id, Beginning_Date__c, Ending_Date__c, Lease_Category__c, Approval_Status__c, Approval_Process_Name__c, Approval_Submission_Date__c,
	    		Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c,
	    		Cars__c, Vehicle_Status__c, New_Car_Order__c, Carrier__c, Next_Billing_Date__c, Remaining_Payments__c, Synced_to_MAPICS__c, PO_Number__c
	    		From CC_Short_Term_Lease__c Where Lease_Type__c = '4FUN'];
	    		
			system.assertEquals('Revenue', lease.Lease_Category__c);
			system.assertEquals('Not Yet Submitted', lease.Approval_Status__c);
			system.assertEquals(500, lease.Delivery_Quoted_Rate__c);
			system.assertEquals(500, lease.Pickup_Quoted_Rate__c);
			system.assertEquals(500, lease.Delivery_Internal_Cost__c);
			system.assertEquals(500, lease.Pickup_Internal_Cost__c);
			system.assertEquals(Date.today(), lease.Beginning_Date__c);
			system.assertEquals(Date.today().addYears(2), lease.Ending_Date__c);	
	        system.assertEquals('ABC', lease.Approval_Process_Name__c);
	        system.assertEquals(Date.today(), lease.Approval_Submission_Date__c); 	
			system.assertEquals('A', lease.Cars__c);
			system.assertEquals('Delivered', lease.Vehicle_Status__c);
			system.assertNotEquals(null, lease.New_Car_Order__c);        
			system.assertEquals('UC LS OUT', lease.Carrier__c); 	        			
			system.assertEquals(Date.today().addDays(30), lease.Next_Billing_Date__c);
			system.assertEquals(12, lease.Remaining_Payments__c);
			system.assertEquals(true, lease.Synced_to_MAPICS__c);   
			system.assertEquals('ABC123', lease.PO_Number__c);      	
			
	    	Test.startTest();
	    	
	    	String responseStr = CC_Clone_Short_Term_Lease_Controller.cloneLeaseRecord(lease.Id, false, lease.Beginning_Date__c.addDays(2), lease.Ending_Date__c.addDays(2));
	    	
	    	Test.stopTest();
	    	
	    	CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse response = (CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse)JSON.deserialize(responseStr, CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse.class);
	    	system.assertEquals(true, response.success);
	    	system.assert(String.isBlank(response.errorMessage));
	    	system.assertNotEquals(null, response.newLeaseId);
	    	
	    	// Verify the new lease values
	    	CC_Short_Term_Lease__c newLease = [Select Id, Parent_Lease__c, Beginning_Date__c, Ending_Date__c, Lease_Category__c, Approval_Status__c, Approval_Process_Name__c, Approval_Submission_Date__c,
	    			Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c, Adjustments_and_Prepping_Fees__c, Lease_Extension_Count__c,
	    			Cars__c, Vehicle_Status__c, New_Car_Order__c, Carrier__c, Next_Billing_Date__c, Remaining_Payments__c, Synced_to_MAPICS__c, PO_Number__c
	    		From CC_Short_Term_Lease__c Where Id = :response.newLeaseId];
	    		
			system.assertEquals(lease.Id, newLease.Parent_Lease__c);
            system.assertEquals(lease.Lease_Category__c, newLease.Lease_Category__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseAprovalStatus, newLease.Approval_Status__c);
			system.assertEquals(null, newLease.Delivery_Quoted_Rate__c);
			system.assertEquals(null, newLease.Pickup_Quoted_Rate__c);
			system.assertEquals(null, newLease.Delivery_Internal_Cost__c);
			system.assertEquals(null, newLease.Pickup_Internal_Cost__c); 
			system.assertEquals(lease.Beginning_Date__c.addDays(2), newLease.Beginning_Date__c);
			system.assertEquals(lease.Ending_Date__c.addDays(2), newLease.Ending_Date__c);		  
			system.assertEquals(null, newLease.Adjustments_and_Prepping_Fees__c); 		
			system.assertEquals(0, Integer.valueOf(newLease.Lease_Extension_Count__c));
	        system.assertEquals(null, newLease.Approval_Process_Name__c);
	        system.assertEquals(null, newLease.Approval_Submission_Date__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseCarsFlag, newLease.Cars__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseVehicleStatus, newLease.Vehicle_Status__c);
	        system.assertEquals(null, newLease.New_Car_Order__c); 
			system.assertEquals(null, newLease.Carrier__c); 	        			
			system.assertEquals(null, newLease.Next_Billing_Date__c);
			system.assertEquals(null, newLease.Remaining_Payments__c);
			system.assertEquals(false, newLease.Synced_to_MAPICS__c);          
			system.assertEquals(null, newLease.PO_Number__c);   						
	    	
	    	Product2 prod1 = [Select Id, Name From Product2 Where Name = 'Club Car 1'];
	    	Product2 prod2 = [Select Id, Name From Product2 Where Name = 'Club Car 2'];    	
	    	
	    	// Verify the cars were cloned
	    	CC_STL_Car_Info__c[] clonedCars = [Select Id, Item__c From CC_STL_Car_Info__c Where Short_Term_Lease__c = :newLease.Id Order By Item__r.Name ASC];
	    	system.assertEquals(2, clonedCars.size());
	    	system.assertEquals(prod1.Id, clonedCars[0].Item__c);
	    	system.assertEquals(prod2.Id, clonedCars[1].Item__c);
	    	
	    	// Verify the accessory line items were cloned
	    	CC_STL_Car_Accessory_Line_Item__c[] clonedLineItems = [Select Id, STL_Car__c, STL_Car_Accessory__c From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c in :clonedCars];
	    	system.assertEquals(4, clonedLineItems.size());   
    	}	
    }
    
    @isTest
    private static void testExtendLeaseRecord(){
    	
    	User testUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(testUser){    	
    	
	    	CC_Short_Term_Lease__c lease = [Select Id, Beginning_Date__c, Ending_Date__c, Lease_Category__c, Approval_Status__c, Approval_Process_Name__c, Approval_Submission_Date__c,
	    			Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c,
	    			Cars__c, Vehicle_Status__c, New_Car_Order__c, Carrier__c, Next_Billing_Date__c, Remaining_Payments__c, Synced_to_MAPICS__c, PO_Number__c
	    		From CC_Short_Term_Lease__c Where Lease_Type__c = '4FUN'];
	    	
			system.assertEquals('Revenue', lease.Lease_Category__c);
			system.assertEquals('Not Yet Submitted', lease.Approval_Status__c);
			system.assertEquals(500, lease.Delivery_Quoted_Rate__c);
			system.assertEquals(500, lease.Pickup_Quoted_Rate__c);
			system.assertEquals(500, lease.Delivery_Internal_Cost__c);
			system.assertEquals(500, lease.Pickup_Internal_Cost__c);
			system.assertEquals(Date.today(), lease.Beginning_Date__c);
			system.assertEquals(Date.today().addYears(2), lease.Ending_Date__c);	
	        system.assertEquals('ABC', lease.Approval_Process_Name__c);
	        system.assertEquals(Date.today(), lease.Approval_Submission_Date__c);
			system.assertEquals('A', lease.Cars__c);
			system.assertEquals('Delivered', lease.Vehicle_Status__c);
			system.assertNotEquals(null, lease.New_Car_Order__c);
			system.assertEquals('UC LS OUT', lease.Carrier__c); 	        			
			system.assertEquals(Date.today().addDays(30), lease.Next_Billing_Date__c);
			system.assertEquals(12, lease.Remaining_Payments__c);
			system.assertEquals(true, lease.Synced_to_MAPICS__c);  
			system.assertEquals('ABC123', lease.PO_Number__c);  		
			
	    	Test.startTest();
	    	
	    	String responseStr = CC_Clone_Short_Term_Lease_Controller.cloneLeaseRecord(lease.Id, true, lease.Beginning_Date__c.addDays(2), lease.Ending_Date__c.addDays(2));
	    	
	    	Test.stopTest();
	    	
	    	CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse response = (CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse)JSON.deserialize(responseStr, CC_Clone_Short_Term_Lease_Controller.CloneLeaseResponse.class);
	    	system.assertEquals(true, response.success);
	    	system.assert(String.isBlank(response.errorMessage));
	    	system.assertNotEquals(null, response.newLeaseId);
	    	
	    	// Verify the new lease values
	    	CC_Short_Term_Lease__c newLease = [Select Id, Parent_Lease__c, Beginning_Date__c, Ending_Date__c, Lease_Category__c, Approval_Status__c, Approval_Process_Name__c, Approval_Submission_Date__c,
	    			Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c, Delivery_Internal_Cost__c, Pickup_Internal_Cost__c, Adjustments_and_Prepping_Fees__c, Lease_Extension_Count__c,
	    			Cars__c, Vehicle_Status__c, New_Car_Order__c, Carrier__c, Next_Billing_Date__c, Remaining_Payments__c, Synced_to_MAPICS__c, PO_Number__c
	    		From CC_Short_Term_Lease__c Where Id = :response.newLeaseId];

            system.assertEquals(lease.Id, newLease.Parent_Lease__c);	
			system.assertEquals(lease.Lease_Category__c, newLease.Lease_Category__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseAprovalStatus, newLease.Approval_Status__c);
			system.assertEquals(null, newLease.Delivery_Quoted_Rate__c);
			system.assertEquals(null, newLease.Pickup_Quoted_Rate__c);
			system.assertEquals(null, newLease.Delivery_Internal_Cost__c);
			system.assertEquals(null, newLease.Pickup_Internal_Cost__c); 
			system.assertEquals(lease.Beginning_Date__c.addDays(2), newLease.Beginning_Date__c);
			system.assertEquals(lease.Ending_Date__c.addDays(2), newLease.Ending_Date__c);		  
			system.assertEquals(null, newLease.Adjustments_and_Prepping_Fees__c); 		
			system.assertEquals(1, Integer.valueOf(newLease.Lease_Extension_Count__c)); 		
	        system.assertEquals(null, newLease.Approval_Process_Name__c);
	        system.assertEquals(null, newLease.Approval_Submission_Date__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseCarsFlag, newLease.Cars__c);
			system.assertEquals(CC_Clone_Short_Term_Lease_Controller.defaultLeaseVehicleStatus, newLease.Vehicle_Status__c);
			system.assertEquals(null, newLease.New_Car_Order__c);         	
			system.assertEquals(null, newLease.Carrier__c); 	        			
			system.assertEquals(null, newLease.Next_Billing_Date__c);
			system.assertEquals(null, newLease.Remaining_Payments__c);
			system.assertEquals(false, newLease.Synced_to_MAPICS__c);   
			system.assertEquals('ABC123', newLease.PO_Number__c);      	
	    	
	    	Product2 prod1 = [Select Id, Name From Product2 Where Name = 'Club Car 1'];
	    	Product2 prod2 = [Select Id, Name From Product2 Where Name = 'Club Car 2'];    	
	    	
	    	// Verify the cars were cloned
	    	CC_STL_Car_Info__c[] clonedCars = [Select Id, Item__c From CC_STL_Car_Info__c Where Short_Term_Lease__c = :newLease.Id Order By Item__r.Name ASC];
	    	system.assertEquals(2, clonedCars.size());
	    	system.assertEquals(prod1.Id, clonedCars[0].Item__c);
	    	system.assertEquals(prod2.Id, clonedCars[1].Item__c);
	    	
	    	// Verify the accessory line items were cloned
	    	CC_STL_Car_Accessory_Line_Item__c[] clonedLineItems = [Select Id, STL_Car__c, STL_Car_Accessory__c From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c in :clonedCars];
	    	system.assertEquals(4, clonedLineItems.size());   	
    	}
    }        
}