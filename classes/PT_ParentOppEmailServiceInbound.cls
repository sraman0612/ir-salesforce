global class PT_ParentOppEmailServiceInbound implements Messaging.InboundEmailHandler {
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
    Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    String bodyStr = email.plainTextBody;        
    string [] bodyParts= bodyStr.split('\\{ref:');
    string [] idParts = bodyParts[1].split('\\}');
    String relatedIdStr=idParts[0].trim();
    system.debug('### the parent opp id of the incoming email is ' + relatedIdStr);
    EmailMessage em = new EmailMessage();
    em.ToAddress =  String.join(email.toAddresses, ',');
    em.FromAddress = email.FromAddress;
    em.FromName = email.FromName;
    em.Subject = email.subject;
    em.status = '2';
    //em.HtmlBody = email.htmlBody;
    em.Incoming= True;
    em.TextBody = bodyStr;
    em.RelatedToId =relatedIdStr;
    insert em;
    if(email.textAttachments != null){
      for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
        Attachment attachment = new Attachment();
        attachment.Name = tAttachment.fileName;
        attachment.Body = Blob.valueOf(tAttachment.body);
        attachment.ParentId = em.Id;
        insert attachment;
      }
    }
    return result;
  }
}