@isTest
private class RshvacOppRevenueScheduleTrigger_Test {

  // Data used to create the User needed for the test
  private static final String FIRST_NAME = 'Thomas';
  private static final String LAST_NAME = 'Pynchon';
  private static final String USERNAME = 'tpynchon@nowhere.com';
  private static final String NICKNAME = 'TPynch';
  private static final String LOCALE = 'en_US';
  private static final String TIMEZONE = 'America/New_York';
  private static final String ENCODING = 'ISO-8859-1';
  private static final String PROFILE_NAME = 'RHVAC Sales';
  private static final String SALES_PERSON_ID = '4242424242';
  // Data used to create the User needed for testing IWD Cost Center's Revenue Schedules.
  private static final String IWD_USER_FIRST_NAME = 'David';
  private static final String IWD_USER_LAST_NAME = 'Garrett';
  private static final String IWD_USER_USERNAME = 'dgarrett@nowhere.com';
  private static final String IWD_USER_NICKNAME = 'dgarr';
  // Default Quota values
  private static final Integer QUOTA = 1000;
  // Default values for the base Account
    private static final String ACCOUNT_NAME = 'Thomas Account';
    private static final String CC_Name = 'Thomas CC';
    private static final String DEFAULT_BRANCH = 'Thomas Default Branch';
    private static final String CHANNEL = 'DSO';
    private static final String EBOPPTY = 'True';
    private static final Integer ANNUAL_REVENUE = 12000;
  private static final String DEALER = 'Dealer';
  private static final String CUSTOMER = 'Customer';
  //Default values for IWD Cost Center
  private static final String IWD_COST_CENTER= 'IWD Cost Center';
  private static final String IWD = 'IWD';
  private static final String IWD_DEFAULT_BRANCH = 'IWD Cost Center';
    
  // Developer Name of the RS_HVAC Record type
  private static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
  private static final String RS_HVAC_Opp_RT_DEV_NAME = 'RS_Existing_Business';
  public static Id rshvacOppRTID;
  public static Id rshvacRTID;

  /**
     *  Sets the data need for the tests
     */
    @testSetup static void mySetup() {
    // Get the Profile Id to assign to the test User
    Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME LIMIT 1].Id;
    // Insert a new User, it'll initially have the FIRST_SP_ID as Sales Person Id
    User testUser = new User(Sales_Person_Id__c = SALES_PERSON_ID, FirstName = FIRST_NAME, LastName = LAST_NAME, ProfileId = profileId,
                              UserName = USERNAME, Email = USERNAME, CommunityNickname = NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                              TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = NICKNAME);
    insert testUser;
    
    // Insert Cost Center record
    Cost_Center__c baseCostCenter = New Cost_Center__c(Name = 'Test Cost Center', Ownerid = testuser.id, Default_Branch__c = 'PQRS', 
                                                        Channel__c ='DSO');
    insert baseCostCenter;    
    System.debug('CostCenter Inserted' +baseCostCenter.id +baseCostCenter.Channel__c+ baseCostCenter.owner.name);   
    // Insert Residential Quota records for all months in the Year
    List<Residential_Quota__c> quotas = new List<Residential_Quota__c>();
    Integer currentYear = Date.today().year();
    for (Integer i = 1;i <= 12;i++){
      Integer daysInMonth = Date.daysInMonth(currentYear, i);
      Date forecastMonth = Date.newInstance(currentYear, i, daysInMonth);
      quotas.add(new Residential_Quota__c(User__c = testUser.Id, Forecast_Month__c = forecastMonth, Plan__c = QUOTA));
    }
    insert quotas;
     
    // Get the RS_HVAC Record Type Id
    rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    rshvacOppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_Opp_RT_DEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;
    // Insert a single Account for the Test User
    // The trigger will take care of inserting the Opportunity and the Revenue Schedules records
    Account baseAccount = new Account(Name = ACCOUNT_NAME, AnnualRevenue = ANNUAL_REVENUE, Type = DEALER,
                  Sales_Person_Id__c = SALES_PERSON_ID, RecordTypeId = rshvacRTID, Status__c = CUSTOMER,  Cost_Center_Name__c= baseCostCenter.id, Default_Branch__c = baseCostCenter.Default_Branch__c, ownerId = testuser.id ); //
    insert baseAccount;
    System.debug('Account Inserted' +baseAccount.id +baseAccount+ baseAccount.OwnerId);
    System.debug('Oppties created ' +[SELECT Id, Name FROM Opportunity Where Account.Name =:ACCOUNT_NAME LIMIT 1]); 
    
    //Data for IWD Cost Center
    User iwdTestUser = new User(FirstName = IWD_USER_FIRST_NAME, LastName = IWD_USER_LAST_NAME, ProfileId = profileId,
                              UserName = IWD_USER_USERNAME, Email = IWD_USER_USERNAME, CommunityNickname = IWD_USER_NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                              TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = IWD_USER_NICKNAME);
    insert iwdTestUser;
    
    Cost_Center__c iwdCostCenter = New Cost_Center__c(Name = IWD_COST_CENTER, Default_Branch__c = IWD_DEFAULT_BRANCH, Channel__c =IWD,
                                                        Create_Existing_Business_Opportunity__c = True, OwnerId = iwdTestUser.Id);
    insert iwdCostCenter;
    
    System.debug('Jaya Iwdcostcenter: '+iwdcostcenter);
    
    List<Residential_Quota__c> iwdQuotas = new List<Residential_Quota__c>();
    for (Integer i = 1;i <= 12;i++){
      Integer daysInMonth = Date.daysInMonth(currentYear, i);
      Date forecastMonth = Date.newInstance(currentYear, i, daysInMonth);
      iwdQuotas.add(new Residential_Quota__c(User__c = iwdTestUser.Id, Forecast_Month__c = forecastMonth, Plan__c = QUOTA));
    }
    System.debug('Jaya iwdQuotas : '+iwdQuotas);
    insert iwdQuotas;
    }

  /**
     *  Tests that the logic on the update trigger is correct
     */
    static testMethod void testUpdateRevenueSchedule() {
      Integer yearOfToday = Date.today().year();
      Integer monthOfRevenueScheduleUpdate = Date.today().month();
      Account baseAccount = [Select id from Account where Name = :ACCOUNT_NAME];
      Date previousYearDate = Date.newInstance(System.today().year()-1,monthOfRevenueScheduleUpdate,1);
      rshvacOppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_Opp_RT_DEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;
      Opportunity Opp = New Opportunity (Name = 'Test Oppty', StageName ='Requested',CloseDate = previousYearDate, AccountID = baseAccount.ID, RecordTypeId = rshvacOppRTID); 
      insert opp;
      Opportunity_Revenue_Schedule__c previousYearRevSchedule = new  Opportunity_Revenue_Schedule__c(opportunity__c = Opp.ID, Forecast_Month__c = previousYearDate, Actual__c =1000); 
      insert previousYearRevSchedule; 
      System.debug('ORS inserted' +previousYearRevSchedule); 
      // Get the revenue schedule of the base Account for the current month
      Opportunity_Revenue_Schedule__c revSchedule = [SELECT Id, Actual__c FROM Opportunity_Revenue_Schedule__c
                                                  WHERE CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate AND CALENDAR_YEAR(Forecast_Month__c) = :yearOfToday AND Opportunity__r.Account.Name =:ACCOUNT_NAME LIMIT 1];
    Test.startTest();
      // Update the actual value on the Revenue Schedule to half of the quota
      Integer newActual = QUOTA/2;
      revSchedule.Actual__c = newActual;
      update revSchedule;
      System.debug('ORS Updated ' +revSchedule + ' end ORS Updated '); 
    Test.stopTest();
    revSchedule = [Select id, prior_Year_Sales__c from Opportunity_Revenue_Schedule__c where id = :revSchedule.id];
    //System.assertEquals(1000, revSchedule.Prior_Year_Sales__c); Dhilip Commented as part of Case 00024336
    // Verify that the Residential Quota for this month was updated correctly
    Residential_Quota__c quotaOfMonth = [SELECT Id, Actual__c FROM Residential_Quota__c
                              WHERE CALENDAR_MONTH(Forecast_Month__c) = :monthOfRevenueScheduleUpdate AND User__r.Sales_Person_Id__c = :SALES_PERSON_ID LIMIT 1];
    System.assertEquals(newActual, quotaOfMonth.Actual__c);
  }
  
  static testMethod void testIwdUpdateRevenueSchedule() {
      Cost_Center__c iwdCostCenter = [Select id from Cost_Center__c where Name = :IWD_COST_CENTER];
      Date previousYearDate = Date.newInstance(System.today().year()-1,System.today().Month(),1);
      rshvacOppRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_Opp_RT_DEV_NAME AND SobjectType = 'Opportunity' LIMIT 1].Id;
      Opportunity Opp = New Opportunity (Name = 'Test Oppty', StageName ='Requested',CloseDate = previousYearDate, Cost_Center_Name__c = iwdCostCenter.ID, RecordTypeId = rshvacOppRTID); 
      insert opp;
      Opportunity_Revenue_Schedule__c previousYearRevSchedule = new  Opportunity_Revenue_Schedule__c(opportunity__c = Opp.ID, Forecast_Month__c = previousYearDate, Actual__c =1000); 
      insert previousYearRevSchedule; 
      System.debug('ORS inserted' +previousYearRevSchedule); 
      // Get the revenue schedule of the base Account for the current month
      Opportunity_Revenue_Schedule__c revSchedule = [SELECT Id, Actual__c FROM Opportunity_Revenue_Schedule__c
                                                  WHERE Forecast_Month__c = THIS_MONTH AND Opportunity__r.Cost_Center_Name__c =:iwdCostCenter.Id LIMIT 1];
    Test.startTest();
      // Update the actual value on the Revenue Schedule to half of the quota
      Integer newActual = QUOTA/2;
      revSchedule.Actual__c = newActual;
      update revSchedule;
    Test.stopTest();
    revSchedule = [Select id, prior_Year_Sales__c from Opportunity_Revenue_Schedule__c where id = :revSchedule.id];
  //  System.assertEquals(1000, revSchedule.Prior_Year_Sales__c); Dhilip Commented as part of Case 00024336
    // Verify that the Residential Quota for this month was updated correctly
    Id iwdUserId = [Select id from user where UserName = :IWD_USER_USERNAME].Id;
    Residential_Quota__c quotaOfMonth = [SELECT Id, Actual__c FROM Residential_Quota__c
                              WHERE Forecast_Month__c = THIS_MONTH AND User__c = :iwdUserId LIMIT 1];
 //   System.assertEquals(newActual, quotaOfMonth.Actual__c); Dhilip Commented as part of Case 00024336
  }
   
}