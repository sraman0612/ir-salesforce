@isTest
public class CTS_OM_TranslatorControllerTest {
    
    @TestSetup
    private static void createData(){
    	Account acct = TestDataUtility.createAccount(true, 'Test Account', [Select Id From RecordType Where isActive = True and sObjectType = 'Account' Order By Name ASC LIMIT 1].Id); 
        Contact con =  new contact(FirstName = 'test', LastName = 'Test', Title ='Mr', AccountId = acct.id, email='test@gmail.com', Phone = '555-555-5555');
        insert con;
        Case cs = TestDataUtility.createCase(true, Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId(), con.id, acct.id, 'Test Case subject', 'New');
    }   
    @isTest
    public static void testCaseField(){
        Case cs;
        for(Case c : [select id from Case limit 1]){
            cs = c;
        }
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMock());
        Object translated = CTS_OM_TranslatorController.translate(cs.id, new List<String>{'Subject', 'Description'}, 'Field');
        System.assert(translated != null);
        Test.stopTest();
        
    }
    
    @isTest
    public static void testCaseComments(){
        Case cs;
        for(Case c : [select id from Case limit 1]){
            cs = c;
        } 
        CaseComment cc = new CaseComment(parentId = cs.id, CommentBody = 'Test Comment');
        insert cc;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMock());
        Object translated = CTS_OM_TranslatorController.translate(cs.id, null, 'caseComments');
        System.assert(translated != null);
        Test.stopTest();
        
    }
    @isTest
    public static void testCaseEmailMessages(){
        Case cs;
        for(Case c : [select id from Case limit 1]){
            cs = c;
        }
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMock());
        Object translated = CTS_OM_TranslatorController.translate(cs.id, null, 'EmailMessages');
        System.assert(translated != null);
        Test.stopTest();
        
    }
    
    @isTest
    public static void testInitialize(){
        Case cs;
        for(Case c : [select id from Case limit 1]){
            cs = c;
        }
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMock());
        Object translated = CTS_OM_TranslatorController.initialize(cs.id);
        System.assert(translated != null);
        Test.stopTest();
        
    }
    @isTest
    public static void testCallGoogleAPI(){
        Case cs;
        for(Case c : [select id from Case limit 1]){
            cs = c;
        }
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GoogleAPIMock());
        Object translated = CTS_OM_TranslatorController.callGoogleAPI('Test Init');
        System.assert(translated != null);
        Test.stopTest();
        
    }

}