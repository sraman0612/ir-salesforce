/* This Class User for  CC_PavilionMyProfile page*/
global class CC_PavilionMyProfileController {
  
    /* member variables */  
    public List<CC_Pavilion_Content__c> bulletinLst { get;set; }
    public Boolean displayPopUpReset { get;set; }
    Public List<CC_Pavilion_Content__c> pavilionTileSettings { get;set;}
    Public List<string>  leftselected { get;set;} 
    Public User usertoUpdate { get;set; }
    Public List<string> selectedTiles { get;set; } 
    Public List<CC_Pavilion_Content__c> selectedcontentTiles { get;set; }   
    public Id usrId ;
    public Id accId ;
    public Id contactId ;
    public List<Contact> pavContacts{get;set;}
    public String resetComment{get;set;}
    public boolean showStatusMessage{get;set;}
    public String ErrorMessage{get;set;}
    public String contactFirstName{get;set;}
    public String contactLastName{get;set;}
    public String contactEmail{get;set;}  
    public String contactTitle{get;set;}
    public String contactDepartment{get;set;}
    public String strcontactNumber{get;set;}
    public String strcontactCellNumber{get;set;}
    public String strUserName{get;set;}
    public List<Contact> lstCntData{get;set;}
    public List<User> lstUserData{get;set;}
    public List<string> admins{get;set;}
    public String adminsEmail{get;set;}

    public integer indexofcontact{get;set;}
    public integer lastindexofcontact {get;set;}
    public map<contact,integer> contact_index_map {get;set;} 
    Public List<User> lstUser{get;set;}
    public String Putfullstop{get;set;}
    public String PutComma{get;set;}
    public string PutVariable {get;set;}
    public string contactString {get;set;}
    public String pavilionAdminEmails{get;set;}
    public Pavilion_Navigation_Profile__c naviProf;
  
  /* constructor */
    public CC_PavilionMyProfileController(CC_PavilionTemplateController ctlr) 
    {
        showStatusMessage=false;
        ErrorMessage='';
        displayPopUpReset = false;
        lstUser = new List<User>();
        indexofcontact = 0;
        lastindexofcontact = 0;
        contact_index_map = new map <contact,integer>(); 
        usrId  = userinfo.getUserId();
        lstCntData=new List<Contact>(); 
        lstUserData=new List<User>();
        pavContacts =new List<Contact>(); 
        admins = new List<String>();
        naviProf = ctlr.navProfile;
            
        accId = [SELECT AccountId from User  where id =:usrId].AccountId;
        List<User> userList= [select id,Email,CC_Pavilion_Navigation_Profile__c from user where AccountId =:accId and CC_Pavilion_Navigation_Profile__c like '%ADMIN'  ];
              
        for(User p: userList){
            if(pavilionAdminEmails != null && pavilionAdminEmails != ''){ 
                pavilionAdminEmails += '; '+p.Email ;
            }
            else {
                pavilionAdminEmails = p.Email ;
            } 
            admins.add(p.Email);
            adminsEmail= string.join(admins,',');
        }
        contactId=[Select ContactId from User where id =:usrId].ContactId;
        pavContacts =[SELECT Name,email from Contact where CC_Pavilion_Admin__c = true and AccountId = :accId ];
        lstUser=[Select Id,UserName,FederationIdentifier from User where Id=:usrId];
        if(lstUser!=null && lstUser.size()>0)
        strUserName=lstUser[0].FederationIdentifier;    
        if(contactId !=null)
        {      
            lstCntData=[Select FirstName,LastName, Email,Title, Department, Phone, MobilePhone, CC_Pavilion_Admin__c from Contact where Id =:contactId];
            if(lstCntData!=null && lstCntData.size() > 0)
            {
                contactFirstName=lstCntData[0].FirstName;
                contactLastName=lstCntData[0].LastName;
                contactEmail=lstCntData[0].Email;
                contactTitle=lstCntData[0].Title;
                contactDepartment=lstCntData[0].Department;
                strcontactNumber=lstCntData[0].Phone;
                strcontactCellNumber=lstCntData[0].MobilePhone;
            }         
        }
        leftselected = new List<string>(); 
        selectedTiles = new List<String>();
        selectedcontentTiles = new List<CC_Pavilion_Content__c>();    
        pavilionTileSettings = [SELECT id,name,Link__c,showOnPavilionHome__c,Tile_Page__c,Title__c,Type__c FROM CC_Pavilion_Content__c WHERE Type__c='Home Page Tile' and RecordType.Name = 'Home Page Tiles'];    
        bulletinLst = new List<CC_Pavilion_Content__c>();
        bulletinLst = [SELECT Title__c,Summary__c,Link__c FROM CC_Pavilion_Content__c WHERE showOnPavilionHome__c= true];
        usertoUpdate = [ SELECT id,name,Pavilion_Tiles__c FROM user WHERE id =: usrId ];
        if(usertoUpdate.Pavilion_Tiles__c != null) {
            for(integer i=1;i<usertoUpdate.Pavilion_Tiles__c.split(';').size();i++){
                selectedTiles.add(usertoUpdate.Pavilion_Tiles__c.split(';')[i]);
            }
        }
        for(CC_Pavilion_Content__c content :  [SELECT id,name,Link__c,showOnPavilionHome__c,Tile_Page__c,
                                        Title__c,Type__c FROM CC_Pavilion_Content__c 
                                        WHERE Type__c='Home Page Tile' AND RecordType.Name = 'Home Page Tiles' AND
                                        Title__c  in: selectedTiles AND CC_isTile__c=true Order by Title__c]) 
        {
            selectedcontentTiles.add(content);
        }
    }

    /* ListofSelectOptions For Tiles to Select*/
    public List<SelectOption> getunselectedvalues(){
      List<SelectOption> options = new List<SelectOption>();
      for (CC_Pavilion_Content__c tile: [SELECT Title__c,CC_Pavilion_Tiles_Navigation__c FROM CC_Pavilion_Content__c WHERE Title__c Not in : selectedTiles AND CC_isTile__c=true ORDER BY Title__c]) {
        if(naviProf.get(tile.CC_Pavilion_Tiles_Navigation__c) == true){options.add(new SelectOption(tile.Title__c, tile.Title__c));}
      }
      return options;
    }
    
    /* ListofSelectOptions For Selected Tiles */
    
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> option1 = new List<SelectOption>();
        if(usertoUpdate.Pavilion_Tiles__c != null) {
            for(string tile: selectedTiles){
                option1.add(new SelectOption(tile,tile));
            }
        }
        return option1;
    }

    /* Method to Save User Tiles */
    public PageReference save() {
        usertoUpdate.Pavilion_Tiles__c = apexpages.currentpage().getparameters().get('rightpanel');
        update usertoUpdate;
        Pagereference pref = new Pagereference ('/apex/CC_PavilionMyProfile');
        pref.setredirect(true);
        return pref;
    }     
   /* Method to Update Contact information */
    public void UpdateInformation()
    {   
        String FirstName= ApexPages.currentPage().getParameters().get('firstName');
        String LastName= ApexPages.currentPage().getParameters().get('lastName');
        String Title= ApexPages.currentPage().getParameters().get('Title');
        String DepartMent= ApexPages.currentPage().getParameters().get('DepartMent');
        String PhoneNo= ApexPages.currentPage().getParameters().get('PhoneNo');
        String CellNo= ApexPages.currentPage().getParameters().get('CellPhno');
        
        Set<id> usrSetId = new Set<id>();
        Set<id> conSetId = new Set<id>();
        try
        { 
            if(lstCntData.size() > 0)
            {
            if(FirstName != null &&  FirstName != '' ) {lstCntData[0].FirstName=FirstName;}
            if(LastName != null  &&  LastName != '' ) {lstCntData[0].LastName=LastName;}
            if(Title != null) {lstCntData[0].Title=Title;}
            if(DepartMent != null) {lstCntData[0].Department=DepartMent;}
            if(PhoneNo != null ) {lstCntData[0].Phone=PhoneNo;}
            if(CellNo != null) {lstCntData[0].MobilePhone=CellNo;}
            update lstCntData;
            usrSetId.add(usrId);
            conSetId.add(lstCntData[0].id);
            updateUser(usrSetId,conSetId);
            ErrorMessage = 'Info successfully updated.';
            showStatusMessage = true;
            }
        }catch(Exception ex)
        {
            
            ErrorMessage = 'Internal  Error ocurred.';
            showStatusMessage = true;
        }
        
        

    }
    
    
    @future
    public static void updateUser(Set<Id> userId, Set<Id> contId){
        
        List<User> finalUsrList = new List<User>();
        List<User> userInfo = [select id, FirstName,LastName,Email,Title,Department,Phone,MobilePhone,contactId from User where Id IN: userId];
        
        List<Contact> listCon = [select id, FirstName,LastName,Email,Title,Department,Phone,MobilePhone from Contact where Id IN: contId];
        
        if(userInfo.size() > 0 && listCon.size() > 0){
        for(User userInfoDetail : userInfo){
        for(Contact contactFieldMapping : listCon){
        if(contactFieldMapping.id == userInfoDetail.ContactId){
        if(contactFieldMapping.FirstName != userInfoDetail.FirstName && (userInfoDetail.FirstName != null))
                  userInfoDetail.FirstName = contactFieldMapping.FirstName;
                  if(contactFieldMapping.LastName != userInfoDetail.LastName && (userInfoDetail.LastName != null))
                  userInfoDetail.LastName = contactFieldMapping.LastName;
                  if(contactFieldMapping.Email != userInfoDetail.Email && (userInfoDetail.Email != null))
                  userInfoDetail.Email = contactFieldMapping.Email;
                  if(contactFieldMapping.Title != userInfoDetail.Title && (userInfoDetail.Title != null))
                  userInfoDetail.Title = contactFieldMapping.Title;
                  if(contactFieldMapping.Department != userInfoDetail.Department && (userInfoDetail.Department != null))
                  userInfoDetail.Department = contactFieldMapping.Department;
                  if(contactFieldMapping.Phone != userInfoDetail.Phone && (userInfoDetail.Phone != null))
                  userInfoDetail.Phone = contactFieldMapping.Phone;
                  if(contactFieldMapping.MobilePhone != userInfoDetail.MobilePhone && (userInfoDetail.MobilePhone != null))
                  userInfoDetail.MobilePhone = contactFieldMapping.MobilePhone;
                  
        }
        finalUsrList.add(userInfoDetail);
        }
        }
        }
        
        System.debug('@@@Final User List@@@'+finalUsrList );
        update finalUsrList;
        
    }
    
    
}