// 11/30/2016
// Cal Steele / Ben Lorenz

public class CC_OpportunityEventHandler {
  public static void setClosedDate(List<Opportunity> oppList){
    for(Opportunity o : oppList){
      if(o.CC_Shipment_Month__c != null && o.CC_Shipment_Month__c > system.today().addDays(30)){
        o.CloseDate = o.CC_Shipment_Month__c.addDays(-30);
      }
      if(null==o.StageName){
        o.Stagename='Account Management - Presale';
      }
      
    }  
  }
}