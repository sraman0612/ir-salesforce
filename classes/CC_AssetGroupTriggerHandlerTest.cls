@isTest
public class CC_AssetGroupTriggerHandlerTest {
    
    @TestSetup
    private static void createData(){
    	
        TestDataUtility testData = new TestDataUtility();
        
        TestDataUtility.createStateCountries();
        
        Profile ccSysAdminProf = [Select Id From Profile Where Name = 'CC_System Administrator'];
        
        User ccSysAdminUser = TestUtilityClass.createNonPortalUser(ccSysAdminProf.Id);
        ccSysAdminUser.LastName = 'Test_CC_Sys_Admin';
        ccSysAdminUser.userName += 'abc123';        
        
        insert ccSysAdminUser;          
        
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        Account acct = testData.createAccount('Club Car: Channel Partner');
        acct.Name = 'TestAccount1';
        acct.CC_Sales_Rep__c = salesRepacc.Id;
        acct.CC_Price_List__c='PCOPI';
        acct.BillingCountry = 'USA';
        acct.BillingCity = 'Augusta';
        acct.BillingState = 'GA';
        acct.BillingStreet = '2044 Forward Augusta Dr';
        acct.BillingPostalCode = '566';
        acct.CC_Shipping_Billing_Address_Same__c = true;
        insert acct;
        
        List<Product2> productslist = new List<Product2>();
        Id ccProduct2RTId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        Product2 prod =  TestUtilityClass.createProduct2();
        prod.CC_Market_Type__c = 'Golf';
        prod.CC_Vendor__c      = 'TEST GOLF VENDOR';
        prod.Name              = 'AGOLFCAR';
        prod.M3__c             = 'Golf';
        prod.RecordTypeid      = ccProduct2RTId;
        productslist.add(prod);
        
        Product2 prod2 =  TestUtilityClass.createProduct2();
        prod2.CC_Market_Type__c = 'Utility';
        prod2.CC_Vendor__c      = 'TEST UTILITY VENDOR';
        prod2.Name              = 'AUTILITYCAR';
        prod2.M3__c             = 'Utility';
        prod2.RecordTypeid      = ccProduct2RTId;
        productslist.add(prod2);
        
        Product2 prod3 =  TestUtilityClass.createProduct2();
        prod3.CC_Market_Type__c = 'Utility';
        prod3.CC_Vendor__c      = 'DIFFERENTUTILITYVENDOR';
        prod3.Name              = 'AUTILITYCAR';
        prod3.M3__c             = 'Utility';
        prod3.RecordTypeid      = ccProduct2RTId;
        productslist.add(prod3);
        
        Product2 prod4 =  TestUtilityClass.createProduct2();
        prod4.CC_Market_Type__c = 'Golf';
        prod4.CC_Vendor__c      = 'DIFFERENTGOLFVENDOR';
        prod4.Name              = 'AGOLFCAR';
        prod4.M3__c              = 'Golf';
        prod4.RecordTypeid      = ccProduct2RTId;
        productslist.add(prod4);
        
        insert productslist;    	
    }
    
    private static testMethod void testSetActiveLookups(){
    	
    	User ccSysAdminUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(ccSysAdminUser){
	    	
	    	List<Product2> products = [Select Id, Name From Product2];
	    	Account acct = [Select Id From Account Where Name = 'TestAccount1'];
	        
	        Test.startTest();
	        
	        List<CC_Asset_Group__c> newAssetGroupList = new List<CC_Asset_Group__c>();
	        
	        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(acct.id, products[0].id);
	        assetGroup1.Fleet_Status__c = 'Purchase-New';
	        assetGroup1.Asset_Group_Key__c = '123-ABC';
	        assetGroup1.Acquired_Date__c = Date.today()-1;
	        assetGroup1.Quantity__c = 10;
	        assetGroup1.Number_Retired__c = 0;
	        newAssetGroupList.add(assetGroup1); 
	        
	        CC_Asset_Group__c assetGroup2 = assetGroup1.clone(false, true);
	        assetGroup2.Fleet_Status__c = 'Lease-Used';
	        assetGroup2.Asset_Group_Key__c = '123-ABD';
	       	assetGroup2.Product__c = products[1].Id;
	        newAssetGroupList.add(assetGroup2);
	        
	        insert newAssetGroupList;        
	        
	        Test.stopTest();   
	        
	        CC_Asset_Group__c[] assetGroups = [Select Asset_Group_Key__c, Original_Fleet_Status__c, Fleet_Status__c From CC_Asset_Group__c Order By Asset_Group_Key__c ASC];     
	        
	        // Verify the field defaults
	        system.assertEquals(assetGroups[0].Fleet_Status__c, assetGroups[0].Original_Fleet_Status__c);
	        system.assertEquals(assetGroups[1].Fleet_Status__c, assetGroups[1].Original_Fleet_Status__c);  
    	}   
    }     
    
    private static testMethod void testSetFieldDefaults(){
    	
    	User ccSysAdminUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(ccSysAdminUser){    	
    	
	    	List<Product2> products = [Select Id, Name From Product2];
	    	Account acct = [Select Id From Account Where Name = 'TestAccount1'];
	        
	        Test.startTest();
	        
	        List<CC_Asset_Group__c> newAssetGroupList = new List<CC_Asset_Group__c>();
	        
	        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(acct.id, products[0].id);
	        assetGroup1.Fleet_Status__c = 'Retirement';
	        assetGroup1.Retired_Date__c = Date.today();
	        assetGroup1.Asset_Group_Key__c = '123-ABC';
	        assetGroup1.Acquired_Date__c = Date.today()-1;
	        assetGroup1.Quantity__c = 10;
	        assetGroup1.Number_Retired__c = 0;
	        assetGroup1.CC_Inactive_Asset_Lookup__c = acct.Id;
	        newAssetGroupList.add(assetGroup1); 
	        
	        CC_Asset_Group__c assetGroup2 = assetGroup1.clone(false, true);
	        assetGroup2.Fleet_Status__c = 'Lease-Used';
	        assetGroup2.Asset_Group_Key__c = '123-ABD';
	       	assetGroup2.Product__c = products[1].Id;
	        newAssetGroupList.add(assetGroup2);
	        
	        insert newAssetGroupList;
	        
	        CC_Asset_Group__c[] assetGroups = [Select Asset_Group_Key__c, Account__c, Fleet_Status__c, CC_Active_Asset_Lookup__c, CC_Inactive_Asset_Lookup__c From CC_Asset_Group__c Order By Asset_Group_Key__c ASC];     
	        
	        // Verify the active lookups
	        system.assertEquals(null, assetGroups[0].CC_Active_Asset_Lookup__c);
	        system.assertEquals(assetGroups[0].Account__c, assetGroups[0].CC_Inactive_Asset_Lookup__c);
	        
	        system.assertEquals(assetGroups[1].Account__c, assetGroups[1].CC_Active_Asset_Lookup__c);
	        system.assertEquals(null, assetGroups[1].CC_Inactive_Asset_Lookup__c);   
	        
	        // Set the status to a non-retirement status    
	        assetGroup1.Fleet_Status__c = 'Lease-Used';  
	        update assetGroup1;       
	        
	        assetGroups = [Select Asset_Group_Key__c, Account__c, Fleet_Status__c, CC_Active_Asset_Lookup__c, CC_Inactive_Asset_Lookup__c From CC_Asset_Group__c Order By Asset_Group_Key__c ASC]; 
	        
	        // Verify the active lookups
	        system.assertEquals(assetGroups[0].Account__c, assetGroups[0].CC_Active_Asset_Lookup__c);
	        system.assertEquals(null, assetGroups[0].CC_Inactive_Asset_Lookup__c);
	        
	        system.assertEquals(assetGroups[1].Account__c, assetGroups[1].CC_Active_Asset_Lookup__c);
	        system.assertEquals(null, assetGroups[1].CC_Inactive_Asset_Lookup__c);          
	        
	        Test.stopTest();     
    	}
    }    
    
    private static testMethod void testCheckAllRetired_RemainingActiveCars(){
    	
    	User ccSysAdminUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(ccSysAdminUser){    	
    	
	    	List<Product2> products = [Select Id, Name From Product2];
	    	Account acct = [Select Id From Account Where Name = 'TestAccount1'];
	    	
	        List<CC_Asset_Group__c> newAssetGroupList = new List<CC_Asset_Group__c>();
	        
	        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(acct.id, products[0].id);
	        assetGroup1.Asset_Group_Key__c = '123-ABC';
	        assetGroup1.Acquired_Date__c = Date.today()-1;
	        assetGroup1.Quantity__c = 10;
	        assetGroup1.Number_Retired__c = 0;
	        newAssetGroupList.add(assetGroup1); 
	        
	        CC_Asset_Group__c assetGroup2 = assetGroup1.clone(false, true);
	        assetGroup2.Asset_Group_Key__c = '123-ABD';
	       	assetGroup2.Product__c = products[1].Id;
	        newAssetGroupList.add(assetGroup2);
	        
	        insert newAssetGroupList;
	        
	        Test.startTest();
	        
	        newAssetGroupList[0].Fleet_Status__c = 'Retirement';
	        newAssetGroupList[0].Retired_Date__c = Date.today();
	        newAssetGroupList[0].Number_Retired__c = 8; // 2 cars remaining
	        
	        newAssetGroupList[1].Fleet_Status__c = 'Retirement';
	        newAssetGroupList[1].Retired_Date__c = Date.today();   
	        newAssetGroupList[1].Number_Retired__c = 10; // All cars retired 
	        
	        update newAssetGroupList;
	        
	        Test.stopTest();
	        
	        CC_Asset_Group__c[] assetGroups = [Select Asset_Group_Key__c, Account__c, Acquired_Date__c, Quantity__c, Fleet_Status__c, Fleet_Type__c, Color__c, Model_Year__c, Order__c, Product__c From CC_Asset_Group__c Order By Asset_Group_Key__c ASC];
	        
	        system.assertEquals(3, assetGroups.size());
	        
	        // Verify the values on the new asset group
	        system.assertEquals(newAssetGroupList[0].Account__c, assetGroups[1].Account__c);
	        system.assertEquals(newAssetGroupList[0].Acquired_Date__c, assetGroups[1].Acquired_Date__c);
	        system.assertEquals(newAssetGroupList[0].Asset_Group_Key__c + '_2', assetGroups[1].Asset_Group_Key__c);
	        system.assertEquals('Lease-New', assetGroups[1].Fleet_Status__c);
	        system.assertEquals(newAssetGroupList[0].Fleet_Type__c, assetGroups[1].Fleet_Type__c);
	        system.assertEquals(newAssetGroupList[0].Model_Year__c, assetGroups[1].Model_Year__c);
	        system.assertEquals(newAssetGroupList[0].Order__c, assetGroups[1].Order__c);
	        system.assertEquals(newAssetGroupList[0].Product__c, assetGroups[1].Product__c);
	        system.assertEquals(newAssetGroupList[0].Quantity__c - newAssetGroupList[0].Number_Retired__c, assetGroups[1].Quantity__c);
    	}
    }
    
    private static testMethod void testCheckAllRetired_GolfUtility(){
    	
    	User ccSysAdminUser = [Select Id From User Where LastName = 'Test_CC_Sys_Admin'];
    	
    	system.runAs(ccSysAdminUser){    	
    	
	    	List<Product2> productslist = [Select Id, Name From Product2];
	    	CC_Sales_Rep__c salesRepacc = [Select Id From CC_Sales_Rep__c];
	    	
	        Test.startTest();
	        
	        //Golf and utility new and old vendors are 'NO CARS'
	        Account acct = [SELECT CC_Golf_New_Vendor__c,CC_Golf_Old_Vendor__c,CC_Utility_New_Vendor__c,CC_Utility_Old_Vendor__c FROM Account WHERE Name = 'TestAccount1'];
	                                       
	        system.assertEquals('NO CARS',acct.CC_Golf_New_Vendor__c);
	        system.assertEquals(null,acct.CC_Golf_Old_Vendor__c);
	        system.assertEquals('NO CARS',acct.CC_Utility_New_Vendor__c);
	        system.assertEquals(null,acct.CC_Utility_Old_Vendor__c);
	        
	        //create some new golf and utility assets
	        List<CC_Asset_Group__c> assetgroupList = new List<CC_Asset_Group__c>();
	        CC_Asset_Group__c assetGroup = TestDataUtility.createAssetGroup(acct.id,productslist[0].id);
	        assetGroup.Acquired_Date__c = date.today()-1;
	        assetGroup.Fleet_Status__c    ='Lease-New';
	        assetgroupList.add(assetGroup);
	        
	        CC_Asset_Group__c assetGroup1 = TestDataUtility.createAssetGroup(acct.id,productslist[1].id);
	        assetGroup1.Fleet_Status__c    ='Purchase-New';
	        assetGroup1.Acquired_Date__c = date.today()-1;
	        assetgroupList.add(assetGroup1);
	        
	        insert assetgroupList;
	        
	        //Golf and utility new vendors changed, old vendors still 'NO CARS'
	        Account [] AccountTest2Lst = [SELECT CC_Golf_New_Vendor__c,CC_Golf_Old_Vendor__c,CC_Utility_New_Vendor__c,CC_Utility_Old_Vendor__c
	                                        FROM Account 
	                                       WHERE Id = :acct.id];
	                                       
	        system.assertEquals('TEST GOLF VENDOR',AccountTest2Lst[0].CC_Golf_New_Vendor__c);
	        system.assertEquals('NO CARS',AccountTest2Lst[0].CC_Golf_Old_Vendor__c);
	        system.assertEquals('TEST UTILITY VENDOR',AccountTest2Lst[0].CC_Utility_New_Vendor__c);
	        system.assertEquals('NO CARS',AccountTest2Lst[0].CC_Utility_Old_Vendor__c);
	        
	        //create some new golf and utility assets for different vendors
	        List<CC_Asset_Group__c> newassetgroupList = new List<CC_Asset_Group__c>();
	        CC_Asset_Group__c assetGroup2 = TestDataUtility.createAssetGroup(acct.id,productslist[2].id);
	        assetGroup2.Acquired_Date__c = date.today()-1;
	        assetGroup2.Fleet_Status__c    ='Lease-New';
	        newassetgroupList.add(assetGroup2);
	        
	        CC_Asset_Group__c assetGroup3 = TestDataUtility.createAssetGroup(acct.id,productslist[3].id);
	        assetGroup3.Fleet_Status__c    ='Purchase-New';
	        assetGroup3.Acquired_Date__c = date.today()-1;
	        newassetgroupList.add(assetGroup3);
	        
	        insert newassetgroupList;
	        
	        //Golf and utility new vendors changed, fomer new vendors to old
	        Account [] AccountTest3Lst = [SELECT CC_Golf_New_Vendor__c,CC_Golf_Old_Vendor__c,CC_Utility_New_Vendor__c,CC_Utility_Old_Vendor__c
	                                        FROM Account 
	                                       WHERE Id = :acct.id];
	                                       
	        system.assertEquals('DIFFERENTGOLFVENDOR',AccountTest3Lst[0].CC_Golf_New_Vendor__c);
	        system.assertEquals(AccountTest2Lst[0].CC_Golf_New_Vendor__c,AccountTest3Lst[0].CC_Golf_Old_Vendor__c);
	        system.assertEquals('DIFFERENTUTILITYVENDOR',AccountTest3Lst[0].CC_Utility_New_Vendor__c);
	        system.assertEquals(AccountTest2Lst[0].CC_Utility_New_Vendor__c,AccountTest3Lst[0].CC_Utility_Old_Vendor__c);
	        
	        //add another asset for same vendor
	        List<CC_Asset_Group__c> evennewerassetgroupList = new List<CC_Asset_Group__c>();
	        CC_Asset_Group__c assetGroup4 = TestDataUtility.createAssetGroup(acct.id,productslist[2].id);
	        assetGroup4.Acquired_Date__c = date.today()-1;
	        assetGroup4.Fleet_Status__c    ='Lease-New';
	        evennewerassetgroupList.add(assetGroup4);
	        
	        CC_Asset_Group__c assetGroup5 = TestDataUtility.createAssetGroup(acct.id,productslist[3].id);
	        assetGroup5.Fleet_Status__c    ='Purchase-New';
	        assetGroup5.Acquired_Date__c = date.today()-1;
	        evennewerassetgroupList.add(assetGroup5);
	        insert evennewerassetgroupList;
	        
	        //Golf and utility new and old vensors don't change
	        Account [] AccountTest4Lst = [SELECT CC_Golf_New_Vendor__c,CC_Golf_Old_Vendor__c,CC_Utility_New_Vendor__c,CC_Utility_Old_Vendor__c
	                                        FROM Account 
	                                       WHERE Id = :acct.id];
	                                       
	        system.assertEquals(AccountTest3Lst[0].CC_Golf_New_Vendor__c,AccountTest4Lst[0].CC_Golf_New_Vendor__c);
	        system.assertEquals(AccountTest3Lst[0].CC_Golf_Old_Vendor__c,AccountTest4Lst[0].CC_Golf_Old_Vendor__c);
	        system.assertEquals(AccountTest3Lst[0].CC_Utility_New_Vendor__c,AccountTest4Lst[0].CC_Utility_New_Vendor__c);
	        system.assertEquals(AccountTest3Lst[0].CC_Utility_Old_Vendor__c,AccountTest4Lst[0].CC_Utility_Old_Vendor__c);
	        
	        //retire all assets
	        assetgroupList[0].Fleet_Status__c = 'Retirement';
	        assetgroupList[0].Retired_Date__c = system.today();
	        assetgroupList[1].Fleet_Status__c = 'Retirement';
	        assetgroupList[1].Retired_Date__c = system.today();
	        update assetgroupList;
	        
	        newassetgroupList[0].Fleet_Status__c = 'Retirement';
	        newassetgroupList[0].Retired_Date__c = system.today();
	        newassetgroupList[1].Fleet_Status__c = 'Retirement';
	        newassetgroupList[1].Retired_Date__c = system.today();
	        update newassetgroupList;
	        
	        evennewerassetgroupList[0].Fleet_Status__c = 'Retirement';
	        evennewerassetgroupList[0].Retired_Date__c = system.today();
	        evennewerassetgroupList[1].Fleet_Status__c = 'Retirement';
	        evennewerassetgroupList[1].Retired_Date__c = system.today();
	        update evennewerassetgroupList;
	        
	        //golf and utility new vendors should be 'NO CARS', old vendors shoudl be former new vendors
	        Account [] AccountTest5Lst = [SELECT CC_Golf_New_Vendor__c,CC_Golf_Old_Vendor__c,CC_Utility_New_Vendor__c,CC_Utility_Old_Vendor__c
	                                        FROM Account 
	                                       WHERE Id = :acct.id];
	                                       
	        system.assertEquals('NO CARS',AccountTest5Lst[0].CC_Golf_New_Vendor__c);
	        system.assertEquals(AccountTest4Lst[0].CC_Golf_New_Vendor__c,AccountTest5Lst[0].CC_Golf_Old_Vendor__c);
	        system.assertEquals('NO CARS',AccountTest5Lst[0].CC_Utility_New_Vendor__c);
	        system.assertEquals(AccountTest4Lst[0].CC_Utility_New_Vendor__c,AccountTest5Lst[0].CC_Utility_Old_Vendor__c);
	        
	        Test.stopTest();
	    }
    }
}