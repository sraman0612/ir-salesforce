@isTest
public class CC_Links_PartCartController_Test{
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    public static testmethod void UnitTest_CC_Links_PartCartController() 
    {
        Account a=TestUtilityClass.createCustomAccount1();
        insert a;
        System.assertNotEquals(null,a);
        Contact c=TestUtilityClass.createCustomContact1(a.id);
        insert c;
        System.assertNotEquals(null,c);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(c.Id,u.ContactId);
        System.runAs(u)
        {
        Id recordtypid = Schema.SObjectType.CC_Pavilion_Content__c.getRecordTypeInfosByName().get('Part Cart').getRecordTypeId();
        CC_Pavilion_Content__c pavContent = new CC_Pavilion_Content__c();
        pavContent.Title__c='Part Cart Page';
        pavContent.RecordTypeId =recordtypid;
        pavContent.CC_Body_1__c='testdata';
        pavContent.CC_Body_2__c='testdatas';
        insert pavContent;
        CC_PavilionTemplateController Controller ;
        CC_Links_PartCartController  partcartController = new CC_Links_PartCartController(Controller);
        Test.startTest();
        CC_Links_PartCartController.getPartCartBody1();
        CC_Links_PartCartController.getPartCartBody2();
        partcartController.getPartCartURL();
        Test.stopTest();
        }
    }
}