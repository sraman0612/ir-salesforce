public class CalloutInterlynxForFiles implements Queueable, Database.AllowsCallouts {
    private Map<String, List<String>> leadToFileMap;
    private List<ContentVersion> contentVersionsToUpdate = new List<ContentVersion>();  // For bulk DML
    private List<Lead> leadsToUpdate = new List<Lead>();  // For bulk DML
    private Map<String, String> leadErrorMap = new Map<String, String>(); // Map to accumulate errors for leads
    private Set<Id> fileErrorIds = new Set<Id>();
	
    //Custom exception for heap size error
    public class HeapSizeException extends Exception {}
    
    // Constructor
    public CalloutInterlynxForFiles(Map<String, List<String>> leadToFileMap) {
        this.leadToFileMap = leadToFileMap;
    }
    
    public void execute(QueueableContext context) {
        for (String leadId : leadToFileMap.keySet()) {
            try{
                List<String> fileIds = leadToFileMap.get(leadId);
                
                for (String fileId : fileIds) {
                    // Perform the callout for each file
                    fileUploadToInterlynx(leadId, fileId);
                    
                }
            }
            catch(Exception ex){
                // Accumulate errors for the lead
                String errorMessage = 'Exception: ' + ex.getMessage();
                if (leadErrorMap.containsKey(leadId)) {
                    leadErrorMap.put(leadId, leadErrorMap.get(leadId) + '\n' + errorMessage);
                } else {
                    leadErrorMap.put(leadId, errorMessage);
                }
            }
        }
        
        // After processing all files, perform the DML update for ContentVersions and Leads
        updateLeadContentVersionRecords();
        
        // After processing all the update, perform the update on leads for exception or errors.
        logLeadError(leadErrorMap,fileErrorIds);
    }
    
    // Helper method to perform callout
    private void fileUploadToInterlynx(String leadId, String fileId) {
        //Retrieve the file details from ContentVersion
        ContentVersion cv = [SELECT Title, VersionData, FileExtension,ContentSize,Lead_Ids_Shared_with_Interlynx__c,Lead_Ids_ready_to_sync_with_Interlynx__c FROM ContentVersion WHERE Id = :fileId LIMIT 1];
        String base64File = EncodingUtil.base64Encode(cv.VersionData);
        
        try{            
            //Check if the file exceeds the heap size limit
            if(base64File.length() > (12 * 1024 * 1024)){ //12MB heap size check
                throw new HeapSizeException('File exceeds the heap size limit.');
            }
            
            //Retrieve the metadata for the integration details
            Interlynx_System__mdt attachmentIntegration = Interlynx_System__mdt.getInstance('Attachment_Integration');
            
            //Generating the JSON format request body
            JSONGenerator jsonGen = JSON.createGenerator(true); // true = pretty print the output
            jsonGen.writeStartObject();
            jsonGen.writeStringField('SalesforceLeadId', leadId);
            jsonGen.writeStringField('FileId', fileId);        
            jsonGen.writeBooleanField('IsDelete', false);
            jsonGen.writeStringField('FileName', cv.Title+'.'+cv.FileExtension);
            jsonGen.writeStringField('File', base64File);
            jsonGen.writeNumberField('FileSize', cv.ContentSize);
            jsonGen.writeStringField('FileMimeType', '.'+cv.FileExtension);
            jsonGen.writeEndObject();
            String jsonData = jsonGen.getAsString();
            
            //Creating the HTTP request for the integration
            HttpRequest req = new HttpRequest();
            req.setHeader('Content-Type','application/json');  
            req.setHeader('authorization', 'Bearer '+attachmentIntegration.Bearer_Token__c);
            req.setMethod('POST');
            req.setEndpoint(attachmentIntegration.Interlynx_Endpoint__c);
            req.setBody(jsonData);
            Http http = new Http();
            HTTPResponse response = http.send(req);
            
            Integer statusCode = response.getStatusCode();
            Map<String,Object> newMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            // If status code is not 200, log the response details
            if (statusCode != 200) {
                String errorMessage;
                // Capture any exceptions during the callout process
                for(String key : newMap.keySet()){
                    if(key == 'Message' && newMap.get(key) != null){
                        errorMessage =  System.today().format() + ' - ' + 'Callout failed. Status: ' + statusCode + ', Response: ' + newMap.get(key) + ' for file: ' + cv.Title + '\n';
                    }
                }
                if (leadErrorMap.containsKey(leadId)) {
                    leadErrorMap.put(leadId, leadErrorMap.get(leadId) + '\n' + errorMessage);
                } else {
                    leadErrorMap.put(leadId, errorMessage);
                }
				fileErrorIds.add(fileId);
            } else {
            	Boolean calloutSuccess = true;
                for(String key : newMap.keySet()){
                    if(key == 'Exception' && newMap.get(key) != null){
                        
						calloutSuccess = false;
                        
                        // Capture any exceptions during the callout process
                        String errorMessage =  System.today().format() + ' - ' + 'Callout Exception: ' + newMap.get(key) + ' for file: ' + cv.Title + '\n';
                        if (leadErrorMap.containsKey(leadId)) {
                            leadErrorMap.put(leadId, leadErrorMap.get(leadId) + '\n' + errorMessage);
                        } else {
                            leadErrorMap.put(leadId, errorMessage);
                        }
						fileErrorIds.add(fileId);
                    }
                }
                if(calloutSuccess){
					updateContentVersionAfterSuccess(fileId,leadId);
                }
            }
        } catch (Exception ex) {
            // Capture any exceptions during the callout process
            String errorMessage =  System.today().format() + ' - ' + 'Exception: ' + ex.getMessage() + ' for file: ' + cv.Title + '\n';
            if (leadErrorMap.containsKey(leadId)) {
                leadErrorMap.put(leadId, leadErrorMap.get(leadId) + '\n' + errorMessage);
            } else {
                leadErrorMap.put(leadId, errorMessage);
            }
			fileErrorIds.add(fileId);
        }
    }

    // Helper method to update the ContentVersion after a successful callout
    private void updateContentVersionAfterSuccess(String fileId, String leadId) {
        ContentVersion cv = [SELECT Id, Lead_Ids_ready_to_sync_with_Interlynx__c, Lead_Ids_Shared_with_Interlynx__c FROM ContentVersion WHERE Id = :fileId LIMIT 1];
        
        // Mark the file as shared with the lead
        cv.Lead_Ids_Shared_with_Interlynx__c = (cv.Lead_Ids_Shared_with_Interlynx__c != null) ? cv.Lead_Ids_Shared_with_Interlynx__c + ',' + leadId + '=' + System.now() : leadId + '=' + System.now();
        
        // Remove the lead from the ready-to-sync list
        List<String> readyLeadIds = new List<String>(cv.Lead_Ids_ready_to_sync_with_Interlynx__c.split(','));
        readyLeadIds.remove(readyLeadIds.indexOf(leadId));
        cv.Lead_Ids_ready_to_sync_with_Interlynx__c = String.join(readyLeadIds, ',');
        
        contentVersionsToUpdate.add(cv);
    }
    
    private void updateLeadContentVersionRecords() {
        
        // Update ContentVersions and capture errors
        if (!contentVersionsToUpdate.isEmpty()) {
            Database.SaveResult[] contentVersionResults = Database.update(contentVersionsToUpdate, false);
            Map<String, String> fileIdToLeadIdMap = new Map<String, String>();
            
            // Build a map of fileId to leadId from leadToFileMap
            for (String leadId : leadToFileMap.keySet()) {
                List<String> fileIds = leadToFileMap.get(leadId);
                for (String fileId : fileIds) {
                    fileIdToLeadIdMap.put(fileId, leadId);
                }
            }
            
            for (Integer i = 0; i < contentVersionResults.size(); i++) {
                if (!contentVersionResults[i].isSuccess()) {
                    // Handle individual ContentVersion update failure
                    Database.Error error = contentVersionResults[i].getErrors()[0];
                    ContentVersion failedCv = contentVersionsToUpdate[i];
                    
                    // Find associated leadId using fileIdToLeadIdMap
                    String leadId = fileIdToLeadIdMap.get(failedCv.Id);
                    
                    if (leadId != null) {
                        // Accumulate errors for the lead
                        String errorMessage = 'ContentVersion update failed: ' + error.getMessage();
                        if (leadErrorMap.containsKey(leadId)) {
                            leadErrorMap.put(leadId, leadErrorMap.get(leadId) + '\n' + errorMessage);
                        } else {
                            leadErrorMap.put(leadId, errorMessage);
                        }
                    }
					fileErrorIds.add(contentVersionResults[i].Id);
                }
            }
        }
	}
    
    private void logLeadError(Map<String, String> leadErrorMap, Set<Id> fileErrorIds){
        
        Map<Id, Lead> mapOfLead = new Map<Id, Lead>([SELECT Id, Interlynx_Integration_Error__c FROM Lead WHERE Id = :leadErrorMap.keySet()]);
		
        Map<Id, ContentVersion> mapOfContentVersion = new Map<Id, ContentVersion>([SELECT Id, Lead_Ids_ready_to_sync_with_Interlynx__c, Lead_Ids_Shared_with_Interlynx__c FROM ContentVersion WHERE Id IN :fileErrorIds]);
		
        // Log accumulated errors for leads
        for (String leadId : leadErrorMap.keySet()) {
        	if(mapOfLead.containsKey(leadId)){
            	mapOfLead.get(leadId).Interlynx_Integration_Error__c = leadErrorMap.get(leadId) + mapOfLead.get(leadId).Interlynx_Integration_Error__c;
				
				for(ContentVersion conver : mapOfContentVersion.values()){
					String[] readyLeadIdsOnCV = conver.Lead_Ids_ready_to_sync_with_Interlynx__c.split(',');
					readyLeadIdsOnCV.remove(readyLeadIdsOnCV.indexOf(leadId));     	
					conver.Lead_Ids_ready_to_sync_with_Interlynx__c = String.join(readyLeadIdsOnCV, ',');
				}
            }
        }
        
        if(mapOfContentVersion.size() > 0){
			update mapOfContentVersion.values();
        }
        if(mapOfLead.size() > 0){
			update mapOfLead.values();
        }
    }    
}