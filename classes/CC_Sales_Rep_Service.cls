public with sharing class CC_Sales_Rep_Service {
    
    public class SalesHierarchy{
    	public User salesRep;
    	public User salesManager;
    	public User salesDirector;
    	public User salesVP;
    }
    
    // Returns a map of the CC_Sales_Rep__c Id and its sales hierarchy
    public static Map<Id, SalesHierarchy> lookupSalesHiearchy(Set<Id> salesRepIds){
    	
    	Map<Id, SalesHierarchy> salesHierarchyMap = new Map<Id, SalesHierarchy>();
    	
    	for (CC_Sales_Rep__c salesRep : 
    		[Select Id, CC_Used_Car_Sales_Rep__c,
    			CC_Sales_Rep__r.Name, CC_Sales_Rep__r.Alias, CC_Sales_Rep__r.isActive, CC_Sales_Rep__r.CC_LOA__c, CC_Sales_Rep__r.PS_Purchasing_LOA__c,
    		  	CC_Sales_Rep__r.Manager.Name, CC_Sales_Rep__r.Manager.Alias, CC_Sales_Rep__r.Manager.isActive, CC_Sales_Rep__r.Manager.CC_LOA__c, CC_Sales_Rep__r.Manager.PS_Purchasing_LOA__c,
    		  	CC_Sales_Rep__r.Manager.Manager.Name, CC_Sales_Rep__r.Manager.Manager.Alias, CC_Sales_Rep__r.Manager.Manager.isActive, CC_Sales_Rep__r.Manager.Manager.CC_LOA__c, CC_Sales_Rep__r.Manager.Manager.PS_Purchasing_LOA__c, 
    		  	CC_Sales_Rep__r.Manager.Manager.Manager.Name, CC_Sales_Rep__r.Manager.Manager.Manager.Alias, CC_Sales_Rep__r.Manager.Manager.Manager.isActive, CC_Sales_Rep__r.Manager.Manager.Manager.CC_LOA__c, CC_Sales_Rep__r.Manager.Manager.Manager.PS_Purchasing_LOA__c 
    		 From CC_Sales_Rep__c 
    		 Where Id in :salesRepIds]
    	){
    			
    		system.debug('salesRep: ' + salesRep);
    		system.debug('salesManager: ' + salesRep.CC_Sales_Rep__r.Manager);
    		system.debug('salesDirector: ' + salesRep.CC_Sales_Rep__r.Manager.Manager);
    		system.debug('salesVP: ' + salesRep.CC_Sales_Rep__r.Manager.Manager.Manager);
    		
    		SalesHierarchy salesHierarchy = new SalesHierarchy();
    		salesHierarchy.salesRep = salesRep.CC_Sales_Rep__r.isActive ? salesRep.CC_Sales_Rep__r : null;
    		salesHierarchy.salesManager = salesRep.CC_Sales_Rep__r.Manager.isActive ? salesRep.CC_Sales_Rep__r.Manager : null;
    		salesHierarchy.salesDirector = salesRep.CC_Sales_Rep__r.Manager.Manager.isActive ? salesRep.CC_Sales_Rep__r.Manager.Manager : null;
    		salesHierarchy.salesVP = salesRep.CC_Sales_Rep__r.Manager.Manager.Manager.isActive ? salesRep.CC_Sales_Rep__r.Manager.Manager.Manager : null;
    		
    		system.debug('salesHierarchy: ' + salesHierarchy);
    		
    		salesHierarchyMap.put(salesRep.Id, salesHierarchy);
    	}
	
    	return salesHierarchyMap;
    }
}