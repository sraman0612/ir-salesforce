public class LGD_CaseApprovalProcess {
private static Boolean approvalSubmitted= false;
    
    
        @InvocableMethod(label='Update Approver' description='Update Approvers in Case' category='Case')
    public static void updateApprover(List<ID> ids) {
        
        List<Case> caseList = [SELECT Id, Business_Region_Country__c, Channel__c, Business__c, 
                                      Approver_Level_2__c, Approver_Level_3__c,
                                     List_Price__c
                                     From Case WHERE id IN :ids];
        
        
        Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt> approvalMdt = L_GD_COMP_Opportunity_Approval_Process__mdt.getAll();
        
        
        List<Case> caseListToUpdate = new List<Case>();
        List<String> userEmails  = new List<String>(); 
        
        System.debug('caseList >>'+caseList);
        Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt> mapOppMetadata = new Map<String, L_GD_COMP_Opportunity_Approval_Process__mdt>();
        for(Case cs :caseList){
            for(L_GD_COMP_Opportunity_Approval_Process__mdt mdt : approvalMdt.values()){
                if( mdt.Business__c == cs.business__c && mdt.Process__c == 'ETO'){
                       userEmails.add(mdt.Level_2__c);
                       userEmails.add(mdt.Level_3__c);
                       mapOppMetadata.put(cs.Id, mdt); 
                   }
            }
            
        } 
        
        System.debug('mapOppMetadata >>'+mapOppMetadata);
        System.debug('userEmails >>'+userEmails);
        List<User> users = [Select id, email From User Where email IN : userEmails];
        System.debug('users >>'+users);
        Map<String, Id> mapcsIdNextApprover = new Map<String, Id>();
        
        for(Case cs :caseList){
            for(String csId: mapOppMetadata.keySet()){
                if(csId == cs.id){
                    for(User approvers : users){
                        
                        if(approvers.email == mapOppMetadata.get(csId).Level_2__c ) {
                            cs.Approver_Level_2__c = approvers.id;
                        }
                        if(approvers.email == mapOppMetadata.get(csId).Level_3__c ) {
                            cs.Approver_Level_3__c = approvers.id;
                        }
                    }  
                   System.debug('caseListToUpdate >>'+caseListToUpdate);
                    caseListToUpdate.add(cs);
                }
            }
            
        }
        
        
        try{
                System.debug('approvalSubmitted >>'+approvalSubmitted);
            if(!approvalSubmitted){
                Update caseListToUpdate;
                System.debug('After Update >>'+caseListToUpdate);
                List<Approval.ProcessSubmitRequest>   approvalRequests = new   List<Approval.ProcessSubmitRequest>();
                Approval.ProcessSubmitRequest approvalRequest;
                
                for(Case cs : caseListToUpdate){
                                       
                    if(cs.List_Price__c < 50000){
                        System.debug('cs.List_Price__c >>'+cs.List_Price__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from < 50000');
                        approvalRequest.setObjectId(cs.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Case_ETO_Approval_List_price_50_K');
                        approvalRequest.setNextApproverIds(new Id[] {mapcsIdNextApprover.get(cs.id)});
                        approvalRequests.add(approvalRequest);
                    }
                    else if((cs.List_Price__c >= 50000 && cs.List_Price__c < 300000)){
                                
                        System.debug('cs.List_Price__c >>'+cs.List_Price__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from 50000 < List_Price__c < 300000');
                        approvalRequest.setObjectId(cs.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Case_ETO_50k_List_price_300k');
                        approvalRequest.setNextApproverIds(new Id[] {mapcsIdNextApprover.get(cs.id)});
                        approvalRequests.add(approvalRequest);
                    }
                    else if(cs.List_Price__c >= 300000){
                        System.debug('cs.List_Price__c >>'+cs.List_Price__c);
                        approvalRequest   = new Approval.ProcessSubmitRequest();
                        approvalRequest.setComments('Submitted from List_Price__c > 300000');
                        approvalRequest.setObjectId(cs.Id);
                        approvalRequest.setProcessDefinitionNameOrId('Case_ETO_300k_List_price');
                        approvalRequest.setNextApproverIds(new Id[] {mapcsIdNextApprover.get(cs.id)});
                        approvalRequests.add(approvalRequest);
                    }
                    else{
                        return;
                    }
                    
                }
                
                System.debug('approvalRequests >>'+approvalRequests);
                // Submit the approval request
                Approval.ProcessResult[] result   =  Approval.process(approvalRequests);
                approvalSubmitted = True;
            }
            
        }catch(Exception ex){
           	
        }
       
        
    }
    
    
}