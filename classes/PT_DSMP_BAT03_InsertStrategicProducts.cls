global class PT_DSMP_BAT03_InsertStrategicProducts implements Schedulable, Database.Batchable<sObject>  {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : PT_DSMP_BAT03_InsertStrategicProducts
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 18-Jan-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

    global List<PT_DSMP_Strategic_Product__c> lstObject = new List<PT_DSMP_Strategic_Product__c>();

    global PT_DSMP_BAT03_InsertStrategicProducts(List<PT_DSMP_Strategic_Product__c> lstObjectInput){
        lstObject = lstObjectInput;
    }

	global Iterable<sObject> start(Database.BatchableContext BC) {

		System.debug('mgr test lstObject ' +  lstObject);

        return lstObject;
    }


	global void execute(Database.BatchableContext BC, List<PT_DSMP_Strategic_Product__c> lstObject) {
        System.debug('mgr execute lstObject ' + lstObject.size());

        try {

        	insert lstObject;

        } catch(Exception e){
            System.debug('mgr Exception e ' + e);
        }
    }

	global void finish(Database.BatchableContext BC) {
        
    }

 	// Method to schedule batch
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new PT_DSMP_BAT03_InsertStrategicProducts(lstObject));
    }

}