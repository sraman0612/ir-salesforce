@istest 
public class CC_PavilionMarketingVideosCnt_Test
{
    static testmethod void UnitTPavilionMarketingVideos()
    { 
        // Creation Of Test Data
        list<selectOption> catOpt= new list<selectOption>();
        List<ContentVersion> contentList = new List<ContentVersion>();
        ContentVersion testContentInsert = TestUtilityClass.createContentVersion('Marketing Videos');
        insert testContentInsert;
        contentList.add(testContentInsert);
        RecordType ContentRT = [select id from RecordType where Name = 'Marketing Videos'];
        system.assertEquals(testContentInsert.RecordTypeId,ContentRT.Id);
        CC_PavilionTemplateController controller;
        catOpt.add(new SelectOption('All', 'All')); 
        catOpt.add(new SelectOption('Product', 'Product'));
        catOpt.add(new SelectOption('Quality', 'Quality'));
        // Performing unit tests for CC_PavilionMarketingVideosCnt.
        CC_PavilionMarketingVideosCnt objvideo=new CC_PavilionMarketingVideosCnt(controller);        
        Test.StartTest();           
        CC_PavilionMarketingVideosCnt.getVideoCategories();
        system.assertnotEquals(CC_PavilionMarketingVideosCnt.getVideoCategories(),catOpt);
        CC_PavilionMarketingVideosCnt.getVideoData('test'); 
        system.assertnotEquals(CC_PavilionMarketingVideosCnt.getVideoData('test'),contentList);                
        Test.StopTest();
        
    }
    
}