@isTest
public class CC_batchDeleteCleanup_Test {
  static testmethod void test1(){
    Id p = [select id from profile where name= 'CC_System Administrator' ].id;
    User sysAdminUser = TestUtilityClass.createNonPortalUser(p);
    insert sysAdminUser; 
    System.runAs(sysAdminUser) {
      CC_Statements__c st=TestUtilityClass.createStatement();
      insert st;
      CC_Vehicle__c vh=TestUtilityClass.createVehicle();
      insert vh;
      CC_Product_Rule__c pr=TestUtilityClass.createProdRule();
      insert pr;
      Product2 prd=TestUtilityClass.createProduct2ForDelete();
      insert prd;
      SchedulableContext sc;
      CC_scheduledDeleteCleanup scheduledDeleteCleanup=new CC_scheduledDeleteCleanup();
      test.startTest();
      scheduledDeleteCleanup.execute(sc);   
      test.stopTest();
    }   
  }
}