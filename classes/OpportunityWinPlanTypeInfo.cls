public class OpportunityWinPlanTypeInfo {
    
    private static OpportunityWinPlanTypeInfo instance = null;
    public Map<Id,Schema.RecordTypeInfo> recordTypeInfoById {get; private set;}
    
    private OpportunityWinPlanTypeInfo(){
            Schema.DescribeSObjectResult opportunityWinPlanSchema = OpportunityWinPlan__c.sObjectType.getDescribe();
			// Get all record types for object mapped by record type Id
			recordTypeInfoById = opportunityWinPlanSchema.getRecordTypeInfosById();
    }
    
    public static OpportunityWinPlanTypeInfo getInstance(){
        
        // There should be only one instance of this object so implemented via the singleton design pattern.
        if (instance == null){
            instance = new OpportunityWinPlanTypeInfo();
        }
        
        return instance;
    }
}