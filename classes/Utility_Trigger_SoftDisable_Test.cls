//
// (c) 2015, Appirio Inc
//
// Test class for Utility to "soft disable" triggers
//
// May 8, 2015     George Acker     original
//
@isTest
private class Utility_Trigger_SoftDisable_Test {
    
    // Method to test utility
    static testMethod void testSoftDisableTrigger() {
        // nothing should be disabled off the bat
        Utility_Trigger_SoftDisable u = new Utility_Trigger_SoftDisable('Account');
        system.assert(!u.insertDisabled(),'Insert should not be disabled');
        system.assert(!u.updateDisabled(),'Update should not be disabled');
        system.assert(!u.deleteDisabled(),'Delete should not be disabled');
        system.assert(!u.undeleteDisabled(),'Undeleteshould not be disabled');
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(name = 'Account');
        insert custSetting;
        
        // after we insert a record, still nothing should be disabled
        u = new Utility_Trigger_SoftDisable('Account');
        system.assert(!u.insertDisabled(),'Insert should not be disabled');
        system.assert(!u.updateDisabled(),'Update should not be disabled');
        system.assert(!u.deleteDisabled(),'Delete should not be disabled');
        system.assert(!u.undeleteDisabled(),'Undeleteshould not be disabled');
        
        custSetting.disable_insert__c = true;
        custSetting.disable_update__c = true;
        custSetting.disable_delete__c = true;
        custSetting.disable_undelete__c = true;
        update custSetting;
        
        // when we update the disabled fields to true, we should now see them as disabled
        u = new Utility_Trigger_SoftDisable('Account');
        system.assert(u.insertDisabled(),'Insert should be disabled');
        system.assert(u.updateDisabled(),'Update should be disabled');
        system.assert(u.deleteDisabled(),'Delete should be disabled');
        system.assert(u.undeleteDisabled(),'Undeleteshould be disabled');
    }
}