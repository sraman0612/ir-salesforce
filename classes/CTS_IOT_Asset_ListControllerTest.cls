/**
 * Test class of CTS_IOT_Asset_ListController
 **/
@isTest
private class CTS_IOT_Asset_ListControllerTest {
	
    @testSetup
    static void setup(){

        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;

        CTS_IOT_Frame_Type__c frameType = CTS_TestUtility.createFrameType(true, 'Centac,1BCVM2', 'Compressor');

        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc.Id, assetRecTypeId, false);   
        asset1.IRIT_RMS_Flag__c = true;
        asset1.IRIT_Frame_Type__c = frameType.Name;
        asset1.CTS_IOT_Frame_Type__c = frameType.Id;
        
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '12345', acc.Id, assetRecTypeId, false);   
        asset2.IRIT_RMS_Flag__c = false;
        asset2.IRIT_Frame_Type__c = null;
        asset2.CTS_IOT_Frame_Type__c = null;

        insert new Asset[]{asset1, asset2};
    }

    @isTest
    static void testGetInit(){

        List<Asset> assets = [Select Id, AccountId From Asset Order By Name ASC];

        Test.startTest();

        CTS_IOT_Asset_ListController.InitResponse initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, '', '');
        system.assertEquals(2, initRes.assets.size());
        system.assertEquals(2, initRes.totalRecordCount);

        initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, 'Compressor', '');
        system.assertEquals(1, initRes.assets.size());
        system.assertEquals(1, initRes.totalRecordCount);   
        
        initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, 'Dryer', '');
        system.assertEquals(0, initRes.assets.size());
        system.assertEquals(0, initRes.totalRecordCount);  
        
        initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, '', 'True');
        system.assertEquals(1, initRes.assets.size());
        system.assertEquals(1, initRes.totalRecordCount); 
        
        initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, '', 'False');
        system.assertEquals(1, initRes.assets.size());
        system.assertEquals(1, initRes.totalRecordCount); 
        
        initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).AccountId, 10, 0, 0, 'Compressor', 'True');
        system.assertEquals(1, initRes.assets.size());
        system.assertEquals(1, initRes.totalRecordCount); 

        Test.setFixedSearchResults(new Id[]{assets[0].Id, assets[1].Id});

        initRes = CTS_IOT_Asset_ListController.getInit('Test', assets.get(0).AccountId, 10, 0, 0, '', '');
        system.assertEquals(2, initRes.assets.size());
        system.assertEquals(2, initRes.totalRecordCount); 

        system.assertEquals(10, CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId()).User_Selected_List_Page_Size__c);

        Test.stopTest();

    }

    @isTest
    static void testGetInitException(){

        List<Asset> assets = [Select Id, AccountId From Asset Order By Name ASC];

        Test.startTest();

        CTS_IOT_Asset_ListController.forceError = true;
        CTS_IOT_Asset_ListController.InitResponse initRes = CTS_IOT_Asset_ListController.getInit(null, assets.get(0).Id, 10, 0, 0, '', '');
        system.assert(initRes != null);
        
        Test.stopTest();
    }    
}