/*
*
* $Revision:  $
* $Date: $
* $Author: $
*/
@isTest
public class CC_PavilionCustomerVIEWVdoCont_Test
{
    public static Account acc;
    public Static Contact con;
    public Static User PortalUser;
    public Static ContentVersion CV1,CV2;
    public Static List<ContentVersion> CVList=new List<ContentVersion>();
        
    static{
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
           //psettingList = TestUtilityClass.createPavilionSettings();
                
           //insert psettingList;
                   
        //creating test data starts here//
        test.startTest();
        //creating test data starts here//
        acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        acc.Type='Dealer';
        acc.CC_Global_Region__c='Japan';
        insert acc;
        
        System.assertEquals('Sample account', acc.Name);
        System.assertEquals('Dealer', acc.type);
        System.assertEquals('Japan', acc.CC_Global_Region__c);
        System.assertNotEquals(null, acc.Id);
        
        con= TestUtilityClass.createContact(acc.id);
        insert con;
        System.assertNotEquals(null, con.Id);
        
        PortalUser= TestUtilityClass.createNewUser('CC_PavilionCommunityUser',con.id);
        insert PortalUser;
        System.assertNotEquals(null, PortalUser.Id);
        CV1=TestUtilityClass.createContentVersion('CustomerVIEW');
        CV1.CC_Type__c='Dealer';
        CV2=TestUtilityClass.createContentVersion('CustomerVIEW');
        CV2.CC_Type__c='Dealer';
        CVList.add(CV1);
        CVList.add(CV2);
        System.assertNotEquals(null, CVList);
        insert CVList;
        System.assertNotEquals(null, CVList);
        Test.stopTest();
    }   
    static testMethod void UnitTest_CustomerVIEWVdoCont()
    {
        System.runAs(PortalUser){
            //Test.startTest();
            CC_PavilionTemplateController controller;
            CC_PavilionCustomerVIEWVdoCont CustView = new CC_PavilionCustomerVIEWVdoCont(controller);
            CC_PavilionCustomerVIEWVdoCont.getContentList();
            //Test.stopTest();
        }
        
    }
}