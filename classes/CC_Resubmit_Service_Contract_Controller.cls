public without sharing class CC_Resubmit_Service_Contract_Controller {
    
    @AuraEnabled
    public static String unlockContract(Id contractId){
    	
    	LightningResponseBase response = new LightningResponseBase();
    	
    	try{
    		
    		Contract c = [Select Id, CC_Contract_Status__c From Contract Where Id = :contractId];
    		
    		if (c.CC_Contract_Status__c != 'Approved'){
    			response.success = false;
    			response.errorMessage = 'The contract must be in Approved status to use this button.';
    		}
	    	else{	 
	    		   	
		    	c.CC_Unlock_Requested__c = true;
		    	c.CC_Unlock_Requestor__c = UserInfo.getUserId();
		    	update c;
    		}	
    	}
    	catch(Exception e){
    		response.success = false;
    		response.errorMessage = e.getMessage();
    	}
    	
    	return JSON.serialize(response);
    }
}