@isTest
public class CC_PavilionHelpcnt_Test{

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testMethod void testPavilionHelpcnt() {
        
        
         //creating test Data
        TestDataUtility testData = new TestDataUtility();
        user u = createPartnerUser(1);
            
        
        system.runAs(u){
            user u1 = [select id,ContactId, AccountId from user where id=:u.id];
           
       Case samplecase = TestUtilityClass.createCaseWithRecordType(u1.ContactId,'Pavilion Question','Club Car');
        insert samplecase;
        PavilionSettings__c thirdPavnSettings = TestUtilityClass.createPavilionSettings('_HelpQ',u.Id);
        insert thirdPavnSettings;
        PavilionSettings__c fourthPavnSettings = TestUtilityClass.createPavilionSettings('Others_HelpQ',u.Id);
        insert fourthPavnSettings;
        test.starttest();
        CC_PavilionTemplateController controller;
        CC_PavilionHelpcnt helpcnt = new CC_PavilionHelpcnt(controller);
        helpcnt.getcasecomponent();
        CC_PavilionHelpcnt.onloadcaseList();
        helpcnt.QuestionformPage();
        helpcnt.ClearForm();
        helpcnt.submitbutton();
        test.stoptest();
        }     
    }
    
    
     public static  user createPartnerUser(integer i){
        
     
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           

            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
            
             //creating sales rep here
              TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;
           
            //Create account
            Account account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,CC_Global_Region__c = 'Test',
                                                BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566',
                                  CC_Billing_County__c = county.id,BillingState = 'GA',CC_Shipping_Billing_Address_Same__c=true, CC_Sales_Rep__c = salesRepacc.Id);
            Database.insert(account);

            //Create contact
            contact contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = account.Id,
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            Database.insert(contact);

            //Create user
            Profile portalProfile = [SELECT Id FROM Profile where name ='Partner Community User' LIMIT 1];
            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+i,
                                 ContactId = contact.Id,ProfileId = portalProfile.Id,
                                 Alias = 'test123',Email = 'test12345@test.ingersollrand.com',
                                 EmailEncodingKey = 'UTF-8',LastName = 'McTesty',
                                 CommunityNickname = 'test12345',TimeZoneSidKey = 'America/New_York',country='USA',
                                 LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US');
            Database.insert(user1);
        }
       return user1; 
    }
}