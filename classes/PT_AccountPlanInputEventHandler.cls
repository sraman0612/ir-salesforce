public class PT_AccountPlanInputEventHandler {
  
  public static void updateAccount(List<PT_DSMP__c> triggerNew, Map<Id,PT_DSMP__c> oldMap){
    Map<Id, Id> account2PTOwnerMap = new Map<Id,Id>();
    Account [] accounts2Update = new List<Account>();
    for (PT_DSMP__c api : triggerNew){
      if(api.Stage__c=='First Level' && oldMap.get(api.Id).Stage__c=='Draft'){
        account2PTOwnerMap.put(api.Account__c,UserInfo.getUserId());
      }
    }
    for (Account a : [SELECT Id,PT_Owner__c,PT_Is_DVP__c FROM Account WHERE Id IN :account2PTOwnerMap.Keyset()]){
      a.PT_Owner__c = account2PTOwnerMap.get(a.Id);
      a.PT_Is_DVP__c = 'DVP';
      accounts2Update.add(a);
    }
    if(!accounts2Update.isEmpty()){
      update accounts2Update;
    }
  }
}