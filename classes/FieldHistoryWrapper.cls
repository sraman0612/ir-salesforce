public class FieldHistoryWrapper implements comparable {
  @AuraEnabled
  public Id Id;
  @AuraEnabled
  public datetime CreatedDate;
  @AuraEnabled
  public string CreatedBy;
  @AuraEnabled
  public string Field;
  @AuraEnabled
  public string OldValue;
  @AuraEnabled
  public string NewValue;
  
  public FieldHistoryWrapper(Id Id,datetime CreatedDate,string CreatedBy,string Field,string OldValue,string NewValue){
    this.Id=Id;
    this.CreatedDate=CreatedDate;
    this.CreatedBy=CreatedBy;
    this.Field=Field;
    this.OldValue=OldValue;
    this.NewValue=NewValue;
  }
  
  public Integer compareTo(Object compareTo) {
    FieldHistoryWrapper compareToFHW = (FieldHistoryWrapper)compareTo;
    if (CreatedDate == compareToFHW.CreatedDate) return 0;
    if (CreatedDate < compareToFHW.CreatedDate) return 1;
    return -1;       
  }
}