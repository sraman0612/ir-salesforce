public class CC_PavilionDownloadPricingUtilCtrl {
    public List<PriceBookEntry> PLISTEntries {get;set;}
    public List<PriceBookEntry> customerPriceBookEntries {get;set;}
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    public CC_PavilionDownloadPricingUtilCtrl(CC_PavilionTemplateController ctrl) {   
        PLISTEntries = getorderItemsFromPLISTPriceBookEntry();
        PLISTEntries = getorderItemsFromPLISTPriceBookEntry();
    }
    
    @ReadOnly
    @RemoteAction
    public static List<PriceBookEntry> getorderItemsFromPLISTPriceBookEntry() {
        // start
        Long startTime = System.currentTimeMillis();
        
        Set<String> theCrossRefSet = new Set<String>();

        for(CC_Parts_Cross_Reference__c elem : 
        [SELECT CC_Club_Car_Item__r.ProductCode
        FROM CC_Parts_Cross_Reference__c 
        WHERE Account__r.ID = :[Select AccountId FROM User WHERE ID=:userinfo.getuserid()].AccountId]) {
            theCrossRefSet.add(elem.CC_Club_Car_Item__r.ProductCode);         
        }
        
        Map<String, PriceBookEntry> PLISTEntriesMap = new Map<String, PriceBookEntry>();
        
        for(PriceBookEntry elem : 
        [SELECT ID, Product2.ProductCode, Product2.Name 
        FROM PriceBookEntry
        WHERE Product2.ProductCode NOT IN :theCrossRefSet AND PriceBook2ID = :[SELECT ID FROM Pricebook2 WHERE Name = 'PLIST'].ID]) {
            PLISTEntriesMap.put(elem.Product2.ProductCode, elem);
        }
        
        // end
        Long endTime = System.currentTimeMillis();
        //system.debug('timeElapsed '+ (endTime-startTime));
        
        return PLISTEntriesMap.values();
    }
}