global class CC_CarOrderVFDetailCntl{
  //thingw we know we use
  Public  List<CC_order_item__c> orderItemList {get;set;}
  public Map<String,Set<String>> orderItemVsCarfeatureList{get;set;}
  Public  List<CC_Car_Item__c> carItemList {get;set;}
  Public Static CC_Order__c carOrder{get;set;}
  Public Map<Decimal,CC_Order_Shipment__c> shipMap {get;set;}
  public List<OrderItemAndShipmentWrapper> wrapperList  {get; set;}
  public List<shipOnlyWrapper> shipOnlyLst  {get; set;}
  
  public CC_CarOrderVFDetailCntl(ApexPages.StandardController controller) {
    orderItemVsCarfeatureList = new Map<String,Set<String>>();
    shipMap = new Map<Decimal, CC_Order_Shipment__c>();
    wrapperList=new List<OrderItemAndShipmentWrapper>();
    shipOnlyLst=new List<shipOnlyWrapper>();
    CC_Car_Feature__c [] nonCarFeatLst = new List<CC_Car_Feature__c>();
    carOrder = (CC_Order__c)controller.getRecord();
    Id oId = carOrder.Id;
    carOrder =[SELECT Id,Name,
                      (SELECT Id, Name,Invoice_Date__c,Order__r.CC_Account__r.CC_Customer_Number__c,Invoice_Number__c,Order__c,CC_OrderShipmentItemsCount_hidden__c,
                              Order_Shipment_Key__c,Pro_Bill__c, Ship_Date__c,Shipment_Header_Number__c,Shipment_Number__c,Carrier__c,ShipmentItem_Quantity__c,CC_Invoice_Key__c
                         FROM Shipments__r)
                 FROM CC_Order__c
                WHERE Id=:oid];
    orderItemList = [SELECT Id,Name,CC_Product_Code__c,Product__r.Name,Product__r.ProductCode,Product__r.Description,CC_TotalPrice__c,Source_line_item_sequence__c,
                            CC_Description__c,CC_Quantity__c,CC_UnitPrice__c, CC_Warehouse__c  ,Order__r.CC_Customer_Invoice_Number__c,
                            (SELECT Comment__c FROM Order_Item_Comments__r),
                            (SELECT Id, Backorder_Quantity__c , Ship_Quantity__c FROM Order_Shipment_Items__r )
                       FROM CC_order_item__c
                      WHERE Order__c =:oId
                   ORDER BY Source_line_item_sequence__c ASC]; 
    carItemList = [SELECT Id, Name,Truck_Sequence__c,Order_Item__r.Order__r.CC_Carrier_ID__c,Serial_No_s__c,Load_Code__c,Number__c,Order_Item__r.CC_Product_Code__c,ASAP__c,
                          Critical_Date__c, Critical_ASAP_Comments__c,Delivery_Date_c__c,Scheduled_Ship_Date_Display__c,Estimated_Ship_Date__c,Car_Item_Key__c ,Truck_Quantity__c,
                          Original_Manufacture_Date__c, Load_Date__c,Load_Time__c, Comments__c, Add_on_Date__c,Drop_Ship_Sequence__c ,Order_Item__c, Ship_Date_Calc__c, 
                          Order_Item__r.Order__r.CC_Customer_Invoice_Number__c,Shipment_Header_Number__c,Order_Item__r.Product__r.Product_Line__c,
                          Order_Item__r.Order__r.CC_Request_Date__c,Manufacture_Due_Date__c, Load_Tracking__c, Notification_Subscriptions__c, Delivery_Date_Status__c, Delivery_Date_Display__c,
                          Koch_Last_Update_Date__c, Original_Delivery_Date_Display__c, Actual_Delivery_Date_Display__c, Koch_Scheduled_Delivery_Date__c, Koch_Load_Code_Out_Of_Sync__c,
                          (SELECT Product__r.Name,Product__r.Description,Product__r.ProductCode from Car_Features__r order by Feature_No__c) 
                     FROM CC_Car_Item__c 
                    WHERE Order_Item__r.Order__c =:oId];
    nonCarFeatLst = [SELECT Product__r.Name,Product__r.Description,Product__r.ProductCode,Order_Item__c
                       FROM CC_Car_Feature__c
                      WHERE CC_Car_Item__c=null and Club_Car_Order__c = :oid 
                   ORDER BY Feature_No__c];
    
            
    for(CC_Order_Shipment__c os : carOrder.Shipments__r){shipMap.put(os.Shipment_Header_Number__c,os);}
    
    for(CC_Car_Item__c ci: carItemList){
      List<String> carFTempList = new List<String>();
      Set<String> carFeatures = new Set<String>();
      //populate car item wrapper with shipment info if available
      if(null!=ci.Shipment_Header_Number__c && shipMap.containsKey(ci.Shipment_Header_Number__c)){
        CC_Order_Shipment__c osh = shipMap.get(ci.Shipment_Header_Number__c);
        shipMap.remove(ci.Shipment_Header_Number__c);
        if(osh.Invoice_Number__c!=null && osh.Invoice_Number__c!=''){
          String invKey=osh.Invoice_Number__c+'_'+osh.Order__r.CC_Account__r.CC_Customer_Number__c+'_'+string.valueOf(osh.Invoice_Date__c);
          OrderItemAndShipmentWrapper osw=new OrderItemAndShipmentWrapper(ci,osh,invKey);
          wrapperList.add(osw);
        } else {
          OrderItemAndShipmentWrapper osw1=new OrderItemAndShipmentWrapper(ci,osh,null);
          wrapperList.add(osw1);
        }
      } else {
        OrderItemAndShipmentWrapper osw2=new OrderItemAndShipmentWrapper(ci,null,null);
        wrapperList.add(osw2);
      }

      for(CC_Car_Feature__c cf2 : ci.Car_Features__r) {
        if(cf2.Product__r.Name !='' && cf2.Product__r.Name !=null ) {
          if(orderItemVsCarfeatureList.containsKey(ci.Order_Item__c)){
            orderItemVsCarfeatureList.get(ci.Order_Item__c).add(cf2.Product__r.Name);
          } else {
            orderItemVsCarfeatureList.put(ci.Order_Item__c, new Set<String>{cf2.Product__r.Name});
          }
        }
      }
    }
    if (!shipMap.isEmpty()){
      for(CC_Order_Shipment__c os : shipMap.Values()){
        shipOnlyWrapper sow=new shipOnlyWrapper(os);
        shipOnlyLst.add(sow);
      }
    }
    for(CC_Car_Feature__c ncf : nonCarFeatLst) {
      if(ncf.Product__r.Name !='' && ncf.Product__r.Name !=null ) {
        if(orderItemVsCarfeatureList.containsKey(ncf.Order_Item__c)){
          orderItemVsCarfeatureList.get(ncf.Order_Item__c).add(ncf.Product__r.Name);
        } else {
          orderItemVsCarfeatureList.put(ncf.Order_Item__c, new Set<String>{ncf.Product__r.Name});
        }
      }
    }
  }
  
  public PageReference redirector(){
    CC_Order__c o = [SELECT Id, RecordType.Name FROM CC_Order__c WHERE id = :ApexPages.currentPage().getParameters().get('id')];
    if(o.RecordType.Name !='Cars Ordering'){
      PageReference newPage = new PageReference('/' + o.id + '?nooverride=1');
      return newPage;
    }
    return null;
  }
    
  public class OrderItemAndShipmentWrapper {
      public CC_Car_Item__c CarOrdListmultW{get;set;}
      public CC_Order_Shipment__c carShipmentListW{get;set;}  
      public String invKeyString{get;set;}
      public OrderItemAndShipmentWrapper(CC_Car_Item__c caritm1,CC_Order_Shipment__c shpItem,String invSt) {
          this.CarOrdListmultW= caritm1;
          this.carShipmentListW= shpItem;
          this.invKeyString='/apex/CC_PavilionInvoiceDetail?invid=' + invSt;
      }   
      public String getDeliveryDateDisplayClasses(){
        return 'delivery-date-changed-' + CC_LoadTrackingUtilities.getDeliveryDateChanged(this.CarOrdListmultW);
      }       
      public String getCarrier(){
        return CC_LoadTrackingUtilities.getCarrier(CarOrdListmultW, (CarOrdListmultW != null ? CarOrdListmultW.Order_Item__r.Order__r : null), carShipmentListW);
      }      
    public String getCarrierLink(){

        if (this.carShipmentListW != null){
            return '/' + this.carShipmentListW.Id;
        }
        else{            
            return '/' + this.CarOrdListmultW.Id;
        }
    }
  }
  
  public class shipOnlyWrapper {
      public CC_Order_Shipment__c shipment{get;set;}  
      public String invKeyString{get;set;}
      public shipOnlyWrapper(CC_Order_Shipment__c shpItem) {
          this.shipment= shpItem;
          this.invKeyString=null!=shpItem.Invoice_Number__c&&''!=shpItem.Invoice_Number__c?'/apex/CC_PavilionInvoiceDetail?invid=' + shpItem.CC_Invoice_Key__c:'#';
      }    
  }
}