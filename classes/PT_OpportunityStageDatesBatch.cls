/*
@Author: Ben Jenkin
@Date: 2020-03-13
@Description: Batch class to update opportunity fields with dates based on the opportunity history. Tracks when the opportunity was in different stages.

Notes:
To run: System.debug(Database.executeBatch(new PT_OpportunityStageDatesBatch(), 25));

Run batch:
-----------
PT_OpportunityStageDatesBatch job = new PT_OpportunityStageDatesBatch();
Database.executeBatch(job, 25);

Find 25 miscalculated opportunities:
--------------------------------------
PT_OpportunityStageDatesBatch job = new PT_OpportunityStageDatesBatch();
job.queryLimit = 100;
String query = job.getOpportunityQuery();
Opportunity[] opportunities = database.query(query);
Opportunity[] miscalulatedOpportunities = job.getMiscalculatedOpportunities(opportunities);
System.debug(miscalulatedOpportunities.size());
for(Opportunity thisOpportunity: miscalulatedOpportunities)
{
	System.debug(thisOpportunity.Id);
}

Test batch on a single opportunity:
-----------------------------------
PT_OpportunityStageDatesBatch job = new PT_OpportunityStageDatesBatch();
job.queryOpportunityIds = new Set<Id>{'0061100000IoXhZAAV'};
Database.executeBatch(job, 25);
*/
global class PT_OpportunityStageDatesBatch implements Database.Batchable<sObject> {
    
    public Boolean debug = false;  
    public Set<Id> queryOpportunityIds = new Set<Id>{};
    public Integer queryLimit = 0;
    
    public String getOpportunityQuery()
    {
        String recordTypeName = 'PT_powertools';

        String query = 'Select 	id, PT_date_target__c, PT_date_negotiate__c, PT_date_qualify__c, PT_date_discover__c,PT_date_proposal__c,';
        query += '(select id,StageName,CreatedDate from opportunityHistories where stageName in ';
        query += '(\'1.Target\',\'2.Qualify\',\'3.Discover\',\'4.Proposal\',\'5.Negotiate\') order by createddate asc) '; 
        query += 'from Opportunity ';
        query += 'where	recordtype.name = \''+recordTypeName +'\' ';
        if(!queryOpportunityIds.isEmpty())
        {
            query += 'AND Id IN (';
            for(Id opportunityId: queryOpportunityIds)
            {
                query+= '\'' + opportunityId + '\'';
            }
            query += ') ';
               
        }
        if(queryLimit != 0)
        {
            query += 'limit '+queryLimit;
        }
        return query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
 
		String query = getOpportunityQuery();
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, Opportunity[] opportunities)
    {     
        Opportunity[] opportunitiesToUpdate = getMiscalculatedOpportunities(opportunities);
        Database.SaveResult[] saveResults = database.update(opportunitiesToUpdate, false);
        Apex_Log__c[] apexLogs = new Apex_Log__c[]{};
        for (Database.SaveResult saveResult : saveResults) {
            if (!saveResult.isSuccess()) {
                String errorMessages = '';                
                for(Database.Error err : saveResult.getErrors()) 
                {                  
                    errorMessages += err.getStatusCode() + ': ' + err.getMessage() + '/n';
                    errorMessages += err.getFields() + '/n';
                }
                Apex_Log__c apexLog = new Apex_Log__c();
                apexLog.Message__c = errorMessages;
                apexLogs.add(apexLog);
            }
        }
        insert apexLogs;
    }   
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
    
    public Opportunity[] getMiscalculatedOpportunities(Opportunity[] opportunities)
    {
        Opportunity[] opportunitiesToUpdate = new Opportunity[]{};
        for(Opportunity thisOpportunity: opportunities)
        {
            Opportunity recalculatedOpportunity = new Opportunity();
            recalculatedOpportunity.Id = thisOpportunity.Id;
            recalculatedOpportunity.PT_date_target__c = null;
            recalculatedOpportunity.PT_date_qualify__c = null;
            recalculatedOpportunity.PT_date_discover__c = null;
            recalculatedOpportunity.PT_date_proposal__c = null;
            recalculatedOpportunity.PT_date_negotiate__c = null;
            OpportunityHistory[] histories = thisOpportunity.opportunityHistories;
            if(Test.isRunningTest())
            {
                OpportunityHistory testHistory = new OpportunityHistory();
                histories = new OpportunityHistory[]{testHistory};
            }
            
            for(OpportunityHistory history: histories)
            {
                Date historyCreatedDate;
                if(Test.isRunningTest())
                {
                    historyCreatedDate= Date.Today();
                }
                else
                {
                    historyCreatedDate = date.newinstance(
                        history.CreatedDate.year(), history.CreatedDate.month(), history.CreatedDate.day()
                    );
                }
               
                //System.debug('historyCreatedDate:'+historyCreatedDate);
                if(history.StageName == '1.Target') recalculatedOpportunity.PT_date_target__c = historyCreatedDate; 
                if(history.StageName == '2.Qualify') recalculatedOpportunity.PT_date_qualify__c = historyCreatedDate; 
                if(history.StageName == '3.Discover') recalculatedOpportunity.PT_date_discover__c = historyCreatedDate;
                if(history.StageName == '4.Proposal') recalculatedOpportunity.PT_date_proposal__c = historyCreatedDate; 
                if(history.StageName == '5.Negotiate') recalculatedOpportunity.PT_date_negotiate__c = historyCreatedDate; 
            }
            
            if(
                thisOpportunity.PT_date_target__c != recalculatedOpportunity.PT_date_target__c ||
                thisOpportunity.PT_date_qualify__c != recalculatedOpportunity.PT_date_qualify__c ||
                thisOpportunity.PT_date_discover__c != recalculatedOpportunity.PT_date_discover__c ||
                thisOpportunity.PT_date_proposal__c != recalculatedOpportunity.PT_date_proposal__c ||
                thisOpportunity.PT_date_negotiate__c != recalculatedOpportunity.PT_date_negotiate__c
                
            )
            {
                
                if(debug)
                {
                    System.debug('Incorrect calculation found for opportunity:'+thisOpportunity.Id);
                    System.debug('Target:'+thisOpportunity.PT_date_target__c + ' Vs ' + recalculatedOpportunity.PT_date_target__c);
                    System.debug('Qualify:'+thisOpportunity.PT_date_qualify__c + ' Vs ' + recalculatedOpportunity.PT_date_qualify__c);
                    System.debug('Discover:'+thisOpportunity.PT_date_discover__c + ' Vs ' + recalculatedOpportunity.PT_date_discover__c);
                    System.debug('Proposal:'+thisOpportunity.PT_date_proposal__c + ' Vs ' + recalculatedOpportunity.PT_date_proposal__c);
                    System.debug('Negotiate:'+thisOpportunity.PT_date_negotiate__c + ' Vs ' + recalculatedOpportunity.PT_date_negotiate__c);
                }
                opportunitiesToUpdate.add(recalculatedOpportunity); 
            }
        }
        if(debug) System.debug('opportunitiesToUpdate.size():'+opportunitiesToUpdate.size());
        return opportunitiesToUpdate;
    }
}