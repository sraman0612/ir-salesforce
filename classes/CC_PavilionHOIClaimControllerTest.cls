/* Description: Test class for CC_PavilionHOIClaimController
 * Created Date: 31-08-2-16
 * Author: sudeep kumar
 * Modified History:
 *
 *
 */

 @istest
public class CC_PavilionHOIClaimControllerTest {
     static  Account testAccount; 
     static Contact testContact;
     static Contract testContract;
     static User testuser;
     static Hole_In_One_Claim__c hioclaim;
     static CC_Hole_in_One__c coopRequest;
     static Pavilion_Navigation_Profile__c navProfile;
     //static Attchment testAttchment;
     
     @istest
     static void testmethod1(){
         InitMethod();
         System.runAs(testuser){
             Test.startTest();
             ApexPages.currentPage().getParameters().put('id', coopRequest.Id);
             CC_PavilionTemplateController tempController;
             CC_PavilionHOIClaimController controller = new CC_PavilionHOIClaimController(tempController );
             System.assertEquals('' ,'');
             //CC_PavilionHOIClaimController.listofHOIRecords('123'); //@@ TODO send HIO id
             Test.stopTest();
         }
         
     }

     @istest
     static void testmethod2(){
         InitMethod();
         System.runAs(testuser){
             Test.startTest();
             //CC_PavilionHOIClaimController controller = new CC_PavilionHOIClaimController(new CC_PavilionTemplateController());
             //ApexPages.currentPage().getParameters().put('id', '123');
             System.assertEquals('' ,'');
             CC_PavilionHOIClaimController.listofHOIRecords(hioclaim.Id); //@@ TODO send HIO id
             Test.stopTest();
             ApexPages.currentPage().getParameters().put('id', coopRequest.Id);
             CC_PavilionTemplateController tempController;
             CC_PavilionHOIClaimController c = new CC_PavilionHOIClaimController(tempController );
              c.holeInOne  = new CC_Hole_in_One__c();
               c.UserAccountId  = '';
              c.lstDateOfFirstTrnmt  = Date.Today();
              c.trntStartDate = '';
              c.lstNameOfTrnmt = '';
              c.lstCourseName = '';
              c.lstVehicle  = '';
              c.lstCourseAdd = '';
              c.listHIO = new List<CC_Hole_in_One__c>();
              c.alreadyClaim = '';
         }
         
     }
     
     @istest
     static void testmethod3(){
         InitMethod();
         //System.runAs(testuser){
             Test.startTest();
             //CC_PavilionHOIClaimController controller = new CC_PavilionHOIClaimController(new CC_PavilionTemplateController());
             //ApexPages.currentPage().getParameters().put('id', '123');
             System.assertEquals('' ,'');
             List<CC_Hole_in_One__c> holeInOneList = [SELECT Id,Name FROM CC_Hole_in_One__c];
             
             if(holeInOneList != null && holeInOneList.size() > 0){
                CC_PavilionHOIClaimController.holeinClaim = holeInOneList[0].Id;
             }
             Attachment att = new Attachment();
             att.Name = 'Test Attachment';
             att.body = Blob.valueOf('testing attachments');
             CC_PavilionHOIClaimController.billAttachment = att;
         
             String jsonres = CC_PavilionHOIClaimController.listofHOIRecords(coopRequest.Id); 
             CC_PavilionHOIClaimController.submitHIOClaim(); //@@ TODO send HIO id
             
             CoopClaimParser cc1 = new CoopClaimParser();
             CoopClaimItems claimItems = new CoopClaimItems();
             claimItems.winnerName = 'abc';
             claimItems.firstMemName = 'abc';
             claimItems.SecondMemName = 'abc';
             claimItems.ThirdMemName = 'abc';
             claimItems.OfficialName = 'abc';
             claimItems.tourndate = String.ValueOf(Date.Today());
             claimItems.tourndate = String.ValueOf('09/09/2016');
             claimItems.vehSerialNum = '111';
             claimItems.clubCarInvNum = '111';
             claimItems.amtOnInv = '111';
             claimItems.comments = 'abc';
             claimItems.winnerPhNum = '111';
             claimItems.firstMemPhNum ='111';
             claimItems.secondMemPhNum ='111';
             claimItems.thirdMemPhNum = '111';
             claimItems.officialPhNum = '111';
             claimItems.conditionsApply = 'false';
            System.debug(cc1+''+claimItems); 
            cc1.CoopClaimItem = new  List<CoopClaimItems>();
            cc1.CoopClaimItem.add(claimItems);
            /*cc1.CoopClaimItem.add(claimItems);
            cc1.CoopClaimItem.add(claimItems);
            cc1.CoopClaimItem.add(claimItems);*/
            //cc1.CoopClaimItem.add(claimItems);
            //cc1.CoopClaimItem.add(claimItems);
            //cc1.CoopClaimItem.add(claimItems);
            
            String jsonData =  JSON.serialize(cc1, true);
            System.debug('@@123'+coopRequest.Id); 
            System.debug('@@123'+jsonData); 
              
            CC_PavilionHOIClaimController.submitHIOClaim(); //@@ TODO send HIO id //
             //CC_PavilionHOIClaimController.submitHIOClaim(coopRequest.Id, jsonres ,'FileName','Fiel Data'); //@@ TODO send HIO id
            Test.stopTest();
         //}
         
     }
     
     
     
     
     public static void InitMethod(){
         testAccount = TestUtilityClass.createAccount();
         insert testAccount;
         System.assertEquals('Sample Account',testAccount.Name);
         System.assertNotEquals(null, testAccount.Id);
         testContact = TestUtilityClass.createContact(testAccount.Id);
         insert testContact;
         System.assertEquals(testAccount.Id, testContact.AccountId);
         System.assertNotEquals(null, testContact.Id);        
         testContract = TestUtilityClass.createContractwithtype('Dealer/Distributor Agreement',testAccount.Id);
         insert testContract;
         coopRequest = new CC_Hole_in_One__c();
         //@@TO DO  add data into coop request.
            coopRequest.RecordtypeId = Schema.SObjectType.CC_Hole_in_One__c.getRecordTypeInfosByName().get('Hole in Request').getRecordTypeId();
            coopRequest.City__c ='City';
            coopRequest.Competitors__c = 'Compete1';
            coopRequest.Complete_Name_of_Tournament__c = 'TName';
            coopRequest.Contract__c = testContract.Id;
            coopRequest.Event_Name__c ='Event 1';
            coopRequest.Status__c = 'Active';
            coopRequest.Yards__c = '190';
           coopRequest.Date_of_the_first_day_of_Tournament__c  = date.today();
         insert coopRequest;
         hioclaim = new  Hole_In_One_Claim__c();
            hioclaim.Coop_Request__c = coopRequest.Id ;
            hioclaim.Club_Car_Invoice_Number__c  = '1212';
            hioclaim.First_Member_Phone_Number__c  = '12121212';
            hioclaim.Second_Member_Phone_Number__c = '12121212';
            hioclaim.Third_Member_Phone_Number__c = '12121212';
            hioclaim.Official_s_Phone_Number__c = '12121212';
            hioclaim.Official_s_Name__c  = 'Name 1';
            hioclaim.Status__c  = 'Submitted';
            hioclaim.Vehicle_Serial_Number__c  = '12121';
            hioclaim.Club_Car_Invoice_Number__c = '12121212';
            hioclaim.RecordtypeId = Schema.SObjectType.Hole_In_One_Claim__c.getRecordTypeInfosByName().get('Hole-In-One').getRecordTypeId();
         //@@TO DO  add data into HIO claim.
         insert hioclaim;
         testuser =  getTestUser('CC_PavilionCommunityUser');
         //insert testuser;
     }
     
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;
    }
     
     public Static User getTestUser(String ProfileName){
        Id p;
        User pavilionUser;
         Map<String,id> profMap = new Map<String,id>();
        list<Profile> profid = [SELECT id, Name FROM Profile];
        for(Profile pr:profid){
            profMap.put(pr.Name,pr.id);
        }
         Account sampleAccount = TestUtilityClass.createAccountWithRegionAndCurrency('USD','United States');
        insert sampleAccount;
        
        if(pavilionUser == null){
            if(ProfileName == null || ProfileName == ''){
              ProfileName  = 'CC_PavilionCommunityUser';
            }
            //p = [SELECT Id FROM Profile WHERE Name LIKE 'CC_PavilionCommunityUser%' Limit 1].Id;
            p = [select id from profile where name=:ProfileName].id;
            pavilionUser = new User();
            pavilionUser.alias = 'Sample';
            pavilionUser.email='SampleUser'+System.now().time().millisecond()+'@irco.com';
            pavilionUser.emailencodingkey='UTF-8';
            pavilionUser.lastname='Sample User';
            pavilionUser.languagelocalekey='en_US';
            pavilionUser.localesidkey='en_US';
            pavilionUser.profileid = p;
            pavilionUser.country='United States';
            pavilionUser.IsActive = true;
            pavilionUser.title='TestTitle';
            pavilionUser.Department='Marketing';
            pavilionUser.MobilePhone='8179947559';
            pavilionUser.Phone='8179947559';
            Contact c = testContact;
            //pavilionUser.accountId = c.Accountid;
            
            pavilionUser.CC_Pavilion_Navigation_Profile__c ='US_CA_SVC_ONLY_DLR__PRINCIPAL_GM_ADMIN';// 'testNavProfile';
            pavilionUser.ContactId =c.Id;
            pavilionUser.timezonesidkey='America/Los_Angeles';
            pavilionUser.username='SampleUser@gmail.com'+TestUtilityClass.randomWithLimit(8);
            pavilionUser.CC_View_Parts_Bulletins__c=true;
            pavilionUser.CC_Pavilion_Navigation_Profile__c='';
            pavilionUser.PS_Business_Unit_Description__c = 'CLUB CAR';
            pavilionUser.CC_Department__c='Marketing';
            insert pavilionUser;
            //navProfile = new  Pavilion_Navigation_Profile__c();
            //navProfile =TestUtilityClass.createPavilionNavigationProfile('testNavProfile');
           // insert navProfile;
        }
        return pavilionUser;
    }
    
    public class CoopClaimParser {
      public List <CoopClaimItems> CoopClaimItem;
     }
     public class CoopClaimItems {
        String winnerName;
        String firstMemName;
        String SecondMemName;
        String ThirdMemName;
        String OfficialName;
        String tourndate;
        String vehSerialNum;
        String clubCarInvNum;
        String amtOnInv;
        String comments;
        String winnerPhNum;
        String firstMemPhNum;
        String secondMemPhNum;
        String thirdMemPhNum;
        String officialPhNum;
        String conditionsApply;
     }
     

}