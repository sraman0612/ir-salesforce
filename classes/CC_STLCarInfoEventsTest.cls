@isTest
public class CC_STLCarInfoEventsTest {    

    @TestSetup
    private static void createData(){

        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        insert masterLease;

        CC_Short_Term_Lease__c lease1 = TestDataUtility.createShortTermLease();
        lease1.Shipping_To_Name__c = 'Lease1';
        lease1.Master_Lease__c = masterLease.Id;

        CC_Short_Term_Lease__c lease2 = TestDataUtility.createShortTermLease();
        lease2.Shipping_To_Name__c = 'Lease2';
        lease2.Master_Lease__c = masterLease.Id;

        insert new CC_Short_Term_Lease__c[]{lease1, lease2};

        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';

        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';

        insert new Product2[]{prod1, prod2};

        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = lease1.Id;
        insert car1;
    }
  
    private static testmethod void testPopulateMapicsInfo(){
        
        CC_Short_Term_Lease__c lease = [Select Id From CC_Short_Term_Lease__c Where Shipping_To_Name__c = 'Lease1'];
        Product2 prod1 = [Select Id From Product2 Where Name = 'Club Car 1'];
        Product2 prod2 = [Select Id From Product2 Where Name = 'Club Car 2'];

        system.assertEquals(1, [SELECT lineSequence__c FROM CC_STL_Car_Info__c WHERE Item__c = :prod1.Id].lineSequence__c);

        CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod2.Id);
        car2.Short_Term_Lease__c = lease.Id;
        insert car2;

        system.assertEquals(2, [SELECT lineSequence__c FROM CC_STL_Car_Info__c WHERE Item__c = :prod2.Id].lineSequence__c);
    }

    private static testmethod void testValidateCarItems(){
        
        CC_Short_Term_Lease__c lease1 = [Select Id From CC_Short_Term_Lease__c Where Shipping_To_Name__c = 'Lease1'];
        CC_Short_Term_Lease__c lease2 = [Select Id From CC_Short_Term_Lease__c Where Shipping_To_Name__c = 'Lease2'];
        Product2 prod1 = [Select Id From Product2 Where Name = 'Club Car 1'];
        Product2 prod2 = [Select Id From Product2 Where Name = 'Club Car 2'];

        system.assertEquals(1, [Select Count() From CC_STL_Car_Info__c Where Short_Term_Lease__c = :lease1.id]);

        // Create another line item with the same product/item on the first lease
        Boolean failure = false;

        try{
            CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod1.Id);
            car2.Short_Term_Lease__c = lease1.Id;
            insert car2;
        }
        catch(Exception e){
            failure = true;
            system.assert(e.getMessage().contains('Duplicate car item found'));
        }

        system.assertEquals(true, failure);

        // Create a new line item with the same product/item on the second lease
        CC_STL_Car_Info__c car3 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car3.Short_Term_Lease__c = lease2.Id;

        // Create a new line item with a different product/item on the first lease
        CC_STL_Car_Info__c car4 = TestDataUtility.createSTLCarInfo(prod2.Id);
        car4.Short_Term_Lease__c = lease1.Id;
           
        insert new CC_STL_Car_Info__c[]{car3, car4};
    }    
}