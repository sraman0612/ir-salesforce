global class PT_MassUpdateOpp implements Database.Batchable<sObject> {
    
    // 8feb2018 CG:PT Org merge: added PT record id      
    Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();         
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // CG: Added record type
        String query = 'select Id,PT_IR_Sales_Rep__c, OwnerId from Opportunity where RecordTypeId=\'' + PT_OpptyRecordTypeId + '\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, list<Opportunity> scope) {
        List<Opportunity> updates = new List<Opportunity>();
        List<User> users = [select Id, Name from User ];
        for(Opportunity opp : scope){            
            for(User u : users){
                if(opp.PT_IR_Sales_Rep__c == u.Name){
                    opp.OwnerId = u.Id;
                    updates.add(opp);
                } 
            }
        }
        if(updates.size() > 0){
            update updates;
        }
        
        system.debug('updates : '+updates);
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('**** Finish');
    }
    
}