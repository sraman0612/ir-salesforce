@isTest
public class PT_ParentOpportunityTriggerHandlerTest 
{
    private static testMethod void testTrigger()
    {
        Account testAccount = new Account();
        Id ptAcctRTId = [Select Id, DeveloperName from RecordType where DeveloperName = 'PT_powertools' and SobjectType='Account' limit 1].Id;
        Account a = new Account();
        testAccount.RecordTypeId = ptAcctRTId;
        testAccount.BillingCountry='Belgium';
        testAccount.Name = 'EYE_R_SEE_OH';
        testAccount.Type = 'Customer';
        testAccount.PT_Status__c = 'New';
        testAccount.PT_IR_Territory__c = 'North';
        testAccount.PT_IR_Region__c = 'EMEA';
        insert testAccount;
        
        Campaign testCampaign = new Campaign();
        testCampaign.Name = 'testCampaign';

        
        //This needs to be uncommented once PT_Campaign_Id__c has been changed from auto number to text and the related validation rule automated
        //Commented out to allow deployment. Once validation rule is active this will cause tests to fail.
        //Validation rule will be deployed as inactive and then activated once in production  
        //testCampaign.PT_Campaign_Id__c = '1234';
        insert testCampaign;
        
        String campaignQuery = 'select id, PT_Campaign_Id__c from Campaign where Name =\'testCampaign\'';
        testCampaign = database.query(campaignQuery);
        
        PT_Parent_Opportunity__c testParentOpportunity = new PT_Parent_Opportunity__c();
        testParentOpportunity.Account__c = testAccount.Id;

        insert testParentOpportunity;
    }
}