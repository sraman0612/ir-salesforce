public class ProductEventHandler {
  public Product2[] newLst = new Product2[]{};
  public Product2[] oldLst = new Product2[]{};
  public Map <ID, Product2> newMap = new Map<ID, Product2>{};
  public Map <ID, Product2> oldMap = new Map<ID, Product2>{};
  
  /* constructor */
  public ProductEventHandler(Product2[] triggered, Product2[] triggeredFrom,
                          Map<ID,Product2> triggerNewMap,Map<ID,Product2> triggerOldMap){
    newLst = triggered;
    oldLst = triggeredFrom;
    newMap = triggerNewMap;
    oldMap = triggerOldMap;
  }
  
  public void createStandardPriceBookEntry() {
    List<PricebookEntry> listPriceBookEntryToCreate = new List<PriceBookEntry>();
    Id standardPBId=Test.isRunningTest() ? Test.getStandardPricebookId() : [Select id from Pricebook2 where isStandard = true limit 1].Id;
    for (Product2 p : newLst) {
      if (p.Family =='Club Car Parts'){
        PricebookEntry pe = new PricebookEntry(UnitPrice=0.0,Product2Id=p.id,PriceBook2Id = standardPBId,isActive=true);
        listPriceBookEntryToCreate.add(pe);
      }
    }
    if(!listPriceBookEntryToCreate.isEmpty())
      insert listPriceBookEntryToCreate;
  }
  
  public void updateLocationInventory() {
    ProductItem [] productItems2Update = new List<ProductItem>();
    ProductItem [] productItems2Create = new List<ProductItem>();
    Map<String,Integer> productItemUpdates = new Map<String, Integer>();
    Map<String, Id> product2Map = new Map<String, ID>();
    Map<String, Id> locationMap = new Map<String, ID>();
    Location [] locationLst = new List<Location>();
    for (Product2 p : newLst) {
      if(p.Family=='Club Car Parts' && p.CC_location__c != null && p.CC_location__c != ''){
        product2Map.put(p.ERP_Item_Number__c,p.Id);
        locationMap.put(p.CC_Warehouse__c+p.CC_Location__c,null);
        Integer inventoryCount = Integer.valueOf(p.CC_Location_Quantity__c);
        productItemUpdates.put(p.CC_Warehouse__c +'_'+ p.CC_Location__c +'_'+ p.ERP_Item_Number__c, inventoryCount);
        //null out values so this won't run again due to some other update on a Product2 record
        p.CC_location__c='';
        p.CC_Warehouse__c='';
        p.CC_Location_Quantity__c=null;
      }
    }
    for(ProductItem pi : [SELECT Id,QuantityOnHand,Location.CC_Location_Key__c,CC_KEY__c FROM ProductItem WHERE CC_KEY__c IN :productItemUpdates.keyset()]){
      pi.QuantityOnHand = productItemUpdates.get(pi.CC_KEY__c);
      productItems2Update.add(pi);
      productItemUpdates.remove(pi.CC_KEY__c);
    }
    if(!productItemUpdates.isEmpty()){
      for(Schema.Location loc : [SELECT Id,CC_Location_Key__c FROM Location WHERE CC_Location_Key__c IN :locationMap.keyset()]){
        locationMap.put(loc.CC_Location_Key__c,loc.Id);
      }
    }
    for(String s : productItemUpdates.keyset()){
      String [] arr = s.split('_');
      String locKey = arr[0] + arr[1];
      String whse = arr[0];
      String loc = arr[1];
      String prodStr = arr[2];
      ProductItem pi = new ProductItem(QuantityOnHand=productItemUpdates.get(s),Product2Id=product2Map.get(prodStr));
      if(null!=locationMap.get(locKey)){
        pi.LocationId=locationMap.get(locKey);
      } else { //this is a new location
        Id parentLocId = [SELECT Id FROM Location WHERE Name = :whse].Id;
        Schema.Location newLocation = new Schema.Location(TimeZone='America/New_York',Name=loc,IsInventoryLocation=TRUE,ParentLocationId=parentLocId);
        insert newLocation;
        pi.LocationId=newLocation.Id;
      }
      productItems2Create.add(pi);
    }
    if(!productItems2Create.isEmpty()){
      insert productItems2Create;
    }
    if(!productItems2Update.isEmpty()){
      update productItems2Update;
    }

  }
}