/**
    @author Ben Lorenz
    @date 3JAN19
    @description Test class for CC_CaseSatisfactionSurveyCtrl.
*/

@isTest
private class CC_CaseSatisfactionSurveyCtrl_Test {
    
    @testSetup
    static void setup() {
        Case c = new Case();
        c.Subject = 'Test';
        insert c;
    }
    static testMethod void testCSat() {
        Case c = [SELECT Id, CaseNumber, CC_Case_Survey_Satisfaction_Level__c, CC_Case_Survey_Comments__c FROM Case];
        Test.setCurrentPage(new PageReference('/apex/CC_CaseSatSurveyVF?cId=' + c.Id + '&lvl=OK'));
        CC_CaseSatisfactionSurveyCtrl cnt = new CC_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CC_CaseSatisfactionSurveyCtrl.submitFeedback(c.Id, 'OK', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CC_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CC_Case_Survey_Satisfaction_Level__c == 'OK');
    }
    
    static testMethod void testCSatNoId() {
        Case c = [SELECT Id, CaseNumber, CC_Case_Survey_Satisfaction_Level__c, CC_Case_Survey_Comments__c FROM Case];
        Test.setCurrentPage(new PageReference('/apex/CC_CaseSatSurveyVF?lvl=OK'));
        CC_CaseSatisfactionSurveyCtrl cnt = new CC_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CC_CaseSatisfactionSurveyCtrl.submitFeedback('', 'OK', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CC_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CC_Case_Survey_Satisfaction_Level__c == null);
    }
    
    static testMethod void testCSatAlreadyDone() {
        Case c = [SELECT Id, CaseNumber, CC_Case_Survey_Satisfaction_Level__c, CC_Case_Survey_Comments__c FROM Case];
        c.CC_Case_Survey_Satisfaction_Level__c = 'OK';
        Test.setCurrentPage(new PageReference('/apex/CC_CaseSatSurveyVF?cId=' + c.Id + '&lvl=great'));
        CC_CaseSatisfactionSurveyCtrl cnt = new CC_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CC_CaseSatisfactionSurveyCtrl.submitFeedback(c.Id, 'OK', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CC_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CC_Case_Survey_Satisfaction_Level__c == 'OK');
    }
}