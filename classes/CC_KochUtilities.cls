public without sharing class CC_KochUtilities {

    public static final String EMPTY_VALUE = '-';

    public static void updateCarItemKochFields(CC_Car_Item__c carItem,                                                         
                                               String LastUpdate, 
                                               String ScheduledPickupDateTime,
                                               String ActualPickupDateTime, 
                                               String ScheduledDeliveryDateTime, 
                                               String LatestCalculatedDeliveryDate, 
                                               String RemainingMiles,
                                               String ActualDeliveryDateTime,
                                               String TimeOffset,
                                               String LoadCode){
        
        if (String.isNotBlank(TimeOffset)){
            carItem.Koch_Time_Zone_Offset__c = Decimal.valueOf(TimeOffset);
        }

        if (String.isNotBlank(RemainingMiles) && RemainingMiles != CC_KochUtilities.EMPTY_VALUE){
            carItem.Koch_Remaining_Miles__c = Decimal.valueOf(RemainingMiles);
        }
        else{
            carItem.Koch_Remaining_Miles__c = null;
        }

        carItem.Koch_Load_Code__c = LoadCode;
        carItem.Koch_Last_Update_Date__c = getDateTimeFromKochData(LastUpdate);
        carItem.Koch_Scheduled_Pickup_Date__c = getDateTimeFromKochData(ScheduledPickupDateTime);
        carItem.Koch_Actual_Pickup_Date__c = getDateTimeFromKochData(ActualPickupDateTime);
        carItem.Koch_Scheduled_Delivery_Date__c = getDateTimeFromKochData(ScheduledDeliveryDateTime);
        carItem.Koch_Estimated_Delivery_Date__c = getDateTimeFromKochData(LatestCalculatedDeliveryDate);
        carItem.Koch_Actual_Delivery_Date__c = getDateTimeFromKochData(ActualDeliveryDateTime); 
        carItem.Koch_Last_Check_Date__c = DateTime.now();
    }

    // Returns the provided GMT date/time string in GMT date/time format
    public static DateTime getDateTimeFromKochData(String val){

        DateTime dt;
        
        if (String.isNotBlank(val) && val != EMPTY_VALUE){

            String originalVal = val;

            try{

                // Must remove seconds from the Koch date/time string                
                String ampm = ' ' + val.substring(val.length()-2, val.length());
                val = val.subString(0, val.lastIndexOf(':'));
                val = val + ampm;
                dt = DateTime.parse(val);
              
                if (dt != null){

                    // Convert to GMT
                    dt = Datetime.newInstanceGMT(dt.date(), dt.time());
                }
            }
            catch (Exception e){
                system.debug('Unable to parse Koch date/time string (' + originalVal + '): ' + e.getMessage());
            }
        }

        return dt;
    }

    public static String getCarItemUniqueKeyPrefix(){
        return '01_1_'; // the unique key on all orders start with company 01 and type 1
    }

    public static String getKochCarItemUniqueKey(String ordRefNum){

        // If the key is incomplete and does not have a line and truck sequence, default to 1 for both
        if (String.isNotBlank(ordRefNum)){

            if (ordRefNum.endsWith('--')){
                ordRefNum = ordRefNum.removeEnd('--') + '-1-1';
            }

            ordRefNum = getCarItemUniqueKeyPrefix() + ordRefNum.replaceAll('-', '_');
        }

        return ordRefNum;
    }    

    public static String cleanNearestGPSLocation(String nearestGPSLocation){
        return String.isNotBlank(nearestGPSLocation) ? nearestGPSLocation.remove('[IGN:X]').remove('[IGN:N]').remove('[IGN:Y]') : nearestGPSLocation;
    }
}