/**
 * @description       : 
 * @author            : Ben Lorenz
 * @group             : 
 * @last modified on  : 10-01-2020
 * @last modified by  : Ben Lorenz
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   10-01-2020   Ben Lorenz   Initial Version
**/
public class CC_PavilionPartsOrderController {
    public static Map<String, List<String>> messageItemsMap;
    public static Map<String, List<String>> itemMessagesMap;
    public static Set<String> nonStandardItemsSet;
    public static Set<String> discontinuedItemsSet;
    public static Map<String, String> supersessionItemsMap;
    public static List<String> replacementProductIDs;
    public static Map<String, PricebookEntry> customerPriceMap;
    public static Map<String, PricebookEntry> plistPriceMap;
    public static Boolean CADPricing;
    public static Boolean qtyUpdateOnly;
    
    public CC_PavilionPartsOrderController(CC_PavilionTemplateController controller) {}
    
    @Remoteaction
    public static String[] getPartsCalendarDates(Date startRange, Date endRange)
    {
        /*** DO NOT REMOVE: fixes issue in select statement starting one day ahead when executed in salesforce; issue not see in execute anonymous. ***/
        startRange = Date.newinstance(startRange.year(), startRange.month(), startRange.day());
        endRange = Date.newinstance(endRange.year(), endRange.month(), endRange.day());
        /*******************/
        
        String[] partsCalendarDates = new String[] {};

        for (CC_Parts_Calendar__c CCPC: [select Date__c, Name from CC_Parts_Calendar__c 
                                        where Date__c >= :startRange AND Date__c <= :endRange
                                        order by Date__c])
        {
            partsCalendarDates.add(Datetime.NewInstance(CCPC.Date__c, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd', 'Universal'));
        }
        
        return partsCalendarDates;
    }
    @Remoteaction
    public static  List<CC_Order__c> getShoppingCarts(String aid){
    //@Priyanka Baviskar: Sorted on created date for ticket: #8533018 : Parts shopping cart table is no longer displaying newest carts first on 11/12/2019
        List<CC_Order__c> UseExistingCart = [SELECT ID,CreatedDate,CC_Shopping_Cart_Name__c FROM CC_Order__c WHERE CC_Status__c='Shopping Cart' and CC_Account__c = :aid ORDER BY CreatedDate desc];
        return UseExistingCart ;
    } 
    
    @Remoteaction
    public static void deleteCart(String cartId){
        CC_Order__c cart = [SELECT Id FROM CC_Order__c WHERE Id =:cartId];
        delete cart;
    }
    
    @Remoteaction
    public static void deleteItem(String itemId){
        CC_Order_Item__c ordItem = [SELECT Id FROM CC_Order_Item__c WHERE Id =:itemId];
        delete ordItem;
    }
    
    @Remoteaction
    public static String createCart(String aid, String cartname){
        CC_Order__c newCart = new CC_Order__c(CC_Account__c=aid,CC_Status__c='Shopping Cart',CC_Shopping_Cart_Name__c=cartname,CC_Warehouse__c='P',isSystemCreated__c=TRUE);
        insert newCart;
        return newCart.Id;
    }
    
    @Remoteaction
    public static void uploadTaxCertificate(string aid,string filename,blob taxcontent,string filetype) {
        CC_Tax_Certificate__c taxCert = new CC_Tax_Certificate__c(CC_Account__c=aid);
        insert taxCert;
        attachment attachment=new attachment(OwnerId=UserInfo.getUserId(),contenttype=filetype,Body=taxcontent,IsPrivate=false,name=filename,ParentId = taxCert.Id);
        insert attachment;
    }
    
    @Remoteaction
    public static CC_Order_Item__c [] getCartItems(String oid){
        CC_Order_Item__c [] itemLst = new List<CC_Order_Item__c>();
        itemLst = [SELECT ID, Name,Order__c, Product__r.ProductCode, CC_OEM__c, Product__r.Name, CC_UnitPrice__c, CC_Quantity__c, CC_TotalPrice__c
                    , Product__r.CC_Inventory_Count__c, Product__r.CC_Domestic_Parcel__c,Product__r.CC_International_Parcel__c,Product__r.CC_Value_Class__c
                    , List_Price__c,Order__r.CC_Shopping_Cart_Name__c, Product__r.CC_Lead_Time_Days__c
                   FROM CC_Order_Item__c 
                   WHERE Order__c=:oid];
        return itemLst;
    }
    
    @RemoteAction
    public static Map<String, List<String>> processSelected(String selectedItems,Boolean isUpdate,String pb, String aid,String cusnum, String oid) {
        CADPricing=cusnum.endsWith('21')?TRUE:FALSE;
        qtyUpdateOnly=isUpdate;
        Map<String, Integer> selectedItemsMap       = new Map<String, Integer>();
        Map<String, CC_Product_Rule__c> freeFreightMap;    
        Map<Id,String> accShippClassMap       = new Map<Id,String>();
        Set<String> prodCodeSet = new Set<String>();
        Object[] values = (Object[])System.JSON.deserializeUntyped(selectedItems);
        if(values.size()>0){
            for (Object id : values) {
                String [] lst = string.valueOf(id).split(':');
                integer quantity=lst.size()==1?1:integer.valueOf(lst[1].trim());
                selectedItemsMap.put(lst[0],quantity);
            }
        }
        Map<String, Integer> finalSelectedPartsProductMap = new Map<String, Integer>();
        if(!isUpdate){
            // get messaging, non-standard, discotinued and supersession items 
            messageItemsMap = new Map<String, List<String>>();
            itemMessagesMap = new Map<String, List<String>>();
            nonStandardItemsSet = new Set<String>();
            discontinuedItemsSet = new Set<String>();
            supersessionItemsMap = new Map<String, String>();
            replacementProductIDs = new List<String>();
            freeFreightMap        = new Map<String, CC_Product_Rule__c>();
            //See if Account is Domestic or INTL for ineligibility of Free Freight
            for(Account ac : [Select id,CC_Shipping_Classification__c from Account where Id = : aid]){
                String domOrIntl ='';
                if(ac.CC_Shipping_Classification__c == 'US' || ac.CC_Shipping_Classification__c == 'CA'){
                    domOrIntl = 'Domestic';
                    accShippClassMap.put(ac.id,domOrIntl);
                }
                else if(ac.CC_Shipping_Classification__c == 'INTL'){
                    domOrIntl = 'International';
                    accShippClassMap.put(ac.id,domOrIntl);  
                }
            }
            
            for (CC_Product_Rule__c pr : [SELECT CC_Type__c, CC_Obsolete_Item__r.ProductCode, CC_Replacement_Item__r.ProductCode, CC_Replacement_Item__r.ID, 
                                          CC_Message__c, CC_Last_Change_Date__c, CC_Sequence__c,CC_Obsolete_Item__r.CC_Battery__c,CC_Obsolete_Item__r.ERP_Item_Number__c,
                                          CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c,CC_Obsolete_Item__r.CC_International_Free_Freight__c,
                                          CC_Replacement_Item__r.CC_Battery__c,CC_Replacement_Item__r.ERP_Item_Number__c,
                                          CC_Replacement_Item__r.CC_Domestic_Free_Freight__c,CC_Replacement_Item__r.CC_International_Free_Freight__c
                                          FROM CC_Product_Rule__c
                                          WHERE Request_Delete__c = FALSE AND ((CC_Type__c='Replace' and CC_Substitute_Effective_Date__c < TODAY) OR (CC_Type__c='Message' and CC_Web_Display__c=true)) 
                                          ORDER BY CC_Type__c,CC_Obsolete_Item__r.ProductCode,CC_Last_Change_Date__c,CC_Sequence__c]) {
                                              if (pr.CC_Type__c == 'Message'){
                                                  if (messageItemsMap.containsKey(pr.CC_Obsolete_Item__r.ProductCode)) {
                                                      messageItemsMap.get(pr.CC_Obsolete_Item__r.ProductCode).add(pr.CC_Message__c);
                                                      freeFreightMap.put(pr.CC_Obsolete_Item__r.ProductCode,pr);
                                                  } 
                                                  else {messageItemsMap.put(pr.CC_Obsolete_Item__r.ProductCode,new List<String>{pr.CC_Message__c});freeFreightMap.put(pr.CC_Obsolete_Item__r.ProductCode,pr);}
                                              } else {
                                                  if(pr.CC_Replacement_Item__r.ProductCode == 'NON STANDARD'){
                                                      nonStandardItemsSet.add(pr.CC_Obsolete_Item__r.ProductCode);
                                                      freeFreightMap.put(pr.CC_Obsolete_Item__r.ProductCode,pr);
                                                  } 
                                                  else if (pr.CC_Replacement_Item__r.ProductCode == 'DISCONTINUED'){discontinuedItemsSet.add(pr.CC_Obsolete_Item__r.ProductCode);} 
                                                  else {
                                                      supersessionItemsMap.put(pr.CC_Obsolete_Item__r.ProductCode, pr.CC_Replacement_Item__r.ProductCode);
                                                      replacementProductIDs.add(pr.CC_Replacement_Item__r.ID);
                                                      freeFreightMap.put(pr.CC_Obsolete_Item__r.ProductCode,pr);
                                                  }
                                                  // for those not existing in supersession map
                                                  freeFreightMap.put(pr.CC_Obsolete_Item__r.ProductCode,pr);
                                              }
                                          }
            // populate selected order items productCode from parts cross reference
            for(CC_Parts_Cross_Reference__c pcr :[SELECT CC_Club_Car_Item__r.ProductCode,Name,CC_Club_Car_Item__r.CC_Battery__c,CC_Club_Car_Item__r.ERP_Item_Number__c,
                                                  CC_Club_Car_Item__r.CC_Domestic_Free_Freight__c,CC_Club_Car_Item__r.CC_International_Free_Freight__c 
                                                  FROM CC_Parts_Cross_Reference__c 
                                                  WHERE Name in :selectedItemsMap.keySet()]) {
                                                      selectedItemsMap.put(pcr.CC_Club_Car_Item__r.ProductCode,selectedItemsMap.get(pcr.Name)); //add the right one into the map
                                                      selectedItemsMap.remove(pcr.Name); //take the xref out
                                                  }
            for(String item : selectedItemsMap.keySet()) {
                Boolean isReplacement = false;
                // check if the item is in supersessionItemsMap
                if(supersessionItemsMap.containsKey(item)) {
                    isReplacement = true;
                    String finalReplacementItemCode = recursiveSupersessionItemCheck(supersessionItemsMap.get(item));
                    finalSelectedPartsProductMap.put(finalReplacementItemCode, selectedItemsMap.get(item));
                    addErrMsg(item,'Part Number '+item+' has been replaced with '+finalReplacementItemCode+' and entered into the shopping cart.');
                    //Logic for non eleigibility of free freight
                    /*Adding FF to Item of Free Freight to avoid
displaying message in the same line,this 'FF'
would be trimmed in the VF page*/
                    if(accShippClassMap.get(aid) == 'Domestic' && ((freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == FALSE) || 
                                                                   (freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == TRUE && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Battery__c == TRUE))){
                                                                       addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight.');
                                                                   }
                    else if(accShippClassMap.get(aid) == 'International' && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_International_Free_Freight__c == FALSE){
                        addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight.');          
                    }
                } else {
                    finalSelectedPartsProductMap.put(item, selectedItemsMap.get(item));
                }
                // check if the item is in nonStandardItemsSet
                if(nonStandardItemsSet.contains(item)) {
                    finalSelectedPartsProductMap.remove(item);
                    addErrMsg(item,'Part Number '+item+' is a non-standard item Please contact your Customer Experience team member for assistance.');
                    if(accShippClassMap.get(aid) == 'Domestic' && ((freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == FALSE) || 
                                                                   (freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == TRUE && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Battery__c == TRUE))){
                                                                       addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight.');
                                                                   }
                    else if(accShippClassMap.get(aid) == 'International' && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_International_Free_Freight__c == FALSE){
                        addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight.');
                    }       
                }
                // check if the item is in discontinuedItemsSet
                if(discontinuedItemsSet.contains(item)) { 
                    finalSelectedPartsProductMap.remove(item); // remove item
                    addErrMsg(item,'Part Number '+item+' has been discontinued.');
                }
                // check if the item is in messageItemsMap
                if(messageItemsMap.containsKey(item) && !isReplacement) { 
                    for(String elem : messageItemsMap.get(item)) {
                        if(elem != null) {
                            addErrMsg(item,elem);
                            if(accShippClassMap.get(aid) == 'Domestic' && ((freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == FALSE) || 
                                                                           (freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Domestic_Free_Freight__c == TRUE && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_Battery__c == TRUE))){
                                                                               addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight');
                                                                           }
                            else if(accShippClassMap.get(aid) == 'International' && freeFreightMap.get(item).CC_Obsolete_Item__r.CC_International_Free_Freight__c == FALSE){
                                addErrMsg(item+'FF',freeFreightMap.get(item).CC_Obsolete_Item__r.ProductCode + ' is not eligible for Free Freight');
                            } 
                        }
                    }
                }
                //CHECK IF THE PRODUCT IS NOT A PART OF ANY OF THESE AND DISPLAY ELIGIBILITY ON FREEFREIGHT
                if(finalSelectedPartsProductMap.containsKey(item) && !messageItemsMap.containsKey(item) && !discontinuedItemsSet.contains(item) 
                   && !nonStandardItemsSet.contains(item) && !supersessionItemsMap.containsKey(item))
                {prodCodeSet.add(item);}
            }
            for(Product2 pd :  [SELECT id,ProductCode,CC_Domestic_Free_Freight__c,CC_International_Free_Freight__c,ERP_Item_Number__c,CC_Battery__c   
                                FROM Product2 WHERE ProductCode IN : prodCodeSet AND RecordType.Name = 'Club Car' 
                                AND Family = 'Club Car Parts' AND isActive = TRUE]){
                                    if(accShippClassMap.get(aid) == 'Domestic' && ((pd.CC_Domestic_Free_Freight__c == FALSE) || 
                                                                                   (pd.CC_Domestic_Free_Freight__c == TRUE && pd.CC_Battery__c == TRUE))){
                                                                                       addErrMsg(pd.ProductCode+'FF',pd.ProductCode + ' is not eligible for Free Freight');
                                                                                       
                                                                                   }
                                    else if(accShippClassMap.get(aid) == 'International' && pd.CC_International_Free_Freight__c == FALSE){
                                        addErrMsg(pd.ProductCode+'FF',pd.ProductCode + ' is not eligible for Free Freight');
                                    } 
                                }
        } else {
            for(String item : selectedItemsMap.keySet()) {finalSelectedPartsProductMap.put(item, selectedItemsMap.get(item));}
        }  
        
        // get existing parts
        Map<String, CC_Order_Item__c> theExistingOrderItemsMap = new Map<String, CC_Order_Item__c>();
        for (CC_Order_Item__c oi : [SELECT CC_Product_Code__c, CC_Description__c, CC_UnitPrice__c, CC_Quantity__c, CC_TotalPrice__c 
                                    FROM CC_Order_Item__c 
                                    WHERE Order__r.ID = :oid]){
                                        theExistingOrderItemsMap.put(oi.CC_Product_Code__c, oi);
                                    }
        // get pricebook info for in scope items
        plistPriceMap = new Map<String, PricebookEntry>();
        customerPriceMap = new Map<String, PricebookEntry>();
        for(PricebookEntry pbe : [SELECT Name, ProductCode,UnitPrice, CC_Quantity_Discount__c, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c 
                                  FROM PricebookEntry 
                                  WHERE Pricebook2.PBNAME__c ='PLIST' and IsActive = true and Product2.ERP_Item_Number__c in :finalSelectedPartsProductMap.keySet()]) {
                                      plistPriceMap.put(pbe.ProductCode, pbe);        
                                  }
        // get customerPricebook items
        for(PricebookEntry pbe : [SELECT Name, ProductCode,UnitPrice, CC_Quantity_Discount__c, CC_CAD_Unit_Price__c, CC_Quantity_Price__c, CC_CAD_Quantity_Price__c 
                                  FROM PricebookEntry 
                                  WHERE (Pricebook2.PBNAME__c = :pb and IsActive = true and Product2.ERP_Item_Number__c in :finalSelectedPartsProductMap.keySet() )]) {
                                      customerPriceMap.put(pbe.ProductCode, pbe);        
                                  }
        
        //loop selected parts and upsert
        List<CC_Order_Item__c> partsToUpsert = new List<CC_Order_Item__c>();
        for(String elem : finalSelectedPartsProductMap.keySet()) {
            CC_Order_Item__c item = new CC_Order_Item__c();
            if(theExistingOrderItemsMap.containsKey(elem)) { // product is present in existing cart
                item = theExistingOrderItemsMap.get(elem);
                if(isUpdate) {item.CC_Quantity__c = finalSelectedPartsProductMap.get(elem);} 
                else {item.CC_Quantity__c += finalSelectedPartsProductMap.get(elem);}
                //check discount and show message to user
                if(customerPriceMap.containsKey(elem)) { // ITEM IS IN USER ASSIGNED PRICEBOOK
                    item = createItemforUpsert(item,customerPriceMap,elem);
                } else if(plistPriceMap.containsKey(elem)) { // ITEM IS IN LIST PRICEBOOK
                    item = createItemforUpsert(item,plistPriceMap,elem);
                }
                partsToUpsert.add(item);
            } else { // product is a net new add
                if(plistPriceMap.containsKey(elem)) {
                    item.Order__c = oid;
                    item.CC_Quantity__c = finalSelectedPartsProductMap.get(elem);
                    item.CC_Product_Code__c = plistPriceMap.get(elem).ProductCode;
                    item.CC_Description__c = plistPriceMap.get(elem).Name;
                    item.putSObject('Product__r', new Product2(ERP_Item_Number__c = elem));
                    item.List_Price__c = CADPricing ? plistPriceMap.get(elem).CC_CAD_Unit_Price__c : plistPriceMap.get(elem).UnitPrice;
                    if(customerPriceMap.containsKey(elem)) { // ITEM IS IN USER ASSIGNED PRICEBOOK
                        partsToUpsert.add(createItemforUpsert(item,customerPriceMap,elem));
                    } else { // ITEM IS IN LIST PRICEBOOK
                        partsToUpsert.add(createItemforUpsert(item,plistPriceMap,elem));
                    }
                } else {  // ITEM IS AN OUTLIER  
                    String errMsg = userinfo.getName() + ' In ' + cusnum + ' was not able to add ' + elem + ' to their cart because it does not exist in PLIST or ' + pb + ' PriceBooks';
                    apexLogHandler log = new apexLogHandler('CC_PavilionPartsOrderswithremote','processSelectedUtil', errMsg);
                    log.addOrder(oid);
                    log.saveLog();
                    addErrMsg(elem,elem + ' unavailable. Please Contact your Customer Experience team member for assistance.');
                }
            }
        }
        if(!partsToUpsert.isEmpty())
            upsert partsToUpsert;
        return itemMessagesMap;
    }
    
    @RemoteAction
    public static Map<String,String> getItemSearchList(String aid) {
        Map<String, String> itemMap = new Map<String,String>();
        //add supersessions
        for (CC_Product_Rule__c pr : [SELECT CC_Obsolete_Item__r.ProductCode,CC_Obsolete_Item__r.Name 
                                      FROM CC_Product_Rule__c
                                      WHERE CC_Type__c='Replace' and CC_Substitute_Effective_Date__c < TODAY AND Request_Delete__c = FALSE]) {
                                          itemMap.put(pr.CC_Obsolete_Item__r.ProductCode,' ::' + pr.CC_Obsolete_Item__r.Name);
                                      }                                  
        //add PLIST 
        for(PriceBookEntry elem : [SELECT ID, Product2.ProductCode, Product2.Name 
                                   FROM PriceBookEntry
                                   WHERE PriceBook2.PBNAME__c = 'PLIST' and IsActive = true]) {
                                       itemMap.put(elem.Product2.ProductCode, ' ::' + elem.Product2.Name);
                                   }
        //add xRefs 
        for(CC_Parts_Cross_Reference__c xr :[SELECT CC_Club_Car_Item__r.ProductCode,CC_Club_Car_Item__r.Name,Name 
                                             FROM CC_Parts_Cross_Reference__c 
                                             WHERE Account__r.ID = :aid]) {
                                                 itemMap.put(xr.CC_Club_Car_Item__r.ProductCode,xr.Name + '::' + xr.CC_Club_Car_Item__r.Name);         
                                             }
        return itemMap;
    }
    
    //@TODO HOW DO WE HANDLE MULTIPLE REPLACEMENTS???  AND NO NEED FOR RECURSION AS A==>B==>C will become A==>C
    public static String recursiveSupersessionItemCheck(String item) {
        String finalReplacementItem = item;
        if(supersessionItemsMap.containsKey(finalReplacementItem)) {
            finalReplacementItem = supersessionItemsMap.get(finalReplacementItem);
            recursiveSupersessionItemCheck(finalReplacementItem);
        }
        return finalReplacementItem;
    }
    
    public static void addErrMsg(String item, String msg){
        if (itemMessagesMap.containsKey(item)) {itemMessagesMap.get(item).add(msg);} 
        else {itemMessagesMap.put(item,new List<String>{msg});}
    }
    
    public static CC_Order_Item__c createItemforUpsert(CC_Order_Item__c i, Map<String, PricebookEntry> priceMap,String e) {
        Integer qtyDisc= Integer.valueOf(priceMap.get(e).CC_Quantity_Discount__c);
        boolean discEligible =(i.CC_Quantity__c >= qtyDisc) && (qtyDisc > 0)?TRUE:FALSE;
        if(CADPricing) {
            if(discEligible) {
                i.CC_UnitPrice__c = priceMap.get(e).CC_CAD_Quantity_Price__c;
                i.CC_TotalPrice__c = i.CC_Quantity__c * i.CC_UnitPrice__c;
            } else {
                i.CC_UnitPrice__c = priceMap.get(e).CC_CAD_Unit_Price__c;
                i.CC_TotalPrice__c = i.CC_Quantity__c * i.CC_UnitPrice__c;
            }
        } else {
            if(discEligible) {
                i.CC_UnitPrice__c = priceMap.get(e).CC_Quantity_Price__c;
                i.CC_TotalPrice__c = i.CC_Quantity__c * i.CC_UnitPrice__c;
            } else {
                i.CC_UnitPrice__c = priceMap.get(e).UnitPrice;
                i.CC_TotalPrice__c = i.CC_Quantity__c * i.CC_UnitPrice__c;
            }
        }
        if((qtyDisc > 0) && (i.CC_Quantity__c < qtyDisc) && (i.CC_Quantity__c != 0) && !qtyUpdateOnly){
            addErrMsg(e,'Quantity price break is available at '+ priceMap.get(e).CC_Quantity_Discount__c);
        }
        return i;
    }
}