public class CTS_OM_TranslatorController {
    @AuraEnabled 
    public static List<Object> translate(String recordId, List<String> fields, String relatedList) {
        
        String objectName = 'Case';        
        String query = '';
        String translationQs = '';
        Sobject sobj;
        
        List<Sobject> caseComments;
        List<Sobject> emailMessages;
        
        List<Sobject> relatedRecords;
        
        if(String.isBlank(recordId)) return null;
        
        if(fields != null && !fields.isEmpty()) {            
            query = 'SELECT ' + String.join(fields, ', ') + ' FROM ' + objectName + ' WHERE Id =: recordId';
            try {
                sobj = Database.query(query);
            } catch(Exception e) {
                System.debug('e**' + e.getMessage());
                throw new AuraException();
            }
            
            for(String f: fields) {
                translationQs += 'q: "' + sobj.get(f) + '",';
            }
        } else if(!String.isEmpty(relatedList)) {
            System.debug('relatedList**' + relatedList);
            if(relatedList == 'caseComments') {
                query = 'SELECT CommentBody FROM CaseComment WHERE ParentId = :recordId';
                try {
                    relatedRecords = Database.query(query);
                    System.debug('caseComments**' + caseComments);
                    if(relatedRecords.size() > 0) {
                        for(Sobject so: relatedRecords) {
                            translationQs += 'q: "' + so.get('CommentBody') + '",';
                        }   
                    }                        
                } catch(Exception e) {
                    System.debug('e**' + e.getMessage());
                    throw new AuraException();
                }                                                
            } else if(relatedList == 'emailMessages') {
                query = 'SELECT TextBody FROM EmailMessage WHERE ParentId = :recordId'; 
                
                try {
                    relatedRecords = Database.query(query);
                    System.debug('emailMessages**' + relatedRecords);
                    if(relatedRecords.size() > 0) {
                        for(Sobject so: relatedRecords) {
                            translationQs += 'q: "' + so.get('TextBody') + '",';
                        }   
                    }                        
                } catch(Exception e) {
                    System.debug('e**' + e.getMessage());
                    throw new AuraException();
                }
            }
            System.debug('translationQs**' + translationQs);         
        }        
        //Object translations = String.isEmpty(translationQs) ? null : CTS_TD_TranslatorController.callGoogleAPI(translationQs);
        return new List<Object>{relatedRecords};
    }
    
    public static Object callGoogleAPI(String translationQs) {
        String source = 'en'; // Not passing this param, let google detect language
        String target = UserInfo.getLanguage();//'es'; //UserInfo.getLanguage(); - translate in user's language
        if(target.length() > 2){
            target = target.substringBefore('_');
        }
        Google_Translation_Settings__c  googleSettings = Google_Translation_Settings__c.getInstance();
        String googleKey = googleSettings != null && googleSettings.google_API_Key__c != null ? googleSettings.google_API_Key__c : 'AIzaSyAsJpRLm4WISowTtyAl4AFit1v1jILRZD8'; 
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        String url = 'https://translation.googleapis.com/language/translate/v2?key=' + googleKey + '&target=' + target + '&format=html' + '&q='+EncodingUtil.urlEncode(translationQs, 'UTF-8');
       /* String body = '{' +
            translationQs +
            //'source: "' + source + '",' + //detect automatically
           'format: "html"' +
            '}';
        */
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        //req.setBody(body);
        try {
            res = h.send(req);
        } catch(Exception e) {
            System.debug('e**' + e.getMessage());
            throw new AuraException();
        }
        System.debug('res**' + res.getBody());
        return res.getBody();
    }
    
    @AuraEnabled 
    public static List<String> initialize(String recordId) {
        
        List<String> fields = new List<String>();        
        String fieldSetName = 'CTS_TranslationFields';
        String objectName = 'Case';
        SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult d = objToken.getDescribe();
        Map<String,Schema.FieldSet> fsMap = d.fieldSets.getMap();
        if(fsMap.containsKey(fieldSetName)) {
            for(Schema.FieldSetMember f : fsMap.get(fieldSetName).getFields()) {
                fields.add(f.getFieldPath());   
            }            
        }
        return fields;
    }
}