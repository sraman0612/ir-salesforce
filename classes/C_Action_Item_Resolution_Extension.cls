public class C_Action_Item_Resolution_Extension {
    
    public Action_Item_Comment__c theComment {get; set;}
    
    public C_Action_Item_Resolution_Extension (ApexPages.StandardController controller) {
        this.theCase = (Case) controller.getRecord();
        this.theCase = [SELECT Id, isClosed, CaseNumber,OwnerId, Subject, IC_Resolution_Notes__c, (SELECT Id, CaseNumber, IsClosed FROM Cases), ParentId, IC_Acton_Item_Owner__c FROM Case WHERE Id =: this.theCase.Id];
        this.subTabId = ApexPages.currentPage().getParameters().get('tabId');
        System.debug('this.subTabId: ' + this.subTabId + ' Apex ' + ApexPages.currentPage().getParameters().get('Id'));
        this.parentId = this.theCase.Id;
        this.AIEditorContent = this.theCase.IC_Resolution_Notes__c;
        this.startingFiles = new Set<Id>();
        
        String commentId = ApexPages.currentPage().getParameters().get('CommentId');
        
        if(String.isNotBlank(commentId)){
            this.theComment = [SELECT Id, Comment__c,Name FROM Action_Item_Comment__c WHERE Id =: commentId];
            this.AIEditorContent = this.theComment.Comment__c;
        }
        System.debug('this.theCase.ParentId: ' + this.theCase.ParentId);
        List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :this.theCase.Id];
        Set<String> linkedEntityIds = new Set<String>();
        for(ContentDocumentLink cdl : CDLs){
            startingFiles.add(cdl.ContentDocumentId);
        }
        
        System.debug('Staring Files:' + this.startingFiles);
        this.canClose = true;
        this.wrapMapF = new Map<Id,FileWrapper>();
        List<String> caseNumbers = new List<String>();
        /*
if(this.theCase.Cases != null){
for(Case c : this.theCase.Cases){
if(!c.IsClosed){
this.canClose = false;
caseNumbers.add(c.CaseNumber);
}
}
}

if(!this.canClose){       
String sprime = '';
for(String s : caseNumbers){
sprime += '<br/>' + s;
//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, s));
}
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'This case cannot be resolved because the following child cases are still open:', sprime));
}
*/        
        if(this.theCase.isClosed){
            this.canClose = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This case has already been closed',''));
        }
    }
    
    public String subTabId {get; private set;}
    
    public PageReference cancel() {    
        PageReference pgref = new PageReference(this.theCase.Id);  
        pgref.setRedirect(true);
        return pgref;
    }
    
    private Set<Id> startingFiles;
    public Map<Id,FileWrapper> wrapMapF;
    public Boolean hasParentFiles {get{
        Set<String> linkedEntityIds = new Set<String>();
        //Add Id for the Case to the list as well
        linkedEntityIds.add(theCase.Id);
        List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :this.theCase.Id];
        Set<String> currFileIds = new Set<String>();
        for (ContentDocumentLink cdl : CDLs) {
            currFileIds.add(cdl.ContentDocumentId);
        }
        List<ContentDocument> CDs = [SELECT Id FROM ContentDocument WHERE Id IN :currFileIds];
        
        return CDs.size() > 0;}
                                   set;}
    
    
    public Case theCase {get; set;}
    public String editorContent {get; set;}
    public String AIeditorContent {get; set;}
    @testVisible
    public String fileID {get; set;}
    public List<FileWrapper> filesWrapper {
        get{
            
            Set<String> linkedEntityIds = new Set<String>();
            
            //Add Id for the Case to the list as well
            linkedEntityIds.add(theCase.Id);
            List<ContentDocumentLink> CDLs = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :this.theCase.Id];
            Set<String> currFileIds = new Set<String>();
            for (ContentDocumentLink cdl : CDLs) {
                currFileIds.add(cdl.ContentDocumentId);
            }
            System.debug('linkedEntityIds:' + LinkedEntityIds);
            System.debug('CurrentFile' + currFileIds);
            List<ContentDocument> files = [SELECT Id, Title, Description, FileExtension, CreatedDate, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :currFileIds];
            System.debug('Files:' +files);
            List<FileWrapper> result = new List<FileWrapper>();
            if(files.size() > 0){
                for(ContentDocument cd : files){
                    if(!wrapMapF.containsKey(cd.Id)){
                        if(!startingFiles.contains(cd.Id)){
                            FileWrapper fw = new FileWrapper(cd, !startingFiles.contains(cd.Id), this);
                            wrapMapF.put(cd.Id,fw);
                            System.debug('wrapMapFNew: ' + wrapMapF.get(cd.Id).theFile.Title + ' - ' + wrapMapF.get(cd.Id).include);
                            result.add(fw);
                        }
                    }    
                    else{
                        if(!startingFiles.contains(cd.Id)){
                            result.add(wrapMapF.get(cd.Id));
                        }    
                        System.debug('wrapMapF: ' + wrapMapF.get(cd.Id).theFile.Title + ' - ' + wrapMapF.get(cd.Id).include);
                    }
                }
                this.hasParentFiles = true;
            }
            else{
                this.hasParentFiles = false;
            }
            System.debug('ending');
            return result;
        }
        set;}
    
    private Case parentCase;
    private Case orgCase;
    //private List<Attachment> parentAttachments {private get; private set;}
    private List<ContentDocumentLink> parentFiles {private get; private set;}
    public String parentId {get; private set;}    
    public boolean canClose {get; private set;}
    
    public PageReference sendContent() {
        System.debug('sendContent');
        System.debug(AIEditorContent);
        this.theCase.IC_Resolution_Notes__c = AIEditorContent;
        System.debug(this.theCase.IC_Resolution_Notes__c);
        return null;
    }
    
    public PageReference save() {
        
        System.debug('save');
        Action_Item_Comment__c aic;
        if(this.theComment == null){
            aic = new Action_Item_Comment__c();
            aic.Comment__c = AIEditorContent;
            aic.Case__c = this.theCase.Id;
        }
        else{
            aic = this.theComment;
            aic.Comment__c = AIEditorContent;
        }                
        
        //this.theCase.IC_Resolution_Notes__c = AIEditorContent;
        System.debug(this.theCase);
        //Updated By CG on 24th Nov 2022 for US SFAVPTECH-16
        Case parCase = new Case(Id = this.theCase.ParentId);
        parCase.Unread_Action_Items__c = false;
        this.theCase.Status = 'Pending Approval';
        System.debug('parCase >>'+parCase);
        
        this.theCase.Last_Commenter__c = UserInfo.getName();
        
        String recordType = [select id from RecordType where developername =: 'Internal_Case'].Id;
        if(recordType == theCase.RecordTypeId){
            System.debug('Case Owner ::: '+this.theCase.OwnerId);
            System.debug('Case User ID ::: '+ UserInfo.getUserId());
            System.debug('Case Requester ID ::: '+ this.theCase.IC_Acton_Item_Owner__c ); 
            
            if(this.theCase.IC_Acton_Item_Owner__c == UserInfo.getUserId()){                 
                this.theCase.Status= 'More Info Required'; 
                //Updated By CG on 24th Nov 2022 for US SFAVPTECH-16 
                parCase.Last_Commenter__c = UserInfo.getName();
                System.debug('parCase.Last_Commenter__c ::: '+ parCase.Last_Commenter__c);
            }
        }
        this.theCase.Unread_Action_Items__c = true;
        
        update this.theCase;
        upsert aic;
        
        //Action Item Comment Name
        String commentName = '';
        if(aic.id != NULL){
            commentName =[SELECT Name FROM Action_Item_Comment__c Where Id =: aic.Id].Name;
        }
        
        System.debug('Current UserId : ' + UserInfo.getUserId());
        System.debug('Action Item Requester : ' + this.theCase.IC_Acton_Item_Owner__c);
        
        if(UserInfo.getUserId() != this.theCase.IC_Acton_Item_Owner__c){
            //Case parCase = new Case(Id = this.theCase.ParentId);
            parCase.Unread_Action_Items__c = true;
        }
        
        // Updated by Mahesh Karadkar - #02900279 - 14Mar2024
        Integer iChildPA = [SELECT Count() FROM Case WHERE parentId =: parCase.Id and status = 'Pending Approval'];
        
        if(iChildPA > 0){
            parCase.Unread_Action_Items__c = True;
        }else{
            Integer iChildMIR = [SELECT Count() FROM Case WHERE parentId =: parCase.Id and status = 'More Info Required'];
            if(iChildMIR > 0){
                parCase.Unread_Action_Items__c = false;
            }
        }
        //#02900279 end
        
        //Updated By CG on 24th Nov 2022 for US SFAVPTECH-16
        database.update(parCase, false);
        List<ContentDocument> filesToDelete = new List<ContentDocument>();
        List<ContentDocument> filesToUpdate = new List<ContentDocument>();
        
        for( fileWrapper fw : filesWrapper){
            
            if(!fw.include){
                filesToDelete.add(fw.theFile);
            }
            
            if(fw.include){
                fw.theFile.Title = commentName+' - '+fw.theFile.Title;
                filesToUpdate.add(fw.theFile);               
            }            
        }
        
        if(filesToDelete.size() > 0){
            delete filesToDelete;
        }
        if(filesToUpdate.size() > 0){
            update filesToUpdate;
        }
        return null;
    }
    
    @RemoteAction 
    public static RemoteSaveResult saveImage (String filename, String imageBody) {
        
        // Get the ID of the folder we wish to keep these pasted images in. The folder should be public if images are to be viewed in external emails.
        Id attachmentsFolderId = [Select Id From Folder Where Name = 'Attachments' Limit 1][0].Id;
        
        // Create the document that keeps the image.
        Document doc = new Document();
        doc.FolderId = attachmentsFolderId;
        doc.Name = filename+'_'+DateTime.now();
        doc.Description = filename;
        doc.ContentType = 'image/png';
        doc.Type = 'png';
        doc.Body = EncodingUtil.base64Decode(imageBody);
        doc.IsPublic = true;
        
        // Save the document.
        Database.saveResult result = Database.insert(doc, false);
        
        // Create the URL to the image being saved. This should be org-agnostic.
        String baseOrgURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        Integer firstIndex = baseOrgURL.indexOf('.')+1;
        Integer secondIndex = baseOrgURL.indexOf('.', firstIndex);
        String orgBase = baseOrgURL.substring(firstIndex, secondIndex);
        String contentBaseURL = 'https://c.'+ orgBase +'.content.force.com/servlet/servlet.ImageServer?id=';
        String docID = ((String)doc.Id).substring(0, 15);
        String orgID = ((String)[Select Id, Name From Organization Limit 1][0].Id).substring(0, 15);
        String imageURL = contentBaseURL+docID+'&oid='+orgID;
        
        
        // Put the results of this operation in a RemoteSaveResult object.
        RemoteSaveResult newResult = new RemoteSaveResult();
        newResult.success = result.isSuccess();
        newResult.successMessage = result.isSuccess() ? imageURL : '';
        newResult.errorMessage = result.isSuccess() ? '' : result.getErrors()[0].getMessage();
        
        return newResult;
    } 
    
    
    public class RemoteSaveResult {
        public Boolean success;
        public String errorMessage;
        public String successMessage;
    }
    
    
    public class FileWrapper{
        private Boolean first = true;
        protected C_Action_Item_Resolution_Extension controller;
        public ContentDocument theFile {get; private set;}
        public Boolean include {get;
                                set{
                                    if(value == false && first){
                                        this.first = false;
                                    }
                                    else{
                                        this.include = value;
                                        this.controller.wrapMapF.put(this.theFile.Id,this);
                                    }
                                }
                               }
        
        public FileWrapper(ContentDocument f, Boolean inc, C_Action_Item_Resolution_Extension con){
            this.controller = con;
            this.theFile = f;
            this.include = inc;
        }
    }
    
    public void createNewFiles_Stage () {
        createNewFiles(fileId);
    }
    
    public void createNewFiles(String fileId) {
        //Get file
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :fileId].ContentDocumentId;
        System.debug('conDocId ====> '+conDocId);
        
        if (conDocId != null) {
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocId; //Add ContentDocumentId
            cDocLink.LinkedEntityId = parentId; //Add file parentId
            cDocLink.ShareType = 'V'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers'; //AllUsers, InternalUsers, SharedUsers
            insert cDocLink;
        }
    }
}