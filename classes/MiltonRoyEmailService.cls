public with sharing class MiltonRoyEmailService implements Messaging.InboundEmailHandler{
    // Email Service for the Milton Roy Order Center.
    // 
    // The purpose of the MiltonRoyEmailService Apex class is to process email messages sent from inContact with voice mail attachments.
    //
    // inContact records a customer's voice message and emails the voice message as a .wav file attachment; along with the 'Caller Id'
    // embedded in the body of the email to Salesforce.  The email is forwarded to a Saleforce email address that is associated with 
    // a Salesforce 'Email Service'; which is associated with the 'MiltonRoyEmailService' Apex class.
    // 
    // The 'MiltonRoyEmailService' Apex class will insert a case into Salesforce, if the voice message attached to the email
    // is greater that seven seconds in length (.wav file attachment is greater than 60,200 bytes).  The 'MiltonRoyEmailService' 
    // Apex class will then retrieve the 'Caller ID' from the body of the email and then query 'Contacts' for that phone number.  If
    // a contact is found, it will be associated with the case.
    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelop) {
        
        // Create an InboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

        // Parse email body in order to retrieve 'Caller Id'
        String [] emailBody = email.htmlBody.split('\n',0);
 
        Contact contact;
 		System.debug('inside Milton Roy Email Service');
        try {
            // Retrieve Caller Id
            System.debug('email Body');
            System.debug(emailBody);
            System.debug('Full Caller ' + emailBody[5]);
            String callerId = emailBody[5].substring(13,23);
 			System.debug('Caller Id : ' + callerId);
            String formattedCallerId = '('+ callerId.substring(0,3)+') '+callerId.substring(3,6)+'-'+callerId.substring(6,10);
            System.debug('formatted Phone Number : '+ formattedCallerId);
            String PSTContactRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Milton_Roy_Contact' LIMIT 1].Id;
            // Query contact by phone to determine if contact exists
            If ([select COUNT() from Contact where RecordTypeId =:PSTContactRecordTypeId AND (Phone =: callerId OR Phone =:formattedCallerId)] !=0) {
                contact = [select Id from Contact where RecordTypeId =:PSTContactRecordTypeId AND (Phone =: callerId OR Phone =:formattedCallerId)][0];
            }
            else {
                contact = new Contact();
            }

            // Build case
            buildCase(email, contact);
                
        }           
        catch(Exception e) {
            // Set the success value of result to 'FALSE'
            result.success = false;
 
            // Set message
            result.message = 'Failed';
        }
 
        // Return result
        return result;
    }

    private Messaging.InboundEmailResult buildCase(Messaging.InboundEmail email, Contact contact) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

        Case newCase;

        // Retrieve binary attachments
        //List<Attachment> binAttachments = getBinAttachments(email);
        
        //Retrive Attachments and insert salesforce files
        List<ContentDocument> binFiles = getFilesAttachment(email);

        // Create case if email attachment is longer than seven seconds - file length > 60,200 bytes

        // If(getMaxAttachSize(binAttachments) > 60200) {

            // Create new case
            newCase = new Case();

            if(contact.id <> null) {
                newCase.contactId = contact.Id;
            }
            // Set Case 'Case Owner'
        Group queueResult = [SELECT Id,DeveloperName FROM Group where Type = 'Queue' AND DeveloperName = 'MR_MRCustomer_Service' LIMIT 1][0];
       	 	if(queueResult <> null){
            newCase.OwnerId = queueResult.Id;
  			}
        	//queryTeamEscalation
        	Team_Escalation__c MRcustomerServiceTeam = [SELECT Id,Name FROM Team_Escalation__c WHERE Name = 'MR Customer Service' LIMIT 1];
        	if(MRcustomerServiceTeam <> null){
            newCase.Team__c = MRcustomerServiceTeam.Id;
       		 }

            // Set Case 'Case Record Type'
            newCase.RecordTypeId = [Select Id, SobjectType, Name,DeveloperName from RecordType where SobjectType = 'Case' and DeveloperName = 'Milton_Roy_Case_Management_Customer' Limit 1].Id;
			
        	//Set Form Name to 'Customer Service'
        	newCase.form_Name__c  = 'Customer Service Form';	
        
            // Set Case 'Type'
            newCase.type = 'Question';

            // Set Case 'Origin'
            newCase.Origin = 'Phone';

            // Set Case 'subject'
            newCase.subject = 'Milton Roy Abandoned Call';
 
            // Set Case 'description'
            If(email.htmlBody != Null && email.htmlBody != '') {

                // Remove HTML markup
                String emailBody = email.htmlBody.replaceAll('<P>','').replaceAll('</P>','');
                newCase.description = emailBody;
            }

            // Insert new case
            if(Schema.sObjectType.Case.isCreateable()) {
                insert newCase;
            }
 
            // Set parent id in attachments
           // SetAttachParent(binAttachments, newCase.id);
          
        	//Set COntentDocumentLink to all files
        	if(binFiles <> null){
            setContentDocumentLink(binFiles,newCase.Id);
        	}
        	
        
            // Insert attachments
           /* if(binAttachments != null) {
                if(Schema.sObjectType.Attachment.isCreateable()) {
                    insert binAttachments;
                }        
            }*/ 
        //} 
             
        // Set the success value of result to 'TRUE'
        result.success = true;

        return result;

    }
     
    
    private List<ContentDocument> getFilesAttachment(Messaging.InboundEmail email) {
       
        List<ContentVersion> contentVers = new List<ContentVersion>();
        Set<Id> contentVIdSet = new Set<Id>();
		List<ContentDocument> filesList = new List<ContentDocument>();
        // Retrieve binary attachments
        if(email.binaryAttachments != null) {
            for(Messaging.Inboundemail.BinaryAttachment bAttachment : email.BinaryAttachments) {
                
                ContentVersion conv = new ContentVersion();
                conv.versionData = bAttachment.body;
                conv.title =  bAttachment.fileName;
                conv.PathOnClient = bAttachment.fileName +'.wav';
                contentVers.add(conv);
                
            }
            if(Schema.sObjectType.ContentVersion.isCreateable()) {
                    insert contentVers;
                }
            for(ContentVersion conv : contentVers){
                if(conv.Id != null){
                    contentVIdSet.add(conv.Id);
                }
            }
            
            filesList = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId IN :contentVIdSet];
            
            
            
            
        }
		return filesList;
        
    } 

   /* private Long getMaxAttachSize (List<Attachment> binAttachments) {
        Long maxSize = 0;

        // Set length of longest attachment

        for(Attachment binAttach: binAttachments) {
            if(binAttach.Body.size() > maxSize) {
                maxSize = binAttach.Body.size();
            }
        }

        return maxSize;
    }*/

   
    
    private void setContentDocumentLink(List<ContentDocument> filesList, Id parentId){
        List<ContentDocumentLink> cLinksList = new List<ContentDocumentLink>();
        for(ContentDocument cFile : filesList){
            ContentDocumentLink clink = new ContentDocumentLink();
            clink.ContentDocumentId = cFile.Id;
            clink.LinkedEntityId = parentId;
            cLinksList.add(clink);
        }
        if(Schema.sObjectType.ContentDocumentLink.isCreateable()){
            insert cLinksList;
        }
    }
}