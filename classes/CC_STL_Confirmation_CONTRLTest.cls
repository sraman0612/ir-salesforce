@isTest
public with sharing class CC_STL_Confirmation_CONTRLTest {
    
    @TestSetup
    private static void createData(){
    	
    	CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
    	hcs.STL_CONF_ESIGN_TEMPLATEID__c = 'ABC123';
    	upsert hcs;
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;     
        
        TestDataUtility dataUtil = new TestDataUtility(); 
        
        User testUser = dataUtil.createIntegrationUser();
        testUser.LastName = 'Test_User_123';
        insert testUser;             
        
        Account acct = [Select Id From Account LIMIT 1];
        CC_Order__c ccOrder = TestUtilityClass.createNatAcctOrder(acct.id, acct.Id);
        insert ccOrder;
        
        Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
        prod1.CC_Item_Class__c = 'LCAR';
        Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
        prod2.CC_Item_Class__c = 'LCAR';     
        insert new Product2[]{prod1, prod2};
            
    	CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
    	insert salesRep;                    
            
        CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
        masterLease.Customer__c = acct.Id;
        insert masterLease;  
        
        CC_STL_Car_Location__c location = TestDataUtility.createCarLocation();    
        insert location;    
            
        CC_Short_Term_Lease__c stl1 = TestDataUtility.createShortTermLeaseToSendToMAPICS(masterLease.Id, location.Id, salesRep.Id);
        stl1.Lease_Category__c = 'Revenue';
        stl1.Delivery_Quoted_Rate__c = 500;
        stl1.Pickup_Quoted_Rate__c = 500;
        stl1.Delivery_Internal_Cost__c = 500;
        stl1.Pickup_Internal_Cost__c = 500;
        stl1.Adjustments_and_Prepping_Fees__c = 500;
        stl1.Approval_Process_Name__c = 'ABC';
        stl1.Approval_Submission_Date__c = Date.today(); 
        stl1.Cars__c = 'A';
        stl1.Vehicle_Status__c = 'Delivered';
        stl1.Carrier__c = 'UC LS OUT'; 
        stl1.Next_Billing_Date__c = Date.today().addDays(30); 
        stl1.Remaining_Payments__c = 12;          
                
        insert stl1;
                    
        CC_STL_Car_Info__c car1 = TestDataUtility.createSTLCarInfo(prod1.Id);
        car1.Short_Term_Lease__c = stl1.Id;
        car1.Quantity__c = 10;
        car1.Per_Car_Cost__c = 25;
        
        CC_STL_Car_Info__c car2 = TestDataUtility.createSTLCarInfo(prod2.Id);
        car2.Short_Term_Lease__c = stl1.Id;
        car2.Quantity__c = 10;
        car2.Per_Car_Cost__c = 25;
        
        insert new CC_STL_Car_Info__c[]{car1, car2};    	
    }
    
    @isTest
    private static void testBuildAndSend(){
    	
    	CC_Short_Term_Lease__c lease = [Select Id, Name From CC_Short_Term_Lease__c];
    	
    	ApexPages.currentPage().getParameters().put('id', lease.Id);
    	ApexPages.StandardController stdCtrl = new ApexPages.StandardController(lease);
		CC_STL_Confirmation_CONTRL ctrl = new CC_STL_Confirmation_CONTRL(stdCtrl);	
		
		system.assertEquals(2, ctrl.CarsOnLease.size());
		system.assertEquals(lease.Id, ctrl.leaseData.Id);
		system.assertEquals(lease.Id, ctrl.lease_Id);
		
		PageReference sendResult;
		
		Test.startTest();
		
		sendResult = ctrl.buildAndSend();
		
		Test.stopTest();
		
		// Verify the results
		CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
		system.assertEquals('/apex/echosign_dev1__AgreementTemplateProcess?masterid='+lease.Id+'&templateId='+hcs.STL_CONF_ESIGN_TEMPLATEID__c, sendResult.getUrl());
		
		ContentDocumentLink cdl = [Select ContentDocumentId, ShareType, Visibility From ContentDocumentLink Where LinkedEntityId = :lease.Id];
		system.assertEquals('V', cdl.ShareType);
		system.assertEquals('AllUsers', cdl.Visibility);
		
		ContentVersion ct = [Select ContentDocumentId, Title, PathOnClient, VersionData From ContentVersion Where ContentDocumentId = :cdl.ContentDocumentId];
		system.assert(ct.Title.contains(lease.Name + ' Confirmation'));
		system.assert(ct.PathOnClient.contains(lease.Name));
		
		Attachment att = [Select Name, IsPrivate From Attachment Where ParentId = :lease.Id];
		system.assert(att.Name.contains(lease.Name + 'Confirmation'));	
		system.assertEquals(false, att.IsPrivate);	
    }    
}