public with sharing class AssetController {
    @AuraEnabled 
    public static user fetchUser(){
        // query current user information  
        User oUser = [SELECT id,DB_Region__c FROM User WHERE id =: userInfo.getUserId()];
        return oUser;
    }
    
    @AuraEnabled
    public static List <Product2> fetchAsset(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        String recType = System.Label.Product_Record_Type;
        List <Product2> returnList = new List <Product2> ();
        List <Product2> lstOfAsset = [SELECT id,Name FROM Product2 WHERE Customizable__c=FALSE AND RecordTypeId = :recType AND Name LIKE: searchKey LIMIT 500];
        
        for (Product2 asset: lstOfAsset) {
            returnList.add(asset);
        }
        system.debug('returnList'+returnList);
        return returnList;
    }
    
    /**
* Create a new Asset Record
*
* @param Asset asset record to be inserted
* 
*/
    @AuraEnabled
    public static void createRecord (Asset asset){
        
        try{
           // System.debug('Asset Created: '+asset);
            
            if(asset != null){
                insert asset;
            }
            
        } catch (Exception ex){
            
        }
        
    }    
}