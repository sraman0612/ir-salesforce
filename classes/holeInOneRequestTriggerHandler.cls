public class holeInOneRequestTriggerHandler {
    //Method for auto submission of Hole-in-One approval process
    public static void submitAndProcessApprovalRequestHIO(List<Hole_In_One_Claim__c> hioapprovalList) {
        for(Hole_In_One_Claim__c hioIdSet: hioapprovalList){
            if(hioIdSet.Status__c == 'Submitted'){
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitting Hole-in-One request for auto approval.');
                req.setObjectId(hioIdSet.id);
                    if(hioIdSet.Coop_Request__r.Contract__r.Account.CC_Global_Region__c != 'IRESA'||hioIdSet.Coop_Request__r.Contract__r.Account.CC_Global_Region__c != 'IR-ESA'||hioIdSet.Coop_Request__r.Contract__r.Account.CC_Global_Region__c != 'EMEA' ){
                        req.setProcessDefinitionNameOrId('Hole_in_One_approval');
                        req.setSkipEntryCriteria(true);
                        Approval.ProcessResult result = Approval.process(req);
                    }
            }
        }
    }
    //Method to create Order & Order Item for Approved Co-Op claims
    public static void createOrderAndOrderitemForHIOC(List<Hole_In_One_Claim__c> hIOCList){
        List<CC_Order__c> ordersToCreate = new List<CC_Order__c>();
        List<CC_Order__c> ordersToUpdate = new List<CC_Order__c>();
        List<CC_Order_Item__c> orderitemsToCreate = new List<CC_Order_Item__c>();
        Map<String,Hole_In_One_Claim__c> ordcoop = new Map<String, Hole_In_One_Claim__c>();
        Map<String,CC_Pavilion_Co_Op_Product_Mapping__c> custSettingMap=new Map<String,CC_Pavilion_Co_Op_Product_Mapping__c>();
        custSettingMap=CC_Pavilion_Co_Op_Product_Mapping__c.getAll();
        Map<String,CC_Pavilion_Co_Op_Product_Mapping__c> custSettingMapValues=new Map<String,CC_Pavilion_Co_Op_Product_Mapping__c>();
        Map<String,String> prodMap=new Map<String,String>();
        Map<String,Product2> prodMap2=new Map<String,Product2>();
        Map<String,Hole_In_One_Claim__c> contractMap=new Map<String,Hole_In_One_Claim__c>();
        Map<Id,Contract> contractMap2=new Map<Id,Contract>();
        Map<Id,Id> accSummaryMap = new Map<Id,Id>();
        Set<Id> orderid = new Set<Id>();
        Map<Id,Account> accIdMap = new Map<Id,Account>();
        Set<Id> accIdSet = new Set<Id>();
        Id orderRecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Co-Op ordering').getRecordTypeId();
        Id orderItemRecordTypeId = Schema.SObjectType.CC_Order_Item__c.getRecordTypeInfosByName().get('Co-Op ordering').getRecordTypeId();
        Id hIOCRecordTypeId = Schema.SObjectType.Hole_In_One_Claim__c.getRecordTypeInfosByName().get('Hole-In-One').getRecordTypeId();
        for(CC_Pavilion_Co_Op_Product_Mapping__c coopProd: custSettingMap.values()){
            String uniqueVal = coopProd.CC_Account_Region__c+coopProd.Marketing_Agreement__c+coopProd.Coop_Type__c;
            custSettingMapValues.put(uniqueVal,coopProd);
        }//system.debug('*****CoopList'+hIOCList);
        for(Hole_In_One_Claim__c coop: hIOCList){
            if(coop.Status__c!= null && coop.Status__c!= '' && coop.Status__c== 'Approved'){
                contractMap.put(coop.Coop_Request__c,coop);
            }
        }//System.debug('***Contractid***'+contractMap);
        for(CC_Hole_in_One__c coopSum : [SELECT id,Contract__c 
                                         FROM CC_Hole_in_One__c
                                         WHERE id IN : contractMap.keyset()]){
            accSummaryMap.put(coopSum.id,coopSum.Contract__c);
        }
        for(Contract ctrct : [SELECT Id,AccountId,Account.CC_Global_Region__c,
                              CC_Sub_Type__c FROM Contract WHERE Id IN : accSummaryMap.values()]){
            accIdSet.add(ctrct.AccountId);
            contractMap2.put(ctrct.id,ctrct);                 
        }
        for(Account acc: [SELECT Id,CC_Global_Region__c,Name,Address_1__c,Address_2__c,CC_City__c,
                           CC_State__c,CC_Zip__c,CC_Country__c,CC_Shipping_Address_1__c,ShippingStreet,
                           ShippingCity,ShippingState,ShippingCountry,ShippingPostalCode,CC_Billing_Address_1__c,
                           BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode FROM Account 
                           WHERE Id IN : accIdSet AND CC_Global_Region__c!='IR-ESA' 
                           AND CC_Global_Region__c!='IRESA' AND CC_Global_Region__c!='EMEA']){
            accIdMap.put(acc.Id,acc);
        }//system.debug('*****contractMap2**'+contractMap2);//only if Global region is not EMEA or IR-ESA alllow the order creation to happen
        if(!accIdMap.isEmpty()){
            for(Hole_In_One_Claim__c coop1: contractMap.Values()){
                String accSumKey=coop1.Coop_Request__c;
                String contractKey='';
                String recTypeName='';
                String accKey='';
                if(coop1.RecordTypeId == hIOCRecordTypeId)
                    recTypeName='Hole-In-One';
                if(accSummaryMap.containskey(accSumKey)){contractKey= accSummaryMap.get(accSumKey);}
                    CC_Order__c order = new CC_Order__c();
                    order.CC_Status__c = 'Pending';
                    order.RecordTypeId = orderRecordTypeId;
                    if(contractMap2.containsKey(contractKey)){
                        order.CC_Account__c= contractMap2.get(contractKey).AccountId;
                        accKey=contractMap2.get(contractKey).AccountId;
                        if(accIdMap.containskey(accKey)){ //coop.CC_Co_Op_Account_Summary__r.AccountId;
                                order.CC_Addressee_name__c=accIdMap.get(accKey).Name;
                                order.CC_Address_line_1__c=accIdMap.get(accKey).Address_1__c;
                                order.CC_City__c=accIdMap.get(accKey).CC_City__c;
                                order.CC_States__c=accIdMap.get(accKey).CC_State__c;
                                order.CC_Postal_code__c=accIdMap.get(accKey).CC_Zip__c;
                                order.CC_Country__c=accIdMap.get(accKey).CC_Country__c;
                                order.Sold_To_Address_Line_1__c=accIdMap.get(accKey).CC_Shipping_Address_1__c;
                                order.Sold_To_City__c=accIdMap.get(accKey).ShippingCity;
                                order.Sold_To_State__c=accIdMap.get(accKey).ShippingState;
                                order.Sold_To_Country__c=accIdMap.get(accKey).ShippingCountry;
                                order.Sold_To_Postal_Code__c=accIdMap.get(accKey).ShippingPostalCode;
                                order.Bill_To_Address_Line_1__c=accIdMap.get(accKey).CC_Billing_Address_1__c;
                                order.Bill_To_City__c=accIdMap.get(accKey).BillingCity;
                                order.Bill_To_State__c=accIdMap.get(accKey).BillingState;
                                order.Bill_To_Country__c=accIdMap.get(accKey).BillingCountry;
                                order.Bill_To_Postal_Code__c=accIdMap.get(accKey).BillingPostalCode;//system.debug('******Account'+contractMap2.get(contractKey).AccountId);
                                String coopUniqueVal=accIdMap.get(accKey).CC_Global_Region__c+contractMap2.get(contractKey).CC_Sub_Type__c+recTypeName;
                                // system.debug('*****UniqueVal***'+coopUniqueVal);//check if the Account region,Marketing Agrremtn and CoOp type matches with the custom setting to fetch the product
                                if(custSettingMapValues.containskey(coopUniqueVal)){prodMap.put(coop1.Name,custSettingMapValues.get(coopUniqueVal).Charge_Account__c);}
                            }
                    }
                    order.PO__c =  coop1.Name;
                    order.CC_Credit_Memo_Code__c='A';
                    order.CC_Credit_debit_reason_code__c='COP';
                    ordersToCreate.add(order);
                    ordcoop.put(coop1.Name,coop1);
              }
         }//system.debug('******prodMap'+prodMap);//system.debug('******ordersToCreate'+ordersToCreate);
        try{
            if(ordersToCreate.size()>0){
                insert ordersToCreate;
                    for(Product2 p :[SELECT id,ProductCode,ERP_Item_Number__c 
                                     FROM Product2 WHERE ProductCode IN : prodMap.values()] ){
                            prodMap2.put(p.ProductCode,p);
                    }
            }//system.debug('****ProdMap**'+prodMap2);
            for(CC_Order__c ord: ordersToCreate){
                    //orderid.add(ord.Id);
                    for(Hole_In_One_Claim__c holeC: contractMap.Values()){
                        holeC.Club_Car_Order__c=ord.id;
                    }
                    CC_Order_Item__c orderItem = new CC_Order_Item__c();
                    String prodId=prodMap.get(ord.PO__c);
                    orderItem.Order__c=ord.Id;
                    orderItem.CC_UnitPrice__c = ordcoop.get(ord.PO__c).HIO_Claim_amount__c;
                    orderItem.CC_Description__c = ordcoop.get(ord.PO__c).Winner_Name__c;
                    orderItem.CC_Quantity__c = 1;
                    orderItem.RecordTypeId = orderItemRecordTypeId;
                    if(!prodMap2.isEmpty() && prodMap2.get(prodId).id!=null ){orderItem.Product__c=prodMap2.get(prodId).Id;}
                    if(!prodMap2.isEmpty() && prodMap2.get(prodId).ERP_Item_Number__c!=null){orderItem.CC_Product_Code__c=prodMap2.get(prodId).ERP_Item_Number__c;}
                    orderitemsToCreate.add(orderItem);
                    ord.CC_Status__c = 'Submitted';
                    ordersToUpdate.add(ord);
            }
            if(orderitemsToCreate.size()>0){insert orderitemsToCreate;}
            if(ordersToUpdate.size()>0){update ordersToUpdate;}
        }
        catch(Exception e){system.debug(e);}
    }
}