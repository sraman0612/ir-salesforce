@isTest
public with sharing class CC_Submit_Service_Contract_Approval_Test {

	@TestSetup
	private static void createData(){
		
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;		
		
        insert CC_Contract_Settings__c.getOrgDefaults();
        
        TestDataUtility dataUtility = new TestDataUtility();	

        Account a1 = dataUtility.createAccount('Club Car');
        a1.Name = 'Account1';	
        insert a1;
        
        Contract c = TestDataUtility.createCCServiceContract(a1.Id, 'Monthly');
        c.CC_Sales_Rep_Requestor__c = UserInfo.getUserId();
        c.CC_Service_Regional_Leader__c = UserInfo.getUserId();
        insert c;
	}
	
	@isTest
    private static void testSubmitContractForApproval() {    
    	
    	Contract c = [Select Id From Contract];
    	
    	Test.startTest();
    	    	
    	LightningResponseBase response = CC_Submit_Service_Contract_Approval_Ctrl.submitContractForApproval(c.Id);
    	
    	system.assertEquals(true, response.success);
    	system.assertEquals('', response.errorMessage);

        system.assertEquals(true, Approval.isLocked(c.Id));

        // Submit again which should return an error
    	LightningResponseBase response2 = CC_Submit_Service_Contract_Approval_Ctrl.submitContractForApproval(c.Id);
    	
    	system.assertEquals(false, response2.success);
    	system.assertNotEquals('', response2.errorMessage);       
    	
    	Test.stopTest();
    }
}