public class CC_TrainingReguestCnt {
  
  Public  CC_TrainingRequest   attendee {get;set;}
  public String returnStr {get;set;}

  public CC_TrainingReguestCnt(CC_PavilionTemplateController controller) {
    attendee = new CC_TrainingRequest();
    String d = ApexPages.currentPage().getParameters().get('date');
    String t = ApexPages.currentPage().getParameters().get('time');
    String c = ApexPages.currentPage().getParameters().get('contents');
    attendee.OwnerId='00G0a000002qnlj';
    if(c.startsWithIgnoreCase('tavant')){
      attendee.CaseSubReason='Warranty Training';
      if(null!=PavilionSettings__c.getValues('Warranty_HelpQ')){attendee.OwnerId=PavilionSettings__c.getValues('Warranty_HelpQ').Value__c;}
    } else if(c.startsWithIgnoreCase('etq')){
      attendee.CaseSubReason='ETQ Training';
      if(null!=PavilionSettings__c.getValues('Warranty_HelpQ')){attendee.OwnerId=PavilionSettings__c.getValues('Warranty_HelpQ').Value__c;}
    } else if(c.startsWithIgnoreCase('marketing')){
      attendee.CaseSubReason='Marketing Training';
      if(null!=PavilionSettings__c.getValues('Marketing_HelpQ')){attendee.OwnerId=PavilionSettings__c.getValues('Marketing_HelpQ').Value__c;}
    } else if(c.startsWithIgnoreCase('club car links')){
      attendee.CaseSubReason='Club Car Links Training';
      if(null!=PavilionSettings__c.getValues('LINKSTrainingOwnerId')){attendee.OwnerId=PavilionSettings__c.getValues('LINKSTrainingOwnerId').Value__c;}
    } else if(c.startsWithIgnoreCase('customerview')){
      attendee.CaseSubReason='SFA/CustomerVIEW Training';
      if(null!=PavilionSettings__c.getValues('SFA_CVTrainingOwnerID')){attendee.OwnerId=PavilionSettings__c.getValues('SFA_CVTrainingOwnerID').Value__c;}
    } else {
      attendee.CaseSubReason='';
    }
    string [] datepartarr = d.split('-');
    attendee.CC_Date_Of_Course = null != d && '' != d ? date.newinstance(integer.valueOf(datepartarr[0]),integer.valueOf(datepartarr[1]),integer.valueOf(datepartarr[2])) : null;
    attendee.CC_Time_Of_Course = null != t && '' != t ? t : null;
    attendee.CC_Courses = null != c && '' != c ? c : null;
    User u = [SELECT Id, ContactId, Name, Phone, Extension, Email, MobilePhone,FederationIdentifier FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];
    Contact [] ctLst = [SELECT Id, AccountId, Account.CC_Sales_Rep__c, Account.CC_Sales_Rep__r.Name, Email, HomePhone, MobilePhone, Name,
                             OtherPhone, Phone, CC_Account_Number__c, Account.CC_Customer_Number__c, Account.Name,account.ShippingCity,
                             account.ShippingState, account.ShippingPostalCode, account.ShippingCountry
                        FROM Contact
                       WHERE ID = : u.ContactId or CORP_ID__c = :u.FederationIdentifier limit 1];
    if (!ctLst.isEmpty()) {
      Contact contct = ctLst[0];                        
      attendee.ContactId = contct.Id;
      attendee.AccountId = contct.AccountId;
      attendee.CC_Company_Name=contct.account.Name!=null?contct.account.Name:'';
      attendee.CC_Clubcar_Customer=contct.account.CC_Customer_Number__c!= null?contct.account.CC_Customer_Number__c:'';
      attendee.CC_Company_Name = contct.account.Name != null?contct.account.Name:'';
      String addressStr = '';
      if (contct.account.ShippingCity != null)
        addressStr += contct.account.ShippingCity + ', ';
      if (contct.account.ShippingState != null)
        addressStr += contct.account.ShippingState + ' ';
      if (contct.account.ShippingPostalCode != null)
        addressStr += contct.account.ShippingPostalCode;
      addressStr +=  '\n';
      if (contct.account.ShippingCountry == null)
        addressStr += contct.account.ShippingCountry;
      attendee.CC_Company_Address = addressStr;
      attendee.CC_Company_Telephone = contct.MobilePhone != null ? contct.MobilePhone : '';
      attendee.CC_Company_Email = contct.Email != null ? contct.Email : '';
      attendee.CC_Sales_Rep_Name = contct.Account.CC_Sales_Rep__r.Name != null ? contct.Account.CC_Sales_Rep__r.Name : '';
      attendee.CC_Attendee_Email = attendee.CC_Company_Email;
      attendee.CC_Attendee_Name = null!=contct.Name?contct.Name:'';
    } else {
      //get all this same stuff from usr
      attendee.CC_Company_Name='Club Car';
      attendee.CC_Company_Address = 'Evans, GA 30809' + '\n' + 'USA';
      attendee.CC_Company_Telephone = u.Phone;
      attendee.CC_Company_Email = u.Email;
      attendee.CC_Attendee_Email = u.Email;
      attendee.CC_Attendee_Name = u.Name;
    }
    attendee.CC_Attendee_Telephone = attendee.CC_Company_Telephone;
  }
  
  public void createTrainingRegistration(){
    Date d = attendee.CC_Date_Of_Course;
    Case newCase = new Case();
    newCase.Origin = 'Community';
    newCase.Reason = 'Training';
    newCase.OwnerId =attendee.OwnerId;
    newCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName ='Club_Car_Training_Request' and SobjectType='Case' LIMIT 1].Id;
    newCase.Sub_Reason__c = attendee.CaseSubReason;
    newCase.ContactId = attendee.ContactId;
    newCase.CC_Sales_Rep__c = attendee.SalesRepId;
    newCase.Subject = attendee.CC_Courses;
    newCase.CC_Courses__c = attendee.CC_Courses;
    newCase.CC_Date_of_Course__c = d;
    newCase.CC_Time_of_Course__c = attendee.CC_Time_Of_Course;
    newCase.CC_Attendee_Name__c = attendee.CC_Attendee_Name;
    newCase.CC_Attendee_Phone__c = attendee.CC_Attendee_Telephone;
    newCase.CC_Attendee_Email__c = attendee.CC_Attendee_Email;
    try{
      insert newCase;
      Event newEvent = new Event();
      newEvent.Subject = attendee.CC_Courses;
      newEvent.WhoId = attendee.ContactId;
      newEvent.WhatId = newCase.Id;
      newEvent.Description = attendee.CC_Attendee_Name + '\n'+ attendee.CC_Attendee_Telephone + '\n' + attendee.CC_Attendee_Email;
      String [] timeArr = attendee.CC_Time_Of_Course.split(' ');
      String [] startTimeArr = timeArr[0].split(':');
      Integer startHour = integer.valueOf(startTimeArr[0]);
      Integer startMinute = integer.valueOf(startTimeArr[1]);
      Integer twelve = 12;
      startHour='PM'==timeArr[3]&&startHour<12?startHour+twelve:startHour;
      String [] endTimeArr = timeArr[2].split(':');
      Integer endHour = integer.valueOf(endTimeArr[0]);
      endHour='PM'==timeArr[3]&&endHour<12?endHour+twelve:endHour;
      Integer endMinute = integer.valueOf(endTimeArr[1]);
      Time startTime = Time.newInstance(startHour,startMinute , 0, 0);
      newEvent.StartDateTime = datetime.newInstanceGmt(d, startTime).addHours(5);
      Time endTime = Time.newInstance(endHour,endMinute , 0, 0);
      newEvent.EndDateTime = datetime.newInstanceGmt(d, endTime).addHours(5);
      system.debug('### course date is '+ newCase.CC_Date_of_Course__c);
      system.debug('### start datetime is ' + newEvent.StartDateTime);
      system.debug('### end datetime is ' + newEvent.EndDateTime);
      insert newEvent;
      returnStr = 'Training Request Created. Thank You!';
    } catch(Exception e) {
      returnStr = 'We were unable to create your training request.' + e.getStackTraceString();
    }
    
  }
}