@isTest
public with sharing class NotificationsTestDataService {

    public static Map<String, Schema.RecordTypeInfo> accountRecordtypeMap = Account.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName();
    public static Id billToRecordTypeId = accountRecordtypeMap.get('CTS_Bill_To_Account').getRecordTypeId();
    public static Id distributorRecordTypeId = accountRecordtypeMap.get('CTS_EU').getRecordTypeId();
    public static Id distributorEndUserRecordTypeId = accountRecordtypeMap.get('CTS_EU').getRecordTypeId();

    public static Notification_Settings__c createNotificationSettings(){

        Notification_Settings__c settings = Notification_Settings__c.getOrgDefaults();
        settings.Enable_Real_Time_Message_Delivery__c = false;
        settings.Enable_Notification_Batch_Job_Sandbox__c = true;
        settings.Twilio_From_Phone_Number__c = '555-555-5555';
        settings.Org_Wide_Email_Address__c = [Select Address from OrgWideEmailAddress where IsAllowAllProfiles = true LIMIT 1].Address;
        upsert settings;

        return settings;
    }

    public static Notification_Type__c createNotificationType(Boolean doInsert, String name, Boolean mandatory, String fieldValue,
        String triggeringObject, String triggeringField, String triggeringObjectParentType, String triggeringObjectParentField, 
        String triggeringObjectTimestampField, String notificationTriggeringObjectRelation, String parentObjectGrandparentType, String parentObjectGrandparentField, 
        String messageTemplate){

        Notification_Type__c notificationType = new Notification_Type__c(
            Name = name,
            Active__c = true,
            Mandatory__c = mandatory,
            Triggering_Object__c = triggeringObject,
            Triggering_Field__c = triggeringField,
            Triggering_Field_Value__c = fieldValue,            
            Triggering_Object_Parent_Type__c = triggeringObjectParentType,
            Triggering_Object_Parent_Field__c = triggeringObjectParentField,
            Triggering_Object_Timestamp_Field__c = triggeringObjectTimestampField,
            Notification_Triggering_Object_Relation__c = notificationTriggeringObjectRelation,
            Parent_Object_Grandparent_Type__c = parentObjectGrandparentType,
            Parent_Object_Grandparent_Field__c = parentObjectGrandparentField,
            Parent_Object_Summary_Fieldset__c = 'CTS_Community_Event_Notification_FS',
            Message_Template__c = messageTemplate
        );

        if (doInsert){
            insert notificationType;
        }

        return notificationType;
    }

    public static Notification_Subscription__c createSubscription(Boolean doInsert, Id subscriberId, Id recordId, Id notificationTypeId,
        String deliveryFrequencies, String deliveryMethods){

        Notification_Subscription__c subscription = new Notification_Subscription__c(
            OwnerId = subscriberId,
            Notification_Type__c = notificationTypeId,
            Record_ID__c = recordId,
            Delivery_Frequencies__c = deliveryFrequencies,
            Delivery_Methods__c = deliveryMethods
        );

        if (doInsert){
            insert subscription;
        }

        return subscription;
    }
    
    public static Notification_Message__c createNotificationMessage(Boolean doInsert, Id recipientId, Id subscriptionId, Id notificationTypeId, Id triggeringRecordId, 
        DateTime scheduledDeliveryDate, String deliveryFrequency, String deliveryMethods){

        Notification_Message__c notificationMessage = new Notification_Message__c(
            OwnerId = recipientId,
            Notification_Subscription__c = subscriptionId,
            Notification_Type__c = notificationTypeId,
            Triggering_Record_ID__c = triggeringRecordId,
            Scheduled_Delivery_Date__c = scheduledDeliveryDate,
            Delivery_Frequency__c = deliveryFrequency,
            Delivery_Methods__c = deliveryMethods
        );

        if (triggeringRecordId != null){

            if (triggeringRecordId.getSobjectType() == CTS_RMS_Event__c.getSObjectType()){
                notificationMessage.RMS_Event__c = triggeringRecordId;
            }
        }

        if (doInsert){
            insert notificationMessage;
            System.debug('Inserting message:' + notificationMessage);
        }

        return notificationMessage;
    }    

    public static CTS_RMS_Event__c createRmsEvent(Boolean doInsert, Id assetId, String eventType, String message){

        CTS_RMS_Event__c rmsEvent = new CTS_RMS_Event__c(
            Name = message,
            Asset__c = assetId,
            Event_Type__c = eventType,
            Event_Message__c = message
        );

        if (doInsert){
            insert rmsEvent;
        }

        return rmsEvent;
    }   
    
    public static void validateMessagesToSend(Notification_Message__c[] messagesToSend){
        validateMessagesToSend(messagesToSend, false);
    }

    public static void validateMessagesToSend(Notification_Message__c[] messagesToSend, Boolean messagesShouldNotBeDelivered){

        // Verify the messages were all successfully sent unless messagesShouldNotBeDelivered = true
        messagesToSend = [Select Id, Delivery_Methods__c,
            Email_Delivered_Date__c, Email_Delivery_Error__c, Email_Delivery_Failure_Count__c,
            SMS_Delivered_Date__c, SMS_Delivery_Error__c, SMS_Delivery_Failure_Count__c,
            Chatter_Delivered_Date__c, Chatter_Delivery_Error__c, Chatter_Delivery_Failure_Count__c
        From Notification_Message__c 
        Where Id in :messagesToSend];

        for (Notification_Message__c msg : messagesToSend){

            if (messagesShouldNotBeDelivered){

                system.assertEquals(null, msg.Email_Delivered_Date__c);

                // Having trouble replicating an error scenario in the Twilio mock response, skipping this validation for now
                if (!NotificationTwilioServiceMock.forceError){

                    system.assertEquals(null, msg.SMS_Delivered_Date__c);
                    system.assertEquals(null, msg.SMS_Delivery_Error__c);
                    system.assertEquals(0, msg.SMS_Delivery_Failure_Count__c);  
                }

                system.assertEquals(null, msg.Chatter_Delivered_Date__c);                                   
            }
            else{

                Boolean forceFail = NotificationDeliveryService.forceFail;

                if (msg.Delivery_Methods__c.contains('Email')){

                    if (forceFail){
                        system.assertEquals(null, msg.Email_Delivered_Date__c);
                    }
                    else{
                        system.debug('Mail Error Message: ' + msg.Email_Delivery_Error__c);
                        system.assertEquals(forceFail ? null : DateTime.now().date(), msg.Email_Delivered_Date__c != null ? msg.Email_Delivered_Date__c.date() : null);
                    }

                    system.assertEquals(forceFail ? 'force fail' : null, msg.Email_Delivery_Error__c);
                    system.assertEquals(forceFail ? 1 : 0, msg.Email_Delivery_Failure_Count__c);
                }

                if (msg.Delivery_Methods__c.contains('SMS')){

                    if (forceFail){
                        system.assertEquals(null, msg.SMS_Delivered_Date__c);
                    }
                    else{
                        system.assertEquals(forceFail ? null : DateTime.now().date(), msg.SMS_Delivered_Date__c.date());
                    }                

                    system.assertEquals(forceFail ? 'force fail' : null, msg.SMS_Delivery_Error__c);
                    system.assertEquals(forceFail ? 1 : 0, msg.SMS_Delivery_Failure_Count__c);
                } 

                if (msg.Delivery_Methods__c.contains('Community')){

                    if (forceFail){
                        system.assertEquals(null, msg.Chatter_Delivered_Date__c);
                    }
                    else{
                        system.assertEquals(forceFail ? null : DateTime.now().date(), msg.Chatter_Delivered_Date__c.date());
                    }                

                    if (forceFail){
                        system.assert(msg.Chatter_Delivery_Error__c.contains('Script-thrown exception'));
                    }
                    else{
                        system.assertEquals(null, msg.Chatter_Delivery_Error__c);
                    }

                    system.assertEquals(forceFail ? 1 : 0, msg.Chatter_Delivery_Failure_Count__c);
                }    
            }            
        }        
    }    
}