@isTest
public Class CC_CarOrderVFDetailCntl_test{
    
    public testmethod static void UnitTest_CarOrderVFDetailCntl()
    {
        Account sampleAccount = TestUtilityClass.createAccount();
        sampleAccount.CC_Customer_Number__c='123';
        insert sampleAccount;
        CC_Order__c newOrder = TestUtilityClass.createCustomOrder(sampleAccount.id,'OPEN','N');
        insert newOrder;
        CC_Order_Item__c ordItem = TestUtilityClass.createOrderItem(newOrder.Id);
        insert ordItem;
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(ordItem.Id);
        carItem.Truck_Sequence__c=111;
        carItem.Order_Item__c=ordItem.id;
        carItem.Shipment_Header_Number__c=111;
        insert carItem;
        CC_Car_Item__c secondCarItem = TestUtilityClass.createCarItem(ordItem.Id);
        secondCarItem.Truck_Sequence__c=111;
        secondCarItem.Order_Item__c=ordItem.id;
        insert secondCarItem;
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        CC_Car_Feature__c cf =  TestUtilityClass.createCarFeature(carItem.Id,prod.Id,newOrder.Id);
        insert cf;
        CC_Invoice2__C inv =TestUtilityClass.createInvoice2(sampleAccount.Id,'123');
        insert inv;
        String invoiceName = [select name from CC_Invoice2__C where id=:inv.Id limit 1].name;
        CC_Order_Shipment__c ordShip = TestUtilityClass.createOrderShipment(newOrder.id);
        ordShip.Shipment_Header_Number__c=111;
        ordShip.Invoice_Number__c='123';
        ordShip.Invoice_Date__c=System.today();
        insert ordShip;
        CC_Order_Shipment__c newOrdship = TestUtilityClass.createOrderShipment(newOrder.id);
        newOrdship.Shipment_Header_Number__c=112;
        newOrdship.Invoice_Number__c='123';
        newOrdship.Invoice_Date__c=System.today();
        insert newOrdship;
        CC_Order_Shipment_Item__c ordShipItem =TestUtilityClass.createOrderShipmentItem(ordItem.Id,ordShip.Id);
        insert ordShipItem;
        Test.startTest();
        ApexPages.StandardController cont =new ApexPages.StandardController(newOrder);
        CC_CarOrderVFDetailCntl cord =new CC_CarOrderVFDetailCntl(cont);
       
        CC_CarOrderVFDetailCntl.OrderItemAndShipmentWrapper ordshipwrapper =new CC_CarOrderVFDetailCntl.OrderItemAndShipmentWrapper(carItem,ordShip,'invst');
         ordshipwrapper.getDeliveryDateDisplayClasses();
        ordshipwrapper.getCarrier();
        ordshipwrapper.getCarrierLink();
        PageReference testPage1 = Page.CC_CarOrderLayout;
        testPage1.getParameters().put('id', newOrder.Id);
        Test.setCurrentPage(testPage1);
         PageReference p = cord.redirector();
        Test.stopTest();    
    }
    
}