public class CC_DealerLocatorPageController {
    
    public String selectedtype{get;set;}
    public String country{get;set;}
    public String postCode{get;set;}
    //public List<CC_DealerWrapper> dealWrappers{get;set;}
    public List<CC_dealerSearch.DealerInfo> dealWrappers{get;set;} 
    public Boolean hideInactiveDealers{get;set;}   
    
    public List<SelectOption> getTypes()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption( '--None--', '--None--'));
        for( CC_Dealer_Locator_Settings__c dealerlocatorsetting : CC_Dealer_Locator_Settings__c.getall().values())
        {
            options.add(new SelectOption(dealerlocatorsetting.Name, dealerlocatorsetting.Name));
        }       
        return options;
    }
    
    
    
    public pageReference getSearchLocations(){
        //dealWrappers = new List<CC_DealerWrapper>();
        // dealWrappers = CC_dealerLocator.getdealerwrappers(selectedtype, country, postCode);
        dealWrappers = new List<CC_dealerSearch.DealerInfo>();
        CC_dealerSearch.hideInactive = hideInactiveDealers;
        
        try {
            dealWrappers = CC_dealerSearch.searchDealers(selectedtype, country, postCode);
        } catch (CC_dealerSearch.DealerLocatorException dle) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, dle.getMessage()));
        }

        return NULL;
    }
    
}