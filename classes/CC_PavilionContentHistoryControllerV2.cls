public class CC_PavilionContentHistoryControllerV2 {
    public Integer pageSize{get;set;}
    public List<ContentVersionHistory> cvhList{get;set;}
    public Integer noOfPages{get;set;}
    public Integer pageNumber{get;set;}
    public String recordTypeN='bulletin';
    public String selectedRecType{get;set;}
    public String msg{get;set;}
    public String baseQuery = 'select ContentVersionId,CreatedBy.Id,ContentVersion.CreatedBy.Title,ContentVersion.CreatedBy.FederationIdentifier,ContentVersion.VersionNumber,ContentVersion.Bulletin_Number__c,ContentVersion.CC_Bulletin_Type__c,ContentVersion.Bulletin_Title__c,CreatedById, Contentversion.CreatedDate,CreatedDate, Field, Id from ContentVersionHistory Where (Field= \'contentVersionDownloaded\')';                                        
    public static Integer totalNoOfRecs{get;set;}
    public Date startDate{get;set;}
    public Date endDate{get;set;}
    public List<SelectOption> recTypeList{get;set;}
    public string selectedPage{get;set{selectedPage=value;}
    }
    
    @remoteAction
    @readOnly
    public static Integer countContentDownloadRecords(){
      totalNoOfRecs=0;
      for(ContentVersionHistory cvh : [select id from ContentVersionHistory where Field='contentVersionDownloaded']){
          totalNoOfRecs++;
        }
       return totalNoOfRecs;
    }
    
    public CC_PavilionContentHistoryControllerV2(ApexPages.StandardController controller) {
        msg = null;
        pageSize = 10;
        totalNoOfRecs=5;
        //FETCH RECORDTYPES TO BE SHOWED AS PICKLIST ON THE PAGE
        recTypeList = new List<SelectOption>();
        recTypeList.add(new SelectOption('--None--', '--None--'));
        for(RecordType rT : [SELECT Id, Name FROM RecordType where sobjecttype = 'ContentVersion' and isActive=true]){ 
            recTypeList.add(new SelectOption(rT.Id, rT.Name));
         }
        getInitialContentVersionHistorySet();  
    }

    public PageReference getInitialContentVersionHistorySet()
    {        
        pageNumber = 0;
        noOfPages = totalNoOfRecs/pageSize;
        
        if (Math.mod(totalNoOfRecs, pageSize) > 0)
            noOfPages++;
        
        try{
            cvhList= Database.query(baseQuery + ' limit '+pageSize);
            
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;  
    }
    
    public PageReference dateRangeFilter(){
        cvhRec();
        return null;
    }
    public PageReference next(){
        pageNumber++; 
        
        contentVersionHistoryRecords();
        return null;
    }

    public PageReference previous(){
        pageNumber--;
        if (pageNumber < 0)
            return null;
        
        contentVersionHistoryRecords();
        return null;
    }
    
    private void contentVersionHistoryRecords()
    {
        Integer offset = pageNumber * pageSize;    
        String query = baseQuery + ' limit '+pageSize +' offset '+ offset;
        System.debug('Query is'+query);
        try{
            cvhList= Database.query(query);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }       
    }
    private void cvhRec(){
       string stDate=string.valueof(startDate);
       string eDate=String.valueof(endDate);
       Integer offset = pageNumber * pageSize;
       String query= '';   
       if(selectedRecType!='--None--' && startDate!=null && endDate!=null){
         query = baseQuery +' and (ContentVersion.RecordTypeId=: selectedRecType ) AND ((ContentVersion.createdDate>=:startDate OR ContentVersion.ContentModifiedDate>=:startDate) AND (ContentVersion.createdDate<=:endDate OR ContentVersion.ContentModifiedDate<=:endDate)) limit '+pageSize;
       }
       if(selectedRecType == '--None--' && startDate!=null && endDate!=null){
         query = baseQuery +' and ((ContentVersion.createdDate>=:startDate OR ContentVersion.ContentModifiedDate>=:startDate) AND (ContentVersion.createdDate<=:endDate OR ContentVersion.ContentModifiedDate<=:endDate)) limit '+pageSize;
       }
       if(selectedRecType != '--None--' && startDate==null && endDate==null){
         query = baseQuery +' and (ContentVersion.RecordTypeId=: selectedRecType ) limit '+pageSize;
       }
      system.debug('query is'+query);
      try{
          boolean flag=false;
          if(startDate !=null && endDate==null){
          msg = 'Please enter the End Date';
          flag=true;
          }
          else if(startDate ==null && endDate!=null){
          msg = 'Please enter the Start Date';
          flag = true;
          }
          if(!flag){cvhList= Database.query(query);}
      }
      catch(Exception e){
      }      
            }
}