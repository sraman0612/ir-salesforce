/**
 * Test class of CTS_IOT_Self_RegisterController
 **/
@isTest
private class CTS_IOT_Self_RegisterControllerTest {
    
    @testSetup
    static void setup(){

UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        List<Account> accounts = new List<Account>();
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        accounts.add(acc);
        accounts.add(CTS_TestUtility.createAccount('Test Account 1', false));
        accounts.add(CTS_TestUtility.createAccount('Test Account 2', false));
        insert accounts;

        accounts.get(0).Bill_To_Account__c = accounts.get(1).Id;
        update accounts[0];

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(false);
        setting.Account_Matching_Record_Types__c = accounts.get(0).recordTypeId;
        setting.Asset_Matching_Record_Types__c = assetRecTypeId;
        setting.Asset_Matching_Record_Types2__c = assetRecTypeId;
        insert setting;

        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', accounts.get(0).Id, assetRecTypeId, false);
        asset.IRIT_RMS_Flag__c = true;
        asset.SerialNumber = '12345';
        insert asset;
        
        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', accounts.get(0).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', accounts.get(0).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact789','Test', 'testcts2@gmail.com', accounts.get(1).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact789','Test', 'testcts3@gmail.com', accounts.get(2).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact444','Test', 'testcts4@gmail.com', accounts.get(0).Id, false));

        for(Contact con : contacts){
            con.CTS_IOT_Community_Status__c = 'Approved';
        }

        insert contacts;
        
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(contacts.get(0).Id, profileId, true);
    }
}
    @isTest
    static void testGetInit(){
        Test.StartTest();
        CTS_IOT_Self_RegisterController.InitResponse initResponse = CTS_IOT_Self_RegisterController.getInit();
        System.assert(initResponse != null);
        Test.stopTest();
    }

    @isTest
    static void testSelfRegister(){

        CTS_IOT_Community_Administration__c setting = CTS_IOT_Community_Administration__c.getOrgDefaults();

        Test.StartTest();

        CTS_IOT_Self_RegisterController.SelfRegisterRequest registerRequest = new CTS_IOT_Self_RegisterController.SelfRegisterRequest();
        registerRequest.firstName = 'Test CTS';
        registerRequest.lastName = 'Community User';
        registerRequest.email = 'testcts1@gmail.com';    
        registerRequest.title = 'testcts';
        registerRequest.address1 = 'C1';
        registerRequest.address2 = 'S Nagar';
        registerRequest.SerialNumber = '12345';

        String requestStr = (String)JSON.serialize(registerRequest);
        CTS_IOT_Self_RegisterController.SelfRegisterResponse response = CTS_IOT_Self_RegisterController.selfRegister(requestStr);
        System.assert(response != null);

        // Test contact no match but in hierarchy
        registerRequest.email = 'testcts2@gmail.com'; 
        requestStr = (String)JSON.serialize(registerRequest);
        response = CTS_IOT_Self_RegisterController.selfRegister(requestStr);
        System.assert(response != null);

        // Test no contact match with no existing super user
        registerRequest.email = 'testcts3@gmail.com'; 
        requestStr = (String)JSON.serialize(registerRequest);
        response = CTS_IOT_Self_RegisterController.selfRegister(requestStr);
        System.assert(response != null);

        // Test contact not found
        registerRequest.email = 'testcts10@gmail.com'; 
        requestStr = (String)JSON.serialize(registerRequest);
        response = CTS_IOT_Self_RegisterController.selfRegister(requestStr);
        System.assert(response != null);

        // Test no contact match with existing super user
        Contact newSuperUserContact = [Select Id, Name From Contact Where Email = 'testcts4@gmail.com'];
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Super_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(newSuperUserContact.Id, profileId, true);

        registerRequest.email = 'testcts11@gmail.com'; 
        requestStr = (String)JSON.serialize(registerRequest);
        response = CTS_IOT_Self_RegisterController.selfRegister(requestStr);
        System.assert(response != null);        

        Test.stopTest();
    }
}