@isTest 
Public class CC_PavilionHIOController_Test
{    

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testmethod void PavilionHIOController()
    {
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con;
        system.debug('the contact created is'+con.Id);
        system.debug('the contact accountid is'+con.AccountId);
        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);        
        Contract cont = TestUtilityClass.createContractwithtype('Dealer/Distributor Agreement',con.AccountId);
        insert cont;
        CC_Co_Op_Account_Summary__c ccas = TestUtilityClass.createCoopAccountSummary(cont.Id);
        insert ccas;
        
        test.startTest();      
        CC_PavilionHIOController.getAggrementTypes();
        CC_PavilionHIOController.refreshTableBasedOnAggrement(cont.CC_Type__c, acc.id);  
        CC_PavilionHIOController.refreshTableBasedOnAggrement(cont.CC_Type__c, null); 
        String coopClaimJSON =  '{"CoopClaimItem":[{"Day":"1", "proAm":"pro", "players":"2" , "par":"3","hole":"16","round":"23", "yards":"230"},{"Day":"1", "proAm":"pro", "players":"2" , "par":"3","hole":"16","round":"23", "yards":"230"},{"Day":"1", "proAm":"pro", "players":"2" , "par":"3","hole":"16","round":"23", "yards":"230"},{"Day":"1", "proAm":"pro", "players":"2" , "par":"3","hole":"16","round":"23", "yards":"230"}]}';
        CC_PavilionHIOController.CoopClaimParser parserData = (CC_PavilionHIOController.CoopClaimParser)JSON.deserializeStrict(coopClaimJSON , CC_PavilionHIOController.CoopClaimParser.class);  
        CC_PavilionHIOController.CoopClaimParser objparser = new CC_PavilionHIOController.CoopClaimParser();
        CC_PavilionHIOController.CoopClaimItems objItems = new CC_PavilionHIOController.CoopClaimItems(); 
        CC_PavilionHIOController.CoopClaimItems ccitem =new CC_PavilionHIOController.CoopClaimItems();
        CC_PavilionHIOController.CoopClaimParser ccp =new CC_PavilionHIOController.CoopClaimParser();                
        CC_PavilionHIOController.submitCoopClaim('TestTournament', 'TestCourse', 'clucar01', 'New York', 'CA', 'USA',String.valueOf(System.today().format()), 'Carryall 2 Plus', '2', cont.id, coopClaimJSON);
        CC_PavilionHIOController.submitCoopClaim('TestTournament', 'TestCourse', 'clucar01', 'New York', 'CA', 'USA',String.valueOf(System.today().format()), 'Carryall 2 Plus', '2','', coopClaimJSON);  
        User newUser = TestUtilityClass.createUser('CC_PavilionCommunityUser', con.id);
        insert newUser;
        System.runas(newUser)
        {
        String userNavigationProfile = newUser.CC_Pavilion_Navigation_Profile__c;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(userNavigationProfile);
        insert newPavilionNavigationProfile;
        CC_PavilionTemplateController controller =new CC_PavilionTemplateController();
        controller.acctId=acc.id;
        CC_PavilionHIOController HioCont =new  CC_PavilionHIOController(controller);
        }
        test.stopTest();
    }
}