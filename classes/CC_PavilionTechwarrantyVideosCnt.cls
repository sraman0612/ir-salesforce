global with sharing class CC_PavilionTechwarrantyVideosCnt
{ 
    public CC_PavilionTechwarrantyVideosCnt(CC_PavilionTemplateController controller) {

    }
    /*-------retrieves the list of Marketing Videos ----- */      
  //code contributed by @Priyanka Baviskar for issue no 7839968.

   
  @RemoteAction
 public static set < string > getChapter() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentLang = new List < ContentVersion > ();

  contentLang = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                      CC_Market__c, CC_Power_Source__c,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name IN ('Training Support Videos', 'Tech Training Videos')
                  ];
  for (ContentVersion version: contentLang) {

   options.add(version.CC_Chapter__c);
  }
  return options;
 }
 
 
  @RemoteAction
 public static set < string > getType() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentCateg = new List < ContentVersion > ();

  contentCateg = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                      CC_Market__c, CC_Power_Source__c,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name IN ('Training Support Videos', 'Tech Training Videos')
                       and CC_Type1__c!= null
                      ];
  for (ContentVersion version: contentCateg) {
   options.add(version.CC_Type1__c);
  }
  return options;
 }


@RemoteAction
 public static set < string > getMarket() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentLang = new List < ContentVersion > ();

  contentLang = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                      CC_Market__c, CC_Power_Source__c,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name IN ('Training Support Videos', 'Tech Training Videos')
                  ];
  for (ContentVersion version: contentLang) {

   options.add(version.CC_Market__c);
  }
  return options;
 }
 
 @RemoteAction
 public static set < string > getPowerSource() {

  set < string > options = new set < String > ();
  List < ContentVersion > contentLang = new List < ContentVersion > ();

  contentLang = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                      CC_Market__c, CC_Power_Source__c,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name IN ('Training Support Videos', 'Tech Training Videos')
                  ];
  for (ContentVersion version: contentLang) {

   options.add(version.CC_Power_Source__c);
  }
  return options;
 }
 
   @RemoteAction
 global static List < ContentVersion > getVideoData(String VideoType) {

  List < ContentVersion > contentList = new List < ContentVersion > ();

  contentList = [select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                      CC_Market__c, CC_Power_Source__c,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name IN ('Training Support Videos', 'Tech Training Videos')
                  ];

  return contentList;

 }
   
   
    
}