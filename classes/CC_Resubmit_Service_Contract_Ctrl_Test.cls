@isTest
public with sharing class CC_Resubmit_Service_Contract_Ctrl_Test {
    
	@TestSetup
	private static void createData(){
		
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();      
        insert psettingList;		
		
		TestDataUtility dataUtility = new TestDataUtility();	

        Account a1 = dataUtility.createAccount('Club Car');
        a1.Name = 'Account1';	
        insert a1;
        
        insert TestDataUtility.createCCServiceContract(a1.Id, 'Monthly');
	}
	
	@isTest
    private static void testUnlockContractSuccess() {    
    	
    	Contract c = [Select Id, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(false, c.CC_Unlock_Requested__c);
    	system.assertEquals(null, c.CC_Unlock_Requestor__c);
    	
    	// Simulate approval but updating the contract status and locking the contract
    	c.CC_Contract_Status__c = 'Approved';
    	update c;
    	
    	Test.startTest();
    	    	
    	String responseStr = CC_Resubmit_Service_Contract_Controller.unlockContract(c.Id);
    	LightningResponseBase response = (LightningResponseBase)JSON.deserialize(responseStr, LightningResponseBase.class);
    	
    	system.assertEquals(true, response.success);
    	system.assertEquals('', response.errorMessage);

    	c = [Select Id, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(true, c.CC_Unlock_Requested__c);
    	system.assertEquals(UserInfo.getUserId(), c.CC_Unlock_Requestor__c);
    	
    	Test.stopTest();
    }

	@isTest
    private static void testUnlockContractNotApproved() {    
    	
    	Contract c = [Select Id, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(false, c.CC_Unlock_Requested__c);
    	system.assertEquals(null, c.CC_Unlock_Requestor__c);
    	
    	Test.startTest();
    	
    	String responseStr = CC_Resubmit_Service_Contract_Controller.unlockContract(c.Id);
    	LightningResponseBase response = (LightningResponseBase)JSON.deserialize(responseStr, LightningResponseBase.class);
    	
    	system.assertEquals(false, response.success);
    	system.assertEquals('The contract must be in Approved status to use this button.', response.errorMessage);  	

    	c = [Select Id, CC_Unlock_Requested__c, CC_Unlock_Requestor__c From Contract];
    	system.assertEquals(false, c.CC_Unlock_Requested__c);
    	system.assertEquals(null, c.CC_Unlock_Requestor__c);
    	
    	Test.stopTest();
    }        
}