/*
 * Revisions:
02-Apr-2024 : Update code against Case #03073249 - AMS Team
*/

public class EMEIA_OpportunityHandler {
    public static Map<Id, String> OpportunityRTMap = new Map<Id, String>();
    public static Map<String, List<Opportunity>> SBUTriggerNewMap = new Map<String, List<Opportunity>>();
    public static Map<String, Map<Id,Opportunity>> SBUTriggerOldMap = new Map<String, Map<Id,Opportunity>>();
    public static Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Opportunity');
    public static Id STANDARD_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
    public static Id LIFESCIENCES_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('LFS_Opportunity').getRecordTypeId();
    public static Id NEW_STANDARD_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
    //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - BEGIN
    public static Id HIBON_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Hibon_Opportunity').getRecordTypeId();
    public static Id IRCOMPEU_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
    public static Id IRCOMPEU_INDIRECT_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU_Indirect').getRecordTypeId();
    public static Id IRCOMPMEIA_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_MEIA').getRecordTypeId();
    public static Id IRCOMPMEIA_INDIRECT_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_MEIA_Indirect').getRecordTypeId();
    public static Id IRCOMP_Global_NEW_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_Global_New').getRecordTypeId();
    public static Id IRCOMPMEIA_NEW_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_MEIA_NEW').getRecordTypeId();
    public static Id IRCOMPOEM_EU_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_OEM_EU').getRecordTypeId();
    public static Id GLOBAL_ENGINEERED_SERVICE_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Global_Engineered_Services').getRecordTypeId();    
    public static Id MILTON_ROY_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Opportunity').getRecordTypeId();
    public static Id New_MILTON_ROY_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Milton_Roy_Opportunity').getRecordTypeId();
    public static Id JDE_Account_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account').getRecordTypeId();
    public static Id JDE_Account_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account_New').getRecordTypeId();
    public static Id ARO_Account_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('New_ARO_Account').getRecordTypeId();
    public static Id ARO_Account_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ARO_Account').getRecordTypeId();
    //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - END
    // Changes by Capgemini Devender Singh Date 23/6/2023. --312 START 
    public static Id GD_Parent_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent_New').getRecordTypeId();
    public static Id GD_Parent_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
    // Changes by Capgemini Devender Singh Date 23/6/2023. --312 END 
    public static List<Opportunity> newPTOppties = new List<Opportunity>();
    public static Map<Id,Opportunity> oldPTOpptiesMap = new Map<Id,Opportunity>();
    public static Map<Opportunity,String[]> opportunityErrors = new Map<Opportunity,String[]>();
    public static Id STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Standard_Lead').getRecordTypeId();
    public static Id NEW_STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('New_Standard_Lead').getRecordTypeId();
    public static Boolean hasRanOnce = false;
    public static User user = new User();
    public static List<RecordType> rtList = new List<RecordType>();
    public static Id NEW_PTL_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_PTL_Opportunity').getRecordTypeId();
    public static Id PTL_OPP_RT_ID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('PTL_Opportunity').getRecordTypeId();


    public static void beforeInsert(List<Opportunity> newOpp,User user){
        // Changes by Capgemini Aman Kumar Date 8/2/2023. START 
        businessRelationshipInactiveCheck(newOpp,user);
        // Changes by Capgemini Aman Kumar Date 8/2/2023. END 
        
        // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -START 
        checkAccountonOpportunities(newOpp,null,true);
        // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -END 

        // Changes by Capgemini Aman Kumar Date 10/3/2023. START SFAVPTECH-667
        System.debug('opportunityErrors Insert>>>'+opportunityErrors);
        for (Opportunity opp : opportunityErrors.keySet()){
            String[] errors = opportunityErrors.get(opp);
            for (String err : errors){
                System.debug('err>>>'+err);
                opp.addError(err);
                Break;
            }   
            
        }
        // Changes by Capgemini Aman Kumar Date 10/3/2023. END SFAVPTECH-667
     
        //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - BEGIN
        updateOppStageDefaultOnLeadConversion(newOpp);
        //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - END
    }
    public static void beforeUpdate(List<Opportunity> newOpp, List<Opportunity> oldOpp, Map<id,Opportunity> oldOpportunities,User user, List<RecordType> rtList){
        getDetails(newOpp,oldOpp,rtList);
        if (!softDisable.updateDisabled()) { 
            PT_OpportunityTriggerHandler.OppPusher(newPTOppties,oldPTOpptiesMap); 
            PT_OpportunityTriggerHandler.setOpportunityCampaign(SBUTriggerNewMap.get('PT_powertools'));
        }
        businessRelationshipInactiveCheck(newOpp,user);
        
        // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -START 
        checkAccountonOpportunities(newOpp,oldOpp,false);
        
        ValidateRelatedCasesStatus(newOpp,oldOpportunities);
        
        System.debug('opportunityErrors Update>>>'+opportunityErrors);
        for(Opportunity opp : newOpp){
            for (Opportunity opptemp : opportunityErrors.keySet()){
                if(opp.id == opptemp.id){
                    String[] errors = opportunityErrors.get(opptemp);
                    for (String err : errors){
                        System.debug('err>>>'+err);
                        opp.addError(err);
                        Break;
                    }   
                }
            }
        }
        // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -END 
    }
    
    public static void beforeDelete(List<Opportunity> newOpp, List<Opportunity> oldOpp, List<RecordType> rtList){
        getDetails(newOpp,oldOpp,rtList);
        if (!softDisable.deleteDisabled()) { 
            PT_OpportunityTriggerHandler.preventDeletion(newPTOppties,oldPTOpptiesMap);
        }
    }
    public static void afterUpdate(List<Opportunity> newOpp, List<Opportunity> oldOpp, List<RecordType> rtList){
        getDetails(newOpp,oldOpp,rtList);
        if (Trigger.isUpdate && !softDisable.updateDisabled() && checkRecursive.runOnce()) { 
            PT_OpportunityTriggerHandler.afterUpdate(newPTOppties,oldPTOpptiesMap);
        } 
    }
    
    public static void getDetails(List<Opportunity> newOpp, List<Opportunity> oldOpp, List<RecordType> rtList){
        for (RecordType rt : rtList) {
            OpportunityRTMap.put(rt.Id,rt.DeveloperName);
            SBUTriggerNewMap.put(rt.DeveloperName,new List<Opportunity>());
            SBUTriggerOldMap.put(rt.DeveloperName,new Map<Id,Opportunity>());
        }
        
        if (null!=newOpp){for (Opportunity newO : newOpp) {
            SBUTriggerNewMap.get(OpportunityRTMap.get(newO.RecordTypeId)).add(newO);}}
        if (null!=oldOpp){for (Opportunity oldO : oldOpp) {SBUTriggerOldMap.get(OpportunityRTMap.get(oldO.RecordTypeId)).put(oldO.Id,oldO);}}
        
        oldPTOpptiesMap = SBUTriggerOldMap.get('PT_powertools');
        newPTOppties = SBUTriggerNewMap.get('PT_powertools'); 
        
    }
    // Changes by Capgemini Aman Kumar Date 10/1/2023. START
    public static void validateBusiness(List<Opportunity> newOpportunities,Boolean isInsert, Map<id,Opportunity> oldOpportunities, User user) {
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');

        
        for (Opportunity opp : newOpportunities) {
            System.debug('opp.CTS_WasConverted__c '+ opp);
            System.debug('opp.CTS_WasConverted__c '+ opp.CTS_WasConverted__c);
                        
            if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){
                if (isInsert){
                    if(opp.Business__c != null) {
                        // Updated by  Snehal M as Part of User Story :-SPTL-77
                        // Added PTL record Type check 
                        if((opp.RecordTypeId == NEW_STANDARD_OPP_RT_ID || opp.RecordTypeId == NEW_PTL_OPP_RT_ID || opp.RecordTypeId == New_MILTON_ROY_OPP_RT_ID)&& !user.Business__c.containsIgnoreCase(opp.Business__c) && !opp.CTS_WasConverted__c) {
                            System.debug('Different Business!!! ');
                            // Changes by Capgemini Aman Kumar Date 10/3/2023. START SFAVPTECH-667
                            if (!opportunityErrors.containsKey(opp)){
                                opportunityErrors.put(opp,new String[]{System.Label.VPTECH_Opportunity_User_Business_Error});
                            }
                            // Changes by Capgemini Aman Kumar Date 10/3/2023. END SFAVPTECH-667
                            
                        }
                    }
                }
                // Updated by  Snehal M as Part of User Story :-SPTL-77
                        // Added PTL record Type check
                // Updated by CTS Team (Suyash) - Added PST RT (Api Name - Milton_Roy_Opportunity) as part of SM-102 
                else{
                    if ((opp.RecordTypeId == STANDARD_OPP_RT_ID || opp.RecordTypeId == PTL_OPP_RT_ID || opp.RecordTypeId == MILTON_ROY_OPP_RT_ID) && oldOpportunities.get(opp.Id) != null &&  oldOpportunities.get(opp.Id).Id == opp.Id && 
                        oldOpportunities.get(opp.Id).Business__c != opp.Business__c && !user.Business__c.containsIgnoreCase(opp.Business__c) && !opp.CTS_WasConverted__c) {
                            opp.addError(System.Label.VPTECH_Opportunity_User_Business_Error);
                            
                        }
                }
            }
            
        }
        
    }
    // Changes by Capgemini Aman Kumar Date 10/1/2023. END     
    
     
    //This code written by Capegemini to validate Business Relationship (User Story - SFAVPTECH - 169)
    // Updated by CTS Team (Suyash) - Added PST RT (Api Name - New_Milton_Roy_Opportunity) as part of SM-102    
    public static void validateBusinessRelationship(List<Opportunity> opportunities, User user) {
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        Set<Id> brId = new Set<Id>();
        Map<Id, Business_Relationship__c> brMap = new Map<Id, Business_Relationship__c>();
        for (Opportunity opportunity : opportunities) {
            if (opportunity.AccountId != null && (opportunity.RecordTypeId == NEW_STANDARD_OPP_RT_ID || opportunity.RecordTypeId == LIFESCIENCES_OPP_RT_ID || (opportunity.RecordTypeId == New_MILTON_ROY_OPP_RT_ID && opportunity.Business__c == 'MP/OB/IRP'))) { 
                brId.add(opportunity.AccountId);
            }
        }
        List<Business_Relationship__c> businessRelationships = [SELECT Id,Business__c FROM Business_Relationship__c WHERE Account__c = :brId];
        
        for (Business_Relationship__c br : BusinessRelationships) {
            brMap.put(br.Id, br); 
        }
        System.debug('BR --> '+businessRelationships);
        //  businessRelationships.size() > 0 &&
        for (Opportunity opportunity : opportunities) {
            if(businessRelationships.size() > 0 && user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){
                System.debug('opportunity.CTS_WasConverted__c '+ opportunity.CTS_WasConverted__c);
                if (opportunity.AccountId != null && !opportunity.CTS_WasConverted__c && (opportunity.RecordTypeId == NEW_STANDARD_OPP_RT_ID || opportunity.RecordTypeId == LIFESCIENCES_OPP_RT_ID || (opportunity.RecordTypeId == New_MILTON_ROY_OPP_RT_ID && opportunity.Business__c == 'MP/OB/IRP'))) {
                    boolean validBusinessRelationship = false;
                    for (Business_Relationship__c br : businessRelationships) {
                        System.debug('inside for businessRelationships '+ businessRelationships);
                        if (br.Business__c == opportunity.Business__c) {
                            validBusinessRelationship = true;
                            System.debug('check '+validBusinessRelationship);
                            break;
                        }
                    }
                    if (!validBusinessRelationship) {
                        // Changes by Capgemini Aman Kumar Date 10/3/2023. START SFAVPTECH-667
                        if (!opportunityErrors.containsKey(opportunity)){
                            opportunityErrors.put(opportunity,new String[]{System.Label.VPTECH_Opportunity_Business_Validation});
                        }
                        // Changes by Capgemini Aman Kumar Date 10/3/2023. END SFAVPTECH-667
                        //opportunity.addError(System.Label.VPTECH_Opportunity_Business_Validation);
                    }
                }
            }
            else if(!opportunity.CTS_WasConverted__c && businessRelationships.isEmpty() && (opportunity.RecordTypeId == NEW_STANDARD_OPP_RT_ID || opportunity.RecordTypeId == LIFESCIENCES_OPP_RT_ID || (opportunity.RecordTypeId == New_MILTON_ROY_OPP_RT_ID && opportunity.Business__c == 'MP/OB/IRP'))){
                opportunity.addError(System.Label.VPTECH_Opportunity_BR_Null_Error);
            }
            // opportunity.CTS_WasConverted__c = false;
        }
        
        
    }
    // Changes by Capgemini Aman Kumar Date 8/2/2023. START 
    
    public static void businessRelationshipInactiveCheck(List<Opportunity> newOpp,User user){
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){            
            
            List<Id> accountIds = new List<Id>();
            List<Account> accounts = new List<Account>();
            for(Opportunity opp : newOpp){
                if(opp.RecordTypeId == STANDARD_OPP_RT_ID || opp.RecordTypeId == NEW_STANDARD_OPP_RT_ID){
                    accountIds.add(opp.AccountId);
                }
            }
            accounts = [Select id,(Select id,status__c,Business__c from Business_Relationships__r) From Account Where id in : accountIds Limit 49999] ;
            
            for(Opportunity opp : newOpp){
                if(opp.RecordTypeId == STANDARD_OPP_RT_ID || opp.RecordTypeId == NEW_STANDARD_OPP_RT_ID){
                    for(Account acc : accounts){
                        if(acc.id == opp.AccountId){
                            for(Business_Relationship__c br : acc.Business_Relationships__r){
                                if(br.Business__c.equalsIgnoreCase(opp.Business__c) && br.Status__c == 'Inactive'){
                                    opp.addError(System.Label.VPTECH_Opportunity_inactive_Account_Error);  
                                }
                            }
                            
                        }
                    }
                    
                }
            }
        }
    }
    // Changes by Capgemini Aman Kumar Date 8/2/2023. END 
    
    // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -START 
    public static void checkAccountonOpportunities(List<Opportunity> newOpportunities,List<opportunity> oldOppList, Boolean isInsert){
        Set< Id > setOfaccountsId  = new Set< Id >();
        List<Id> convertedLeadIds = new List<id>();
        Map< Id, Account > mapOfAccount = New Map< Id, Account >(); 
        
        Boolean isFinlandUser = FeatureManagement.checkPermission('Finland_Sales');

        for (Opportunity opp : newOpportunities) {
            setOfaccountsId.add(opp.AccountId); 
            convertedLeadIds.add(opp.Converted_Lead_ID__c);
        }
        
        if(!setOfaccountsId.isEmpty()){
            for(Account objAcc :[SELECT Id,RecordType.Name,RecordTypeid  FROM Account WHERE ID in:setOfaccountsId]){
                mapOfAccount.put( objAcc.Id ,objAcc); 
            }
            
        }
        
        
        Map<id , Lead> converedLead = new Map<Id, Lead>( [Select id, RecordTypeId,Business__c from Lead Where id IN : convertedLeadIds]);
        
        for(Opportunity Opport : newOpportunities){

            //Lead conversion Validation for Cross Record Type selection Between Opportunity and Lead.
            // Updated by AMS Team (Mahesh) - #03073249 - 2APR2024;  added 'containsKey' to avoid the null pointer exception
            if(Opport.Converted_Lead_ID__c != null && converedLead.containsKey(Opport.Converted_Lead_ID__c) 
               /*|| oldOpp.Converted_Lead_ID__c != Opport.Converted_Lead_ID__c*/){
                   System.debug('Opport.RecordTypeId 1>>>>'+Opport.RecordTypeId);
                 //  System.debug('converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId 1>>>>'+converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId);
                   //System.debug('converedLead.get(Opport.Converted_Lead_ID__c).Business__c 1>>>>'+converedLead.get(Opport.Converted_Lead_ID__c).Business__c);
                   if((Opport.RecordTypeId == STANDARD_OPP_RT_ID  || Opport.RecordTypeId == NEW_STANDARD_OPP_RT_ID || Opport.RecordTypeId == MILTON_ROY_OPP_RT_ID  || Opport.RecordTypeId == New_MILTON_ROY_OPP_RT_ID) 
                      && converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId !=STANDARD_LEAD_RT_ID  
                      && converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId !=NEW_STANDARD_LEAD_RT_ID){
                          if (!opportunityErrors.containsKey(Opport)){
                              opportunityErrors.put(Opport,new String[]{System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label});
                              System.debug('Error 1>>>>'+System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label);
                          }
                          
                      }
                   else if((Opport.RecordTypeId != STANDARD_OPP_RT_ID  && Opport.RecordTypeId != NEW_STANDARD_OPP_RT_ID) 
                           && (converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                               || converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                           && converedLead.get(Opport.Converted_Lead_ID__c).Business__c  == 'Compressor' ) {
                               System.debug('Opport.RecordTypeId 2>>>>'+Opport.RecordTypeId);
                               System.debug('converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId 2>>>>'+converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId);
                               if (!opportunityErrors.containsKey(Opport)){
                                   opportunityErrors.put(Opport,new String[]{System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label});
                                   System.debug('Error 2>>>>'+System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label);
                               }
                           }

                        // Milton Roy Change
                        else if((Opport.RecordTypeId != MILTON_ROY_OPP_RT_ID  && Opport.RecordTypeId != New_MILTON_ROY_OPP_RT_ID)
                        && (converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                            || converedLead.get(Opport.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                        && converedLead.get(Opport.Converted_Lead_ID__c).Business__c  == 'Milton Roy' ) {
                            if (!opportunityErrors.containsKey(Opport)){
                                opportunityErrors.put(Opport,new String[]{System.Label.L_GD_Compressor_Lead_Conversion_to_New_Opportunity_Message_Label});
                            }

                        }  
            }
            
            // Validation for Cross RecordType Selection between Opportunity and Account
            if( oldOppList != null){
                for(Opportunity oldOpp : oldOppList){
                    System.debug('isInsert 2>>>>'+isInsert);
                        //    System.debug('mapOfAccount.size()>0 2>>>>'+mapOfAccount.size());
                        //    System.debug('Opport.Converted_Lead_ID__c 2>>>>'+Opport.Converted_Lead_ID__c);
                        //    System.debug('oldOpp.Converted_Lead_ID__c 2>>>>'+oldOpp.Converted_Lead_ID__c);
                        //    System.debug('Opport.RecordTypeId 2>>>>'+Opport.RecordTypeId);
                        //    System.debug('mapOfAccount.get(Opport.AccountId).RecordTypeid 2>>>>'+mapOfAccount.get(Opport.AccountId).RecordTypeid);
                           
                    if((isInsert && mapOfAccount.size()>0 && Opport.Converted_Lead_ID__c == null )
                       || (oldOpp.Converted_Lead_ID__c == Opport.Converted_Lead_ID__c  && !isInsert 
                           && mapOfAccount.size()>0)){
                               if((Opport.RecordTypeId== STANDARD_OPP_RT_ID || Opport.RecordTypeId== NEW_STANDARD_OPP_RT_ID)  
                                  && mapOfAccount.get(Opport.AccountId).RecordTypeid != GD_Parent_RT_ID 
                                  && mapOfAccount.get(Opport.AccountId).RecordTypeid != GD_Parent_New_RT_ID ) {
                                      if (!opportunityErrors.containsKey(Opport)){
                                          opportunityErrors.put(Opport,new String[]{System.Label.Opportunity_To_Account_Message_Label});
                                          System.debug('Error 3>>>>'+System.Label.Opportunity_To_Account_Message_Label);
                                      }
                                      //Updated by CTS Team(Suyash)--->(SM-130) - Added PST Record Type Condition
                                  } else if(!isFinlandUser && Opport.RecordTypeId!= NEW_STANDARD_OPP_RT_ID && Opport.RecordTypeId!= STANDARD_OPP_RT_ID 
                                            && Opport.RecordTypeId!= MILTON_ROY_OPP_RT_ID && Opport.RecordTypeId!= New_MILTON_ROY_OPP_RT_ID
                                            && (mapOfAccount.get(Opport.AccountId).RecordTypeid == GD_Parent_RT_ID 
                                                || mapOfAccount.get(Opport.AccountId).RecordTypeid == GD_Parent_New_RT_ID)) {
                                                    if (!opportunityErrors.containsKey(Opport)){
                                                        opportunityErrors.put(Opport,new String[]{System.Label.Opportunity_To_Account_Message_Label});
                                                        System.debug('Error 4>>>>'+System.Label.Opportunity_To_Account_Message_Label);
                                                    }
                                                }
                                // Milton Roy Change
                                else if(Opport.RecordTypeId != MILTON_ROY_OPP_RT_ID && Opport.RecordTypeId != New_MILTON_ROY_OPP_RT_ID
                                && (mapOfAccount.get(Opport.AccountId).RecordTypeid == JDE_Account_RT_ID
                                    || mapOfAccount.get(Opport.AccountId).RecordTypeid == JDE_Account_New_RT_ID
                                   	|| mapOfAccount.get(Opport.AccountId).RecordTypeid == ARO_Account_RT_ID
                                    || mapOfAccount.get(Opport.AccountId).RecordTypeid == ARO_Account_New_RT_ID)) {
                                        if (!opportunityErrors.containsKey(Opport)){
                                            opportunityErrors.put(Opport,new String[]{System.Label.Opportunity_To_Account_Message_Label});
                                        }
                                    }
                             }
                }
            }
        }
        
    }
    
    // Changes by Capgemini Aman Kumar Date 26/6/2023. SGGC 312 -END 
    
    //Changes by Capgemini Devender Singh Date 22/6/2023. START -312
    public static String getRecordTypeNameById(String objectName, Id strRecordTypeId) {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(strRecordTypeId).getDeveloperName();
    } 
    //Changes by Capgemini Devender Singh Date 22/6/2023. END -312    
    
    
    // Changes by Capgemini Aman Kumar Date 28/6/2023. SGGC 221 -START
    public static void ValidateRelatedCasesStatus(List<Opportunity> newOpportunities,Map<id,Opportunity> oldOpportunities ){
        List<id> oppIds = new List<Id>();
        
        for(Opportunity opp : newOpportunities){
            if((opp.RecordTypeId== STANDARD_OPP_RT_ID || opp.RecordTypeId== NEW_STANDARD_OPP_RT_ID || opp.RecordTypeId == MILTON_ROY_OPP_RT_ID ||  opp.RecordTypeId == New_MILTON_ROY_OPP_RT_ID) && opp.StageName == 'Stage 4. Negotiate'  && oldOpportunities.get(opp.id).stageName == 'Stage 3. Propose/Quote'){
                oppIds.add(opp.id);
            }           
        }
        
         List<Opportunity> opportunities = [Select Id, StageName, (Select id, status,Related_Opportunity__c from Cases__r) 
                                               From Opportunity Where id IN:oppIds Limit 49999];
            
        for(Opportunity opp : opportunities){
            for(Case cs : opp.Cases__r){
                if(cs.Related_Opportunity__c == opp.id && cs.Status == 'Sent for Approval'){
                    if (!opportunityErrors.containsKey(opp)){
                        opportunityErrors.put(opp,new String[]{System.Label.LGD_Case_Approval_inProgress});
                        System.debug('Case Approval Error >>>>'+System.Label.LGD_Case_Approval_inProgress);
                    } 
                    
                }
            }
            
        }
        
    }
    // Changes by Capgemini Aman Kumar Date 28/6/2023. SGGC 221 -END   
    
    //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - BEGIN
    private static void updateOppStageDefaultOnLeadConversion (List<Opportunity> oppList){
        //Sets the target Record Types for the case
        List<String> targetRecordTypes = new List<String> {HIBON_OPP_RT_ID, IRCOMPEU_OPP_RT_ID, IRCOMPEU_INDIRECT_OPP_RT_ID, IRCOMPMEIA_OPP_RT_ID,
                                                           IRCOMPMEIA_INDIRECT_OPP_RT_ID, STANDARD_OPP_RT_ID, IRCOMP_Global_NEW_OPP_RT_ID, IRCOMPMEIA_NEW_OPP_RT_ID,
                                                           IRCOMPOEM_EU_OPP_RT_ID, LIFESCIENCES_OPP_RT_ID, GLOBAL_ENGINEERED_SERVICE_OPP_RT_ID, NEW_STANDARD_OPP_RT_ID, NEW_PTL_OPP_RT_ID, PTL_OPP_RT_ID
                                                          };
                                                                                                                
        for(Opportunity opp : oppList){
            //Checks if the opportunity came from lead convertion and it is inside the record types scopes for the case  
            if(opp.CTS_WasConverted__c && targetRecordTypes.contains(opp.RecordTypeId)){
               //sets the value to Stage 1. Qualify otherwise it will start on target stage
               opp.StageName = 'Stage 1. Qualify';
            }
        }
        
    }
    //Changes by Cognizant Artur Poiata 1/15/2024 - Reference case: 02960447 - END
    
}