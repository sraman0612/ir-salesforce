/**
* @createdBy   Sangram Ray
* @date         07/30/2016
* @description  Test Class for CC_PavilionPartsOrderHistoryController
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest
public class CC_PavilionPartsOrderHistoryCtrl_Test {
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }

    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Account account;
    static CC_Order__c order;
    static CC_Order_Shipment__c shipment;
    static CC_Order_Shipment__c newShipment;
    static CC_Carriers__c carrierWebsite;
    static CC_Carriers__c carrierWebsite1;
    
    // test constructor
    @isTest
    public static void constructor_Test() {
        setup();
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrderHistory'));
            CC_PavilionPartsOrderHistoryController cc_PavilionPartsOrderHistoryController = new CC_PavilionPartsOrderHistoryController(cc_PavilionTemplateController);
        }
    }
    
    // test getOrderStatusData
    @isTest
    public static void getOrderStatusData_Test() {
        setup();
        System.runAs (testUser) {
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today()+10,
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,CC_Warehouse__c='P',
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert shipment
            shipment = new CC_Order_Shipment__c(
                Shipment_Header_Number__c = 123,
                Ship_Date__c = Date.today(),
                Invoice_Number__c = 'INV-1',
                Order__c = order.Id,
                Carrier__c = 'DHL',
                Pro_Bill__c = 'bill'
            );
            insert shipment;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrderHistory'));
            CC_PavilionPartsOrderHistoryController.getOrderStatusData(account.Id);
        }
    }
    
    // test getOrderStatusData_NoShipments
    @isTest
    public static void getOrderStatusData_NoShipments_Test() {
        setup();
        System.runAs (testUser) {
            // insert order
            CC_Order__c ordr = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert ordr;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrderHistory'));
            CC_PavilionPartsOrderHistoryController.getOrderStatusData(account.Id);
        }
    }
    
    // test getShipments
    @isTest
    public static void getShipments_Test() {
        setup();
        System.runAs (testUser) {
            // insert order
            order = new CC_Order__c(
                CC_Quote_Order_Date__c = Date.today(),
                CC_Addressee_name__c = 'Test Name',
                PO__c = '123',
                CC_Order_Number_Reference__c = '123',
                Hold_Code__c = 'code',
                Complete_Date__c = Date.today(),
                CC_Status__c = 'Open',
                CC_Account__c = account.Id,
                issystemcreated__c=TRUE,
                RecordTypeId = Schema.SObjectType.CC_Order__c.getRecordTypeInfosByName().get('Parts Ordering').getRecordTypeId()
            );
            insert order;
            
            // insert shipment
            shipment = new CC_Order_Shipment__c(
                Shipment_Header_Number__c = 123,
                Ship_Date__c = Date.today(),
                Invoice_Number__c = 'INV-1',
                Carrier__c = 'DHL',
                Order__c = order.Id,
                Pro_Bill__c = 'bill'
            );
            insert shipment;
            newShipment = new CC_Order_Shipment__c(
                Shipment_Header_Number__c = 123,
                Ship_Date__c = Date.today(),
                Invoice_Number__c = 'INV-1',
                Order__c = order.Id,
                Pro_Bill__c = 'bill'
            );
            insert newShipment;
            
            List<CC_Carriers__c> ccCarrierList = new List<CC_Carriers__c>();
            // insert carrierWebsite
            carrierWebsite = new CC_Carriers__c(
                Name = 'AVERITT',
                Website__c = 'http://www.google.com'
            );
            carrierWebsite1 = new CC_Carriers__c(
                Name = 'DHL',
                Website__c = 'http://www.google.com'
            );
            
            ccCarrierList.add(carrierWebsite);
            ccCarrierList.add(carrierWebsite1);
            
            insert ccCarrierList;
            
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionPartsOrderHistory'));
            CC_PavilionPartsOrderHistoryController.getShipments(order.Id);
        }
    }
    
    public static void setup() {
        // for CAD Pricing 
                 account = TestUtilityClass.createAccount();
         account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
         account.name = 'Test';
         account.CC_Customer_Number__c = '5621';
        insert account;


        contact = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN',
            FirstName = 'FS',
            AccountId = account.Id,
            Email = 'test@test.ingersollrand.com'
        );
        insert contact;

        portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Partner Community User%' Limit 1];
        
        
        testUser = new User(
            Username = System.now().millisecond() + 'test12345@test1.com',
            ContactId = contact.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty1',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert testUser;
        
         //AccountTeamMember for Account share error
        
        AccountTeamMember atm = TestUtilityClass.createATM(account.Id,testUser.Id);
        insert atm;
        
        AccountShare accShare = TestUtilityClass.createAccShare(account.Id,testUser.Id);
        insert accShare;
    }
}