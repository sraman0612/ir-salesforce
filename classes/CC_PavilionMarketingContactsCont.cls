global without sharing class CC_PavilionMarketingContactsCont {
  public CC_PavilionMarketingContactsCont(CC_PavilionTemplateController controller) {}
  public static List<User> getMarUserContacts() {
    User [] usrLst = [SELECT id,Department,CC_Department__c,Name,Email,Phone,PS_Business_Unit_Description__c,Email_To_Display__c,Title, Extension
                        FROM User
                       WHERE PS_Business_Unit_Description__c = 'CLUB CAR' and 
                             CC_Department__c='Marketing' AND IsActive = TRUE];
    return usrLst; 
  } 
}