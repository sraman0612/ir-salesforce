/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioJsonParser {
    global TwilioJsonParser(String jsonString) {

    }
    global static TwilioSF.TwilioJsonParser createParser(String jsonString) {
        return null;
    }
    global TwilioSF.TwilioJsonParser.JSONField get(String key) {
        return null;
    }
    global static List<TwilioSF.TwilioJsonParser> parseArray(String jsonArray) {
        return null;
    }
global class JSONField {
    global String Value {
        get;
    }
    global JSONField(String jsonString) {

    }
    global JSONField(String jsonString, String currentValue) {

    }
    global TwilioSF.TwilioJsonParser.JSONField get(String key) {
        return null;
    }
    global Boolean getBoolean() {
        return null;
    }
    global Date getDate() {
        return null;
    }
    global Datetime getDatetime() {
        return null;
    }
    global Double getDouble() {
        return null;
    }
    global Integer getInteger() {
        return null;
    }
    global String getString() {
        return null;
    }
}
}
