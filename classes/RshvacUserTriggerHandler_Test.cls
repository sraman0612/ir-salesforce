@isTest
private class RshvacUserTriggerHandler_Test {

  // Data used to create the User needed for the test
  private static final String FIRST_NAME = 'Thomas';
  private static final String LAST_NAME = 'Pynchon';
  private static final String USERNAME = 'tpynchon@nowhere.com';
  private static final String NICKNAME = 'TPynch';
  private static final String LOCALE = 'en_US';
  private static final String TIMEZONE = 'America/New_York';
  private static final String ENCODING = 'ISO-8859-1';
  private static final String PROFILE_NAME = 'RHVAC Sales';
  // Sales Person Ids
  private static final String FIRST_SP_ID = '4242424242';
  private static final String SECOND_SP_ID = '2424242424';
  // Number of accounts for each Sales Person ID
  private static final Integer FIRST_SP_ID_ACCOUNTS = 3;
  private static final Integer SECOND_SP_ID_ACCOUNTS = 5;
  // Developer Name of the RS_HVAC Record type
  private static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';

  /**
	 *	Sets the data need for the tests
	 */
	@testSetup static void mySetup() {
    // Get the RS_HVAC Record Type Id
    Id rshvacRTID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :RS_HVAC_RT_DEV_NAME AND SobjectType = 'Account' LIMIT 1].Id;
    List<Account> accountsToInsert = new List<Account>();
    // Create the accounts for the first Sales Person Id
    for (Integer i = 0;i < FIRST_SP_ID_ACCOUNTS; i++){
      accountsToInsert.add(new Account(Name = FIRST_SP_ID + i, RecordTypeId = rshvacRTID, Sales_Person_Id__c = FIRST_SP_ID));
    }
    // Create the accounts for the first Sales Person Id
    for (Integer i = 0;i < SECOND_SP_ID_ACCOUNTS; i++){
      accountsToInsert.add(new Account(Name = SECOND_SP_ID + i, RecordTypeId = rshvacRTID, Sales_Person_Id__c = SECOND_SP_ID));
    }
    // Insert the Accounts
    insert accountsToInsert;
  }

  /**
	 *	Tests that the logic on the insert/update trigger is correct
	 */
	static testMethod void testModifySalesPersonId() {
    // Get the Profile Id to assign to the new User
    Id profileId = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME LIMIT 1].Id;
    Test.startTest();
      // Insert a new User, it'll initially have the FIRST_SP_ID as Sales Person Id
      User testUser = new User(Sales_Person_Id__c = FIRST_SP_ID, FirstName = FIRST_NAME, LastName = LAST_NAME, ProfileId = profileId,
                                UserName = USERNAME, Email = USERNAME, CommunityNickname = NICKNAME, LocaleSidKey = LOCALE, IsActive = true,
                                TimeZoneSidKey = TIMEZONE, EmailEncodingKey = ENCODING, LanguageLocaleKey = LOCALE, Alias = NICKNAME);
      insert testUser;
      // Get the accounts that the User should own after the insert
      List<Account> ownedAccountsAfterInsert = [SELECT Id FROM Account WHERE OwnerId = :testUser.Id];
      // Change the Sales Person Id and update the test User
      testUser.Sales_Person_Id__c = SECOND_SP_ID;
      update testUser;
      // Get the accounts that the User should own after the update
      List<Account> ownedAccountsAfterUpdate = [SELECT Id FROM Account WHERE OwnerId = :testUser.Id];
    Test.stopTest();
    // Verify that the right amount of Accounts are owned after the insert
    System.assertEquals(FIRST_SP_ID_ACCOUNTS, ownedAccountsAfterInsert.size());
    // Verify that the right amount of Accounts are owned after the update
    System.assertEquals(FIRST_SP_ID_ACCOUNTS + SECOND_SP_ID_ACCOUNTS, ownedAccountsAfterUpdate.size());
  }
}