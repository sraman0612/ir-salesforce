public class OpportunityUtils {
    //cost_center_name__c,gross_sales__c field commented by Yadav Vineet Capgemini for descoped field as per Prashanth 

    // Opportunity Record Type Developer Names, Added Strategic type by Ashwini: 15-12-17
    public static final string RECORD_TYPE_EXISTING = 'CTS_MEIA';
    public static final string RECORD_TYPE_IWD_NEW_BUSINESS = 'IWD_New_Business';
    public static final string RECORD_TYPE_NEW_BUSINESS = 'CTS_MEIA';
    public static final string RECORD_TYPE_NEW_CUSTOMER = 'New_Customer';
    public static final string RECORD_TYPE_STRATEGIC_IWD = 'Strategic_Opportunity_IWD';
    public static final string RECORD_TYPE_STRATEGIC_DSO = 'Strategic_Opportunity_DSO';
    // Get the Current Year dinamically by using the standard Date class methods
    public static final Integer CURRENT_YEAR = Date.today().year();
    // Map that contains all Opportunity Record Types by Developer Name
    public static Map<String, RecordType> OPP_RECORD_TYPES = new Map<String, RecordType>();
    public static final List<String> monthNames = new List<String>{'January', 'February', 'March', 'April', 'May',
                    'June', 'July', 'August', 'September', 'October', 'November', 'December'};
    // Getting Project Type values
    public static final String Multi_Family_NC = 'Multi-Family - NC';
    public static final String Multi_Family_Replacement = 'Multi-Family - Replacement';
    public static final String Single_Family_NC = 'Single Family - NC';
    public static final String Single_Family_Non_Owner_Occup = 'Single Family Non Owner Occupied';
    //Getting channel of Account
    public static final String IWD = 'IWD';
    public static final String DSO = 'DSO';                    
    // Static initializer
    static{
      for (RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Opportunity']){
        OPP_RECORD_TYPES.put(rt.DeveloperName, rt);
      }
    }
/*Commented as part of EMEIA Cleanup
    // Creates DSO Account's Opportunity Revenue Schedules for twelve months of current year.
    public List<Opportunity_Revenue_Schedule__c> createRevenueSchedule(RS_District_Seasonal_Schedule__c schedule, Decimal salesBudget) {

        List<Opportunity_Revenue_Schedule__c> records = new List<Opportunity_Revenue_Schedule__c>();

        for (Integer monthIter = 0; monthIter < 12; monthIter++) {
            Opportunity_Revenue_Schedule__c revSched = new Opportunity_Revenue_Schedule__c();
            revSched.Month__c = monthNames[monthIter];
            //revSched.Opportunity__c = opportunity.Id;
            revSched.Actual__c = 0;
            revSched.Budget__c = salesBudget != null ? (schedule != null ? salesBudget * ((Decimal) schedule.get(revSched.Month__c +'__c')) : salesBudget / 12) : 0;
            revSched.Forecast_Adjustment__c = revSched.Budget__c;
            
            Integer lastDate = Date.daysInMonth(CURRENT_YEAR, monthIter + 1);
            Date forecastMonth = Date.newInstance(CURRENT_YEAR, monthIter + 1, lastDate);
            revSched.Forecast_Month__c = forecastMonth;

            records.add(revSched);
        }
        return records;
    }*/
    
    // Creates Non DSO Cost Center's Opportunity Revenue Schedules for twelve months of current year.
    public Static List<Opportunity_Revenue_Schedule__c> createIwdRevenueSchedule() {

        List<Opportunity_Revenue_Schedule__c> records = new List<Opportunity_Revenue_Schedule__c>();

        for (Integer monthIter = 0; monthIter < 12; monthIter++) {
            Opportunity_Revenue_Schedule__c revSched = new Opportunity_Revenue_Schedule__c();
            revSched.Month__c = monthNames[monthIter];
            revSched.Actual__c = 0;
            revSched.Budget__c = 0;
            revSched.Forecast_Adjustment__c = revSched.Budget__c;
            
            Integer lastDate = Date.daysInMonth(CURRENT_YEAR, monthIter + 1);
            Date forecastMonth = Date.newInstance(CURRENT_YEAR, monthIter + 1, lastDate);
            revSched.Forecast_Month__c = forecastMonth;

            records.add(revSched);
        }
        return records;
    }

    // Creates Existing Business Opportunity for DSO Account
    public Opportunity createExistingBusinessOpportunity(Account account) {
        ID oppID = null;

        Opportunity newOpp = new Opportunity();
      //  newOpp.name = 'FY' + CURRENT_YEAR + ' ' +  account.Name; Commented for Case-23786 , Name is replaced by Alias Name
        newOpp.name = 'FY' + CURRENT_YEAR + ' ' +  account.Alias_Name__c;
        newOpp.recordTypeId = OPP_RECORD_TYPES.get(RECORD_TYPE_EXISTING).Id;
        newOpp.accountId = account.Id;
        newOpp.CloseDate = Date.newInstance(CURRENT_YEAR, 12, 31);
        //Commented as part of EMEIA Cleanup
        //newOpp.Sales_Budget__c = account.RHVAC_Expected_Annual_Purchases__c;
        newOpp.Type = 'Budgetary';

        return newOpp;
    }
    // Commenting as part of EMEIA Cleanup.
    /*
    // Creates Existing Business Opportunity for Non DSO Cost Center.
    public static Opportunity createIwdExistingBusinessOpportunity(Cost_Center__c costCenter) {
        ID oppID = null;

        Opportunity newOpp = new Opportunity();
        newOpp.name = 'FY' + CURRENT_YEAR + ' ' +  costCenter.Name;
        newOpp.recordTypeId = OPP_RECORD_TYPES.get(RECORD_TYPE_EXISTING).Id;
        newOpp.StageName = 'On-Track';
        newOpp.CloseDate = Date.newInstance(CURRENT_YEAR, 12, 31);
        newOpp.Type = 'Budgetary';
        //newOpp.Cost_Center_Name__c = costCenter.Id;
        newOpp.OwnerId = costCenter.OwnerId;
        return newOpp;
    }

    // Updates Existing Business Opportunity's Amount.
    public void updateExistingOpportunityAmount(Opportunity opp, RSHVAC_Order__c newOrder, RSHVAC_Order__c oldOrder) {
        // Check if the amount is not null
        opp.Amount = opp.Amount != null ? opp.Amount : 0;
        // Add the Billed today value of the Updated Order
        opp.Amount += newOrder.Billed_Today__c != null ? newOrder.Billed_Today__c : 0;
        // If there's an Old Order, subtract its Billed today value to get the actual increase
        opp.Amount -= oldOrder != null && oldOrder.Billed_Today__c != null ? oldOrder.Billed_Today__c : 0;
    }

    // Updates Opportunity Revenue Schedule's Actual field on insertion or updation of Order.
    public void updateExistingOpportunityRevenueScheduleForOrder(Opportunity_Revenue_Schedule__c revenue, RSHVAC_Order__c newOrder, RSHVAC_Order__c oldOrder) {
        // Check if the Acutal is not null
        revenue.Actual__c = revenue.Actual__c != null ? revenue.Actual__c : 0;
        // Add the Billed today value of the Updated Order
        revenue.Actual__c += newOrder.Billed_Today__c != null ? newOrder.Billed_Today__c : 0;
        // If there's an Old Order, subtract its Billed today value to get the actual increase
        revenue.Actual__c -= oldOrder != null && oldOrder.Billed_Today__c != null ? oldOrder.Billed_Today__c: 0;
    }
    */
    // Updates Opportunity Revenue Schedule's fields on insertion or updation of new business opportunity.
    // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed or RNC/NOO segment sales by opportunity type because this data is
    // being displayed on a Tableau report now. This code is also causing issues noted in ticket 7532561.
    /*
    public static void updateExistingOpportunityRevenueScheduleForOpportunity(Opportunity_Revenue_Schedule__c revenue, Opportunity newOpp, Opportunity oldOpp, Boolean oppUpdate) {
        if(newOpp.Type == Single_Family_Non_Owner_Occup){
          revenue.Single_Family_NOO_Actual__c = revenue.Single_Family_NOO_Actual__c != null ? revenue.Single_Family_NOO_Actual__c : 0;
          revenue.Single_Family_NOO_Actual__c += newOpp.Todays_Claim_Billed__c != null ?  newOpp.Todays_Claim_Billed__c : 0 ;
          revenue.Single_Family_NOO_Actual__c -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ?  oldOpp.Todays_Claim_Billed__c : 0 ;
        }
        else if(newOpp.Type == Multi_Family_Replacement){
          revenue.Multi_Family_Replacement_Actual__c = revenue.Multi_Family_Replacement_Actual__c != null ? revenue.Multi_Family_Replacement_Actual__c : 0;
          revenue.Multi_Family_Replacement_Actual__c += newOpp.Todays_Claim_Billed__c != null ?  newOpp.Todays_Claim_Billed__c : 0 ;
          revenue.Multi_Family_Replacement_Actual__c -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ?  oldOpp.Todays_Claim_Billed__c : 0 ;
        }
        else if(newOpp.Type == Multi_Family_NC){
          revenue.Multi_Family_NC_Actual__c = revenue.Multi_Family_NC_Actual__c != null ? revenue.Multi_Family_NC_Actual__c : 0;
          revenue.Multi_Family_NC_Actual__c += newOpp.Todays_Claim_Billed__c != null ?  newOpp.Todays_Claim_Billed__c : 0 ;
          revenue.Multi_Family_NC_Actual__c -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ?  oldOpp.Todays_Claim_Billed__c : 0 ;
        }
        else if(newOpp.Type == Single_Family_NC){
          revenue.Single_Family_NC_Actual__c = revenue.Single_Family_NC_Actual__c != null ? revenue.Single_Family_NC_Actual__c : 0;
          revenue.Single_Family_NC_Actual__c += newOpp.Todays_Claim_Billed__c != null ?  newOpp.Todays_Claim_Billed__c : 0 ;
          revenue.Single_Family_NC_Actual__c -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ?  oldOpp.Todays_Claim_Billed__c : 0 ;
        } 
      }
      */

    // Updates Residential Quota of respective users.
    // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed or RNC/NOO segment sales by opportunity type because this data is
    // being displayed on a Tableau report now. This code is also causing issues noted in ticket 7532561.
    /*
    public static void updateResidentialQuota(Opportunity newOpp, Opportunity oldOpp, Boolean oppUpdate){
      String fieldName;
      Residential_Quota__c bdmMdmResQuo;
      Residential_Quota__c rmResQuo;
      // For Non DSO Account select respective IWD Field. 

      if(newOpp.Account.Channel__c != DSO){
        if(newOpp.Type == Single_Family_Non_Owner_Occup){
          fieldName = 'Single_Family_NOO_Actual_IWD__c';
        }
        else if(newOpp.Type == Multi_Family_Replacement){
          fieldName = 'Multi_Family_Replacement_Actual_IWD__c';    
        }
        else if(newOpp.Type == Multi_Family_NC){
          fieldName = 'Multi_Family_NC_Actual_IWD__c';    
        }
        else if(newOpp.Type == Single_Family_NC){
          fieldName = 'Single_Family_NC_Actual_IWD__c';   
        }
      }
      // For DSO Account select respective DSO Field. 
      if(newOpp.Account.Channel__c == DSO){
        if(newOpp.Type == Single_Family_Non_Owner_Occup){
          fieldName = 'Single_Family_NOO_Actual_DSO__c';
        }
        else if(newOpp.Type == Multi_Family_Replacement){
          fieldName = 'Multi_Family_Replacement_Actual_DSO__c';    
        }
        else if(newOpp.Type == Multi_Family_NC){
          fieldName = 'Multi_Family_NC_Actual_DSO__c';    
        }
        else if(newOpp.Type == Single_Family_NC){
          fieldName = 'Single_Family_NC_Actual_DSO__c';    
        }
      }
      // For Non DSO Account select the residential quota of RM.
      if(newOpp.Account.Channel__c != DSO){
        if (RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.containsKey(newOpp.Account.Cost_Center_Name__r.OwnerId)){
          rmResQuo = RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.get(newOpp.Account.Cost_Center_Name__r.OwnerId);      
        } 
      }
      // If new opportunity is of BDM type select BDM's residential quota.
      if (newOpp.Belong_to_BDM__c && RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.containsKey(newOpp.Account.Cost_Center_Name__r.BDM__c)){
          bdmMdmResQuo = RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.get(newOpp.Account.Cost_Center_Name__r.BDM__c);   
      }
      // If new opportunity is of MDM type select MDM's residential quota.
      else if (newOpp.Belong_to_MDM__c && RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.containsKey(newOpp.Account.Cost_Center_Name__r.MDM__c)){
          bdmMdmResQuo = RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.get(newOpp.Account.Cost_Center_Name__r.MDM__c);
      }
      // Update residential quota's respective field of BDM or MDM with the amount added to Gross Sales. 
      if(bdmMdmResQuo != null && String.isNotBlank(fieldName)){
        Decimal amount;
        amount = bdmMdmResQuo.get(fieldName) != Null ? (Decimal)bdmMdmResQuo.get(fieldName) : 0 ;         
        amount += newOpp.Todays_Claim_Billed__c != null ? (Decimal)newOpp.Todays_Claim_Billed__c : 0 ;    
        amount -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ? (Decimal)oldOpp.Todays_Claim_Billed__c : 0 ; 
        bdmMdmResQuo.put(fieldName, amount);
        // Ronnie Stegall (3/6/2017) - should be using resQuotasToUpdate instead of resQuotaByUserMap
        //RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.put(bdmMdmResQuo.User__c, bdmMdmResQuo);
        RshvacNewBus_OpportunityTriggerHandler.resQuotasToUpdate.put(bdmMdmResQuo.User__c, bdmMdmResQuo);

      }
      // Update residential quota's respective field of RM with the amount added to Gross Sales. 
      if(rmResQuo != null && String.isNotBlank(fieldName)){
        Decimal amount = 0;
        amount = rmResQuo.get(fieldName) != Null ? (Decimal)rmResQuo.get(fieldName) : 0 ;         
        amount += newOpp.Todays_Claim_Billed__c != null ? (Decimal)newOpp.Todays_Claim_Billed__c : 0 ;    
        amount -= (oppUpdate && oldOpp.Todays_Claim_Billed__c != null) ? (Decimal)oldOpp.Todays_Claim_Billed__c : 0 ; 
        rmResQuo.put(fieldName, amount);
        // Ronnie Stegall (3/6/2017) - should be using resQuotasToUpdate instead of resQuotaByUserMap
        //RshvacNewBus_OpportunityTriggerHandler.resQuotaByUserMap.put(rmResQuo.User__c, rmResQuo);
        RshvacNewBus_OpportunityTriggerHandler.resQuotasToUpdate.put(rmResQuo.User__c, rmResQuo);
      }
    }
    */
   /* public void updateNewBusinessOpportunityAmount(RSHVAC_Quote__c quote) {
        Id oppId = quote.Opportunity__c;

        List<RSHVAC_Quote__c> quotes =
            [SELECT ID, Siebel_ID__c, 
			Extended_Gross_Sales__c FROM RSHVAC_Quote__c WHERE Opportunity__c = :oppId];

        Decimal grossSales = 0;

        for(RSHVAC_Quote__c quoteIter : quotes) {
            grossSales += quoteIter.Extended_Gross_Sales__c;
        }

        Opportunity oppToUpdate = [SELECT ID
		//Gross_Sales__c 
		from Opportunity WHERE ID = :oppId];
        //oppToUpdate.Gross_Sales__c = grossSales;

        try {
            update oppToUpdate;
        } catch (DmlException e) {
        }
    }*/
}