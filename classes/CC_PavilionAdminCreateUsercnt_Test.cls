/**
* @createdBy    Sudeep 
* @date         07/30/2016
* @description  Test Class for CC_PavilionAdminCreateUsercnt
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
* 
*/

@isTest
public class CC_PavilionAdminCreateUsercnt_Test {

    static User testUser;
    static Contact contact;
    static Profile portalProfile;
    static Profile testProfile;
    static User testUser2;
    static Account account;
    static Contract contract;
    static Pavilion_Navigation_Profile__c pNavProfile;
    
   
    @isTest
    public static void constructor() {

        setupData(1);
        System.runAs (testUser) {
            CC_PavilionTemplateController controller1; // = new CC_PavilionTemplateController()
            //Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            System.debug('@@123 Contact ='+contact);
            //ApexPages.currentPage().getParameters().put('cntid', contact.Id);
            //System.debug('@@123 Contact ='+ApexPages.currentPage().getParameters().get('cntid'));
            CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(controller1);   
            ApexPages.currentPage().getParameters().put('cntid', contact.Id);
            System.debug('@@123 Contact ='+ApexPages.currentPage().getParameters().get('cntid'));
            instance.singleuser = testUser;
            instance.SelectedUserid = testUser.Id;
            instance.selectedExistcontactid = testUser.Id;
            instance.user = testUser;
            instance.makehideOnly = false;
               
            instance.CheckBox = true;
            instance.listToShow = 'Test';
            instance.agrType = new String[]{'test'};
            instance.profileSet = new Set<String>{'test'};
        }
    }
    
    // pass valid contact Id
    @isTest
    public static void contactverify() {
        setupData(2);
        System.runAs (testUser) {
            CC_PavilionTemplateController controller2; // = new CC_PavilionTemplateController()
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            //ApexPages.currentPage().getParameters().put('cntid', contact.Id);
            //System.debug('@@123 cont  ID  '+ApexPages.currentPage().getParameters().get('cntid'));
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(); 
        ApexPages.currentPage().getParameters().put('cntid', contact.Id);   
System.debug('@@456 cont  ID  '+ApexPages.currentPage().getParameters().get('cntid'));      
          //instance.contactverify();
        }
    }
    
    // pass null contact Id
    @isTest
    public static void contactverify2() {

        setupData(3);
        System.runAs (testUser) {
            CC_PavilionTemplateController controller3; // = new CC_PavilionTemplateController()
            //Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            ApexPages.currentPage().getParameters().put('cntid', null);
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(controller3 );  
          instance.contactverify();
        }
    }
    
    @isTest
    public static void getpavilionUsers() {
        setupData(4);
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            //Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            ApexPages.currentPage().getParameters().put('cntid', null);
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(cc_PavilionTemplateController);  
          System.assertNotEquals(null, instance.getpavilionUsers());
        }
    }
    
    @isTest
    public static void getpavilionNavProfiles() {

        setupData(5);
        
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            //Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            ApexPages.currentPage().getParameters().put('cntid', null);
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(cc_PavilionTemplateController);  
          System.assertNotEquals(null, instance.getpavilionNavProfiles());
        }
    }
    
    // null contact Id
    @isTest
    public static void insertUserandContact1() {

        setupData(6);
        
        System.runAs (testUser) {
            CC_PavilionTemplateController cc_PavilionTemplateController = new CC_PavilionTemplateController();
            //Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            ApexPages.currentPage().getParameters().put('cntid', null);
            
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(cc_PavilionTemplateController); 
             instance.getpavilionNavProfiles();
             instance.getroles();
             instance.newuser.firstname = 'Testu';
             instance.newuser.lastname = 'Testl';
             instance.selectedProfile = 'Test';
             instance.newuser.UserName = 'Test123@username.com';
             instance.newuser.Email    ='Testemail@testing.com';
             instance.newuser.CommunityNickname = 'community';
             ApexPages.currentPage().getParameters().put('selectedYesRadio','true');
        ApexPages.currentPage().getParameters().put('enableUser','true');
        ApexPages.currentPage().getParameters().put('SelectedYesContRadio','true');
        ApexPages.currentPage().getParameters().put('PartsCheck','true');
        ApexPages.currentPage().getParameters().put('ServiceCheck','true');
        ApexPages.currentPage().getParameters().put('SalesCheck','true');
        ApexPages.currentPage().getParameters().put('custViewCheckBox','true');
        ApexPages.currentPage().getParameters().put('PartsPricingCheck','true');
        ApexPages.currentPage().getParameters().put('PartsOrderInvoicePricingCheck','true');
        ApexPages.currentPage().getParameters().put('PricingCheck','true');
        ApexPages.currentPage().getParameters().put('WarrantyTavantCheck','true');
          instance.insertUserandContact();
        }

       
    }
    
    // valid contact Id
    @isTest
    public static void insertUserandContact2(){

        setupData(7);
        System.runAs (testUser2) {
            CC_PavilionTemplateController controller4; // = new CC_PavilionTemplateController()
            Test.setCurrentPageReference(new PageReference('Page.CC_PavilionAdminCreateUser'));
            ApexPages.currentPage().getParameters().put('cntid', contact.Id);
          CC_PavilionAdminCreateUsercnt instance = new CC_PavilionAdminCreateUsercnt(controller4); 
          instance.selectedProfile = 'Test';
          instance.mapForNavProfile = new   Map<String,String>(); 
           
          instance.mapForNavProfile.put('Test','Test');       
          instance.insertUserandContact();
        }
    }
    
    
    public static void setupData(integer i) {

       
           //creating test Data
        TestDataUtility testData = new TestDataUtility();

         testUser = createPartnerUser(i);   
          
    testProfile = [SELECT Id, Name FROM Profile WHERE Name='System Administrator'];
    
    testUser2 = new User (LastName = 'CreateTest', Email ='CreateTest@gmail.com', 
                             CommunityNickname = 'CreateTest', Alias = 'crttst', 
                             Username = 'crttst@gmail.com',             
                             TimeZoneSidKey  = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US', LanguageLocaleKey = 'en_US', ProfileId = testProfile.Id,
                             EmailEncodingKey = 'UTF-8',CC_Pavilion_Navigation_Profile__c = 'TestProf', CC_View_Parts_Order_Invoice_Pricing__c = true,
                             Pavilion_Tiles__c = ';Custom Solutions Project Request;CustomerVIEW;Marketing Events;PreApproved Requests;Transportation Requests;Marketing_MyCo-OpAccount;Marketing Hole-In-One');          
        insert testUser2;
        
        system.runAs(testuser){
            
        
       
        
        contract = new Contract(
            name = 'Test Contract',
            CC_Sub_Type__c = 'Test',
            CC_Type__c = 'Dealer/Distributor Agreement',
            AccountId = account.Id
        );
        insert contract;
        }     
    }
    
     
    @testSetup static void setupData1() {
    List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
psettingList = TestUtilityClass.createPavilionSettings();
insert psettingList;
}

    
    
   public static  user createPartnerUser(integer i){
        
     
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           

            //creating county
            CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            insert county;
            
             //creating sales rep here
              TestDataUtility testData = new TestDataUtility();
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;
            
             pNavProfile = new Pavilion_Navigation_Profile__c(
            Name = 'Test',
            Description__c = 'Test',
            Region__c = 'Test',
            Agreement_Sub_Type__c = 'Test',
            Display_Name__c = 'Test'
        );
        insert pNavProfile;
            //Create account
            account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,CC_Global_Region__c = 'Test',
                                                BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566',
                                  CC_Billing_County__c = county.id,BillingState = 'GA',CC_Shipping_Billing_Address_Same__c=true, CC_Sales_Rep__c = salesRepacc.Id);
            Database.insert(account);

            //Create contact
            contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = account.Id,
                                           Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            Database.insert(contact);

            //Create user
            Profile portalProfile = [SELECT Id FROM Profile where name ='Partner Community User' LIMIT 1];
            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+i,
                                 ContactId = contact.Id,ProfileId = portalProfile.Id,
                                 Alias = 'test123',Email = 'test12345@test.ingersollrand.com',
                                 EmailEncodingKey = 'UTF-8',LastName = 'McTesty',
                                 CommunityNickname = 'test12345',TimeZoneSidKey = 'America/New_York',country='USA',
                                 LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US');
            Database.insert(user1);
        }
       return user1; 
    }
}