//
// (c) 2015, Appirio Inc
//
// Test class for Account Trigger
//
// Apr 28, 2015     Barkha Jain     original
// May 22,2015      Surabhi Sharma  Updated
//
@isTest
private class Account_TriggerHandler_Test {
    // Method to test account trigger
    static testMethod void testAccountTrigger() {
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'CTS_MEIA' 
                             and SobjectType='Account' limit 1].Id;
        
        List<account> acclist = new List<account>();
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        acc.AccountSource='Data.com';
        //insert acc;
        acclist.add(acc);
        
        Account acc1 = new Account();
        acc1.Name = 'srs_2';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry = 'USA';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        //insert acc1;
        acclist.add(acc1);
        insert acclist;
        //acc1.Siebel_ID__c = '123457';
        acclist[1].Siebel_ID__c = '123457';
        //update acc1;
        update acclist[1];
        
        
        Test.startTest();
        //merge acc acc1;
        merge acclist[0] acclist[1];
        
        //system.assert([SELECT count() FROM OBM_Notifier__c] > 0);  
        
        acc.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
        update acc;
        
        Account[] sObjQuery = [SELECT Id FROM Account WHERE Id = :acclist[0].Id];
        System.assertEquals(1, sObjQuery.size()); 
        Test.stopTest(); 
        
    }
    
    
    static testmethod void testAccountTriggerBulk(){
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'CTS_MEIA' 
                             and SobjectType='Account' limit 1].Id;
        Account[] accounts = new Account[]{};
            // Decreased iterations from 199 to 100 due to Apex CPU time limit exceeded error
            for (integer i=0;i<20;i++)
        {
            Account acc2 = new Account();
            acc2.Name = 'srs_145' + i;
            acc2.BillingCity = '654' + i+'city2ere' +i;
            acc2.BillingCountry = 'USA';
            acc2.BillingPostalCode = String.valueOf(i + 10000);
            acc2.BillingState = 'CA';
            acc2.BillingStreet = '125' + i + ' billing street ' + i;
            acc2.Siebel_ID__c = i + '123456453';
            acc2.ShippingCity = '655' + i+'city2ere' +i; 
            acc2.ShippingCountry = 'USA'; 
            acc2.ShippingState = 'CA'; 
            acc2.ShippingStreet = '126' + i + ' Shipping street ' + i; 
            acc2.ShippingPostalCode = String.valueOf(i + 10000);
            acc2.County__c = 'testCounty1';
            acc2.RecordTypeId = AirNAAcctRT_ID;
            acc2.AccountSource='Data.com';
            accounts.add(acc2);
        } 
        
        Test.startTest();
        
        insert accounts;
        
        Test.stopTest();
        
        update accounts;
        delete accounts;
    }
    
    static testMethod void testAccountTrigger1() {
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'CTS_MEIA' 
                             and SobjectType='Account' limit 1].Id;
        
        List<account> acclist = new List<account>();
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        acc.AccountSource='Data.com';
        acc.Is_Approved__c = true;
        //insert acc;
        acclist.add(acc);
        
        Account acc1 = new Account();
        acc1.Name = 'srs_2';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry = 'USA';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        acc1.IRIT_Dealer_Approved_By_text__c = 'Test';
        acc1.Is_Approved__c = true;
        //insert acc1;
        acclist.add(acc1);
        insert acclist;
        
        List<Account> newList = new List<Account>();
        newList.addAll(acclist);
        
        //acc1.Siebel_ID__c = '123457';
        acclist[1].IRIT_Dealer_Approved_By_text__c = 'Test Updated';
        //update acc1;
        update acclist[1];
        
        Map<Id,Account> oldMap  = new Map<Id,Account>();
        for(Account acc2 : acclist){
            oldMap.put(acc2.id, acc2);
        }
        
        
        Test.startTest();
        Account_TriggerHandler.beforeUpdate(acclist, oldMap);
        Account_TriggerHandler.afterUpdate(acclist);
        Account_TriggerHandler.afterDelete(acclist);
        
        Test.stopTest(); 
        
    }
    
    static testMethod void testAccountTrigger2() {
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        Lead l2 = new Lead();
        l2.firstname = 'George';
        l2.lastname = 'Acker';
        l2.phone = '1234567890';
        l2.City = 'city';
        l2.Country = 'USA';
        l2.PostalCode = '12345';
        l2.State = 'AK';
        l2.Street = '12, street1';
        l2.company = 'ABC Corp';
        l2.Business__c='Compressor';
        l2.county__c = 'test county';
        l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
        insert l2;
        
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'GD_Parent' 
                             and SobjectType='Account' limit 1].Id;
        
        List<account> acclist = new List<account>();
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123'; 
        acc.LeadConversion__c='True';
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        acc.AccountSource='Data.com';
        acc.Is_Approved__c = true;
        acc.Converted_Lead_ID__c=l2.Id;
        acc.Phone='9971602209';
        acc.Type='Indirect';
        //acc.Converted_Lead_ID__c='00Q7i00000BRjCx';
        
        acclist.add(acc);
        
        Account acc1 = new Account();
        acc1.Name = 'srs_2';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry = 'USA';
        acc1.LeadConversion__c='True';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        acc1.IRIT_Dealer_Approved_By_text__c = 'Test';
        acc1.Is_Approved__c = true;
        acc1.Phone='9971602209';
        acc1.Type='Indirect';
        //acc1.Converted_Lead_ID__c='00Q7i00000BRjCx';
        
        acclist.add(acc1);
        try{
            insert acclist;
        }catch(exception ex){
            System.debug('Exception Catch>>'+ex.getMessage());
            System.debug('assert Contains>>'+ex.getMessage().contains('Please, select the right Account for the Business (IR or GD) to convert the Lead'));
            
        }
        
        
        Test.startTest();
        Test.stopTest(); 
        
    }
    static testMethod void testAccountTrigger3() {
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        Lead l2 = new Lead();
        l2.firstname = 'George';
        l2.lastname = 'Acker';
        l2.phone = '1234567890';
        l2.City = 'city';
        l2.Country = 'US';
        l2.PostalCode = '12345';
        l2.State = 'AK';
        l2.Street = '12, street1';
        l2.company = 'ABC Corp';
        l2.Business__c='Compressor';
        l2.county__c = 'test county';
        l2.AIRD_Closed_Won_Total__c = 20000;
        l2.CTS_Value_Stream__c ='Completes';
        l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard Lead').getRecordTypeId();
        insert l2;
        
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'CTS_EU' 
                             and SobjectType='Account' limit 1].Id;
        
        List<account> acclist = new List<account>();
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123'; 
        acc.LeadConversion__c='True';
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        acc.AccountSource='Data.com';
        acc.Is_Approved__c = true;
        acc.Converted_Lead_ID__c=l2.Id;
        acc.Phone='9971602209';
        acc.Type='Indirect';
        //acc.Converted_Lead_ID__c='00Q7i00000BRjCx';
        
        acclist.add(acc);
        
        Account acc1 = new Account();
        acc1.Name = 'srs_2';
        acc1.BillingCity = 'rsr_!city2';
        acc1.BillingCountry = 'USA';
        acc1.BillingPostalCode = '1234557858898';
        acc1.BillingState = 'CA';
        acc1.BillingStreet = '123, rsr_!!street2';
        acc1.ShippingCity = 'city3';
        acc1.ShippingCountry = 'USA';
        acc1.LeadConversion__c='True';
        acc1.ShippingState = 'CA';
        acc1.ShippingStreet = '132, street2';
        acc1.ShippingPostalCode = '1234';
        acc1.County__c = 'testCounty2';
        acc1.RecordTypeId = AirNAAcctRT_ID; 
        acc1.AccountSource='Data.com';
        acc1.IRIT_Dealer_Approved_By_text__c = 'Test';
        acc1.Is_Approved__c = true;
        acc1.Phone='9971602209';
        acc1.Type='Indirect';
        //acc1.Converted_Lead_ID__c='00Q7i00000BRjCx';
        
        acclist.add(acc1);
        try{
            Test.startTest();
            insert acclist;
            Test.stopTest(); 
        }catch(exception ex){
            System.debug('Exception Catch>>'+ex.getMessage());
            System.debug('assert Contains>>'+ex.getMessage().contains('Please, select the right Account for the Business (IR or GD) to convert the Lead'));
            
        }
         
    }
    static testMethod void testAccountTrigger4() {
        
        Trigger_Soft_Disable__c custSetting = new Trigger_Soft_Disable__c(Name = 'Account',disable_insert__c = false,
                                                                          disable_update__c = false,
                                                                          disable_delete__c = false,
                                                                          disable_undelete__c = false);
        insert custSetting;
        
        Lead l2 = new Lead();
        l2.firstname = 'George';
        l2.lastname = 'Acker';
        l2.phone = '1234567890';
        l2.City = 'city';
        l2.Country = 'US';
        l2.PostalCode = '12345';
        l2.State = 'AK';
        l2.Street = '12, street1';
        l2.company = 'ABC Corp';
        l2.Business__c='Compressor';
        l2.county__c = 'test county';
        l2.AIRD_Closed_Won_Total__c= 1000;
        l2.CTS_Value_Stream__c ='Completes';
        l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard Lead').getRecordTypeId();
        insert l2;
        
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'GD_Parent' 
                             and SobjectType='Account' limit 1].Id;
        
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = '';
        acc.BillingCountry = '';
        acc.BillingPostalCode = '';
        acc.BillingState = '';
        acc.BillingStreet = '';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123'; 
        acc.LeadConversion__c='True';
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        acc.AccountSource='Data.com';
        acc.Is_Approved__c = true;
        acc.Converted_Lead_ID__c=l2.Id;
        acc.Phone='';
        acc.Type='Indirect';
        //acc.Converted_Lead_ID__c='00Q7i00000BRjCx';

        try{
            Test.startTest();
            insert acc;
            acc.Phone='9898989898';
            acc.BillingCity = 'srs_!city';
            acc.BillingCountry = 'USA';
            acc.BillingPostalCode = '674564569';
            acc.BillingState = 'CA';
            acc.BillingStreet = '12, street1678';
            update acc;
            Test.stopTest(); 
        }catch(exception ex){
            System.debug('Exception Catch>>'+ex.getMessage());
            System.debug('assert Contains>>'+ex.getMessage().contains('Please, select the right Account for the Business (IR or GD) to convert the Lead'));
            
        }
        
    }
     static testMethod void testAccountTrigger5() {
       
         Test.startTest(); 
       TestDataUtility dataUtil = new TestDataUtility();
        
        Account thisAccount = dataUtil.createAccount('PT_powertools');
        thisAccount.BillingCountry = 'USA';
        thisAccount.BillingCity = 'Augusta';
        thisAccount.BillingState = 'GA';
        thisAccount.BillingStreet = '2044 Forward Augusta Dr';
        thisAccount.BillingPostalCode = '566';
        insert thisAccount;
         Test.stopTest(); 
       
    }
    static testMethod void UpdateAccountDistributorFieldTest() {
       Id leadRecordId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Standard Lead').getRecordTypeId();
       Id idRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('New PTL Account').getRecordTypeId();
        Lead lead = New Lead(firstname = 'George',lastname = 'Acker',
                             phone = '1234567890',City = 'city',
                             Country = 'USA',PostalCode = '12345',
                             State = 'AK',Street = '12, street1',
                             company = 'ABC Corp',Business__c='Power Tools Americas',
                             county__c = 'test county',RecordTypeId =leadRecordId,
                             AIRD_Closed_Won_Total__c= 1000,
            				 CTS_Value_Stream__c ='Completes'
                             );
        insert lead;
        system.debug('idRecordId'+idRecordId);
        Account acc = new Account (RecordTypeId =idRecordId,Converted_Lead_ID__c =lead.Id,Name='from lead',
                                   BillingCity = 'city',BillingCountry = 'USA',BillingPostalCode = '12345',
                             	   BillingState = 'AK',BillingStreet = '12, street1', ShippingCity = 'city',ShippingCountry = 'USA',
                                   ShippingPostalCode = '12345',ShippingState = 'AK',ShippingStreet = '12, street1');
        test.startTest();
 			insert acc;
        test.stopTest();
       
    }
    
}