// This must implement the sfdc_checkout.CartTaxCalculations interface
// in order to be processed by the checkout flow and used for your Taxes integration.
global class welch_B2BTaxSample implements sfdc_checkout.CartTaxCalculations {
    global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
        try {
            // In the Spring '20 release, there should be one delivery group per cart.
            // In the future, when multiple delivery groups can be created,
            // this sample should be updated to loop through all delivery groups.

            // We need to get the ID of the delivery group in order to get the DeliverTo info.
            Id cartDeliveryGroupId = [SELECT CartDeliveryGroupId FROM CartItem WHERE CartId = :cartId][0].CartDeliveryGroupId;

            CartDeliveryGroup deliveryGroup = [SELECT DeliverToCity, DeliverToStreet, DeliverToState, DeliverToCountry, DeliverToPostalCode 
                                               FROM CartDeliveryGroup
                                               WHERE Id = :cartDeliveryGroupId][0];
            System.debug('deliveryGroup' + deliveryGroup); 
            
            Decimal productTotal = [SELECT TotalProductAmount
                                    FROM WebCart
                                    WHERE Id =: cartId][0].TotalProductAmount;
            System.debug('product Total: ' + productTotal);  
            Id currAcct = [SELECT  AccountId
                                    FROM WebCart
                                    WHERE Id =: cartId][0].AccountId;
            Boolean isAccountTaxExempt = false;
            Boolean isTaxableState = false;  
            //Updated by AMS Team (Artur) - #03055954 - 09JUL2024
            Boolean isTaxExemp = false;
            for(ContactPointAddress addr :  [SELECT Id, ParentId, 
                                                          Street,
                                                          Address, 
                                                          AddressType, 
                                                          Exemption_Valid_From__c, 
                                                          Exemption_Valid_To__c, 
                                                          Tax_Exempt__c
                                                FROM ContactPointAddress
                                                WHERE AddressType = 'Shipping'
                                                AND ParentId =: currAcct]){

                
                if(addr.Street == deliveryGroup.DeliverToStreet){
                    System.debug('Found a match ');
                    System.debug('Tax_Exempt__c ' + addr.Tax_Exempt__c);
                    System.debug('Exemption_Valid_To__c ' + addr.Exemption_Valid_To__c );
                    System.debug('TODAY ' + Date.TODAY());
                    if(addr.Tax_Exempt__c == true && (addr.Exemption_Valid_To__c >= Date.TODAY())){
                        System.debug('Tax Exempt User');
                        isAccountTaxExempt = true;
                    }
                }                                                    
            }
            
            //Updated by AMS Team (Artur) - #03055954 - 09JUL2024 - Checking if Tax Exempt is applied for the current account 
            //Tax_Exempt__c field is set based on Tax Exempt file, once is uploaded, an admin verifies and sets it as true is the file is correct and the user will be exempt of taxes)
			isTaxExemp = [SELECT Id, Tax_Exempt__c FROM Account where Id=:currAcct].Tax_Exempt__c;
            
            //Updated by AMS Team (Artur) - #03055954 - 09JUL2024 - Added isTaxExemp variable check
            if(String.isNotBlank(deliveryGroup.DeliverToState) && !isTaxExemp) {
                isTaxableState = getIsTaxableState(deliveryGroup.DeliverToState);
            }
            
            //Updated by AMS Team (Artur) - #03055954 - 09JUL2024 - Added isTaxExemp variable check
            //only calculate if NOT taxEmept 
            if(!isAccountTaxExempt && isTaxableState && !isTaxExemp){
                // Get all SKUs, the cart item IDs, and the total line amounts from the cart items.
                Map<String, Id> cartItemIdsBySKU = new Map<String, Id>();
                Map<String, Decimal> cartItemTotalLineAmountBySKU = new Map<String, Decimal>();
                //Need at least one sku for the CartTax 
                for (CartItem cartItem : [SELECT Sku, TotalLineAmount 
                                          FROM CartItem 
                                          WHERE CartId = :cartId 
                                          AND Type = 'Product' 
                                          AND (NOT Sku LIKE '%surcharge%')
                                          LIMIT 1 ]) {
                    cartItemIdsBySKU.put(cartItem.Sku, cartItem.Id);
                    cartItemTotalLineAmountBySKU.put(cartItem.Sku, productTotal);
                }      
                
                // Get the tax rates and tax amounts from an external service
                // Other parameters will be passed here, like ship_from, bill_to, more details about the ship_to, etc.
        System.debug('cartItemTotalLineAmountBySKU: + ' + cartItemTotalLineAmountBySKU);
                System.debug('DeliverToState: + ' +deliveryGroup.DeliverToState);
                System.debug('DeliverToCity: + ' + deliveryGroup.DeliverToCity);
                System.debug('DeliverToPostalCode: + ' + deliveryGroup.DeliverToPostalCode); 
                System.debug('getTaxRatesAndAmountsFromExternalService: + ' + getTaxRatesAndAmountsFromExternalService(
                    cartItemTotalLineAmountBySKU, deliveryGroup.DeliverToState, deliveryGroup.DeliverToCity, deliveryGroup.DeliverToPostalCode
                ));
                Map<String, TaxDataFromExternalService> rateAndAmountFromExternalServicePerSku = getTaxRatesAndAmountsFromExternalService(
                    cartItemTotalLineAmountBySKU, deliveryGroup.DeliverToState, deliveryGroup.DeliverToCity, deliveryGroup.DeliverToPostalCode
                );
               
                // If there are taxes from a previously cancelled checkout, delete them.
                List<Id> cartItemIds = cartItemIdsBySKU.values();
                delete [SELECT Id FROM CartTax WHERE CartItemId IN :cartItemIds];
                
                // For each cart item, insert a new tax line in the CartTax entity.
                // The total tax is automatically rolled up to TotalLineTaxAmount in the corresponding CartItem line.
                CartTax[] cartTaxestoInsert = new CartTax[]{};
                for (String sku : rateAndAmountFromExternalServicePerSku.keySet()) {
                    TaxDataFromExternalService rateAndAmountFromExternalService = rateAndAmountFromExternalServicePerSku.get(sku);
                    if (rateAndAmountFromExternalService == null){
                        return integrationStatusFailedWithCartValidationOutputError(
                            integStatus,
                            'The product with sku ' + sku + ' could not be found in the external system',
                            jobInfo,
                            cartId
                        );
                    }
                    // If the sku was found in the external system, add a new CartTax line for that sku
                    // The following fields from CartTax can be filled in:
                    // Amount (required): Calculated tax amount.
                    // CartItemId (required): ID of the cart item.
                    // Description (optional): Description of CartTax.
                    // Name (required): Name of the tax.
                    // TaxCalculationDate (required): Calculation date for this tax line.
                    // TaxRate (optional): The percentage value of the tax. Null if the tax is a flat amount.
                    // TaxType (required): The type of tax, e.g. Actual or Estimated.
                    CartTax tax = new CartTax( 
                        Amount = rateAndAmountFromExternalService.getAmount(),
                        CartItemId = cartItemIdsBySKU.get(sku),
                        Name = rateAndAmountFromExternalService.getTaxName(),
                        TaxCalculationDate = Date.today(),
                        TaxRate = rateAndAmountFromExternalService.getRate(),
                        TaxType = 'Actual'
                    );
                    cartTaxestoInsert.add(tax);
                }
                insert(cartTaxestoInsert);
        }
            integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
        } catch(Exception e) {
            // For testing purposes, this example treats exceptions as user errors, which means they are displayed to the buyer user.
            // In production you probably want this to be an admin-type error. In that case, throw the exception here
            // and make sure that a notification system is in place to let the admin know that the error occurred.
            // See the readme section about error handling for details about how to create that notification.
            return integrationStatusFailedWithCartValidationOutputError(
                integStatus,
                'An exception of type ' + e.getTypeName() + ' has occurred: ' + e.getMessage() + e.getStackTraceString(),
                jobInfo,
                cartId
            );
        }
        return integStatus;
    }
    
    private Map<String, TaxDataFromExternalService> getTaxRatesAndAmountsFromExternalService (
        Map<String, Decimal> cartItemTotalAmountBySKU, String state, String city, String zip) {
            System.debug('in get rates'+ cartItemTotalAmountBySKU); 
            System.debug('in get rates'+ state); 
           System.debug( 'in get rates'+ city);
             System.debug( 'in get rates'+   zip);
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            Integer SuccessfulHttpRequest = 200;
            
            request.setHeader('content-type', 'application/json');
            request.setEndpoint('callout:welch_ZipTax' + '?key=' + System.Label.welch_ZipTaxKey + '&postalcode=' + zip );
            
            request.setMethod('GET');
            HttpResponse response = http.send(request);
        system.debug(response.getStatusCode() );
            system.debug(response.getBody());
            if (response.getStatusCode() == SuccessfulHttpRequest) {
                Map<String, Object> resultsFromExternalServiceBySKU = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                List<Object> temp = (List<Object>) resultsFromExternalServiceBySKU.get('results');
                Decimal salesTax = 0.0; 
                for(Object fld : temp){    
                    Map<String,Object> data = (Map<String,Object>)fld;
                    system.Debug('city' + city); 
                    system.debug('geocity' + (String) data.get('geoCity'));
                    system.Debug('results check:'+ (city.toLowerCase()).containsAny(((String) data.get('geoCity')).toLowerCase()) );
                        if((city.toLowerCase()).containsAny(((String) data.get('geoCity')).toLowerCase())){
                            salesTax = (Decimal) data.get('taxSales');
                        }else{
                            //if I can't match the city use the first tax in response 
                            //salesTax = (Decimal) data.get('taxSales');
                        }
                   
                    
                    
                }
                Map<String, TaxDataFromExternalService> taxDataFromExternalServiceBySKU = new Map<String, TaxDataFromExternalService>();
                
                for (String sku : cartItemTotalAmountBySKU.keySet()) {
                    taxDataFromExternalServiceBySKU.put(sku, new TaxDataFromExternalService(
                                                    salesTax,
                                                    salesTax * cartItemTotalAmountBySKU.get(sku),
                                                    'Tax'
                                                ));
                    
                }
                
                
                return taxDataFromExternalServiceBySKU;
            }
            else {
                // welch_ErrorLogUtil.log(response);
                // welch_ErrorLogUtil.commitLogs();
                apexLogHandler logHandler = new apexLogHandler('welch_B2BTaxSample', 'getTaxRatesAndAmountsFromExternalService', 'There was a problem with the request. Error');
                logHandler.logHttpResponse(response);
                logHandler.saveLog();

                throw new CalloutException ('There was a problem with the request. Error: ' + response.getStatusCode());
            }
    }
    
    // Structure to store the tax data retrieved from external service
    // This simplifies our ability to access it when storing it in Salesforce's CartTax entity
    Class TaxDataFromExternalService {
        private Decimal rate;
        private Decimal amount;
        private String taxName;
        
        public TaxDataFromExternalService () {
            rate = 0.0;
            amount = 0.0;
            taxName = '';
        }
        
        public TaxDataFromExternalService (Decimal someRate, Decimal someAmount, String someTaxName) {
            rate = someRate;
            amount = someAmount;
            taxName = someTaxName;
        }
        
        public Decimal getRate() {
            return rate;
        }
        
        public Decimal getAmount() {
            return amount;
        }
        
        public String getTaxName() {
            return taxName;
        }
    }
    
    private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
        sfdc_checkout.IntegrationStatus integrationStatus, String errorMessage, sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
            integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
            // For the error to be propagated to the user, we need to add a new CartValidationOutput record.
            // The following fields must be populated:
            // BackgroundOperationId: Foreign Key to the BackgroundOperation
            // CartId: Foreign key to the WebCart that this validation line is for
            // Level (required): One of the following - Info, Error, or Warning
            // Message (optional): Message displayed to the user (maximum 255 characters)
            // Name (required): The name of this CartValidationOutput record. For example CartId:BackgroundOperationId
            // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
            // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
            CartValidationOutput cartValidationError = new CartValidationOutput(
                BackgroundOperationId = jobInfo.jobId,
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage.left(255),
                Name = (String)cartId + ':' + jobInfo.jobId,
                RelatedEntityId = cartId,
                Type = 'Taxes'
            );
            insert(cartValidationError);
            return integrationStatus;
    }
    
    //To Validate if Tax can be applied to a state
    Private Boolean getIsTaxableState(String deliverToState) {
        Tax_Setting__mdt welchTaxableStates = Tax_Setting__mdt.getInstance('Welch_Tax_Applicable_States');
        Tax_Setting__mdt welchTaxableStateCodes = Tax_Setting__mdt.getInstance('Welch_Tax_Applicable_StatesCodes');
        if(String.isNotBlank(welchTaxableStates.Values__c)) {
            List<String> stateList = welchTaxableStates.Values__c?.toLowerCase()?.split(',');
            List<String> stateCodeList = welchTaxableStateCodes.Values__c?.toLowerCase()?.split(',');
            return (new Set<String>(stateList).contains(deliverToState?.toLowerCase()) || new Set<String>(stateCodeList).contains(deliverToState?.toLowerCase()) );
        }
        return false;
    }

}