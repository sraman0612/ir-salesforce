@isTest
private class MRAssignmentGroupExtractTest{
    @isTest
    static  void getChannelPartnerInformationTest(){
        
        Id JDE_AccountRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'JDE_Account' 
                             and SobjectType='Account' limit 1].Id;
        
        Id GD_Parent_AccountRT_ID = [Select Id, DeveloperName from RecordType 
                                     where DeveloperName = 'GD_Parent' 
                                     and SobjectType='Account' limit 1].Id;

        Id ARO_AccountRT_ID = [Select Id, DeveloperName from RecordType 
        where DeveloperName = 'ARO_Account' 
        and SobjectType='Account' limit 1].Id;
        
         String DistributorLabel = System.Label.MR_Distributor_Trio;
         String RSMLabel = System.Label.RSM_Group_Name;
         String RSMLabelMPOB = System.Label.MP_OB_IRP_RSM_Group_Name;
         String DistributorLabelMPOB = System.Label.MP_OB_IRP_Distributor_Territory;
         String MRDistributorLabel = System.Label.MR_Distributor_Territory;
         String MRRSMLabel = System.Label.MR_RSM_Territory;
         String ARODistributorLabel = System.Label.ARO_Distributor_Territory;
         String ARORSMLabel = System.Label.ARO_RSM_Territory;
        
        Assignment_Group_Name__c group1 = new Assignment_Group_Name__c(Name = MRDistributorLabel);
        insert group1;
         Assignment_Group_Name__c group2 = new Assignment_Group_Name__c(Name = MRRSMLabel);
        insert group2;
        Assignment_Group_Name__c group3 = new Assignment_Group_Name__c(Name = 'RSMLabelMPOB');
        insert group3;
        Assignment_Group_Name__c group4 = new Assignment_Group_Name__c(Name = 'DistributorLabelMPOB');
        insert group4;
        Assignment_Group_Name__c group5 = new Assignment_Group_Name__c(Name = ARODistributorLabel);
        insert group5;
        Assignment_Group_Name__c group6 = new Assignment_Group_Name__c(Name = ARORSMLabel);
        insert group6;
        

        Account acc = new Account(Name = 'Test Account1', RecordTypeId = JDE_AccountRT_ID,ShippingCountry = 'US',ShippingCity = 'Test',ShippingState = 'Test',ShippingStreet = 'Test',ShippingPostalCode = '12345');
        insert acc;
        //MPOB Account Insert with GD Parent RT
        Account acc1 = new Account(Name = 'Test Account2', RecordTypeId = GD_Parent_AccountRT_ID,ShippingCountry = 'US',ShippingCity = 'TestCity',ShippingState = 'TestState',ShippingStreet = 'TestStreet',ShippingPostalCode = '123456');
        insert acc1;
        //ARO Account insert with ARO record Type
        Account accARO = new Account(Name = 'Test Account3', RecordTypeId = ARO_AccountRT_ID, ShippingCountry = 'US',ShippingCity = 'Tst1',ShippingState = 'TstARO',ShippingStreet = 'Test',ShippingPostalCode = '67890');
        insert accARO;

        Assignment_Groups__c assGrp = new Assignment_Groups__c(Group_Name__c = group1.Id,Country__c = 'US',Brand__c = 'Albin Pump',Distributor_Name__c = acc.Id);
        insert assGrp;
         Assignment_Groups__c assGrp2 = new Assignment_Groups__c(Group_Name__c = group2.Id,Country__c = 'US',Brand__c = 'Albin Pump',Distributor_Name__c = acc.Id);
        insert assGrp2;
        //MPOB Assignment Group Record Insert
        Assignment_Groups__c assGrp3 = new Assignment_Groups__c(Group_Name__c = group3.Id,Country__c = 'US',Brand__c = 'Albin Pump',Distributor_Name__c = acc1.Id);
        insert assGrp3;
        Assignment_Groups__c assGrp4 = new Assignment_Groups__c(Group_Name__c = group4.Id,Country__c = 'US',Brand__c = 'Albin Pump',Distributor_Name__c = acc1.Id);
        insert assGrp4;
        //ARO Assignment Group Records insert
        Assignment_Groups__c assGrp5 = new Assignment_Groups__c(Group_Name__c = group5.Id,Country__c = 'US',Brand__c = 'ARO',Distributor_Name__c = accARO.Id);
        insert assGrp5;
        Assignment_Groups__c assGrp6 = new Assignment_Groups__c(Group_Name__c = group6.Id,Country__c = 'US',Brand__c = 'ARO',Distributor_Name__c = accARO.Id);
        insert assGrp6;


      	String DistributorAccountName = 'Tes';
        Test.startTest(); 
        List<Assignment_Groups__c> result = MRAssignmentGroupExtract.getChannelPartnerInformation(MRDistributorLabel, DistributorAccountName);
        List<Assignment_Groups__c> result1 = MRAssignmentGroupExtract.getChannelPartnerInformation(MRRSMLabel, DistributorAccountName);
		//MPOB Criteria Test
		List<Assignment_Groups__c> result2 = MRAssignmentGroupExtract.getChannelPartnerInformation(RSMLabelMPOB, DistributorAccountName);
        List<Assignment_Groups__c> result3 = MRAssignmentGroupExtract.getChannelPartnerInformation(DistributorLabelMPOB, DistributorAccountName);

        //ARO Test Case
        List<Assignment_Groups__c> result4 = MRAssignmentGroupExtract.getChannelPartnerInformation(ARODistributorLabel, DistributorAccountName);
        List<Assignment_Groups__c> result5 = MRAssignmentGroupExtract.getChannelPartnerInformation(ARORSMLabel, DistributorAccountName);

        Test.stopTest(); 
    }
}