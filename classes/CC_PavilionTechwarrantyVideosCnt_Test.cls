@isTest
public class CC_PavilionTechwarrantyVideosCnt_Test 
{
      static testMethod void testPavilionTechwarrantyVideosCnt()
    {
        List<ContentVersion> testContentVersion=[select id, ContentDocumentId,Video_Category__c, ContentSize, ContentUrl, Description,Title, CC_Chapter__c, CC_Type1__c,
                                                 CC_Market__c, CC_Power_Source__c,FileType, 
                                                 RecordType.Name from ContentVersion 
                                                 where RecordType.Name='Training Support Videos' ];
        CC_PavilionTemplateController Controller;
        /* Creating an instance of the class CC_PavilionTechwarrantyVideosCnt */
        CC_PavilionTechwarrantyVideosCnt videosCnt = new CC_PavilionTechwarrantyVideosCnt(Controller);
        test.starttest();
        CC_PavilionTechwarrantyVideosCnt.getVideoData('test');
        /* Verify that method returns list of ContentVersion records*/
        System.assertEquals(testContentVersion,CC_PavilionTechwarrantyVideosCnt.getVideoData('test'));
        CC_PavilionTechwarrantyVideosCnt.getChapter();
        System.assertNotEquals(null,CC_PavilionTechwarrantyVideosCnt.getChapter());
        CC_PavilionTechwarrantyVideosCnt.getType();
        System.assertNotEquals(null,CC_PavilionTechwarrantyVideosCnt.getType());
        CC_PavilionTechwarrantyVideosCnt.getMarket();
        System.assertNotEquals(null,CC_PavilionTechwarrantyVideosCnt.getMarket());
        CC_PavilionTechwarrantyVideosCnt.getPowerSource();
        System.assertNotEquals(null,CC_PavilionTechwarrantyVideosCnt.getPowerSource());
        test.stopTest();
    }
    
}