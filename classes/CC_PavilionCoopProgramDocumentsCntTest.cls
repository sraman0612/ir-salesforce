@isTest
public class CC_PavilionCoopProgramDocumentsCntTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    
  static testMethod void UnitTest_CoopProgramDocuments(){ 
  
  
        Account acc = TestUtilityClass.createAccountWithRegionAndCurrency('USD','Japan');
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c=TestUtilityClass.createContact(acc.id);
      	c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        insert c;
        System.assertEquals(c.AccountId,acc.Id);
        System.assertNotEquals(null, c.Id);
     
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        Pavilion_Navigation_Profile__c pnp = TestUtilityClass.createPavilionNavigationProfile('US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN');
        insert pnp;
        System.assertEquals(u.ContactId,c.Id);
        System.assertNotEquals(null, u.Id);
        System.runAs(u)
    {
    Test.startTest();
    CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
    controller.acctId = acc.Id;
    
    CC_PavilionCoopProgramDocumentsCnt CoopPrgDoc = new CC_PavilionCoopProgramDocumentsCnt(controller);
    Test.stopTest();
    }
  }
}