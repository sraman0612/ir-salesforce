Public with sharing class CC_PavilionTemplateController {
    
    public String viewifNoConcopy { get; set; }
    
    public String viewifYesConcopy { get; set; }
    
    public String SalesRep { get; set; }
    
    public String ImageDataFilter { get; set; }
    
    public String zipLogoURL { get; set; }
    
    /* member variables */
    public String acctId {get;set;}
    public String shipClassification{get;set;}
    Public String AccountName {get;set;}
    public String CustomerNumber {get;set;}
    public String globalRegion {get;set;}
    public String bulletinCurrency {get;set;}
    public String ConnectivityCurrency {get;set;}
    public String pricebook {get;set;}
    public String acctType {get;set;}
    public Pavilion_Navigation_Profile__c navProfile {get;set;}
    Public String imageLibCurrency {get;set;}
    public Boolean viewPartsBulletins {get;set;}
    public Boolean viewSalesBulletins {get;set;}
    public Boolean viewSserviceBulletins {get;set;}
    public Boolean viewSalesPricing {get;set;}
    public Boolean viewPartsPricing {get;set;}
    public Boolean viewPartsOrdersInvoicePricing {get;set;}
    Public Boolean ViewifCopyconYes{get;set;}
    Public Boolean ViewifCopyconNo{get;set;}
    Public Boolean viewSalesforce{get;set;}
    public Boolean isSuspended {get;set;}
    public boolean accessMyTeam {get;set;}
    public boolean accessMyProfile {get;set;}
    public boolean accessSysAdmin {get;set;}
    public boolean accessBulletinHome {get;set;}
    public boolean accessClubCarContacts {get;set;}
    public boolean accessDealerManual {get;set;}
    public boolean accessContractLeadAssignment {get;set;}
    public boolean accessHelp {get;set;}
    public boolean accessLogOut {get;set;}
    public boolean accessPartHome {get;set;}
    public boolean accessNewPartsOrder {get;set;}
    public boolean accessPartsOrderHistory {get;set;}
    public boolean accessPartCart {get;set;}
    public boolean accessAfterMarketCatalog {get;set;}
    public boolean accessSpeedCodes {get;set;}
    public boolean accessCarsHome {get;set;}
    public boolean accessOrderHistory {get;set;}
    public boolean accessNationalOrderHistory {get;set;}
    public boolean accessiStore {get;set;}
    public boolean accessBlueBook {get;set;}
    public boolean accessCustomerVIEW {get;set;}
    public boolean accessCCInsight {get;set;}
    public boolean accessCustomSolutions {get;set;}
    public boolean accessInventory {get;set;}
    public boolean accessSerialNumPrefixes {get;set;}
    public boolean accessTransportationRequest {get;set;}
    public boolean accessViewTransportationRequest{get;set;}
    public boolean accessMarketingHome {get;set;}
    public boolean accessMerchandising {get;set;}
    public boolean accessBoothRental {get;set;}
    public boolean accessDealerSignage {get;set;}
    public boolean accessTheMarketPlace {get;set;}
    public boolean accessUniformOrdering {get;set;}
    public boolean accessCoOpHome {get;set;}
    public boolean accessMyCoOpAccount {get;set;}
    public boolean accessPreApprovalRequest {get;set;}
    public boolean accessProgramDocuments {get;set;}
    public boolean accessHoleInOne {get;set;}
    public boolean accessClubCarConnect {get;set;}
    public boolean accessEvents {get;set;}
    public boolean accessMarketingContacts {get;set;}
    public boolean accessResourceLibrary {get;set;}
    public boolean accessMarketingIdeas {get;set;}
    public boolean accessLiterature {get;set;}
    public boolean accessPromotionsForms {get;set;}
    public boolean accessVideos {get;set;}
    public boolean accessSmartLead {get;set;}
    public boolean accessCreditCenterHome {get;set;}
    public boolean accessAccountInformation {get;set;}
    public boolean accessDealerInvoicing {get;set;}
    public boolean accessCreditApplication {get;set;}
    public boolean accessFinancePrograms {get;set;}
    public boolean accessFinanceContactList {get;set;}
    public boolean accessAccountInvoiceHistory {get;set;}
    public boolean accessTermsAndConditions {get;set;}
    public boolean accessTechWarrantyHome {get;set;}
    public boolean accessMSDSDocuments {get;set;}
    public boolean accessTechnicalPublications {get;set;}
    public boolean accessTavant {get;set;}
    public boolean accessCustomerComplaint {get;set;}
    public boolean accessSafetyCampaigns {get;set;}
    public boolean accessTrainingSuppHome {get;set;}
    public boolean accessReporting {get;set;}
    public boolean accessSalesTraining {get;set;}
    public boolean accessTrainingCourseCatalog {get;set;}
    public boolean accessTrainingVideos {get;set;}
    public boolean accessPricing {get;set;}
    private static final String SUSPENDED = 'SUSPENDED';
    private static final String TERMINATED = 'TERMINATED';
    private static final String Distributor = 'Distributor';
    private static final String Region_EMEA = 'EMEA';
    private static final String Region_IRESA = 'IRESA';
    private static final String Region_US = 'United States';
    private static final String Region_Canada = 'Canada';
    private static final String cc_pavilionhome = 'cc_pavilionhome';
    private static final String Home_Page_Tile = 'Home Page Tile';  
    private static User userFields = null;
    /* Code changes made by Ashutosh - for Coonectivity Page */
    public boolean accessConnectivity {get;set;}
    public Boolean viewPartsConnectivity {get;set;}
    public Boolean viewSalesConnectivity {get;set;}
    public Boolean viewSserviceConnectivity {get;set;}
    /* Lazy-loaded, cached user query */
    Public static User getUserFields() {
        if (null == userFields) {
            userFields = [SELECT id, name, CC_Pavilion_Navigation_Profile__c,CC_View_Parts_Bulletins__c,CC_View_Sales_Bulletins__c,CC_View_Service_Bulletins__c,
                          CC_View_Sales_Pricing__c,CC_View_Parts_Order_Invoice_Pricing__c,CC_View_Parts_Pricing__c,AccountId,CC_Sales__c,
                          FirstName, LastName, FederationIdentifier, Email
                          FROM User 
                          WHERE id = :userinfo.getUserId() LIMIT 1];
        }
        return userFields;    
    }
    
    /* constructor */
    Public CC_PavilionTemplateController() {
        User userDetails = CC_PavilionTemplateController.getUserFields();
        viewPartsBulletins=userDetails.CC_View_Parts_Bulletins__c;
        viewSalesBulletins=userDetails.CC_View_Sales_Bulletins__c;
        viewSserviceBulletins=userDetails.CC_View_Service_Bulletins__c;
        viewSalesPricing=userDetails.CC_View_Sales_Pricing__c;
        viewPartsPricing=userDetails.CC_View_Parts_Pricing__c;
        viewPartsOrdersInvoicePricing=userDetails.CC_View_Parts_Order_Invoice_Pricing__c;
        viewSalesforce=userDetails.CC_Sales__c;
        
        Id accId=null==userDetails.AccountId?PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c:userDetails.AccountId;
        acctId=Apexpages.currentPage().getParameters().get('aid');
        acctId=null==acctId?accId:acctId;
        acctId=string.escapeSingleQuotes(acctId);
        Account acc = new Account();
        acc = [SELECT Id,Name,CC_Global_Region__c,Currency__c,CC_Bulletins_Currency__c,CC_Customer_Number__c,Type,CC_Shipping_Classification__c,CC_account_status__c,CC_Price_List__c 
               FROM Account 
               WHERE Id = : acctId LIMIT 1];
        AccountName = acc.Name;
        shipClassification = acc.CC_Shipping_Classification__c;
        CustomerNumber=null==acc.CC_Customer_Number__c?'':acc.CC_Customer_Number__c;
        globalRegion=acc.CC_Global_Region__c;
        bulletinCurrency=acc.CC_Bulletins_Currency__c;
        ConnectivityCurrency= acc.Currency__c;
        imageLibCurrency = acc.Currency__c;
        priceBook=null==acc.CC_Price_List__c?'PLIST':acc.CC_Price_List__c;
        acctType=acc.Type;
        if(acc.CC_account_status__c!=null && (acc.CC_account_status__c.toUpperCase()==SUSPENDED ||acc.CC_account_status__c.toUpperCase()==TERMINATED)){
            isSuspended=true;
        }else{
            isSuspended=false;
        }
        String userNavigationProfile = userDetails.CC_Pavilion_Navigation_Profile__c;
        if (userNavigationProfile != null && userNavigationProfile != '') {
            navProfile=[SELECT id, name, showMyProfile__c, showMyTeam__c, showSysAdmin__c, showBulletinHome__c, showClubCarContacts__c, showDealerManual__c, ShowContractLeadAssignment__c, showHelp__c, 
                        showLogOut__c,showPartHome__c, showNewPartsOrder__c, showPartsOrderHistory__c, showPartCart__c, showAfterMarketCatalog__c, showSpeedCodes__c,
                        showCarsHome__c, showOrderHistory__c, showNationalOrderHistory__c, showiStore__c, showBlueBook__c, showCustomerVIEW__c, showCCInsight__c, 
                        showCustomSolutions__c, showInventory__c, showSerialNumPrefixes__c, showTransportationRequest__c,showViewTransportationRequest__c,showMarketingHome__c, showMerchandising__c,
                        showBoothRental__c, showDealerSignage__c, showTheMarketPlace__c, showUniformOrdering__c, showCoOpHome__c, showMyCoOpAccount__c, showPreApprovalRequest__c,
                        showProgramDocuments__c,showHoleInOne__c, showClubCarConnect__c, showEvents__c, showMarketingContacts__c, showResourceLibrary__c, showMarketingIdeas__c,
                        showLiterature__c, showPromotionsForms__c, showVideos__c, showSmartLead__c,showCreditCenterHome__c, showAccountInformation__c, showDealerInvoicing__c, 
                        showCreditApplication__c, showFinancePrograms__c, showFinanceContactList__c, showAccountInvoiceHistory__c, showTermsAndConditions__c,
                        showTechWarrantyHome__c, showMSDSDocuments__c, showTechnicalPublications__c, showTavant__c, showCustomerComplaint__c, showSafetyCampaigns__c,
                        showTrainingSuppHome__c, showReporting__c, showSalesTraining__c, showTrainingCourseCatalog__c, showTrainingVideos__c,Region__c,showConnectivity__c
                        FROM Pavilion_Navigation_Profile__c 
                        WHERE name = : userNavigationProfile LIMIT 1];
                        
            accessMyProfile = navProfile.showMyProfile__c;
            accessMyTeam = navProfile.showMyTeam__c;
            accessSysAdmin = navProfile.showSysAdmin__c;
            accessBulletinHome = navProfile.showBulletinHome__c;
            accessClubCarContacts = navProfile.showClubCarContacts__c;
            accessDealerManual = navProfile.showDealerManual__c;
            accessContractLeadAssignment = navProfile.ShowContractLeadAssignment__c;
            accessHelp = navProfile.showHelp__c;
            accessLogOut = navProfile.showLogOut__c;
            accessPartHome = navProfile.showPartHome__c;
            accessNewPartsOrder = navProfile.showNewPartsOrder__c;
            accessPartsOrderHistory = navProfile.showPartsOrderHistory__c;
            accessPartCart = navProfile.showPartCart__c;
            accessAfterMarketCatalog = navProfile.showAfterMarketCatalog__c;
            accessSpeedCodes = navProfile.showSpeedCodes__c;
            accessCarsHome = navProfile.showCarsHome__c;
            accessOrderHistory = navProfile.showOrderHistory__c;
            accessNationalOrderHistory = navProfile.showNationalOrderHistory__c;
            accessConnectivity= navProfile.showConnectivity__c;
            if((acc.CC_Global_Region__c  == Region_EMEA || acc.CC_Global_Region__c  == Region_IRESA) && navProfile.showiStore__c){
            	accessiStore = true;
        	}
            else {
            	accessiStore = false;
        	}
            
            if (acc.Type == Distributor && acc.CC_Global_Region__c == Region_US && navProfile.showBlueBook__c){
            	accessBlueBook = true;
        	}
            else{
            	accessBlueBook = false;
        	}
            
            accessCustomerVIEW = navProfile.showCustomerVIEW__c;
            
            if (acc.Type == Distributor && (acc.CC_Global_Region__c == Region_US || acc.CC_Global_Region__c == Region_Canada)){
            	accessCCInsight = true;
        	}
            else{
            	accessCCInsight = false;
        	}

        	if ((navProfile.Region__c != null && navProfile.Region__c.contains(Region_EMEA)) || (acc.CC_Global_Region__c == Region_EMEA)){
        		accessPricing = false;
        	}
        	else{
        		accessPricing = true;
        	}
            
            accessCustomSolutions = navProfile.showCustomSolutions__c;
            accessInventory = navProfile.showInventory__c;
            accessSerialNumPrefixes = navProfile.showSerialNumPrefixes__c;
            accessTransportationRequest = navProfile.showTransportationRequest__c;
            accessViewTransportationRequest=navProfile.showViewTransportationRequest__c;
            accessMarketingHome = navProfile.showMarketingHome__c;
            accessMerchandising = navProfile.showMerchandising__c;
            accessBoothRental = navProfile.showBoothRental__c;
            accessDealerSignage = navProfile.showDealerSignage__c;
            accessTheMarketPlace = navProfile.showTheMarketPlace__c;
            accessUniformOrdering = navProfile.showUniformOrdering__c;
            accessCoOpHome = navProfile.showCoOpHome__c; 
            accessMyCoOpAccount = navProfile.showMyCoOpAccount__c;
            accessPreApprovalRequest = navProfile.showPreApprovalRequest__c;
            accessProgramDocuments = navProfile.showProgramDocuments__c;
            accessHoleInOne = navProfile.showHoleInOne__c;
            accessClubCarConnect = navProfile.showClubCarConnect__c;
            accessEvents = navProfile.showEvents__c;
            accessMarketingContacts = navProfile.showMarketingContacts__c;
            accessResourceLibrary = navProfile.showResourceLibrary__c;
            accessMarketingIdeas = navProfile.showMarketingIdeas__c; //Changed by @Priyanka Baviskar for ticket #8239217: PLEASE DELETE THE MARKETING IDEAS LIBRARY IN LINKS on 3/7/2019.
            accessLiterature = navProfile.showLiterature__c;
            accessPromotionsForms = navProfile.showPromotionsForms__c;
            accessVideos = navProfile.showVideos__c;
            accessSmartLead = navProfile.showSmartLead__c;
            accessCreditCenterHome = navProfile.showCreditCenterHome__c;
            accessAccountInformation = navProfile.showAccountInformation__c;
            accessDealerInvoicing = navProfile.showDealerInvoicing__c;
            accessCreditApplication = navProfile.showCreditApplication__c;
            accessFinancePrograms = navProfile.showFinancePrograms__c;
            accessFinanceContactList = navProfile.showFinanceContactList__c;
            accessAccountInvoiceHistory = navProfile.showAccountInvoiceHistory__c;
            accessTermsAndConditions = navProfile.showTermsAndConditions__c;
            accessTechWarrantyHome = navProfile.showTechWarrantyHome__c;
            accessMSDSDocuments = navProfile.showMSDSDocuments__c;
            accessTechnicalPublications = navProfile.showTechnicalPublications__c;
            accessTavant = navProfile.showTavant__c;
            accessCustomerComplaint = navProfile.showCustomerComplaint__c;
            accessSafetyCampaigns = navProfile.showSafetyCampaigns__c;
            accessTrainingSuppHome = navProfile.showTrainingSuppHome__c;
            accessReporting = navProfile.showReporting__c;
            accessSalesTraining = navProfile.showSalesTraining__c;
            accessTrainingCourseCatalog = navProfile.showTrainingCourseCatalog__c;
            accessTrainingVideos = navProfile.showTrainingVideos__c;
        }
    }
    
    /* page refs */
    
    
    public PageReference redirectMyTeam() {return null==accessMyTeam?null:accessMyTeam?null:page.CC_PavilionHome;}
    
    public PageReference redirectMyProfile() {return null==accessMyProfile?null:accessMyProfile?null:page.CC_PavilionHome;}
    
    public PageReference redirectSysAdmin() {return null==accessSysAdmin?null:accessSysAdmin?null:page.CC_PavilionHome;}
    
    public PageReference redirectBulletins() {return null==accessBulletinHome?null:accessBulletinHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectClubCarContacts() {return null==accessClubCarContacts?null:accessClubCarContacts?null:page.CC_PavilionHome;}
    
    public PageReference redirectDealerManual() {return null==accessDealerManual?null:accessDealerManual?null:page.CC_PavilionHome;}
    
    public PageReference redirectContractLeadAssignment() {return null==accessContractLeadAssignment?null:accessContractLeadAssignment?null:page.CC_PavilionHome;}
    
    public PageReference redirectHelp() {return null==accessHelp?null:accessHelp?null:page.CC_PavilionHome;}
    
    public PageReference redirectPartHome() {return null==accessPartHome?null:accessPartHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectNewPartsOrder() {return null==accessNewPartsOrder?null:accessNewPartsOrder?null:page.CC_PavilionHome;}
    
    public PageReference redirectPartsOrderHistory() {return null==accessPartsOrderHistory?null:accessPartsOrderHistory?null:page.CC_PavilionHome;}
    
    public PageReference redirectPartCart() {return null==accessPartCart?null:accessPartCart?null:page.CC_PavilionHome;}
    
    public PageReference redirectAfterMarketCatalog() {return null==accessAfterMarketCatalog?null:accessAfterMarketCatalog?null:page.CC_PavilionHome;}
    
    public PageReference redirectSpeedCodes() {return null==accessSpeedCodes?null:accessSpeedCodes?null:page.CC_PavilionHome;}
    
    public PageReference redirectCarsHome() {return null==accessCarsHome?null:accessCarsHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectOrderHistory() {return null==accessOrderHistory?null:accessOrderHistory?null:page.CC_PavilionHome;}
    
    public PageReference redirectNationalOrderHistory() {return null==accessNationalOrderHistory?null:accessNationalOrderHistory?null:page.CC_PavilionHome;}
    
    public PageReference redirectiStore() {return null==accessiStore?null:accessiStore?null:page.CC_PavilionHome;}
    
    public PageReference redirectBlueBook() {return null==accessBlueBook?null:accessBlueBook?null:page.CC_PavilionHome;}
    
    public PageReference redirectCustomerVIEW() {return null==accessCustomerVIEW?null:accessCustomerVIEW?null:page.CC_PavilionHome;}
    
    public PageReference redirectCustomSolutions() {return null==accessCustomSolutions?null:accessCustomSolutions?null:page.CC_PavilionHome;}
    
    public PageReference redirectInventory() {return null==accessInventory?null:accessInventory?null:page.CC_PavilionHome;}
    
    public PageReference redirectTransportationRequest() {return null==accessTransportationRequest?null:accessTransportationRequest?null:page.CC_PavilionHome;}
    
    //code contributed by @Priyanka Baviskar for issue no 7629587.
    public PageReference redirectViewTransportationRequest() {return null==accessViewTransportationRequest?null:accessViewTransportationRequest?null:page.CC_PavilionHome;}
    
    public PageReference redirectMarketingHome() {return null==accessMarketingHome?null:accessMarketingHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectMerchandising() {return null==accessMerchandising?null:accessMerchandising?null:page.CC_PavilionHome;}
    
    public PageReference redirectBoothRental() {return null==accessBoothRental?null:accessBoothRental?null:page.CC_PavilionHome;}
    
    public PageReference redirectDealerSignage() {return null==accessDealerSignage?null:accessDealerSignage?null:page.CC_PavilionHome;}
    
    public PageReference redirectTheMarketPlace() {return null==accessTheMarketPlace?null:accessTheMarketPlace?null:page.CC_PavilionHome;}
    
    public PageReference redirectUniformOrdering() {return null==accessUniformOrdering?null:accessUniformOrdering?null:page.CC_PavilionHome;}
    
    public PageReference redirectCoOpHome() {return null==accessCoOpHome?null:accessCoOpHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectMyCoOpAccount() {return null==accessMyCoOpAccount?null:accessMyCoOpAccount?null:page.CC_PavilionHome;}
    
    public PageReference redirectPreApprovalRequest() {return null==accessPreApprovalRequest?null:accessPreApprovalRequest?null:page.CC_PavilionHome;}
    
    public PageReference redirectProgramDocuments() {return null==accessProgramDocuments?null:accessProgramDocuments?null:page.CC_PavilionHome;}
    
    public PageReference redirectHoleInOne() {return null==accessHoleInOne?null:accessHoleInOne?null:page.CC_PavilionHome;}
    
    public PageReference redirectClubCarConnect() {return null==accessClubCarConnect?null:accessClubCarConnect?null:page.CC_PavilionHome;}
    
    public PageReference redirectEvents() {return null==accessEvents?null:accessEvents?null:page.CC_PavilionHome;}
    
    public PageReference redirectMarketingContacts() {return null==accessMarketingContacts?null:accessMarketingContacts?null:page.CC_PavilionHome;}
    
    public PageReference redirectResourceLibrary() {return null==accessResourceLibrary?null:accessResourceLibrary?null:page.CC_PavilionHome;}
    
    public PageReference redirectMarketingIdeas() {return null==accessMarketingIdeas?null:accessMarketingIdeas?null:page.CC_PavilionHome;}
    
    public PageReference redirectLiterature() {return null==accessLiterature?null:accessLiterature?null:page.CC_PavilionHome;}
    
    public PageReference redirectPromotionsForms() {return null==accessPromotionsForms?null:accessPromotionsForms?null:page.CC_PavilionHome;}
    
    public PageReference redirectVideos() {return null==accessVideos?null:accessVideos?null:page.CC_PavilionHome;}
    
    public PageReference redirectSmartLead() {return null==accessSmartLead?null:accessSmartLead?null:page.CC_PavilionHome;}
    
    public PageReference redirectCreditCenterHome() {return null==accessCreditCenterHome?null:accessCreditCenterHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectAccountInformation() {return null==accessAccountInformation?null:accessAccountInformation?null:page.CC_PavilionHome;}
    
    public PageReference redirectDealerInvoicing() {return null==accessDealerInvoicing?null:accessDealerInvoicing?null:page.CC_PavilionHome;}
    
    public PageReference redirectCreditApplication() {return null==accessCreditApplication?null:accessCreditApplication?null:page.CC_PavilionHome;}
    
    public PageReference redirectFinancePrograms() {return null==accessFinancePrograms?null:accessFinancePrograms?null:page.CC_PavilionHome;}
    
    public PageReference redirectFinanceContactList() {return null==accessFinanceContactList?null:accessFinanceContactList?null:page.CC_PavilionHome;}
    
    public PageReference redirectAccountInvoiceHistory() {return null==accessAccountInvoiceHistory?null:accessAccountInvoiceHistory?null:page.CC_PavilionHome;}
    
    public PageReference redirectTermsAndConditions() {return null==accessTermsAndConditions?null:accessTermsAndConditions?null:page.CC_PavilionHome;}
    
    public PageReference redirectTechWarrantyHome() {return null==accessTechWarrantyHome?null:accessTechWarrantyHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectMSDSDocuments() {return null==accessMSDSDocuments?null:accessMSDSDocuments?null:page.CC_PavilionHome;}
    
    public PageReference redirectTechnicalPublications() {return null==accessTechnicalPublications?null:accessTechnicalPublications?null:page.CC_PavilionHome;}
    
    public PageReference redirectTavant() {return null==accessTavant?null:accessTavant?null:page.CC_PavilionHome;}
    
    public PageReference redirectCustomerComplaint() {return null==accessCustomerComplaint?null:accessCustomerComplaint?null:page.CC_PavilionHome;}
    
    public PageReference redirectTrainingSuppHome() {return null==accessTrainingSuppHome?null:accessTrainingSuppHome?null:page.CC_PavilionHome;}
    
    public PageReference redirectReporting() {return null==accessReporting?null:accessReporting?null:page.CC_PavilionHome;}
    
    public PageReference redirectSalesTraining() {return null==accessSalesTraining?null:accessSalesTraining?null:page.CC_PavilionHome;}
    
    public PageReference redirectTrainingCourseCatalog() {return null==accessTrainingCourseCatalog?null:accessTrainingCourseCatalog?null:page.CC_PavilionHome;}
    
    public PageReference redirectTrainingVideos() {return null==accessTrainingVideos?null:accessTrainingVideos?null:page.CC_PavilionHome;}
    
    public PageReference redirectConnectivity() {return null==accessConnectivity?null:accessConnectivity?null:page.CC_PavilionHome;}
    @RemoteAction
    Public static string getData(string currenturl) {
        string tilesMap;
        /*Get all tiles in to Map from content version*/
        map < string, CC_Pavilion_Content__c > settingstiles = new map < string, CC_Pavilion_Content__c > ();
        for (CC_Pavilion_Content__c tile: [SELECT id, name, Link__c, showOnPavilionHome__c, Tile_Page__c,Title__c, Type__c FROM CC_Pavilion_Content__c WHERE Type__c = :Home_Page_Tile]) {
            if (tile.Tile_Page__c != null){settingstiles.put(tile.Tile_Page__c, tile);}
        }
        User Userrecord = [SELECT id, name, CC_Pavilion_LastVisited__c from User WHERE id = : userinfo.getuserid()];
        /*For pages that are not home*/
        if (currenturl.toLowerCase() != cc_pavilionhome) {
            for (string s: settingstiles.keyset()) {
                if (currenturl.toLowerCase() == s.toLowerCase()) {
                    Userrecord.CC_Pavilion_LastVisited__c = settingstiles.get(s).Title__c + ';' + currenturl;
                    tilesMap = currenturl;
                }
            }
            //if (!test.isrunningtest()) {
            if (Userrecord.Id != null){Update Userrecord;}
            // }
        }
        if (tilesMap != null){return tilesMap;}
        else{return '';}
    }
    
    
}