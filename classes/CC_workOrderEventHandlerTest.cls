@isTest
public class CC_workOrderEventHandlerTest {
  @testSetup static void setupData() {
    List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
    psettingList = TestUtilityClass.createPavilionSettings();        
    insert psettingList;
    IR_API_Bypass__c iab = IR_API_Bypass__c.getInstance(userinfo.getUserId());
    iab.CC_FS_Edit_Synced_WorkOrder__c = TRUE;
    insert iab;
    Product2 p = TestUtilityClass.createInboundProduct2();
    insert p;
    Product2 pNonInv = TestUtilityClass.createInboundProduct2();
    pNonInv.ERP_Item_Number__c='ADAM12';
    pNonInv.ProductCode='ADAM12';
    pNonInv.CC_Non_Inventory__c=TRUE;
    insert pNonInv;
    Pricebook2 pb = TestUtilityClass.createPriceBook2();
    insert pb;
    PricebookEntry pbeCustom = TestUtilityClass.createPriceBookEntry(pb.Id,p.Id);
    insert pbeCustom;
    Account a = TestUtilityClass.createAccount();
    a.ShippingCountry='USA';
    a.CC_Price_List__c='PBook';
    a.CC_Customer_Number__c ='9999920';
    a.Name='CLUB CAR DEPT EXP US';
    insert a;
    CC_Asset__c car = new CC_Asset__c(Account__c=a.Id,Serial__c='FIVEFIVEFIVE8675309');
    insert car;
    CCProcessBuilderIDs__c hcs = CCProcessBuilderIDs__c.getOrgDefaults();
    hcs.FS_No_Charge_Acct_Id__c=a.Id;
    hcs.FS_No_Charge_Pricebook_ID__c=pb.Id;
    upsert hcs CCProcessBuilderIDs__c.Id;
  }
  
  static testmethod void billableUnitTest(){
    Id aid = [SELECT Id FROM Account WHERE Name='CLUB CAR DEPT EXP US' LIMIT 1].Id;
    WorkOrder wo = TestUtilityClass.createWorkOrder(aid,'CC_Service');
    wo.subject='Test';
    wo.SLAM_Report__c='Completed';
    wo.Work_Order_status__c='Work Completed';
    insert wo;
    wo.Pricebook2Id = [SELECT Id FROM Pricebook2 WHERE Name='PBook'].Id;
    update wo;
    WorkOrderLineItem woli =  TestUtilityClass.createWorkOrderLineItem(wo.Id,1,1,[select Id from PricebookEntry where PriceBook2.Name='PBook' LIMIT 1].Id);
    insert woli;
    CC_FS_Labor__c l = TestUtilityClass.createCCFSLabor(wo.Id);
    insert l;
    l.Labor_Cost__c=2;
    update l;
    CC_FS_Non_Inventory_Line_Item__c nili = TestUtilityClass.createCCFSNonInvLineItem(wo.Id, 7,77,[SELECT Id FROM Product2 WHERE CC_Non_Inventory__c = TRUE LIMIT 1 ].Id);
    insert nili;
    Id [] idLst = new List<Id>();
    idLst.add(wo.Id);
    test.startTest();
    CC_workOrderEventHandler.sync2MAPICS(idLst);
    test.stopTest();
  }
  
  static testmethod void nochargeUnitTest(){
    Id aid = [SELECT Id FROM Account WHERE Name='CLUB CAR DEPT EXP US' LIMIT 1].Id;
    WorkOrder wo = TestUtilityClass.createWorkOrder(aid,'CC_Service_Contract');
    wo.Work_Order_status__c='Work Completed';
    wo.SLAM_Report__c='Completed';
    wo.Subject='Test';
    insert wo;
    wo.Pricebook2Id = [SELECT Id FROM Pricebook2 WHERE Name='PBook'].Id;
    update wo;
    WorkOrderLineItem woli =  TestUtilityClass.createWorkOrderLineItem(wo.Id,1,1,[select Id from PricebookEntry where PriceBook2.Name='PBook' LIMIT 1].Id);
    insert woli;
    CC_FS_Labor__c l = TestUtilityClass.createCCFSLabor(wo.Id);
    insert l;
    l.Labor_Cost__c=2;
    update l;
    CC_FS_Non_Inventory_Line_Item__c nili = TestUtilityClass.createCCFSNonInvLineItem(wo.Id, 7,77,[SELECT Id FROM Product2 WHERE CC_Non_Inventory__c = TRUE LIMIT 1 ].Id);
    insert nili;
    Id [] idLst = new List<Id>();
    idLst.add(wo.Id);
    test.startTest();
    CC_workOrderEventHandler.sync2MAPICS(idLst);
    test.stopTest();    
  }
  
  static testmethod void addvalueUnitTest(){
    Id aid = [SELECT Id FROM Account WHERE Name='CLUB CAR DEPT EXP US' LIMIT 1].Id;
    WorkOrder wo = TestUtilityClass.createWorkOrder(aid,'CC_Add_Value');
    wo.Subject='Test';
    wo.SLAM_Report__c='Completed';
    wo.Work_Order_status__c='Work Completed';
    insert wo;
    wo.Pricebook2Id = [SELECT Id FROM Pricebook2 WHERE Name='PBook'].Id;
    update wo;
    WorkOrderLineItem woli =  TestUtilityClass.createWorkOrderLineItem(wo.Id,1,1,[select Id from PricebookEntry where PriceBook2.Name='PBook' LIMIT 1].Id);
    insert woli;
    CC_FS_Labor__c l = TestUtilityClass.createCCFSLabor(wo.Id);
    insert l;
    l.Labor_Cost__c=2;
    update l;
    CC_FS_Non_Inventory_Line_Item__c nili = TestUtilityClass.createCCFSNonInvLineItem(wo.Id, 7,77,[SELECT Id FROM Product2 WHERE CC_Non_Inventory__c = TRUE LIMIT 1 ].Id);
    insert nili;
    CC_FS_Asset__c a = TestUtilityClass.createCCFSAsset(wo.Id,[SELECT Id FROM CC_Asset__c LIMIT 1].Id);
    insert a;
    Id [] idLst = new List<Id>();
    idLst.add(wo.Id);
    test.startTest();
    CC_workOrderEventHandler.sync2MAPICS(idLst);
    test.stopTest();       
  }
}