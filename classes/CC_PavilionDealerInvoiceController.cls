global class CC_PavilionDealerInvoiceController {
    
 public Map<Id,String> salesRepsList{get;set;}

 /* constructor */
 public CC_PavilionDealerInvoiceController(CC_PavilionTemplateController controller) {}
  /* Remote method to submit the invoice data
Remote method to be called from CC_PavilionDealerInvoice page via JS Remoting */
 @RemoteAction
 global static String submitCoopInvoiceData(String invoiceNumber, String customerName, String typeOfCredit,
   String representative, String dateOfService,
    String Comments, String jsonInvoiceItem) {

   try {
   
   List<CC_Sales_Rep__c> salesRepList=new List<CC_Sales_Rep__c>(); 
   List<User> lstUser = [SELECT AccountId,Account.OwnerId,Account.Address_1__c,Account.Name,Account.CC_City__c,Account.CC_State__c,Account.CC_Zip__c,Account.CC_Country__c,ContactId, 
                          Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry,
                          Account.ShippingStreet,Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,Account.ShippingCountry
                          FROM User WHERE id = : UserInfo.getUserId() LIMIT 1 ];
    RecordType orderrt = [select id, Name from RecordType where SobjectType = 'CC_Order__c' and Name = 'Dealer Invoicing' Limit 1 ];
    RecordType orderitemrt = [select id, Name from RecordType where SobjectType = 'CC_Order_Item__c' and Name = 'Dealer Invoicing' Limit 1 ];
    salesRepList=[select id,Name from CC_Sales_Rep__c where CC_Type__c = 'Sales' AND CC_Obsolete__c = FALSE AND CC_Sales_Rep__c = : (Id)representative];
    
    Id AccId = lstUser[0].AccountId;
    CC_Order__c objordr = new CC_Order__c();
    objordr.isSystemCreated__c=TRUE;
    objordr.RecordTypeId = orderrt.id;
    objordr.CC_Customer_Invoice_Number__c = invoiceNumber;
    objordr.CC_Type_Of_Credit_Requested__c = typeOfCredit;
    objordr.CC_Account__c=AccId;
    objordr.CC_Addressee_name__c=lstUser[0].Account.Name;
    objordr.CC_Address_line_1__c=lstUser[0].Account.Address_1__c;
    objordr.CC_City__c=lstUser[0].Account.CC_City__c;
    objordr.CC_States__c=lstUser[0].Account.CC_State__c;
    objordr.CC_Postal_code__c=lstUser[0].Account.CC_Zip__c;
    objordr.CC_Country__c=lstUser[0].Account.CC_Country__c;
    objordr.CC_Request_Date__c=system.today()+1;
    objordr.CC_Warehouse__c='1';
    objordr.CC_Credit_Memo_Code__c='A';
    objordr.CC_Status__c = 'New';
    objordr.CC_Credit_debit_reason_code__c='D/I';
    objordr.PO__c = invoiceNumber;
    objordr.CC_User_Assigned_to__c = (Id)representative;
    objordr.CC_Customer_Name_1__c=customerName;
    //Sales Rep Assignment
    if(salesRepList.size()==1 && salesRepList.size()>0){
        objordr.CC_Sales_Person_Number__c=salesRepList[0].id;
    }
    system.debug('dateOfService===' + dateOfService);
    if(String.isNotBlank(dateOfService)) {
     objordr.CC_Date_Of_Service__c = date.parse(dateOfService);
    }
    system.debug('testDateserivce===' + objordr.CC_Date_Of_Service__c);
    objordr.CC_Comments__c = Comments;
    
    try {
        System.debug('Oder CReadted with id before insert'+objordr.id);
        insert objordr;    
        System.debug('Oder CReadted with id'+objordr.id);
    } catch(Exception e) {
        system.debug(e);
    }
    
    //fetch order Total Invoice Amount
    Double totalInvAmt = 0.0;
    
    //Order Items line item
    if (String.isNotEmpty(jsonInvoiceItem)) {
     List < CC_Order_Item__c > lstInvoiceItem = new List < CC_Order_Item__c > ();
     CC_Order_Item__c invoiceItem;
     InvoiceItemsParser parser = parse(jsonInvoiceItem);
     if (parser != null) {
      for (InvoiceItems item: parser.invoiceItems) {
       invoiceItem = new CC_Order_Item__c();
       invoiceItem.recordTypeId = orderitemrt.Id;
       invoiceItem.Order__c = objordr.id;
       invoiceItem.CC_Quantity__c = item.quantity*1;
       invoiceItem.CC_Description__c = item.chargeCode;
       invoiceItem.CC_UnitPrice__c = item.cost;
       invoiceItem.CC_TotalPrice__c = item.totalcost;
       totalInvAmt += item.totalcost;
       lstInvoiceItem.add(invoiceItem);
      }
      
      system.debug('print Invoice Item list '+lstInvoiceItem);
      if (lstInvoiceItem.size() > 0) {
          try {
            system.debug('print Invoice Item'+lstInvoiceItem);
            insert lstInvoiceItem;    
          } catch(Exception e) {
              system.debug(e);
          }
      }
     }
    }
    return 'Success' + objordr.id;
   } catch (Exception e) {
    return 'Failure';
   } 
  }
  /* Method to get list of typeofcredit picklistvalues  */
 public List < String > getTypeOfCredit() {
   List < String > options = new List < String > ();
   for (Schema.PicklistEntry f: CC_Order__c.CC_Type_Of_Credit_Requested__c.getDescribe().getPicklistValues()) {
    options.add(f.getValue());
   }
   return options;
  }
  /* Method to  list of displayed name*/
   public List < User > getSalesRepresentative() {
     List < User > options = new List < User > ();
     List<Id> userIds = new List<Id>();
     Set <Id> userset = new  Set <Id>();
   try{
   User   lstUser = [SELECT AccountId, Account.OwnerId ,ContactId FROM User WHERE id = : UserInfo.getUserId() LIMIT 1 ][0];
   String accountId = lstUser.AccountId;
   userset.add(lstUser.Account.OwnerId);
   for(AccountTeamMember id1 : [Select   UserId  From AccountTeamMember where AccountID = :accountId AND User.Profile.Name != 'CC_CommunityLoginUser' AND User.Profile.Name != 'CC_PavilionCommunityUser' ]){
        userIds.add(id1.UserId);
   }
   userset.addAll(userIds);
   options = [select Id, name from User where id = :userset];
   }catch(Exception e){
    System.debug(e.getStackTraceString()+e);
    return new List<User>();
   }
   return options;
  }
  
  // Wrapper Classes and methods for parse list of Invoice Line Item from json array
 public class InvoiceItemsParser {
  public List < InvoiceItems > invoiceItems;
 }
 public class InvoiceItems {
  public Integer quantity;
  public String chargeCode;
  public Decimal cost;
  public Decimal totalcost;
 }
 private static InvoiceItemsParser parse(String json) {
  return (InvoiceItemsParser) System.JSON.deserialize(json, InvoiceItemsParser.class);
 }
}