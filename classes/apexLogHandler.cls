public class apexLogHandler { 

  public class For_Testing_Force_Exception extends Exception {}
  public Apex_Log__c logObj {get;set;}
  
  /* Constructor */
  public apexLogHandler(String classOrTriggerName, String methodName, String msg) {
    logObj = new Apex_Log__c();
    logObj.class_name__c = classOrTriggerName ;
    logObj.method_name__c = methodName; 
    logObj.Message__c = msg;
    startLogTimes();  
  }

  /* helper methods */
  public void logMessage(String msg) {
    logObj.Message__c = msg;
  }
  
  public void logException(Exception ex) {
    try {
      logObj.exception_cause__c = '' + ex.getCause();
      logObj.message__c += ex.getMessage();
      logObj.exception_type__c = ex.getTypeName();
      logObj.exception_line_Number__c = string.valueOf(ex.getLineNumber());
      logObj.exception_stack_Trace_String__c = ex.getStackTraceString();
    } catch (Exception ex2) {
      System.debug('\n\n### ApexAuditLog is swallowing exception on logException(): ' + ex2);
      // An Exception while logging an exception is uglier and more confusing than swallowing the exception
      //consider sendEmail() here
    }
  }

  public void logRequestInfo(String requestInfo) {
    if (logObj.request_info__c == null) logObj.request_info__c = '_';
    logObj.request_info__c += requestInfo + '\n';
    if((logObj.request_info__c).length()>1 && (logObj.request_info__c).indexof('_')==0){
      logobj.request_info__c = (logobj.request_info__c).substring(1);
    }
  }
  
  public void logResponseInfo(String responseInfo) {
    if (logObj.response_info__c == null)
      logObj.response_info__c = '_';
    logObj.response_info__c += responseInfo + '\n';
    if((logObj.response_info__c).length()>1 && (logObj.response_info__c).indexof('_')==0){
      logobj.response_info__c = (logobj.response_info__c).substring(1);
    }
  }

  public void logStatusCode(Integer sc){
    logObj.Status_Code__c = sc;
  }

  private void startLogTimes() {
    logObj.start_dtm__c = System.now();
    logObj.start_ms__c  = System.currentTimeMillis();
  }
 
  private void stopLogTimes() {
    logObj.stop_dtm__c = System.now();
    logObj.stop_ms__c  = System.currentTimeMillis();
  }
   
  public void addOrder (Id orderId) {
    //Commented as part of EMEIA Cleanup
    //logObj.CC_Order__c = orderId;
  } 
  
  /*public void addWorkOrder (Id workOrderId) {
    logObj.Work_Order__c = workOrderId;
  } */
    
  public void saveLog() {
    stopLogTimes();
    if (logObj.exception_type__c == 'ApexAuditLog.For_Testing_Force_Exception') {
      throw new For_Testing_Force_Exception('For testing: all exceptions on logging cannot stop txn & must be caught');
    }      
    try { 
      // Never throw a governor exception while trying to save log records.
      if ((Limits.getLimitDMLStatements() - Limits.getDMLStatements()) > 0 && (Limits.getLimitDMLRows() - Limits.GetDMLRows()) > 0) {
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.allowFieldTruncation = true;
        Database.SaveResult sr = Database.insert(logObj,dml);
        for(Database.Error err : sr.getErrors()) {
          System.debug('\n\n### ApexAuditLog is erroring out on saveLog():');
          system.debug('### SAVE RES GET ERR ' + sr.getErrors());
          system.debug('### SAVE RES GET STAT ' + err.getStatusCode());
          system.debug('### SAVE RES GET MSG ' + err.getMessage());
          system.debug('### SAVE RES GET FLDS ' + err.getFields());
        }
      }
    } catch (Exception ex) {
      System.debug('\n\n### ApexAuditLog is swallowing exception on saveLog(): ' + ex);
      System.debug('\n logObj = ' + logObj);
      // Never leave an exception on logging uncaught. Recommended: sendEmail()...
    }
  }
}