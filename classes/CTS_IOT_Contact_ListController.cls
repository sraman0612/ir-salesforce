public with sharing class CTS_IOT_Contact_ListController {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse ccr;
        @AuraEnabled public Account[] sites;
        @AuraEnabled public Boolean isSuperUser = CTS_IOT_Data_Service_WithoutSharing.getIsSuperUser(UserInfo.getUserId());
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public CTS_IOT_Community_Administration__c userSettings = CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId());
        @AuraEnabled public Integer totalRecordCount = 0;
    }

    @AuraEnabled
    public static InitResponse getInit(String searchTerm, Id siteId, Integer pageSize, Integer offSet, Integer totalRecordCount){

        InitResponse response = new InitResponse();

        try{

            if (pageSize == null){
                pageSize = response.userSettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.userSettings.User_Selected_List_Page_Size__c) : (response.communitySettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.communitySettings.User_Selected_List_Page_Size__c) : null);
            }

            response.ccr = CTS_IOT_Data_Service_WithoutSharing.getContacts(searchTerm, siteId, pageSize, offSet);
            response.sites = CTS_IOT_Data_Service.getCommunityUserSites(null);
            response.totalRecordCount = CTS_IOT_Data_Service_WithoutSharing.getContactsCount(searchTerm, siteId);

            if (pageSize != null && (response.userSettings.User_Selected_List_Page_Size__c == null || pageSize != response.userSettings.User_Selected_List_Page_Size__c)){
                response.userSettings.User_Selected_List_Page_Size__c = pageSize;
                upsert response.userSettings;
            }            
        }
        catch(Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();            
        }

        return response;
    }

    public class CreateUserRequest{
        public NewUser newUser;
        public Id siteId;
        public List<Id> additionalSiteIds = new List<Id>();
    }

    public class NewUser{
        public Boolean IsSuperUser = false;
        public Boolean IsHybridStandardUser = false;
        public String FirstName;
        public String LastName;
        public String Email;
        public String Phone;
        public String Title;
        public Boolean Terms = false;
    }

    @AuraEnabled
    public static LightningResponseBase createUser(String requestStr){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{
                                    
            CreateUserRequest request = (CreateUserRequest)JSON.deserialize(requestStr, CreateUserRequest.class);

            Contact[] contactCheck = [Select Id, Name, FirstName, LastName, Email, AccountId From Contact Where Email = :request.newUser.Email];

            if (contactCheck.size() > 0){

                response.success = false;
                response.errorMessage = 'This user already exists in the system.  Please select their name from the user drop down list and update accordingly.  User found: "' + contactCheck[0].Name + '"';
            }
            else{

                CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest regRequest = new CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest();
                regRequest.firstName = request.newUser.FirstName;
                regRequest.lastName = request.newUser.LastName;
                regRequest.email = request.newUser.Email;
                regRequest.username = request.newUser.Email + (CTS_IOT_Data_Service_WithoutSharing.getIsSandbox() ? '.sandbox.' : '.') + String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                regRequest.siteId = request.siteId;
                regRequest.superUser = request.newUser.IsSuperUser;
                regRequest.phone = request.newUser.Phone;
                regRequest.title = request.newUser.Title;
                regRequest.sendEmailConfirmation = true;
                regRequest.hybridStandardUser = request.newUser.IsHybridStandardUser;

                CTS_IOT_Data_Service_WithoutSharing.RegisterUserResponse regResponse = CTS_IOT_Data_Service_WithoutSharing.registerUser(regRequest);
                response.success = regResponse.success;
                response.errorMessage = regResponse.errorMessage;
                system.debug('newUserId: ' + regResponse.newUserId);

                if (response.success){

                    // Assign any additional sites requested
                    if (request.additionalSiteIds != null && request.additionalSiteIds.size() > 0){                        
                        CTS_IOT_Data_Service_WithoutSharing.addSitesToUser(regResponse.newUserId, request.additionalSiteIds);
                    }
                }
            }
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }   

    @AuraEnabled
    public static LightningResponseBase activateUser(Id contactId){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{            
            response = CTS_IOT_Contact_EditController.activateUser(contactId);
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }     

    @AuraEnabled
    public static LightningResponseBase deactivateUser(Id contactId){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{            
            response = CTS_IOT_Contact_EditController.deactivateUser(contactId);
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }               
}