/* This Class User for CC_PavilionHelp page*/
Public without sharing Class CC_PavilionHelpcnt
{
    /* member variables */ 
    Public boolean displayquestions {get;set;}
    Public boolean casetable {get;set;}
    Public User userDetails {get;set;}
    Public string ReasonforCase { get;set; }
    Public string description{ get;set; }
    Public List<String> casecomponentLst{get;set;}
   // public static Attachment helpAttachment{get;set;}
    
    /* constructor */
    Public CC_PavilionHelpcnt(CC_PavilionTemplateController controller){
        displayquestions =  false;
        casetable = true;
        userDetails = [SELECT AccountId,contact.phone,account.name,contact.name,
                        contact.email,account.AccountNumber 
                        FROM user 
                        WHERE id=:userinfo.getuserid()];
                        
        casecomponentLst = new List<String>();
        getcasecomponent();
    }
     /* ListofSelectOptions For case PavilionReasons*/
    Public void getcasecomponent(){
        Schema.DescribeFieldResult fieldResult = case.CC_PavilionReason_for_Case__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            casecomponentLst.add(f.getValue());
        }       
    }
    /* remote action Method to load the case list */
    @Remoteaction 
    Public static List<case> onloadcaseList(){
        List<Case> CaseList = new List<Case>();
        string accountid='';
        accountId = [SELECT AccountId FROM user WHERE id=:userinfo.getuserid()].AccountId;
        CaseList =[SELECT id,CaseNumber,CC_Pavilion_Answers__c,CC_PavilionReason_for_Case__c,ClosedDate,CreatedDate,Description,Accountid,Type 
                     FROM case 
                    WHERE type  = 'Pavilion Question' and  accountid=:accountId ];
        return CaseList ;
    }
    
    /* void  Method QuestionformPage */
    Public void QuestionformPage(){
        displayquestions =  true;
        casetable = false;
        ReasonforCase ='';
        description ='';
    }
    /* void  Method submit case*/
    Public PageReference submitbutton(){  
        system.debug('@@@@ in submitbutton');
        try {
        String smeid ; 
        Id askQuestionRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Club Car - Ask a Question').getRecordTypeId();
        Case insertnewcase = new case();
        insertnewcase.type  = 'Pavilion Question';
        insertnewcase.description = description;
        insertnewcase.subject = description;
        insertnewcase.Accountid = userDetails.accountid;
        insertnewcase.Contactid = userDetails.contactid;
        insertnewcase.CC_PavilionReason_for_Case__c = ReasonforCase;
        insertnewcase.RecordtypeId = askQuestionRTId;
      
        casetable = true;
        displayquestions =  false;
        map<string,string> catogories = new map<string,string>();
        for(CC_My_Team__c myteam : [SELECT user__c, help_sme_Categories__c 
                                      FROM cc_my_team__c 
                                     WHERE account__c = :userDetails.accountid and 
                                           user__c != null and  
                                           help_sme_Categories__c INCLUDES (:ReasonforCase)]){
            smeid =  myteam.user__c;
        }                       
        
    
        if(smeid==null){
          smeid= null==PavilionSettings__c.getInstance(ReasonforCase+'_HelpQ')?PavilionSettings__c.getInstance('Others_HelpQ').Value__c:PavilionSettings__c.getInstance(ReasonforCase+'_HelpQ').Value__c;
        }
        insertnewcase.ownerid = smeid;
        insert insertnewcase;
        system.debug('@@@@@insertnewcase'+insertnewcase);
        
        if(insertnewcase.id != NULL){
            system.debug('@@@@@@insertnewcase.id'+insertnewcase.id);
            helpAttachment.ParentId = insertnewcase.id ;
            try {
                insert helpAttachment;
            } 
            catch (DMLException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                //return null;
            } 
        }
      }
      catch(Exception e){
          System.debug('Excetion eeee @@'+e.getmessage());
      }
        
      PageReference pr = Page.CC_PavilionHelp;
      pr.setRedirect(true);
      return pr;
    }
    /* void  Method clear case*/
    Public PageReference ClearForm(){
        ReasonforCase ='';
        description ='';
        displayquestions =  false;
        casetable = true;
      
        PageReference pr = Page.CC_PavilionHelp;
        pr.setRedirect(true);
        return pr;
    }
    
    //Getter and Setter method for attachment record
     public static Attachment helpAttachment {
        get {
            if (helpAttachment == null)
                helpAttachment = new Attachment();
            return helpAttachment;
        }
        set;
    } 
}