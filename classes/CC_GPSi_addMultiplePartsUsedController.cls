public class CC_GPSi_addMultiplePartsUsedController {
  
  public CC_GPSi_addMultiplePartsUsedController(ApexPages.StandardController controller){}
  
  @RemoteAction
  public static List<Product2> getParts() {
    List<Product2> Lst = [SELECT Id, ProductCode,Name,CC_GPSi_Most_Used_Order__c
                            FROM Product2 
                           WHERE RecordType.DeveloperName='CC_GPSi'
                        ORDER BY CC_GPSi_Most_Used_Order__c, Name];
    return Lst;
  }
  
  @RemoteAction
  public static String createPartUsed(String repairTicketId, List<String> parts) {
    CC_Part_Used__c [] partUsedLst4Insert= new List<CC_Part_Used__c>();
    String returnStr='OK';
    try{
      for(String s : parts){
        CC_Part_Used__c ccpu = new CC_Part_Used__c(Part_Description__c=s,CC_Repair_Ticket__c=repairTicketId);
        partUsedLst4Insert.add(ccpu);
      }
      if(!partUsedLst4Insert.isEmpty()){
        insert partUsedLst4Insert;
      }
    } catch (exception e){
      returnStr = 'Unable to create Part Used records for this Repair Ticket.' + e.getStackTraceString();
    }
    return returnStr;
  }
}