@isTest
public class CC_PavilionSysAdminCntTest 
{   
    
     
    static testMethod void UntTest_CC_PavilionSysAdminCnt()
    {

        Account acc = TestUtilityClass.createAccount();      
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.id);
        insert con;
        Contact conn = TestUtilityClass.createContact(acc.id);
        insert conn;
        Contact newconn = TestUtilityClass.createContact(acc.id);
        newconn.ReportsToId=conn.Id;
        insert newconn;
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name ='CC_PavilionCommunityUser' Limit 1];
        PavilionSettings__c pavillionCommunityProfileId = new PavilionSettings__c(
            Name = 'PavillionCommunityProfileId',
            Value__c = portalProfile.Id
        );
        insert pavillionCommunityProfileId;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',portalProfile.Id);
        insert newPavnSettings;
         PavilionSettings__c pavilionPublicGroupID = new PavilionSettings__c(
            Name = 'PavilionPublicGroupID',
            Value__c = portalProfile.Id
        );
        insert pavilionPublicGroupID;
        PavilionSettings__c ps= PavilionSettings__c.getValues('MyTeamDefaultQueue');
        id qID = [select id from Group where developername='MyTeamDefaultQueue'].Id;
        PavilionSettings__c myteamdefq = new PavilionSettings__c(
            Name = 'MyTeamDefaultQueue',
            Value__c = qId
        );
        insert myteamdefq;
        User testUser = new User(
            Username = System.now().millisecond() + 'test12345ddmmyy@test.ingersollrand.com',
            ContactId = con.Id,
            CC_Pavilion_Navigation_Profile__c='Sample Pavilion Navigation Profile',
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert testUser;
          Pavilion_Navigation_Profile__c pnp =new Pavilion_Navigation_Profile__c();
          pnp.Name='Sample Pavilion Navigation Profile';
          pnp.Region__c='Asia Pacific; Canada; Latin America; United States';
          pnp.Display_Name__c='SampleAdmin';
          insert pnp;
          
      system.runAs(testUser){
            CC_PavilionTemplateController controller=new CC_PavilionTemplateController();
            controller.acctId=acc.id;
            CC_PavilionSysAdminCnt admn = new CC_PavilionSysAdminCnt(controller);
            CC_PavilionSysAdminCnt.UserListAndPavilionDisplayWrapper usrwrap=new CC_PavilionSysAdminCnt.UserListAndPavilionDisplayWrapper(testUser,pnp.Display_Name__c);
           
       }
  
    }
    
}