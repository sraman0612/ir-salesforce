@isTest
public class AssignmentGroupsTriggerHandlerTest {
    @isTest
    static void queueNameHandlerTest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'RSMUser', Email='RSM@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='RSMuser1@IR.com');
        insert u;
        
        Id JDE_AccountRT_ID = [Select Id, DeveloperName from RecordType 
                             where DeveloperName = 'JDE_Account' 
                             and SobjectType='Account' limit 1].Id;
        
       Id GD_Parent_AccountRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'GD_Parent' 
                                    and SobjectType='Account' limit 1].Id;
        
        Group queue1 = new Group(Name = 'Milton Roy Test Queue',DeveloperName = 'Milton_Roy_Test_Queue', Type = 'Queue');
        insert queue1;
        Assignment_Group_Name__c group1 = new Assignment_Group_Name__c(Name = 'Distributor Territory');
        insert group1;
        Assignment_Group_Name__c group2 = new Assignment_Group_Name__c(Name = 'Special First Assignment');
        insert group2;
        
        //MPOB Assignment Group Names added
        Assignment_Group_Name__c group3 = new Assignment_Group_Name__c(Name = 'MP/OB/IRP RSM');
        insert group3;
        Assignment_Group_Name__c group4 = new Assignment_Group_Name__c(Name = 'MP/OB/IRP Distributor');
        insert group4;
        Assignment_Group_Name__c group5 = new Assignment_Group_Name__c(Name = 'MP/OB/IRP Special First Assignment');
        insert group5;
        
        
        Account acc = new Account(Name = 'Test Account1', RecordTypeId = JDE_AccountRT_ID,ShippingCountry = 'US',ShippingCity = 'Test',ShippingState = 'Test',ShippingStreet = 'Test',ShippingPostalCode = '12345');
        insert acc;
		Assignment_Groups__c assGrp1 = new Assignment_Groups__c(Group_Name__c = group1.Id, Country__c = 'US', Brand__c = 'Albin Pump', Distributor_Name__c = acc.Id, User__c = u.Id);
		Assignment_Groups__c assGrp2 = new Assignment_Groups__c(Group_Name__c = group2.Id, Country__c = 'US', Brand__c = 'Milton Roy', Distributor_Name__c = acc.Id, Queue_Name__c = queue1.Name);

        //MPOB Account and Assignment Group Records insertion
        Account acc1 = new Account(Name = 'Test Account2', RecordTypeId = GD_Parent_AccountRT_ID,ShippingCountry = 'US',ShippingCity = 'Test1',ShippingState = 'Test1',ShippingStreet = 'Test1',ShippingPostalCode = '123456');
        insert acc1;
		Assignment_Groups__c assGrp3 = new Assignment_Groups__c(Group_Name__c = group3.Id, Country__c = 'US', Brand__c = 'Oberdorfer', Distributor_Name__c = acc.Id, User__c = u.Id);
		Assignment_Groups__c assGrp4 = new Assignment_Groups__c(Group_Name__c = group4.Id, Country__c = 'US', Brand__c = 'Oberdorfer', Distributor_Name__c = acc.Id, Queue_Name__c = queue1.Name);
        Assignment_Groups__c assGrp5 = new Assignment_Groups__c(Group_Name__c = group5.Id, Country__c = 'US', Brand__c = 'Oberdorfer', Distributor_Name__c = acc.Id, Queue_Name__c = queue1.DeveloperName);
        Assignment_Groups__c assGrp6 = new Assignment_Groups__c(Group_Name__c = group1.Id, Country__c = 'US', Brand__c = 'Oberdorfer', Distributor_Name__c = acc.Id, Queue_Name__c = 'Abc_abc');
        
        Test.startTest(); 
		Database.SaveResult result1 = Database.insert(assGrp1, false);         
      	Database.SaveResult result2 = Database.insert(assGrp2, false);
        
        //MPOB results
        Database.SaveResult result3 = Database.insert(assGrp3, false);         
      	Database.SaveResult result4 = Database.insert(assGrp4, false);
        Database.SaveResult result5 = Database.insert(assGrp5, false);
        Database.SaveResult result6 = Database.insert(assGrp6, false);
        
        Test.stopTest(); 
        System.assert(result1.isSuccess());
        System.assert(!result2.isSuccess());
        System.assert(result3.isSuccess());
        System.assert(!result4.isSuccess());
        System.assert(result5.isSuccess());
        System.assert(!result6.isSuccess());
       /* System.assertEquals('"Current Territory :'+group1.Name+
                            '"\n Queue Name cannot be inserted in other territories except Special First Assignment',result1.getErrors()[0].getMessage());*/
    }
}