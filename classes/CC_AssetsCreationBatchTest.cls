@isTest
public class CC_AssetsCreationBatchTest {
  
  	@TestSetup
  	private static void createData(){
  		
        TestDataUtility testData = new TestDataUtility();
        
        TestDataUtility.createStateCountries();
        
        CC_Sales_Rep__c salesRepacc = testData.createSalesRep('0001');
        salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
        insert salesRepacc;
        
        TestDataUtility.createproductToassetMapSetting();
        account acc = testData.createAccount('Club Car: Channel Partner');
        acc.CC_Sales_Rep__c = salesRepacc.Id;
        acc.CC_Price_List__c='PCOPI';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Augusta';
        acc.BillingState = 'GA';
        acc.BillingStreet = '2044 Forward Augusta Dr';
        acc.BillingPostalCode = '566';
        acc.CC_Shipping_Billing_Address_Same__c = true;
        acc.CC_Siebel_ID__c = '123456789';
        acc.CC_Enterprise_Code__c = 'F';
        insert acc;
        
        CC_Order__c ord = TestUtilityClass.createCustomOrder(acc.id,'OPEN','N');
        ord.Siebel_Acct_Row_ID__c = '123456789';
        insert ord;
        
        List<Product2> productsList = new List<Product2>();
        Id ccProduct2RTId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Club_Car').getRecordTypeId();
        
        // Non-car product
        Product2 prod = TestUtilityClass.createProduct2();
        prod.ProductCode = '103774902';
        prod.RecordTypeId = ccProduct2RTId;
        productsList.add(prod);
        
        // Car product
        Product2 prod2 = TestUtilityClass.createProduct2();
        prod2.RecordTypeId = ccProduct2RTId;
        productsList.add(prod2);
        
        insert productsList;
        
        List<CC_Order_Item__c> orderItemsList = new List<CC_Order_Item__c>();
        
        // Non-car order item
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItem(ord.Id);
        orditem.Product__c = prod.id;
        orderItemsList.add(orditem);
        
        // Car order item
        CC_Order_Item__c orditem2 = TestUtilityClass.createOrderItem(ord.Id);
        orderItemsList.add(orditem2);
        
        insert orderItemsList;
        
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(orderItemsList[1].Id);
        carItem.Serial_No_s__c = 'SL1740747483~9;SL1~10;SL1740747488~11';
        carItem.Truck_Quantity__c = 5;
        carItem.Estimated_Ship_Date__c=System.today()+7;
        carItem.Asset_Status__c = 'STL Cars';
        insert carItem;
        
        CC_Car_Feature__c cf = TestUtilityClass.createCarFeature(carItem.Id,productsList[1].Id,ord.Id);
        insert cf;
        
        CC_Invoice2__C inv = TestUtilityClass.createInvoice2(acc.Id,'123');
        insert inv;
        
        CC_Order_Shipment__c ordship = TestUtilityClass.createOrderShipment(ord.id);
        ordship.Car_Assets_Created__c = false;
        ordship.Invoice_Date__c = date.today();
        ordship.Invoice_Number__c = '123';
        ordship.Ship_Date__c = date.today();
        insert ordship;
        
        List<CC_Order_Shipment_Item__c>shipmentitems = new List<CC_Order_Shipment_Item__c>();
        
        CC_Order_Shipment_Item__c ordshipitem = TestUtilityClass.createOrderShipmentItem(orderItemsList[1].Id,ordship.Id); // car asset
        shipmentitems.add(ordshipitem);
        
        CC_Order_Shipment_Item__c ordshipitem2 = TestUtilityClass.createOrderShipmentItem(orderItemsList[0].Id,ordship.Id); // product asset
        ordshipitem2.Ship_Quantity__c = 7;
        shipmentitems.add(ordshipitem2);
        
        CC_Order_Shipment_Item__c ordshipitem3 = TestUtilityClass.createOrderShipmentItem(orderItemsList[1].Id,ordship.Id); // car asset, same product as ordshipitem
        shipmentitems.add(ordshipitem3);
        
        insert shipmentitems;  		
  	}
  
    private static testmethod void testBatch(){
            
        CC_Order__c ord = [Select Id From CC_Order__c];   
        Product2 nonCarProduct = [Select Id From Product2 Where ProductCode != null];
            
        Test.startTest();
        
            String qstring = 'SELECT Id, Order__r.Siebel_Acct_Row_ID__c, Ship_Date__c, Order__r.Order_Number__c,' +
                            'Order__r.CC_Quote_Order_Date__c, Shipment_Header_Number__c, Car_Assets_Created__c,' +
                            'Order__r.Siebel_Opp_Row_ID__c, Order__r.CC_Account__r.CC_Siebel_ID__c '+
                            'FROM CC_Order_Shipment__c';
                            
            CC_AssetsCreationBatch acb = new CC_AssetsCreationBatch(qstring);
            Database.executebatch(acb,1);
            CC_AssetsCreationBatch sh1 = new CC_AssetsCreationBatch('');
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, sh1); 
            
        Test.stopTest();    
        
        // Validate results
        CC_Asset_Group__c[] productAssetGroups = [Select Id, Fleet_Status__c, Acquired_Date__c,  Order__c, Product__c, Quantity__c From CC_Asset_Group__c Where Product__r.ProductCode != null];  
        system.assertEquals(1, productAssetGroups.size());  
        system.assertEquals('Purchase-New', productAssetGroups[0].Fleet_Status__c);
        system.assertEquals(date.today(), productAssetGroups[0].Acquired_Date__c);
        system.assertEquals(ord.Id, productAssetGroups[0].Order__c);
        system.assertEquals(nonCarProduct.Id, productAssetGroups[0].Product__c);
        system.assertEquals(7, productAssetGroups[0].Quantity__c);
        
        CC_Asset_Group__c[] carAssetGroups = [Select Id, Fleet_Status__c, Acquired_Date__c,  Order__c, Product__c, Quantity__c From CC_Asset_Group__c Where Product__r.ProductCode = null];  
        system.assertEquals(1, carAssetGroups.size());   
        system.assertEquals('Lease-New', carAssetGroups[0].Fleet_Status__c);
        system.assertEquals(date.today(), carAssetGroups[0].Acquired_Date__c);
        system.assertEquals(ord.Id, carAssetGroups[0].Order__c);
        system.assertEquals(null, carAssetGroups[0].Product__c);
        system.assertEquals(5, carAssetGroups[0].Quantity__c);              
    }
}