@isTest
public class CC_PlanPipelineUpdateTest {
    
    static testmethod void testPlanPipelineUpdate(){
        
        //creating test Data
        TestDataUtility testData = new TestDataUtility();
        
           TestDataUtility.createStateCountries();
            
            //creating sales rep here
            CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1101');
            salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
            insert salesRepacc;
            
            account acc = TestDataFactory.createAccount('test accpimt', 'Club Car: New Customer');
            acc.CC_Sales_Rep__c = salesRepacc.Id;
            acc.CC_Price_List__c='PCOPI';
            acc.BillingCountry = 'USA';
            acc.BillingCity = 'Augusta';
            acc.BillingState = 'GA';
            acc.BillingStreet = '2044 Forward Augusta Dr';
            acc.BillingPostalCode = '566';
            acc.CC_Shipping_Billing_Address_Same__c = true;
            acc.CC_Customer_Number__c = '78685';
            insert acc;
        
           
            //create product line
            CC_Product_Line__c productLine = TestDataUtility.createProductLine();
            insert productLine;
        
            // create cc plans
           List<CC_Plan__c> ccplans = new List<CC_Plan__c>();
           ccplans.add(TestDataUtility.createCCPlan('Plan', salesRepacc.id, acc.id, productLine.Id,salesrepacc.CC_Sales_Person_Number__c,productLine.Name,acc.CC_Customer_Number__c));
           ccplans.add(TestDataUtility.createCCPlan('Objective', salesRepacc.id, acc.Id, productLine.Id,salesrepacc.CC_Sales_Person_Number__c,productLine.Name,acc.CC_Customer_Number__c));
           insert ccplans;
         
          
        
           opportunity opp = TestDataFactory.createOpportunity('testopty', 'CC Dealer Contract', acc.id, 'No','Budgetary','Qualify','Pipeline');
            opp.CC_Sales_Number__c = salesRepacc.id;
            opp.CC_Requested_Ship_Date__c = date.today();
            insert opp;
            
            
            
           
        
            //create pricebook and custom setting
        PriceBook2 pb = new PriceBook2(name='CC Utility Sales',isActive=true,description='ccutilsalestest',PBNAME__c='CC_UTILITY_SALES');
        insert pb;
        
         //create products and opp line item to pass validation rule
        Product2 objProduct = new Product2();
        objProduct.Name = 'Test';
        objProduct.Description = 'Test';
        objProduct.Family='Club Car Parts';
        Insert objProduct;
        
        PricebookEntry objPBEntry = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id=objProduct.Id,UnitPrice=500,IsActive=true);
        Insert objPBEntry;
   
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.PricebookEntryId = objPBEntry.id;
        oli.OpportunityId = opp.id;
        oli.Quantity = 1;
        oli.UnitPrice = 5000;
        oli.CC_Club_Car_Product_Line__c = 'Test ProdcutLine';
        insert oli;
            
            Test.startTest();
                
                 CC_PlanPipelineUpdate sh1 = new CC_PlanPipelineUpdate();
                String sch = '0 0 23 * * ?'; 
                system.schedule('Test Territory Check', sch, sh1); 
            Test.stopTest();
          
           
    }

}