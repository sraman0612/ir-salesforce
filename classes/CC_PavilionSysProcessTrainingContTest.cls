@isTest
public class CC_PavilionSysProcessTrainingContTest {

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testmethod void UnitTest_CustomerViewHome()
    {
        // Creation Of Test Data
        List<CC_Pavilion_Content__c> pclist = new List<CC_Pavilion_Content__c>();
        Set<String> TrainingEventTypList =new Set<String>();
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account',acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c=TestUtilityClass.createContact(acc.id);
        insert c;
        System.assertEquals(c.AccountId,acc.Id);
        System.assertNotEquals(null, c.Id);
     
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(u.ContactId,c.Id);
        System.assertNotEquals(null, u.Id);
        System.runAs(u)
        {
            CC_Pavilion_Content__c pc = TestUtilityClass.createCustomPavilionContentWithEventType('Process & System Training','Every Wednesday');
            CC_Pavilion_Content__c newpc = TestUtilityClass.createCustomPavilionContentWithEventType('Process & System Training','Every Third Thursday');
            pc.CC_Body_1__c = 'Test Body Content 11';
            newpc.CC_Body_1__c = 'Test Body Content 1';
            pclist.add(pc);
            pclist.add(newpc);
            insert pclist;
            TrainingEventTypList.add('Every Wednesday');
            TrainingEventTypList.add('Every Third Thursday');
            Attachment att = TestUtilityClass.createAttachment(pc.Id);
            insert att;
            System.assertEquals(att.ParentId,pc.Id);
            System.assertNotEquals(null, att.Id);
            CC_PavilionTemplateController controller;
            CC_PavilionSysProcessTrainingController SysProcess =new CC_PavilionSysProcessTrainingController(controller);    
            Test.startTest();
            SysProcess.ListEveryWednesday=pclist;
            SysProcess.TrainingEventTypeList=TrainingEventTypList;
            CC_PavilionSysProcessTrainingController.getEventType();
            System.assertNotEquals(null, CC_PavilionSysProcessTrainingController.getEventType());
            CC_PavilionSysProcessTrainingController.getCustomerVIEWBody();
            CC_PavilionSysProcessTrainingController.getCustomerVIEWHeading();
            CC_PavilionSysProcessTrainingController.getEveryWednesdayData('Every Wednesday');
            System.assertNotEquals(null,CC_PavilionSysProcessTrainingController.getEveryWednesdayData('Every Wednesday'));
            SysProcess.redirectToDiffPage();
            SysProcess.getEveryThursdayData();
            System.assertEquals(null,SysProcess.getEveryThursdayData());
            Test.stopTest();
        }
        
    }
    
}