public with sharing class CC_STL_Send_Master_Lease_Agreement_Ctrl {

    public CC_STL_Send_Master_Lease_Agreement_Ctrl(ApexPages.StandardController controller){

    }
    
    public PageReference buildAndSend(){
            
        Id recordId = ApexPages.currentPage().getParameters().get('id');    
        PageReference pdf = Page.CC_STL_Master_Lease_Agreement_PDF;
        pdf.getParameters().put('id', recordId);
        
        Attachment attach = new Attachment();
        Blob body;
        DateTime now = System.now();
        
        try{
            body = pdf.getContent();
        }
        catch(VisualforceException e){
            body = Blob.valueOf('Exception is thrown while creating Pdf Page');
        }
                
        attach.Body = body;
        attach.Name = 'Master Lease Agreement-' + now + '.pdf';
        attach.IsPrivate = false;
        attach.ParentId = recordId;
        insert attach;
        
        Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
    
        return new PageReference('/apex/echosign_dev1__AgreementTemplateProcess?masterid=' + recordId + '&templateId=' + templates.CC_Master_Lease_Template_ID__c);
    }
}