@isTest
global class InterlynxCalloutMockTest implements HttpCalloutMock{
    Integer statusCode; 
    global InterlynxCalloutMockTest(Integer statusCode){
        this.statusCode = statusCode;
    }
    global HttpResponse respond(HttpRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setBody('{"Message":"Lead Created Successfully"}');
        res.setStatusCode(this.statusCode);
        return res;
    }
}