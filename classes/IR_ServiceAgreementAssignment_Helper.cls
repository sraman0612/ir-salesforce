public without sharing  class IR_ServiceAgreementAssignment_Helper {

    
    public static void doWork(List<Service_Agreement__c> agreements) {
        
        Map<Id, Service_Agreement__c> agrMap = new Map<Id, Service_Agreement__c>();
        for(Service_Agreement__c agr : agreements){
            agrMap.put(agr.Id, agr);
        }            
        
        List<AggregateResult> results = [SELECT Asset_Number__c, Service_Agreement__c FROM Entitlement__c WHERE Service_Agreement__c IN :agreements GROUP BY Service_Agreement__c, Asset_Number__c];

        List<Asset> theAssets = new List<Asset>();

        for(AggregateResult ar : results){
            System.debug(ar);
            if(String.isNotEmpty( (String) ar.get('Asset_Number__c'))){
                Asset aTemp = new Asset();
                aTemp.Id = (Id) ar.get('Asset_Number__c');
                Service_Agreement__c agr = agrMap.get((Id) ar.get('Service_Agreement__c'));
                
                aTemp.Service_Agreement__c = agr.Id;
                
                if(agr.Agreement_Start_Date__c != null){
                    aTemp.Contract_Start_Date__c = agr.Agreement_Start_Date__c.date();
                }
                if(agr.Agreement_End_Date__c != null){
                    aTemp.Contract_End_Date__c = agr.Agreement_End_Date__c.date();
                }
                theAssets.add(aTemp);
                
            }
        }
  
        if(theAssets.size() > 0){
            update theAssets;

            for(Service_Agreement__c agr : agreements){
                agr.Assets_Associated__c = true;
            }

            update agreements;
        }

    }

}