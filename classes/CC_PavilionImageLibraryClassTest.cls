@isTest
public class CC_PavilionImageLibraryClassTest
{
      static testmethod void UnitTest_CC_PavilionImageLibraryClass()
    {
        User u = [Select id from User where Id = :UserInfo.getUserId() LIMIT 1];
     List<Contract> cntList =new List<Contract>();
        List<ContentVersion> contverList =new List<ContentVersion>();
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        System.assertEquals('Sample Account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c = TestUtilityClass.createContact(acc.id);
        insert c;
        Contract firstcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer', acc.id);
        cntList.add(firstcnt);
        System.assertEquals('Golf Car Dealer', firstcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,firstcnt.AccountId);
        Contract secondcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Distributor', acc.id);
        cntList.add(secondcnt);
        System.assertEquals('Golf Car Distributor', secondcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,secondcnt.AccountId);
        Contract thirdcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Industrial Utility', acc.id);
        cntList.add(thirdcnt);
        System.assertEquals('Industrial Utility', thirdcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,thirdcnt.AccountId);
        insert cntList; 
        ContentVersion contentVer=TestUtilityClass.createCustomContentVersion ('Custom Solutions','Japan','USD','Industrial Utility');
        insert contentVer;    
         System.runAs(u)
        {
            test.startTest();
            CC_PavilionTemplateController controller;
            CC_PavilionImageLibraryClass imageLib = new CC_PavilionImageLibraryClass(controller);
        CC_PavilionImageLibraryClass.getTires(acc.id);
        CC_PavilionImageLibraryClass.getConcopyCol(acc.id);
        CC_PavilionImageLibraryClass.getConopyColr(acc.id);
        CC_PavilionImageLibraryClass.getBoxes(acc.id);
        CC_PavilionImageLibraryClass.getTypeBoxes(acc.id);
        CC_PavilionImageLibraryClass.getBoxColor(acc.id);
        CC_PavilionImageLibraryClass.getWindows(acc.id);
        CC_PavilionImageLibraryClass.getAmbulance(acc.id);
        CC_PavilionImageLibraryClass.getLadderRack(acc.id);
        CC_PavilionImageLibraryClass.getsplfeatures(acc.id);
        CC_PavilionImageLibraryClass.getPeople(acc.id);
        CC_PavilionImageLibraryClass.getviewSet(acc.id);
        CC_PavilionImageLibraryClass.getTireSets(acc.id);
        CC_PavilionImageLibraryClass.getcontractSubTypes(acc.id);
        CC_PavilionImageLibraryClass.getImages('Japan','USD',acc.id);
        String s='';
        s='testlogo';
        imageLib.zipLogoURL=s;
           
            test.stopTest();
        }
        
      }
      
  }