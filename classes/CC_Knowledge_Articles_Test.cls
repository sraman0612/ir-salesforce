@isTest(SeeAllData=false)
public class CC_Knowledge_Articles_Test {
    
    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    
	@isTest
    private static void getParameterTest(){
        List<User> knwUsers = buildUsers(2, 'CC_System Administrator', 't56');
        insert knwUsers;
        System.runAs(knwUsers[0]){
            insertKnowledgeArticles(5, true);
            CC_Knowledge_Articles.getArticlesOnLoad('mostHelpfulArticles');
            CC_Knowledge_Articles.getArticlesOnLoad('EntitySubscription');
            CC_Knowledge_Articles.knowledgeObject kObject = 
            new CC_Knowledge_Articles.knowledgeObject('String Test','Url Test',System.now());
        }
    }
    
    
    private static List<Knowledge__kav> insertKnowledgeArticles(Integer batchSize,
                                                                Boolean isPublish) {
            List<Knowledge__kav> knowledgeArticles = new List<Knowledge__kav>();
            Id clubCarRTId = [SELECT Id FROM RecordType WHERE SObjectType='Knowledge__kav' AND DeveloperName='Club_Car'].Id;
            for(Integer i = 0; i < batchSize; i++) {
              Knowledge__kav testKnw = new Knowledge__kav();
              testKnw.Title = 'Test CC knw'+i;
              testKnw.Summary = 'Knw Summary'+i;
              testKnw.URLName = 'test'+i;
              testKnw.Language='en_US';
              testKnw.CTS_TechDirect_Article_Type__c = 'Case Sharing';
              testKnw.RecordTypeId = clubCarRTId;
              knowledgeArticles.add(testKnw);
            }
            insert knowledgeArticles;
        
            if(isPublish == true){ 
                for(Knowledge__kav knw : [SELECT KnowledgeArticleId
                                          FROM Knowledge__kav
                                          WHERE ID IN: knowledgeArticles]) {
                  KbManagement.PublishingService.publishArticle(knw.KnowledgeArticleId, true);
                }
            }
            return knowledgeArticles;
  }
    
    
  public static List<User> buildUsers(Integer numberOfTestUsers, String profile,String uniqueIdentifier) {

    List<User> resultUsers = new List<User>();
    Profile standardProfile = [Select Id From Profile Where Name =: profile];

    	User u = null;
        for (Integer i = 0; i < numberOfTestUsers; i++) {
          u = new User();
          u.Email = 'testUser'+uniqueIdentifier + i + '@' + 'test' + i + 'test.com';
          u.LastName = 'TestUserLastName' + i+uniqueIdentifier;
          u.Username = 'testUser'+uniqueIdentifier + i + '@' + i + 'cctest.com';
          u.CommunityNickname = 'tu' + i + uniqueIdentifier;
          u.Alias = 'CCU' + i + 'CCK';
          u.ProfileId = standardProfile.Id;
          u.EmailEncodingKey = 'UTF-8';
          u.LanguageLocaleKey = 'en_US';
          u.TimeZoneSidKey = 'America/Los_Angeles';
          u.LocaleSidKey = 'en_US';
          u.UserPermissionsKnowledgeUser = true;
          resultUsers.add(u);
        }
    return resultUsers;
  }
}