//
// (c) 2015, Appirio Inc
//
// Test class for Lead Trigger
//
// Apr 28, 2015     George Acker     original
//
/* Revisions:

16-Sept-2024 : Added setup method and testLeadOwnerOOOCheck method to check 'OOO' for Lead Assignment for Case #03210798 by AMS Team
*/
@isTest
private class Lead_TriggerHandler_Test {
    
    @testSetup
    static void setup(){
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        list<User> userList = new list<User>();
        User delegatedUser = new User(alias = 'delg', email='test123@noemail.com',
                                      Business_Hours__c = 'IR Comp Business Hours - EU + Africa',
                                      emailencodingkey='UTF-8', lastname='DelegatedUser', languagelocalekey='en_US',
                                      localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                                      Business__c ='Thomas',timezonesidkey='America/Los_Angeles',
                                      username='testervdelegateduser@noemail.comxyz');
        userList.add(delegatedUser);
        User secondUser = new User(alias = 'test123', email='test123@noemail.com',
                                  Business_Hours__c = 'IR Comp Business Hours - EU + Africa',
                                  emailencodingkey='UTF-8', lastname='FirstUser', languagelocalekey='en_US',
                                  localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                                  Business__c ='Thomas',timezonesidkey='America/Los_Angeles',
                                  username='testseconduser@noemail.comxyz');
        userList.add(secondUser);
        User firstUser = new User(alias = 'test123', email='test123@noemail.com',
                                  Business_Hours__c = 'IR Comp Business Hours - EU + Africa',
                                  emailencodingkey='UTF-8', lastname='FirstUser', languagelocalekey='en_US',
                                  localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                                  Business__c ='Thomas',timezonesidkey='America/Los_Angeles',
                                  username='testfirstuser@noemail.comxyz');
        userList.add(firstUser);
        insert userList;
        
        firstUser.Out_of_Office_Start_Date__c = System.today()-1;
        firstUser.Out_of_Office_End_Date__c = System.today()+3;
        firstUser.Delegated_User_When_Out_Of_Office__c = delegatedUser.id;
        
        update firstUser;
        
    }
    
    // Method to test lead trigger
    static testMethod void testLeadTrigger() {
        
        Group q = new Group(name = 'Test Queue', developername = 'Test_Queue', type = 'Queue');
        insert q;
        
        QueueSobject mappingObject = new QueueSobject(QueueId = q.Id, SobjectType = 'Lead');
        //insert mappingObject;
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
        Assignment_Group_Name__c agn1 = new Assignment_Group_Name__c(name = 'Test AGN 1');
        insert agn1;
        
        Assignment_Group_Queues__c agq1 = new Assignment_Group_Queues__c(Assignment_Group_Name__c = agn1.Id, name = 'Test Queue');
        insert agq1;
        
        Assignment_Groups__c ag1 = new Assignment_Groups__c();
        ag1.group_name__c = agn1.Id;
        ag1.state__c = 'New York';
        ag1.county__c = 'test county';
        ag1.active__c = 'True';
        ag1.Assignment_Key__c = 'New Yorktest county';
        ag1.user__c = UserInfo.getUserId();
        insert ag1;
        
        Lead l = new Lead();
        l.firstname = 'George';
        l.lastname = 'Acker';
        l.phone = '1234567890';
        l.City = 'city';
        l.Country = 'USA';
        l.PostalCode = '12345';
        l.State = 'AK';
        l.Street = '12, street1';
        l.company = 'ABC Corp';
        l.county__c = 'test county';
        l.ownerId = q.Id;
        l.Assignment_Key__c = 'New Yorktest county';
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('PT Lead').getRecordTypeId();
        
        insert l;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User u = new User( ProfileId = profile1.Id,
                          Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                          Firstname='test', Lastname='Test',
                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                          TimeZoneSidKey='America/New_York',country='USA');
        Database.insert(u);
        
        system.runAs(u){
            Test.startTest();
            
            l.ownerId = q.Id;
            update l;
            
            l.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
            update l;
            
            sObject[] sObjQuery = [SELECT Id FROM Lead WHERE Id = :l.Id];
            //System.assertEquals(1, sObjQuery.size());     
            Lead[] leads = new Lead[]{};
                for (integer i=0;i<7;i++)
            {
                Lead l2 = new Lead();
                l2.firstname = 'George'+i;
                l2.lastname = 'Acker'+i;
                l2.phone = '1234567890'+i;
                l2.City = 'city'+i;
                l2.Country = 'USA'+i;
                l2.PostalCode = '12345'+i;
                l2.State = 'AK';
                l2.Street = '12, street1'+i;
                l2.company = 'ABC Corp'+i;
                l2.county__c = 'test county'+i;
                l2.ownerId = q.Id;
                l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
                
                leads.add(l2);
            }
            insert leads;
            update leads;
            delete leads;
            Test.stopTest();
        }     
    }
    static testMethod void testValidateBusiness_Insert() {
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                             Business__c ='Vacuum',timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
        
        System.runas(user) {
            Lead l3 = new Lead();
            l3.firstname = 'George';
            l3.lastname = 'Acker';
            l3.phone = '1234567890';
            l3.City = 'city';
            l3.Country = 'USA';
            l3.PostalCode = '12345';
            l3.State = 'AK';
            l3.Street = '12, street1';
            l3.company = 'ABC Corp';
            l3.county__c = 'test county';
            l3.Assignment_Key__c = 'New Yorktest county';
            l3.Business__c = 'Low Pressure';
            l3.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'New_Standard_Lead' LIMIT 1].Id;
            
            List<Lead> leads = new List<Lead>();
            leads.add(l3);
            Map<id,Lead> oldLeads = new Map<id,Lead>();
            Test.startTest();  
            User user1  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
            
            EMEIA_LeadTriggerHandler.validateBusiness(leads,true,oldLeads,user1);
            // Assert that the method added an error to the test event
            System.assertEquals(1, l3.getErrors().size());
            //commented as it is hardcoded string instead used VPTECH_Lead Custom Label.
            //System.assertEquals('You can not create/update Leads for businesses other than yours.', l3.getErrors()[0].getMessage());
            System.assertEquals(System.Label.VPTECH_Lead, l3.getErrors()[0].getMessage());
 
            Test.stopTest();}
    }
    
        static testMethod void testLeadConversion() {
        // Create test data
        //User erUser = [Select id from User where Profile.name='System Administrator' and Business__c='Low Pressure' and Channel__c='Direct' limit 1];
        /* Profile prof = [select id from profile where name = 'Vac and Lower Pressure'];

User u2 = new User( ProfileId = prof.Id,
Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
Firstname='test2', Lastname='Test2',
LanguageLocaleKey='en_US',LocaleSidKey='en_US',Business__c = 'Low Pressure',
TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa'
);
insert u2;*/
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User eruser = new User(alias = 'test123', email='test123@noemail.com',
                               Business_Hours__c = 'IR Comp Business Hours - EU + Africa',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                               Business__c ='Low Pressure',timezonesidkey='America/Los_Angeles',
                               username='tester@noemail.comxyz'+System.now().millisecond());
        insert eruser;
        
        System.debug('User --> '+erUser);
        System.runAs(erUser){
           
            Account a = new Account();
            a.Name = 'Acme';
            a.ShippingStreet ='TestAddress';
            a.ShippingCity = 'TestCity';
            a.ShippingState = 'TestState';
            a.ShippingCountry = 'BD';
            a.ShippingPostalCode = '421002';
            a.CTS_Global_Region__c ='MEIA';
            a.RecordTypeId =[SELECT Id FROM RecordType WHERE DeveloperName = 'GD_Parent' LIMIT 1].Id;
            insert a;
            
            Business_Relationship__c br = new Business_Relationship__c();
            br.Account__c = a.Id;
            br.Business__c = 'Low Pressure';
            br.FWA__c = true;
            insert br;
            BusinessHours bhs=[select id from BusinessHours where name = 'IR Comp Business Hours - EU + Africa'];
            System.debug(bhs.id);
            Lead testLead = new Lead();
            testLead.LeadSource='Identified by Sales Rep';
            testLead.Status='New';
            testLead.Company = 'Acme';
            testLead.Business__c='Low Pressure';
            testLead.Opportunity_Type__c='Budgetary';
            testLead.CurrencyIsoCode= 'EUR';
            testLead.firstname = 'George';
            testLead.lastname = 'Acker';
            testLead.phone = '1234567890';
            testLead.City = 'city';
            testLead.Country = 'BD'; 
            testLead.PostalCode = '12345';
            testLead.State = 'AK';
            testLead.Street = '12, street1';
            testLead.company = 'ABC Corp';
            testLead.county__c = 'test county';
            testLead.Assignment_Key__c = 'New Yorktest county';
            testLead.Business__c = 'Low Pressure';
            testLead.CTS_Lead_Region_Mappin__c='MEIA';
            testLead.Business_Hours__c = bhs.id;
            testLead.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Lead' LIMIT 1].Id;
            insert testLead;
            
            Contact testContact = new Contact();
            testContact.FirstName = 'Test Contact1';
            testContact.LastName = 'Test Contact';
            testContact.Business__c = 'Low Pressure';
            testContact.Lead_Convert_Bypass__c = 'false';
            testContact.AccountId = a.id;
            testContact.email='testuser@test.com';
            testContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            insert testContact;
            
            Opportunity o = new Opportunity();
            o.name = 'Test Opportunity';
            o.CurrencyIsoCode = 'USD'; //'EUR'; 
            o.stagename = 'Stage 1. Qualify';
            o.amount = 1000000;
            o.closedate = system.today();
            o.Business__c = 'Low Pressure';
            o.CTS_WasConverted__c=false;
            o.Framework_Flag__c = true;
            o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
            o.AccountId = a.Id;
            insert o;
            
            Test.startTest();
            // Test the lead conversion method
            Database.LeadConvert lc = new database.LeadConvert();  
            lc.setLeadId( testLead.Id );
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Converted');
            lc.setAccountId(a.Id);
            lc.setContactId(testContact.Id);
            lc.setOpportunityId(o.id);
            try{
                Database.LeadConvertResult lcr = Database.convertLead(lc); 
            }catch(exception ex){
                system.debug('exception --> '+ex.getMessage());
            }            
            Test.stopTest();
            //system.debug( 'Errors are ' + lcr.getErrors() );  
            
            //system.assert( lcr.isSuccess() );
        }
    }
    
     static testMethod void testLeadConversionValidation() {
        // Create test data
        //User erUser = [Select id from User where Profile.name='System Administrator' and Business__c='Low Pressure' and Channel__c='Direct' limit 1];
        /* Profile prof = [select id from profile where name = 'Vac and Lower Pressure'];

User u2 = new User( ProfileId = prof.Id,
Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
Firstname='test2', Lastname='Test2',
LanguageLocaleKey='en_US',LocaleSidKey='en_US',Business__c = 'Low Pressure',
TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa'
);
insert u2;*/
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User eruser = new User(alias = 'test123', email='test123@noemail.com',
                               Business_Hours__c = 'IR Comp Business Hours - EU + Africa',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                               localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                               Business__c ='Low Pressure',timezonesidkey='America/Los_Angeles',
                               username='tester@noemail.comxyz'+System.now().millisecond());
        insert eruser;
        
        System.debug('User --> '+erUser);
        System.runAs(erUser){
            Account a = new Account();
            a.Name = 'Acme';
            a.ShippingStreet ='TestAddress';
            a.ShippingCity = 'TestCity';
            a.ShippingState = 'TestState';
            a.ShippingCountry = 'BD'; 
            a.ShippingPostalCode = '421002';
            a.CTS_Global_Region__c ='MEIA';
            a.RecordTypeId =[SELECT Id FROM RecordType WHERE DeveloperName = 'GD_Parent' LIMIT 1].Id;
            insert a;
            
            Business_Relationship__c br = new Business_Relationship__c();
            br.Account__c = a.Id;
            br.Business__c = 'Low Pressure';
            br.FWA__c = true;
            insert br;
            BusinessHours bhs=[select id from BusinessHours where name = 'IR Comp Business Hours - EU + Africa'];
            System.debug(bhs.id);
            Lead testLead = new Lead();
            testLead.LeadSource='Identified by Sales Rep';
            testLead.Status='New';
            testLead.Company = 'Acme';
            testLead.Business__c='Low Pressure';
            testLead.Opportunity_Type__c='Budgetary';
            testLead.CurrencyIsoCode= 'EUR';
            testLead.firstname = 'George';
            testLead.lastname = 'Acker';
            testLead.phone = '1234567890';
            testLead.City = 'city';
            testLead.Country = 'BD'; 
            testLead.PostalCode = '12345';
            testLead.State = 'AK';
            testLead.Street = '12, street1';
            testLead.company = 'ABC Corp';
            testLead.county__c = 'test county';
            testLead.Assignment_Key__c = 'New Yorktest county';
            testLead.Business__c = 'Low Pressure';
            testLead.CTS_Lead_Region_Mappin__c='MEIA';
            testLead.Business_Hours__c = bhs.id;
            testLead.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Lead' LIMIT 1].Id;
            insert testLead;
            
            Contact testContact = new Contact();
            testContact.FirstName = 'Test Contact1';
            testContact.LastName = 'Test Contact';
            testContact.Business__c = 'Vacuum';
            testContact.Lead_Convert_Bypass__c = 'false';
            testContact.AccountId = a.id;
            testContact.email='testuser@test.com';
            testContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            insert testContact;
            
            Opportunity o = new Opportunity();
            o.name = 'Test Opportunity';
            o.CurrencyIsoCode = 'USD'; //'EUR'; 
            o.stagename = 'Stage 1. Qualify';
            o.amount = 1000000;
            o.closedate = system.today();
            o.Business__c = 'Vacuum';
            o.CTS_WasConverted__c=false;
            o.Framework_Flag__c = true;
            o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
            o.AccountId = a.Id;
            insert o;
            // Test the lead conversion method
			Test.startTest();            
            Database.LeadConvert lc = new database.LeadConvert();  
            lc.setLeadId( testLead.Id );
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Converted');
            lc.setAccountId(a.Id);
            lc.setContactId(testContact.Id);
            lc.setOpportunityId(o.id);
            try{
                Database.LeadConvertResult lcr = Database.convertLead(lc); 
            }catch(exception ex){
                system.debug('exception --> '+ex.getMessage());
            }            
            Test.stopTest();
            //system.debug( 'Errors are ' + lcr.getErrors() );  
            
            //system.assert( lcr.isSuccess() );
       }
    }
    /* changes for enhancement case : #03210798 by AMS Team (Mahesh) - Sept24 Release */
    static testMethod void testLeadOwnerOOOCheck(){
        
        test.startTest();
        
        User queriedUser = [select id, Out_of_Office_Start_Date__c, Out_of_Office_End_Date__c, Delegated_User_When_Out_Of_Office__c, Is_OOO__c from user where username = 'testseconduser@noemail.comxyz'];

        BusinessHours bhs=[select id from BusinessHours where name = 'IR Comp Business Hours - EU + Africa'];
        TestDataUtility TD = new TestDataUtility();
        Lead testLead = TD.createLead('Standard Lead','productAuthorization');
        insert testLead;
        
        User queryFirstUser = [select id, Out_of_Office_Start_Date__c, Out_of_Office_End_Date__c, Delegated_User_When_Out_Of_Office__c, Is_OOO__c from user where username = 'testfirstuser@noemail.comxyz'];
        
        testLead.ownerid = queryFirstUser.Id;
        update testLead;
        
        list <Lead> insertedLead = [select id,owner.name from Lead where id =: testLead.Id];
        //system.debug('=====insertedLead[0].owner.name: '+insertedLead[0]);
        system.assertEquals('DelegatedUser',insertedLead[0].owner.name);
        test.stopTest();
    }
    
    /*static testMethod void testAfterUpdate() {

// Create test data
List<Lead> newLeads = new List<Lead>();
Lead l = new Lead();
l.FirstName = 'John';
l.LastName = 'Doe';
l.Company = 'Acme';
l.Status = 'Open';
l.Business__c = 'Vacuum';
l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard_Lead').getRecordTypeId();
insert l;
newLeads.add(l);

Lead convertedLead = new Lead();
convertedLead.ConvertedContactId = l.Id;
convertedLead.ConvertedAccountId = l.Id;
convertedLead.ConvertedOpportunityId = l.Id;
convertedLead.IsConverted = true;
convertedLead.Status = 'Closed - Converted';
convertedLead.Business__c = 'Vacuum';
convertedLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard_Lead').getRecordTypeId();
insert convertedLead;

Contact c = new Contact();
c.LastName = 'Doe';
c.AccountId = l.ConvertedAccountId;
insert c;

Account a = new Account();
a.Name = 'Acme';
insert a;

Opportunity o = new Opportunity();
o.Name = 'Test Opportunity';
o.AccountId = l.ConvertedAccountId;
insert o;

Business_Relationship__c br = new Business_Relationship__c();
br.Account__c = a.Id;
br.Business__c = l.Business__c;
br.FWA__c = true;
insert br;

// Call trigger handler method
Test.startTest();
EMEIA_LeadTriggerHandler.afterUpdate(newLeads, new Map<Id,Lead>(), new List<Lead>{convertedLead});
Test.stopTest();

// Verify results
c = [SELECT Business__c FROM Contact WHERE Id = :c.Id];
System.assertEquals('Vacuum', c.Business__c);

o = [SELECT Framework_Flag__c FROM Opportunity WHERE Id = :o.Id];
System.assertEquals(true, o.Framework_Flag__c);

}*/
    static testMethod void shareLeadsWithChannelPartnerTest(){
       
        
        User queriedUser1 = [select id, Out_of_Office_Start_Date__c, Out_of_Office_End_Date__c, Delegated_User_When_Out_Of_Office__c, Is_OOO__c from user where username = 'testseconduser@noemail.comxyz'];

        BusinessHours bhs=[select id from BusinessHours where name = 'IR Comp Business Hours - EU + Africa'];
        TestDataUtility TD = new TestDataUtility();
        Lead testLead1 = TD.createLead('Standard Lead','productAuthorization');
        insert testLead1;
        
        //User queryFirstUser1 = [select id, Out_of_Office_Start_Date__c, Out_of_Office_End_Date__c, Delegated_User_When_Out_Of_Office__c, Is_OOO__c from user where username = 'testfirstuser@noemail.comxyz'];
        test.startTest();
        testLead1.Channel_Partner_Sales_Manager__c=queriedUser1.Id;
        update testLead1;
        LeadShare shareRec = [Select Id from LeadShare where UserOrGroupId =:queriedUser1.Id ];
        system.assertNotEquals(null,shareRec);
        test.stopTest();
   
    }
   
    
}