global class C_EscalationBatch implements Database.Batchable<sObject>, Database.Stateful{
    global final String Query;
    global Map<Id, String> failedCases;
    
    global C_EscalationBatch(String qry){
        Query = qry;
        failedCases = new Map<Id, String>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for(sObject s : scope){
            if(s instanceof Case){
                Case c = (Case) s;
                c.Escalation_Count__c = c.Escalation_Count__c == NULL ? 0 : ++c.Escalation_Count__c;
                c.isEscalated = false;
                
                try{
                    update c;
                    if(Test.isRunningTest()){
                        throw new DMLException();
                    }
                }
                catch(exception e){
                    failedCases.put(c.Id, e.getMessage());
                }
            }
        }      
        
    }
    
    global void finish(Database.BatchableContext BC){
        if(failedCases.size() > 0){
            String errorMessage = 'The following cases failed their escalation processes:<br/>';
            
            for(Id i : failedCases.keySet()){
                errorMessage += i + ' - ' + failedCases.get(i) + '<br/>';
            }
            
           // C_ErrorReporter.sendErrorReport('Exception in Escalation Batch Scheduler', C_EscalationBatch.class.getName(), errorMessage, '-', '');
        }
        
    }
    
}