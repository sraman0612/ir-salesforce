@isTest
public class CC_PavilionOrderLiteratureTest{

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
    static testmethod void UnitTest_CC_PavilionOrderLiterature() {
        // Creation Of Test Data
        List<Contract> cntList =new List<Contract>();
        List<ContentVersion> contverList =new List<ContentVersion>();
        Account acc = TestUtilityClass.createAccountWithRegionAndCurrency('Japan','USD');
        insert acc;
        System.assertEquals('Sample Account', acc.Name);
        System.assertNotEquals(null, acc.Id);
        Contact c = TestUtilityClass.createContact(acc.id);
        insert c;
        Contract firstcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Dealer', acc.id);
        cntList.add(firstcnt);
        System.assertEquals('Golf Car Dealer', firstcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,firstcnt.AccountId);
        Contract secondcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Golf Car Distributor', acc.id);
        cntList.add(secondcnt);
        System.assertEquals('Golf Car Distributor', secondcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,secondcnt.AccountId);
        Contract thirdcnt = TestUtilityClass.createContractWithTypes('Dealer/Distributor Agreement','Industrial Utility', acc.id);
        cntList.add(thirdcnt);
        System.assertEquals('Industrial Utility', thirdcnt.CC_Sub_Type__c);
        System.assertEquals(acc.Id,thirdcnt.AccountId);
        insert cntList;
       
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(u.ContactId,c.Id);
        System.assertNotEquals(false, u.IsActive); 
        ContentVersion testContentInsert = TestUtilityClass.createCustomContentVersion('Order Literature','Japan','USD','Commercial Utility; Golf Car Distributor');
        insert testContentInsert;
        System.assertNotEquals(null,testContentInsert.Id);        
       
        System.runAs(u)
        {
            test.startTest();
            CC_PavilionTemplateController controller;
            CC_PavilionOrderLiterature CC_PVOrderLit=new CC_PavilionOrderLiterature (controller);
            CC_PavilionOrderLiterature.getMarketingImages();
            test.stopTest();
        }
           
    }
}