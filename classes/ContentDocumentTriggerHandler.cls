public class ContentDocumentTriggerHandler {
    private static boolean run = true;
    public static boolean runOnce(){
    if(run){
     run=false;
     return true;
    }else{
        return run;
    }
    }
    public static void sendEmailForNewContDoc(List<ContentDocument> cDList){
        List<CC_Content_Publish_Email__c> cPEInsertlist = new List<CC_Content_Publish_Email__c>();
        Map<Id,Set<Id>> contDocAndUserMap=new Map<Id,Set<Id>>();
        Map<Id,Set<String>> regCurrAgMap= new Map<Id,Set<String>>();
        Map<Id,ContentVersion> cntDocMap=new Map<Id,ContentVersion>();
        Map<Id,String> contractCntDocMap=new Map<Id,String>();
        Map<Id,String> contractMap=new Map<Id,String>();
        Map<Id,ContentVersion> cntVerMap=new Map<Id,ContentVersion>();
        Set<String> regionSet=new Set<String>();
        List<User> userList=new List<User>();
        List<User> userList1=new List<User>();
        
        List<String> agglist = new List<String>();
        Set<Id> accId=new Set<Id>();
        List<Contract> cntList=new List<Contract>();
        List<ContentVersion> cnvList=new List<ContentVersion >();
        List<ContentVersion> updateCnvList=new List<ContentVersion >();
        Set<Id> contractidSet=new Set<Id>();
        Set<Id> contentDocIdSet=new Set<Id>();
        try{
            
            for(ContentDocument cd : cDList){
                    contentDocIdSet.add(cd.Id);   
            }
            if(contentDocIdSet.size()>0){
             System.debug('%%%%%%%contentDocIdSet'+contentDocIdSet);

                cnvList=[select Id,Title,Owner.AccountId,CC_DoNotSendEmail__c,ContentDocumentId,RecordType.Name,Bulletin_Title__c,CC_Send_Email__c,CC_Region__c,Currency__c,Agreement_Type__c,Bulletin_Number__c,CC_Bulletin_Type__c 
                                       from ContentVersion 
                                       where ContentDocumentId IN: contentDocIdSet];
               System.debug('&&&&&ContentVersionList'+cnvList);
               System.debug('&&&&&ContentDocumentList'+cDList);
            }                           
           //prepare map of ContentDocumentId and Contentversion records 
           for(ContentDocument cd1 : cDList){
               for(ContentVersion cv: cnvList){
                 if(cv.RecordType.Name == 'bulletin' && cv.CC_Send_Email__c == True )  
                            cntDocMap.put(cd1.Id,cv);
                            //cntVerMap.put(cv.id,cv);
                }
           }      
            String agType;
           System.debug('%%%%%%%cntDocMap'+cntDocMap);
            for(Id keyId : cntDocMap.keyset()){
            ContentVersion cVersion = cntDocMap.get(keyId);
                   
                   if(cVersion.CC_Region__c != null) {
                       String reg = cVersion.CC_Region__c;
                       if(reg.contains(';')) {
                          
                          regionSet.addAll(reg.split(';'));
                          regCurrAgMap.put(keyId,regionSet);
                       }
                       else {
                       
                         regionSet.add(reg);
                         regCurrAgMap.put(keyId,regionSet);
                       }
                   } 
                   
                   if(cVersion.CC_Bulletin_Type__c!= null) {
                       String reg = cVersion.CC_Bulletin_Type__c;
                      
                         regionSet.add(reg);
                         regCurrAgMap.put(keyId,regionSet);
                      
                   }  
                   
                   if(cVersion.Currency__c != null) {
                       String curr = cVersion.Currency__c;
                       if(curr.contains(';')) {
                          regionSet.addAll(curr.split(';'));
                          regCurrAgMap.put(keyId,regionSet);
                          
                       }
                       else {
                          regionSet.add(curr);
                          regCurrAgMap.put(keyId,regionSet);
                          
                       }
                   }
                   
                   if(cVersion.Agreement_Type__c != null) {
                       agType = cVersion.Agreement_Type__c;
                       if(agType.contains(';')) {
                           regionSet.addAll(agType.split(';'));
                          regCurrAgMap.put(keyId,regionSet);
                         
                       }
                       else {
                          regionSet.add(agType);
                          regCurrAgMap.put(keyId,regionSet);
                          
                       }
                   }
                   system.debug('@@@@agtype'+cntDocMap.get(keyId));
                 } 
             system.debug('*****regCurrMap***'+regCurrAgMap);
             Set<Id> ccCommProfSet = new Set<Id>();
             ccCommProfSet.add(PavilionSettings__c.getInstance('ClubCarCommunityLoginProfileId').Value__c);
             ccCommProfSet.add(PavilionSettings__c.getInstance('PavillionCommunityProfileId').Value__c);
             system.debug('@@@@regionSet'+regionSet);
             
             //code contributed by @Priyanka Baviskar for issue no 7823346. 
             for(Id conId : regCurrAgMap.keyset()){
             userList= [select id , AccountID, Email, UserName,Profile.Name,Account.CC_Bulletins_Currency__c,Account.CC_Global_Region__c,ContactId,CC_View_Parts_Bulletins__c,
                            CC_View_Parts_Pricing__c,CC_View_Sales_Bulletins__c,CC_View_Sales_Pricing__c,CC_View_Service_Bulletins__c from User 
                             Where AccountID In (Select AccountID from Contract where (CC_Sub_Type__c In :regCurrAgMap.get(conId))) and ProfileId in :ccCommProfSet and isActive=true];
             
             system.debug('*****userList***'+userList);
             
                Set<Id> stSet=new Set<Id>();
                List<User> usrList = new List<User>();
                String partsSalesServSet = '';
                for(User usr : userList){
                    accId.add(usr.AccountId);
                    if(usr.CC_View_Parts_Bulletins__c==True && regCurrAgMap.get(conId).contains('Parts')){partsSalesServSet ='Parts';}
                    if(usr.CC_View_Parts_Pricing__c==True && regCurrAgMap.get(conId).contains('Parts Pricing')){partsSalesServSet ='Parts Pricing';}
                    if(usr.CC_View_Sales_Bulletins__c==True && regCurrAgMap.get(conId).contains('Sales')){partsSalesServSet ='Sales';}
                    if(usr.CC_View_Sales_Pricing__c==True && regCurrAgMap.get(conId).contains('Sales Pricing')){partsSalesServSet ='Sales Pricing';}
                    if(usr.CC_View_Service_Bulletins__c ==True && regCurrAgMap.get(conId).contains('Service & Warranty')){partsSalesServSet ='Service & Warranty';}
                    system.debug('email value'+cntDocMap.get(conId).CC_DoNotSendEmail__c);
                    if(regCurrAgMap.get(conId).contains(usr.Account.CC_Global_Region__c) && cntDocMap.get(conId).CC_DoNotSendEmail__c !='1' && regCurrAgMap.get(conId).contains(usr.Account.CC_Bulletins_Currency__c)){
                      if((partsSalesServSet =='Parts' && usr.CC_View_Parts_Bulletins__c==True) ||
                         (partsSalesServSet =='Sales' && usr.CC_View_Sales_Bulletins__c==True) ||
                         (partsSalesServSet =='Parts Pricing' && usr.CC_View_Parts_Pricing__c==True) ||
                         (partsSalesServSet =='Sales Pricing' && usr.CC_View_Sales_Pricing__c==True) ||
                         (partsSalesServSet =='Service & Warranty' && usr.CC_View_Service_Bulletins__c==True) ||
                         (regCurrAgMap.get(conId).contains('Digest')) //MMattawar: 13Nov 2018: RT 8015920: Added to send email to all users for Bulletin Type = Digest.
                         ){
                      //usr.Account.CC_Bulletins_Currency__c.contains(stSet)
                           stSet.add(usr.Id);
                      }
                    }
                }
                 system.debug('$$$stSet'+stSet);
                 system.debug('user size'+stSet.size());
                 contDocAndUserMap.put(conId,stSet);
             }
            system.debug('%%%contDocAndUserMap'+contDocAndUserMap);
            system.debug('#####accId'+accId);
            system.debug('#####accId size'+accId.size());
            if(accId.size()>0)
            cntList = [Select Id, AccountId, CC_Type__c, CC_Sub_Type__c from Contract 
                                                       where AccountId IN : accId and 
                                                       CC_Type__c = 'Dealer/Distributor Agreement' and CC_Contract_Status__c = 'Current' and CC_Contract_End_Date__c >=:system.today()];
                                                      // and CC_Sub_Type__c IN : agreementSet];
             
              system.debug('****cntList **'+cntList);
            
            for(Id contD : regCurrAgMap.keyset()){
              String subType='';
              Set<String> subTypeSet = new Set<String>();
              for(Contract c : cntList){
                if(regCurrAgMap.get(contD).contains(c.CC_Sub_Type__c)){
                  //subType += c.CC_Sub_Type__c + ';';
                  contractidSet.add(contD);
                  subTypeSet.add(c.CC_Sub_Type__c);              
                }
              }
              
              system.debug('@@@contractidSet'+contractidSet);
              system.debug('@@@subTypeSet'+subTypeSet);
              for(String s : subTypeSet){
                  subType += s+';';
                }
              contractCntDocMap.put(contD,subType);
            }
            
            system.debug('@@@contractCntDocMap after cntlist'+contractCntDocMap);
            
            system.debug('@@@contDocAndUserMap'+contDocAndUserMap);
            
            for(Id conkey : contDocAndUserMap.keyset()){
            
             system.debug('!!!!conkey'+conkey);
             
                if(contractidSet.contains(conkey)){
            //Create new records for the object CC_Content_Publish_Email__c
                        for(Id userId : contDocAndUserMap.get(conkey)){
                         system.debug('!!!!userId '+userId );
                         
                          CC_Content_Publish_Email__c cPE = new CC_Content_Publish_Email__c();
                          cPE.Name = 'CPE';
                          cPE.CC_Content_Document_Id__c=conkey;
                          cPE.CC_Agreement_Type__c=contractCntDocMap.get(conkey);
                          cPE.CC_Send_Email_To__c=userId;
                          cPE.CC_Content_Document_Record_Type_Name__c=cntDocMap.get(conkey).RecordType.Name;
                          if(cntDocMap.get(conkey).Bulletin_Title__c!=null){cPE.CC_Bulletin_Title__c=cntDocMap.get(conkey).Bulletin_Title__c;}                      
                          if(cntDocMap.get(conkey).CC_Bulletin_Type__c!=null){cPE.CC_Bulletin_Type__c=cntDocMap.get(conkey).CC_Bulletin_Type__c;}
                          if(cntDocMap.get(conkey).Bulletin_Number__c!=null){cPE.CC_Bulletin_Number__c=cntDocMap.get(conkey).Bulletin_Number__c;}
                          cPEInsertlist.add(cPE);
                         
                        }
                        cntDocMap.get(conkey).CC_DoNotSendEmail__c='1';
                        updateCnvList.add(cntDocMap.get(conkey));
                        
                    }
                     
               } 
               system.debug('!!!!updateCnvList'+updateCnvList);
               system.debug('!!!!cPEInsertlist'+cPEInsertlist.size());
               //inser new Content Publish Email records
                if(cPEInsertlist.size()>0){insert cPEInsertlist;}
                system.debug('Records Create '+cPEInsertlist.size()+''+cPEInsertlist);
                if(updateCnvList.size()>0){update updateCnvList;}    
                
        }
        catch(Exception e){
            system.debug('****Exception at***:'+e.getStackTraceString());
        }
  }  
 }