/*************************************
* CTS_ArticleSearchController
* Controller class for Knowledge Article search
**************************************/
public class CTS_ArticleSearchController {
    /**
    * Knowledge Article search 
    * @param keyword, selectedProduct, selectedCategory, lang, type
    * @return List
    */
    @AuraEnabled
    public static List<ObjectWrapper> search(String keyword, String selectedProduct, String selectedCategory, String lang, String type) {        
        
        List<ObjectWrapper> queryResults = new List<ObjectWrapper>();        
        List<String> objectNames = new List<String>{CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT};
        Map<String, List<String>> fieldMap = CTS_ArticleSearchConfig.getFields(objectNames, true);        
        Map<String, List<CTS_Article_Search_Filter_Config__mdt>> filterMap = CTS_ArticleSearchConfig.getFilterConfig(objectNames, CTS_ArticleSearchConstants.AUTO);
        CTS_Article_Search_Config__c searchConfigSetting = CTS_ArticleSearchConfig.getSearchConfig();
        
        String queryType = String.isBlank(keyword) ? 'SOQL' : 'SOSL';
        
        String kbQuery =  CTS_ArticleSearchHelper.buildKnowledgArticleQuery(
            fieldMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT), 
            filterMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT),
            selectedProduct, selectedCategory, lang, queryType, type
        );
        
        String q = queryType == 'SOQL' ? kbQuery : 'FIND \'' + keyword + '*\' IN ALL FIELDS RETURNING ' + kbQuery;
        q = q.trim().removeEnd(',');
        System.debug('q****'+q);
        
        try {            
            List<SObject> result = queryType == 'SOSL' ? Search.query(q)[0] : Database.query(q);            
            if(result.size() > 0 && !result.isEmpty()) {
                String objectType;
                for(Sobject sobj : result) {
                    ObjectWrapper objWrapper = new ObjectWrapper();
                    objWrapper.sobj = sobj;
                    objWrapper.searchType = sobj.getSObjectType().getDescribe().getName();
                    if(objWrapper.searchType == 'Knowledge__kav') {
                        objWrapper.iconName = 'standard:knowledge';
                        if(objectType == null){
                            objectType = CTS_ArticleSearchConstants.KNOWLEDGE;
                        }
                    }                  
                    queryResults.add(objWrapper);
                }
            }
            return queryResults;
        } catch(Exception e) {
            System.debug('e**' +e.getMessage());
            return null;
        }
    }
    
    /**
    * Load Search Config Details
    * @return SearchConfigWrapper
    */
    @AuraEnabled
    public static SearchConfigWrapper getSearchConfigDetails() {
        CTS_Article_Search_Config__c searchConfigSetting = CTS_ArticleSearchConfig.getSearchConfig();
        return new SearchConfigWrapper(Integer.valueOf(searchConfigSetting.Default_Page_size__c), searchConfigSetting.Show_Search_Filter_Options__c, searchConfigSetting.Available_Options_In_The_Page_Size__c, searchConfigSetting.Record_Type__c);
    }
    
    /**
    * Load searchFilters
    * @return List<Object>
    */
    @AuraEnabled
    public static List<Object> getFilters() {
        Schema.DescribeFieldResult fieldResult = Knowledge__kav.Language.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<LabelValue> options = new List<LabelValue>();         
        LabelValue option;
        for(Schema.PicklistEntry f :ple) {
            Boolean selected = f.getValue() == UserInfo.getLanguage() ? true : false;
            option = new LabelValue(f.getLabel(), f.getValue(), selected);
            options.add(option);
        }
        List<Object> result = new List<Object>();        
        result.addAll(CTS_DataCategoryGroups.getDescribeDataCategoryGroupStructureResults());
        result.add(JSON.serialize(options));
        result.add(JSON.serialize([SELECT Id, Name, Email, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()][0]));
        return result;
    }
    
    /**
    * Save Search Keyword from Community Knowledge Search
    * @return String <record id>
    */
    @AuraEnabled
    public static String saveSearchKeyword(String term, Double count, String product, 
                                           String subProduct, String category, String lang, String type) {
                                               CTS_Knowledge_Search_Statistics__c stat = new CTS_Knowledge_Search_Statistics__c ();
                                               stat.Keywords__c = String.isBlank(term) ? term : term.removeEnd(';');
                                               stat.Article_Count__c = count;
                                               stat.Product__c = product;
                                               stat.Sub_Product__c = subProduct;
                                               stat.Category__c = category;
                                               stat.Language__c = lang;
                                               stat.Type__c = type;
                                               
                                               List<User> u = [SELECT Name, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
                                               stat.Account__c = u[0].AccountId; //"u" will never be null. Account ID can be, which is Ok
                                               
                                               try {
                                                   insert stat;
                                                   return stat.Id;
                                               } catch(Exception e) {
                                                   System.debug('e**' + e.getMessage());
                                                   return 'error: could not log the keyword.';
                                               }		
                                           }
    
    /**
    * update ViewStat of opened article
    * @param String recordId
    * @return Knowledge__kav
    */
    @AuraEnabled
    public static Knowledge__kav updateViewStat(String recordId) {
        try {
            Knowledge__kav kb = [SELECT Title FROM Knowledge__kav WHERE Id =: recordId UPDATE VIEWSTAT];
            return kb;
        } catch(Exception e) {
            return null;            
        }
    }
    
    /**
    * get associated products data category for the article
    * @param String recordId
    * @return Boolean
    */
    @AuraEnabled
    public static Knowledge__kav getKBWProdDataCats(String recordId) {        
        
        List<String> objectNames = new List<String>{CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT};
        List<String> fieldList = CTS_ArticleSearchConfig.getFields(objectNames, false).get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT);
        
        try {
            String q = '';
            for(String s: fieldList) {
                q += s + ',';                
            }
            q = q.removeEnd(',');
            q = 'SELECT ' + q;
            q += ', (SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections)';
            q += ' FROM Knowledge__kav WHERE Id = :recordId';
            
            Knowledge__kav kb = (Knowledge__kav) (Database.query(q)[0]);
            return kb;
        } catch(Exception e) {
            System.debug('e**' + e.getMessage());
            return null;            
        }
    }
    
    /**
    * Get articles based on SOQL for the selected tab's criteria
    * @param String parameter
    * @return List<ObjectWrapper> [article results]
    */
    @AuraEnabled
    public static List<ObjectWrapper> getArticlesOnLoad(String parameter){
        Set<Id> parentSet = new Set<Id>();
        Set<Id> parentVoteSet = new Set<Id>();
        
        String lang = UserInfo.getLanguage();
        List<String> objectNames = new List<String>{CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT};
        Map<String, List<String>> fieldMap = CTS_ArticleSearchConfig.getFields(objectNames, true);
        Map<String, List<CTS_Article_Search_Filter_Config__mdt>> filterMap = CTS_ArticleSearchConfig.getFilterConfig(objectNames, CTS_ArticleSearchConstants.AUTO);
        CTS_Article_Search_Config__c searchConfigSetting = CTS_ArticleSearchConfig.getSearchConfig();
        
        String kbQuery =  '';
        if(!String.isBlank(parameter)) {
            if(parameter.equals('LastModifiedDate')){
                kbQuery = CTS_ArticleSearchHelper.buildKnowledgArticleQuery(
                    fieldMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT), 
                    filterMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT),
                    null, null, lang, 'SOQL', parameter, null, null
                );
            } else if(parameter.equals('ArticleTotalViewCount')) {
                Integer minViewCount = searchConfigSetting.Min_View_Count__c.intValue();
                List<Knowledge__ViewStat > viewStatlst = [SELECT Id, ParentId FROM Knowledge__ViewStat WHERE ViewCount >= :minViewCount];
                for(Knowledge__ViewStat viewStat: viewStatlst){
                    parentVoteSet.add(viewStat.ParentId);
                }
                kbQuery = CTS_ArticleSearchHelper.buildKnowledgArticleQuery(
                    fieldMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT), 
                    filterMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT),
                    null, null, lang, 'SOQL', null, 'knowledgeArticleId IN :parentVoteSet', null
                );
            } else if(parameter.equals('mostHelpfulArticles')){
                List<Knowledge__VoteStat > votStatlst = [SELECT Id, ParentId FROM Knowledge__VoteStat WHERE NormalizedScore >= :searchConfigSetting.Vote_Stat_Min_Score__c];
                for(Knowledge__VoteStat votStat: votStatlst){
                    parentVoteSet.add(votStat.ParentId);
                }
                kbQuery = CTS_ArticleSearchHelper.buildKnowledgArticleQuery(
                    fieldMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT), 
                    filterMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT),
                    null, null, lang, 'SOQL', null, 'knowledgeArticleId IN :parentVoteSet', null
                );
            } else {
                List<EntitySubscription> eSublst = [SELECT ParentId 
                                                    FROM EntitySubscription 
                                                    WHERE SubscriberId =: UserInfo.getUserId()
                                                    AND Parent.Type = 'Knowledge__ka'
                                                    LIMIT 1000];
                for(EntitySubscription enSub: eSublst){
                    parentSet.add(enSub.ParentId);
                }
                kbQuery = CTS_ArticleSearchHelper.buildKnowledgArticleQuery(
                    fieldMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT), 
                    filterMap.get(CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT),
                    null, null, null, 'SOQL', null, 'knowledgeArticleId IN :parentSet', null
                );
            }
        }
        List<Knowledge__kav> knowledgeLst = Database.query(kbQuery);
        List<ObjectWrapper> kObjectlst = new List<ObjectWrapper>();
        for(Knowledge__kav kObj : knowledgeLst){
            ObjectWrapper kObject = new ObjectWrapper();
            kObject.sobj = kObj;
            kObject.searchType = 'Knowledge__kav';
            kObject.iconName = 'standard:knowledge';
            kObjectlst.add(kObject);
        }
        return kObjectlst;
    }
    
    // Create a wrapper class with @AuraEnabled Properties 
    public class LabelValue {
        public String label;
        public String value;
        public Boolean selected;
        public LabelValue(String l, String v, Boolean s) {
            this.label = l;
            this.value = v;
            this.selected = s;
        }
    }  
    public class ObjectWrapper {
        @AuraEnabled public String searchType {get;set;}
        @AuraEnabled public Sobject sobj {get;set;}
        @AuraEnabled public String iconName {get;set;}    
    }
    
    public class SearchConfigWrapper {
        @AuraEnabled public Integer pageSize{get;set;}
        @AuraEnabled public Boolean showSearchOption {get;set;}
        @AuraEnabled public String pageSizeOptions {get;set;}
        @AuraEnabled public String recordTypeId{get;set;}
        
        public SearchConfigWrapper(){}
        
        public SearchConfigWrapper(Integer pageSize, Boolean showSearchOption, String pageSizeOptions, String recordTypeName){
            this.pageSize = pageSize;
            this.showSearchOption = showSearchOption;
            this.pageSizeOptions = pageSizeOptions;
            this.recordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().containsKey(recordTypeName) ? 
                Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId() : 
            null;
        }
    }
}