public with sharing class CC_TwilioService {

    public class CC_TwilioServiceLimitException extends Exception{}
    private static CC_Twilio_Settings__c settings = CC_Twilio_Settings__c.getOrgDefaults();

    public class TwilioApiClientResponseWrapper{
        public Boolean hasError = false;
        public String errorMessage = '';
    }

    public static TwilioApiClientResponseWrapper sendSMS(String toPhone, String message){

        TwilioApiClientResponseWrapper response = new TwilioApiClientResponseWrapper();

        if (settings != null && String.isNotBlank(settings.Account_SID__c) && String.isNotBlank(settings.From_Phone_Number__c)){

            try{

                // Truncate the message if it exceeds the SMS message size limit of 1600 characters
                if (String.isNotBlank(message) && message.length() > 1600){

                    system.debug('SMS message too long, truncating message..');
                    String truncateMessage = '[MESSAGE_EXCEEDED_MAX_LENGTH_ALLOWED]';
                    message = message.subString(0, 1600 - truncateMessage.length()) + truncateMessage;
                }

                TwilioSF.TwilioApiClient client = new TwilioSF.TwilioApiClient();
                client.addUrlPart('Accounts');
                client.addUrlPart(settings.Account_SID__c);
                client.addUrlPart('Messages.json');
                
                client.addParam('To', toPhone);
                client.addParam('From', settings.From_Phone_Number__c);
                client.addParam('Body', message);
            
                TwilioSF.TwilioApiClientResponse TWresponse = client.doPost();
                system.debug('message send successfull? ' + !TWresponse.hasError());
                system.debug('error message: ' + TWresponse.getErrorMessage());  
                response.hasError = TWresponse.hasError();     
                response.errorMessage = TWresponse.getErrorMessage();
            }
            catch(Exception e){
                system.debug('catch exception: ' + e.getMessage());
                response.hasError = true;     
                response.errorMessage = e.getMessage();
            }
        }

        return response;
    }

    public static String getRecipientMobilePhone(User recipient){

        String phone = '';
                            
        if (recipient.IsPortalEnabled){

            if (String.isNotBlank(recipient.Contact.MobilePhone)){
                phone = recipient.Contact.MobilePhone;
            }                                                                                              
        }
        else if (String.isNotBlank(recipient.MobilePhone)){          
            phone = recipient.MobilePhone;                         
        }

        return phone;
    }
}