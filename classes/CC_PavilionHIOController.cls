/*
 *
 * $Revision: 1.0 $
 * $Date: $
 * $Author: $
 Developer Log:
 --------------------------------------------------
 Name               Date            Description
 Sudeep Kumar       24 Aug 2016     Fix to unlisted agrement types
 
 */
global class CC_PavilionHIOController {

 /* member variables */
 public List < Contract > aggrementType {get;set;} /*used for Marketing Aggrement Type picklist */
 public String UserAccountId {get;set;}
 public List < String > country {get;set;}
 public list<String> vehicleList{get;set;}

 /* constructor */
 public CC_PavilionHIOController(CC_PavilionTemplateController controller) {
   country = new List < String > ();
   Schema.DescribeFieldResult fieldResult = CC_Hole_in_One__c.Country__c.getDescribe();
   List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
   country.add('United States of America');
   String temp = '';
   for (Schema.PicklistEntry f: ple) {
   temp = f.getValue();
   if(temp != 'United States of America')
    country.add(temp);
   }
   UserAccountId =controller.acctId;

   vehicleList = new list<String>();       
   Schema.DescribeFieldResult vehicleResult = CC_Hole_in_One__c.Vehicle__c.getDescribe();
   List < Schema.PicklistEntry > vlist = vehicleResult.getPicklistValues();
   
   for (Schema.PicklistEntry f: vlist) {

    vehicleList.add(f.getValue());
   }
   
  }
  
//method added by tapas for passing the Agrrement Types dynamically via js remote call. Projecet Task 285..
//method added by tapas for passing the Agrrement Types dynamically via js remote call. Projecet Task 285..
    @RemoteAction
      public Static List<Contract> getAggrementTypes() {//String ACCID

          Set<Contract> setContract = new Set<Contract>();
          List<Contract> agreementList= new List<Contract>();

          User U = [select id, ContactId, Contact.Accountid from user where Id = : UserInfo.getUserId()];
          Id accId = U.Contact.Accountid;

          
          For(Contract CCop:[SELECT Id,CC_Sub_Type__c,CC_Type__c
                                                   FROM Contract Where CC_Type__c = 'Dealer/Distributor Agreement' 

                                                   AND AccountId =:accId ]){
                                                   setContract.add(CCop);
                                                }
          System.debug('^^^^^^^^^^^^^^^setContract = '+setContract);
          for(Contract cc: setContract){
              agreementList.add(cc);
          }
          return agreementList;
     }
 
 /*** Remote method to be called from CC_PavilionCoopHIO page via JS Remoting **/
     @RemoteAction
     global static String refreshTableBasedOnAggrement(String selectedAggrementType, String UserAccountId) {
       List < Contract > listContract = new List < Contract > ();
       List < Contract > listContractToParse = new List < Contract > ();
       if (String.isNotBlank(selectedAggrementType)) {
        listContract = [SELECT Id, Name, CC_Contract_End_Date__c, StartDate 
         from Contract where AccountId = : UserAccountId
         AND CC_Type__c = : selectedAggrementType
         AND CC_Contract_End_Date__c > : System.today()
        ];
        System.debug('@@@listContract@@@'+listContract);
       }

       for (Contract objContract: listContract) {
        objContract.CC_Contract_End_Date__c = objContract.CC_Contract_End_Date__c.addHours(-5).addMinutes(-30);
        listContractToParse.add(objContract);
       }
       if (!listContract.isEmpty()) {
        String jsonData = (String) JSON.serialize(listContractToParse);
        System.debug('@@@jsonData@@@'+jsonData);
        return jsonData;
       } else {
        return 'No data found';
       }
      }
  /* Remote method to submit the CC Coop Claim
Remote method to be called from CC_PavilionDealerInvoice page via JS Remoting */
 @RemoteAction
 global static String submitCoopClaim(String nameOfTournament, String courseName, String courseAddress,
   String courseCity, String stateProvince, String countryName,
   String dateFirstTournament, String Vehicle, String noOfDaysVO, String ContractId, String CCJSON
  ) {
   try{
        List<RecordType> rt = [select id, Name from RecordType where SobjectType = 'CC_Hole_in_One__c' and Name = 'Hole in Request'  Limit 1 ];
        List < CC_Hole_in_One__c > lstcoopClaim = new List < CC_Hole_in_One__c > ();
          if (String.isNotEmpty(CCJSON) && ContractId != null) {
             System.debug('@@@@@CCJSON@@@@@@'+CCJSON);
         CoopClaimParser parserData = (CoopClaimParser) JSON.deserializeStrict(CCJSON, CoopClaimParser.class);
            List < CoopClaimItems > hioList = new List < CoopClaimItems > ();
    
          CC_Hole_in_One__c objCoopClaim = new CC_Hole_in_One__c();
          objCoopClaim.Contract__c = ContractId;
          objCoopClaim.Complete_Name_of_Tournament__c = nameOfTournament;
          objCoopClaim.Course_Name__c = courseName;
          objCoopClaim.Course_Address__c = courseAddress;
          objCoopClaim.City__c = courseCity;
          objCoopClaim.State_Province__c = stateProvince;
          objCoopClaim.Country__c = countryName;
          objCoopClaim.Date_of_the_first_day_of_Tournament__c = Date.parse(dateFirstTournament);
          objCoopClaim.Vehicle__c = Vehicle;
          objCoopClaim.Number_of_days_vehicle_will_be_offered__c = noOfDaysVO;
          objCoopClaim.CC_Status__c = 'Pending';
          objCoopClaim.RecordTypeId = rt[0].id;
          
          for(CoopClaimItems coi : parserData.CoopClaimItem){
          hioList.add(coi);
          }
          
          for(Integer i=0; i<hioList.size(); i++){
          
          if(i == 0){
          objCoopClaim.Day__c = hioList[i].Day;
          objCoopClaim.Pro_Am__c = hioList[i].proAm;
          objCoopClaim.Players__c = hioList[i].players;
          objCoopClaim.par__c = hioList[i].par;
          objCoopClaim.hole__c = hioList[i].hole;
          objCoopClaim.round__c = hioList[i].round;
          objCoopClaim.Yards__c = hioList[i].yards;
          }
          
          if(i == 1){
          objCoopClaim.Day1__c = hioList[i].Day;
          objCoopClaim.Pro_Am1__c = hioList[i].proAm;
          objCoopClaim.Player1__c = hioList[i].players;
          objCoopClaim.par1__c = hioList[i].par;
          objCoopClaim.hole1__c = hioList[i].hole;
          objCoopClaim.round1__c = hioList[i].round;
          objCoopClaim.Yards1__c = hioList[i].yards;
          }
            
          if(i == 2){
          objCoopClaim.Day2__c = hioList[i].Day;
          objCoopClaim.Pro_Am2__c = hioList[i].proAm;
          objCoopClaim.Player2__c = hioList[i].players;
          objCoopClaim.par2__c = hioList[i].par;
          objCoopClaim.hole2__c = hioList[i].hole;
          objCoopClaim.round2__c = hioList[i].round;
          objCoopClaim.Yards2__c = hioList[i].yards;
          } 
            
          if(i == 3){
          objCoopClaim.Day3__c = hioList[i].Day;
          objCoopClaim.Pro_Am3__c = hioList[i].proAm;
          objCoopClaim.Player3__c = hioList[i].players;
          objCoopClaim.par3__c = hioList[i].par;
          objCoopClaim.hole3__c = hioList[i].hole;
          objCoopClaim.round3__c = hioList[i].round;
          objCoopClaim.Yards3__c = hioList[i].yards;
          }
        }
          
          SYSTEM.DEBUG(objCoopClaim);
          lstcoopClaim.add(objCoopClaim);
          }
         if (lstcoopClaim.size() > 0) {
          insert lstcoopClaim;
         }
          String holeinName = [SELECT Name FROM CC_Hole_in_One__c WHERE id=:lstcoopClaim[0].id].Name;
          String returnString = 'Your Hole in One request# '+holeinName+' has been submitted.';
          return returnString;
        
        }
        catch(Exception e) {     
        system.debug(e);
        return 'Failure';
        }
      }
  // Wrapper Classes and methods for parse list of CC Coop Claim from json array
 public class CoopClaimParser {
  public List < CoopClaimItems > CoopClaimItem;
 }
 public class CoopClaimItems {
  String Day;
  String proAm;
  String players;
  String par;
  String hole;
  String round;
  String yards;
 }
}