/**
	@author Ben Lorenz
	@date 5AUG2019
	@description Test class for CC_LeadDispositionController.cls
*/

@isTest
private class CC_LeadDispositionControllerTest {
	
    @testSetup
    static void setupData() {
        User u = new User();
        u.LastName = 'Test';
        u.FirstName = 'Test';
        u.Alias = 'test-675';
        u.Email = 'test@test.com.6755';
        u.Username = 'test@test.com.6755';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'][0].Id;
        u.EmailEncodingKey = 'ISO-8859-1';
        insert u;
        
        Lead l = new Lead();
        l.LastName = 'Test';
        l.FirstName = 'Test';
        l.Company = 'Test';
        l.Email = 'test@test.com';
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Club Car New Lead').getRecordTypeId();
        insert l;
    }
    
    @isTest
    static void testDisp() {
        Lead l = [SELECT Id FROM Lead];        
        PageReference pg = Page.CC_LeadDisposition;
        pg.getParameters().put('icon', 'duplicate');
        pg.getParameters().put('lId', l.Id);
        Test.setCurrentPageReference(pg);
        
        Test.startTest();
        CC_LeadDispositionController cnt = new CC_LeadDispositionController();        
        CC_LeadDispositionController.updateLead((String)l.Id, 'duplicate', '', '', '', '12345.0', 'test', 'comments');
        
        pg = Page.CC_LeadDisposition;
        pg.getParameters().put('icon', 'message');
        pg.getParameters().put('lId', l.Id);
        Test.setCurrentPageReference(pg);
        cnt = new CC_LeadDispositionController();
        CC_LeadDispositionController.updateLead((String)l.Id, 'message', '', '3432423', '', '12345.0', 'test', 'comments');
        Test.stopTest();
    }
}