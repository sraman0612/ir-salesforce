public with sharing class CTSCustomerCommunityNewUserFlowService {

    public class RegisterContactRequest{
        @InvocableVariable public Id contactId;
        @InvocableVariable public Id profileId;  
        @InvocableVariable public String email;
        @InvocableVariable public String timezone;
        @InvocableVariable public String mobilePhone;    
        @InvocableVariable public String communityApprovalStatus = 'Internal Submission';  
    }

    // This is designed to register 1 contact at a time, InvocableMethod requires a list input
    @InvocableMethod
    public static void registerUser(List<RegisterContactRequest> requests){

        RegisterContactRequest request = requests[0];

        CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest newUserRequest = new CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest();
        newUserRequest.profileId = request.profileId;
        newUserRequest.phone = request.mobilePhone;
        newUserRequest.timezone = request.timezone;
        newUserRequest.username = request.email;
        newUserRequest.email = request.email;        

        // Defaults from contact record
        Contact contact = [Select Id, FirstName, LastName From Contact Where Id = :request.contactId];
        newUserRequest.contactId = contact.Id;
        newUserRequest.firstName = contact.FirstName;
        newUserRequest.lastName = contact.LastName;
        newUserRequest.communityApprovalStatus = request.communityApprovalStatus;

        CTS_IOT_Data_Service_WithoutSharing.RegisterUserResponse newUserResult = CTS_IOT_Data_Service_WithoutSharing.registerUser(newUserRequest);
        system.debug('newUserResult: ' + newUserResult);
    }
}