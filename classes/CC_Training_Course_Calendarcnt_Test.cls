@isTest
public class CC_Training_Course_Calendarcnt_Test
{
    static testMethod void testPavilionWarrPoliciesCont() 
        
    {
 Id devRecordTypeId = [Select Id From RecordType Where SobjectType = 'CC_Pavilion_Content__c'and DeveloperName = 'Training_Catalog'].id;
        CC_Pavilion_Content__c pc=TestUtilityClass.createCustomPavilionContentHeader('test catalog header','Regional Training');
        pc.RecordTypeId=devRecordTypeId;
        insert pc; 
        System.assertEquals(pc.RecordTypeId,devRecordTypeId);
        CC_PavilionTemplateController controller;
        CC_Training_Course_Calendarcnt courseCalendercnt = new CC_Training_Course_Calendarcnt(controller);
        test.startTest();
        Apexpages.currentpage().getparameters().put('catalogheader',pc.CC_Training_Catalog_Header__c);
        courseCalendercnt.currentcatogory();
        test.stopTest();
    }
    static testMethod void testPavilionWarrPoliciesCont1() 
        
    {
        test.startTest();
        Id devRecordTypeId = [Select Id From RecordType Where SobjectType = 'CC_Pavilion_Content__c'and DeveloperName = 'Training_Catalog'].id;
        CC_Pavilion_Content__c  firstpc=TestUtilityClass.createCustomPavilionContentHeader('test catalog header','Factory Training'); 
        firstpc.RecordTypeId=devRecordTypeId;
        insert firstpc;
        CC_PavilionTemplateController controller;
        CC_Training_Course_Calendarcnt courseCalendercnt = new CC_Training_Course_Calendarcnt(controller);
        Apexpages.currentpage().getparameters().put('catalogheader',firstpc.CC_Training_Catalog_Header__c);
        courseCalendercnt.currentcatogory();
        test.stopTest();
    }
    static testMethod void testPavilionWarrPoliciesCont2() 
        
    { 
        Id devRecordTypeId = [Select Id From RecordType Where SobjectType = 'CC_Pavilion_Content__c'and DeveloperName = 'Training_Catalog'].id;
        CC_Pavilion_Content__c nextpc=TestUtilityClass.createCustomPavilionContentHeader('test catalog header',null);
        nextpc.RecordTypeId=devRecordTypeId;
        insert nextpc;
        CC_PavilionTemplateController controller;
        CC_Training_Course_Calendarcnt courseCalendercnt = new CC_Training_Course_Calendarcnt(controller);
        test.startTest();
        Apexpages.currentpage().getparameters().put('catalogheader',nextpc.CC_Training_Catalog_Header__c);
        courseCalendercnt.currentcatogory();
        test.stopTest();
    }
}