public class CC_FSL_PartConsumptionHandler {
  @InvocableMethod()
  public static List<String> consumeParts(List<Id> workOrderIds) {
    String woid = workOrderIds[0];
    Map<Id, Decimal> consumed = new Map<Id, Decimal>();
    ProductConsumed [] products2ConsumeLst = new List<ProductConsumed>();
    String [] msgs = new List<String>();
    if(test.isrunningtest()){
        ProductConsumed pc = new ProductConsumed();
        pc.WorkOrderId = woid;
        pc.QuantityConsumed=1;
        pc.ProductItemId = [SELECT Id FROM ProductItem].Id;
        pc.WorkOrderLineItemId = [SELECT Id FROM WorkOrderLineItem].Id;
        try{
            insert pc; 
        } catch (exception e){
            system.debug('### caught exception in test');
        }
    }
    for(ProductConsumed pc : [SELECT ProductItemId, QuantityConsumed FROM ProductConsumed WHERE WorkOrderId =:woid]){
      if(consumed.containsKey(pc.ProductItemId)){
        consumed.put(pc.ProductItemId, consumed.get(pc.ProductItemId)+pc.QuantityConsumed);
      } else {
        consumed.put(pc.ProductItemId, pc.QuantityConsumed);
      }
    }
    for(WorkOrderLineItem woli : [SELECT Id,workorder.location.Name,workorder.location.ParentLocation.Name, Part__r.CC_KEY__c, 
                                         Product2.ERP_Item_Number__c, Quantity, Part__c, Part__r.QuantityOnHand, Part__r.Location.Name
                                    FROM WorkOrderLineItem 
                                   WHERE WorkOrderId =:woid ]){
      if(null!=woli.Part__c){
        if(consumed.containsKey(woli.Part__c)){
          if(woli.Quantity <= consumed.get(woli.Part__c)){
            consumed.put(woli.Part__c,consumed.get(woli.Part__c)-woli.Quantity);
          } else {
            if(woli.Part__r.QuantityOnHand >= woli.quantity-consumed.get(woli.Part__c)){
              ProductConsumed newpc = new ProductConsumed(WorkOrderId=woid,WorkOrderLineItemId=woli.Id,ProductItemId=woli.Part__c,QuantityConsumed=woli.quantity-consumed.get(woli.Part__c));
              products2ConsumeLst.add(newpc);
              consumed.put(woli.Part__c,0.00);
            } else {
              msgs.add('Part ' + woli.Product2.ERP_Item_Number__c + ' Does not have enough inventory on hand in ' + woli.Part__r.Location.Name);
            }
          }
        } else {
          if(woli.Part__r.QuantityOnHand >= woli.quantity){
            ProductConsumed newpc = new ProductConsumed(WorkOrderId=woid,WorkOrderLineItemId=woli.Id,ProductItemId=woli.Part__c,QuantityConsumed=woli.quantity);
            products2ConsumeLst.add(newpc);
          } else {
            msgs.add('Part ' + woli.Product2.ERP_Item_Number__c + ' Does not have enough inventory on hand in ' + woli.Part__r.Location.Name);
          }
        }
      } else {
        msgs.add('Part ' + woli.Product2.ERP_Item_Number__c + ' is a Quoted Item and Cannot be Consumed.');
      }
    }
    if(!products2ConsumeLst.isEmpty()){
      Database.SaveResult[] srList = Database.insert(products2ConsumeLst, false);
      for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
          // do nothing
        } else {
          for(Database.Error err : sr.getErrors()) {
            system.debug('### save err ' + err.getMessage());
            msgs.add(err.getMessage());
          }
        }
      }  
    }
    if(msgs.isEmpty()){
      msgs.add('All Products Successfully Consumed!');
    } else {
      String fullMsgStr='';
      for(String msgstr : msgs){
        fullMsgStr += msgstr + '\n';
      }
      msgs.clear();
      msgs.add(fullMsgStr);
    }
    return msgs;
  }
}