/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 08-22-2023
 * @last modified by  : RISHAB GOYAL
**/
@IsTest
public class CaseResponseTriggerHandlerTest {
    
	@IsTest
    public static void copyOnHoldReasonToSubStatusTest() {
        List<Case> c = new List<Case> {
            new Case( Sub_Status__c = 'Waiting on Pricing',
                Product_Category__c = 'Medical' )
        };
        CaseResponseTriggerHandler.copyOnHoldReasonToSubStatus(c);
        system.assert(true, c.get(0).Sub_Status__c == c.get(0).On_Hold_Reason__c);
    }
    
	/*@IsTest
    public static void replyTestThomas() {
        replyTest(Label.Thomas);
    }

 
    
    private static void replyTest(Id queueId){

        List<Case> c = new List<Case> {
            new Case( 
                Sub_Status__c = 'Waiting on Pricing', 
				SuppliedEmail = 'broc.roberts@cognizant.com',
                Product_Category__c = 'Medical',
                OwnerId = queueId
            )
        };
        Test.startTest();
        CaseResponseTriggerHandler.reply(c);
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
       
    }*/

   /* @IsTest
    public static void calculateBusinessHoursTestNA() {
        List<Case> cases = new List<Case> {
            new Case( OwnerId = Label.TriContinent, Sub_Status__c = 'Waiting on Pricing' )
        };
        CaseResponseTriggerHandler.calculateBusinessHours(cases);
        
        for (Case c : [Select Id, Business_Time__c, EightHourSLA__c From Case Where Id in :cases]){
            system.assert(true, c.Business_Time__c != null);
            system.assert(true, c.EightHourSLA__c != null);
        }
    }

    @IsTest
    public static void calculateBusinessHoursTestEurope() {
        List<Case> cases = new List<Case> {
            new Case( OwnerId = Label.TriContinent_EMEIA, Sub_Status__c = 'Waiting on Pricing' )
        };
        
        CaseResponseTriggerHandler.calculateBusinessHours(cases);
        
        for (Case c : [Select Id, Business_Time__c, EightHourSLA__c From Case Where Id in :cases]){
            system.assert(true, c.Business_Time__c != null);
            system.assert(true, c.EightHourSLA__c != null);
        }
    }*/
}