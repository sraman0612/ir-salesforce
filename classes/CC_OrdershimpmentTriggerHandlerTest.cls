@isTest
public class CC_OrdershimpmentTriggerHandlerTest {

    static testmethod void run2condition(){
        
        //creating test Data here
        
        //creating account
        Account acc = TestUtilityClass.createAccount();
        insert acc;
        
        //creating order
        CC_Order__c ord = TestUtilityClass.createCustomOrder(acc.id,'OPEN','N');
        insert ord;
        
        //creating orderitem
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItem(ord.Id);
        insert orditem;
        
        //creating car item
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(orditem.Id);
        carItem.Serial_No_s__c = 'SL1740747483::9;SL1740747485::10;SL1740747488::11';
        carItem.Estimated_Ship_Date__c=System.today()+7;
        insert carItem;
        
         //creating product
        Product2 prod =  TestUtilityClass.createProduct2();
        prod.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Club_Car'].Id;
        insert prod;
        
        //creating car feature
        CC_Car_Feature__c cf =  TestUtilityClass.createCarFeature(carItem.Id,prod.Id,ord.Id);
        insert cf;
        
        //creating invoice
         CC_Invoice2__C inv =TestUtilityClass.createInvoice2(acc.Id,'123');
        insert inv;
        
        //creating order shipment
        CC_Order_Shipment__c ordship = TestUtilityClass.createOrderShipment(ord.id);
        ordship.Car_Assets_Created__c = True;
        ordship.Invoice_Date__c        = date.today();
        ordship.Invoice_Number__c     = '123';
        insert ordship;
        
        //creating order shipment item
        CC_Order_Shipment_Item__c ordshipitem =TestUtilityClass.createOrderShipmentItem(orditem.Id,ordship.Id);
        insert ordshipitem;

        //creating asset here
        CC_Asset__c ast = TestDataUtility.createAsset(acc.Id, prod.Id, null, ord.Id);
        insert ast;
        
        Test.startTest();
        	delete ordship;
        Test.stopTest();
        
        
    }
}