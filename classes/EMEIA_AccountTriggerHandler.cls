/*
 * Revisions:
02-Apr-2024 : Update code against Case #03073249 - AMS Team
*/

public class EMEIA_AccountTriggerHandler {
    
    public static Id BillRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_Bill_To_Account').getRecordTypeId();
    public static Id EURTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
    public static Id EUIRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU_Distributor_End_User').getRecordTypeId();
    public static Id EUINRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU_INDIRECT').getRecordTypeId();
    public static Id MEIARTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA').getRecordTypeId();
    public static Id MEIADRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA_Distributor_End_User').getRecordTypeId();
    public static Id MEIAIRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_MEIA_INDIRECT').getRecordTypeId();  
    public static Id VPTECH_GD_PARENT  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
    public static Id GD_Parent_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent_New').getRecordTypeId();
    public static Id STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Standard_Lead').getRecordTypeId();
    public static Id NEW_STANDARD_LEAD_RT_ID = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('New_Standard_Lead').getRecordTypeId();
    public static Id PT_Power_Tools_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    public static Boolean firstcall=false;
    public static Id New_PTL_Account_Id = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('New_PTL_Account').getRecordTypeId();
 	public static Id PTL_Account_Id = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PTL_Account').getRecordTypeId();
    public static Id JDE_Account_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account').getRecordTypeId();
    public static Id JDE_Account_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account_New').getRecordTypeId();
	public static Id DOSATRON_Account_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dosatron_Account').getRecordTypeId();
    public static Id DOSATRON_Account_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('New_Dosatron_Account').getRecordTypeId();
    public static Id ARO_Account_New_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('New_ARO_Account').getRecordTypeId();
    public static Id ARO_Account_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('ARO_Account').getRecordTypeId();
    
    public static void beforeInsert(List<Account> newacc){
        /**AccountTrigger's before-insert method*/
       
        List<Account> acclist =new List<Account>();
        List<String> parentKeyList = new List<String>();
       
        
       // Set<Id> accountIds = new Set<Id>();
        for(Account acc:newacc){
            parentKeyList.add(acc.Parent_Key__c);
            System.debug('acc.LeadConversion__c >>>'+acc.LeadConversion__c);
        }
        
        for(Account acc:newacc){
            if((BillRTID!=null && acc.RecordTypeId == BillRTID)
               || (EURTID!=null && acc.RecordTypeId == EURTID)
               || (EUIRTID!=null && acc.RecordTypeId == EUIRTID)
               || (EUINRTID!=null && acc.RecordTypeId == EUINRTID)
               || (MEIARTID!=null && acc.RecordTypeId == MEIARTID)
               || (MEIADRTID!=null && acc.RecordTypeId == MEIADRTID)
               || (MEIAIRTID!=null && acc.RecordTypeId == MEIAIRTID))
            {
                CTSUpdateAccountStreetAddress.insertadd(newacc);
            }
            // Capgemini 
            if(UserInfo.getName() == 'CPI Integration'){
                if(acc.SAP_Account_External_ID__c.contains('Parent') && acc.CTS_Channel__c == null){
                    acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
                } 
            }
            
            
        } 
        EMEIA_AccountTriggerHandler.LGDCompressorValidateAccounts(newacc);
        //Calling UpdateAccountDistributorField methos only if Record Type is PTL Account and Account Converted_Lead_ID__c not null (Account getting created from lead conversion)
        //Added Dosatron Condition (CTS Team) ---> Story No. SD-53 
        //Added ARO condition (CTS Team) ---> Story No. SFARO-67
        List<Account> ptlAccList = new List<Account>();
        for(Account acc:newacc){
            if(acc.RecordTypeId==New_PTL_Account_Id || acc.RecordTypeId ==JDE_Account_New_RT_ID || acc.RecordTypeId ==DOSATRON_Account_New_RT_ID || acc.RecordTypeId == ARO_Account_New_RT_ID){
                system.debug('acc.RecordTypeId' +acc.RecordTypeId);
                if(acc.Converted_Lead_ID__c != null) {
                    ptlAccList.add(acc);
                }
            }
        }
        if(ptlAccList.size()!= null ){
           EMEIA_AccountTriggerHandler.UpdateAccountDistributorField(ptlAccList);
       }        
        
    }
    
    public static void beforeUpdate(List<Account> newacc, List<Account> oldacc, Map<Id,Account> oldMap){
        Set<Id> accountIds = new Set<Id>();
        
        for(Account acc:newacc){
            if((BillRTID!=null && acc.RecordTypeId == BillRTID)
               || (EURTID!=null && acc.RecordTypeId == EURTID)
               || (EUIRTID!=null && acc.RecordTypeId == EUIRTID)
               || (EUINRTID!=null && acc.RecordTypeId == EUINRTID)
               || (MEIARTID!=null && acc.RecordTypeId == MEIARTID)
               || (MEIADRTID!=null && acc.RecordTypeId == MEIADRTID)
               || (MEIAIRTID!=null && acc.RecordTypeId == MEIAIRTID))
            {
                CTSUpdateAccountStreetAddress.updateadd(newacc,oldMap);
            }
            
         /* Changes by Capgemini Aman Kumar Date 16/2/2023. START     
            Phone and Billing are default standard mapping which is getting updated on lead conversion for 
            existing SAP accounts, but we can not update address and phone and should be always in Syn with 
            SAP so making these all field blank if getting updated by Lead conversion.*/

            System.debug('acc.LeadConversion__c >>>'+acc.LeadConversion__c);
            System.debug('acc.Phone >>>'+acc.Phone);
            System.debug('oldMap.get(acc.id).phone >>>'+oldMap.get(acc.id).phone);
            if(acc.LeadConversion__c  == 'True' && acc.RecordTypeId == VPTECH_GD_PARENT && UserInfo.getName() != 'CPI Integration' && acc.Type != 'Prospect'){
                if(acc.Phone != oldMap.get(acc.id).phone && (oldMap.get(acc.id).phone == null || oldMap.get(acc.id).phone == '')){
                    acc.Phone = oldMap.get(acc.id).phone;   
                } 
                
                if((acc.BillingCity != oldMap.get(acc.id).BillingCity  && (oldMap.get(acc.id).BillingCity == null || oldMap.get(acc.id).BillingCity == '')) ||
                   (acc.BillingCountry != oldMap.get(acc.id).BillingCountry && (oldMap.get(acc.id).BillingCountry == null || oldMap.get(acc.id).BillingCountry == ''))||
                   (acc.BillingPostalCode != oldMap.get(acc.id).BillingPostalCode && (oldMap.get(acc.id).BillingPostalCode == null || oldMap.get(acc.id).BillingPostalCode == ''))||
                   (acc.BillingState != oldMap.get(acc.id).BillingState && (oldMap.get(acc.id).BillingPostalCode == null || oldMap.get(acc.id).BillingPostalCode == '')) ||
                   (acc.BillingStreet != oldMap.get(acc.id).BillingStreet && (oldMap.get(acc.id).BillingStreet == null || oldMap.get(acc.id).BillingStreet == ''))
                  ){
                      acc.BillingCity = oldMap.get(acc.id).BillingCity ;
                      acc.BillingCountry = oldMap.get(acc.id).BillingCountry ;
                      acc.BillingPostalCode = oldMap.get(acc.id).BillingPostalCode;
                      acc.BillingState = oldMap.get(acc.id).BillingState ;
                      acc.BillingStreet = oldMap.get(acc.id).BillingStreet;   
                  }
            } // Changes by Capgemini Aman Kumar Date 16/2/2023. END 
        }
        
        
        List<Account> accList1=accountTriggerMethod(newacc,null);
        
        if(!accList1.isEmpty()){
            Account_TriggerHandler.beforeUpdate(newacc,oldMap);
            System.debug('im inside account trigger beforeupdate');
        }
        
        EMEIA_AccountTriggerHandler.LGDCompressorValidateAccounts(newacc);
       
    }
    
    public static List<Account> accountTriggerMethod(List<Account> newAcc, List<Account> oldAcc){//newA=trigger.new; oldA=trigger.old
        List<Account> accList = new List<Account>();
         Map<Id, String> accountRTMap = new Map<Id, String>();
         Map<String, List<Account>> SBUTriggerNewMap = new Map<String, List<Account>>();
         Map<String, List<Account>> SBUTriggerOldMap = new Map<String, List<Account>>();                                      
       for (RecordType rt : RecordTypeUtilityClass.getRecordTypeList('Account')){  
            accountRTMap.put(rt.Id,rt.Name);
            SBUTriggerNewMap.put(accountRTMap.get(rt.Id),new List<Account>());
            SBUTriggerOldMap.put(accountRTMap.get(rt.Id),new List<Account>());
       }
        
        if(newAcc!=null){
           for(Account newA:newAcc){
                if(newA.RecordTypeId!=null){
                SBUTriggerNewMap.get(accountRTMap.get(newA.RecordTypeId)).add(newA);
                } 
            } 
        }
        
        if(oldAcc!=null){
            for(Account oldA:oldAcc){
                if(oldA.RecordTypeId!=null){
                     SBUTriggerOldMap.get(accountRTMap.get(oldA.RecordTypeId)).add(oldA);
                }
            }
        }
        
        return accList;
    }
    
    //AccountTrigger
    public static void afterUpdate(List<Account> newA,Map<Id,Account> oldAccountsMap,Map<Id,Account> newAccountsMap){
        List<Account> acc=accountTriggerMethod(newA,null);
        if(!acc.isEmpty())
        {
           Account_TriggerHandler.afterUpdate(acc);
           System.debug('im inside account trigger afterupdate');
        }
        updatePartnerFlagOnContact(newA,oldAccountsMap,newAccountsMap);
        
    }
    
    //AccountTrigger
    public static void afterDelete(List<Account> oldA){
        List<Account> accList=accountTriggerMethod(null,oldA);
        if(!accList.isEmpty()){
            Account_TriggerHandler.afterDelete(accList);
            System.debug('im inside account trigger afterdelete');
        }
    }
    
    public static void afterInsert(List<Account> newA){
        
    List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
    List<Selling__c> sellings = new List<Selling__c>();    
    string currentYear = string.valueof(system.today().year());
    string nextYear = string.valueof((system.today().year())+1);
    PT_Record_types__c ptrt = PT_Record_types__c.getOrgDefaults();
    List<Account> lstAccountForBatch = new List<Account>();
    for(account acc : newA){
        if(acc.recordtypeid == ptrt.Account_RT_Id__c){
            for(string mo : months){
                Selling__c s = new Selling__c(name=mo+' '+currentYear,Account__c=acc.id,PT_Account__c=acc.id,PT_Year__c=currentYear,currencyisocode='EUR');
                sellings.add(s);
                if(system.today().month()>=10){
                    sellings.add(new Selling__c(name=mo+' '+nextYear,PT_Account__c=acc.id,PT_Year__c=nextYear));
                }
            }
            if(acc.PT_BO_Customer_Number__c != null){lstAccountForBatch.add(acc);}
        }
    }    
    if(!sellings.isEmpty()){insert sellings;}
    
    if(!lstAccountForBatch.isEmpty()){
        PT_DSMP_BAT01_createRecordsAccountDVP batch = new PT_DSMP_BAT01_createRecordsAccountDVP(lstAccountForBatch);
        Database.executebatch(batch);
    }
        //updating shipping address from Lead on conversion
        system.debug('newA====='+newA);
        Account_TriggerHandler.updateAccountAddresOnLeadConversion(newA);
        
        
    }
    
    // Changes by Capgemini Devender Singh Date 12/6/2023. START -80
    public static void LGDCompressorValidateAccounts(List<Account> newAcc){
        if(firstcall == false)
        {
         System.debug('Inside LGDCompressorValidateAccounts>>>'+firstcall);
        Boolean isFinlandUser = FeatureManagement.checkPermission('Finland_Sales');
        
        
        List<Id> convertedLeadIds = new List<id>();
        for(Account acc : newAcc){
            convertedLeadIds.add(acc.Converted_Lead_ID__c);
        }
        
        Map<id , Lead> converedLead = new Map<Id, Lead>( [Select id, RecordTypeId,Business__c from Lead Where id IN : convertedLeadIds]);
        
        
        for(Account acc : newAcc){
            // Updated by AMS Team (Mahesh) - #03073249 - 2APR2024;  added 'containsKey' to avoid the null pointer exception
            // Added Dosatron RT Condition(CTS Team - Suyash)---> Story No. SD-53
            
            if(acc.Converted_Lead_ID__c != null && converedLead.containsKey(acc.Converted_Lead_ID__c)){
            if(!isFinlandUser && (acc.RecordTypeId == VPTECH_GD_PARENT  || acc.RecordTypeId == GD_Parent_New_RT_ID ||  acc.RecordTypeId == PTL_Account_Id || acc.RecordTypeId == New_PTL_Account_Id || 
                                  acc.RecordTypeId == JDE_Account_RT_ID || acc.RecordTypeId == JDE_Account_New_RT_ID || acc.RecordTypeId == DOSATRON_Account_New_RT_ID || acc.RecordTypeId == DOSATRON_Account_RT_ID || acc.RecordTypeId == ARO_Account_New_RT_ID ||acc.RecordTypeId == ARO_Account_RT_ID ) 
               && converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId !=STANDARD_LEAD_RT_ID  
               && converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId !=NEW_STANDARD_LEAD_RT_ID){
                   acc.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Account_Message_Label);   
               }
            else if((acc.RecordTypeId != VPTECH_GD_PARENT  && acc.RecordTypeId != GD_Parent_New_RT_ID && acc.RecordTypeId != PT_Power_Tools_ID && acc.RecordTypeId != PTL_Account_Id && acc.RecordTypeId != New_PTL_Account_Id) 
                    && (converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                        || converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                    && converedLead.get(acc.Converted_Lead_ID__c).Business__c  == 'Compressor'  ) {
                               acc.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Account_Message_Label);
                    }
                //Milton Roy Change
               else if((acc.RecordTypeId != JDE_Account_RT_ID && acc.RecordTypeId != JDE_Account_New_RT_ID ) 
                    && (converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                        || converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                   && converedLead.get(acc.Converted_Lead_ID__c).Business__c  == 'Milton Roy'  ) {
                     acc.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Account_Message_Label);
                    } 
                //Dosatron Change (CTS Team)  ---> Story No. SD-53
                else if((acc.RecordTypeId != DOSATRON_Account_RT_ID && acc.RecordTypeId != DOSATRON_Account_New_RT_ID ) 
                    && (converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                        || converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                   && converedLead.get(acc.Converted_Lead_ID__c).Business__c  == 'Dosatron'  ) {
                     acc.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Account_Message_Label);
                    }  
                //ARO Change (CTS Team) --->Story No. SFARO-120
                 else if((acc.RecordTypeId != ARO_Account_RT_ID && acc.RecordTypeId != ARO_Account_New_RT_ID ) 
                    && (converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==STANDARD_LEAD_RT_ID  
                        || converedLead.get(acc.Converted_Lead_ID__c).RecordTypeId ==NEW_STANDARD_LEAD_RT_ID)
                   && converedLead.get(acc.Converted_Lead_ID__c).Business__c  == 'ARO'  ) {
                     acc.addError(System.Label.L_GD_Compressor_Lead_Conversion_to_New_Account_Message_Label);
                    }
        }
        }
        firstcall = true;
    }
    }
        // Changes by Capgemini Devender Singh Date 12/6/2023. END -80
        
    //Created Date -4/2/2024
//created By- Snehal M
//created method UpdateAccountDistributorField to populate distribitor fields on account when account getting created from lead conversion
   

    public static void UpdateAccountDistributorField(List<Account> newacc){
        List<Id> convertedLeadIds = new List<id>();
        try{
        if(newacc.size() != null){
            for(Account acc : newacc){
                if(acc.Converted_Lead_ID__c != null){
                    convertedLeadIds.add(acc.Converted_Lead_ID__c);
                    system.debug('convertedLeadIds'+convertedLeadIds);
                }
            }
        }
        Map<id , Lead> convertedLeadMap = new Map<Id, Lead>( [Select id,PTL_Distributor__c,Indirect_Lead__c,Milton_Roy_Distribution_Account__c from Lead Where id IN : convertedLeadIds]);
        // List<Lead> converetedLead = new List<Lead>([Select id,PTL_Distributor__c,Indirect_Lead__c from Lead Where id IN : convertedLeadIds]);
        System.debug('convertedLeadMap'+convertedLeadMap);
        if(newacc.size() != null){
            for(Account acc :newacc){
                if(acc.RecordTypeId == New_PTL_Account_Id){
                acc.PTL_Distributor__c = convertedLeadMap.get(acc.Converted_Lead_ID__c).PTL_Distributor__c;
                acc.Distributor_End_User__c = convertedLeadMap.get(acc.Converted_Lead_ID__c).Indirect_Lead__c ;
                }
                //Added Dosatron RT Condition (CTS Team - Suyash) ---> Story No. SD-53
                //Added ARO RT Condition (CTS Team - Sairaman) ---> Story No. SFARO-67
                else if((acc.RecordTypeId == JDE_Account_New_RT_ID || acc.RecordTypeId == DOSATRON_Account_New_RT_ID || acc.RecordTypeId == ARO_Account_New_RT_ID) && convertedLeadMap.get(acc.Converted_Lead_ID__c).Indirect_Lead__c == true){
                acc.Milton_Roy_Channel_Partner_Account__c = convertedLeadMap.get(acc.Converted_Lead_ID__c).Milton_Roy_Distribution_Account__c;
                acc.Distributor_End_User__c = convertedLeadMap.get(acc.Converted_Lead_ID__c).Indirect_Lead__c ;
                acc.CTS_Channel__c ='DISTRIBUTOR';    
                }
                system.debug('Account converted from lead' +acc);
            }
        }
    }
        catch(Exception e){
            system.debug('The following exception has occurred: ' + e.getMessage()+e.getLineNumber());
        }        
    }
    public static void updatePartnerFlagOnContact(List<Account> newA,Map<Id,Account> oldAccountsMap,Map<Id,Account> newAccountsMap){
         List<Account> accsList = new List<Account>();
        for(Account acc1 : newA){
            if(acc1.RecordTypeId == PTL_Account_Id && oldAccountsMap.get(acc1.Id).isPartner == true && newAccountsMap.get(acc1.Id).isPartner == false){
                accsList.add(acc1);
            }
        }
        if(accsList != null){

        List<Contact> contactsList = new List<Contact>([SELECT Id,is_PTL_Partner_User__c FROM Contact WHERE AccountId IN :accsList]);
        if(contactsList != null){

        for(Contact con : contactsList){
                con.is_PTL_Partner_User__c = false;
            }
            System.debug('Contacts List before updating to false : ' + contactsList);
            update contactsList;
        }
    }
  }
}