/* 
 *  Snehal Chabukswar || Date 01/07/2020 
 *  Called from Developer Console
 *  Used one time for historical closed case caseAge update
 *  Otherwise called from CTS_OM_CalculateOpenCaseAgeScheduler for Open case caseAge update
 *  Batch Size should be 25 only
*/
Global class CTS_OM_CalculateOpenCaseAge implements Database.Batchable<sObject>  {
    public String businessHoursId{get;set;}
    public Boolean isClosed{get;set;}
    //Constructor to initialize Business Hours Id
    //@param businessHours Id specifies null if need to run batch class for all business hour cases
    //@param isCaseClosed specifies if case is  closed or not
    public CTS_OM_CalculateOpenCaseAge(String businessHours, Boolean isCaseClosed){
        businessHoursId = businessHours;
        isClosed = isCaseClosed;
    }
    
    //Batch Start method query all OM Close Cases 
    //@param Database.BatchableContext 
	global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT CaseNumber,BusinessHoursId,Id,IsClosed,CreatedDate,ClosedDate,Case_Age_Business_Hours__c'+
                       ' FROM Case'+ 
                       ' WHERE (RecordType.DeveloperName LIKE \'CTS_OM%\''+
                       'OR RecordType.DeveloperName =\'CTS_LATAM_Brazil_Application_Engineering\')'+
                       'AND isClosed = '+isClosed; 
        if(businessHoursId != null){
            query = query+' AND BusinessHoursId=\''+businessHoursId+'\' ';
        }
        return Database.getQueryLocator(query);
    }
    //Batch execute method update case age based on coresponding Business Hours on Cases 
    //@param Database.BatchableContext
    //@param caseList 
    global void execute(Database.BatchableContext bc, List<Case>caseList){
        List<Case>updatedCaseList = New List<Case>();
        for(Case caseObj:caseList){
            if(caseObj.IsClosed){
                //calculate case age in ms for closed cases
                Long diffMs= BusinessHours.diff(caseObj.BusinessHoursId,caseObj.CreatedDate,caseObj.ClosedDate);
                Double hours = diffMs/3600000.00;
                caseObj.Case_Age_Business_Hours__c = BusinessHours.diff(caseObj.BusinessHoursId,caseObj.CreatedDate,caseObj.ClosedDate)/3600000.00;    
            }else{
                //calculate case age in ms for Open cases
                Long diffMs= BusinessHours.diff(caseObj.BusinessHoursId,caseObj.CreatedDate,system.now());
                Double hours = diffMs/3600000.00;
                caseObj.Case_Age_Business_Hours__c = hours; 
            }
			
            updatedCaseList.add(caseObj);
        }
        Database.update(updatedCaseList,false);
 
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }
}