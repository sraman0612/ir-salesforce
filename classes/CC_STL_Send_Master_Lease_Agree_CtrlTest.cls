@isTest
public with sharing class CC_STL_Send_Master_Lease_Agree_CtrlTest {
        
        @TestSetup
        private static void createData(){
            
            Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
            templates.CC_Master_Lease_Template_ID__c = 'ABC123';
            upsert templates;
            
            List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
            psettingList = TestUtilityClass.createPavilionSettings();        
            insert psettingList;     
            
            TestDataUtility dataUtil = new TestDataUtility(); 
            
            User testUser = dataUtil.createIntegrationUser();
            testUser.LastName = 'Test_User_123';
            insert testUser;             
            
            Account acct = [Select Id From Account LIMIT 1];
            CC_Order__c ccOrder = TestUtilityClass.createNatAcctOrder(acct.id, acct.Id);
            insert ccOrder;
            
            Product2 prod1 = TestDataUtility.createProduct('Club Car 1');
            prod1.CC_Item_Class__c = 'LCAR';
            Product2 prod2 = TestDataUtility.createProduct('Club Car 2');
            prod2.CC_Item_Class__c = 'LCAR';     
            insert new Product2[]{prod1, prod2};
                
            CC_Sales_Rep__c salesRep = TestUtilityClass.createSalesRep('12345_abc123', testUser.Id);
            insert salesRep;                    
                
            CC_Master_Lease__c masterLease = TestDataUtility.createMasterLease();
            masterLease.Customer__c = acct.Id;
            insert masterLease; 	
        }
        
        @isTest
        private static void testBuildAndSend(){
            
            CC_Master_Lease__c masterLease = [Select Id, Name From CC_Master_Lease__c];
            
            ApexPages.currentPage().getParameters().put('id', masterLease.Id);
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(masterLease);
            CC_STL_Send_Master_Lease_Agreement_Ctrl ctrl = new CC_STL_Send_Master_Lease_Agreement_Ctrl(stdCtrl);	
            
            PageReference sendResult;
            
            Test.startTest();
            
            sendResult = ctrl.buildAndSend();
            
            Test.stopTest();
            
            // Verify the results
            Adobe_Sign_Templates__c templates = Adobe_Sign_Templates__c.getOrgDefaults();
            system.assertEquals('/apex/echosign_dev1__AgreementTemplateProcess?masterid=' + masterLease.Id + '&templateId=' + templates.CC_Master_Lease_Template_ID__c, sendResult.getUrl());
            
            Attachment att = [Select Name, IsPrivate From Attachment Where ParentId = :masterLease.Id];
            system.assert(att.Name.contains('Master Lease Agreement'));	
            system.assertEquals(false, att.IsPrivate);	
        }    
    }