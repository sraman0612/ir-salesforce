@isTest
public class ccp_cloneKnowledgeArticleControllerT {
	
    static User setupData() {        
        
        Id profileId = [Select Id From Profile Where Name = 'System Administrator' LIMIT 1].Id;
        Id roleId = [Select Id From UserRole Where Name = 'CTS TechDirect BU Global' LIMIT 1].Id;
        
        User u = TestUtilityClass.createNonPortalUser(profileId);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            u.Username += '1';
            u.FederationIdentifier = '1234560';
            u.DB_Region__c = 'EMEIA';
            u.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';
            u.UserRoleId = roleId;
            insert u;   
        }
        return u;
    }
    
    public static testMethod void test_success_getArticleInfo()
    {
        User u = ccp_cloneKnowledgeArticleControllerT.setupData();
        
        Schema.SObjectType sobjectType = getArticleSObjectType();
        System.assertNotEquals(null, sobjectType);
        SObject article = sobjectType.newSObject();
        article.put('Title','testTitle');
        article.put('UrlName','testTitle');
        article.put('CTS_TechDirect_Author_Reviewer__c',u.Id);
        article.put('RecordTypeID','0120c000001luKLAAY');
        
        System.runAs(new User(Id = u.Id)) {
        	insert article;
        }
        
        Map<String,Object> mapOfObjectsArticleInfo = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.getArticleInfo(article.Id)
            );
        Boolean isCompatible = (Boolean) mapOfObjectsArticleInfo.get('isCompatible');
        System.assertEquals(true,isCompatible);
    }
    
    public static testMethod void test_success_cloneArticle()
    {
        User u = ccp_cloneKnowledgeArticleControllerT.setupData();
        
        Schema.SObjectType sobjectType = getArticleSObjectType();
        System.assertNotEquals(null, sobjectType);
        SObject article = sobjectType.newSObject();
        article.put('Title','testTitle');
        article.put('UrlName','testTitle');
        article.put('CTS_TechDirect_Author_Reviewer__c',u.Id);
        
        System.runAs(new User(Id = u.Id)) {
        	insert article;
        
        Map<String,Object> mapOfObjectsCloneArticle = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.cloneArticle(article.Id,'testTitle2','testTitle2','0120c000001luKLAAY',False)
            );
        
        String newRecordId = (String) mapOfObjectsCloneArticle.get('newRecordId');
        //System.assertNotEquals(null, newRecordId);
        }
    }
    
    public static testMethod void test_success_cloneArticle2()
    {
        User u = ccp_cloneKnowledgeArticleControllerT.setupData();
        
        Schema.SObjectType sobjectType = getArticleSObjectType();
        System.assertNotEquals(null, sobjectType);
        SObject article = sobjectType.newSObject();
        article.put('Title','testTitle');
        article.put('UrlName','testTitle');
        article.put('CTS_TechDirect_Author_Reviewer__c',u.Id);
        
        System.runAs(new User(Id = u.Id)) {
        	insert article;
        
        
            Knowledge__kav kb = [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE Id =: (Id) article.get('Id')];
            
            KbManagement.PublishingService.publishArticle(kb.KnowledgeArticleId, true);
            
            Map<String,Object> mapOfObjectsCloneArticle = (Map<String,Object>) System.JSON.deserializeUntyped(
                ccp_cloneKnowledgeArticleController.cloneArticle(article.Id,'testTitle2','testTitle2','0120c000001luKLAAY',True)
                );
            String newRecordId = (String) mapOfObjectsCloneArticle.get('newRecordId');
            //System.assertNotEquals(null, newRecordId);
        }
    }
    
    public static testMethod void test_fail_getArticleInfo()
    {
        Map<String,Object> mapOfObjectsArticleInfo = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.getArticleInfo(System.UserInfo.getUserId())
            );
        Boolean isCompatible = (Boolean) mapOfObjectsArticleInfo.get('isCompatible');
        System.assertEquals(false,isCompatible);
    }
    
    public static testMethod void test_fail_getArticleInfo2()
    {
        Map<String,Object> mapOfObjectsArticleInfo = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.getArticleInfo('bogusrecordid')
            );
        Boolean isCompatible = (Boolean) mapOfObjectsArticleInfo.get('isCompatible');
        System.assertEquals(false,isCompatible);
    }
    
    public static testMethod void test_fail_getArticleInfo3()
    {
        Map<String,Object> mapOfObjectsArticleInfo = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.getArticleInfo(null)
            );
        Boolean isCompatible = (Boolean) mapOfObjectsArticleInfo.get('isCompatible');
        System.assertEquals(false,isCompatible);
    }
    
    public static testMethod void test_fail_cloneArticle()
    {
        Map<String,Object> mapOfObjectsCloneArticle = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.cloneArticle(System.UserInfo.getUserId(), null, null, null, null)
            );
        System.debug('mapOfObjectsCloneArticle: ' + mapOfObjectsCloneArticle);
        String errorMsg = (String) mapOfObjectsCloneArticle.get('errorMsg');
        System.assertNotEquals(null, errorMsg);
    }
    
    public static testMethod void test_fail_cloneArticle2()
    {
        Map<String,Object> mapOfObjectsCloneArticle = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.cloneArticle('bogusrecordid', null, null, null, null)
            );
        System.debug('mapOfObjectsCloneArticle: ' + mapOfObjectsCloneArticle);
        String errorMsg = (String) mapOfObjectsCloneArticle.get('errorMsg');
        System.assertNotEquals(null, errorMsg);
    }
    
    public static testMethod void test_fail_cloneArticle3()
    {
        Map<String,Object> mapOfObjectsCloneArticle = (Map<String,Object>) System.JSON.deserializeUntyped(
            ccp_cloneKnowledgeArticleController.cloneArticle(null, null, null, null, null)
            );
        System.debug('mapOfObjectsCloneArticle: ' + mapOfObjectsCloneArticle);
        String errorMsg = (String) mapOfObjectsCloneArticle.get('errorMsg');
        System.assertNotEquals(null, errorMsg);
    }
    
    private static Schema.SObjectType getArticleSObjectType()
    {
        Schema.SObjectType result;
        Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe(); 
            
        //Loop through all the sObject types returned by Schema
        for(Schema.SObjectType stype : gd.values()){
            Schema.DescribeSObjectResult r = stype.getDescribe();
            String sobjectName = r.getName();
            if(sobjectName.endsWithIgnoreCase('__kav'))
            {
                result = stype;
                return result;
            }            
        }        
        return result;        
    }    
}