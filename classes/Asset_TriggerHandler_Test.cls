//
// (c) 2015, Appirio Inc.
//
// Test Class for Asset_TriggerHandler class
//
// Sept 3'15    Surabhi Sharma    
//
@isTest
private class Asset_TriggerHandler_Test {
    
    // Method to test account trigger
    static testMethod void testAssetTrigger() {
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_OEM_EU' 
                                    and SobjectType='Account' limit 1].Id;
        
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'testCounty';
        acc.Division__c = 'San Francisco Customer Center';
        acc.recordtypeid=AirNAAcctRT_ID;
        insert acc;
        
        Product2 prod = new Product2();
        prod.Name = 'testProduct';
        prod.productcode='RTR2000';
        insert prod;
        
        District_Division__c disDiv = new District_Division__c();
        disDiv.Name = '1';
        disDiv.Division__c = 'San Francisco Customer Center';
        disDiv.District__c = 'Bay Area';
        insert disDiv;

        
        Id AirNAAssetRT_ID = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'CTS_OEM_EU_Asset' and SobjectType='Asset' limit 1].Id;
        Asset obj = new Asset();
        obj.Name = 'testName';
        obj.Product2Id  = prod.id;
        obj.AccountId = acc.id;
        obj.SerialNumber = '12345';
        obj.District__c = 'Bay Area';
        obj.HP__c = '20';
        obj.RecordTypeId=AirNAAssetRT_ID;
        
             
        Test.startTest();
        insert obj;
        
        Test.stopTest();
        System.assertEquals(obj.AccountId,acc.id);
        
       }
        }