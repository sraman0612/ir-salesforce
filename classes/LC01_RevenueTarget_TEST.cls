@IsTest
public with sharing class LC01_RevenueTarget_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for LC01_RevenueTarget
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 
	
    static User mainUser;
	static User subUser;
	static List<Account> lstAccount = new List<Account>();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_TARGET__c> lstPTDsmpTarget = new List<PT_DSMP_TARGET__c>();
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd;
    static Id rtAccPowertools;

	static {
		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();

        UserRole r = new UserRole(DeveloperName = 'MyCustomRole1', Name = 'My Role1');
        insert r;
        UserRole r1 = new UserRole(DeveloperName = 'MyCustomRole2', Name = 'My Role2', ParentRoleId = r.Id);
        insert r1;

		mainUser = PT_DSMP_TestFactory.createAdminUser('LC01_RevenueTarget_TEST@test.COM', 
														PT_DSMP_Constant.getProfileIdAdmin());
        mainUser.UserRoleId = r.Id;

        subUser = PT_DSMP_TestFactory.createAdminUser('LC01_RevenueTarget_TEST1@test.COM', 
                                                        PT_DSMP_Constant.getProfileIdAdmin());
        subUser.UserRoleId = r1.Id;
        insert subUser;

        System.runAs(mainUser){
        	Account testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');
        	testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            lstAccount.add(testAcc);
        	insert lstAccount;

        	lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() + 1 ) ));
        	lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() - 1 ) ));
        	lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() - 2 ) ));
        	insert lstPTDsmpYear;

        	lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test ' ,
        												lstAccount[0].Id,
        												lstPTDsmpYear[0].Id
        												));
        	lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test ' ,
        												lstAccount[0].Id,
        												lstPTDsmpYear[1].Id
        												));
        	lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test ' ,
        												lstAccount[0].Id,
        												lstPTDsmpYear[2].Id
        												));
        	insert lstPTDsmp;


        	for(Integer i=1;i<=12;i++){
        		lstPTDsmpTarget.add(PT_DSMP_TestFactory.createDSMPTarget(
        			'test1 ' + i,
        			lstPTDsmp[0].Id,
        			String.valueOf(i)
        		));
        	}
        	for(Integer i=1;i<=12;i++){
        		lstPTDsmpTarget.add(PT_DSMP_TestFactory.createDSMPTarget(
        			'test2 ' + i,
        			lstPTDsmp[1].Id,
        			String.valueOf(i)
        		));
        	}
        	for(Integer i=1;i<=12;i++){
        		lstPTDsmpTarget.add(PT_DSMP_TestFactory.createDSMPTarget(
        			'test3 ' + i,
        			lstPTDsmp[2].Id,
        			String.valueOf(i)
        		));
        	}
        	insert lstPTDsmpTarget;



        }

	}


	@isTest static void test_retrieveTarget_1(){
        System.runAs(mainUser){ 
        	Test.startTest();
    			lstPTDsmp[0].Display_Type__c = '1';
    			update lstPTDsmp;

        		LC01_RevenueTarget.retrieveTarget(lstPTDsmp[0].id);
        	Test.stopTest();
        }
    }


    @isTest static void test_retrieveTarget_2(){
        System.runAs(mainUser){ 
        	Test.startTest();
    			lstPTDsmp[0].Display_Type__c = '2';
    			update lstPTDsmp;

        		LC01_RevenueTarget.retrieveTarget(lstPTDsmp[0].id);
        	Test.stopTest();
        }
    }


    @isTest static void test_retrieveTarget_3(){
        System.runAs(mainUser){ 
        	Test.startTest();
    			lstPTDsmp[0].Display_Type__c = '3';
    			update lstPTDsmp;

        		LC01_RevenueTarget.retrieveTarget(lstPTDsmp[0].id);
        	Test.stopTest();
        }
    }


    @isTest static void test_saveTarget(){
        System.runAs(mainUser){ 
        	Test.startTest();

				List<LC01_RevenueTarget.wrapperDSMPTarget> lstTargetDSMP = new List<LC01_RevenueTarget.wrapperDSMPTarget>();
				for(Integer i=0;i<lstPTDsmpTarget.size();i++){
					LC01_RevenueTarget.wrapperDSMPTarget current = new LC01_RevenueTarget.wrapperDSMPTarget();
					current.id = lstPTDsmpTarget[0].Id;
	        		lstTargetDSMP.add(current);
	        	}

        		LC01_RevenueTarget.saveTarget( JSON.serialize(lstTargetDSMP) );

                PT_DSMP_Constant.getSubordinateRoles(mainUser.UserRoleId);
                PT_DSMP_Constant.getParentRoles(mainUser.UserRoleId);
                
        	Test.stopTest();
        }
    }



}