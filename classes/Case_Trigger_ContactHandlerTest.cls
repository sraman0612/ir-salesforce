@isTest
public with sharing class Case_Trigger_ContactHandlerTest {
    
    public static void createData(){
    	
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	settings.Email_Message_Trigger_Enabled__c = true;
    	settings.CTS_OM_Case_Contact_Trigger_Enabled__c = true;
    	//Commented as part of EMEIA Cleanup
		//settings.CC_Case_Contact_Trigger_Enabled__c = true;
    	//settings.CC_Email_Trigger_Enabled__c = true;
    	settings.CTS_OM_Email_Case_Record_Type_IDs__c = [Select Id From RecordType Where isActive = True and sObjectType = 'Case' and DeveloperName = 'CTS_OM_Account_Management'].Id;
    	//settings.CC_Email_Case_Record_Type_IDs__c = [Select Id From RecordType Where isActive = True and sObjectType = 'Case' and DeveloperName = 'Club_Car_Case'].Id;
    	settings.CTS_OM_Contact_Record_Type_IDs_to_Match__c = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'Customer_Contact'].Id;
    	//settings.CC_Contact_Record_Type_IDs_to_Match__c = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'Club_Car'].Id;
    	//insert settings;  
    	
    	//Account acct = TestDataUtility.createAccount(true, 'Test Account', [Select Id From RecordType Where //isActive = True and sObjectType = 'Account' Order By Name ASC LIMIT 1].Id);    	
    } 
    
    public static void testContactSingleMatch1(String caseRecordTypeIds, String contactRecordTypeIds, String contactMatchStatusField){
    
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	
    	//Account acct = [Select Id From Account LIMIT 1];
    
    	Id otherCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management').getRecordTypeId();
    	Id otherContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Compressed_Air').getRecordTypeId();   	
         system.debug('othercaseRecId>>'+otherCaseRecordTypeId)	;
        system.debug('otherconRecId>>'+otherContactRecordTypeId)	;	
    	Contact ct1 = new Contact();
    	ct1.FirstName = 'Jim';
    	ct1.LastName = 'Test1';
    	ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';
    	ct1.RecordTypeId = contactRecordTypeIds;
    	
    	Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = otherContactRecordTypeId;	    		

		insert new Contact[]{ct1, ct2}; 	
    	
    	// Valid case record type and valid contact record type
    	Case case1 = TestDataUtility.createCase(false, caseRecordTypeIds, ct1.Id, null, 'Test 1', 'New');
    	case1.SuppliedEmail = 'abc1@testing.com';
    	
    	// Invalid case record type and invalid contact record type
    	Case case2 = TestDataUtility.createCase(false, otherCaseRecordTypeId, ct2.Id, null, 'Test 2', 'New'); // Different record type
    	case2.SuppliedEmail = 'abc1@testing.com';
    	
    	Test.startTest();
    	{
    		insert new Case[]{case1, case2};
    	}
    	Test.stopTest();
    	
    	Id case1Id = case1.Id, case2Id = case2.Id;
    	
    	// Confirm both cases still have the contact assigned  
    	Case case1Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case1Id');
    	Case case2Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case2Id'); 
    	
    	system.assertEquals(ct1.Id, case1Saved.ContactId); 
    	system.assertEquals(ct2.Id, case2Saved.ContactId);
    	
    	system.assertEquals('Single Match Found', case1Saved.get(contactMatchStatusField));
    	system.assertEquals(null, case2Saved.get(contactMatchStatusField));
    }
    
    public static void testContactSingleMatch2(String caseRecordTypeIds, String contactRecordTypeIds, String contactMatchStatusField){
    
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	
    	//Account acct = [Select Id From Account LIMIT 1];
    
    	Id otherCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management').getRecordTypeId();
    	Id otherContactRecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Compressed_Air').getRecordTypeId();   	
         system.debug('othercaseRecId>>'+otherCaseRecordTypeId)	;
        system.debug('otherconRecId>>'+otherContactRecordTypeId)	;	
    	Contact ct1 = new Contact();
    	ct1.FirstName = 'Jim';
    	ct1.LastName = 'Test1';
    	ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';        
    	ct1.RecordTypeId = contactRecordTypeIds;
    	
    	Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = otherContactRecordTypeId;	    		

		insert new Contact[]{ct1, ct2}; 	
    	
    	// Valid case record type and valid contact record type
    	Case case1 = TestDataUtility.createCase(false, caseRecordTypeIds, null, null, 'Test 1', 'New');
    	case1.SuppliedEmail = 'abc1@testing.com';
    	
    	// Invalid case record type and invalid contact record type
    	Case case2 = TestDataUtility.createCase(false, otherCaseRecordTypeId, null, null, 'Test 2', 'New'); // Different record type
    	case2.SuppliedEmail = 'abc1@testing.com';

    	Test.startTest();
    	{
    		insert new Case[]{case1, case2};
    	}
    	Test.stopTest();
    	
    	Id case1Id = case1.Id, case2Id = case2.Id;
    	
    	// Confirm the CTS OM case found the contact and the non-CTS OM case was not changed
    	Case case1Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case1Id');
    	Case case2Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case2Id'); 
    	
    	system.assertEquals(ct1.Id, case1Saved.ContactId); 
    	system.assertEquals(null, case2Saved.ContactId);
    	
    	system.assertEquals('Single Match Found', case1Saved.get(contactMatchStatusField));
    	system.assertEquals(null, case2Saved.get(contactMatchStatusField));
    }    
    
    public static void testContactSingleDuplicateMatch(String caseRecordTypeIds, String contactRecordTypeIds, String contactMatchStatusField){
    
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	
    	//Account acct = [Select Id From Account LIMIT 1];
    
    	Id otherCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management').getRecordTypeId();
    	Id otherContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Compressed_Air').getRecordTypeId();  
         system.debug('othercaseRecId>>'+otherCaseRecordTypeId)	;
        system.debug('otherconRecId>>'+otherContactRecordTypeId)	;	
    	Contact ct1 = new Contact();
    	ct1.FirstName = 'Jim';
    	ct1.LastName = 'Test1';
    	ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';        
    	ct1.RecordTypeId = contactRecordTypeIds;
    	
    	Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = contactRecordTypeIds;	    		

		insert new Contact[]{ct1, ct2}; 	
    	
    	// Valid case record type and valid contact record type
    	Case case1 = TestDataUtility.createCase(false, caseRecordTypeIds, null, null, 'Test 1', 'New');
    	case1.SuppliedEmail = 'abc1@testing.com';
    	
    	// Invalid case record type and valid contact record type
    	Case case2 = TestDataUtility.createCase(false, otherCaseRecordTypeId, ct2.Id, null, 'Test 2', 'New'); // Different record type
    	case2.SuppliedEmail = 'abc1@testing.com';

    	Test.startTest();
    	{
    		insert new Case[]{case1, case2};
    	}
    	Test.stopTest();
    	
    	Id case1Id = case1.Id, case2Id = case2.Id;
    	
    	// Confirm the CTS OM case found duplicate matches and the non-CTS OM case was not changed 
    	Case case1Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case1Id');
    	Case case2Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case2Id'); 
    	
    	system.assertEquals(ct1.Id, case1Saved.ContactId); 
    	system.assertEquals(ct2.Id, case2Saved.ContactId);
    	
    	system.assertEquals('Duplicate Matches Found', case1Saved.get(contactMatchStatusField));
    	system.assertEquals(null, case2Saved.get(contactMatchStatusField));
    }
    
    public static void testContactNoMatch1(String caseRecordTypeIds, String contactRecordTypeIds, String contactMatchStatusField){
    
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	
    	//Account acct = [Select Id From Account LIMIT 1];
    
    	Id otherCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management').getRecordTypeId();
    	Id otherContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Compressed_Air').getRecordTypeId();  
        system.debug('othercaseRecId>>'+otherCaseRecordTypeId)	;
        system.debug('otherconRecId>>'+otherContactRecordTypeId)	;
    	Contact ct1 = new Contact();
    	ct1.FirstName = 'Jim';
    	ct1.LastName = 'Test1';
    	ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';        
    	ct1.RecordTypeId = otherContactRecordTypeId;
    	
    	Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = otherContactRecordTypeId;	    		

		insert new Contact[]{ct1, ct2}; 	
    	
    	// Valid case record type and valid contact record type
    	Case case1 = TestDataUtility.createCase(false, caseRecordTypeIds, ct1.Id, null, 'Test 1', 'New');
    	case1.SuppliedEmail = 'abc1@testing.com';
    	
    	// Invalid case record type and valid contact record type
    	Case case2 = TestDataUtility.createCase(false, otherCaseRecordTypeId, ct2.Id, null, 'Test 2', 'New'); // Different record type
    	case2.SuppliedEmail = 'abc1@testing.com';

    	Test.startTest();
    	{
    		insert new Case[]{case1, case2};
    	}
    	Test.stopTest();
    	
    	Id case1Id = case1.Id, case2Id = case2.Id;
    	
    	// Confirm the CTS OM case could not locate a unique match and the non-CTS OM case was not changed 
    	Case case1Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case1Id');
    	Case case2Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case2Id'); 
    	
    	system.assertEquals(null, case1Saved.ContactId); 
    	system.assertEquals(ct2.Id, case2Saved.ContactId);
    	
    	system.assertEquals('No Matches Found', case1Saved.get(contactMatchStatusField));
    	system.assertEquals(null, case2Saved.get(contactMatchStatusField));
    }         
    
    public static void testContactNoMatch2(String caseRecordTypeIds, String contactRecordTypeIds, String contactMatchStatusField){
    
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
    	
    	//Account acct = [Select Id From Account LIMIT 1];
    
    	Id otherCaseRecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Case' and DeveloperName = 'Milton_Roy_Case_Management' Order By Name ASC LIMIT 1].Id;
    	Id otherContactRecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'Compressed_Air' Order By Name ASC LIMIT 1].Id;    	
        	
    	Contact ct1 = new Contact();
    	ct1.FirstName = 'Jim';
    	ct1.LastName = 'Test1';
    	ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';        
    	ct1.RecordTypeId = otherContactRecordTypeId;
    	
    	Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = otherContactRecordTypeId;	    		

		insert new Contact[]{ct1, ct2}; 	
    	
    	// Valid case record type and valid contact record type
    	Case case1 = TestDataUtility.createCase(false, caseRecordTypeIds, null, null, 'Test 1', 'New');
    	case1.SuppliedEmail = 'abc1@testing.com';

    	// Invalid case record type and valid contact record type
    	Case case2 = TestDataUtility.createCase(false, otherCaseRecordTypeId, ct2.Id, null, 'Test 2', 'New'); // Different record type
    	case2.SuppliedEmail = 'abc1@testing.com';

    	Test.startTest();
    	{
    		insert new Case[]{case1, case2};
    	}
    	Test.stopTest();
    	
    	Id case1Id = case1.Id, case2Id = case2.Id;
    	
    	// Confirm the CTS OM case could not locate a unique match and the non-CTS OM case was not changed 
    	Case case1Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case1Id');
    	Case case2Saved = Database.query('Select Id, ContactId, ' + contactMatchStatusField + ' From Case Where Id = :case2Id'); 
    	
    	system.assertEquals(null, case1Saved.ContactId); 
    	system.assertEquals(ct2.Id, case2Saved.ContactId);
    	
    	system.assertEquals('No Matches Found', case1Saved.get(contactMatchStatusField));
    	system.assertEquals(null, case2Saved.get(contactMatchStatusField));
    }             
}