global class CC_PavilionConnectivityCnt { 

public CC_PavilionConnectivityCnt(CC_PavilionTemplateController controller) {}
  
  
  @Remoteaction
  public static set<String> getMarkets(string aid){
    return getcontractSubTypes(aid);
  }
  
  
  
   @Remoteaction
  Public static set<ContentVersion> getConnectivity(string region, string currncy,string aid){
  
  System.debug('region: ' + region);
  System.debug('currncy: ' + currncy);
  
    Id  usrId  = userinfo.getUserId();
    Boolean isInternal=aid==PavilionSettings__c.getInstance('INTERNALCLUBCARACCT').Value__c?TRUE:FALSE;
    set<string> contractSubTypes  = getcontractSubTypes(aid);
    List<ContentVersion> acctFilteredBulletinsLst = new List<ContentVersion>();
    set<ContentVersion> finalBulletinSet = new set<ContentVersion>();
    String qryStr = 'SELECT id,CC_Date__c,Title,Agreement_Type__c,Currency__c,RecordType.Name ' +
                      'FROM ContentVersion ' + 
                     'WHERE RecordType.Name=\'Connectivity\' and  Title != null';
    
     if (!isInternal){qryStr += ' and Currency__c INCLUDES(:currncy) and CC_Region__c INCLUDES(:region)';}
    acctFilteredBulletinsLst = Database.query(qryStr);

    if(isInternal){
      finalBulletinSet.addAll(acctFilteredBulletinsLst);
    } else {
      finalBulletinSet.addAll(filterBulletinsByUserandAgreement(acctFilteredBulletinsLst,contractSubTypes));
    }
    return finalBulletinSet;
  }
  
  
  //code contributed by @Priyanka Baviskar for issue no 7842483.
  private static set<String> getcontractSubTypes(String acctId){
      
   Account children = null;
  
    set<string> subtypeSet = new set<string>();
     Contract []  cLst; 
     List<Account> accs = [select Id, Name from Account where ParentId=:acctId];
     if(!accs.isEmpty()){
       children = accs[0];
      }
    
       if(children!= null)
      {
         Id cid = children.Id;
         cLst = [SELECT id,name,CC_Sub_Type__c,CC_Type__c 
                           FROM Contract 
                           WHERE AccountId IN(:acctId,:cid) and CC_Type__c='Dealer/Distributor Agreement' and 
                                 CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and
                              CC_Contract_End_Date__c >=:system.today()];
        }
        else
        {
         
           cLst = [SELECT id,name,CC_Sub_Type__c,CC_Type__c 
                           FROM Contract 
                           WHERE AccountId=:acctId and CC_Type__c='Dealer/Distributor Agreement' and 
                                 CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and
                                 CC_Contract_End_Date__c >=:system.today()];
        }
          
    for(Contract c: cLst){if(c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != ''){subtypeSet.add(c.CC_Sub_Type__c); }}                             
    return subtypeSet;
  }
  
  private static ContentVersion [] filterBulletinsByUserandAgreement(List<ContentVersion> cvLst, set<String> subTypes){
    List<ContentVersion> contentListToReturn = new List<ContentVersion>();
    for(ContentVersion cv2 : cvLst) {
      for(String s : cv2.Agreement_Type__c.split(';')){if(subTypes.contains(s)){contentListToReturn.add(cv2);break;}}
    }
    return contentListToReturn;
  }
}