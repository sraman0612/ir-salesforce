public without sharing class CC_STL_Approval_Override_Schedule implements Schedulable{
    
    public void execute(SchedulableContext sc) {           

        /* This job is used to unlock records from approval to allow resubmission and to override approvals where applicable */
		Database.executeBatch(new CC_Resubmit_Service_Contract_Batch(), 25);

        /* This job is used to allow managers to approve on behalf of suboordinates when they are not declard as their delegated approver */
        CC_Batch_Controls__c batchControls = CC_Batch_Controls__c.getOrgDefaults();

        if (batchControls != null && String.isNotBlank(batchControls.Mgr_Approval_Override_Supported_Objects__c)){
            
            for (String obj : batchControls.Mgr_Approval_Override_Supported_Objects__c.split(',')){
                Database.executeBatch(new CC_Approval_Override_Batch(obj), 25);
            }
        }                
    }    
}