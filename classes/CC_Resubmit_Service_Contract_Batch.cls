public without sharing class CC_Resubmit_Service_Contract_Batch implements Database.Batchable<sObject>{
	
	@TestVisible private static String defaultContractStatus = 'Draft';
    @TestVisible private static Boolean forceErrors = false;
    
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        // Find all contracts that are flagged for unlocking
        return Database.getQueryLocator('Select Id, ContractNumber, CC_Unlock_Requestor__c From Contract Where RecordType.DeveloperName = \'Club_Car_Service_Contract\' and CC_Unlock_Requested__c = true and CC_Unlock_Requestor__c != null');
   	}    
   	
   	public void execute(Database.BatchableContext BC, List<Contract> contracts){
   		
   		try{
   			
   			Map<Id, Contract> contractMap = new Map<Id, Contract>();
   			Map<Id, Id> contractIdRequestorMap = new Map<Id, Id>();
   			
   			for (Contract c : contracts){
   				contractMap.put(c.Id, c);
   				contractIdRequestorMap.put(c.Id, c.CC_Unlock_Requestor__c);
   			}
   			
   			system.debug('contractIdRequestorMap: ' + contractIdRequestorMap);
   			
   			// Unlock the contracts
   			List<Approval.UnlockResult> unlockResults = Approval.unlock(contracts, false);
   			
   			Contract[] successes = new Contract[]{};
   			Map<Id, String> failures = new Map<Id, String>();
   			
   			for (Approval.UnlockResult unlockResult : unlockResults){
   				
                if (Test.isRunningTest() && forceErrors){
                    failures.put(unlockResult.getId(), 'Test failure message.');
                }
                else if (unlockResult.isSuccess()){
   					successes.add(new Contract(Id = unlockResult.getId()));
   				}
   				else{
   					failures.put(unlockResult.getId(), unlockResult.getErrors()[0].getMessage());
   				}
   			}
   			
   			system.debug('successes: ' + successes);
   			system.debug('failures: ' + failures);

   			if (successes.size() > 0){
   				
   				// Set the status back to Draft and reset the unlock request fields
   				for (Contract c : successes){

   					c.CC_Contract_Status__c = defaultContractStatus;
   					c.CC_Unlock_Requested__c = false;
   					c.CC_Unlock_Requestor__c = null;
   				}
   				
   				sObjectService.updateRecordsAndLogErrors(successes, 'CC_Resubmit_Service_Contract_Batch', 'execute');
   			}
   			
			// Build email notifications for successes and failures
			Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
			
			for (Contract success : successes){
				
				Contract c = contractMap.get(success.Id);
				system.debug('contract: ' + c);
				
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(c);
				String viewURL = URL.getSalesforceBaseUrl().toExternalForm() + stdCtrl.view().getURL();	
				String emailSubject = 'Service contract ' + c.ContractNumber + ' successfully unlocked.';
				String emailTextBody = 'The contract has been unlocked and can now be modified and resubmitted for approval.<br/><br/>Here is a link to to view the contract: <a href="' + viewURL + '">' + c.ContractNumber + '</a>';		                				
                Id requestor = contractIdRequestorMap.get(c.Id);
                
                system.debug('requestor: ' + requestor);
                system.debug('viewURL: ' + viewURL);
                
                messages.add(EmailService.buildSingleEmailMessage(
                    requestor, 
                    null, 
                    null, 
                    null,
                    emailSubject,
                    null, 
                    emailTextBody, 
                    null,
                    null)
                );		
			}
			
			for (Id contractId : failures.keySet()){
				
				Contract c = contractMap.get(contractId);
				system.debug('contract: ' + c);
				
				ApexPages.StandardController stdCtrl = new ApexPages.StandardController(c);
				String viewURL = URL.getSalesforceBaseUrl().toExternalForm() + stdCtrl.view().getURL();	
				String emailSubject = 'Contract ' + c.ContractNumber + ' failed to unlock.';
				String emailTextBody = 'The contract could not be unlocked due to the following error below.  Please contact your administrator for more details.<br/><br/>' + 'Error: ' + failures.get(contractId) + '<br/><br/>Here is a link to to view the contract: <a href="' + viewURL + '">' + c.ContractNumber + '</a>';		                				
                Id requestor = contractIdRequestorMap.get(c.Id);
                
                system.debug('requestor: ' + requestor);
                system.debug('viewURL: ' + viewURL);
                
                messages.add(EmailService.buildSingleEmailMessage(
                    requestor, 
                    null, 
                    null, 
                    null,
                    emailSubject,
                    null, 
                    emailTextBody, 
                    null,
                    null)
                );		
			}					
			
			EmailService.sendSingleEmailMessages(messages);  			
   		}
   		catch(Exception e){
   			system.debug('error: ' + e.getMessage());
   			ApexLogHandler logHandler = new ApexLogHandler('CC_Resubmit_Service_Contract_Batch', 'execute', e.getMessage());
   			insert logHandler.logObj;
   		}
   	}
   	
	public void finish(Database.BatchableContext BC){
		
   	}       	
}