@isTest
private class CC_dealerSearch_test {


    
    static testMethod void testGettersAndSetters() {
    
                CC_dealerSearch.DealerInfo d    = new CC_dealerSearch.DealerInfo();
                d.accId               = d.accId + '';
                d.agrId               = d.agrId + '';
                d.DealerName          = d.DealerName + '';
                d.Street              = d.Street + '';
                d.City                = d.City + '';
                d.State               = d.State + '';
                d.Zip                 = d.Zip + '';
                d.Country             = d.Country + '';
                d.DealerPhone         = d.DealerPhone + '';
                d.DealerWebsite       = d.DealerWebsite + '';
                d.DealerContact       = d.DealerContact + '';
                d.DealerEmail         = d.DealerEmail + '';                        
                d.Latitude            = d.Latitude + '';
                d.Longitude           = d.Longitude + '';
                d.County              = d.County + '';
                d.BlackandGoldFlag    = d.BlackandGoldFlag + 'TBD';
                d.Distance            = d.Distance + '';
                d.PowerChordSite      = d.PowerChordSite + ''; 
                d.locatorActive       = true;
                d.locatorActive       = d.locatorActive; 
                d.AgreementType       = d.AgreementType + ''; 
                d.SalesPersonNumber   = d.SalesPersonNumber + '';
                d.SalesPersonName     = d.SalesPersonName + '';
    }
    
    
    static testMethod void testSearch() {
        
       // States and countries
       StatesCountries__c[] sc = new StatesCountries__c[] {
           new StatesCountries__c (name     = 'Georgia',
                                   type__c  = 'S',
                                   code__c  = 'GA',
                                   country__c = 'USA'),
           new StatesCountries__c (name     = 'USA',
                                   type__c  = 'C',
                                   code__c  = 'US',
                                   country__c = '',
                                   cc_PostalCode_Exclusive__c = true,
                                   cc_PostalCode_Proximity__c = true, 
                                   cc_PostalCode_Size__c = 5
                                  ),
           new StatesCountries__c (name     = 'Canada',
                                   type__c  = 'C',
                                   code__c  = 'CA',
                                   country__c = '',
                                   cc_PostalCode_Exclusive__c = true,
                                   cc_PostalCode_Proximity__c = true,
                                   cc_PostalCode_Size__c = 3
                                  ),
          new StatesCountries__c  (name     = 'Mexico',
                                   type__c  = 'C',
                                   code__c  = 'MX',
                                   country__c = '',
                                   cc_PostalCode_Exclusive__c = false,
                                   cc_PostalCode_Proximity__c = false
                                  )     
       };
       insert sc;                                                         
                                                         
                                                                   
       // Postal Codes
       CC_PostalCode__c[] pc = new cc_PostalCode__c[] {
           new CC_PostalCode__c(Name = '00000',
                                CC_Country__c = 'USA',
                                CC_Lat__c = '50',
                                CC_Long__c = '50',
                                CC_County__c = 'test',
                                CC_StateOrProvince__c = 'test'),
           new CC_PostalCode__c(Name = '00000',
                                CC_Country__c = 'Canada',
                                CC_Lat__c = '50',
                                CC_Long__c = '50',
                                CC_County__c = 'test',
                                CC_StateOrProvince__c = 'test')
       };
       insert pc;
              
       // Sales Rep, Account, Contract, Territory
       CC_Sales_Rep__c sr = new CC_Sales_Rep__c(Name = 'Test',   
                                                CC_Sales_Person_Number__c = '11111');
       insert sr;       
       Id ccAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='Club_Car'].Id;
       Account a = new Account (Name = 'Dealer Locator Test',
                                RecordTypeId = ccAcctRTID,
                                CC_Sales_Rep__c = sr.Id,
                                CC_Shipping_Billing_Address_Same__c = true,
                                ShippingStreet = '2044 Forward Augusta Dr',
                                ShippingCity   = 'Augusta',
                                ShippingState  = 'GA',
                                ShippingPostalCode = '00000',
                                ShippingCountry = 'USA',
                                ShippingLatitude = 50,
                                ShippingLongitude = 50,
                                BillingStreet = '2044 Forward Augusta Dr',
                                BillingCity   = 'Augusta',
                                BillingState  = 'GA',
                                BillingPostalCode = '00000',
                                BillingCountry = 'USA',
                                BillingLatitude = 50,
                                BillingLongitude = 50);
       insert a;
       Contract x = New Contract(AccountId = a.Id,
                                 CC_Contract_Status__c = 'Current',
                                 CC_Type__c = 'Dealer/Distributor Agreement',
                                 CC_Sub_Type__c = 'Retail Dealer',
                                 CC_Primary_Sales_Reporting__c = '11111'); 
       insert x;
       CC_Territory__c  r = New CC_Territory__c (CC_Agreement_Number__c = x.Id,
                                                 CC_Territory_Type__c = 'Channel', 
                                                 CC_ZipCode__c = '00000',
                                                 CC_Country__c = 'USA');
       insert r;
       Contract c = New Contract (AccountId = a.Id,
                                  CC_Contract_Status__c = 'Current',
                                  CC_Type__c = 'Dealer/Distributor Agreement',
                                  CC_Sub_Type__c = 'Golf Car Distributor',
                                  CC_Primary_Sales_Reporting__c = '11111',
                                  CC_Override_Partner__c = true
                                 ); 
       insert c;
       CC_Territory__c  t = New CC_Territory__c (CC_Agreement_Number__c = c.Id,
                                                 CC_Territory_Type__c = 'Channel', 
                                                 CC_ZipCode__c = '00000',
                                                 CC_Country__c = 'USA');
       insert t;
  
  
         // Dealer Locator Settings
       CC_Dealer_Locator_Settings__c[] config = new CC_Dealer_Locator_Settings__c[] {
           new CC_Dealer_Locator_Settings__c (name = 'G', 
                                              Value__c ='Retail Dealer', 
                                              Search_Type__c = 'Proximity', 
                                              Search_Radius__c = 200,
                                              Default_Account_Id__c = a.Id),
           new CC_Dealer_Locator_Settings__c (name = 'D', 
                                              Value__c ='Golf Car Distributor',
                                              Search_type__c = 'Exclusive', 
                                              Search_Radius__c = 200,
                                              Default_Account_Id__c = a.Id)
       };
       insert config;
    
       // Search
       CC_dealerSearch.hideInactive = false;
       CC_dealerSearch.search();
       CC_dealerSearch.test();     
       for (CC_Dealer_Locator_Settings__c dls :CC_Dealer_Locator_Settings__c.getAll().values()) {
           CC_dealerSearch.SearchDealers(dls.Name, 'USA', '00000');
           CC_dealerSearch.SearchDealers(dls.Name, 'Canada', '00000');
           CC_dealerSearch.SearchDealers(dls.Name, 'Australia', '00000');
           CC_dealerSearch.SearchDealers(dls.Name, 'MX', '00000');
           CC_dealerSearch.getPartnerForLeadAssignment('USA', '00000', dls.Value__c);
       }
       
       
       // Exception handling
       try {
           CC_dealerSearch.SearchDealers('G', 'USA', '');
       } catch (Exception e) {
       }
    
    }


}