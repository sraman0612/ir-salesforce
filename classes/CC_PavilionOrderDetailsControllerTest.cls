@isTest
public class CC_PavilionOrderDetailsControllerTest {
    static testmethod void UnitTest_CC_PavilionOrderDetailsController()
    {
        
        //Creation Of Test Data
        Account a = TestUtilityClass.createAccount();
        insert a;
        
        CC_My_Team__c myTeam = TestUtilityClass.createMyTeam('Customer Account Specialist',a.Id,'Custom Accounts');
        myTeam.User__c = userinfo.getuserid();
        insert myTeam;
        
       Contact con = new Contact(
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId(),
            LastName ='LN',
            FirstName = 'FS',
            AccountId = a.Id,
            Email = 'test@test.ingersollrand.com'
        );
        insert con;

       Profile portalProfile = [SELECT Id FROM Profile WHERE Name ='CC_PavilionCommunityUser' Limit 1];
        
        // populate custom settings before user creation
        PavilionSettings__c pavillionCommunityProfileId = new PavilionSettings__c(
            Name = 'PavillionCommunityProfileId',
            Value__c = portalProfile.Id
        );
        insert pavillionCommunityProfileId;
        PavilionSettings__c newPvnSetting = TestUtilityClass.createPavilionSettings('CASQ',userinfo.getUserId());
        insert newPvnSetting;
        PavilionSettings__c newPavnSettings = TestUtilityClass.createPavilionSettings('ClubCarCommunityLoginProfileId',portalProfile.Id);
        insert newPavnSettings;
        
        PavilionSettings__c pavilionPublicGroupID = new PavilionSettings__c(
            Name = 'PavilionPublicGroupID',
            Value__c = portalProfile.Id
        );
        insert pavilionPublicGroupID;
        PavilionSettings__c ps= PavilionSettings__c.getValues('MyTeamDefaultQueue');
        id qID = [select id from Group where developername='MyTeamDefaultQueue'].Id;
        PavilionSettings__c myteamdefq = new PavilionSettings__c(
            Name = 'MyTeamDefaultQueue',
            Value__c = qId
        );
        insert myteamdefq;
        
        User testUser = new User(
            Username = System.now().millisecond() + 'test12345ddmmyy@test.ingersollrand.com',
            ContactId = con.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.ingersollrand.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert testUser;
        Pavilion_Navigation_Profile__c newPavilionNavigationProfile = TestUtilityClass.createPavilionNavigationProfile(testUser.CC_Pavilion_Navigation_Profile__c);
        insert newPavilionNavigationProfile;

        CC_Order__c ord = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
        insert ord;
        CC_Order_Item__c orditem = TestUtilityClass.createOrderItem(ord.Id);
        insert orditem;
        CC_Car_Item__c carItem = TestUtilityClass.createCarItem(orditem.Id);
        insert carItem;
        CC_Order_Shipment__c createOrderShipment = TestUtilityClass.createOrderShipment(ord.Id);
        insert createOrderShipment;
        Product2 prod =  TestUtilityClass.createProduct2();
        insert prod;
        CC_Car_Feature__c cf =  TestUtilityClass.createCarFeature(carItem.Id,prod.Id,ord.Id);
        insert cf;
        
        system.runAs(testUser){
          Test.startTest();
          ApexPages.currentPage().getParameters().put('id',null);
          //CC_PavilionOrderDetailsController OrdDe =new CC_PavilionOrderDetailsController();
          ApexPages.currentPage().getParameters().put('p','p');
          ApexPages.currentPage().getParameters().put('id',carItem.Id);
           ApexPages.currentPage().getParameters().put('id',createOrderShipment.Id);
          CC_PavilionTemplateController controller = new CC_PavilionTemplateController();
          controller.acctId=a.id;
          controller.CustomerNumber='12321';
          CC_PavilionOrderDetailsController OrdDet = new CC_PavilionOrderDetailsController(controller);
          List<CC_Invoice2__c> closedOrdInvoiceItems = new List<CC_Invoice2__c>();
          List<CC_Order_Shipment__c> shipListtt =new List<CC_Order_Shipment__c>();
          List<CC_Order_Shipment_Item__c > shipItemListt =new List<CC_Order_Shipment_Item__c >();
          List<OrderItemAndFeatureWrapper> wrapperTestList=new List<OrderItemAndFeatureWrapper>();
          Map<String,String> mapDataFOrItemDesc = new Map<String,String>();
          CC_Order__c orderInit=new CC_Order__c();
          OrdDet.ShowButtonDiv=true;
          OrdDet.lessThan21Days=true;
          OrdDet.orderId=ord.Id;
          OrdDet.o=orderInit;
          OrdDet.customerRep='new customerrep';
          CC_PavilionOrderDetailsController.OrderItemAndFeatureWrapper oifw=new CC_PavilionOrderDetailsController.OrderItemAndFeatureWrapper(orditem);
          String accid=a.id;
          String orderId=ord.id;
          Test.stopTest();
          CC_Order__c ordTemp = TestUtilityClass.createCustomOrder(a.id,'OPEN','N');
          insert ordTemp;
        }
        CC_PavilionOrderDetailsController.changeStatus(a.id,ord.id,'comment','N');
        CC_PavilionOrderDetailsController.changeStatus(a.id,ord.id,'comment','C');
      }
    
}