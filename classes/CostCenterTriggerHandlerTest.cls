@isTest
public class CostCenterTriggerHandlerTest{
  public static List<Cost_Center__c> costCenterList = new List<Cost_Center__c>();
  public static List<Opportunity> opportunityList = new List<Opportunity>();
  public static List<Opportunity_Revenue_Schedule__c> revenueScheduleList = new List<Opportunity_Revenue_Schedule__c>();
  public static String RSHVAC_RT_NAME = 'RS_HVAC';
  public static Id RSHVAC_RT_Id = [Select id from RecordType where DeveloperName = :RSHVAC_RT_NAME AND SObjectType = 'Account'].Id;
  public static Cost_Center__c costCenter = new Cost_Center__c();
  public static Account acc = new Account();
  public static User apiUser1;
  public static Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;  
  
  public testMethod static void costCenterInsertionTest(){
    costCenterList.add(new Cost_Center__c(Channel__c = 'IWD', Default_Branch__c = 'Testing Center1', Create_Existing_Business_Opportunity__c = True));  
    costCenterList.add(new Cost_Center__c(Channel__c = 'IWD', Default_Branch__c = 'Testing Center2', Create_Existing_Business_Opportunity__c = True)); 
    Test.startTest();
      insert costCenterList;
      costCenterList = [select id, Create_Existing_Business_Opportunity__c from Cost_Center__c where Id in :costCenterList];
      System.assert(!costCenterList[0].Create_Existing_Business_Opportunity__c);
      opportunityList = [select id from opportunity where Cost_Center_Name__c In :costCenterList];
      System.assert(opportunityList.size()>0);
      revenueScheduleList = [select id from Opportunity_Revenue_Schedule__c where Opportunity__c in :opportunityList];
      System.assert(revenueScheduleList.size()>0);
    Test.stopTest();
  }
  
  public testMethod static void costCenterUpdateTest(){
    costCenter = new Cost_Center__c(Channel__c = 'IWD', Default_Branch__c = 'Testing Center1', Create_Existing_Business_Opportunity__c = False);  
    insert costCenter;
    opportunityList = [select id from Opportunity where Cost_Center_Name__c = :costCenter.Id];
    System.assert(opportunityList.isEmpty());
    costCenter.Create_Existing_Business_Opportunity__c = True;
    update costCenter;
    opportunityList = [select id from Opportunity where Cost_Center_Name__c = :costCenter.Id];
    System.assert(opportunityList.size()>0);
    revenueScheduleList = [select id from Opportunity_Revenue_Schedule__c where opportunity__c in :opportunityList];
    System.assert(revenueScheduleList.size()>0);
  } 
  
  
  public testMethod static void ownerAssignmentTest(){
    Id apiProfileId = [select Id from Profile where name like 'API_Profile'].Id;
    apiUser1 = new User(LastName = 'testingUser1', profileId=apiProfileId, Username='aaaTest@example.com', Email='aaa@example.com',
         Alias = 'abc1', CommunityNickname='abc1', TimeZoneSidKey = 'GMT', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US');
    Test.startTest();
    insert apiUser1;
    costCenter = new Cost_Center__c(name = 'testcost', Channel__c = 'IWD', Default_Branch__c = 'Testing Center1', 
                                                      Create_Existing_Business_Opportunity__c = True);  
    insert costCenter;
    acc = new Account(Name = 'Testing Account', Cost_Center_Name__c = costCenter.Id, RecordTypeId = RSHVAC_RT_Id);
    insert acc;
    acc.OwnerId = apiUser1.Id;
    update acc;
    
    opportunityList = [Select id, OwnerId  from Opportunity where Cost_Center_Name__c = :costCenter.id];
    costCenter.OwnerId = apiUser1.Id;
    update costCenter;
    acc = [Select Id, OwnerId from Account where id = :acc.id];
    System.assertEquals(costCenter.OwnerId, acc.OwnerId);
    opportunityList = [Select id, OwnerId  from Opportunity where Cost_Center_Name__c = :costCenter.id];
    costCenter = [select id, OwnerId from Cost_Center__c where Id = :costCenter.id];
    System.assertEquals(costCenter.OwnerId, opportunityList[0].OwnerId);
    Test.stopTest(); 
  } 
  
}