/******************************************************************************
Name:       RS_CRS_CaseFTPController_Test
Purpose:    Test class for RS_CRS_CaseFTPController. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Neela Prasad    02/28/2018  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_CaseFTPController_Test {
    /******************************************************************************
	Method Name : updateConcessionStatus1_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing updateConcessionStatus  Method(Positive Scenario)
	******************************************************************************/
    Static testmethod void updateConcessionStatus1_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecordTypeId1 = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        System.runAs(userRec){
            //insert test case
            Case caseObj1 = new Case();
            caseObj1.RecordTypeId = caseRecordTypeId1;
            caseObj1.Status = 'Customer Action1';
            caseObj1.RS_CRS_Concession_Status__c = 'Approved';
            insert caseObj1;
            
            Test.startTest();
            RS_CRS_CaseFTPController.updateConcessionStatus(caseObj1.Id);
            Test.stopTest();
        }
    } 
    
    /******************************************************************************
	Method Name : updateConcessionStatus2_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing updateConcessionStatus  Method(Negative Scenario)
	******************************************************************************/
    Static testmethod void updateConcessionStatus2_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecordTypeId1 = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        System.runAs(userRec){
            //Test Case
            Case caseObj2 = new Case();
            caseObj2.RecordTypeId = caseRecordTypeId1;
            caseObj2.Status = 'Customer Action1';
            caseObj2.RS_CRS_Concession_Status__c = 'Rejected';
            insert caseObj2;
            
            Test.startTest();
            RS_CRS_CaseFTPController.updateConcessionStatus(caseObj2.Id);
            Test.stopTest();
        }
    }
    
    /******************************************************************************
	Method Name : updateConcessionStatus3_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing updateConcessionStatus  Method(Negative Scenario)
	******************************************************************************/
    Static testmethod void updateConcessionStatus3_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecordTypeId1 = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        System.runAs(userRec){
            //Test Case
            Case caseObj3 = new Case();
            caseObj3.RecordTypeId = caseRecordTypeId1;
            caseObj3.Status = 'Customer Action3';
            caseObj3.RS_CRS_Concession_Status__c = 'Submitted for FTP';
            insert caseObj3;
            
            Test.startTest();
            RS_CRS_CaseFTPController.updateConcessionStatus(caseObj3.Id);
            Test.stopTest();
        }
    }
    
    /******************************************************************************
	Method Name : updateConcessionStatus3_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing updateConcessionStatus  Method(Negative Scenario)
	******************************************************************************/
    Static testmethod void updateConcessionStatus4_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting  Record Type's Id through Schema.Describe Class
        Id caseRecordTypeId1 = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        System.runAs(userRec){
            //Test Case
            Case caseObj4 = new Case();
            caseObj4.RecordTypeId = caseRecordTypeId1;
            caseObj4.Status = 'Customer Action4';
            caseObj4.RS_CRS_Concession_Status__c = 'Submitted for FTP';
            insert caseObj4;
            
            try{
            Test.startTest();
            RS_CRS_CaseFTPController.updateConcessionStatus(null);
            Test.stopTest();
               }Catch (Exception ex)
               { //catching Error messages and comparing
                System.assertEquals(ex.getMessage().contains('NullPointerException'), TRUE);
               }
        }
     }
}