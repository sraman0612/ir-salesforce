global class CC_PavilionDealerInvoiceHistoryCnt {
/** variable declaration start**/
 Public Id AcctId;
 public Map<String,String> MapInvoiceOrderData{get;set;}
 Public list<CC_Order__c> lstCCOrders { get; set;}
 
/** variable declaration end**/
 /* constructor */
 public CC_PavilionDealerInvoiceHistoryCnt() {
  constructorFunction();
 }
 public CC_PavilionDealerInvoiceHistoryCnt(CC_PavilionTemplateController controller) {
  AcctId = controller.AcctId;
  constructorFunction();
 }
 
 public void constructorFunction() {

    
        MapInvoiceOrderData=new Map<String, String>();
        if (AcctId != null) {    
            lstCCOrders = new List<CC_Order__c>();
            datetime qryStart = datetime.now();
            
            lstCCOrders = [Select Id, CC_Customer_Invoice_Number__c,CC_Dealer_Invoice_Status__c,CC_Customer_Number__c,CC_Order_Number_Reference__c, Name,CC_Sales_Person_Number__r.CC_Sales_Rep__c,CC_Account__c, 
                                CC_Account__r.Name, CC_Comments__c ,Total_Invoice_Amount__c, CreatedDate,CC_Sales_Person_Number__r.CC_Sales_Rep__r.FirstName,CC_Status__c,Sales_Person_Name__c,CC_Customer_Name_1__c
                                
                                From CC_Order__c Where RecordType.Name = 'Dealer Invoicing' and CC_Account__c=:AcctId order by CreatedDate Desc];
            
            datetime qryEnd = datetime.now();
            System.debug('###before Query');
            system.debug(logginglevel.WARN, (qryEnd.getTime() - qryStart.getTime()) /1000);

            
            
            //calling the methods to set the map data.  
            MapInvoiceOrderData=setMapdataForOrderInvoiceLinkage(lstCCOrders);
        }
   
   }
    /*-------retrieves the list of Orders Data Based on status----- */ 
    //code contributed by @Priyanka Baviskar for issue no 7592100.            
      @RemoteAction
     public static List<orderWrapper> getOrderStatusData(String status)
      {
          System.debug('#####Priyanka####');
          list<orderWrapper> orderwithIVllist = new list<orderWrapper>();
          String userId = Userinfo.getUserid();
          String AcctId = [Select AccountId from user where id = : userId].AccountId;
          List<CC_Order__c> lstOrder = new List<CC_Order__c>();
          String rectypename='Dealer Invoicing';
          String selectstatus=status;
          
         System.debug('****Status**'+status);
          datetime qryStart = datetime.now();
          if(selectstatus=='')
          {
          lstOrder = [Select Id, CreatedDate, CC_Customer_Invoice_Number__c,CC_Dealer_Invoice_Status__c,CC_Customer_Number__c,CC_Order_Number_Reference__c, Name,CC_Account__c,CC_Sales_Person_Number__r.CC_Sales_Rep__c, CC_Account__r.Name, Sales_Person_Name__c, Total_Invoice_Amount__c, CC_Comments__c ,CC_Sales_Person_Number__r.CC_Sales_Rep__r.FirstName,CC_Status__c,(Select StepStatus,ActorId,Actor.Name From ProcessSteps  order by SystemModstamp desc limit 1),CC_Customer_Name_1__c  
                      From CC_Order__c Where RecordType.Name =:rectypename  AND CC_Account__c=:AcctId];            
         }
         else{
         lstOrder = [Select Id, CreatedDate, CC_Customer_Invoice_Number__c,CC_Dealer_Invoice_Status__c,CC_Customer_Number__c,CC_Order_Number_Reference__c, Name,CC_Account__c,CC_Sales_Person_Number__r.CC_Sales_Rep__c, CC_Account__r.Name, Sales_Person_Name__c, Total_Invoice_Amount__c,CC_Comments__c ,CC_Sales_Person_Number__r.CC_Sales_Rep__r.FirstName,CC_Status__c,(Select StepStatus,ActorId,Actor.Name From ProcessSteps  order by SystemModstamp desc limit 1),CC_Customer_Name_1__c  
                      From CC_Order__c Where RecordType.Name =:rectypename  AND CC_Account__c=:AcctId AND CC_Dealer_Invoice_Status__c=:selectstatus];            
         }
         datetime qryEnd = datetime.now();
            System.debug('###before CC_Order__c Query');
            system.debug(logginglevel.WARN, (qryEnd.getTime() - qryStart.getTime()) /1000);

         
          set<id> orderSet = new set<id>();
          for(CC_Order__c cco:lstOrder){
              orderSet.add(cco.id);
          }
          
          datetime qryStart1 = datetime.now();
          
          list<CC_Order_Shipment__c> ordershiplist = [SELECT id,Invoice_Number__c,  Order__r.CC_Account__r.CC_Customer_Number__c, Invoice_Date__c, Order__c,  Order__r.CC_Status__c FROM CC_Order_Shipment__c WHERE Order__c IN: orderSet LIMIT 50000];
          
          datetime qryEnd1 = datetime.now();
            System.debug('###before Shipping Query');
            system.debug(logginglevel.WARN, (qryEnd1.getTime() - qryStart1.getTime()) /1000);
 
          
          Map<id,CC_Order_Shipment__c> ordershipMap = new Map<id,CC_Order_Shipment__c>();
          if(ordershiplist.size()>0){
          for(CC_Order_Shipment__c ccs:ordershiplist){
              ordershipMap.put(ccs.order__c,ccs);
            }
          }
          for(CC_Order__c cco:lstOrder){
              String invoiceName = 'Not Closed';
              String invoiceNum;
              if(ordershipMap.get(cco.id) != null && ordershipMap.get(cco.id).Order__r.CC_Status__c =='Closed'){
              invoiceName = String.valueOf(ordershipMap.get(cco.id).Invoice_Number__c)+'_'+String.valueOf(ordershipMap.get(cco.id).Order__r.CC_Account__r.CC_Customer_Number__c)+'_'+String.valueOf(ordershipMap.get(cco.id).Invoice_Date__c);
              System.debug('****Invoice Name'+invoiceName);
              invoiceNum = String.valueOf(ordershipMap.get(cco.id).Invoice_Number__c);
              System.debug('****Invoice Name'+invoiceNum);
              
              }
              
              orderwithIVllist.add(new orderWrapper(cco,invoiceName,invoiceNum));
              
          }
 
          
         
          return orderwithIVllist ;              
              
          }
          
       
       
      public Map<String,String> setMapdataForOrderInvoiceLinkage(List<CC_Order__c> tempOrderList){
          //**********declaring local variables***********//
          List<CC_Invoice2__c> tempInvoicesList=new List<CC_Invoice2__c>();
          Set<String> customerNumberSet=new Set<String>();
          Map<String, String> mapInvoiceOrderData=new Map<String,String>();
          //**********declaring local variables***********//
          //collecting the set of customer numbers into a set to filter the number of invoices.
          for(CC_Order__c ordr: tempOrderList){
            customerNumberSet.add(ordr.CC_Customer_Number__c);
          }
          
          //code contributed by @Priyanka Baviskar for issue no 7592100.
            if(customerNumberSet.size()>0){
                //fetching all the invoices records from Invoice object and putting it into a local variable with the customer number filter
                tempInvoicesList=[select id,Customer_Number__c,CC_Reference_Number__c from CC_Invoice2__c where CC_sequence__c=1 and Customer_Number__c IN:customerNumberSet];
            }
                
          
          
          return mapInvoiceOrderData;
      }
      
      public class orderWrapper{
         public CC_Order__c ccord {get;set;}
         public STring invoiceNAme {get;set;}
         public String invoiceNumber{get;set;}
         public orderWrapper(CC_Order__c ccorderlist, String invname, String invNum ){
             ccord = ccorderlist;
             invoiceNAme = invname;
             invoiceNumber = invNum;
         }
      }
          
     

}