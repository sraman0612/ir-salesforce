public class CC_allInvoiceLineController {
  
  private CC_Invoice2__c inv;
  public List<CC_Invoice2__c> invLineLst {get;set;}
  
  public CC_allInvoiceLineController(ApexPages.StandardController controller){
    this.inv = (CC_Invoice2__c)controller.getRecord();
    invLineLst = new List<CC_Invoice2__c>();
    for (CC_Invoice2__c i : [SELECT Id,CC_Item_Number__c,CC_Item_Description__c,CC_Unit_Measure__c,CC_Qty_Shipped__c,CC_Qty_Back_Ord__c,CC_Unit_Price__c,CC_Line_Net_Sale_Amt__c
                               FROM CC_Invoice2__c 
                              WHERE Name = :inv.Name ORDER BY CC_sequence__c]){
      invLineLst.add(i);
    }
  }
  
  public PageReference redirector(){
    CC_Invoice2__c i = [SELECT Name FROM CC_Invoice2__c WHERE id = :ApexPages.currentPage().getParameters().get('id')];
    PageReference newPage = new PageReference('/apex/CC_PavilionInvoicedetail?invid=' + i.name);
    return newPage;
  }
}