global class cc_PavilionHomeController {
  /* member variables */
  public List<String> AllListofprofiles {get;set;}
  Public List < CC_Pavilion_Content__c > pavilionTileSettings {get;set;}
  Public List < string > leftselected  {get;set;}
  Public List < string > RightSelected {get;set;}
  Public List < string > Leftvalues  {get;set;}
  Public User usertoUpdate  {get;set;}
  Public List < string > selectedTiles  {get;set;}
  Public List < CC_Pavilion_Content__c > selectedcontentTiles  {get;set;}
  public static AccountTeamMember teamMemberInfo  {get;set;}
  public static List < AccountTeamMember > teamMemberInfo1  {get;set;}
  public static List < User > usr  {get;set;}
  public static Id usrId  {get;set;}
  public static Id acountId;
  public List<String> permittedValues;
  public List < CC_Pavilion_Content__c > lstDymaniccontent  {get;set;}
  public String imagelink  {get;set;}
  public String imgkey  {get;set;}
  public String messageOnTop{get{return [select  CC_Body_1__c from CC_Pavilion_Content__c where Title__c =:'errorMessage'][0].CC_Body_1__c;}set;}
  //ALL THE STUFF FROM ALERTS CTLR
  public List<String> alertsAndNotificationsList{get;set;}
  public List<Case> caseList;
  public List<Case> resolvedCaseList;
  public List<Case> systemAccessCaseList;
  public List<CC_Pavilion_Content__c> pavilionContentList ;
  public String AccountId{get;set;}
  public Pavilion_Navigation_Profile__c naviProf;
  public String pavilionAdminEmails{get;set;}
  public List<string>admins{get;set;}

  public String adminsEmail{get;set;}
  public Id accId ;
        
  public cc_PavilionHomeController(CC_PavilionTemplateController controller) {
   accountID=controller.acctId;
   naviProf=controller.navProfile;
   
   //code contributed by @Priyanka Baviskar for issue no 7621178.
   admins = new List<String>();
    usrId  = userinfo.getUserId();
   
     accId = [SELECT AccountId from User  where id =:usrId].AccountId;
  List<User> userList= [select id,Email,CC_Pavilion_Navigation_Profile__c from user where AccountId =:accId and CC_Pavilion_Navigation_Profile__c like '%ADMIN'  ];
              
        for(User p: userList){
            if(pavilionAdminEmails != null && pavilionAdminEmails != ''){ 
                pavilionAdminEmails += '; '+p.Email ;
            }
            else {
                pavilionAdminEmails = p.Email ;
            } 
            admins.add(p.Email);
            adminsEmail= string.join(admins,',');
           }
   
   //START ALERT CTLR STUFF
    
  
    usrId  = Userinfo.getUserId();
    Id ContId = [SELECT ContactId from User where id =:usrId].ContactId;
    
    alertsAndNotificationsList = new List<String>();
    
    //National Account Orders
    Integer daysInt = null==PavilionSettings__c.getInstance('NATACCTALERTDAYS')?2:integer.valueOf(PavilionSettings__c.getInstance('NATACCTALERTDAYS').Value__c);        
    Date orderDate = system.today().addDays(daysInt *-1);
    
    for (CC_Order__c o : [SELECT id,Name,Order_Number__c,CC_Account__c,CC_Servicing_Dealer__c,CreatedDate 
                          FROM CC_Order__c WHERE (CC_Servicing_Dealer__c =: accountID AND CC_Account__c !=: accountID) 
                          AND CreatedDate = LAST_N_DAYS:14 ORDER BY CreatedDate DESC]){
      alertsAndNotificationsList.add('New order# ' + o.Order_Number__c + ' has been added to your National/Strategic Accounts view');
    }
    
    Case[] trainingCaseLst = [SELECT CC_Courses__c, CC_Date_Of_Course__c,CC_Time_Of_Course__c 
                                FROM Case 
                               WHERE ContactId =: usrId and RecordType.DeveloperName='Club_Car_Training_Request'
                            ORDER BY createdDate DESC LIMIT 1];
    
    for(Case trc : trainingCaseLst) {  
      if(trc.CC_Courses__c != null && trc.CC_Date_Of_Course__c != null) {
          String dateOfCourse = String.valueof(trc.CC_Date_Of_Course__c.day())+'/'+
                                String.valueof(trc.CC_Date_Of_Course__c.month())+'/'+
                                String.valueof(trc.CC_Date_Of_Course__c.year());
          String alert =  'You have registered for training "'+trc.CC_Courses__c+'" on '+dateOfCourse+' '+trc.CC_Time_Of_Course__c;
          alertsAndNotificationsList.add(alert);
      }
    }
    
    caseList = new List<Case>();
    //fetching most recently logged Cases
    caseList = [SELECT Id, ContactId,status, CaseNumber, CC_PavilionReason_for_Case__c, RecordType.Name, Subject
                FROM Case WHERE ContactId =: ContId and status != 'Closed' ORDER BY CreatedDate DESC LIMIT 5];
    for(Case c : caseList) {  
       //Notification new Case created
       system.debug('### case is ' + c);
       if(c.CaseNumber !=  null && c.subject != null){    
           //String caseNotification = 'You have Raised a Case (Case # '+c.CaseNumber+') with Category "'+c.CC_PavilionReason_for_Case__c+'"'; 
           String caseNotification = 'Case # <a href="/ClubCarLinks/s/case/' + c.Id + '/need-cards">' + c.CaseNumber + '</a> has been created with subject '+c.subject+'.'; 
           alertsAndNotificationsList.add(caseNotification);
        }
   }
   
   /*
   resolvedCaseList = new List<Case>();
   resolvedCaseList = [SELECT Id, ContactId,status, CaseNumber, CC_PavilionReason_for_Case__c, RecordType.Name, Subject
                        FROM Case WHERE  RecordType.Name = 'Club Car - Ask a Question' 
                        AND ContactId =: ContId AND status = 'Closed' ORDER BY lastmodifieddate DESC limit 1];
   
   for(Case c : resolvedCaseList) {  
   // fetching most recently Closed/Resolved case - Help Questions
        if(c.CaseNumber !=  null && c.CC_PavilionReason_for_Case__c != null){
            String caseNotification = 'Your Ticket (Ticket # '+c.CaseNumber+') on "'+c.CC_PavilionReason_for_Case__c+'" has been resolved'; 
            alertsAndNotificationsList.add(caseNotification);
        }
   }
   
   systemAccessCaseList = new List<Case>();
   systemAccessCaseList = [SELECT Id, AccountId, CC_User__r.Name,Subject, status, CaseNumber, RecordType.Name, Owner.Name
                        FROM Case WHERE Status = 'Closed' AND AccountId =: accountID AND ClosedDate = LAST_N_DAYS:7 ORDER BY LastModifiedDate DESC];
   
   String caseMaintainUserNotification = '';
   if(!systemAccessCaseList.isEmpty()){
        for(Case c : systemAccessCaseList) {  
        //Non-Help Admin Cases Resolved Notifications
            if(c.RecordType.Name == 'Club Car - Maintain User Info'){
                if(c.Owner.Name == 'User Deactivated'){
                    caseMaintainUserNotification = 'Case # '+c.CaseNumber+' is now Resolved and '+c.CC_User__r.Name +' has been De-Activated'; 
                    alertsAndNotificationsList.add(caseMaintainUserNotification);
                } else if (c.Owner.Name == 'User Reactivated'){
                    caseMaintainUserNotification = 'Case # '+c.CaseNumber+' is now Resolved and '+c.CC_User__r.Name +' has been Re-Activated';
                    alertsAndNotificationsList.add(caseMaintainUserNotification);
                } else if (c.Owner.Name == 'New User PartCart Queue'){
                    caseMaintainUserNotification = 'Case # '+c.CaseNumber+' is now Resolved and '+c.Subject+ ' for '+c.CC_User__r.Name+'';
                    alertsAndNotificationsList.add(caseMaintainUserNotification);
                } else if (c.Owner.Name == 'New User Warranty Queue'){
                    caseMaintainUserNotification = 'Case # '+c.CaseNumber+' is now Resolved and '+c.Subject+ ' for '+c.CC_User__r.Name+'';
                    alertsAndNotificationsList.add(caseMaintainUserNotification);
                }
            }
            if(c.RecordType.Name == 'Club Car - New User'){
                    String caseNewUserNotification = 'Case # '+c.CaseNumber+' is now Resolved and new Club Car Links user has been created for '+c.CC_User__r.Name+'';
                    alertsAndNotificationsList.add(caseNewUserNotification);
            }
            if(c.RecordType.Name == 'Club Car - CustomerVIEW Admin'){
              //@TODO Add CustomerVIEW Admin notifications  
            }
        }
   }*/
   
    pavilionContentList = new List<CC_Pavilion_Content__c>();
    pavilionContentList = [SELECT Title__c,Summary__c, RecordType.DeveloperName FROM CC_Pavilion_Content__c 
                            WHERE RecordType.DeveloperName = 'CC_Alerts_Notifications' ORDER BY lastmodifieddate DESC]; 
    for(CC_Pavilion_Content__c pCon : pavilionContentList) {
        if(String.isNotBlank(pCon.Summary__c)) {alertsAndNotificationsList.add(pCon.Summary__c);} 
    }
   
   
   // END ALERT CTLR STUFF
        imagelink = 'resource/Parts/Parts/Parts_OrderPrivateSpeedCodes.svg';
        imgkey = 'Parts';
        Id uid = userinfo.getUserId();
        leftselected = new List < string > ();
        rightselected = new List < string > ();
        selectedTiles = new List < String > ();
        lstDymaniccontent = new List < CC_Pavilion_Content__c > ();
        selectedcontentTiles = new List < CC_Pavilion_Content__c > ();
        Leftvalues = new List < String > ();
        pavilionTileSettings = [SELECT id, name, Link__c, Resource_Link__c, showOnPavilionHome__c, Tile_Page__c, Title__c, Type__c FROM CC_Pavilion_Content__c WHERE Type__c = 'Home Page Tile' AND RecordType.Name = 'Home Page Tiles'];//Get all list of tiles
        
      
        usertoUpdate = [SELECT id, name, Pavilion_Tiles__c FROM user WHERE id = : uid];
        
        if(usertoUpdate.Pavilion_Tiles__c != null) {
            for (integer i = 1; i < usertoUpdate.Pavilion_Tiles__c.split(';').size(); i++) {
                selectedTiles.add(usertoUpdate.Pavilion_Tiles__c.split(';')[i]);
            }
        }
 
        permittedValues = new List < String >();
        permittedValues  = getAllListofprofiles();
      
        if(!permittedValues.isEmpty()){
            for (CC_Pavilion_Content__c content: [SELECT id, name, Link__c, Link_Text__c, Resource_Link__c, showOnPavilionHome__c, Tile_Page__c, Title__c, Type__c 
                                                 FROM CC_Pavilion_Content__c
                                                 WHERE Type__c = 'Home Page Tile' AND CC_isTile__c=true AND RecordType.Name = 'Home Page Tiles' AND Resource_Link__c != '' AND Resource_Link__c != null AND Title__c in : selectedTiles  AND CC_Pavilion_Tiles_Navigation__c IN : permittedValues Order by Title__c]) {
                selectedcontentTiles.add(content);
            }
        }
         
        

    }
    
    /* ListofSelectOptions For Unselected Tiles */
        /* ListofSelectOptions For Tiles to Select*/
    public List<SelectOption> getunselectedvalues(){
      List<SelectOption> options = new List<SelectOption>();
      for (CC_Pavilion_Content__c tile: [SELECT Title__c,CC_Pavilion_Tiles_Navigation__c FROM CC_Pavilion_Content__c WHERE Title__c Not in : selectedTiles AND CC_isTile__c=true ORDER BY Title__c]) {
        if(naviProf.get(tile.CC_Pavilion_Tiles_Navigation__c) == true){options.add(new SelectOption(tile.Title__c, tile.Title__c));}
      }
      return options;
    }
    
    
    /* ListofSelectOptions For Selected Tiles */
    public List < SelectOption > getSelectedValues() {
        List < SelectOption > option1 = new List < SelectOption > ();
        
        if (usertoUpdate.Pavilion_Tiles__c != null) {
            for (string tile: selectedTiles) {
                option1.add(new SelectOption(tile, tile));
            }
        }
        
        return option1;
    }
    
    /* Method to Save User Tiles */
    public PageReference save() {
        usertoUpdate.Pavilion_Tiles__c = apexpages.currentpage().getparameters().get('rightpanel');
        
        update usertoUpdate;
        
        Pagereference pref = new Pagereference('/CC_PavilionHome');
        pref.setredirect(true);
        
        return pref;
    }
    
    /* Action Method for Home Page */
    public PageReference redirectGuest() {
      /* warm invcoie history query cache if needed */
      //COMMENTED 10MAY2017 by Ben Lorenz
      //keeping through fall 2017 in case performance issues recurr
      //if (naviProf.showAccountInvoiceHistory__c) {
        //system.debug('### this user needs inv hist query cache warmed');
        /* NOTES
          SESSION CACHE TAKES UP 35 KB
          ORG CACHE TAKES UP 48 KB
          Using org cache will take up less space due to only 1 cache per org(v. 1 per session) and use less future methods to warm the query cache
        */
        //string cacheKey = 'local.ClubCar.' + accountID;
        //if (null!=Cache.Org.get(cacheKey)){
          //cache already warmed
          //system.debug('### cache already warmed');
        //} else {
          //system.debug('### warming the cache');
          //Cache.Org.put(accountID,'',7200);
          //warmInvoiceHistoryQueryCache(accountID);
        //}
      //}
      /* redirect guest to login page */
      if (UserInfo.getUserType().equals('Guest')){return new PageReference('/login');}
      return null;
    }
    
    /* future method to warm the invoice hisotry query cache */
    @future
    private static void warmInvoiceHistoryQueryCache(string aid){
      CC_Invoice2__c [] invLst = CC_PavilionInvoiceHistoryController.getInvoices(system.today().addDays(-365),system.today(),aid);
    }
    
    /* remote action to display OrderItems*/
    //@TODO pass in accountID from page
    @RemoteAction
    global static Set<ContentVersion> getOrderItems(String accountID) {
      String lastid = '';
      List<string> agrContent = new List <string> ();
      map<string,string> accountmap = new map <string,string> ();
      List < Contract > cont = new List < Contract > ();
      Account acc = [SELECT Id, CC_Global_Region__c, CC_Agreement_Type__c, CC_Bulletins_Currency__c FROM Account WHERE Id = : accountID];
      for (Contract c: [SELECT id, name, CC_Sub_Type__c, CC_Type__c 
                          FROM Contract 
                         WHERE AccountId = : accountID and CC_Type__c = 'Dealer/Distributor Agreement' and 
                               CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and 
                               CC_Contract_End_Date__c >=:system.today()]) {
        if (c.CC_Sub_Type__c != null && c.CC_Sub_Type__c != '') {accountmap.put(c.CC_Sub_Type__c, '');}
      }
      List<ContentVersion> internalcontentResource = new List<ContentVersion>();
      set<ContentVersion> finalcontentResource = new set<ContentVersion>();
      internalcontentResource = [SELECT id, Content_Title__c,Bulletin_Title__c, Title,Agreement_Type__c, Currency__c,RecordType.Name 
                                   FROM ContentVersion
                                  WHERE RecordType.Name = 'bulletin' and 
                                        Currency__c INCLUDES(: acc.CC_Bulletins_Currency__c) and 
                                        CC_Region__c INCLUDES(: acc.CC_Global_Region__c)
                               ORDER BY Createddate desc
                                  LIMIT 3];
      for (ContentVersion c: internalcontentResource){
        if (c.Agreement_Type__c != null) {
          agrContent = c.Agreement_Type__c.split(';');
          for (string s: agrContent) {
            if (accountmap.containskey(s)) {
              finalcontentResource.add(c);
            }
          }
        }
      }
      return finalcontentResource;
    }
    
    /* remote action to display bulletins */
    @RemoteAction
    global static List < CC_Pavilion_Content__c > getPavilionContent(String accountID, String gRegion) {
      CC_Pavilion_Content__c [] bulletinLst= new List<CC_Pavilion_Content__c>();
      
      Set<String> SubType = new Set<String>();
      for(Contract Con : [SELECT Id, AccountId, CC_Sub_Type__c, CC_Type__c 
                            from Contract 
                           where AccountId = : accountID AND CC_Type__c = 'Dealer/Distributor Agreement' and 
                                 CC_Contract_Status__c != 'Suspended' and CC_Contract_Status__c != 'Terminated' and CC_Contract_Status__c != 'Expired' and 
                                 CC_Contract_End_Date__c >=:system.today()])
      {
          SubType.add(con.CC_Sub_Type__c);
      }
      
      
      
      for(CC_Pavilion_Content__c PC: [SELECT Id, Title__c, Summary__c, Link__c, Agreement_Type__c, CC_Region__c 
                                        FROM CC_Pavilion_Content__c 
                                       WHERE RecordType.Name = 'Home Page Bulletins' AND Type__c = 'Home Page Bulletin' AND showOnPavilionHome__c = true])
      {
          if(PC.Agreement_Type__c != null && PC.Agreement_Type__c != '' && PC.CC_Region__c != '' && PC.CC_Region__c != NULL){
              
              List<String> agreementTypeList = new List<String>();
              agreementTypeList = PC.Agreement_Type__c.split(';');
              
              for(String agType : agreementTypeList) {
                  if(SubType.contains(agType) && PC.CC_Region__c.contains(gRegion)){
                      bulletinLst.add(PC);
                      break;
                  }
              } 
          }
      }
      
      return bulletinLst;          
}

    /* remote action to display Account Team Member Role */
    @RemoteAction
    global static List < AccountTeamMember > getTeamMemberRole(String accountID) {
        teamMemberInfo1 = [Select Id, UserID, TeamMemberRole FROM AccountTeamMember WHERE AccountID = : accountId and TeamMemberRole = 'Sales Rep' LIMIT 1];
        if(teamMemberInfo1 == null || teamMemberInfo1.size() < 1)
            return new List < AccountTeamMember >();
        return teamMemberInfo1;
    }
    
    /*
    @Author Sudeep kumar
    @Functionality TO add Security for Set your Tiles
    */
    
    public List<String> getAllListofprofiles(){
    //try{
        List<String> finalDisplayOptions = new  List<String>();
        List<String> finalFieldList = new     List<String>();
        Schema.DescribeSObjectResult  dsor = Pavilion_Navigation_Profile__c.sObjectType.getDescribe();
        Map<String,Schema.DescribeFieldResult> finalMap =  new Map<String, Schema.DescribeFieldResult>();
        Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
        Set<String> fields = objectFields.keySet();
        for(String field : fields){
            // skip fields that are not part of the object
            if (objectFields.containsKey(field)) {
              Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
              // add the results to the map to be returned
                if(dr.isCustom() == true){// &&   (DisplayType){'Boolean'}==  dr.getType()){
                    finalMap.put(field, dr); 
                    finalFieldList.add(field);
                }
            }
        }


        Id uid = userinfo.getUserId();

        String profName = [select Id,   CC_Pavilion_Navigation_Profile__c    from User where id=: uid ][0].CC_Pavilion_Navigation_Profile__c;
        String Qry = 'Select Id' ;
        for(String fname :finalFieldList){
            Qry= Qry +','+fname;
        }
        Qry = Qry + ' from  Pavilion_Navigation_Profile__c   where  Name = \''+profName +'\'';
        List<Pavilion_Navigation_Profile__c> p = Database.Query(Qry);
        Set<String> tmp = new Set<String>();
        if(!p.isEmpty()){
            if(p[0] != null){
                for(String fld: finalFieldList ){
                    if(p[0].get(fld) == true){
                        tmp.add(fld);
                    }
                }
            }
        }

        finalDisplayOptions.addAll(tmp);
        return finalDisplayOptions;

    }
    
    
}