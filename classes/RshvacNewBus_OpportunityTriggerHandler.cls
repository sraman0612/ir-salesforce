public class RshvacNewBus_OpportunityTriggerHandler{
    
    // Instance of the helper class OpportunityUtils
    OpportunityUtils oppUtils;
    // Map for collecting opportunities' Accounts.
    public static Map<id, Account>  accMap = new Map<id, Account>();
    // Map for collecting Residential Quotas
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<String, Residential_Quota__c> ResidentialQuotaMap = new Map<String, Residential_Quota__c>();
    // List of Residential Quotas to update
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static List<Residential_Quota__c> ResidentialQuotaToUpdate = new List<Residential_Quota__c>();
    // Getting Id of Siebel Integration user from UserDetails custom setting
    public static RSHVAC_Properties__c IntegrationUser = RSHVAC_Properties__c.getValues('Integration User');
    // This map will contain the IWD Existing Opportunity Id corresponding to each New Business IWD Opportunity
    public static Map<Id, Id> oppIdByNewBusOpp = new Map<Id, Id>();
    // Map that will contain all the Opportunities related to the Orders being processed
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>();
    // Map that will contain all the Opportunities related to the New Business Opportunities being processed
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Opportunity> newBusinessOpportunitiesMap = new Map<Id, Opportunity>();
    // Map that will contain all the Residential Quotas related to the User
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Residential_Quota__c> resQuotaByUserMap = new Map<Id, Residential_Quota__c>();
    // Map that will contain all the Residential Quotas related to the User
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Residential_Quota__c> resQuotasToUpdate = new Map<Id, Residential_Quota__c>();
    // Map that will contain all the Revenue Schedules corresponding to each Opportunity
    // The key of this map will be the Id of the Opportunity
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Opportunity_Revenue_Schedule__c> revenuesMap= new Map<Id, Opportunity_Revenue_Schedule__c>();
    // Map that will contain all the Revenue Schedules that need to be updated
    // The key of this map will be the Id of the Opportunity
    // Case 27331 - Ronnie Stegall 1/8/2018 - variable no longer needed
    //public static Map<Id, Opportunity_Revenue_Schedule__c> revenuesToUpdate= new Map<Id, Opportunity_Revenue_Schedule__c>();
    // Getting Project Type values for BDM, MDM and RM
    public static final string SINGLE_FAMILY_NC = 'Single Family - NC';
    public static final string MULTI_FAMILY_NC = 'Multi-Family - NC';
    public static final string MULTI_FAMILY_REPLACEMENT = 'Multi-Family - Replacement';
    public static final string SINGLE_FAMILY_NOO = 'Single Family Non Owner Occupied';
    public static final string COMMERCIAL = 'Commercial';
    // Account Sales Channel
    public static final String DSO = 'DSO';
    public static final String IWD = 'IWD';
    // IWD New Business record type developer name
    public static final String IWD_NEW_BUSINESS_RT_NAME = 'IWD_New_Business';
    // DSO New Business record type developer name
    public static final String DSO_NEW_BUSINESS_RT_NAME = 'RS_Business';
    // IWD New Business record type Id
    public static Id DSO_NEW_BUSINESS_RT_ID = OpportunityUtils.OPP_RECORD_TYPES.get(DSO_NEW_BUSINESS_RT_NAME).Id;
    // DSO New Business record type Id
    public static Id IWD_NEW_BUSINESS_RT_ID = OpportunityUtils.OPP_RECORD_TYPES.get(IWD_NEW_BUSINESS_RT_NAME).Id;
    // Get the Id of the Opportunity Existing BUsiness Record Type
    public static Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;
    
    // Updates Owner of the opportunity to Cost Center's BDM or MDM based on the type(If the current user is RHVAC Integration user). 
    // Updates Todays_Claim_Billed__c based on amount added to Gross_Sales__c. 
    public static void beforeInsertUpdate(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap, Boolean insertOperation){
        Set<id> accountIdSet = new Set<id>();
        // Collecting accounts related to new opportunities.
        for(Opportunity opp: newOpportunityList){
            if(opp.AccountId != null){
                accountIdSet.add(opp.AccountId);
            }
        }
        for(Account acc: [select id, Cost_Center_Name__r.BDM__c,Cost_Center_Name__r.OwnerId, Cost_Center_Name__r.MDM__c from Account where id in :accountIdSet]){
             accMap.put(acc.id, acc);   
        }
        
        // For insert operation update Todays_Claim_Billed__c as Gross_Sales__c. Update Last_Billed_Date__c as today.
        if(insertOperation){
          for(Opportunity newOpp: newOpportunityList){
            // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
            // being displayed on a Tableau report now.  
            //newOpp.Todays_Claim_Billed__c = newOpp.Gross_Sales__c;
            //newOpp.Last_Billed_Date__c = Date.today();
            // If the logged in user is RHVAC Integration user, update the owner of opportunity based on type. 
            if (IntegrationUser!= Null && IntegrationUser.Value__c.contains(Userinfo.getName()) && accMap.containsKey(newOpp.AccountId) && newOpp.Type != null){ //
              updateOwner(newOpp);
            }
          }
        }
        // For update operation  
        if(!insertOperation){
          for(Opportunity newOpp: newOpportunityList){
            Opportunity oldOpp = (Opportunity)oldOpportunityMap.get(newOpp.Id);
            // If Gross_Sales__c is changed update Todays_Claim_Billed__c as the amount added to Gross_Sales__c. Update Last_Billed_Date__c as today.
            // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed because this data is
            // being displayed on a Tableau report now.  
            //if (newOpp.Gross_Sales__c != null && (oldOpp.Gross_Sales__c == Null || oldOpp.Gross_Sales__c != newOpp.Gross_Sales__c)){
            //  newOpp.Last_Billed_Date__c = Date.today();
            //  newOpp.Todays_Claim_Billed__c = newOpp.Gross_Sales__c;
            //  newOpp.Todays_Claim_Billed__c -= oldOpp.Gross_Sales__c != null ? oldOpp.Gross_Sales__c : 0;
            //  newOpp.Todays_Claim_Billed__c += oldOpp.Todays_Claim_Billed__c != null ? oldOpp.Todays_Claim_Billed__c : 0;
            //}
            // If the logged in user is RHVAC Integration User and Type of opportunity is not blank then update Opportunity owner. 
            if (accMap.containsKey(newOpp.AccountId) && newOpp.Type != null && IntegrationUser.Value__c.contains(Userinfo.getName()) && 
                (( newOpp.AccountId != null && (oldOpp.AccountId == null || oldOpp.AccountId != newOpp.AccountId))||
                 (oldOpp.Type == null || oldOpp.Type != newOpp.Type))){
              updateOwner(newOpp);
            }
          }
      }

    }

    // Helper method to update Opportunity owner based on type.
    public static void updateOwner(Opportunity newOpp){
      //Ticket: 8271377 - Jehri Reed 4/10/2019 - Update Code based on new hierarchy
      //Assigning BDM (RNC) as owner of opportunity if Project Type comes under BDM
        if((newOpp.Type == SINGLE_FAMILY_NC) && accMap.get(newOpp.AccountId).Cost_Center_Name__r.BDM__c != Null){
        newOpp.OwnerId = accMap.get(newOpp.AccountId).Cost_Center_Name__r.BDM__c;    
      }
      //Assigning MDM (NOO) as owner of opportunity if Project Type comes under MDM
      else if((newOpp.Type == SINGLE_FAMILY_NOO || newOpp.Type == MULTI_FAMILY_REPLACEMENT || newOpp.Type == MULTI_FAMILY_NC) && accMap.get(newOpp.AccountId).Cost_Center_Name__r.MDM__c != Null){
        newOpp.OwnerId = accMap.get(newOpp.AccountId).Cost_Center_Name__r.MDM__c;    
      }
      //Assigning cost center owner as owner of opportunity if Project Type doesn't fall into SINGLE_FAMILY_NC, SINGLE_FAMILY_NOO, MULTI_FAMILY_REPLACEMENT, MULTI_FAMILY_NC
      else if(accMap.get(newOpp.AccountId).Cost_Center_Name__r.OwnerId != Null){
        newOpp.OwnerId = accMap.get(newOpp.AccountId).Cost_Center_Name__r.OwnerId;    
      }   
    }

    // Case 27331 - Ronnie Stegall 1/8/2018 - No longer keeping track of todays claim billed or RNC/NOO segment sales by opportunity type because this data is
    // being displayed on a Tableau report now. This code is also causing issues noted in ticket 7532561.
    // Updates Opportunity Revenue Schedules of related Account or Cost Center. 
    // Updates Residential Quota of the BDM, MDM and RM of Cost Center based on the type and Record Type of opportunity.
    /*
    public static void afterInsertUpdate(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap, Boolean insertOperation){
      revenuesMap = new Map<id, Opportunity_Revenue_Schedule__c>();
      resQuotaByUserMap = new Map<Id, Residential_Quota__c>();
      resQuotasToUpdate = new Map<Id, Residential_Quota__c>();
      revenuesToUpdate= new Map<Id, Opportunity_Revenue_Schedule__c>();
      // Map for collecting new business opportunities based on Account(for DSO new business oppties) or Cost Center(for Non DSO new business opportunities).
      Map<Id, Set<Id>> opptiesByAccountOrCostCenter = new Map<Id, Set<Id>>();
      // Finding the month of yesterday for DSO new business opportunities.
      Date Yesterday = Date.today()-1;
      Integer monthOfYesterday = Yesterday.month();
      List<Id> costCenterList = new List<Id>();
      // List for collecting Cost Center Members.
      List<Id> dsoCostCenterMembersList = new List<Id>();
      List<Id> iwdCostCenterMembersList = new List<Id>();
      // Querying all the related object fields like related Cost Center's BDM, MDM, RM, etc.
      // Ronnie Stegall (3/6/2017) - instead of resetting the value of newOpportunityList, keep newOpportunityList as is and instead loop through query results.
      //newOpportunityList = [select id, Type, Belong_to_BDM__c, Belong_to_MDM__c, Todays_Claim_Billed__c, RecordTypeId, Account.RecordTypeId, Account.Cost_Center_Name__c, Account.Cost_Center_Name__r.BDM__c,Account.Cost_Center_Name__r.MDM__c,Account.Cost_Center_Name__r.OwnerId,Account.Channel__c
      //                         from Opportunity where Id in :newOpportunityList];
      // Ronnie Stegall (3/6/2017)- iterate using query instead of changing newOpportunityList fields by assigning output of query to newOpportunityList. 
      // Iterating through new opportunities.
      //for (Opportunity opp : newOpportunityList){
      for (Opportunity opp : [select id, Type, Belong_to_BDM__c, Belong_to_MDM__c, Todays_Claim_Billed__c, RecordTypeId, Account.RecordTypeId, Account.Cost_Center_Name__c, Account.Cost_Center_Name__r.BDM__c,Account.Cost_Center_Name__r.MDM__c,Account.Cost_Center_Name__r.OwnerId,Account.Channel__c
                               from Opportunity where Id in :newOpportunityList]){
        newBusinessOpportunitiesMap.put(opp.id, opp);
        //Collecting BDM, MDM and RM(i.e, Owner) of the Cost Center.
        if(opp.Account.Channel__c == DSO){
          dsoCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.BDM__c);
          dsoCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.MDM__c);
          dsoCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.OwnerId);
        }
        else if(opp.Account.Channel__c != DSO){
          iwdCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.BDM__c);
          iwdCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.MDM__c);
          iwdCostCenterMembersList.add(opp.Account.Cost_Center_Name__r.OwnerId);
        }// For non DSO Accounts collect opportunities by Cost Center Id. 
        if (opp.Account.Channel__c != DSO ){
          if (opptiesByAccountOrCostCenter.get(opp.Account.Cost_Center_Name__c) == null){
            opptiesByAccountOrCostCenter.put(opp.Account.Cost_Center_Name__c, new Set<Id>());
          }
          opptiesByAccountOrCostCenter.get(opp.Account.Cost_Center_Name__c).add(opp.Id);
          costCenterList.add(opp.Account.Cost_Center_Name__c);  
        }
        // For DSO Accounts collect opportunities by Account Id.
        if (opp.Account.Channel__c == DSO ){
          if (opptiesByAccountOrCostCenter.get(opp.AccountId) == null){
            opptiesByAccountOrCostCenter.put(opp.AccountId, new Set<Id>());
          }
          opptiesByAccountOrCostCenter.get(opp.AccountId).add(opp.Id);
          }
      }
      if(!opptiesByAccountOrCostCenter.isEmpty()){
        // Iterating through existing business opportunities of the related DSO Accounts and Non DSO Cost Centers.
        // Preparing Map with key as new business opportunity id and value as related existing business opportunity. 
        for(Opportunity exOpp: [SELECT Id, Amount, AccountId, Account.Channel__c, Cost_Center_Name__c FROM Opportunity WHERE (Cost_Center_Name__c IN :opptiesByAccountOrCostCenter.keySet() OR AccountId IN :opptiesByAccountOrCostCenter.keySet()) AND CloseDate = THIS_YEAR AND RecordTypeId = :existingBusinessRecTypeId]){          
            opportunitiesMap.put(exOpp.Id, exOpp);
            if(exOpp.Account.Channel__c !=Null && exOpp.Account.Channel__c == DSO && opptiesByAccountOrCostCenter.containsKey(exOpp.AccountId)){
              for (Id oppId : opptiesByAccountOrCostCenter.get(exOpp.AccountId)){
                oppIdByNewBusOpp.put(oppId, exOpp.Id);
              }
            }
            if(exOpp.Cost_Center_Name__c != Null && opptiesByAccountOrCostCenter.containsKey(exOpp.Cost_Center_Name__c)){
              for (Id oppId : opptiesByAccountOrCostCenter.get(exOpp.Cost_Center_Name__c)){
                oppIdByNewBusOpp.put(oppId, exOpp.Id);
              }
            }
          }
        }
      // Iterating through Current month and previous month's Opportunity Revenue Schedules of existing business opportunies 
      for (Opportunity_Revenue_Schedule__c rev : [SELECT Id, Opportunity__c, Actual__c, Multi_Family_NC_Actual__c, Multi_Family_Replacement_Actual__c,Single_Family_NC_Actual__c,Single_Family_NOO_Actual__c,Opportunity__r.Account.Channel__c,Opportunity__r.Cost_Center_Name__r.Channel__c,Forecast_Month__c FROM Opportunity_Revenue_Schedule__c 
                                                  WHERE Opportunity__c IN :opportunitiesMap.keySet() AND  ((Opportunity__r.Account.Channel__c != Null AND Opportunity__r.Account.Channel__c = :DSO AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfYesterday) OR (Opportunity__r.Cost_Center_Name__r.Channel__c != Null AND Opportunity__r.Cost_Center_Name__r.Channel__c != :DSO AND Forecast_Month__c = THIS_MONTH))]){
        // Preparing map with key as existing business opportunity id and and yesterday month's revenue schedule for DSO opportunity. 
        if (rev.Opportunity__r.Account.Channel__c != NULL && rev.Opportunity__r.Account.Channel__c == DSO && rev.Forecast_Month__c.Month() == monthOfYesterday){
          revenuesMap.put(rev.Opportunity__c, rev);
        }
        // Preparing map with key as existing business opportunity id and and today month's revenue schedule for Non DSO opportunity. 
        else if (rev.Opportunity__r.Cost_Center_Name__r.Channel__c != NULL && rev.Opportunity__r.Cost_Center_Name__r.Channel__c != DSO && rev.Forecast_Month__c.Month() == Date.today().Month()){
          revenuesMap.put(rev.Opportunity__c, rev);
        }
      }
      // Preparing Residential Quota map with key as user and record as value. 
      for (Residential_Quota__c resQuota: [Select id, User__c, Forecast_Month__c, Multi_Family_NC_Actual_DSO__c, Multi_Family_NC_Actual_IWD__c, Multi_Family_Replacement_Actual_DSO__c, Multi_Family_Replacement_Actual_IWD__c, Single_Family_NC_Actual_DSO__c, Single_Family_NC_Actual_IWD__c, Single_Family_NOO_Actual_DSO__c, Single_Family_NOO_Actual_IWD__c 
                                             From Residential_Quota__c where (User__c IN :iwdCostCenterMembersList AND Forecast_Month__c = THIS_MONTH) OR (User__c IN :dsoCostCenterMembersList AND CALENDAR_MONTH(Forecast_Month__c) = :monthOfYesterday)]){
        Set<Id> iwdCostCenterMembersSet = new Set<Id>(iwdCostCenterMembersList);
        Set<Id> dsoCostCenterMembersSet = new Set<Id>(dsoCostCenterMembersList);
        if(iwdCostCenterMembersSet.contains(resQuota.User__c) && resQuota.Forecast_Month__c.month() == Date.Today().Month()){
          resQuotaByUserMap.put(resQuota.User__c, resQuota);
        }
        else if(dsoCostCenterMembersSet.contains(resQuota.User__c) && resQuota.Forecast_Month__c.month() == monthOfYesterday){
          resQuotaByUserMap.put(resQuota.User__c, resQuota);
        }
      }
      for (Opportunity newOpp : newOpportunityList){
        // Get the existing business opportunity related to new business opportunity
        Id oppId = oppIdByNewBusOpp.get(newOpp.Id);        
        // There should always be one, but just in case
        if (oppId != null && opportunitiesMap.get(oppId) != null){
          // Update the Revenue Schedule Actual value
          // If it was already on the ToUpdate Map get the Revenue from it
          // That way we can make sure that we're updating the proper value
          Opportunity_Revenue_Schedule__c revenue = revenuesToUpdate.get(oppId) != null ? revenuesToUpdate.get(oppId) : revenuesMap.get(oppId);
          if (revenue != null){
            if(insertOperation){
              OpportunityUtils.updateExistingOpportunityRevenueScheduleForOpportunity(revenue, newOpp, null, False);
              revenuesToUpdate.put(oppId, revenue);
            }
            else if(!insertOperation){
                // Ronnie Stegall (3/6/2017) - Need old opportunity for comparison
                Opportunity oldOpp = (Opportunity)oldOpportunityMap.get(newOpp.Id);
                // Ronnie Stegall (3/6/2017) - Only call if If Gross_Sales__c is changed.  If changed update project type actual value on existing business opportunity revenue schedule. 
                if (newOpp.Gross_Sales__c != null && (oldOpp.Gross_Sales__c == Null || oldOpp.Gross_Sales__c != newOpp.Gross_Sales__c)){
                    OpportunityUtils.updateExistingOpportunityRevenueScheduleForOpportunity(revenue, newOpp, oldOpportunityMap.get(newOpp.Id), True);
                    revenuesToUpdate.put(oppId, revenue);
                }
            }
            // Ronnie Stegall (3/6/2017) - Moving this to inside if statement above.  This should only be called if opportunity gross sales changed or new opportunity.
            // revenuesToUpdate.put(oppId, revenue);
          }
        }
        // Update Residential Quota of the respective users. 
        if (newBusinessOpportunitiesMap.containsKey(newOpp.Id)){
          Opportunity newBusOpp = newBusinessOpportunitiesMap.get(newOpp.Id);
          if(insertOperation){
            OpportunityUtils.updateResidentialQuota(newBusOpp, Null, False); 
          }
          else if(!insertOperation){
              // Ronnie Stegall (3/6/2017) - Need old opportunity for comparison
                Opportunity oldOpp = (Opportunity)oldOpportunityMap.get(newOpp.Id);
            // Ronnie Stegall (3/6/2017) - Only call if If Gross_Sales__c is changed.  If changed update project type actual value on residential quota for RM and BDM/MDM             
            if (newOpp.Gross_Sales__c != null && (oldOpp.Gross_Sales__c == Null || oldOpp.Gross_Sales__c != newOpp.Gross_Sales__c)){
                OpportunityUtils.updateResidentialQuota(newBusOpp, oldOpportunityMap.get(newBusOpp.Id), True); 
            }
          }
        }   
      }
      // If there are Revenue Schedules to be updated. 
      if (!revenuesToUpdate.isEmpty()){
        update revenuesToUpdate.values();
      }
      // If there are Residential Quotas to be updated. 
      if (!resQuotaByUserMap.isEmpty()){
        // Ronnie Stegall (3/6/2017) - Update - should be updating resQuotasToUpdate instead of resQuotaByUserMap
        //update resQuotaByUserMap.values();
        update resQuotasToUpdate.values();         
      }
    }
    */
    
 /*public static void afterUpdate(List<Opportunity> newList)
    {   
        List<RSHVAC_Quote__c> RSHVACQuotes = new List<RSHVAC_Quote__c>();
        for (Opportunity opp :newList) 
        {
            if(opp.StageName == 'Closed Lost')
            {            
                RSHVACQuotes = [select Win_Loss__c,Reason_For_Loss__c from RSHVAC_Quote__c where Opportunity__c = :Opp.Id];
                for(RSHVAC_Quote__c RSQuote :RSHVACQuotes)
                {
                    if(RSQuote.Win_Loss__c != 'Loss')
                    {
                        RSQuote.Win_Loss__c = 'Loss';
                        RSQuote.Reason_For_Loss__c = opp.Closed_Lost_Reason__c;
                        System.debug('Updated RSHVACQuote ==' + RSQuote.Id + '\n');
                    }
                }
                update RSHVACQuotes;
            }  
        }    
    } */
    
}