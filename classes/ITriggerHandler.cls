/*
@Author : Snehal Chabukswar
@CreatedDate : 12 March 2020
@Description : Trigger Handler Interface
*/
public interface ITriggerHandler{
     /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework before insert of the records
        @params : List<sObject> newRecordsList 
    */
    void beforeInsert(List<sObject> newRecordsList);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework after insert of the records
        @params : Map<Id, sObject> newRecordsMap
    */
    void afterInsert(Map<Id, sObject> newRecordsMap);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework before update of the records
        @params : Map<Id, sObject> newRecordsMap,Map<Id, sObject> oldRecordsMap
    */
    void beforeUpdate(Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework after update of the records
        @params : Map<Id, sObject> newRecordsMap, Map<Id, sObject> oldRecordsMap
    */
    void afterUpdate(Map<Id, sObject> newRecordsMap,  Map<Id, sObject> oldRecordsMap);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework before delete of the records
        @params : List<sObject> newRecordsList , Map<Id, sObject> newRecordsMap
    */            
    void beforeDelete(Map<Id, sObject> oldRecordsMap);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework after delete of the records
        @params : Map<Id, sObject> oldRecordsMap
    */
    void afterDelete(Map<Id, sObject> oldRecordsMap);
    
    /*
        @Author : Snehal Chabukswar
        @BuiltDate : 12 March 2020
        @Description : Called by the trigger framework after undelete of the records
        @params : Map<Id, sObject> oldRecordsMap
    */
    void afterUnDelete( Map<Id, sObject> oldRecordsMap);
    /*
        @Author : Snehal Chabukswar
      	@BuiltDate : 12 March 2020
      	@Description : Called by the trigger framework to check the trigger for the object is enabled or disabled
      	@Parameters :
    */
    Boolean isDisabled();

}