public with sharing class CC_PavilionMarketingIdeasCnt
{

    /* member variables */
    public List<ContentVersion> contentList {get; set;}
    
    /* constructor */
    public CC_PavilionMarketingIdeasCnt(CC_PavilionTemplateController controller)
    {
        contentList=new List<ContentVersion>();      
        Showmarketingideas();
    }
    /* method for getting contentlist for marketing ideas recordtype  */
    Public Void Showmarketingideas()
    {
        contentList = [select id, title, ContentSize, Description,FileType, 
                       RecordType.Name from ContentVersion 
                       where RecordType.Name='Marketing Ideas'];
    }
}