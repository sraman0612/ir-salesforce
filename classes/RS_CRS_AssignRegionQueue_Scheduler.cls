/*****************************************************************
Name:      RS_CRS_AssignRegionQueue_Scheduler
Purpose:   Used to schedule RS_CRS_AssignRegionQueue_Batch
History                                                            
-------                                                            
VERSION     AUTHOR          DATE            DETAIL
1.0         Ashish Takke    02/05/2018      INITIAL DEVELOPMENT
*****************************************************************/
global class RS_CRS_AssignRegionQueue_Scheduler implements Schedulable {
    
    /*****************************************************************************
	Purpose            :  Schedules RS_CRS_AssignRegionQueue_Batch job
	******************************************************************************/
    global void execute(SchedulableContext SC) {
        RS_CRS_AssignRegionQueue_Batch batchJob = new RS_CRS_AssignRegionQueue_Batch();        
        Database.executebatch(batchJob);
    }
}