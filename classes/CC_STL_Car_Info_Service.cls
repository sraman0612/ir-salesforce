public with sharing class CC_STL_Car_Info_Service {

    public static void updateCarAccessoryLaborCost(Set<Id> stlCarIds){
        
        // Get the total labor cost for each of the cars that need updated
        AggregateResult[] aggResults = [Select STL_Car__c, SUM(Total_Labor_Cost__c) Accessory_Labor_Cost From CC_STL_Car_Accessory_Line_Item__c Where STL_Car__c in :stlCarIds Group By STL_Car__c];
        
        system.debug('aggResults: ' + aggResults);
                    
        // Assign the updated total labor cost to each of the STL cars                
        List<CC_STL_Car_Info__c> stlCars = new List<CC_STL_Car_Info__c>();      
        
        if (aggResults != null){
                  
            Set<Id> stlCarIdsProcessed = new Set<Id>();
            
            for (AggregateResult aggResult : aggResults){
                
                Id stlCarId = (Id)aggResult.get('STL_Car__c');
                stlCarIdsProcessed.add(stlCarId);              
                stlCars.add(new CC_STL_Car_Info__c(Id = stlCarId, Accessory_Labor_Cost__c = (Double)aggResult.get('Accessory_Labor_Cost')));
            }
            
            // Make sure to reset the labor cost to zero for any car that did not have an agg result (i.e. no line items)
            for (Id stlCarId : stlCarIds){
                
                if (!stlCarIdsProcessed.contains(stlCarId)){
                    stlCars.add(new CC_STL_Car_Info__c(Id = stlCarId, Accessory_Labor_Cost__c = 0));             
                }
            }
        } 
        
        sObjectService.updateRecordsAndLogErrors(stlCars, 'CC_STL_Car_Info_Service', 'updateCarAccessoryLaborCost');
    }
}