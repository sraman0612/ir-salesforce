@isTest
public with sharing class CC_PavilionSpeedCodeOrderCntTest {

    @testSetup
    private static void createData(){

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>(); 
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    
        Account acc= TestUtilityClass.createStatementCustomAccount('Sample account',2000,'Sample Address','12320','USA','Atlanta','550023',500,600,400,600);
        insert acc;

        Product2 pro=TestUtilityClass.createProduct2();
        pro.ProductCode='PASSCODE';
        insert pro;

        PavilionSettings__c newpsetting1 =TestUtilityClass.createPavilionSettings('PASSCODEPRICE','12345');
        PavilionSettings__c newpsetting2 =TestUtilityClass.createPavilionSettings('MAPICSSPEEDCODEENDPOINT','https://osbomwqa2.ingersollrand.com/CRM_SF/MapicsPartsOrder/ProxyServices/MapicsSpeedCodeReqABCSImpl_PS');
        insert new PavilionSettings__c[]{newpsetting1, newpsetting2};        
    }

    
    @isTest
    private static void testSerialNumCallout(){

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new CC_PavilionSpeedCodeOrderCntMock());

        CC_PavilionTemplateController controller;
        CC_PavilionSpeedCodeOrderCnt c = new CC_PavilionSpeedCodeOrderCnt(controller);   
        CC_PavilionSpeedCodeOrderCnt.serialNumCallout('html','css');
        CC_PavilionSpeedCodeOrderCnt.generateTag('String', 'tag three', 0.0);

        Test.stopTest();          
    }
    
    @isTest
    private static void testPurchaseSpeedCode1(){

        try{

            Test.setMock(HttpCalloutMock.class, new CC_PavilionSpeedCodeOrderCntMock());
            
            CC_PavilionSpeedCodeOrderCnt c = new CC_PavilionSpeedCodeOrderCnt(new CC_PavilionTemplateController());

            Account a = [Select Id From Account LIMIT 1];

            TestUtilityClass.callSerialNumCallOutFromUtility();

            Test.startTest();
            
            System.debug('Account Data'+[SELECT Id,Name,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry FROM Account WHERE Id =:a.id LIMIT 1]);
            System.debug(CC_PavilionSpeedCodeOrderCnt.purchaseSpeedCode('vsn','csn','sample custname', '11111111', 'codeA', 'codeB', 'codeC',a.id, false));  
            
            Test.stopTest();
        }
        catch(Exception e){}
    }

    @isTest
    private static void testPurchaseSpeedCode2(){

        insert new PavilionSettings__c(Name='PASSCODEPRICE', Value__c = '10.0');

        Account a = [Select Id From Account LIMIT 1];

        try{

            Test.startTest();

            CC_PavilionSpeedCodeOrderCnt.purchaseSpeedCode('vsn','csn','sample custname', '11111111', '10.00', '10.00', '10.00',a.id, false);
            CC_PavilionSpeedCodeOrderCnt.purchaseSpeedCode('vsn','csn','sample custname', '11111111', '1A', '1B', '1C',a.id, false);

            Test.stopTest();  
        }
        catch(Exception e){}
    }
    
    
    @isTest
    private static void testParseXML(){

        Test.startTest();

        XmlStreamReader reader = new XMLStreamReader('<xml><hello>HI</hello></xml>');
        CC_PavilionSpeedCodeOrderCnt.parseXML( reader);

        Test.stopTest();
    }    
}