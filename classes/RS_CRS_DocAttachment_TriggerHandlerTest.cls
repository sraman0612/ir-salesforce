/******************************************************************************
Name:       RS_CRS_DocAttachment_TriggerHandlerTest
Purpose:    Test class for RS_CRS_DocAttachment_TriggerHandler. 
History:                                                           
-------                                                            
VERSION     AUTHOR          DATE        DETAIL
1.0         Neela Prasad    12/26/2017  INITIAL DEVELOPMENT
******************************************************************************/
@isTest
public class RS_CRS_DocAttachment_TriggerHandlerTest {
    
    /******************************************************************************
	Method Name : sendEmailOnAttachment_Test
	Arguments   : No Arguments
	Purpose     : Method for Testing sendEmailOnAttachment Method
	******************************************************************************/
    Static testmethod void sendEmailOnAttachment_Test(){
        //get profile
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'RS_CRS_Escalation_Specialist'];
        
        //get user role
        UserRole userRole =new UserRole(Name= 'RS CRS Escalation Specialist'); 
        insert userRole;             
        
        //prepare username
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = prof.Id, UserRoleId = userRole.Id,
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName=uniqueUserName);
        
        //Getting Case Record Type Id through Schema.Describe Class
        Id caseRecId = RS_CRS_Utility.getRecordTypeIdFromRecordTypeName('Case', Label.RS_CRS_Case_Record_Type);
        
        System.runAs(userRec){
            
            //insert test case
            Case caseObj = new Case();
            caseObj.RecordTypeId = caseRecId;
            caseObj.Status = 'Customer Action';
            caseObj.RS_CRS_Send_Attachment_Email__c = false;
            insert caseObj;
            
            //insert test content document
            ContentVersion cv = new ContentVersion();
            cv.title = 'test content trigger';      
            cv.PathOnClient ='test';           
            cv.VersionData =Blob.valueOf('Unit Test Attachment Body');          
            insert cv;         
            
            ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
            
            ContentDocumentLink documentObject = new ContentDocumentLink();    
            documentObject.LinkedEntityId = caseObj.id;
            documentObject.ContentDocumentId = testContent.ContentDocumentId;
            documentObject.Visibility = 'AllUsers';
            documentObject.shareType = 'v';
            
            Test.startTest();
            
            insert documentObject;
            
            Test.stopTest();
            
	        System.assert(true, caseObj.RS_CRS_Send_Attachment_Email__c);
        }
    }
    
}