/* 
*  Snehal Chabukswar || Date 01/07/2020 
*  Test class for CTS_OM_SurveyRandomization batch class,CTS_OM_CalculateOpenCaseAgeScheduler and CTS_OM_UpdateCaseAge 
*/
@isTest
private class CTS_OM_SurveyRandomizationTest {
    private static testMethod void testBusinessHoursassignment(){        
        Account acct = TestDataUtility.createAccount(true, 'Test Account', Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId());  
        Contact ct1 = new Contact();
        ct1.Title = 'Mr.';
        ct1.phone= '1231345334563';
        ct1.FirstName = 'Jim';
        ct1.LastName = 'Test1';
        ct1.Email = 'abc1@testing.com';
        ct1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
        ct1.AccountId = acct.Id;
        insert ct1;
        
        Case caseObj = new Case();
        caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
        caseObj.Priority = 'Medium';
        caseObj.Status = 'New';
        caseObj.Origin = 'Email';
        caseObj.Type = 'Question';
        caseObj.SuppliedEmail = 'abc1@testing.com';
        caseObj.ContactId = ct1.Id;
        insert caseObj;
        test.startTest();
        Case newCaseObj = [SELECT Id,ContactId,CTS_OM_Send_Email_to_Contact__c,OwnerId FROM Case WHERE id=:caseObj.Id limit 1];
        system.debug('newCaseObj-------->'+newCaseObj.ContactId);
        String sch = '0 0 23 * * ?';
        system.schedule('Test status Check', sch, new CTS_OM_SurveyRandomizationBatchScheduler());
        test.stopTest();
        
        
        
    }
}