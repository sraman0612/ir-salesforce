// 
// (c) 2015 Appirio, Inc.
//
// Test Class for Event trigger
//
// Apr 30, 2015      Barkha Jain           Original: T-382779
//
@isTest
private class Event_TriggerHandler_Test {
    
    static testMethod void testEventTrigger() {
        Lead l = new Lead();
        l.firstname = 'George';
        l.lastname = 'Acker';
        l.phone = '1234567890';
        l.City = 'city';
        l.Country = 'USA';
        l.PostalCode = '12345';
        l.State = 'CA';
        l.Street = '12, street1';
        l.company = 'ABC Corp';
        l.county__c = 'test county';
        insert l;
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_EU' 
                                    and SobjectType='Account' limit 1].Id;        
        Account acc = new Account();
        acc.Name = 'testName';
        acc.BillingCity = 'city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '56';
        acc.BillingState = 'CA';
        acc.BillingStreet = 'billStreet';
        acc.ShippingCity = 'shipcity';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = 'shipStreet';
        acc.ShippingPostalCode = '67';
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'testCounty';   
        acc.recordtypeid=AirNAAcctRT_ID;
        insert acc;
        
        
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.stagename = 'Stage 1. Qualify';
        o.amount = 1000000;
        o.Type = 'New Complete Machine';
        o.AccountID = acc.Id;
        o.closedate = system.today();
        insert o;
        
        Event oppEvent = new Event();
        oppEvent.WhatId = o.Id;
        oppEvent.RecordTypeId=[Select Id, DeveloperName from RecordType where DeveloperName = 'NA_Air_Event' and SobjectType='Event' limit 1].Id;
        oppEvent.Type = 'Phone Call';
        oppEvent.Subject = 'TestAccName/Test Opportunity:testSub';
        
        Test.startTest();
        try {
            insert oppEvent; 
        }
        catch (Exception e){
            System.debug(e);
        }
        Test.stopTest();
    }

    
    static testMethod void testInactiveEvent() {
        // Insert a test account
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Status__c = 'Active';
        testAccount.Currency__c = 'USD';
        testAccount.ShippingCountry = 'US';
        testAccount.ShippingStreet = '123';
        testAccount.ShippingPostalCode = '123';
        testAccount.ShippingCity = 'test';
        testAccount.ShippingState = 'CA';
        testAccount.CTS_Global_Shipping_Address_1__c = '13';
        testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
        testAccount.County__c = '123';
        testAccount.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'GD_Parent' LIMIT 1].Id;
        insert testAccount;
        
        // Create a set of account IDs
        Set<ID> accIDs = new Set<ID>();
        accIDs.add(testAccount.Id);

        // Create a test event
        Event testEvent = new Event();
        testEvent.Subject = 'Test Event';
        testEvent.WhatId = testAccount.Id;
        testEvent.DurationInMinutes = 10;
        testEvent.ActivityDateTime = datetime.newInstance(2023, 9, 15, 12, 30, 0);
        testEvent.RecordTypeId = [Select Id,DeveloperName From RecordType Where DeveloperName = 'Standard_Event' LIMIT 1].id;
        insert testEvent;

        // Call the method and pass the set of account IDs and list of events
        //Event_TriggerHandler.inactiveEvent(accIDs, testEvents);

        // Assert that the method did not add any error to the test event
        System.assertEquals(0, testEvent.getErrors().size());
        
        testAccount.Status__c = 'Inactive';
        update testAccount;
        
        Event testEvent1 = new Event();
        testEvent1.Subject = 'Test Event1';
        testEvent1.WhatId = testAccount.Id;
        testEvent1.DurationInMinutes = 10;
        testEvent1.ActivityDateTime = datetime.newInstance(2023, 10, 15, 12, 30, 0);
        testEvent1.RecordTypeId = [Select Id,DeveloperName From RecordType Where DeveloperName = 'Standard_Event' LIMIT 1].id;

        Try{
        insert testEvent1;
            
        }catch(Exception exc){
          System.debug('Catch Error : '+exc.getMessage());
          System.assertEquals(true, exc.getMessage().contains('This Account is Inactive'));            
        }
        
    }


}