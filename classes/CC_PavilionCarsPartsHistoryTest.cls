@isTest
public class CC_PavilionCarsPartsHistoryTest
{
      static testmethod void UnitTest_CC_PavilionCarsPartsHistory()
    {
         Account acc = TestUtilityClass.createAccount();
        insert acc;
        CC_PavilionCarsPartsHistoryController CC_CarPartHist=new CC_PavilionCarsPartsHistoryController();
        CC_PavilionCarsPartsHistoryController.getOrderStatusData('FloorPlans',acc.id);
        CC_CarPartHist.userAccountId=acc.id;
    }
    
    static testmethod void UnitTest_CC_PavilionCarsPartsHistory1()
    {
         Account acc = TestUtilityClass.createAccount();
        insert acc;
        CC_PavilionCarsPartsHistoryController CC_CarPartHist=new CC_PavilionCarsPartsHistoryController();
        CC_PavilionCarsPartsHistoryController.getOrderStatusData('Approved',acc.id);
        CC_CarPartHist.userAccountId=acc.id;
    }
    
    static testmethod void UnitTest_CC_PavilionCarsPartsHistory2()
    {
         Account acc = TestUtilityClass.createAccount();
        insert acc;
        CC_PavilionCarsPartsHistoryController CC_CarPartHist=new CC_PavilionCarsPartsHistoryController();
        CC_PavilionCarsPartsHistoryController.getOrderStatusData('Pending',acc.id);
        CC_CarPartHist.userAccountId=acc.id;
    }
}