/* 
 *  Snehal Chabukswar || Date 01/07/2020 
 *  Test class for CTS_OM_CalculateOpenCaseAge batch class,CTS_OM_CalculateOpenCaseAgeScheduler and CTS_OM_UpdateCaseAge 
*/
@isTest
private class CalculateCaseAgeTestClass {
    private static testMethod void testBusinessHoursassignment(){
        
        Profile prof = [select id from profile where name = 'IR Comp EU Sales'];
     
        User u2 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                           Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test2', Lastname='Test2',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa'
                          );
        insert u2;
           PermissionSetAssignment assignedPS = new PermissionSetAssignment();
        assignedPS.PermissionSetId = [Select id from permissionset where name = 'IR_Comp_EU_Leadership'].id;
        assignedPS.AssigneeId = u2.id;
        insert assignedPS;
        
        system.runas(u2){
            Account acct = TestDataUtility.createAccount(true, 'Test Account');  
            Contact ct1 = new Contact();
            ct1.Title = 'Mr.';
            ct1.phone= '1231345334563';
            ct1.FirstName = 'Jim';
            ct1.LastName = 'Test1';
            ct1.Email = 'abc1@testing.com';
           // ct1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            ct1.AccountId = acct.Id;
            insert ct1;
            Case caseObj = new Case();
           // caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Ask_a_Question').getRecordTypeId();
            caseObj.Priority = 'Medium';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Type = 'Question';
            test.startTest();
            insert caseObj;
            caseObj.Status = 'Resolved';
            caseObj.CTS_OM_Case_Resolution_Notes__c = 'Test';
            caseObj.CTS_OM_Category__c = 'Pre Order';
            caseObj.CTS_OM_Category2__c = 'Lead Time Quotes';
            caseObj.CTS_OM_Category3__c = 'Standard Lead Time Quote';
            caseObj.CTS_OM_Disposition_Category__c = 'No Issues';
            caseObj.CTS_OM_Disposition_Category2__c = 'No Action Needed';
            caseObj.ContactId = ct1.Id;
            caseObj.CTS_OM_Sourcing_Location__c = 'US: Kent';
            
            try{
                update caseObj; 
            }
            catch (Exception e){
                system.debug(e);
            }
            test.stopTest();
        }
        
        
    }
     private static testMethod void testBusinessHoursassignment1(){
        
        Profile prof = [select id from profile where name = 'IR Comp EU Sales'];
        
        User u2 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                           Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test2', Lastname='Test2',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa');
        insert u2;
           PermissionSetAssignment assignedPS = new PermissionSetAssignment();
        assignedPS.PermissionSetId = [Select id from permissionset where name = 'IR_Comp_EU_Leadership'].id;
        assignedPS.AssigneeId = u2.id;
        insert assignedPS;
        system.runas(u2){
            Account acct = TestDataUtility.createAccount(true, 'Test Account');  
            Contact ct1 = new Contact();
            ct1.Title = 'Mr.';
            ct1.phone= '1231345334563';
            ct1.FirstName = 'Jim';
            ct1.LastName = 'Test1';
            ct1.Email = 'abc1@testing.com';
            ct1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            ct1.AccountId = acct.Id;
            insert ct1;
            Case caseObj = new Case();
          //  caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Ask_a_Question').getRecordTypeId();
            caseObj.Priority = 'Medium';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Type = 'Question';
            test.startTest();
            insert caseObj;
            caseObj.Status = 'Resolved';
            caseObj.CTS_OM_Case_Resolution_Notes__c = 'Test';
            caseObj.CTS_OM_Category__c = 'Pre Order';
            caseObj.CTS_OM_Category2__c = 'Lead Time Quotes';
            caseObj.CTS_OM_Category3__c = 'Standard Lead Time Quote';
            caseObj.CTS_OM_Disposition_Category__c = 'No Issues';
            caseObj.CTS_OM_Disposition_Category2__c = 'No Action Needed';
            caseObj.ContactId = ct1.Id;
            caseObj.CTS_OM_Sourcing_Location__c = 'US: Kent';
            
            try{
                update caseObj; 
            }
            catch (Exception e){
                system.debug(e);
            }
            test.stopTest();
        }
        
        
    }
    private static testMethod void testDefaultBusinessHoursassignment(){
        Profile prof = [select id from profile where name = 'System Administrator'];
        User u1 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test1@test.ingersollrand.com',Alias = 'batman',
                           Email='test1@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test', Lastname='Test',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA');
        
        insert u1;
        system.runas(u1){
            Account acct = TestDataUtility.createAccount(true, 'Test Account');  
            Contact ct1 = new Contact();
            ct1.Phone = '1324253455';
            ct1.Title = 'Mr.';
            ct1.FirstName = 'Jim';
            ct1.LastName = 'Test1';
            ct1.Email = 'abc1@testing.com';
            ct1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            ct1.AccountId = acct.Id;
            insert ct1;
            Case caseObj = new Case();
            caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
            caseObj.Priority = 'Medium';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Type = 'Question';
            test.startTest();
            insert caseObj;
            caseObj.Status = 'Resolved';
            caseObj.CTS_OM_Case_Resolution_Notes__c = 'Test';
            caseObj.CTS_OM_Category__c = 'Pre Order';
            caseObj.CTS_OM_Category2__c = 'Lead Time Quotes';
            caseObj.CTS_OM_Category3__c = 'Standard Lead Time Quote';
            caseObj.CTS_OM_Disposition_Category__c = 'No Issues';
            caseObj.CTS_OM_Disposition_Category2__c = 'No Action Needed';
            caseObj.ContactId = ct1.Id;
            caseObj.CTS_OM_Sourcing_Location__c = 'US: Kent';
            try
            {
            update caseObj;
            }
            catch(exception e)
            {
                system.debug(e);
            }
            test.stopTest();
        }
    }
    private static testMethod void testCTS_OM_CalculateOpenCaseAgeScheduler(){
        Profile prof = [select id from profile where name = 'System Administrator'];
        User u1 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test1@test.ingersollrand.com',Alias = 'batman',
                           Email='test1@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test', Lastname='Test',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa');
        
        insert u1;
        system.runas(u1){
            Account acct = TestDataUtility.createAccount(true, 'Test Account');  
            Contact ct1 = new Contact();
            ct1.Phone = '1324253455';
            ct1.Title = 'Mr.';
            ct1.FirstName = 'Jim';
            ct1.LastName = 'Test1';
            ct1.Email = 'abc1@testing.com';
            ct1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            ct1.AccountId = acct.Id;
            insert ct1;
            List<case> caseList = new List<Case>();
            Case caseObj = new Case();
            caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
            caseObj.Priority = 'Medium';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Type = 'Question';
            caseList.add(caseObj);
            Case caseObj2 = new Case();
            caseObj2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
            caseObj2.Priority = 'Medium';
            caseObj2.Status = 'New';
            caseObj2.Origin = 'Email';
            caseObj2.Type = 'Question';
            caseList.add(caseObj2);
            insert caseList;
            caseObj.Status = 'Resolved';
            caseObj.CTS_OM_Case_Resolution_Notes__c = 'Test';
            caseObj.CTS_OM_Category__c = 'Pre Order';
            caseObj.CTS_OM_Category2__c = 'Lead Time Quotes';
            caseObj.CTS_OM_Category3__c = 'Standard Lead Time Quote';
            caseObj.CTS_OM_Disposition_Category__c = 'No Issues';
            caseObj.CTS_OM_Disposition_Category2__c = 'No Action Needed';
            caseObj.ContactId = ct1.Id;
            caseObj.CTS_OM_Sourcing_Location__c = 'US: Kent';
            
            try
            {
            update caseObj;
            }
            catch(exception e)
            {
                system.debug(e);
            }
            
            test.startTest();
                String sch = '0 0 23 * * ?';
            	system.schedule('Test status Check', sch, new CTS_OM_CalculateOpenCaseAgeScheduler());
                Database.executeBatch(new CTS_OM_CalculateOpenCaseAge(null,true), 25);
            test.stopTest();
        }
    }
}