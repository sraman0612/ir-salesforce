public class tableColumns {
    @AuraEnabled
    public String label {get;set;}
    @AuraEnabled
    public String fieldName {get;set;}
    @AuraEnabled
    public String type {get;set;}
    @AuraEnabled
    public typeAttributes typeAtt {get;set;}

    public tableColumns(String label, String fieldName, String type, typeAttributes typeAtt){
        this.label = label;
        this.fieldName = fieldName;
        this.type = type;
        this.typeAtt = typeAtt;
    }
}