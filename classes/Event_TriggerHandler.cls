// 
// (c) 2015 Appirio, Inc.
//
// Handler class for event Trigger
//
// Apr 30, 2015     Barkha Jain        Original: T-382779
//
public with sharing class Event_TriggerHandler{
    
    public static void beforeInsert(List<Event> newList){    
        populateOpportunityStage(newList);
    }
    
    // Method to auto populate Opportunity Stage field on Events
    private static void populateOpportunityStage(List<Event> newEvents){
        list<Event> opportunityEvents = new list<Event>();
        set<Id> oppIdSet = new set<Id>();
        for(Event event : newEvents){
            if(event.WhoId != null && String.valueOf(event.WhoId).startsWith('00Q')){
                event.Opportunity_Stage__c = 'Target';
            }else if(event.WhatId != null && String.valueOf(event.WhatId).startsWith('006')){
                opportunityEvents.add(event);
                oppIdSet.add(event.WhatId);
            }
        }
        
        map<Id, Opportunity> oppMap = new map<Id, Opportunity>([SELECT Id, StageName FROM Opportunity
                                                                WHERE  Id IN :oppIdSet]);
                                                                
        for(Event event : opportunityEvents){
            event.Opportunity_Stage__c = oppMap.get(event.WhatId).StageName;
        }
    }
    public static void inactiveEvent(Set<ID> accIDs, List<Event> newEvents){
        Map<ID,Account> eventAccountMap = new Map<ID,Account>([SELECT Status__c,Id, recordtypeid FROM Account WHERE Id IN :accIDs]);
    Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
      for (Event e : newEvents) {
            if(e.WhatId != null){
                if(String.valueOf(e.WhatId).startswith('001') && eventAccountMap.get(e.WhatId).status__c == 'Inactive' && eventAccountMap.get(e.WhatId).recordtypeId == accRecordTypeId){
                   e.addError('This Account is Inactive'); 
                }
   
    }
    }
    }
    
}