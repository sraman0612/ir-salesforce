@isTest
public class CC_TrainingRequestCnt_Test {  

    @testSetup static void setupData() {
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        insert psettingList;
    }
     
    static testMethod void myTest(){
        Account a=TestUtilityClass.createAccount();
        insert a;
        System.assertNotEquals(null,a);
        Contact c=TestUtilityClass.createContact(a.id);
        insert c;
        System.assertNotEquals(null,c);
        
        string profileID = PavilionSettings__c.getAll().get('PavillionCommunityProfileId').Value__c;
        System.debug('the profile id is'+profileID);
        User u = TestUtilityClass.createUserBasedOnPavilionSettings(profileID, c.Id);
        insert u;
        System.assertEquals(c.Id,u.ContactId);
        System.runAs(u)
        {   
        List<SelectOption> options;
        test.startTest();   
        Date d = System.today();
        ApexPages.currentPage().getParameters().put('date',d.year() + '-' + d.month() + '-' + d.day());
        ApexPages.currentPage().getParameters().put('time','12:50');
        CC_PavilionTemplateController controller;
        ApexPages.currentPage().getParameters().put('contents','tavant');
        CC_TrainingReguestCnt tr1 =new CC_TrainingReguestCnt(controller);
        ApexPages.currentPage().getParameters().put('contents','club car links');
        CC_TrainingReguestCnt tr2 =new CC_TrainingReguestCnt(controller);
        ApexPages.currentPage().getParameters().put('contents','etq');
        CC_TrainingReguestCnt tr3 =new CC_TrainingReguestCnt(controller);
        ApexPages.currentPage().getParameters().put('contents','marketing');
        CC_TrainingReguestCnt tr4 =new CC_TrainingReguestCnt(controller);
        ApexPages.currentPage().getParameters().put('contents','customerview');
        CC_TrainingReguestCnt tr5 =new CC_TrainingReguestCnt(controller);
        ApexPages.currentPage().getParameters().put('contents','xxxyyyzzz');
        CC_TrainingReguestCnt tr =new CC_TrainingReguestCnt(controller);
        CC_TrainingRequest trainingReq;
        PageReference pref=Page.CC_PavillionCvTrainingReq;
        test.setCurrentPage(pref);
        System.debug(pref.getUrl());
        //tr.attendee=trainingReq;
        System.debug(ApexPages.currentPage().getUrl());
        tr.createTrainingRegistration();
        test.stopTest();
        }
    }
    
    
}