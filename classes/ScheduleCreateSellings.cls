public class ScheduleCreateSellings implements Schedulable{
        
    public static String scheduleJob(string CRON_EXP) {
        string jobId = System.schedule('Create_sellings', CRON_EXP, new ScheduleCreateSellings());
        return jobId;
    }
    
    public void execute(SchedulableContext sc){       
        MassCreateSellings t = new MassCreateSellings(); 
        Database.executeBatch(t);
    }
}