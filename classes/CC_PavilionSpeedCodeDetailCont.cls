global class CC_PavilionSpeedCodeDetailCont{

    public Boolean vcmFound {get;set;} // If any speed code history record found is VCM
  
    public class CC_SpeedCodeHistoryWrapper{

        public CC_SpeedCodeHistory__c speedCodeHistory {get;set;}
        public Boolean isVCM {get;set;} // VCM is identified when both codes B and C are empty

        public CC_SpeedCodeHistoryWrapper(CC_SpeedCodeHistory__c speedCodeHistory){

            this.speedCodeHistory = speedCodeHistory;
            this.isVCM = (speedCodeHistory.CC_Speed_Code_B__c == null && speedCodeHistory.CC_Speed_Code_C__c == null);
        }
    }
    
    public List<CC_SpeedCodeHistoryWrapper> speedCodeDetails {get;set;}
  
    public CC_PavilionSpeedCodeDetailCont(CC_PavilionTemplateController controller){

        vcmFound = false;
        speedCodeDetails = new CC_SpeedCodeHistoryWrapper[]{};

        for (CC_SpeedCodeHistory__c speedCodeHistory : 
                        [SELECT id, Name, CC_Controller_Serial_Numbr__c, CC_Speed_Code_A__c,CC_Speed_Code_B__c,CC_Speed_Code_C__c 
                         FROM CC_SpeedCodeHistory__c 
                         WHERE Id = :ApexPages.currentPage().getParameters().get('spcid')]){

            CC_SpeedCodeHistoryWrapper speedCodeDetail = new CC_SpeedCodeHistoryWrapper(speedCodeHistory);

            speedCodeDetails.add(speedCodeDetail);   
            
            if (speedCodeDetail.isVCM){
                vcmFound = true;
            }
        } 
    }
}