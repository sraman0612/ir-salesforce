/*------------------------------------------------------------
Author:       Randhir Kumar
Description:  Class used to schedule the batch "BatchResetDailysalesOnIwdNewBusOpp"
------------------------------------------------------------*/
global class ScheduleResetDailysalesOnIwdNewBusOpp implements Schedulable{

    public static String SCHEDULE_INTERVAL = '0 1 0 * * ?';  // Every Day, one minute past midnight
    public static String DEFAULT_JOB_NAME = 'Batch to reset daily sales on IWD Opportunity';

    global void execute(SchedulableContext sc) {
        // Instantiate the batch and execute it
        BatchResetDailysalesOnIwdNewBusOpp batch = new BatchResetDailysalesOnIwdNewBusOpp();
        Database.executebatch(batch);
    }

    global static String scheduleMe(String jobName) {
        // Create an instance of the Schedulable class
        ScheduleResetDailysalesOnIwdNewBusOpp scheduleInstance = new ScheduleResetDailysalesOnIwdNewBusOpp();
        // Schedule it for execution
        jobName = String.isNotBlank(jobName) ? jobName : DEFAULT_JOB_NAME;
        return System.schedule(jobName, SCHEDULE_INTERVAL, scheduleInstance);
    } 
}