// Test Class for AccountPlan_TriggerHandler class
//
// Created by Dhilip on 4th Dec 2018    
//
@isTest
private class AccountPlan_TriggerHandler_Test {
    //Commented as part of EMEIA Cleanup
    //Account Plan Record Types
    //public static final String RS_Account_Plan_Standard_RT_DEV_NAME = 'RHVAC_Account_Plan_Standard';
    public static final String CTS_Account_Plan_RT_DEV_NAME = 'CTS_Account_Plan';
            
    // Method to test account trigger
    static testMethod void testAccountPlanTrigger() {
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_EU' 
                                    and SobjectType='Account' limit 1].Id;
        //integer currentyear = System.Today().year();
        integer currentyear = 2021;
        
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
		acc.CTS_Global_Shipping_Address_1__c = '13, street2';
		acc.CTS_Global_Shipping_Address_2__c = '14';        
        acc.County__c = 'testCounty';
        acc.Division__c = 'San Francisco Customer Center';
        acc.recordtypeid=AirNAAcctRT_ID;
                
        insert acc;
        
        Product2 prod = new Product2();
        prod.Name = 'testProduct';
        prod.productcode='RTR2000';
        insert prod;
        
        District_Division__c disDiv = new District_Division__c();
        disDiv.Name = '1';
        disDiv.Division__c = 'San Francisco Customer Center';
        disDiv.District__c = 'Bay Area';
        insert disDiv;

        
        AccountPlan__c obj = IRSPXTestDataFactory.createAccountPlan(acc.id, CTS_Account_Plan_RT_DEV_NAME, string.valueOf(currentYear));  
        AccountPlan__c obj2 = IRSPXTestDataFactory.createAccountPlan(acc.id, CTS_Account_Plan_RT_DEV_NAME, string.valueOf(currentYear));
        obj.recalculateFormulas();
        obj2.recalculateFormulas();
        
        Test.startTest();
        insert obj;
        insert obj2;
        
        acc = [Select Id,AccountPlan_Count__c from Account where Id =: acc.id];
        obj = [Select Id,AccountName__c, AccountID__c,AccountName__r.Id,AccountName__r.Name from AccountPlan__c where Id =: obj.Id];
        obj2 = [Select Id,AccountName__c, AccountID__c,AccountName__r.Id,AccountName__r.Name from AccountPlan__c where Id =: obj2.Id];
        
        obj2.Name = 'test_AccountPlan__c_2';
        update obj2;
        
        System.debug('obj.AccountName__r.Id  == ' + obj.AccountName__r.Id + '\n');
        System.debug('obj.AccountName__r.Name  == ' + obj.AccountName__r.Name + '\n');
        System.debug('acc.id == ' + acc.Id + '\n');
        System.debug('acc details == ' + acc + '\n');
        System.debug('obj details == ' + obj + '\n');
        System.debug('acntplncnt == ' + acc.AccountPlan_Count__c + '\n');
        System.debug('obj AccountID__c == ' + obj.AccountID__c + '\n');
        System.debug('obj2 AccountID__c == ' + obj2.AccountID__c + '\n');
        
        obj2 = [Select Id, name, AccountName__c, AccountID__c,AccountName__r.Id,AccountName__r.Name from AccountPlan__c where Id =: obj2.Id];
        System.debug('obj2: ' + obj2);
        
        delete obj2;
        
        Test.stopTest();
        
        //System.assertEquals(obj.AccountName__r.Id,acc.Id);
        
       }
}