public class CTSUpdateAccountStreetAddress {
    
  public static void insertadd(List<Account> newlst){            
      
          for(Account acct : newlst)
          {                  
			      if (acct.CTS_Global_Shipping_Address_1__c == null){
                      acct.CTS_Global_Shipping_Address_1__c = '';
                  }
                  if (acct.CTS_Global_Shipping_Address_2__c == null){
                      acct.CTS_Global_Shipping_Address_2__c = '';
                  }
              acct.ShippingStreet = acct.CTS_Global_Shipping_Address_1__c + '\n' +
              acct.CTS_Global_Shipping_Address_2__c;
          }
   }
    
  public static void updateadd(List<Account> newlst1, Map<Id,Account> oldMap){            
      
          for(Account acct : newlst1)
          {                  
			  if (oldMap == null || (oldMap.get(acct.Id).CTS_Global_Shipping_Address_1__c != acct.CTS_Global_Shipping_Address_1__c)
                  || (oldMap.get(acct.Id).CTS_Global_Shipping_Address_2__c !=acct.CTS_Global_Shipping_Address_2__c))
              {
                  if (acct.CTS_Global_Shipping_Address_1__c == null){
                      acct.CTS_Global_Shipping_Address_1__c = '';
                  }
                  if (acct.CTS_Global_Shipping_Address_2__c == null){
                      acct.CTS_Global_Shipping_Address_2__c = '';
                  }
                  acct.ShippingStreet = acct.CTS_Global_Shipping_Address_1__c + '\n' +
                  acct.CTS_Global_Shipping_Address_2__c;
              }
          }
   }
}