@isTest 
public class UpdateDataCategoryOnKnowledge_DUTTest 
{
    static testMethod void test() 
    {
        
        user userForTesting = [SELECT Id FROM User where profile.name = 'CTS_TechDirect_Global_Administrator' and IsActive=True LIMIT 1];
        system.runAs(userForTesting)
        {
            Database.QueryLocator QL;
            Database.BatchableContext BC;
            List<Knowledge__kav> kList= new List<Knowledge__kav>();
            string RecordTypeId = [Select Id from RecordType where developername = 'CTS_TechDirect_Knowledge_Article'].Id;
            for(Integer i=0 ;i <100;i++)
            {
                Knowledge__kav kav = new Knowledge__kav();
                kav.Title ='Title'+i;
                kav.RecordTypeId = RecordTypeId;
                kav.CTS_TechDirect_Author_Reviewer__c = userForTesting.Id;
                kav.UrlName = 'test-batch-class-' + i;
                kav.language = 'nl_NL';            
                kList.add(kav);                
            }
            
            insert kList;
            
            List<Knowledge__kav> kListDC = new List<Knowledge__kav>();            
            for(Integer i=20 ;i <50;i++)
            {
                 kListDC.add(kList[i]);                 
            }            
            
            Knowledge__DataCategorySelection[] newRecords = new Knowledge__DataCategorySelection[] {};
            for (Knowledge__kav k :kListDC) 
            {
                if(null != k.id) 
                {
                    newRecords.add(new Knowledge__DataCategorySelection(ParentId = k.Id, DataCategoryGroupName = 'IR_Global_Category_Group', DataCategoryName = 'Cooling'));    
                }
            }
            insert newRecords; 
            
            Test.startTest();  
            
            UpdateDataCategoryOnKnowledge_Dutch obj = new UpdateDataCategoryOnKnowledge_Dutch();
            QL = obj.start(BC); 
            obj.execute(BC,kList);
            obj.execute(null);
            obj.finish(BC);            
            Test.stopTest();
        }
    }
}