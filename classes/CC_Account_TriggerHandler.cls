/* There are several methods in this handler that are not called from any trigger or class so they are commented out for now */
public class CC_Account_TriggerHandler {
	
  	public static boolean AfterUpdateAccountRecursion = true;
  	public static boolean BeforeUpdateAccountRecursion = true;

    private static Map<Id, String> accountRecordTypeMap = new Map<Id, String>();

    static{
        
        for (RecordType rt : [Select Id, DeveloperName From RecordType Where sObjectType = 'Account']){
            accountRecordTypeMap.put(rt.Id, rt.DeveloperName);
        }
    }
 
  	@TestVisible
  	private static AccountTeamMember createATM(Id accId,Id usrId) {
	    AccountTeamMember act = new AccountTeamMember();
	    act.AccountId              = accId;
	    act.UserId                 = usrId;
	    act.TeamMemberRole         = 'Application Engineer';  
	    act.AccountAccessLevel     = 'Edit';
	    act.OpportunityAccessLevel = 'Read';
	    act.CaseAccessLevel        = 'Read';
	    return act;
  	}    
  
  	public static void onBeforeUpdate(Account[] accounts, Map<Id, Account> oldMap){
  	
		CC_Account_Settings__c settings = CC_Account_Settings__c.getOrgDefaults();
		system.debug('CC_AccountTriggerHandler - account settings: ' + settings);
		
		if (settings != null && settings.Enable_Asset_Group_Rollup__c){
			rollupAssetGroups(accounts, oldMap);    		
		}   	
  	}  

    public static void rollupAssetGroups(Account[] accounts, Map<Id, Account> oldMap){
    	
    	system.debug('--rollupAssetGroups');
    	
    	Account[] accountsToProcess = new Account[]{};
    	
    	for (Account a : accounts){
    		
    		Account old = oldMap != null ? oldMap.get(a.Id) : null;
    		
            // Only process for club car accounts excluding channel partner
            String recordTypeName = a.RecordTypeId != null ? accountRecordTypeMap.get(a.RecordTypeId) : '';

            if (recordTypeName.contains('Club_Car') && recordTypeName != 'Club_Car_ChannelPartner'){

                // Use the roll-up summary field to identify a change in the asset groups that needs to trigger a recalc on the other asset group summary fields
                // Process accounts if not being called from a trigger
                if ((old != null && a.CC_Asset_Group_Rollup__c != old.CC_Asset_Group_Rollup__c) || Trigger.isExecuting == false){
                    accountsToProcess.add(a);
                }
            }
    	}
    	
    	system.debug('accountsToProcess: ' + accountsToProcess.size());
    	
    	if (accountsToProcess.size() > 0){
    		
    		// Get the counts of the different asset group types, cannot group on a formula field (Car_Type__c)
    		AggregateResult[] aggResults = [Select Account__c AccountId, Product__r.P2__c ProductType, SUM(Quantity__c) CarCount 
    										From CC_Asset_Group__c 
    										Where Account__c in :accountsToProcess and Fleet_Status__c != 'Retirement'
    										Group By Account__c, Product__r.P2__c
    										Order By Product__r.P2__c ASC];
    		
    		// Store the results for each account in a map
    		Map<Id, AggregateResult[]> accountAggResultMap = new Map<Id, AggregateResult[]>();
    		
    		for (AggregateResult aggResult : aggResults){
    			
    			Id accountId = (Id)aggResult.get('AccountId');
    			
    			if (accountAggResultMap.containsKey(accountId)){
    				accountAggResultMap.get(accountId).add(aggResult);
    			}
    			else{
    				accountAggResultMap.put(accountId, new AggregateResult[]{aggResult});
    			}    			    			
    		}		
    		
    		for (Account a : accountsToProcess){
    			
    			AggregateResult[] acctAggResults = accountAggResultMap.get(a.Id);
    			system.debug('aggResults for account (' + a.Name + '): ' + acctAggResults);
    			
    			Double gasCount = 0, electricCount = 0, otherCount = 0;
    			
    			if (acctAggResults != null && acctAggResults.size() > 0){
    				
    				for (AggregateResult aggResult : acctAggResults){
    			
	    				String prodType = aggResult.get('ProductType') != null ? (String)aggResult.get('ProductType') : '';  
	    				prodType = prodType.toUpperCase();  				
	    				system.debug('prodType: ' + prodType);    			
	    			
	    				Double carCount = aggResult.get('CarCount') != null ? (Double)aggResult.get('CarCount') : 0;
	    				system.debug('carCount: ' + carCount);
	    				
						if (prodType.contains('GAS')){
							gasCount += carCount;
						}
						else if (prodType.contains('ELECTRIC')){
							electricCount += carCount;
						}
						else{
							otherCount += carCount;
						}			
    				}
    			}
    						
    			a.CC_Number_of_Gas_Cars__c = gasCount;
    			a.CC_Number_of_Electric_Cars__c = electricCount;
    			a.CC_Number_Of_Cars__c = otherCount + gasCount + electricCount;
    			
    			system.debug('-asset rollup stats-');
    			system.debug('electric car count: ' + a.CC_Number_of_Electric_Cars__c);
    			system.debug('gas car count: ' + a.CC_Number_of_Gas_Cars__c);
    			system.debug('total car count: ' + a.CC_Number_Of_Cars__c);
    		}						    										
    	}
    }
   
  
  private static Boolean isClubCarIntegrationUser() {return UserInfo.getName() == 'Clubcar Integration';}
  private static Boolean isPartner() {return userinfo.getusertype() == 'PowerPartner';}  
  
  //public static void beforeInsertUpdate(Map<String, List<Account>> trgNewMap){
  public static void beforeInsertUpdate(List<Account> trgNew,Map<Id, String> acctRTMap){
    Account[] CC_AccLst = new Account[]{};
    for(Account a : trgNew){
      if((null != a.RecordTypeId && acctRTMap.get(a.RecordTypeId).contains('Club Car')) || (null==a.RecordTypeId && isClubCarIntegrationUser())){
        CC_AccLst.add(a);
      }
    }
    if(!CC_AccLst.isEmpty()){beforInsertUpdateActions(CC_AccLst);}
    //for(String recType : trgNewMap.keySet()){if(recType.contains('Club Car')){CC_AccLst.addAll(trgNewMap.get(recType));}}
  }

  public static void beforeUpdate(Map<String, List<Account>> trgNewMap, Map<Id,Account> trgOldMap){
    Account[] CC_AccLst = new Account[]{};
    Map<Id, Account> ccNewMap = new Map<Id, Account>();
    for(String recType : trgNewMap.keySet()){
      if(recType.contains('Club Car')){CC_AccLst.addAll(trgNewMap.get(recType));}
    }
    if(!CC_AccLst.isEmpty()){
      for(Account a : CC_AccLst){ccNewMap.put(a.id, a);}
      CC_FieldHistoryLogger logger = new CC_FieldHistoryLogger(SObjectType.Account.FieldSets.CC_AccountFieldHistory);
      logger.LogHistory(trgOldMap, ccNewMap);
      beforInsertUpdateActions(CC_AccLst);
    }
  }
  
  public static void afterInsert(Map<String, List<Account>> trgNewMap){clubcarATMscreation(trgNewMap);}
     
  public static void afterUpdate(Map<String, List<Account>> trgNewMap){
    Account[] CC_AccLst = new Account[]{};
    for(String recType : trgNewMap.keySet()){
      if(recType.contains('Club Car')){CC_AccLst.addAll(trgNewMap.get(recType));}
    }
    if(!CC_AccLst.isEmpty()){
      //afterUpdateActions(CC_AccLst);
    }
  }  
  /*
   * Description    : After update, if the owner changes, delete any ATM record for old owner and add ATM record for new owner.
   * createdDate    : 12/09/2017
   *
  */ 

  public static void afterUpdateActions(List<Account> accLst){
    Set<Id>AccountanduserIds      = new Set<Id>();
    List<AccountTeamMember> accountTeamMebersList = new List<AccountTeamMember>();
    List<AccountTeamMember> deleteATMList         = new List<AccountTeamMember>();
    system.debug('### deleting ATMs');
    for(account acc :accLst ){
      Account oldAcc = (Account)trigger.oldMap.get(acc.Id);
      system.debug('### old acc is ' + oldAcc);
      system.debug('### old acc owner is ' + oldAcc.OwnerId + ' new acc owner is ' + acc.OwnerId);
      if(acc.OwnerId != oldAcc.OwnerId){
        system.debug('### the owner is being changed so we add a new atm to the list');
        AccountTeamMember act = createATM(acc.Id,acc.ownerId);
        accountTeamMebersList.add(act);
        AccountanduserIds.add(oldAcc.OwnerId);
        AccountanduserIds.add(acc.id);
      }
    }
    
    Map<string,AccountTeamMember> keyVsatm = new Map<string,AccountTeamMember>();
    system.debug('### accoutn and user id set is ' + AccountanduserIds);
    if(!AccountanduserIds.isEmpty()){
      for(AccountTeamMember atm : [select id, userId,AccountId,user.name
                                     From AccountTeamMember
                                    where userId IN:AccountanduserIds AND AccountId IN:AccountanduserIds]){
                                      system.debug('### adding user id and account id to map for ' + atm.user.name);
        //keyVsatm.put(string.valueOf(atm.AccountId) + string.valueOf(atm.userId), atm);                              
        deleteATMList.add(atm);
      }
    }
    for(account acc :accLst ){
      if(!keyVsatm.isEmpty()  && keyVsatm.containsKey(string.valueOf(acc.id) + string.valueOf(acc.OwnerId))){
        system.debug('### adding to the delete list');
        deleteATMList.add(keyVsatm.get(string.valueOf(acc.id) + string.valueOf(acc.OwnerId)));
      }
    }
    if(!accountTeamMebersList.isEmpty()){
      try{insert accountTeamMebersList;}
      catch(Exception ex){system.debug('@@@@exception'+ ex);}
    }
    if(!deleteATMList.isEmpty()){delete accountTeamMebersList;}
  }
  /*
   * Description   : Account, Contact and Opportunity are integrated to Siebel. 
   *                 When one of these is deleted in Salesforce we need to send 
   *                 an outbound message to Siebel so they can delete on their side.
   * Created Date  : 14/06/2017
  */

  public static void afterDelete(Map<String, List<Account>> oldMap){
    List<account> clubcaraccounts       = new List<account>();
    list<OBM_Notifier__c> notifiersList = new list<OBM_Notifier__c>();
    for(String recType : oldMap.keySet()){
      if(recType.contains('Club Car') && !oldMap.get(recType).isEmpty() ){clubcaraccounts.addAll(oldMap.get(recType));}
    }
    Set<Id> masterIdSet = new set<Id>();
    for(Account accObj : clubcaraccounts){if(Account.MasterRecordId != null){masterIdSet.add(accObj.MasterRecordId);}}
    map<Id, String> masterExtIdMap = new map<Id, String>();
    if(!masterIdSet.isEmpty()){
      for(Account masterAccount : [SELECT Id, Siebel_ID__c,CC_Siebel_Id__c 
                                     FROM Account
                                    WHERE  Id IN :masterIdSet]){
        masterExtIdMap.put(masterAccount.Id, masterAccount.CC_Siebel_Id__c);
      }   
    }
    OBM_Notifier__c notifier;
    for(Account accObj : clubcaraccounts){
      notifier = new OBM_Notifier__c();       
      notifier = CC_Utility.createOBMNotifier(accObj, 'Account');
      if(accObj.MasterRecordId != null && masterExtIdMap.containsKey(accObj.MasterRecordId)){
        notifier.SFDC_Id__c = accObj.MasterRecordId;
        notifier.Master_External_Id__c = masterExtIdMap.get(accObj.MasterRecordId);
      }
      notifiersList.add(notifier);
    }    
    if(!notifiersList.isEmpty()){insert notifiersList;}     
  }
  
  public static void clubcarATMscreation(Map<String, List<Account>> trgNewMap){
    Account[] CC_AccLst              = new Account[]{};
    Boolean isClubCarIntegrationUser = isClubCarIntegrationUser();
    Boolean isPartner                = isPartner();
    Set<String> salesRepIdSet        = new Set<String>();
    Map<Id,CC_Sales_Rep__c> idVsSalesRep     = new Map<Id,CC_Sales_Rep__c>();
    Map<Id,CC_Sales_Rep__c> useridVsSalesRep = new Map<Id,CC_Sales_Rep__c>();
    List<AccountTeamMember> accountTeamMebersList = new List<AccountTeamMember>();
    String communityAcc;
    List<Partner> ptList = new List<Partner>();
    for(String recType : trgNewMap.keySet()){
      if(recType.contains('Club Car')){CC_AccLst.addAll(trgNewMap.get(recType));}
    }
    if(isPartner){communityAcc = [Select id,Contact.AccountId,Contact.Account.CC_Sales_Rep__c from User where Id = : userinfo.getuserid()].Contact.AccountId;}
    for(Account acc : CC_AccLst){
      //Create Account Team member for the owner of the account : 13/07/2017
      if(!isClubCarIntegrationUser){
        AccountTeamMember act = createATM(acc.Id,acc.ownerId);
        accountTeamMebersList.add(act);
      }    
      //Club Car Account is created by MAPICS -Club Car Integration user 
      if(isClubCarIntegrationUser && acc.CC_MAPICS_Sales_Rep__c != NULL){
        salesRepIdSet.add(acc.CC_MAPICS_Sales_Rep__c);
      } else if(isPartner){
        if(acc.OwnerId != userinfo.getuserid()){
          AccountTeamMember actpartner = createATM(acc.Id,userinfo.getuserid());
          accountTeamMebersList.add(actpartner);
        }
        // Create Partner Record 
        if(communityAcc != NULL){ptList.add(createPartner(acc.Id,communityAcc));}
      }
      if(!isPartner && null != PavilionSettings__c.getValues('ClubCarPartnerAccountID')){
        ptList.add(createPartner(acc.Id,PavilionSettings__c.getValues('ClubCarPartnerAccountID').Value__c));
      }
    }   
    //
    for(CC_Sales_Rep__c sr : [SELECT Id, CC_Sales_Rep__c 
                                FROM CC_Sales_Rep__c 
                               WHERE CC_Obsolete__c = false AND (Id IN : salesRepIdSet OR CC_Sales_Rep__c =:userinfo.getuserid())]){
      idVsSalesRep.put(sr.id, sr);
      useridVsSalesRep.put(sr.CC_Sales_Rep__c,sr);                      
    }
    for(Account acc : CC_AccLst){
      if(isClubCarIntegrationUser && acc.CC_MAPICS_Sales_Rep__c != NULL){
        if(idVsSalesRep.containsKey(acc.CC_MAPICS_Sales_Rep__c)){
          AccountTeamMember act = createATM(acc.Id,idVsSalesRep.get(acc.CC_MAPICS_Sales_Rep__c).CC_Sales_Rep__c);
          accountTeamMebersList.add(act);
        }
      } else if(!isPartner){
        if(useridVsSalesRep.containsKey(userinfo.getuserid())){
          AccountTeamMember act = createATM(acc.Id,useridVsSalesRep.get(userinfo.getuserid()).CC_Sales_Rep__c);
          accountTeamMebersList.add(act);
        }
      }
    }    
    if(!accountTeamMebersList.isEmpty()){insert accountTeamMebersList;}
    if(!ptList.isEmpty()){insert ptList;}
  } 
  
  @TestVisible
  private static Partner createPartner(Id fromAId, Id toAId){
    partner p  = new Partner (AccountFromId=fromAID,AccountToId=toAid);
    return p;
  }
    
  /*
   * Descriptions : This class covers following functionalities
   *                1.updating terms logic(before insert/update)
   *                2.update billing address
   *                3.update account type based on enterprise code
   *                4. update organization
   *                5. update salesrep details
   *                6.normalize the values in country and state fields
  */
  
  public static void beforInsertUpdateActions(List<Account> accLst){
    Boolean isClubCarIntegrationUser         = isClubCarIntegrationUser();
    Boolean isPartner                        = isPartner();
    Set<String> salesRepIdSet                = new Set<String>();
    Map<Id,CC_Sales_Rep__c> idVsSalesRep     = new Map<Id,CC_Sales_Rep__c>();
    Map<Id,CC_Sales_Rep__c> useridVsSalesRep = new Map<Id,CC_Sales_Rep__c>();
    Set<Id> accountIds                       = new Set<Id>();
    Map<Id,String> accIdVsPartnerCCOrg       = new Map<Id,String>();
    List<Partner> parnerList                 = new List<Partner>();
    Set<String> knownStateCodes              = new Set<String>();
    Set<String> knownCountries               = new Set<String>();
    Map<String,String> stateCodeMap          = new Map<String,String>();
    Map<String,String> countryCodeMap        = new Map<String,String>();
    Map<String,String> caseNormalizedCountryMap  = new Map<String,String>();
    //create maps for address transformation
    for (StatesCountries__c sc : StatesCountries__c.getall().values()){
      if (sc.Type__c=='S'){
        if (sc.Country__c=='USA'){
          knownStateCodes.add(sc.Code__c);
          stateCodeMap.put(sc.Name.toUpperCase(), sc.Code__c);
        }
      } else { //type is C
        knownCountries.add(sc.Name.toUpperCase());
        countryCodeMap.put(sc.Code__c, sc.Name);
        caseNormalizedCountryMap.put(sc.Name.toUpperCase(),sc.Name);
      }
    }  
    for(Account acc : accLst){
      if(acc.Type != 'Distributor' && acc.Type != 'Dealer' && acc.Type != 'EKA'){accountIds.add(acc.Id);}
      Account oldAcc = null;
      String oldEnterpriseCode = '';
      if(trigger.oldMap != null) { 
        oldAcc = (Account)trigger.oldMap.get(acc.Id);
        oldEnterpriseCode = oldAcc.CC_Enterprise_Code__c;
      }
      //normalize billing country, shipping country, us billing states adn us shipping states
      If((trigger.isInsert && acc.BillingCountry != NULL) ||(trigger.isUpdate && acc.BillingCountry != oldAcc.BillingCountry)){
        If(acc.BillingCountry.equalsIgnoreCase('United States')){acc.BillingCountry = 'USA';}
        else if(countryCodeMap.containsKey(acc.BillingCountry.toUpperCase())){acc.BillingCountry = countryCodeMap.get(acc.BillingCountry.toUpperCase());}
        else if(!knownCountries.contains(acc.BillingCountry.toUpperCase())){acc.addError('Please enter a valid country name');}
        else if(knownCountries.contains(acc.BillingCountry.toUpperCase())){acc.BillingCountry =caseNormalizedCountryMap.get(acc.BillingCountry.toUpperCase());}
      }
      If((trigger.isInsert && acc.ShippingCountry != NULL) ||(trigger.isUpdate && acc.ShippingCountry != oldAcc.ShippingCountry)){
        If(acc.ShippingCountry.equalsIgnoreCase('United States')){acc.ShippingCountry = 'USA';}
        else if(countryCodeMap.containsKey(acc.ShippingCountry.toUpperCase())){acc.ShippingCountry = countryCodeMap.get(acc.ShippingCountry.toUpperCase());}
        else if(!knownCountries.contains(acc.ShippingCountry.toUpperCase())){acc.addError('Please enter a valid country name');}
        else if(knownCountries.contains(acc.ShippingCountry.toUpperCase())){acc.ShippingCountry =caseNormalizedCountryMap.get(acc.ShippingCountry.toUpperCase());}
      }
      If((trigger.isInsert && acc.BillingState != NULL) || (trigger.isUpdate && acc.BillingState != oldAcc.BillingState) ){
        if(acc.BillingCountry == 'USA'){
          if(knownStateCodes.contains(acc.BillingState.toUpperCase())){acc.BillingState  = acc.BillingState.toUpperCase();}
          else if(stateCodeMap.containsKey(acc.BillingState.toUpperCase())){acc.BillingState   = stateCodeMap.get(acc.BillingState.toUpperCase());}
        }
      }
      If((trigger.isInsert && acc.ShippingState != NULL) || (trigger.isUpdate && acc.ShippingState != oldAcc.ShippingState) ){
        if(acc.ShippingCountry == 'USA'){
          if(knownStateCodes.contains(acc.ShippingState.toUpperCase())){acc.ShippingState  = acc.ShippingState.toUpperCase();}
          else if(stateCodeMap.containsKey(acc.ShippingState.toUpperCase())){acc.ShippingState   = stateCodeMap.get(acc.ShippingState.toUpperCase());}
        }
      }
      if(isClubCarIntegrationUser){
        //transform car terms and parts terms
        if ((trigger.isInsert || (trigger.isUpdate && oldAcc.CC_Car_Terms__c != acc.CC_Car_Terms__c)) && string.isNotBlank(acc.CC_Car_Terms__c)) {
          acc.CC_Car_Terms_Method__c = null != Club_Car_Payment_Terms__c.getValues(acc.CC_Car_Terms__c) ?  Club_Car_Payment_Terms__c.getValues(acc.CC_Car_Terms__c).CC_Car_Terms_Method__c : null;  
          acc.CC_Car_Terms__c = null != Club_Car_Payment_Terms__c.getValues(acc.CC_Car_Terms__c) ?  Club_Car_Payment_Terms__c.getValues(acc.CC_Car_Terms__c).Display_Value__c : null;
        }
        if ((trigger.isInsert || (trigger.isUpdate && oldAcc.CC_Part_Terms__c != acc.CC_Part_Terms__c)) && string.isNotBlank(acc.CC_Part_Terms__c)) {
          acc.CC_Part_Terms__c = null != Club_Car_Payment_Terms__c.getValues(acc.CC_Part_Terms__c) ?  Club_Car_Payment_Terms__c.getValues(acc.CC_Part_Terms__c).Display_Value__c : null;
        }
        //update Billing and Shipping  and record type and name for MAPICS Account Creates
        if(trigger.isInsert ){
          if( acc.CC_MAPICS_Sales_Rep__c != NULL){salesRepIdSet.add(acc.CC_MAPICS_Sales_Rep__c);}
          if(String.isNotBlank(acc.CC_MAPICS_Address1__c)){acc.BillingStreet = acc.CC_MAPICS_Address1__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Address2__c)){acc.BillingStreet += acc.CC_MAPICS_Address2__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Address3__c)){acc.BillingStreet += acc.CC_MAPICS_Address3__c;}
          if(String.isNotBlank(acc.CC_MAPICS_City__c)){acc.BillingCity = acc.CC_MAPICS_City__c;}
          if(String.isNotBlank(acc.CC_MAPICS_State__c)){acc.BillingState = acc.CC_MAPICS_State__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Zip__c)){acc.BillingPostalCode = acc.CC_MAPICS_Zip__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Country__c)){acc.BillingCountry = countryCodeMap.get(acc.CC_MAPICS_Country__c);}
          if(String.isNotBlank(acc.CC_MAPICS_Address1__c)){acc.ShippingStreet = acc.CC_MAPICS_Address1__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Address2__c)){acc.ShippingStreet += acc.CC_MAPICS_Address2__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Address3__c)){acc.ShippingStreet += acc.CC_MAPICS_Address3__c;}
          if(String.isNotBlank(acc.CC_MAPICS_City__c)){acc.ShippingCity = acc.CC_MAPICS_City__c;}
          if(String.isNotBlank(acc.CC_MAPICS_State__c)){acc.ShippingState = acc.CC_MAPICS_State__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Zip__c)){acc.ShippingPostalCode = acc.CC_MAPICS_Zip__c;}
          if(String.isNotBlank(acc.CC_MAPICS_Country__c)){acc.ShippingCountry = countryCodeMap.get(acc.CC_MAPICS_Country__c);}
          if(String.isNotBlank(acc.Alias_Name__c)){acc.Name = acc.Alias_Name__c;}
        }
      }
      //transform enterprise code
      if( (isClubCarIntegrationUser && Trigger.isInsert) || (Trigger.isUpdate && acc.CC_Enterprise_Code__c != oldEnterpriseCode) ) {
        if(CC_Enterprise_Code_to_Type_Mapping__c.getValues(acc.CC_Enterprise_Code__c) != null){
          acc.Type = CC_Enterprise_Code_to_Type_Mapping__c.getValues(acc.CC_Enterprise_Code__c).Type__c;
        } else {
          acc.Type = null;
        }
      }
    }
    //build map of partners
    parnerList = [SELECT AccountToId,AccountFromId,AccountFrom.Name,AccountTo.Name,AccountTo.CC_Organization__c
                    FROM Partner 
                   WHERE AccountFromId IN : accountIds AND AccountTo.CC_Organization__c != null];
    for(Partner ptnr : parnerList){
      system.debug('### account from is ' + ptnr.AccountFrom.Name);
      system.debug('### account to is ' + ptnr.AccountTo.Name);
      system.debug('### accoun to org is ' + ptnr.AccountTo.CC_Organization__c);
      String ccOrganization = '';
      if(accIdVsPartnerCCOrg.containsKey(ptnr.AccountFromId)){
        system.debug('### account is in map ');
        ccOrganization = accIdVsPartnerCCOrg.get(ptnr.AccountFromId);
        system.debug('### account from organization is ' + ccOrganization);
      }
      if(ccOrganization != '' && String.isNotBlank(ccOrganization) && !ccOrganization.contains(ptnr.AccountTo.CC_Organization__c)){
        ccOrganization += '; '+ptnr.AccountTo.CC_Organization__c;
        system.debug('### updated organization is ' + ccOrganization);
      } else {
        ccOrganization = ptnr.AccountTo.CC_Organization__c;
        system.debug('### new organization is ' + ccOrganization);
      }
      accIdVsPartnerCCOrg.put(ptnr.AccountFromId, ccOrganization);
    }
    //build map of sales reps
    If(trigger.isInsert){
      for(CC_Sales_Rep__c sr : [SELECT Id, CC_Sales_Rep__c 
                                  FROM CC_Sales_Rep__c 
                                 WHERE CC_Obsolete__c = false AND (Id IN : salesRepIdSet OR CC_Sales_Rep__c =:userinfo.getuserid())]){
        idVsSalesRep.put(sr.id, sr);
        useridVsSalesRep.put(sr.CC_Sales_Rep__c,sr);                      
      }
    }
    for(Account acc : accLst){
      //update organization
      if(!accIdVsPartnerCCOrg.isEmpty() && accIdVsPartnerCCOrg.containsKey(acc.Id)){acc.CC_Organization__c = accIdVsPartnerCCOrg.get(acc.Id);}
      If(trigger.isInsert){
        //set owner and sales rep when MAPICS creates the Account
        if(isClubCarIntegrationUser && acc.CC_MAPICS_Sales_Rep__c != NULL){
          if(idVsSalesRep.containsKey(acc.CC_MAPICS_Sales_Rep__c)){
            acc.CC_Sales_Rep__c = idVsSalesRep.get(acc.CC_MAPICS_Sales_Rep__c).id;
            acc.OwnerId         = idVsSalesRep.get(acc.CC_MAPICS_Sales_Rep__c).CC_Sales_Rep__c;
          }
        } else if(isPartner){  //do nothing, account rep set by a process builder/flow when partners create accounts
        } else {  //internal user creating account, set sales rep to internal user if user is not populating it
          if(useridVsSalesRep.containsKey(userinfo.getuserid()) && null == acc.CC_Sales_Rep__c){
            acc.CC_Sales_Rep__c = useridVsSalesRep.get(userinfo.getuserid()).id;
          }
        }
      }  
    }
  }
}