public without sharing class topicAssignmentAccessor {
    public static List<Id> getEntityIdsByTopic(List<String> topics){
        Id [] kavLst = new List<Id>();
        for (TopicAssignment ta : [SELECT EntityId FROM TopicAssignment WHERE Topic.Name IN :topics and EntityType = 'Knowledge__kav']) {kavLst.add(ta.EntityId);}
        return kavLst;
    }
}