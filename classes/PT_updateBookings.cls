global class PT_updateBookings implements Database.Batchable<sObject>, Database.Stateful, schedulable {
  global final string queryStr;
  
  public PT_updateBookings(string qry){
    queryStr = qry;
  }
  
  public Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(queryStr);
  }
  
  public void execute(Database.BatchableContext BC, List<PT_Booking__c> bookings){
    PT_Booking__c bookingToUpdate = bookings[0];
    Date startDate = bookingToUpdate.Date__c.toStartofMonth();
    Date endDate = bookingToUpdate.Date__c.addMonths(1).toStartofMonth().addDays(-1);
    Set <String> eligibleStages = new Set<String>{'2.Qualify','3.Discover','4.Proposal','5.Negotiate','6a.Closed Won'};
    Decimal totalBookings = 0.00;
    Boolean isPriorMonth = startDate <= system.Today() && startDate.Month()!=system.today().Month()?TRUE:FALSE;
    if(!isPriorMonth){    
      for(Opportunity o : [SELECT Amount, StageName
                             FROM Opportunity 
                            WHERE OwnerId =:bookingToUpdate.OwnerId and
                                  CloseDate >= :startDate and CloseDate <= :endDate and
                                  StageName IN :eligibleStages and
                                Amount != null]){
        totalBookings += o.Amount;          
      }
    }
    bookingToUpdate.Future_Bookings__c = totalBookings;
    update bookingToUpdate;
  }

  public void finish(Database.BatchableContext BC){}
  
  global void execute(SchedulableContext sc) {
    string qstring = 'SELECT  Date__c, Future_Bookings__c, OwnerId, winrate__c FROM PT_Booking__c';
    PT_updateBookings ptub = new PT_updateBookings(qstring);
    Database.executebatch(ptub,1);
   }  
}