@isTest
public without sharing class TaskTestMethods {

    static testMethod void testFaceToFaceFunctionality(){
    
    /* Create test data for Accounts */
		Profile p = [SELECT Id FROM Profile WHERE Name='PT System Administrator']; 
        system.debug('p ' + p);
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        system.runAs(u){
            
        
            List<Account> testAccountList = new List<Account>();
            
            Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                        where DeveloperName = 'CTS_EU' 
                                        and SobjectType='Account' limit 1].Id;
            
            /*Division__c divn = new Division__c();
            divn.Name = 'Test Division';
            divn.Division_Type__c = 'Customer Center';
            divn.EBS_System__c = 'Oracle 11i';     
            insert divn;*/
    
            for(Integer i = 0; i <= 3; i++){
                Account acc = new Account(
                Name = 'Test Account ' + i,
                BillingStreet = i + ' Main Street',
                BillingCity = 'This City',
                BillingState = 'OH',
                BillingPostalCode ='44114',
                BillingCountry = 'USA',
                County__c = 'Cuyahoga',
                ShippingStreet = i + ' Main Street',
                ShippingCity = 'This City',
                ShippingState = 'CA',
                ShippingPostalCode ='44114',
                RecordTypeId = AirNAAcctRT_ID,
                //Account_Division__c = divn.Id,
                ShippingCountry = 'USA',WasConverted__c = true );
                testAccountList.add(acc);
            }
    
            insert testAccountList;
    
            Map<string, Account> testAccountMap = new Map<string, Account>();
    
            for ( Account acc : testAccountList ){
                testAccountMap.put(acc.Name, acc);
            }
            
            /* Create test data for Tasks*/
    
            List<Task> testTaskList = new List<Task>();
    
            Task tsk11 = new Task(
            Subject = 'Test Task 1-1',
            CurrencyIsoCode = 'USD',
            ActivityDate = date.newInstance(2018,05,15),
            WhatId = testAccountMap.get('Test Account 1').id,
            Activity_Type__c = 'Phone Call',
            Description = 'These are some comments.',
            Priority = 'Normal',
            Status = 'Not Started',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk11.ownerId = UserInfo.getUserId();
            testTaskList.add(tsk11);
    
            Task tsk12 = new Task(
            Subject = 'Test Task 1-2',
            ActivityDate = date.newInstance(2018,05,25),
            Activity_Type__c = 'Phone Call',
            Status = 'Completed',
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 1').id,
            Description = 'These are some comments.',
            Priority = 'Normal',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk12.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk12);
    
            Task tsk13 = new Task(
            Subject = 'Test Task 1-3',
            ActivityDate = date.newInstance(2018,07,25),
            Activity_Type__c = 'Face To Face Call',
            Status = 'Not Started',
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 1').id,
            Description = 'These are some comments.',
            Priority = 'Normal' ,
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk13.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk13);
    
            Task tsk21 = new Task(
            Subject = 'Test Task 2-1',
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 2').id,
            ActivityDate = date.newInstance(2017,03,22),
            Activity_Type__c = 'Face To Face Call',
            Description = 'These are some comments.',
            Priority = 'Normal',
            Status = 'Completed',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk21.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk21);
    
            Task tsk22 = new Task(
            Subject = 'Test Task 2-2',
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 2').id,
            Description = 'These are some comments.',
            Priority = 'Normal',
            ActivityDate = date.newInstance(2017,05,18),
            Activity_Type__c = 'Face To Face Call',
            Status = 'Completed',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk22.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk22);
    
            Task tsk23 = new Task(
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 2').id,
            Description = 'These are some comments.',
            Priority = 'Normal',
            Subject = 'Test Task 2-3',
            ActivityDate = date.newInstance(2017,10,17),
            Activity_Type__c = 'Phone Call',
            Status = 'Completed',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk23.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk23);
    
            Task tsk31 = new Task(
            CurrencyIsoCode = 'USD',
            WhatId = testAccountMap.get('Test Account 3').id,
            Description = 'These are some comments.',
            Priority = 'Normal',
            Subject = 'Test Task 3-1',
            ActivityDate = date.newInstance(2019,06,15),
            Activity_Type__c = 'Face To Face Call',
            Status = 'Not Started',
            RecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId());
            tsk31.ownerId = UserInfo.getUserId();
    
            testTaskList.add(tsk31);
    
            insert testTaskList;
            
            Event evt = new Event(
                WhatId = testAccountMap.get('Test Account 3').id,
                Description = 'These are some comments.',
                //Status = 'Not Started',
                StartDateTime = System.today(),
                EndDateTime = System.today(),
                Activity_Type__c = 'Face To Face Call',
                Subject = 'Test Event 3-1',
                RecordtypeID = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Hibon_Event').getRecordTypeId()
            );
            insert evt;
            
            evt.whatId = testAccountMap.get('Test Account 2').id;
            update evt;
            
            Map<string, Account> testAccountResultMap = new Map<string, Account>();
    
            for ( Account acc : [SELECT Id, Name, Face_to_Face_Date__c FROM Account WHERE Id in :testAccountList ]){
                testAccountResultMap .put(acc.Name, acc);
            }
            
            /* Assert that the Face to Face Date calculation on each Account equals the expected value */
    
            system.assertEquals(testAccountResultMap.get('Test Account 1').Face_to_Face_Date__c, null);
            system.assertEquals(evt.StartDateTime.Date(),testAccountResultMap.get('Test Account 2').Face_to_Face_Date__c);
            system.assertEquals(testAccountResultMap.get('Test Account 3').Face_to_Face_Date__c, null);
            
            /* Modify some of the data */
            test.startTest();
    
            tsk31.Status = 'Completed';
            update tsk31;
    
            tsk23.Activity_Type__c = 'Face To Face Call';
            tsk23.ActivityDate = date.newInstance(2017,10,17);
            update tsk23;
    
            testAccountResultMap = new Map<string, Account>();
    
            for ( Account acc : [SELECT Id, Name, Face_to_Face_Date__c FROM Account WHERE Id in :testAccountList ]){
                testAccountResultMap .put(acc.Name, acc);
            }
            
            /* Re-assert values vis-a-vis expectations */
    
            system.assertEquals(testAccountResultMap.get('Test Account 1').Face_to_Face_Date__c, null);
            system.assertEquals(evt.StartDateTime.Date(),testAccountResultMap.get('Test Account 2').Face_to_Face_Date__c);
            system.assertEquals(testAccountResultMap.get('Test Account 3').Face_to_Face_Date__c, date.newInstance(2019,06,15));
            
            /* Modify the data again */
    
            tsk11.Activity_Type__c = 'Face To Face Call';
            tsk11.Status = 'Completed';
            update tsk11;
    
            tsk12.Activity_Type__c = 'Face To Face Call';
            update tsk12;
    
            tsk13.Status = 'Completed';
            update tsk13;
    
            testAccountResultMap = new Map<string, Account>();
    
            for ( Account acc : [SELECT Id, Name, Face_to_Face_Date__c FROM Account WHERE Id in :testAccountList ]){
                testAccountResultMap .put(acc.Name, acc);
            }
    
            /* Re-assert values vis-a-vis expectations */
    
            system.assertEquals(testAccountResultMap.get('Test Account 1').Face_to_Face_Date__c, date.newInstance(2018,07,25));
            
            /* Delete a Task */
    
            delete tsk13;
    
            testAccountResultMap = new Map<string, Account>();
    
            for ( Account acc : [SELECT Id, Name, Face_to_Face_Date__c FROM Account WHERE Id in :testAccountList ]){
                testAccountResultMap .put(acc.Name, acc);
            }
    
            /* Re-assert values vis-a-vis expectations */
    
            system.assertEquals(testAccountResultMap.get('Test Account 1').Face_to_Face_Date__c, date.newInstance(2018,05,25));
            test.stopTest();
        }
    }
}