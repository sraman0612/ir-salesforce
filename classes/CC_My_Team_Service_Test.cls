@isTest
public with sharing class CC_My_Team_Service_Test {
    
    @TestSetup
    private static void createData(){
        
        TestDataUtility dataUtility = new TestDataUtility();
        
        User financeUser = dataUtility.createIntegrationUser();
        financeUser.LastName = 'FinanceTestUser';
        insert financeUser;
        
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();
        
        insert psettingList;

        CC_Contract_Settings__c contractSettings = CC_Contract_Settings__c.getOrgDefaults();
        contractSettings.CC_Contract_Trigger_Handler_Enabled__c = true;
        upsert contractSettings;	

        Account a1 = dataUtility.createAccount('Club Car');
        a1.Name = 'Account1';
        
        Account a2 = a1.clone();
        a2.Name = 'Account2';
        
        insert new Account[]{a1, a2};
            
		// Add a finance rep to account 1 but not account 2
		insert TestDataUtility.createMyTeam(a1.Id, financeUser.Id, CC_My_Team_Service.financeRepRole);  
    }    	
    
    @isTest
    private static void testLookupUsersByAccountAndRole(){
 
        User financeUser = [Select Id From User Where LastName = 'FinanceTestUser'];
        Account a1 = [Select Id From Account Where Name = 'Account1'];
        Account a2 = [Select Id From Account Where Name = 'Account2'];
        
        system.assertEquals(1, [Select Count() From CC_My_Team__c Where Account__c = :a1.Id and My_Team_Role__c = :CC_My_Team_Service.financeRepRole]);
        system.assertEquals(0, [Select Count() From CC_My_Team__c Where Account__c = :a2.Id and My_Team_Role__c = :CC_My_Team_Service.financeRepRole]);
        
        // Call the method and validate the results
        Map<Id, Map<String, Id>> accountUserRoleMap = CC_My_Team_Service.lookupUsersByAccountAndRole(new Set<String>{CC_My_Team_Service.financeRepRole}, new Set<Id>{a1.Id, a2.Id});
        system.assert(accountUserRoleMap.containsKey(a1.Id));
        system.assert(accountUserRoleMap.containsKey(a2.Id));
        system.assertEquals(1, accountUserRoleMap.get(a1.Id).values().size());
        system.assertEquals(0, accountUserRoleMap.get(a2.Id).values().size());
        system.assertEquals(financeUser.Id, accountUserRoleMap.get(a1.Id).get(CC_My_Team_Service.financeRepRole));
    }
}