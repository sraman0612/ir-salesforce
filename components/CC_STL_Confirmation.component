<apex:component >

    <apex:attribute name="lease" type="CC_Short_Term_Lease__c" description=""/>
    <apex:attribute name="cars" type="CC_STL_Car_Info__c[]" description=""/>

    <apex:form >
        <div class="header">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">            
                <tr>
                    <td style="width:80%; background:padding:1px;font-size: 15px;">
                        <img src="{!URLFOR($Resource.ClubCar_Logo_STL)}"/>
                    </td>                    
                    <td style="background:padding:1px; font-size: 12px; align:right;">
                        Augusta Branch<br/>
                        4125 Washington Road<br/>
                        Evans, GA 30809 <br/>
                        Phone: 706-863-3000x7156 <br/>
                        Fax: 706-869-1666
                    </td>
                </tr>
            </table>                
        </div>

        <table cellspacing="0" cellpadding="10" border="0" width="100%">
                <tr>                    
                    <td style="width:80%; font-weight:normal; padding:1px;font-size: 12px;text-align:left;">{!lease.Master_Lease__r.Bill_To_Name__c}</td>
                    <td style="font-weight:normal; padding:1px;font-size: 12px; text-align:right;">
                        <apex:outputText value="{0,date,MM/dd/yyyy}">
                            <apex:param value="{!lease.CreatedDate}" />
                        </apex:outputText>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:normal; padding:1px;font-size: 12px;text-align:left;">ATTN:{!lease.Billing_Contact__r.Name}</td>
                    <td style="font-weight:bold; padding:1px;font-size: 15px;text-align:left;">CUSTOMER</td>
                </tr>
                <tr>
                    <td style="font-weight:normal; padding:1px;font-size: 12px;text-align:left;">
                        {!lease.Master_Lease__r.Bill_To_Address__c}<br/>
                        {!lease.Master_Lease__r.Bill_To_City__c},&nbsp;{!lease.Master_Lease__r.Bill_To_State__c}&nbsp;{!lease.Master_Lease__r.Bill_To_Zip__c}<br/>
                        {!lease.Master_Lease__r.Bill_To_Country__c}
                    </td>
                    <td style="font-weight:bold; padding:1px;font-size: 15px;text-align:left;">CONFIRMATION</td>
                </tr>
        </table>
        <table cellspacing="20" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="font-weight:normal; padding:1px;font-size: 12px;text-align:left;">
                    Dear {!lease.Billing_Contact__r.Name},<br/>This letter is to inform you that Club Car, LLC 
                    has received your lease request to rent the following vehicles for the specified dates below.                             
                </td>
            </tr>  
        </table>    
        <table cellspacing="10" cellpadding="0" border="0" width="100%">
            <tr>
                <td style=" background:padding:1px;font-size: 15px;"><b>Delivery Date: </b>
                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                    <apex:param value="{!lease.Beginning_Date__c}" />
                    </apex:outputText>
                </td>
                <td style="background:padding:1px;font-size: 15px;"></td>
                <td style="background:padding:1px;font-size: 15px;"><b>Pickup Date: </b>
                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                    <apex:param value="{!lease.Ending_Date__c}" />
                    </apex:outputText>
                </td>
            </tr>
        </table>    
        <table cellspacing="0" cellpadding="5" border="0" width="100%">
            <tr>
               <th style="width:5%; font-weight:bold;border-bottom:2px solid #000; background:padding:1px;font-size: 12px;text-align:center;"><b>Qty.</b></th>
               <th style="font-weight:bold; border-bottom:2px solid #000; padding:1px;font-size: 12px;text-align:left;"><b>Description</b></th>
               <th style="width:25%; font-weight:bold;border-bottom:2px solid #000; padding:1px;font-size: 12px;text-align:right;"><b>Price/Car/Month</b></th>
               <th style="width:25%; font-weight:bold;border-bottom:2px solid #000; padding:1px;font-size: 12px;text-align:right;"><b>Billing Amount</b></th>
            </tr>

            <apex:repeat value="{!cars}" var="car">
                <tr>
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px; font-size: 12px;text-align:center;">{!car.Quantity__c}</td>
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px; font-size: 12px;text-align:left;">{!car.Description__c}</td>
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px; font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!car.Per_Car_Cost__c}"/>
                        </apex:outputText>
                    </td>  
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px; font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                       		<apex:param value="{!car.Total_Cost__c}"/>
                        </apex:outputText>
                    </td>                                                        
                </tr>
            </apex:repeat>
            
                <tr>
                    <td style="border-bottom:1px solid #000; border-top:1px solid #000; "></td>
                    <td style="border-bottom:1px solid #000; border-top:1px solid #000; "></td>
                    <td style="font-weight:bold; border-bottom:1px solid #000; border-top:1px solid #000;  padding:0px; font-size: 12px;text-align:right;">Total Retail Charge:</td>
                    <td style="font-weight:normal; border-bottom:1px solid #000; border-top:1px solid #000;  padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Adj_Cost_Before_Transportation__c}"/>
                        </apex:outputText>
                    </td>                                              
                </tr>
                
                <tr>
                    <td style="border-bottom:1px solid #000;"></td>
                    <td style="border-bottom:1px solid #000;"></td>
                    <td style="font-weight:bold; border-bottom:1px solid #000; padding:0px;font-size: 12px;text-align:right;">Transportation Out:</td>
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Delivery_Quoted_Rate__c}"/>
                        </apex:outputText>
                    </td>                       
                </tr>
                
                <tr>
                    <td style="border-bottom:1px solid #000;"></td>
                    <td style="border-bottom:1px solid #000;"></td>
                    <td style="font-weight:bold; padding:0px; border-bottom:1px solid #000; font-size: 12px;text-align:right;">Transportation Back:</td>
                    <td style="font-weight:normal; padding:0px; border-bottom:1px solid #000; font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Pickup_Quoted_Rate__c}"/>
                        </apex:outputText>
                    </td>                       
                </tr>
                
                <apex:outputPanel layout="none" rendered="{!lease.One_time_Charge__c != null && lease.One_time_Charge__c <> 0}">
                
                <tr>
                    <td style="border-bottom:1px solid #000; "></td>
                    <td style="border-bottom:1px solid #000; "></td>
                    <td style="font-weight:bold; padding:0px; border-bottom:1px solid #000; font-size: 12px;text-align:right;">One-Time Charge:</td>
                    <td style="font-weight:normal; padding:0px; border-bottom:1px solid #000; font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.One_time_Charge__c}"/>
                        </apex:outputText>
                    </td>                       
                </tr>   
                
                </apex:outputPanel> 
                
                <apex:outputPanel layout="none" rendered="{!lease.Additional_Discount__c != null && lease.Additional_Discount__c <> 0}">
                
                <tr>
                    <td style=""></td>
                    <td style=""></td>
                    <td style="font-weight:bold; padding:0px;font-size: 12px;text-align:right;">Additional Discount:</td>
                    <td style="font-weight:normal; padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Additional_Discount__c}"/>
                        </apex:outputText>
                    </td>                       
                </tr>                   
                
                </apex:outputPanel>            
                
                <tr>
                    <td style="border-bottom:1px solid #000; border-top:2px solid #000; colspan=2"></td>
                    <td style="border-bottom:1px solid #000; border-top:2px solid #000;"></td> 
                    <td style="font-weight:bold; border-top:2px solid #000; border-bottom:1px solid #000;padding:0px;font-size: 12px;text-align:right;">Sub-Total:</td>
                    <td style="font-weight:normal; border-top:2px solid #000; border-bottom:1px solid #000; padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Confirmation_Subtotal__c}"/>
                        </apex:outputText>
                    </td>
                </tr>
                
                <tr>
                    <td style="border-bottom:1px solid #000;"></td>
                    <td style="border-bottom:1px solid #000;"></td> 
                    <td style="font-weight:bold; border-bottom:1px solid #000; padding:0px;font-size: 12px;text-align:right;">Sales Taxes:</td>
                    <td style="font-weight:normal; border-bottom:1px solid #000; padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Estimated_Tax__c}"/>
                        </apex:outputText></td>                        
                </tr>
                
                <tr>
                    <td style=""></td>
                    <td style=""></td> 
                    <td style="font-weight:bold; padding:0px;font-size: 12px;text-align:right;">Total Charges:</td>
                    <td style="font-weight:normal; padding:0px;font-size: 12px;text-align:right;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Total_Lease_Price__c}"/>
                        </apex:outputText></td>
                </tr>              
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="98%">
            <tr>
                <td colspan="8" style="font-weight:bold; text-align:left; font-size: 12px; color:red;">
                    Price/Payments do not include applicable property taxes. </td>
            </tr>
            <tr>
                <td style="width:10%; font-weight:bold; background:padding:1px;font-size: 12px;text-align:right;"><b>Terms:</b></td>
                <td style="width:10%; font-weight:normal; background:padding:1px;font-size: 12px;text-align:center;">{!lease.Invoice_Terms__c}</td>
                <td style="width:10%; font-weight:bold; background:padding:1px;font-size: 12px;text-align:right;"><b># Pmts.:</b></td>
                <td style="width:10%; font-weight:normal; background:padding:1px;font-size: 12px;text-align:center;">{!lease.Num_of_Payments__c}</td>
                <td style="width:20%; font-weight:bold; background:padding:1px;font-size: 12px;text-align:right;"><b>Std. Monthly Bill:</b></td>
                <td style="width:10%; font-weight:normal; background:padding:1px;font-size: 12px;text-align:center;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.Standard_Monthly_Billing__c}"/>
                        </apex:outputText></td> 
                <td style="width:15%; font-weight:bold; background:padding:1px;font-size: 10px;text-align:right;"><b>1st Bill:</b></td>
                <td style="width:15%; font-weight:normal; background:padding:1px;font-size: 10px;text-align:left;">
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.First_Bill__c}"/>
                        </apex:outputText></td>                     
            </tr>
            <tr>
                <td colspan="2" style="font-weight:normal; text-align:center; font-size: 10px;">(M = Monthly T = Term)</td>
                <td colspan="6" style="font-weight:normal; text-align:right; font-size: 10px;">(Includes Transportation)</td>
            </tr> 
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="width:40%; font-weight:normal; background:padding:0px;font-size: 10px;text-align:center;"></td>
                <td style="width:12%; font-weight:bold; background:padding:0px;font-size: 10px;text-align:right;"></td>
                <td style="width:12%; font-weight:normal; background:padding:0px;font-size: 10px;text-align:center;"></td>
                <td style="width:12%; font-weight:bold; background:padding:0px;font-size: 10px;text-align:right;"><b>1st Bill Total:</b></td>
                <td style="width:12%; font-weight:normal; background:padding:0px;font-size: 10px;text-align:left;">             
                        <apex:outputText value="{0, Number, Currency}">
                            <apex:param value="{!lease.First_Bill__c}"/>
                        </apex:outputText></td> 
            </tr>
        </table>    
        <table>
            <tr>
                <td style="width:12%; font-weight:normal; background:padding:0px;font-size: 10px;text-align:right;"><b>Options:</b></td>
                <td style="font-weight:bold; background:padding:0px;font-size: 10px;text-align:left;">{!lease.Options_on_Cars__c}</td>
            </tr>      
        </table>

        <table cellspacing="0" cellpadding="5" border="0" width="100%">
            <tr>
                <td cellpadding="0" colspan="8" style="font-weight:normal; text-align:left; font-size: 12px;">
                    {!lease.Master_Lease__r.Bill_To_Name__c}'s 
                    current Certificate of Insurance for Liability expires on &nbsp;
                    <apex:outputText value="{0,date,MM/dd/yyyy}">
                        <apex:param value="{!lease.Liability_End_Date__c}" />
                    </apex:outputText>.<br/>
                    Please note, in order to ship cars, we need a current insurance certificate on file.  
                    If necessary, fax us an updated copy to the fax number above.<br/><br/>                    
                    Club Car, LLC, has the following information about {!lease.Master_Lease__r.Bill_To_Name__c}
                </td>
            </tr>            
        </table>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="font-weight:bold; background:padding:0px; font-size: 12px;text-align:left; border-bottom:2px solid #000;">Shipping Address:</td>
                <td style="font-weight:bold; background:padding:0px; font-size: 12px;text-align:left; border-bottom:2px solid #000;">Billing Address:</td>
            </tr>
            <tr>
                <td style="width:50%; font-weight:normal; background:padding:0px; font-size: 12px;text-align:left; ">
                    {!lease.Shipping_To_Name__c}<br/>
                    {!lease.Shipping_Address__c}<br/>
                    Contact Name: {!lease.Shipping_Contact__r.Name}<br/>
                    Contact Phone: {!lease.Shipping_Contact__r.Phone}<br/>
                    Contact Email: {!lease.Shipping_Contact__r.Email}<br/>
                </td>       
                <td style="font-weight:normal; background:padding:0px; font-size: 12px;text-align:left; ">
                    {!lease.Master_Lease__r.Bill_To_Name__c}<br/>
                    {!lease.Master_Lease__r.Bill_To_Address__c}<br/>
                    {!lease.Master_Lease__r.Bill_To_City__c},&nbsp;{!lease.Master_Lease__r.Bill_To_State__c}&nbsp;{!lease.Master_Lease__r.Bill_To_Zip__c}<br/>
                    {!lease.Master_Lease__r.Bill_To_Country__c}<br/>
                    Contact Name: {!lease.Billing_Contact__r.Name}<br/>
                    Contact Phone: {!lease.Billing_Contact__r.Phone}<br/>
                    Contact Email: {!lease.Billing_Contact__r.Email}<br/>
                </td>
            </tr>
            <tr>    
                <td style="padding:5px;font-size: 12px;">
                    Please confirm this lease with your electronic signature.<br/>
                    Thank you!  <br/><br/>
                    For further assistance please call {!$User.Phone} or email {!$User.Email} and reference Lease # 
                    {!lease.Name}.
                </td>                               
            </tr>
            <tr>    
                <td style="padding:12px;font-size: 12px;">Customer Confirmation</td>                               
            </tr>
            <tr>
                <td style="width:45% font-weight:normal; padding:5px;font-size: 12px;text-align:left;">{{Sig_es_:signer1:signature}}</td>
                <td style="width:25% font-weight:normal; padding:5px;font-size: 12px;text-align:center;"></td>  
                <td style="font-weight:normal; padding:5px;font-size: 12px;text-align:center;">
                    <apex:outputText value="{0,date,MM/dd/yyyy}" >  
                    <apex:param value="{!TODAY()}"/>  
                    </apex:outputText> </td>             
            </tr> 
            <tr>
                <td style="font-weight:normal; border-top:2px solid #000; padding:5px;font-size: 12px;text-align:left;">Signature</td>
                <td style="font-weight:normal; border-top:2px solid #000; padding:5px;font-size: 12px;text-align:center;"></td>  
                <td style="font-weight:normal; border-top:2px solid #000; padding:5px;font-size: 12px;text-align:center;">Date</td>                       
            </tr> 
        </table>            
    </apex:form>

</apex:component>