<!--
	@author Ben Lorenz
	@date 9AUG2019
	@description A generic VF component that can be used on any VF page to show a lookup as autocomplete input
-->

<apex:component controller="AC_JSONCtrl" id="autoc-comp">
 <apex:attribute name="componentLabel" description="Label of Component" type="String" required="true"/>
 <apex:attribute name="for" description="Id of input field" type="String" required="true"/>
 <apex:attribute name="sObject" description="SObject to query" type="String" required="true"/>
 <apex:attribute name="label" description="Label for autocomplete" type="String" required="true"/>
 <apex:attribute name="value" description="Value for autocomplete" type="String" required="true"/>
 <apex:attribute name="returnValue"  description="Return value for autocomplete" type="Object" required="true"/>

 <apex:attribute name="labelStyleClass" description="Label CSS class" type="String" required="false"/>
 <apex:attribute name="details" description="Details for autocomplete" type="String" required="false"/>
 <apex:attribute name="whereClause" description="Additional where clause for query" type="String" required="false"/>
 <!-- limit defaults to 10 -->
 <apex:attribute name="limitClause" description="Limits the return number of records" type="String" required="false"/>

<!-- CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
<apex:stylesheet value="{!URLFOR($Resource.AutoComplete, '/css/jquery-ui-1.8.17.custom.css')}"/>
<apex:stylesheet value="{!URLFOR($Resource.AutoComplete, '/css/basic.css')}"/>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.10.2.js"/>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"/>



<style type="text/css">
    .ui-autocomplete-loading { background: white url('{!URLFOR($Resource.AutoComplete, '/img/AjaxLoad.gif')}') right center no-repeat; }
</style>

<!-- START COMPONENT BODY -->
<div id="{!for}_hiddenDiv">
    <label class="{!LabelStyleClass}">{!ComponentLabel}</label>
    <apex:form id="autoc-form"> <apex:inputHidden value="{!ReturnValue}" id="returnValue"/></apex:form>
    </div>
<apex:componentBody />

<!-- END COMPONENT BODY -->



<script type="text/javascript">
$ac = jQuery.noConflict();

function getLoadingImage()
{
    var loadImagURL = "{!URLFOR($Resource.AutoComplete, 'BigLoad.gif')}";
    var retStr = ['<img src="', loadImagURL ,'" title="loading..." alt="loading..." class="middleAlign" />'];
    return retStr.join("");
}

var sourcePage = '/apex/AC_JSON?core.apexpages.devmode.url=0';

 $ac(function() {
        var txtVal =  $ac('[id$="{!for}"]');
        //This method returns the last character of String
        function extractLast(term) {
            return term.substr(term.length - 1);
        }

        $ac('[id$="{!for}"]').autocomplete({
            source: function( request, response ) {

                //Abort Ajax
                var $this = $ac(this);
                var $element = $ac(this.element);
                var jqXHR = $element.data('jqXHR');
                if(jqXHR)
                    jqXHR.abort();

                $ac('[id$="{!for}"]').addClass('ui-autocomplete-loading');
                $element.data('jqXHR',$ac.ajax({
                    url: sourcePage+'&q='+txtVal.val()+'&obj={!SObject}&label={!Label}&value={!Value}&detail={!Details}&wc={!URLENCODE(whereClause)}&lm={!limitClause}',
                    dataType: "json",
                    data: {
                    },
                    success: function( data ) {
                        response( $ac.map( data , function( item ) {
                            return {
                                label: '<a>'+
                                item.label+"<br />"+
                                '<span style="font-size:0.8em;font-style:italic">'
                                +item.detail+
                                "</span></a>",
                                value: item.label,
                                id: item.value
                            }
                        }));
                    },
                    complete: function() {

                        //This method is called either request completed or not
                        $this.removeData('jqXHR');

                        //remove the class responsible for loading image
                        $ac('[id$="{!for}"]').removeClass('ui-autocomplete-loading');
                    }
                })
                );
            },

            search: function() {
                //If String contains at least 2 characters
                if (this.value.length >= 2)
                {
                    $ac('[id$="{!for}"]').autocomplete('option', 'delay', 100);
                    return true;
                }
                return false;
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function(event, ui) {
                console.log('select');
                var selectedObj = ui.item.label;
                $ac('[id$="{!for}_hiddenDiv"] input[type=hidden]').val(ui.item.id);
                return true;
            }
        }).data("ui-autocomplete")._renderItem = autoCompleteRender;

    });

function autoCompleteRender(ul, item) {
    return $ac("<li></li>").data("item.autocomplete", item).append(item.label).appendTo(ul);
}
</script>

</apex:component>