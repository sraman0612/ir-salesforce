<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact_IR_Comp_EU_Sales_Delegated_Admin</label>
    <protected>false</protected>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>Permission_Set_Name__c</field>
        <value xsi:type="xsd:string">IR_Comp_EU_Sales_Delegated_Admin</value>
    </values>
    <values>
        <field>RecordType_Developer_Name__c</field>
        <value xsi:type="xsd:string">CTS_EU_Contact</value>
    </values>
</CustomMetadata>
