<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Upper Midwest Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Calgary Customer Center,Denver Customer Center,Des Moines Customer Center,Kansas City Customer Center,Midwest District,Minneapolis Customer Center,Northwest District,Omaha Customer Center,Rocky Mountain District,Seattle Customer Center,Upper Midwest Area</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">shauna.carlson@irco.com,Chad.Steiner@irco.com,frank_betz@irco.com,mitch_bugge@irco.com,keuni.martin@irco.com,angie_ohanesian@irco.com,jeremy_alford@irco.com,phillip.henderson@irco.com,jessie.finney@irco.com,matthew.wedan@irco.com,leah.kain@irco.com,chris.waldron@irco.com,andrea.gierke@irco.com,brandy.meyer@irco.com,doug_abels@irco.com,jackie.keller@irco.com,Jeannie.Berry@irco.com,esmith2@irco.com,jason.oliver@irco.com,harlan.smith@irco.com,jj.faubel@irco.com,lauren.olrunfullerton@irco.com,sarah.hodgins@irco.com,mike.weisz@irco.com,jon_sprunk@irco.com,john.bernsten@irco.com</value>
    </values>
</CustomMetadata>
