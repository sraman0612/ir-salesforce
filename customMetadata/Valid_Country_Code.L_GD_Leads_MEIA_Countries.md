<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>L-GD Leads MEIA Countries</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AE,AF,BH,EG,IN,IQ,JO,KW,LB,LY,OM,QA,SA,YE,AO,BW,CD,ER,ET,GH,GN,KE,KM,LS,MG,MU,MW,MZ,NA,NG,SC,SD,SO,SS,SZ,TZ,UG,ZA,ZM,ZW,DJ</value>
    </values>
</CustomMetadata>
