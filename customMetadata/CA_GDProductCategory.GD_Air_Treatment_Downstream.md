<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GD_Air_Treatment_Downstream</label>
    <protected>false</protected>
    <values>
        <field>FilterCategory__c</field>
        <value xsi:type="xsd:string">GD_Air_Treatment_Downstream</value>
    </values>
</CustomMetadata>
