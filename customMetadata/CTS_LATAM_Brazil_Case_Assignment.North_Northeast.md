<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>North / Northeast</label>
    <protected>false</protected>
    <values>
        <field>CTS_Region__c</field>
        <value xsi:type="xsd:string">North / Northeast</value>
    </values>
    <values>
        <field>Case_Owner_Alias__c</field>
        <value xsi:type="xsd:string">rcarv</value>
    </values>
    <values>
        <field>Case_Owner_Id__c</field>
        <value xsi:type="xsd:string">0054Q00000G9GKiQAN</value>
    </values>
</CustomMetadata>
