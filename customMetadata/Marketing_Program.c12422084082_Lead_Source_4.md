<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>c12422084082 Lead Source 4</label>
    <protected>false</protected>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">Lead Source 4</value>
    </values>
    <values>
        <field>Marketing_Program__c</field>
        <value xsi:type="xsd:string">LI</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">12422084082</value>
    </values>
</CustomMetadata>
