<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Southwest Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Los Angeles Customer Center,San Diego Customer Center,Santa Fe Springs Customer Center,Southwest Area</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">caitlin.storey@irco.com,terry.scrymgeour@irco.com,vince.salinas@irco.com,IRGYDQ@irco.onmicrosoft.com,rick.butsch@irco.com,flor.gomezgallardo@irco.com,mayra.pimentel@irco.com,benoit.kieny@irco.com,amy.tarango@irco.com,d.gallegos@irco.com,ronnie.sanchez@irco.com,b.yepez@irco.com,jmackaig@irco.com</value>
    </values>
</CustomMetadata>
