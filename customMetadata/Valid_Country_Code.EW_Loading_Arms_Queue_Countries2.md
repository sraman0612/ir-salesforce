<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EW Loading Arms Queue Countries2</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">ID,IT,JP,LA,LR,LS,LY,MO,MG,MW,MY,MV,ML,MT,MH,MR,MU,YT,MA,MZ,MM,NA,NZ,NE,NG,NO,OM,PW,PG,PH,PT,RW,AF,BH,BE,IO,DK,EG,ER,ET,GG,IS,IQ,IE,AS,AQ,BT,CX,CC,CK,FJ,PF,TF,GU,HM,KI,FM,NR,NC,NU,NF,MP,PN,WS,SG,SB,SS,TW,TH,TL,TK,TO,TV,UM,VU,VN,WF,ES,SM,SN,SL,SO,ZA,SZ,TZ</value>
    </values>
</CustomMetadata>
