<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TechDirect Ask a Question</label>
    <protected>false</protected>
    <values>
        <field>Criteria__c</field>
        <value xsi:type="xsd:string">Creation</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:type="xsd:string">CTS_TechDirect_Ask_a_Question</value>
    </values>
    <values>
        <field>Template_Id__c</field>
        <value xsi:type="xsd:string">00X0a000001F8ZzEAK</value>
    </values>
</CustomMetadata>
