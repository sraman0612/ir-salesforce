<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact_Hibon_Standard_User</label>
    <protected>false</protected>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>Permission_Set_Name__c</field>
        <value xsi:type="xsd:string">Hibon_Standard_User</value>
    </values>
    <values>
        <field>RecordType_Developer_Name__c</field>
        <value xsi:type="xsd:string">Hibon_Contact</value>
    </values>
</CustomMetadata>
