<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>South Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Atlanta Customer Center,Birmingham Customer Center,Chattanooga Customer Center,Nashville Customer Center,South Area</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">jcissa@irco.com,justin_davis@irco.com,kimberley.copeland@irco.com,Stephanie.Smith@irco.com,carlton_ayers@irco.com,david.everett@irco.com,David_Everett@irco.com,matt.wilemon@irco.com,tom_tomisek@irco.com,sherryrussell@irco.com,jeff.payton@irco.com,brian.fondren@irco.com,leisa_raymond@irco.com,crystal.mcbride@irco.com</value>
    </values>
</CustomMetadata>
