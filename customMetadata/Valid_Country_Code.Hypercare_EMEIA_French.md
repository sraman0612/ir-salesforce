<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Hypercare_EMEIA_French</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">SO;SS;BF;BI;BJ;CF;CG;CI;CM;DJ;DZ;FR;GA;GM;KM;LR;MA;MG;ML;MR;MU;NE;RW;SC;SL;SN;TD;TG;TN;AO;BW;CD;CV;ER;ET;GH;GN;GQ;GW;KE;LS;MW;MZ;NA;NG;SD;ST;SZ;TZ;UG;ZA;ZM;ZW</value>
    </values>
</CustomMetadata>
