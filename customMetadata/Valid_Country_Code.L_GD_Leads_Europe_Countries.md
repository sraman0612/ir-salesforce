<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>L-GD Leads Europe Countries</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AL,AM,AZ,BA,BY,CY,CZ,EE,GE,GR,HR,HU,LT,LV,MD,ME,MK,MT,PL,RE,RO,RS,RU,SI,SK,TR,UA,UZ,XK,BG,IL,AT,BE,CH,DE,LI,LU,NL,DK,FI,GB,IE,IS,NO,SE,ES,FR,GA,GM,GQ,GW,MA,IT,PT,SM,BF,BI,BJ,CF,CG,CI,CM,CV,DZ,LR,ML,MR,NE,RW,SL,SN,ST,TD,TG,TN,KZ,TJ,MC</value>
    </values>
</CustomMetadata>
