<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MR ME Claims</label>
    <protected>false</protected>
    <values>
        <field>Business_Hours__c</field>
        <value xsi:type="xsd:string">MR Customer Service</value>
    </values>
    <values>
        <field>Form_Name__c</field>
        <value xsi:type="xsd:string">Customer Service Form</value>
    </values>
    <values>
        <field>Org_Wide_Email_Address__c</field>
        <value xsi:type="xsd:string">lavesh.galipelli@cognizant.com</value>
    </values>
    <values>
        <field>Queue_Name__c</field>
        <value xsi:type="xsd:string">MR_ME_Claims</value>
    </values>
</CustomMetadata>
