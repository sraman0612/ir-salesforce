<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AP RHVAC ST 26</label>
    <protected>false</protected>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Manager_Feedback_Count__c</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">AccountPlan__c</value>
    </values>
    <values>
        <field>Page_Layout_Name__c</field>
        <value xsi:type="xsd:string">RHVAC Account Plan - Standard</value>
    </values>
    <values>
        <field>Record_Type_Name__c</field>
        <value xsi:type="xsd:string">RHVAC_Account_Plan_Standard</value>
    </values>
    <values>
        <field>Rollup_Summary_Field__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
