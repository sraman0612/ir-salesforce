<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Leads AP Countries</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">CN,AU,BN,HK,ID,JP,KI,KR,MN,MY,NZ,PH,SG,TH,TW,VN,BT,CC,CK,CX,FJ,HM,KH,KP,LA,MH,MM,MO,NF,NP,NR,NU,PG,PN,SB,TK,TL,TO,TV,VU,WF,WS,PW,FM,GU</value>
    </values>
</CustomMetadata>
