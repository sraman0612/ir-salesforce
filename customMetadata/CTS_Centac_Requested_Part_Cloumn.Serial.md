<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Serial</label>
    <protected>false</protected>
    <values>
        <field>API_Name__c</field>
        <value xsi:type="xsd:string">Serial__c</value>
    </values>
    <values>
        <field>CTS_Sequence_Number__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">If it is a Motor or Starter provide its Serial number</value>
    </values>
    <values>
        <field>Tab_Option__c</field>
        <value xsi:type="xsd:string">Add Parts Tab</value>
    </values>
    <values>
        <field>fieldType__c</field>
        <value xsi:type="xsd:string">STRING</value>
    </values>
    <values>
        <field>isMandatory__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
