<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RecordType</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">String</value>
    </values>
    <values>
        <field>Filter_Customer_Profile_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Filter_Type__c</field>
        <value xsi:type="xsd:string">Auto</value>
    </values>
    <values>
        <field>Filter_Values__c</field>
        <value xsi:type="xsd:string">IR Comp TechDirect Knowledge Article</value>
    </values>
    <values>
        <field>Matching_Field__c</field>
        <value xsi:type="xsd:string">RecordType.Name</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">Knowledge__kav</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">Equals</value>
    </values>
</CustomMetadata>
