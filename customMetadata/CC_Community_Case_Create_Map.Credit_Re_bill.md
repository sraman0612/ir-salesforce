<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Credit/Re-bill</label>
    <protected>false</protected>
    <values>
        <field>OwnerId__c</field>
        <value xsi:type="xsd:string">00G0a000000cMZK</value>
    </values>
    <values>
        <field>Reason__c</field>
        <value xsi:type="xsd:string">Credit/Re-bill</value>
    </values>
    <values>
        <field>Root_Cause__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sub_Reason__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
