<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Helix Alarm Trip Minor</label>
    <protected>false</protected>
    <values>
        <field>Default_Color__c</field>
        <value xsi:type="xsd:string">#FFE428</value>
    </values>
    <values>
        <field>Hover_Color__c</field>
        <value xsi:type="xsd:string">#FFE428</value>
    </values>
    <values>
        <field>Icon_Class__c</field>
        <value xsi:type="xsd:string">fas fa-exclamation-triangle</value>
    </values>
</CustomMetadata>
