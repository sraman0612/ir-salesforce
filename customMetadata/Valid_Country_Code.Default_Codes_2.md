<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Codes 2</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">TM;TO;TT;TV;TW;US;UY;VC;VE;VG;VI;VN;VU;WS;AI;CW;BL;BM;BV;CK;CU;GL;IO;MF;NR;PM;WF;MM;AQ;BQ;CX;CC;TF;GG;HM;MS;NU;NF;MP;PN;GS;TK;UM;EH;IR;BIH;CS;KO;XZ</value>
    </values>
</CustomMetadata>
