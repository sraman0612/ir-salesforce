<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EW Loading Arms Queue Countries1</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">US,CA,BO,BR,CL,CO,CR,DO,EC,GT,HN,NI,PA,PR,PY,SV,TT,VE,AR,MX,PE,UY,CH,EE,LV,LT,DE,IL,AZ,BY,MN,RU,TJ,GE,KZ,KG,TM,UG,UA,UZ,AE,BD,FI,IN,JO,KW,LB,NP,PK,PS,QA,SA,LK,SD,SY,YE,KR,DZ,AD,AO,AU,BJ,BW,BN,BF,BI,KH,CM,CV,CF,TD,CN,CG,CD,CI,DJ,GQ,GF,GA,GM,GH,GI,GN,GW,HK</value>
    </values>
</CustomMetadata>
