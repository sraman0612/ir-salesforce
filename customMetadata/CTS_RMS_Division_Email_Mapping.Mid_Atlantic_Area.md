<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mid Atlantic Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Federal City District,Greensboro Customer Center,Lehigh Valley Customer Center,Mid Atlantic Area,Richmond Customer Center,York Customer Center</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">kristy.fishel@irco.com,ashley_patat@irco.com,Jennifer.Schroeder@irco.com,fritz.wagner@irco.com,dan_thompson@irco.com,elizabeth.maiorana@irco.com,grace.ross@irco.com,john.ridgway@irco.com,lawrence_kunkel@irco.com,ernest.moppin@irco.com,tiff.frank@irco.com,todd_sheridan@irco.com</value>
    </values>
</CustomMetadata>
