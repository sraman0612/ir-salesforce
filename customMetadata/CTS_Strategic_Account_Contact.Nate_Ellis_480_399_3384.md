<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Nate Ellis (480) 399-3384</label>
    <protected>false</protected>
    <values>
        <field>Contact_Email__c</field>
        <value xsi:type="xsd:string">nate_ellis@irco.com</value>
    </values>
</CustomMetadata>
