<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CTS OM Administration</label>
    <protected>false</protected>
    <values>
        <field>CTS_Asset_No_Serial_Misc__c</field>
        <value xsi:type="xsd:string">02i0a0000031m6mAAA</value>
    </values>
    <values>
        <field>CTS_North_CentralParts_R_T_Id__c</field>
        <value xsi:type="xsd:string">0124Q000001NhHoQAK</value>
    </values>
    <values>
        <field>CTS_South_Central_Parts_R_T_Id__c</field>
        <value xsi:type="xsd:string">0124Q000001NhrmQAC</value>
    </values>
    <values>
        <field>CTS_South_Parts_R_T_Id__c</field>
        <value xsi:type="xsd:string">0124Q000001NhrnQAC</value>
    </values>
    <values>
        <field>Case_Queue_CTS_Non_Warranty__c</field>
        <value xsi:type="xsd:string">00G0c000004ZqiTEAS</value>
    </values>
    <values>
        <field>Case_Queue_CTS_OM_NA__c</field>
        <value xsi:type="xsd:string">00G0c000004ZqiREAS</value>
    </values>
    <values>
        <field>Case_Queue_CTS_OM_ZEKS__c</field>
        <value xsi:type="xsd:string">00G4Q000005RbFZUA0</value>
    </values>
    <values>
        <field>Case_Queue_CTS_Prod_Support_NA__c</field>
        <value xsi:type="xsd:string">00G0a000002J2hPEAS</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_APAC__c</field>
        <value xsi:type="xsd:string">00G0a00000314GbEAI</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_EMEIA_PartsID__c</field>
        <value xsi:type="xsd:string">00G0a0000031IKvEAM</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_EMEIA__c</field>
        <value xsi:type="xsd:string">00G0a00000314GdEAI</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_LATAM__c</field>
        <value xsi:type="xsd:string">00G0a00000314GgEAI</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_NA_AIRPS__c</field>
        <value xsi:type="xsd:string">00G4Q000004hvV0UAI</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_NA_Dryers__c</field>
        <value xsi:type="xsd:string">00G4Q000004hvVKUAY</value>
    </values>
    <values>
        <field>Case_Queue_TechDirect_NA__c</field>
        <value xsi:type="xsd:string">00G0a00000314GiEAI</value>
    </values>
    <values>
        <field>Case_R_T_AM_Closed__c</field>
        <value xsi:type="xsd:string">0120c000001luH7AAI</value>
    </values>
    <values>
        <field>Case_R_T_AM_Incident__c</field>
        <value xsi:type="xsd:string">0120c000001luH8AAI</value>
    </values>
    <values>
        <field>Case_R_T_AM_Problem__c</field>
        <value xsi:type="xsd:string">0120c000001luH9AAI</value>
    </values>
    <values>
        <field>Case_R_T_AM_Question__c</field>
        <value xsi:type="xsd:string">0120c000001luGEAAY</value>
    </values>
    <values>
        <field>Case_R_T_Americas_Transfers__c</field>
        <value xsi:type="xsd:string">0124Q000001Nhy1QAC</value>
    </values>
    <values>
        <field>Case_R_T_Americas__c</field>
        <value xsi:type="xsd:string">0124Q000001NhxzQAC</value>
    </values>
    <values>
        <field>Case_R_T_CTS_OM_ZEKS__c</field>
        <value xsi:type="xsd:string">0124Q000001Nhy2QAC</value>
    </values>
    <values>
        <field>Case_R_T_CTS_Prod_Support__c</field>
        <value xsi:type="xsd:string">012j0000001DbXKAA0</value>
    </values>
    <values>
        <field>Case_R_T_Non_Warranty__c</field>
        <value xsi:type="xsd:string">0120c000001luIKAAY</value>
    </values>
    <values>
        <field>Case_R_T_TechDirect_AAQ__c</field>
        <value xsi:type="xsd:string">0120a0000011AlhAAE</value>
    </values>
    <values>
        <field>Case_R_T_TechDirect_AIRPS__c</field>
        <value xsi:type="xsd:string">0124Q000001dWMJQA2</value>
    </values>
    <values>
        <field>Case_R_T_TechDirect_ZEKS__c</field>
        <value xsi:type="xsd:string">0124Q000001dWMKQA2</value>
    </values>
    <values>
        <field>Case_R_T_Transfers__c</field>
        <value xsi:type="xsd:string">0120c000001luILAAY</value>
    </values>
</CustomMetadata>
