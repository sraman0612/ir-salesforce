<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MP/OB/IRP LATAM Queue</label>
    <protected>false</protected>
    <values>
        <field>Business_Hours_Label__c</field>
        <value xsi:type="xsd:string">IR Comp Business Hours - NA</value>
    </values>
    <values>
        <field>Business_Hours_Record_Id__c</field>
        <value xsi:type="xsd:string">01m4Q000000CkIpQAK</value>
    </values>
</CustomMetadata>
