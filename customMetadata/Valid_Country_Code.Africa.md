<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Africa</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AO;BW;ET;KE;MG;MW;MU;MZ;NA;RW;SC;SZ;TZ;UG;ZM;ZW;SD;SH;SL;SN;SO;SS;ST;TD;TG;TN;YT;ZA</value>
    </values>
</CustomMetadata>
