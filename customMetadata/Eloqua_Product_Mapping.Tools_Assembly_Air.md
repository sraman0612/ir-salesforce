<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Tools – Assembly/Air</label>
    <protected>false</protected>
    <values>
        <field>Eloqua_Product__c</field>
        <value xsi:type="xsd:string">Tools – Assembly/Air</value>
    </values>
    <values>
        <field>Salesforce_Product__c</field>
        <value xsi:type="xsd:string">Tools - Assembly / Air</value>
    </values>
</CustomMetadata>
