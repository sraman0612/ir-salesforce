<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>South Central Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Dallas Customer Center,New Orleans Customer Center,San Antonio Customer Center,South Central Area,Southern Gulf District,Tyler Customer Center</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">ariel.romo@irco.com,jeremiah.moore@irco.com,Jeremiah.Moore@irco.com,Sarah.Kerzee@irco.com,michael.lowrance@irco.com,colleen.mcnamara@irco.com,grover_lee@irco.com,christopher.hernandez@irco.com,vickie.sauceda@irco.com,Adrian.Morales@irco.com,keith_whitaker@irco.com,jennifer.dotson@irco.com,jmcginni@irco.com,greg_bell@irco.com,mak.casey@irco.com</value>
    </values>
</CustomMetadata>
