<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>non cycling refrigerated dryer 0 2 8 m3</label>
    <protected>false</protected>
    <values>
        <field>Field__c</field>
        <value xsi:type="xsd:string">Lead Source 2</value>
    </values>
    <values>
        <field>Marketing_Program__c</field>
        <value xsi:type="xsd:string">LI</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">non-cycling-refrigerated-dryer-0-2-8-m3-min-7-212-cfm</value>
    </values>
</CustomMetadata>
