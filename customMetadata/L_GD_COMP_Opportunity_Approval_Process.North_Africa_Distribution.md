<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>North_Africa_Distribution</label>
    <protected>false</protected>
    <values>
        <field>Business__c</field>
        <value xsi:type="xsd:string">Compressor</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:type="xsd:string">Distribution</value>
    </values>
    <values>
        <field>Level_1__c</field>
        <value xsi:type="xsd:string">sofiane.djebari@irco.com</value>
    </values>
    <values>
        <field>Level_2__c</field>
        <value xsi:type="xsd:string">francisco_gomez@irco.com</value>
    </values>
    <values>
        <field>Level_3_1__c</field>
        <value xsi:type="xsd:string">rajesh_ganjoo@irco.com</value>
    </values>
    <values>
        <field>Level_3__c</field>
        <value xsi:type="xsd:string">frederic_roux@irco.com</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">Special price request</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">Standard</value>
    </values>
    <values>
        <field>Region__c</field>
        <value xsi:type="xsd:string">North Africa</value>
    </values>
    <values>
        <field>Sales_Enablement__c</field>
        <value xsi:type="xsd:string">david_gonzales@irco.com</value>
    </values>
</CustomMetadata>
