<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>A child record for the Account Plan object; contains the goals, strengths and weaknesses for the Account Plan for the plan year.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AccountObjective1__c</fullName>
        <description>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</description>
        <externalId>false</externalId>
        <inlineHelpText>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</inlineHelpText>
        <label>Account Objective #1</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>AccountObjective2__c</fullName>
        <description>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</description>
        <externalId>false</externalId>
        <inlineHelpText>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</inlineHelpText>
        <label>Account Objective #2</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>AccountObjective3__c</fullName>
        <description>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</description>
        <externalId>false</externalId>
        <inlineHelpText>The key objectives for the account in the plan year - objectives should be specific, measurable, agreed-upon, realistic, and time-bound</inlineHelpText>
        <label>Account Objective #3</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>AccountPlan__c</fullName>
        <externalId>false</externalId>
        <label>Account Plan</label>
        <referenceTo>AccountPlan__c</referenceTo>
        <relationshipName>Account_Plan_Discovery_Strategy</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Attachment_Rate__c</fullName>
        <description>Attachment rate for consumer</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the attachment rate for the consumer based on plan year</inlineHelpText>
        <label>Attachment Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>CC_Connectivity_Target__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Connectivity Target</inlineHelpText>
        <label>Connectivity Target</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Commercial_UVO__c</fullName>
        <description>Annual Commercial UVO</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Commercial UVO for the account plan year</inlineHelpText>
        <label>Commercial UVO</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Consumer_UVO__c</fullName>
        <description>Consumer UVO</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Consumer UVO for the account plan year</inlineHelpText>
        <label>Consumer UVO</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Golf_Fleet__c</fullName>
        <description>Golf Car Fleet UVO for channel partners</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Golf Fleet UVO for channel</inlineHelpText>
        <label>Golf Fleet UVO</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IRSPX_RS_Type_of_Goal__c</fullName>
        <description>Please enter the Account&apos;s goals and objectives from an Ingersoll Rand and Account perspective. (note: You&apos;ll need to click &quot;Save and New&quot; to capture both IR&apos;s and Account)</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the Account&apos;s goals and objectives from an Ingersoll Rand and Account perspective. (note: You&apos;ll need to click &quot;Save and New&quot; to capture both IR&apos;s and Account)</inlineHelpText>
        <label>Type of Goal</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Customer</fullName>
                    <default>true</default>
                    <label>Customer</label>
                </value>
                <value>
                    <fullName>Ingersoll Rand</fullName>
                    <default>false</default>
                    <label>Ingersoll Rand</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>MainAccountGoal__c</fullName>
        <description>For the plan year, the main goal for the account. This is likely to be around revenue growth or share and it should be measurable</description>
        <externalId>false</externalId>
        <inlineHelpText>For the plan year, the main goal for the account. This is likely to be around revenue growth or share and it should be measurable</inlineHelpText>
        <label>Main Account Goal</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Parts_Objective__c</fullName>
        <description>Parts objective for account plan</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the annual parts objective for the current plan year</inlineHelpText>
        <label>Parts Objective</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Account Plan Goals and Objectives</label>
    <nameField>
        <displayFormat>IRSPX-APGO-{000000}</displayFormat>
        <label>Account Plan Goals and Objectives</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Plan Goals and Objectives</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
