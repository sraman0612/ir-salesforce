<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CC_ERP_Item_Number__c</fullName>
        <description>Club Car ERP (currently MAPICS) Item Number of the Product being transacted.</description>
        <externalId>false</externalId>
        <formula>ProductItem.Product2.ERP_Item_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Club Car ERP (currently MAPICS) Item Number of the Product being transacted.</inlineHelpText>
        <label>CC ERP Item Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CC_Inventory_Trx_Type__c</fullName>
        <description>Identifier/Code for the type of inventory transaction being sent to Club Car ERP (currently MAPICS)</description>
        <externalId>false</externalId>
        <formula>IF( 
	ISPICKVAL(TransactionType, &quot;Transferred&quot;),
	&quot;TW&quot;, 
	IF(
		ISPICKVAL(TransactionType, &quot;Consumed&quot;),
		&quot;IS&quot;,
		IF(
			ISPICKVAL(TransactionType, &quot;Adjusted&quot;) &amp;&amp; NOT(ISBLANK( Source_Location__c )) &amp;&amp; NOT(ISBLANK( Source_Warehouse__c )),
			&quot;IS&quot;,
			&quot;ERR&quot;
		)
	) 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Identifier/Code for the type of inventory transaction being sent to Club Car ERP (currently MAPICS)</inlineHelpText>
        <label>CC Inventory Trx Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Destination_Location__c</fullName>
        <description>Destination location this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</description>
        <externalId>false</externalId>
        <inlineHelpText>Destination location this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</inlineHelpText>
        <label>Destination Location</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Destination_Warehouse__c</fullName>
        <description>Destination warehouse this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</description>
        <externalId>false</externalId>
        <inlineHelpText>Destination warehouse this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</inlineHelpText>
        <label>Destination Warehouse</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductItemId</fullName>
        <trackHistory>true</trackHistory>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>ProductItemTransactionNumber</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Quantity</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>RelatedRecordId</fullName>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Reprocess__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Reprocess</label>
        <trackHistory>true</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Source_Location__c</fullName>
        <description>Source location this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</description>
        <externalId>false</externalId>
        <inlineHelpText>Source location this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</inlineHelpText>
        <label>Source Location</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Warehouse__c</fullName>
        <description>Source warehouse this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</description>
        <externalId>false</externalId>
        <inlineHelpText>Source warehouse this Product Item Transaction is for.  Used in Club Car to MAPICS inventory integration.</inlineHelpText>
        <label>Source Warehouse</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TransactionType</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Work_Order_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Work_Order__r.WorkOrderNumber</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Work Order Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Work_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Work Order</label>
        <referenceTo>WorkOrder</referenceTo>
        <relationshipLabel>Product Item Transactions</relationshipLabel>
        <relationshipName>Product_Item_Transactions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
